<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<link rel="stylesheet" href="<?php echo base_url("css/credit.css"); ?>">
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Company</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Company</a></li>
      <li class="active">Add Compan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
     
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="save_company" action="" method="post" enctype="multipart/form-data" autocomplete="off">
              
            <input type="hidden" value="<?=(isset($company[0]->id))?$company[0]->id:"";?>"  name="id"  id="id"/>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Name</label>
                      <input type="text" class="form-control validate[required]" name="company_name" id="company_name" value="<?=(isset($company[0]->company_name))?$company[0]->company_name:"";?>" placeholder="Company Name">
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Email</label>
                      <input type="text" class="form-control" name="email" value="<?=(isset($company[0]->email))?$company[0]->email:"";?>" id="email" placeholder="Email" onchange="check_email();">
                  </div>  
                </div>
              </div>
            
               <!-- <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Password</label>
                      <input type="password" class="form-control" name="password" value="<?=(isset($company[0]->password))?$company[0]->email:"";?>" id="password" placeholder="Password" ">
                  </div>  
                </div>
              </div> -->
           
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Contact</label>
                      <input type="text" class="form-control validate[required]" name="phone" value="<?=(isset($company[0]->phone))?$company[0]->phone:"";?>" id="phone" placeholder="contact">
                  </div>  
                </div>
              </div>
              
               <!-- <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Address</label>
                    <textarea id="address" name="address" class="form-control validate[required]" rows="3" placeholder="Enter ..."><?php if(isset($company[0]->address)) {echo $company[0]->address; } else { echo "";}?></textarea>
                  </div>  
                </div>
              </div> -->
              
                              
               <?php  if ($company[0]->is_card_added==0){
               	$adddisplay="display:";
				$removedisplay="display:none";
               }else{
               	$adddisplay="display :none";
				$removedisplay="";
               	
               }?>
              <div  class="row Add_CC" style="<?php echo $adddisplay ?>">
              	<div class="col-sm-6">
              		<div class="form-group">
              		    <button id="CC" class="btn btn bg-olive margin" type="button">Add Credit Card</button>
              		</div>
              	</div>
              </div>
             <div id="ccerror" style="display: none; width: 500px" class="alert alert-danger alert-dismissible"></div>
           
             
              <div id="ccsuccess" style="display: none; width: 500px" class="alert alert-success alert-dismissible"></div>
              <div id="Addcc">
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Credit Card Number</label>
                      <input type="text"  class="form-control cc_number" maxlength="19" name="cc_number" value="" id="cc_number" placeholder="Card Number">
                  </div>  
                </div>
              </div>
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Exp Date</label>
                    <div> <label>Month </label>
                      <span style="padding: 12px">
                      	<select name="month" id="month" class="month">
                      			<option value="">Month</option>
                      		<?php foreach (range(1, 12) as $number) {?>
  						<option value="<?php echo $number ?>"><?php echo $number ?></option>
						<?php }?></select>
						</span>
						<label>Year</label>
 						<span style="padding: 12px">
 							<select name="year" id="year" class="year">
 								<option value="">Year</option>
 								<?php foreach (range(date(Y), date("Y")+12) as $number) {?>
  							<option value="<?php echo $number ?>"><?php echo $number ?></option>
							<?php }?>
							</select>
						</span>
                      </div>
                  </div>  
                </div>
              </div>
              
               <div class="row">
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="last name">CVV</label>
                      <input class="cvv" min="3" maxlength="4" type="text" class="form-control validate[required]" name="cvv" value="<?=(isset($company[0]->phone))?$company[0]->cvv:"";?>" id="cvv" placeholder="CVV">
                  </div>  
                </div>
                </div>
                <div class="row">
                	<div class="col-sm-2">
                  <div class="form-group">
                  	 <label for="last name"></label>
                 <button type="button" class="form-control btn btn-info" id="verify" name="submit">Verify</button>
                 </div>
                 </div>
                </div>
                 <input type="hidden"  name="btcustomer_id" id="btcustomer_id"  />
                 <input type="hidden" name="token" id="token" />
                 <input type="hidden"  name="type" id="type"  />
             
             
              	
             
              
              </div>
            
                <div class="row" id="RemoveCC" style="<?php echo $removedisplay ?>">
              	<div class="col-sm-6">
              		<div class="form-group">
              			 <label for="last name"><?php echo $company[0]->credit_card_no ?></label>
              		    <button id="RemoveCC1" class="label label-danger" type="button">Remove Credit Card</button>
              		</div>
              	</div>
              </div>
               <div id="card_loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              	
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" id="submit" name="submit">Submit</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>

<!--<script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>
    
<script type="text/javascript">
//var $ = jQuery.noConflict();

$(document).ready(function(){
	//$(".cc_number").credit();
$("#Addcc").hide();
    function submit_form(formData,status)
    {
        if(status==true)
        {
            var querystr = $("#save_company").serialize();
            
            
            
            $.ajax({
                url		:	"<?php echo site_url('Company/submit_company'); ?>",
                type	:	"POST",
                data	:	querystr,
                beforeSend  :   function(){
                $("#loader").show();
               },   
                success	:	function(data){
                  if(data == 1)
                  {
                    $("#loader").hide();
                 
                    setTimeout(function() {
                        $.bootstrapGrowl("Company is added successfully", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_company')[0].reset();
                    window.location='<?php echo site_url('company'); ?>';
                  }
                  else
                  {
                    $("#loader").hide();
                    //window.location='<?php echo site_url('login/dashboard'); ?>';
                    //alert('failed to save');
                  }
                 }
            });
            return false;
        }
        
    }
            
      
    
  $("#save_company").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});
$('#cc_number').keyup(function() {
		  var foo = $(this).val().split("-").join(""); // remove hyphens
		  if (foo.length > 0) {
		    foo = foo.match(new RegExp('.{1,4}', 'g')).join("-");
		  }
		  $(this).val(foo);
		});
</script>
<script>
  function check_email()
{
  var email=$('#email').val();
   $.ajax({
                url		:	"<?php echo site_url('Company/check_email'); ?>",
                type	:	"POST",
                data	:	{'email_id':email},
               
                
                beforeSend  :   function(){
                                   
                                    //alert('show loader');
                                },   
                success	:	function(data){
                  if(data == 1)
                  {
                   
                    setTimeout(function() {
                        $.bootstrapGrowl("This email is already exist", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                   $('#submit').prop('disabled', true);
                  }
                  
                   
                          else
                       {
                   $('#submit').removeAttr('disabled');;
                  }
                 }
            });
            return false;
   }
   
    $( "#CC" ).click(function() {
  		$( "#Addcc" ).toggle("slow", function(e) {
  			
  			var isVisible = $( "#Addcc" ).is( ":visible" );
  		   if(isVisible==true)
  		   {
  		   	
  		   	  $('#submit').attr('disabled','disabled');
  		   }else
  		   {
  		   	$("#ccerror").css("display",'none');
                  	$("#ccerror").html('');
  		   	  $('#submit').removeAttr('disabled');
  		   }
  			
    // Animation complete.
  });
	});

$("#verify").click(function(){

	var cc=$(".cc_number").val();
	var month=$("#month").val();
	var year=$("#year").val();
	var cvv=$("#cvv").val();
	var name=$("#company_name").val();
	var email=$("#email").val();
	var phone=$("#phone").val();
	
	var status=true;
		$("#ccerror").html('');
	if(cc=="")
	{
		
		$(".cc_number").css("border","2px solid #dd4b39");
		status=false;
	}
	if(month=='')
	{
		$("#month").css("border","solid #dd4b39 2px");
		status=false;
	}
	if(year=='')
	{
		$("#year").css("border","solid #dd4b39 2px");
		status=false;
	}
	if(cvv=='')
	{
			$("#cvv").css("border","solid #dd4b39 2px");
			status=false;
	}
	if (status==true)
{	 
	$("#verify").attr('disabled','disabled');
	  $.ajax({
                url		:	"<?php echo site_url('Company/VerifyBtAccount'); ?>",
                type	:	"POST",
                data	:	{'cc_number':cc,"month":month,"year":year,"cvv":cvv,"name":name,"phone":phone,"email":email},
               beforeSend  :   function(){
                                   
                                    $("#card_loader").css("display","");
                                },   
                success	:	function(data){
                	   var ResponseData=jQuery.parseJSON(data);
                	     $("#card_loader").css("display","none");
                	     	
                  if(ResponseData.status == 1)
                  {
                  	$("#btcustomer_id").val(ResponseData.customer_id);
                  	$("#token").val(ResponseData.token);
                  	$("#type").val(ResponseData.cardtype);
                     $("#ccsuccess").css("display",'');
                	 $("#ccerror").css("display",'none');
                	  $("#ccsuccess").html("Success! Your Card verified ");
                    setTimeout(function(){  $("#ccsuccess").css("display",'none');
                          $("#ccerror").css("display",'none');
                     }, 3000);
                  $('#submit').removeAttr('disabled');
                  }else
                  {
                  //	alert(ResponseData.message);
                  	$("#verify").removeAttr('disabled');
                  	$("#ccerror").css("display",'');
                  	$("#ccerror").html(ResponseData.message);
                  	  setTimeout(function(){  $("#ccsuccess").css("display",'none');
                          $("#ccerror").css("display",'none');
                     }, 3000);
                   // $('#submit').removeAttr('disabled');
                  }
                 }
            });
           }
            
           
                 
                 //return false;
	
})

$(".cvv").keypress(function(){
	
	$(".cvv").css("border","solid 2px");
})
 
 $(".month").keypress(function(){
	
	$(".month").css("border","solid 2px");
})

 $(".year").keypress(function(){
	
	$(".year").css("border","solid 2px");
})
 $(".cc_number").keypress(function(){
	
	$(".cc_number").css("border","solid 2px");
})


$("#RemoveCC").click(function(){
	
	var companyID=$("#id").val();
	 $.ajax({
                url		:	"<?php echo site_url('Company/DeleteCC'); ?>",
                type	:	"POST",
                data	:	{'id':companyID},
               beforeSend  :   function(){
                       
                                      $("#card_loader").css("display","");
                                },   
                success	:	function(data){
                	  $("#card_loader").css("display","none");
                	//alert(data);
                  var ResponseData=jQuery.parseJSON(data);
                  if(ResponseData.status == 1)
                  {
                  	//alert();
                  	$("#RemoveCC").css("display","none");
                  	$(".Add_CC").css("display","");
                  }else
                  {
                  	$("#ccerror").val(ResponseData.message);
                    
                  }
                 }
            });
})
</script>
