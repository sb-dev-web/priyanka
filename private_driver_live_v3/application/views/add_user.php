<?php 

 $employee = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
 if($employee){
 	$eoormessage=0;
	 $display="display: ";
if($emp_detail['is_delete']==1)
{
	$errormessage="This Employee already deleted";
}elseif(!$emp_detail)
	{
		$errormessage="This employee is not belong to your company";
	}else
		{
			
		}
 }
   ?>

<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>User</h1>
    <ol class="breadcrumb">
      <!-- <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li> -->
      <li><a href="#">User</a></li>
      <li class="active">Add User</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
      <?php 
      if($errormessage)
	  { $display="display: none";?>
	  	 <div id="ccerror" style="display: ; width: 500px" class="alert alert-danger alert-dismissible">
	  	 	<?php echo $errormessage ?>
	  	 	
	  	 </div>
	  	
	 <?php  }      ?>
      <div class="box-body" > <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0" style="<?php echo $display ?>">
            <form id="save_driver" action="" method="post" enctype="multipart/form-data" autocomplete="off">
              
            
              <input id="company_name" name="company_name"  value="" type="hidden" />
              
               <input type="hidden" style="display: none"  name="userData" id="userData" value='<?php echo json_encode($user_detail); ?>' />
               
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">User Type</label>
                    <select class="form-control validate[required] user_type" name="user_type" id="user_type" placeholder="Select User Type">
                    	<option></option>
                                          
                        <option value="N">New User</option>
						<option value="A">App User</option>
					   
                    </select>
                  </div>  
                </div>
              </div>
              
               <div class="row" id ="newUserEmail" style="display: none">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email_id">Email</label>
                      <select class="form-control" id="user" name="email_id" onchange="getUserDetail(this)" placeholder="Select App User ">
                      <option></option>
                      	<?php foreach($user_detail as $user) { ?>
                      		<option value="<?php echo $user['email'] ?>"><?php echo $user['name'] ?> </option>
                      		
                      		
                      <?php }?>
                      	
                      </select>
                  </div>  
                </div>
              </div>
              
               <div class="row" id ="appUserEmail" >
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email_id">Email</label>
                      <input type="email" class="form-control validate[required, custom[email]]" id="email_id"  name="email_id" placeholder="Email" value="" onchange="check_email();">
                  </div>  
                </div>
              </div>

              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Name</label>
                      <input type="text" class="form-control validate[required]" name="first_name" id="first_name" value="" placeholder="Name">
                  </div>
                </div>
              </div>
              
                              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email_id">Country Code</label>
                      <input type="contact" maxlength="4" class="form-control validate[required]" id="country_code"  name="country_code" value="" placeholder="Country Code" onchange="check_phone()" >
                  <p class="help">ex. +47</p>
                  </div>  
                </div>
              </div>
             
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email_id">Phone Number</label>
                      <input type="contact" class="form-control validate[required]" id="contact" value="" name="contact" placeholder="Phone Number" onchange="check_phone()" >
                  <p class="help">ex. 123456799</p>
                  </div>  
                </div>
              </div>
            
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Company Name</label>
                     <select class="form-control validate[required]" onchange="companySelect()" name="company_id" id="company_id" placeholder="Select Company Name">
                     	<option></option>
                     	<?php foreach ($company_detail as $company) {?>
							 
                     	<option value="<?php echo $company['id'] ?>"><?php echo $company['company_name'] ?></option>
                     						<?php 	 } ?>

                     </select>
                  </div>  
                </div>
              </div> 
              
             <input type="hidden" name="user_id" id="user_id" />
              
               
              <!-- <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="Password">Password</label>
                      <input type="password" class="form-control  id="password" value="" name="password" placeholder="Enter Password">
                  </div>  
                </div>
              </div> -->            
           
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" id="submit" name="submit">Send</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>
<!--<script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>
    
<script type="text/javascript">
//var $ = jQuery.noConflict();
$(document).ready(function(){
	
	$("#company_id").select2();
	//$("#user_type").select2({  allowClear: true});
	$("#user").select2({  allowClear: true});

    function submit_form(formData,status)
    {
        if(status==true)
        {
        	$('#submit').removeAttr('disabled');
            var querystr = $("#save_driver").serialize();
            $.ajax({
                url		:	"<?php echo site_url('Generalinfo/SaveUser'); ?>",
                type	:	"POST",
                data	:	querystr,
                            
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                	//alert(data);
                	if(data==1){
                		// if(('#id').val()>0){
                			// var action ="update";
                		// }else
                		// {
                			// var action="added"
                		// }
                		var msg ="Employee detail successfully saved";
                		var type="success";
                		 
                	}else
                	{
                		var msg ="Employee detail not saved Please contact to service provider"
                		var type ="error";
                		
                	}
                     var r = confirm(msg); 
                
                    if(r==true) // 
                    {
                    $("#loader").hide();
                 
                    setTimeout(function() {
                        $.bootstrapGrowl(msg, {
                            type: type,
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_driver')[0].reset();
                   
                  }
                  else
                  {
                    $("#loader").hide();
                     $('#save_driver')[0].reset();
                   
                  }
                 }
            });
            return false;
        }
        
    }
            
      
    
  $("#save_driver").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});
</script>

<script>
function companySelect()
{

	var cname= $("#company_id option:selected").text();
	$("#company_name").val(cname);
	//alert(cname);
}
	
//})

$("#user_type").change(function(){
	//alert(this.value);
	if(this.value=="A")
	{
		$("#appUserEmail").css("display","none");
		$("#newUserEmail").css("display","");
		$("#email").prop('readonly', true);
		$("#contact").prop("readonly",true);
		$("#country_code").prop("readonly",true);
		$("#first_name").prop("readonly",true);
		
		
	}else
	{
		
		$("#appUserEmail").css("display","");
		$("#newUserEmail").css("display","none");
		$("#email").prop('readonly', false);
		$("#contact").prop("readonly",false);
		$("#country_code").prop("readonly",false);
		$("#first_name").prop("readonly",false);
		
		$("#email_id" ).val('');
		$("#first_name").val('');
		$("#contact").val('');
		$("#country_code").val('');
		
	}
})

function getUserDetail(datavalue)
{
	//alert(datavalue.value);
	var data=$("#userData").val();
	$("#email_id").val(datavalue.value);
	 var json = $.parseJSON(data);
$(json).each(function(i,val){
	if(val.email==datavalue.value)
	{
		var userName=val.name;
		var email=val.email;
		var countryCode=val.country_code;
		var phone=val.phone;
		var user_id=val.id;
		$("#first_name").val(userName);
		$("#email").val(email);
		$("#contact").val(phone);
		$("#country_code").val(countryCode);
		$("#user_id").val(user_id);
		
	}
   /* $.each(val,function(k,v){
          console.log(k+" : "+ v);     
});*/
});
	
}

$("#country_code").change(function() {
  var contact=$("#country_code").val();
  var FirstChar=contact.charAt(0);
  if(FirstChar!="+")
  {
  
  	$("#country_code").val("");
  }
  
});

$("#contact").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

function check_email()
{
	
  var email=$('#email_id').val();
  var user_id=$("#id").val();
   $.ajax({
                url		:	"<?php echo site_url('Generalinfo/check_email'); ?>",
                type	:	"POST",
                data	:	{'email_id':email,'user_id':user_id},
               
                
                beforeSend  :   function(){
                },   
                success	:	function(data){
                  if(data == 1)
                  {
                   
                    setTimeout(function() {
                        $.bootstrapGrowl("This email id already register", {
                            type: 'error',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#email_id').val('');
                   $('#submit').prop('disabled', true);
                  }
                         
                          else
                       {
                   $('#submit').removeAttr('disabled');
                  }
                 }
            });
            return false;
}


function check_phone()
{
	
    var phone=$('#contact').val();
  var country_code=$("#country_code").val();
  
//  var phone=country_code+phone;
  
  var user_id=$("#id").val();
   $.ajax({
                url		:	"<?php echo site_url('Generalinfo/check_phone'); ?>",
                type	:	"POST",
                data	:	{'phone':phone,'user_id':user_id,'country_code' : country_code},
               
                
                beforeSend  :   function(){
                },   
                success	:	function(data){
                  if(data == 1)
                  {
                   
                    setTimeout(function() {
                        $.bootstrapGrowl("This phone number already register", {
                            type: 'error',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#contact').val('');
                   $('#submit').prop('disabled', true);
                  }
                         
                          else
                       {
                   $('#submit').removeAttr('disabled');
                  }
                 }
            });
            return false;
}


 

</script>     
