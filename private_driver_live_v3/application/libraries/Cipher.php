<?php
error_reporting(0);
class Cipher
{
    private $securekey, $iv;
    function __construct($textkey)
    {
         $textkey="PRIVATETAXI_APP";
        $this->securekey = hash('sha256', $textkey, true);
        $this->iv = mcrypt_create_iv(32);
    }
    function encrypt($input)
    {
        $dirty = array(  "+",     "/",      "=");
        $clean = array(
            "_PLUS_",
            "_SLASH_",
            "_EQUALS_");
        $encrypted_string = base64_encode(mcrypt_encrypt(MCRYPT_BLOWFISH, $this->
            securekey, $input, MCRYPT_MODE_ECB, $this->iv));
        return str_replace($dirty, $clean, $encrypted_string);

    }
    function decrypt($input)
    {
        $dirty = array(
            "+",
            "/",
            "=");
        $clean = array(
            "_PLUS_",
            "_SLASH_",
            "_EQUALS_");

        $input = str_replace($clean, $dirty, $input);
        return trim(mcrypt_decrypt(MCRYPT_BLOWFISH, $this->securekey, base64_decode($input),
            MCRYPT_MODE_ECB, $this->iv));
    }
}

?>