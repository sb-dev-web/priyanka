<?php
	class General_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		/* fetch order information */
		function fetch_order_info()
		{
			$query = $this -> db -> query("Select * from item_info where isdelete='0' order by item_id ASC");
			// echo $this->db->last_query();
			return $query -> result();

		}

		/* save order information */
		function save_order($id = "0")
		{

			$arr_feilds = array(
				'item_code' => $_POST['item_code'],
				'item_name' => $_POST['item_name'],
				'item_price' => $_POST['item_price'],
				'isactive' => '1'
			);

			if ($id > 0)
			{
				$this -> db -> where('item_id', $id);

				$this -> db -> update('item_info', $arr_feilds);
				return 'updated';
			}
			else
			{

				$this -> db -> insert('item_info', $arr_feilds);

				$insert = $this -> db -> insert_id();

				return $insert;

			}
		}

		/* get order item */
		function get_order($id)
		{
			if ($id > 0)
				$this -> db -> where('item_id', $id);

			$query = $this -> db -> get("item_info");
			//echo $this->db->last_query();exit;
			return $query -> result();
		}

		/* get order delete */
		function delete_item($id)
		{
			$this -> db -> where('item_id', $id);
			//$this->db->set('isdelete','1');
			$this -> db -> update("item_info");
			return 1;
		}

		/* fetch fare information */
		function fetch_fare_info()
		{
			$query = $this -> db -> query("Select * from fare_info as f ,cab_type as c where f.cab_type=c.type_id and f.type='NORMAL'");
			// echo $this->db->last_query();
			return $query -> result();
		}

		/*get particular fare */
		function fetch_particular_fare($type)
		{
			$query = $this -> db -> query("Select * from fare_info as f ,cab_type as c where f.cab_type=c.type_id and f.type='$type'");
			// echo $this->db->last_query();
			return $query -> result();
		}

		/* get fare */
		function get_fare($id)
		{
			if ($id > 0)
				$query = $this -> db -> query("select fare_info.id,fare_info.no_of_person,fare_info.min_charges,fare_info.min_km,fare_info.charges_per_km,fare_info.wait_time_charges,fare_info.ride_later_time,fare_info.type,cab_type.title from fare_info,cab_type where fare_info.id=$id and fare_info.cab_type=cab_type.type_id");
			return $query -> result();
		}

		/* save fare info */
		function save_fare($id = "0")
		{

			$arr_feilds = array(
				'description' => $_POST['description'],
				'no_of_person' => $_POST['nop'],
				'min_charges' => $_POST['min_charges'],
				'min_km' => $_POST['min_km'],
				'charges_per_km' => $_POST['charge_km'],
				'wait_time_charges' => $_POST['Waittime_charges'],
				'ride_later_time' => $_POST['booking_time']
			);

			if ($id > 0)
			{
				$this -> db -> where('id', $id);

				$this -> db -> update('fare_info', $arr_feilds);
				return 'updated';
			}
			else
			{

				$this -> db -> insert('fare_info', $arr_feilds);

				$insert = $this -> db -> insert_id();

				return $insert;

			}
		}

		/* change order staus */
		function change_order_status($id, $status)
		{
			if ($status == "1")
			{
				$field_array = array("isactive" => "1");
				$status == '1';
			}
			else
			{
				$field_array = array("isactive" => "0");
				$status == '0';
			}
			$this -> db -> where('item_id', $id);
			$this -> db -> update("item_info", $field_array);
			return $status;
		}

		/* change fare status */
		function change_fare_status($id, $status)
		{
			if ($status == "1")
			{
				$field_array = array("isactive" => "1");
				$status == '1';
			}
			else
			{
				$field_array = array("isactive" => "0");
				$status == '0';
			}
			$this -> db -> where('id', $id);
			$this -> db -> update("fare_info", $field_array);
			return $status;
		}

		/* fetch company user infomation */
		function Fetch_cuser_info($limit, $offset = 0)
		{
			// $query=$this->db->query("select user.*,company.company_name as assgin_company
			// from user LEFT JOIN company ON company.id=user.company_id where
			// user.is_company='Y' order by  user.id DESC limit $offset,$limit");
			$query = $this -> db -> query("SELECT user.id ,user.name,user.email,concat(user.country_code,user.phone) phone,user_company.company_name,user_company.is_approved,company.company_name as approv_companyname,user_company.company_id FROM `user_company` join user on user.id=user_company.user_id and is_delete=0 left join company on user_company.company_id=company.id order by user.company_id  limit $offset,$limit");
			return $query -> result();
		}

		/*count company user */
		function company_user_count()
		{
			$query = $this -> db -> query("select id from user_company where is_delete=0");
			return $query -> num_rows();
		}

		/* actrive company user */
		function Active_companyuser($id, $status)
		{
			if ($status == "1")
			{
				$field_array = array("is_active" => "1");
				$status == '1';
			}
			else
			{
				$field_array = array("is_active" => "0");
				$status == '0';
			}
			$this -> db -> where('id', $id);
			$this -> db -> update("user", $field_array);
			return $status;
		}

		/*fetch normal user */
		function Fetch_user_info($limit, $offset = 0)
		{
			$query = $this -> db -> query("Select * from user order by id DESC limit $offset,$limit");
			// echo $this->db->last_query();
			return $query -> result();
		}

		/*count normal user */
		function user_count()
		{
			$query = $this -> db -> query("Select * from user order by id DESC");
			return $query -> num_rows();

		}

		/* function fetch coupon information */
		function Fetch_coupon_info($limit, $offset = 0)
		{
			$coupon = $this -> db -> query("select * from coupon where isdelete='0'   limit $offset,$limit");
			// echo $this->db->last_query();
			return $coupon -> result();
		}

		function Update_coupon_status($id, $status)
		{
			if ($status == "1")
			{
				$field_array = array("isactive" => "1");
				$status == '1';
			}
			else
			{
				$field_array = array("isactive" => "0");
				$status == '0';
			}
			$this -> db -> where('id', $id);
			$this -> db -> update("coupon", $field_array);
			return $status;
		}

		/* get coupon detail */
		function get_coupon($id)
		{
			if ($id > 0)
				$this -> db -> where('coupon.id', $id);
			$this -> db -> select("coupon.*,user_coupon.coupon_id,user_coupon.user_id");
			$this -> db -> from("coupon");
			$this -> db -> join("user_coupon", "coupon.id=user_coupon.coupon_id", "left");
			$query = $this -> db -> get();
			//echo $this->db->last_query();
			return $query -> result();
		}

		/* submit coupon */
		function save_coupon()
		{
			//print_r($_POST);
			//exit;
			if ($_POST['id'] > 0)
			{
				$id = $_POST['id'];
				$this -> db -> where('id', $id);

				$arr_feilds = array(
					'title' => $_POST['title'],
					'offer' => $_POST['offer'],
					'description' => $_POST['description'],
					'start_date' => $_POST['start_date'],
					'expire_date' => $_POST['end_date'],
					'code' => $_POST['code'],
					"coupon_type" => $_POST['coupon_type'],
					'isactive' => '1'
				);
				$update = $this -> db -> update('coupon', $arr_feilds);

				if (count($_POST['userlist']) > 0)
				{
					//delete the existing user
					$this -> db -> query("delete from user_coupon where coupon_id=$id");
					//echo "there";
					foreach ($_POST['userlist'] as $value)
					{

						$device_token = array();
						$insert_arr = array(
							"coupon_id" => $id,
							"user_id" => $value,
							"created_on" => get_gmt_time()
						);
						$this -> db -> insert("user_coupon", $insert_arr);

						//insert into notification table
						$data = array(
							"notification_id" => $id,
							"user_id" => $value,
							"is_view" => "N",
							"type" => "Campaign",
							"created_on" => get_gmt_time()
						);
						//print_r($data);
						$insert = $this -> db -> insert("user_notification", $data);

						//send push
						$device_detail = $this -> GetDeviceInfo($value);

						if ($device_detail['device_type'] == 'I')
						{
							if ($device_detail['device_token'])
							{
								//$device_token[] = $device_detail['device_token'];
								$badge = $this -> GetBadge($device_detail['id']);
								//$badge=1;
								$device_tokens[] = array(
									"user_id" => $device_detail['id'],
									"badge" => $badge,
									"token" => $device_detail['device_token'],
									//"username"=>$device_token['name']
								);

								$message_text = $_POST['description'];
								$otherdata = array("type" => "Campaign");
								send_push($device_tokens, $message_text, $otherdata);
							}
						}
						else
						{
							if ($device_detail['device_token'])
							{
								$badge = $this -> GetBadge($device_detail['id']);
								$device_token[] = $device_detail['device_token'];
								$message_text = $_POST['description'];
								//$device_detail['device_token'];
								$registration[] = array(
									//"user_id" => $device_token['id'],
									"badge" => $badge,
									"token" => $device_detail['device_token'],
									"type" => "Campaign",
								);

							}
							//print_r($registration);
							send_andorid_push($message_text, $registration);
							//send_andorid_push_coupon($message_text, $device_token);
						}
					}

				}

				return "updated";
			}
			else
			{
				$date = get_gmt_time();
				$arr_feilds = array(
					'title' => $_POST['title'],
					'offer' => $_POST['offer'],
					'description' => $_POST['description'],
					'start_date' => $_POST['start_date'],
					'expire_date' => $_POST['end_date'],
					'code' => $_POST['code'],
					'isactive' => '1',
					"coupon_type" => $_POST['coupon_type'],
					'createdon' => $date
				);

				$this -> db -> insert('coupon', $arr_feilds);

				$insert = $this -> db -> insert_id();
				if ($insert)
				{
					if (count($_POST['userlist']) > 0)
					{
						foreach ($_POST['userlist'] as $value)
						{

							$device_token = array();
							$insert_arr = array(
								"coupon_id" => $insert,
								"user_id" => $value,
								"created_on" => get_gmt_time()
							);
							$this -> db -> insert("user_coupon", $insert_arr);
							//send push
							$device_detail = $this -> GetDeviceInfo($value);

							if ($device_detail['device_type'] == 'I')
							{
								if ($device_detail['device_token'])
								{
									//$device_token[] = $device_detail['device_token'];
									$badge = $this -> GetBadge($device_detail['id']);
									//$badge=1;
									$device_tokens[] = array(
										"user_id" => $device_detail['id'],
										"badge" => $badge,
										"token" => $device_detail['device_token'],
										//"username"=>$device_token['name']
									);

									$message_text = $_POST['description'];
									$otherdata = array("type" => "Campaign");
									send_push($device_tokens, $message_text, $otherdata);
								}
							}
							else
							{
								if ($device_detail['device_token'])
								{
									$badge = $this -> GetBadge($device_detail['id']);
									$device_token[] = $device_detail['device_token'];
									$message_text = $_POST['description'];
									//$device_detail['device_token'];
									$registration[] = array(
										//"user_id" => $device_token['id'],
										"badge" => $badge,
										"token" => $device_detail['device_token'],
										"type" => "Campaign",
									);

								}
								//print_r($registration);
								send_andorid_push($message_text, $registration);
							}

						}
					}
				}

				return $insert;

			}
		}

		function GetDeviceInfo($userid)
		{
			$this -> db -> select("device_type,device_token,id");
			//$this -> db -> where("id", $userid);
			$query = $this -> db -> get_where("user", array("id" => $userid));
			//echo $this->db->last_query();
			if ($query -> num_rows())
			{
				return $query -> row_array();
			}
			else
			{
				false;
			}
		}

		/* count coupon */
		function coupon_count()
		{
			$get_count = $this -> db -> query("select * from coupon where isdelete='0' and isactive='1'");
			return $get_count -> num_rows();
		}

		/*fetch customer birthday */
		function Fetch_customer_birthday($date)
		{
			$get_date = $this -> db -> query("SELECT *FROM user WHERE DATE_FORMAT(dob,'%m-%d') = DATE_FORMAT('$date','%m-%d')");
			//echo $this->db->last_query();
			if ($get_date)
			{
				return $get_date -> result_array();
			}
			else
			{
				return false;
			}

		}

		/*delete camapgin */
		function Delete_coupon($id)
		{
			$this -> db -> where('id', $id);
			//   $this->db->set('isdelete','0');
			$this -> db -> delete("coupon");
			// echo $this->db->last_query();
			return 1;
		}

		/*get company */
		function get_company()
		{
			$get_company = $this -> db -> query("select * from company where isdelete='0' order by company_name");
			$count = $get_company -> num_rows();
			if ($count > 0)
			{
				return $get_company -> result_array();
			}
			else
			{
				return array();
			}
		}

		/*update user */
		function c_user($cid, $uid, $cname)
		{
			//update the user_company staus
			$update_data = array(
				"is_approved" => "Y",
				"company_id" => $cid,
				//"company_name" => $cname,
			);
			$this -> db -> where("user_id", $uid);
			$this -> db -> where("is_delete", "0");
			$update = $this -> db -> update("user_company", $update_data);
			if ($update)
			{
				//update the user table
				$update_user = $this -> db -> query("update user set company_id='$cid',company_name='$cname' where id=$uid");
				if ($update_user)
				{
					//get user email
					$data = $this -> db -> get_where("user", array("id" => $uid));
					$userdata = $data -> row_array();
					$email = $userdata['email'];
					//AccountPasswordSet($email, $uid, $unique_number=rand());
					if($userdata['password']=='' && $userdata['is_social']=='N'){
					CompanyAddNewUser($email);
					}
					//send a push notification
					if($userdata['device_token']){
					if ($userdata['device_type'] == 'A')
					{
						$registration[] = array(
							//"user_id" => $device_token['id'],
							"badge" => 0,
							"token" => $userdata['device_token'],
							"type" => "Approve",
						);
						$message_text = COMPANY_APPROVE;
						// echo "there";
						send_andorid_push($message_text, $registration);
					}
					else
					{
						$device_tokens[] = array(
							//"user_id" => $device_token['id'],
							"badge" => 0,
							"token" => $userdata['device_token'],
							//"username"=>$device_token['name']
						);

						// print_r($device_tokens);
						$otherdata = array("type" => "Approve");
						$message_text = COMPANY_APPROVE;
						send_push($device_tokens, $message_text, $otherdata);

					}
					}

					return 1;
				}
				else
				{
					return 0;
				}

			}
			else
			{
				return 0;
			}

		}

		/* update timezone */
		function update_time_zone($time)
		{
			$update = get_gmt_time();
			$time_info = $this -> db -> query("Update timezone SET time_zone='$time',updatedon='$update'");

			return 1;
		}

		/* update charge */
		function update_charge($time)
		{
			$update = get_gmt_time();
			$time_info = $this -> db -> query("Update prebooking_charge SET price='$time',createdon='$update' where id='1'");

			return 1;
		}

		/* get time zone */
		function get_charge()
		{
			$get_time = $this -> db -> query("select * from prebooking_charge");
			$count = $get_time -> num_rows();
			if ($count)
			{
				return $get_time -> result();
			}
			else
			{
				return array();
			}
		}

		/* get time zone */
		function get_time_zone()
		{
			$get_time = $this -> db -> query("select * from timezone");
			$count = $get_time -> num_rows();
			if ($count)
			{
				return $get_time -> result();
			}
			else
			{
				return array();
			}
		}

		/* get company user email */
		function get_email($id)
		{
			$get_email = $this -> db -> query("select * from user where id='$id'");
			$count = $get_email -> num_rows();
			if ($count)
			{
				$email = $get_email -> row();
				$email_id = $email -> email;
				return $email_id;
			}
			else
			{
				return '0';
			}
		}

		function GetUser()
		{
			$this -> db -> select("id,name,email");
			$query = $this -> db -> get_where("user", array("is_active" => '1'));

			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else

			{
				return false;
			}
		}

		function GetNonCompanyUser()
		{
			$this -> db -> select("id,name,email,country_code,phone");
			$this -> db -> order_by("name");
			$query = $this -> db -> get_where("user", array(
				"is_active" => '1',
				"company_id" => "0"
			));

			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else

			{
				return false;
			}
		}

		function GetUserDetail()
		{
			$requestData = $_REQUEST;

			$columns = array(
				// datatable column index  => database column name
				0 => 'name',
				1 => 'dob',
				2 => 'email',
				4 => 'phone',
				5 => "status"
			);

			//$company_id=4;
			$query = $this -> db -> get_where('user');
			//	echo $this->db->last_query();

			$totalData = $query -> num_rows();
			//echo $totalFiltered = $totalData;
			//exit;

			$sql = "SELECT user.name,user.company_name,user.email,user.dob,user.country_code,is_active,user.phone,user.id,user.is_active FROM `user` where is_deleted='0'  ";
			//exit;
			if (!empty($requestData['search']['value']))
			{
				// if there is a search parameter, $requestData['search']['value'] contains
				// search parameter
				$sql .= " AND ( name LIKE '" . $requestData['search']['value'] . "%' ";
				$sql .= " OR email LIKE '%" . $requestData['search']['value'] . "%' ";
				$sql .= " OR company_name LIKE '%" . $requestData['search']['value'] . "%' ";
				$sql .= " OR phone LIKE '%" . $requestData['search']['value'] . "%' )";
			}
			$query = $this -> db -> query($sql);
			//echo $this->db->last_query();
			//exit;
			$totalFiltered = $query -> num_rows();

			$sql .= " ORDER BY  id asc limit " . $requestData['start'] . " ," . $requestData['length'] . "   ";

			$query = $this -> db -> query($sql);
			// $this->db->last_query();
			$data = array();

			// preparing an array
			foreach ($query->result() as $row)
			{
				//print_r($row);
				$nestedData = array();
				$id = $row -> id;
				$company_name = ($row -> company_name == '') ? "--" : $row -> company_name;
				$onclick = 'changeStatus(' . $row -> id . ',"loading_' . $row -> id . '","span_' . $row -> id . '")';
				$action = "<a href='javascript:void(0);' onclick='$onclick'>";
				if ($row -> is_active == '0')
				{
					$action .= "<span id='span_$row->id' class='label label-warning'>Inactive</span></a>";
				}
				else
				{
					$action .= "<span id='span_$row->id' class='label label-success'>Active</span></a>";
				}
				$action .= " <br /><div id='loading_$row->id;' style='display: none;'><img src= 'base_url()/images/loader19.gif' /></div>
				";
				$nestedData[] = $row -> name . " ( " . $company_name . " )";
				$nestedData[] = $row -> dob;
				$nestedData[] = $row -> email;
				$nestedData[] = ($row -> country_code) . $row -> phone;
				//$nestedData[] = $this -> UserBookingCount($id);
				$nestedData[] = $action;
				$data[] = $nestedData;
			}

			$json_data = array(
				"draw" => intval($requestData['draw']), // for every request/draw by clientside ,
				// they send a number as a parameter, when they recieve a response/data they
				// first check the draw number, so we are sending same number in draw.
				"recordsTotal" => intval($totalData), // total number of records
				"recordsFiltered" => intval($totalFiltered), // total number of records after
				// searching, if there is no searching then totalFiltered = totalData
				"data" => $data // total data array
			);
			//print_r($json_data);
			return $json_data;
			//echo json_encode($json_data);  // send data as json format
		}

		function GetCompany()
		{
			$this -> db -> select("id,company_name");
			$this -> db -> where("isdelete", "0");
			$this -> db -> order_by("company_name");
			//$this->db->from("company");
			$query = $this -> db -> get("company");
			return $query -> result_array();

		}

		function check_coupon($coupon)
		{
			$today = date("Y-m-d");
			$query = "SELECT * FROM `coupon` where start_date <='$today' and expire_date>='$today' and code='$coupon'";
			$query = $this -> db -> query($query);
			if ($query -> num_rows() > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		function check_email($input_method)
		{
			$query = $this -> db -> get_where("user", array(
				"email" => $input_method['email_id'],
				"id!=" => $input_method['user_id']
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		function check_phone($input_method)
		{
			$query = $this -> db -> get_where("user", array(
				"country_code" => $input_method['country_code'],
				"phone" => $input_method['phone'],
				"id!=" => $input_method['user_id']
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		function AddUser($input_method)
		{
			$this -> db -> trans_begin();
			//chk the new user or existing user
			if ($input_method['user_type'] == 'N')
			{
				//delete the same email or phone number row from temp table
				$this -> db -> where("email", $input_method['email_id']);

				$contact_number = $input_method['country_code'] . $input_method['contact'];
				$this -> db -> query("delete from user_temp where email='" . $input_method['email_id'] . "' or concat(country_code,phone)=$contact_number");
				//echo $this->db->last_query();
				$email_code = random_number();
				$input_arr = array(
					"name" => $input_method['first_name'],
					"company_name" => $input_method['company_name'],
					"email" => $input_method['email_id'],
					//"password" => md5($input_method['password']),
					"phone" => $input_method['contact'],
					"country_code" => $input_method['country_code'],
					"login_type" => "W",
					"is_social" => "N",
					"is_active" => '1',
					"is_company" => "Y",
					"is_varify" => "1",
					"is_emailverify" => "0",
					"email_code" => $email_code,
					"mode" => "COMPANY",
					"company_id" => $input_method['company_id'],
					"createdon" => get_gmt_time()
				);

				$data = $this -> db -> insert("user_temp", $input_arr);
				$user_id = $this -> db -> insert_id();

				if ($user_id)
				{
					//delete the email if in temp table
					//$this -> db -> delete("user_temp", array("email" => $email));
					$email = $input_method['email_id'];
					//$email_code=
					//AccountPasswordSet($email, $user_id, $email_code);
					CompanyAddNewUser($email);

					$this -> db -> trans_commit();
					$this -> db -> trans_complete();
					return 1;

					// echo $data[0];
					if ($data[0] == 'ok')
					{
						$this -> db -> trans_commit();
						return 1;

					}
					else
					{
						$this -> db -> trans_rollback();
						return 0;

					}
					$this -> db -> trans_complete();

				}
				else
				{
					$this -> db -> trans_rollback();
					return 0;
				}

			}
			else
			{

				//update user tabel add the company name and compnay id
				$update_arr = array(
					"company_id" => $input_method['company_id'],
					"company_name" => $input_method['company_name']
				);
				$this -> db -> where("email", $input_method['email_id']);
				$this -> db -> update("user", $update_arr);
				//echo $this->db->last_query();

				//add the company and user detail in compnay table
				$insert_arr = array(
					"user_id" => $input_method['user_id'],
					"company_id" => $input_method['company_id'],
					"is_approved" => "Y",
					"created_on" => get_gmt_time(),
					"company_name" => $input_method['company_name']
				);
				$this -> db -> insert("user_company", $insert_arr);
				if ($this -> db -> insert_id() > 0)
				{
					//get the device infomation
					$this -> db -> select("device_token,device_type");
					$query = $this -> db -> get_where("user", array(
						"id" => $input_method['user_id'],
						"device_token!=" => ""
					));
					//echo $this->db->last_query();
					if ($query -> num_rows())
					{
						$tokan_data = $query -> row_array();
						if ($tokan_data['device_type'] == 'I')
						{
							$device_tokens[] = array(
								//"user_id" => $device_token['id'],
								"badge" => 0,
								"token" => $token['device_token'],
								//"username"=>$device_token['name']
							);

							// print_r($device_tokens);
							$otherdata = array("type" => "Other");
							$message_text = ADDCOMPNAY;
							send_push($device_tokens, $message_text, $otherdata);

							//	send_push_Coupon($token['device_token'], ADDCOMPNAY);
						}
						else
						{
							$registration[] = array(
								//"user_id" => $device_token['id'],
								"badge" => 0,
								"token" => $tokan_data['device_token'],
								"type" => "Other",
							);
							send_andorid_push(ADDCOMPNAY, $registration);
						}
					}
					//send and infomation mail now he have add under a compnay
					CompanyAddInExistingUser($input_method['email_id']);
					//send notification

					$this -> db -> trans_commit();
					$this -> db -> trans_complete();
					return 1;
				}
				else
				{
					$this -> db -> trans_rollback();
					$this -> db -> trans_complete();
				}
				//return 0;

			}
		}

		function GetBadge($user_id)
		{
			$query = $this -> db -> query("SELECT count(id) count FROM `user_notification` WHERE user_id=$user_id and type!='NOTIFICATION' and is_view='N'");
			//echo $this->db->last_query();
			$data = $query -> row_array();
			return $data['count'];
		}

	}
