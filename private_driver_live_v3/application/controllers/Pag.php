<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pag extends CI_Controller {
    /**
     * Index Page for this controller.
     */
    public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model('Pagmodel', 'pag');
        $this->load->library("pagination");
    }
    public function index( $offset = 0 ) {
    $offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3): 0);
    $config['total_rows'] = $this->pag->total_count();;
    $config['per_page']= 4;
    $config['first_link'] = 'First';
    $config['last_link'] = 'Last';
    $config['uri_segment'] = 3;
    $config['base_url']= base_url().'/pag/index';
    $config['suffix'] = '?'.http_build_query($_GET, '', "&");
    $this->pagination->initialize($config);
    $this->data['paginglinks'] = $this->pagination->create_links();
        // Showing total rows count
    if($this->data['paginglinks']!= '') {
      $this->data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of ';
    }  
    $this->data['result'] = $this->pag->get_users($config["per_page"], $offset); 
    $this->load->view('index1', $this->data);
   }
}