<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class PasswordSet extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url',
				'function_helper',
				'constants_helper'
			));
			$this -> load -> library('Cipher');
			$this -> load -> model('Passwordset_model');
			$this->load->library('Openfire');
			// $this->load->library('session');
			$this -> clear_cache();
		}

		function index()
		{

			$data['code'] = $_GET['code'];
			$data['identifier'] = $_GET['identifier'];

			$cipher = new Cipher('PRIVATETAXI_APP');

			$data['code'] = $encryptedcode = $cipher -> decrypt($data['code']);
			$CompanyDetail = $this -> Passwordset_model -> GetEmployee($data);
						
			//print_r($CompanyDetail);
			
			//print_r($data);

			if ($CompanyDetail)
			{
				//print_R($CompanyDetail);
				$data['CompanyDetail'] = $CompanyDetail;
				$this -> load -> view("userpassword", $data);
			}
			else
			{
				$data['notallow'] = 1;
				$this -> load -> view("userpassword", $data);
			}

		}

		function submit()
		{
			
			$passwordset= $this -> Passwordset_model -> SetPassword($_POST);
			if($passwordset==1)
			{
				echo json_encode(array("IsValid"=>"1"));
			}else
				{
					echo json_encode(array("IsValid"=>"0"));
				}

		}

		function test()
		{
			$cipher = new Cipher('PRIVATETAXI_APP');
			echo $encryptedcode = $cipher -> encrypt('priyanka@foxinfosoft.com');
		}

		function success()
		{
			//echo $this->session->userdata['logged_in'];
			if (!$this -> session -> userdata('logged_in'))
			{
				redirect(base_url());
			}
			$data['active_page'] = 'index';
			$this -> load -> view('site-nav/company_header', $data);
			$this -> load -> view('index');
			$this -> load -> view('site-nav/footer');
		}

		function validate_login()
		{

			$result = $this -> login_model -> validate_login();
			if ($result == false)
			{
				return false;
			}
			else
			{

				$session_data = array(
					"company_id" => $result['id'],
					"company_name" => $result['company_name'],
					"c_email" => $result['email'],
					"company_detail" => $result,
					"company_logged_in" => true
				);
				$this -> session -> set_userdata($session_data);
				//$this->session->set_companydata($result);
				//$GLOBALS['company_detail']=$result;
				return true;
			}

		}

		public function is_logged_in()
		{
			$user = $this -> session -> userdata('company_logged_in');
			return $user;
		}

		
		function edit_password()
		{
			$oldPass = $_POST['old_password'];
			$newPass = $_POST['new_password'];
			$confirmPass = $_POST['retype_password'];

			$userid = $this -> session -> userdata('id');
			$res = $resetPass = $this -> login_model -> resetPassword($userid, $oldPass, $newPass);
			echo $res;
		}

		function clear_cache()
		{
			$this -> output -> set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
			$this -> output -> set_header("Pragma: no-cache");
		}

	}
?>