<?php

	defined('BASEPATH') or exit('No direct script access allowed');

	// This can be removed if you use __autoload() in config.php OR use Modular
	// Extensions
	require APPPATH . '/libraries/REST_Controller.php';

	class Booking extends REST_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> library(array(
				'upload',
				'Twilio',
				'Braintree_lib'
			));
			$this -> load -> library('xmpp', false);
			$this -> load -> library('curl');

			$this -> load -> helper(array(
				'form',
				'url',
				'string',
				'constants_helper',
				'function_helper',
				'braintree_function_helper'
			));

			$this -> load -> model('Bookingapi_model');

		}

		function nearby_search()
		{
			/* check that user is already register or not */
			$input_method = $this -> webservices_inputs();

			// $this->validate_param('nearby-search', $input_method);
			$this -> param_validate($input_method, array(
				"pickup_latitude",
				"pickup_longitude",
				"user_id",
				"cab_type"
			));
			// check user is exist or not
			$user_exist = $this -> Bookingapi_model -> user_exist($input_method['user_id']);
			if ($user_exist['status'] == 0)
			{
				$this -> response(array(
					'status' => "-1",
					"jid" => "",
					"message" => $user_exist['message']
				), 200);
			}
			elseif ($user_exist['status'] == 2)
			{
				$jid = makejid($input_method['user_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => $user_exist['message']
				), 200);
			}
			$cab_detail = $this -> Bookingapi_model -> nearby_serach($input_method);
			if ($cab_detail)
			{
				if (sizeof($cab_detail) > 0)
				{
					foreach ($cab_detail as $data)
					{
						$profile_image = ($data['profile_pic'] == '') ? base_url() . DRIVER_DEFAULT_IMAGE : base_url() . $data['profile_pic'];
						$cab_data[] = array(
							"driver_id" => $data['driver_id'],
							"name" => $data['name'],
							"contactno" => $data['contact'],
							"email" => $data['email_id'],
							"profile_image" => $profile_image,
							"distance" => $data['distance'],
							"status" => $data['status'],
							"latitude" => $data['latitude'],
							"longitude" => $data['longitude'],
							"cab_type" => $data['title'],
							"car_number" => $data['cab_plate_no'],
						);
					}

					$this -> response(array(
						'status' => 1,
						"cab_data" => $cab_data
					), 200);
					// print_r($cap_detail);
				}
			}
			else
			{
				$cap_data = array();
				$jid = makejid($input_method['user_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => CAB_NOT_EXIST
				), 200);
			}

		}

		function nearby_booking_search()
		{
			//error_reporting(1);
			/* check that user is already register or not */
			$input_method = $this -> webservices_inputs();
			//get app detail from headre and set it in db log

			$header = getallheaders();
			$app_version = @$header['App-Version'];
			$os = @$header['Os-Type'];
			$input_method['os_type'] = $os;
			$input_method['app_version'] = $app_version;
			$this -> param_validate($input_method, array(
				"pickup_latitude",
				"pickup_longitude",
				"user_id",
				"cab_type",
				"destination_longitude",
				"destination_latitude",
				"pickup_location",
				"destination_location",
				"coupon_code"
			));

			//Check booking user active or exist or not
			$user_exist = $this -> Bookingapi_model -> user_exist($input_method['user_id']);
			if ($user_exist['status'] == 0)
			{
				$this -> response(array(
					'status' => "0",
					"jid" => "",
					"message" => $user_exist['message']
				), 200);
				exit ;
			}
			elseif ($user_exist['status'] == 2)
			{
				$jid = makejid($input_method['user_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => $user_exist['message']
				), 200);
				exit ;
			}
			//make auth the user card with min 500NOK
			/*	if ($user_exist['is_companyuser'] == 'N')
			 {

			 $data = array(
			 "token" => $user_exist['token'],
			 "is_companyuser" => $user_exist['is_companyuser']
			 );

			 $UserCardAuth = AuthUserCard($data);
			 if ($UserCardAuth['status'] == 1)
			 {
			 $UserCardAuthData = array(
			 "transaction_id" => $UserCardAuth['transaction_id'],
			 "status" => $UserCardAuth['transaction_status'],
			 "user_id" => $input_method['user_id'],
			 "createdon"=>get_gmt_time()
			 );
			 $SaveCardAuthID = $this -> Bookingapi_model -> SaveCardAuth($UserCardAuthData);
			 if (!$SaveCardAuthID)
			 {
			 $this -> response(array(
			 'status' => "0",
			 "jid" => $jid,
			 "message" => SERVER_ERROR
			 ), 200);
			 }

			 }
			 else
			 {
			 $jid = makejid($input_method['user_id'], USER_JID_NAME);
			 $this -> response(array(
			 'status' => "0",
			 "jid" => $jid,
			 //"message" => CARDAUTHFAIL
			 "message" => $UserCardAuth['message']
			 ), 200);
			 }
			 }*/
			//make sure customer paid last invoice
			if ($user_exist['is_companyuser'] == 'N')
			{
				$invoice = $this -> IsLastInvoicePaid($input_method['user_id']);
				if (!$invoice)
				{
					$jid = makejid($input_method['user_id'], USER_JID_NAME);
					LastInvoiceNotPaid($user_exist['email']);
					$this -> response(array(
						'status' => "0",
						"jid" => $jid,
						"message" => LASTINVOICENOTPAID
					), 200);
				}
			}
			$result = $this -> Bookingapi_model -> nearby_booking_serach($input_method);
			if (!$result)
			{

				$jid = makejid($input_method['user_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => DRIVER_BUSY_MESSAGE
				), 200);
				exit ;
			}
			$cab_detail = $result['driver_info'];
			if (count($cab_detail) == 0)
			{
				$jid = makejid($input_method['user_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => DRIVER_BUSY_MESSAGE
				), 200);
				exit ;
			}
			if (@$input_method['coupon_code'] != "")
			{
				$coupon_detail = $this -> Bookingapi_model -> get_coupon_detail($input_method['coupon_code']);
				if ($coupon_detail)
				{
					$discount = $coupon_detail[0]['offer'];
					$input_method['discount_amount'] = $discount;
				}
				else
				{
					$discount = "0";
					$input_method['discount_amount'] = "0";
				}
			}
			else
			{
				$input_method['discount_amount'] = "0";

			}
			// check the pickup nad distination location its fixed rate booking or not
			/*  Driver discide the booking is fix rate or not
			 * */
			$IsFixRating = $this -> IsFixRateBooking($input_method);
			if ($IsFixRating)
			{
				$input_method['is_fixrate'] = $IsFixRating['isairportbooking'];
				$input_method['airport_id'] = $IsFixRating['airport'];
			}
			//$input_method['is_fixrate'] = "N";
			$input_method['os_type'] = $os;
			$input_method['app_version'] = $app_version;

			$booking = $this -> Bookingapi_model -> cab_booking($input_method);
			// check user is exist or not
			if (!$booking)
			{
				$jid = makejid($input_method['user_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => SERVER_ERROR
				), 200);
				exit ;
			}
			$booking_detail = $this -> Bookingapi_model -> get_booking_detail($booking);

			$booking_detail = $booking_detail[0];
			//linked the card Auth id to booking id
			/*if ($user_exist['is_companyuser'] == 'N')
			 $LinkCardAuthBooking = $this -> Bookingapi_model ->
			 UpdateCardAuth($SaveCardAuthID, $booking_detail['booking_id']);
			 */
			$user_id = $booking_detail['user_id'];
			$destination_latitude = $booking_detail['destination_latitude'];
			$destination_longitude = $booking_detail['destination_longitude'];
			$lat = $booking_detail['pickup_latitude'];
			$lng = $booking_detail['pickup_longitude'];
			$user_id = $booking_detail['user_id'];
			$user_name = $booking_detail['name'];
			$phone = $booking_detail['phone'];
			$booking_id = $booking_detail['booking_id'];
			$CBN = $booking_detail['booking_number'];
			$comment = ($booking_detail['comment'] == '') ? "" : $booking_detail['comment'];

			$destination_loaction = $booking_detail['destination_location'];

			// $jid = "user-" . $user_id . '@privatedriverapp.no';
			$jid = makejid($user_id, USER_JID_NAME);

			$pickup_location = $booking_detail['pickup_location'];

			$message = array(
				"user_id" => "$user_id",
				"username" => $user_name,
				"contactno" => $phone,
				"booking_id" => "$booking_id",
				"CBN" => $CBN,
				"pick_up_latitude" => $lat,
				"pick_up_longitude" => $lng,
				"pick_up_location" => $pickup_location,
				"destination_latitude" => $destination_latitude,
				"destination_longitude" => $destination_longitude,
				"destination_loaction" => $destination_loaction,
				"comment" => $comment,
				//"extra_order" => $other_item_data,
				"jid" => $jid,
			);
			//sort the driver according to booking detail

			//	usort($cap_detail, array($this, 'distance'));
			//print_r($cab_detail);
			if (sizeof($cab_detail) > 0)
			{
				foreach ($cab_detail as $data)
				{
					$driver_id = $data['driver_id'];
					//GET DRIVER DISTANCE
					$origins = $data['latitude'] . ',' . $data['longitude'];
					$current_booking = $this -> Bookingapi_model -> getdriver_currentbooking($driver_id);
					//print_r($current_booking);
					$duration = $distance = "0";
					if ($current_booking)
					{

						for ($i = 0; $i < count($current_booking); $i++)
						{
							$pickup_origin = $current_booking[$i]['pickup_latitude'] . ',' . $current_booking[$i]['pickup_longitude'];
							$pickup_location = $current_booking[$i]['pickup_location'];
							$destination_origin = $current_booking[$i]['destination_latitude'] . ',' . $current_booking[$i]['destination_longitude'];
							$destination_location = $current_booking[$i]['destination_location'];
							$book_id = $current_booking[$i]['id'];
							if ($booking_status == 'TRIP_STARTED')
							{
								$time_detail = gettimedetail($origins, $destination_origin, 'now', "", "", $booking_id);
								$distance = $distance + $time_detail['distance_m'];
								$duration = $duration + $time_detail['duration_sec'];
							}
							else
							{
								$time_detail = gettimedetail($origins, $pickup_origin, 'now', "", "", $booking_id);
								$distance = $distance + $time_detail['distance_m'];
								$duration = $duration + $time_detail['duration_sec'];

								//current pickup to current booking (driver running booking) destination
								$time_detail = gettimedetail($pickup_origin, $destination_origin, 'now', "", "", $booking_id);
								// time should be distance + now
								$distance = $distance + $time_detail['distance_m'];
								$duration = $duration + $time_detail['duration_sec'];
								//driver new booking pickup location

							}
							$origins = $destination_origin;
						}
					}
					$newBookingPickupLocation = $lat . ',' . $lng;
					$time_detail = gettimedetail($origins, $newBookingPickupLocation, 'now', "", "", $booking_id);
					$distance = $distance + $time_detail['distance_m'];
					$duration = $duration + $time_detail['duration_sec'];
					if ($duration)
					{
						$duration = format_time($duration);
						$distance = $distance / 1000;
					}
					else
					{
						$time_detail = gettimedetail($origins, $newBookingPickupLocation);
						if ($time_detail)
						{
							$duration = $time_detail['duration'];
							$distance = $time_detail['distance'];
							$duration = format_time($duration);
							$distance = $distance / 1000;
						}
						else
						{
							$duration = "";
							$distance = "";
						}
					}
					$data['distance_google'] = $distance;
					$data['duration_google'] = $duration;

					//DISTANCE CODE END HERE
					$profile_image = ($data['profile_pic'] == '') ? base_url() . DRIVER_DEFAULT_IMAGE : base_url() . $data['profile_pic'];
					// $driver_jid = 'driver-' . $data['driver_id'] . '@privatedriverapp.no';
					$driver_jid = makejid($data['driver_id'], DRIVER_JID_NAME);
					$driver_info[] = array(
						"driver_id" => $data['driver_id'],
						"name" => $data['name'],
						"contactno" => $data['contact'],
						"email" => $data['email_id'],
						"profile_image" => $profile_image,
						"distance" => $data['distance'],
						"status" => $data['status'],
						"latitude" => $data['latitude'],
						"longitude" => $data['longitude'],
						"jid" => $driver_jid,
						"cab_type" => $data['title'],
						"distance_linear" => $data['distance'],
						"distance" => $data['distance_google'],
						"duration_google" => $data['duration_google'],
						"car_number" => $data['cab_plate_no']
					);

				}
				usort($driver_info, distance);
				$this -> Bookingapi_model -> SaveBookingDriver($booking_id, $driver_info);
				$this -> response(array(
					'status' => "1",
					'driver_waiting_time' => DRIVERWAITINGTIME,
					"message" => "success",
					"driver_info" => $driver_info,
					"booking_detail" => $message
				), 200);
				// print_r($cap_detail);
			}
			else
			{
				// $cab_data = array();
				$jid = makejid($input_method['user_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => DRIVER_BUSY_MESSAGE
				), 200);
			}

		}

		function driver_available()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('driver_info', $input_method);
			$this -> param_validate($input_method, array("driver_id"));

			$result = $this -> Bookingapi_model -> driver_available($input_method);
			if ($result)
			{
				$this -> response(array(
					'status' => "1",
					"driver" => "available"
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"driver" => "busy"
				), 200);
			}
		}

		function booking_status()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('booking-status', $input_method);

			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id"
			));
			$result = $this -> Bookingapi_model -> booking_status($input_method);
			//print_r($result);
			//exit;
			if ($result)
			{
				$this -> response(array(
					'status' => "1",
					"booking_status" => $result
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"booking_status" => $result,
					"message" => BOOKING_CANCEL
				), 200);
			}
		}

		function driver_response()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('driver-response', $input_method);
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id",
				"type"
			));
			$header = getallheaders();
			$app_version = @$header['App-Version'];
			$os_type = @$header['Os-Type'];
			$driver_status_data = $this -> Bookingapi_model -> getdriverstatus($input_method['driver_id']);
			//$driver_status = $driver_status_data[0]['status'];
			$driver_status = $driver_status_data;
			$result = $this -> Bookingapi_model -> driver_response($input_method);
			$IsAutoAccept = "False";
			if ($result)
			{
				if ($result['status'] == "1" && $result['type'] == 'accept')
				{
					$driver_detail = $result['driver_detail'][0];
					$first_name = $driver_detail['first_name'];
					$driver_image = $driver_detail['profile_pic'];
					$last_name = $driver_detail['last_name'];
					$contact = $driver_detail['contact'];
					$profile_image = $driver_detail['profile_pic'];
					$current_lat = $driver_detail['latitude'];
					$current_lng = $driver_detail['longitude'];
					$current_location = getaddress($current_lat, $current_lng);
					// $jid = $driver_detail['driver_id'] . '@privatedriverapp.no';
					$driver_id = $driver_detail['driver_id'];
					$user_id = $driver_detail['user_id'];
					$pickup_latitude = $driver_detail['pickup_latitude'];
					$pickup_longitude = $driver_detail['pickup_longitude'];
					$destination_latitude = $driver_detail['destination_latitude'];
					$destination_latitude = $driver_detail['destination_longitude'];
					$car_number = $driver_detail['cab_plate_no'];
					$cab_type = $driver_detail['title'];
					//$jid = "user-" . $user_id . "@privatedriverapp.no";
					$jid = makejid($user_id, USER_JID_NAME);
					//$booking_lat=$driver_detail[''];
					$CBN = $driver_detail['booking_number'];
					$customer_phone = $driver_detail['country_code'] . $driver_detail['phone'];
					//driver current lat lng
					$origins = $current_lat . ',' . $current_lng;
					// driver location
					$destination = $pickup_latitude . ',' . $pickup_longitude;
					/// suresh sir location
					//$driver_status = $driver_detail['status'];
					$driver_status = $driver_status;
					$customer_os_type = $driver_detail['os_type'];
					$customer_app_version = $driver_detail['app_version'];
					// can be commented
					//$new_booking_origin=

					if ($driver_status == 'BUSY')
					{
						$duration = $distance = "0";
						//get driver current booking
						$current_booking = $this -> Bookingapi_model -> getdriver_currentbooking($driver_id, $input_method['booking_id']);
						if ($current_booking)
						{
							for ($i = 0; $i < count($current_booking); $i++)
							{
								$pickup_origin = $current_booking[$i]['pickup_latitude'] . ',' . $current_booking[$i]['pickup_longitude'];
								$pickup_location = $current_booking[$i]['pickup_location'];
								$destination_origin = $current_booking[$i]['destination_latitude'] . ',' . $current_booking[$i]['destination_longitude'];
								$destination_location = $current_booking[$i]['destination_location'];
								$booking_id = $current_booking[$i]['id'];
								$booking_status = $current_booking[$i]['status'];
								if ($booking_status == 'TRIP_STARTED')
								{
									$time_detail = gettimedetail($origins, $destination_origin, 'now', $current_location, $destination_location, $booking_id);
									$distance = $distance + $time_detail['distance_m'];
									$duration = $duration + $time_detail['duration_sec'];
								}
								else
								{
									$time_detail = gettimedetail($origins, $pickup_origin, 'now', $current_location, $pickup_location, $booking_id);
									$distance = $distance + $time_detail['distance_m'];
									$duration = $duration + $time_detail['duration_sec'];

									//current pickup to current booking (driver running booking) destination
									$time_detail = gettimedetail($pickup_origin, $destination_origin, 'now', $pickup_location, $destination_location, $booking_id);
									// time should be distance + now
									$distance = $distance + $time_detail['distance_m'];
									$duration = $duration + $time_detail['duration_sec'];
									//driver new booking pickup location

								}
								$origins = $destination_origin;
							}
						}
						$time_detail = gettimedetail($destination_origin, $destination, 'now', $pickup_location, $destination_location, $booking_id);
						$distance = $distance + $time_detail['distance_m'];
						$duration = $duration + $time_detail['duration_sec'];
						if ($duration)
						{
							$duration = format_time($duration);
							$distance = $distance / 1000;
						}
						else
						{
							$time_detail = gettimedetail($origins, $destination);
							if ($time_detail)
							{
								$duration = $time_detail['duration'];
								$distance = $time_detail['distance'];
								$duration = format_time($duration);
								$distance = $distance / 1000;
							}
							else
							{
								$duration = "";
								$distance = "";
							}
						}

					}
					else
					{
						//$timestamp=current_time();
						$time_detail = gettimedetail($origins, $destination);
						if ($time_detail)
						{
							$duration = $time_detail['duration'];
							$distance = $time_detail['distance'];
						}
						else
						{
							$duration = "";
							$distance = "";
						}
					}
					$duration = ($duration == "") ? "1 Min" : $duration;
					$booking_id = $driver_detail['id'];
					$profile_image = ($driver_detail -> profile_pic == '') ? base_url() . DRIVER_DEFAULT_IMAGE : base_url() . DRIVER_IMAGE_PATH . $driver_detail -> profile_pic;

					//If Duration is <=20 auto accept by customer
					if (($customer_os_type == 'IOS' && $customer_app_version > '1.3') || $customer_app_version > '1.6' && $customer_os_type == 'ANDROID')
					{

						//if ($os_type = 'ANDROID' && $app_version > '1.4')
						{

							$duration_min = $duration;
							$IsAutoAccept = $this -> IsAutoCustomerAccept($duration);
							if ($IsAutoAccept)
							{
								//Call the customer Accept api and call a chat server web service
								$BookingData = array(
									"user_id" => $user_id,
									"booking_id" => $booking_id,
									"driver_id" => $driver_id
								);
								$this -> SendPacketToDriverCutomer($BookingData);
							}
						}
					}
					//$duration_min=str_replace(array("Min","Sec"), "", $duration);

					$this -> response(array(
						'status' => "1",
						"driver_id" => "$driver_id",
						"jid" => $jid,
						"name" => $first_name . ' ' . $last_name,
						"contact" => $contact,
						"driver_image" => base_url() . DRIVER_IMAGE_PATH . $driver_image,
						"image" => $profile_image,
						"current_latitude" => $current_lat,
						"current_longitude" => $current_lng,
						"current_location" => $current_location,
						"distance" => ucwords($distance),
						"duration" => ucwords($duration),
						"CBN" => $CBN,
						"booking_id" => $booking_id,
						"cab_type" => $cab_type,
						"driver_status" => $driver_status,
						"lo1" => $origins,
						"lo2" => $destination,
						"car_number" => $car_number,
						"auto_accept" => $IsAutoAccept,
					), 200);
				}
				elseif ($result['status'] == "1" && $result['type'] == 'reject')
				{
					$this -> response(array(
						'status' => "1",
						"message" => $result['message'],
					), 200);
				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => $result['message'],
					), 200);
				}

			}
			else
			{
				
				$this -> response(array(
					'status' => "0",
					"message" => $result['message']
				), 200);
			}
		}

		/*****************************************************************
		 ************CUSTOMER RESPONSE**************************************
		 /*****************************************************************/
		function customer_response()
		{
			error_reporting(1);
			$input_method = $this -> webservices_inputs();
			$this -> validate_param('customer-response', $input_method);
			// $this->param_validate($input_method, array("driver_id"));
			$this -> param_validate($input_method, array(
				"booking_id",
				"user_id",
				"type"
			));

			if ($input_method['type'] == 'accept')
			{
				//echo "test";
				$this -> param_validate($input_method, array("driver_id"));
			}
			$result = $this -> Bookingapi_model -> customer_response($input_method);

			if ($result)
			{
				if ($result['status'] == '1' && $result['type'] == 'accept')
				{
					$driver_status_data = $this -> Bookingapi_model -> getdriverstatus($input_method['driver_id']);
					$driver_status = $driver_status_data;
					$customer_detail = $result['customer_detail'][0];
					$customer_name = $customer_detail['name'];
					$driver_name = $customer_detail['first_name'] . ' ' . $customer_detail['last_name'];
					$email = $customer_detail['email'];
					$contact = $customer_detail['phone'];
					$driver_contact = $customer_detail['contact'];
					$CBN = $customer_detail['booking_number'];
					$pickup_latitude = $customer_detail['pickup_latitude'];
					$pickup_longitude = $customer_detail['pickup_longitude'];
					$cur_pickup_location = $customer_detail['pickup_location'];
					$destination_latitude = $customer_detail['destination_latitude'];
					$destination_longitude = $customer_detail['destination_longitude'];
					$cur_destination_location = $customer_detail['destination_location'];
					$driver_latitude = $customer_detail['latitude'];
					$driver_longitude = $customer_detail['longitude'];
					$driver_loaction = getaddress($driver_latitude, $driver_longitude);
					$origins = $driver_latitude . ',' . $driver_longitude;
					$destination = $pickup_latitude . ',' . $pickup_longitude;
					$time_detail = gettimedetail($origins, $destination);
					$customer_id = $customer_detail['id'];
					$driver_id = $customer_detail['driver_id'];
					$cur_booking_id = $customer_detail['booking_id'];
					$customer_phone = $customer_detail['country_code'] . $customer_detail['phone'];
					//$jid = 'driver-' . $driver_id . '@privatedriverapp.no';
					$jid = makejid($driver_id, DRIVER_JID_NAME);
					$customer_jid = makejid($user_id, USER_JID_NAME);
					/*if ($time_detail)
					 {
					 $duration = $time_detail['duration'];
					 $distance = $time_detail['distance'];
					 }
					 else
					 {
					 $duration = "";
					 $distance = "";
					 }*/
					$destination_origin = $origins;
					$distance = $duration = 0;
					$current_booking = $this -> Bookingapi_model -> getdriver_currentbooking($input_method['driver_id'], $input_method['booking_id']);
					if ($current_booking)
					{
						for ($i = 0; $i < count($current_booking); $i++)
						{
							$pickup_origin = $current_booking[$i]['pickup_latitude'] . ',' . $current_booking[$i]['pickup_longitude'];
							$pickup_location = $current_booking[$i]['pickup_location'];
							$destination_origin = $current_booking[$i]['destination_latitude'] . ',' . $current_booking[$i]['destination_longitude'];
							$destination_location = $current_booking[$i]['destination_location'];
							$booking_id = $current_booking[$i]['id'];
							$booking_status = $current_booking[$i]['status'];
							if ($booking_status == 'TRIP_STARTED')
							{
								$time_detail = gettimedetail($origins, $destination_origin, 'now', $current_location, $destination_location, $booking_id);
								$distance = $distance + $time_detail['distance_m'];
								$duration = $duration + $time_detail['duration_sec'];
							}
							else
							{
								$time_detail = gettimedetail($origins, $pickup_origin, 'now', $current_location, $pickup_location, $booking_id);
								$distance = $distance + $time_detail['distance_m'];
								$duration = $duration + $time_detail['duration_sec'];

								//current pickup to current booking (driver running booking) destination
								$time_detail = gettimedetail($pickup_origin, $destination_origin, 'now', $pickup_location, $destination_location, $booking_id);
								// time should be distance + now
								$distance = $distance + $time_detail['distance_m'];
								$duration = $duration + $time_detail['duration_sec'];
								//driver new booking pickup location

							}
							$origins = $destination_origin;
						}
					}
					$time_detail = gettimedetail($destination_origin, $destination, 'now', $pickup_location, $destination_location, $booking_id);
					//print_r($time_detail);
					$distance = $distance + $time_detail['distance_m'];
					$duration = $duration + $time_detail['duration_sec'];
					if ($duration)
					{
						$duration = format_time($duration);
						if ($duration == "")
						{
							$duration = "1 Min";
						}
						$distance = $distance / 1000;
					}
					else
					{
						$time_detail = gettimedetail($origins, $destination);
						//print_r($time_detail);
						if ($time_detail)
						{
							$duration = $time_detail['duration'];
							$distance = $time_detail['distance'];
							$duration = format_time($duration);
							if ($duration == "")
							{
								$duration = "1 Min";
							}
							$distance = $distance / 1000;
						}
						else
						{
							$duration = "";
							$distance = "";
						}
					}

					$comment = ($customer_detail['comment'] == "") ? "" : $customer_detail['comment'];

					$message = CUSTOMERACCEPT . TELEPHONENO . $customer_phone . " " . BOOKINGNUMBER . $CBN . " " . ADDRESS . " " . $pickup_location;
					//send_sms($driver_contact, $message);

					$get_message = BOOKINGSMS;
					$msg_replace = array(
						"<CBN>",
						"<driver_name>",
						"<driver_contact>"
					);
					$detail = array(
						$CBN,
						$driver_name,
						$driver_contact
					);
					$booking_sms = str_replace($msg_replace, $detail, $get_message);
					send_sms($customer_phone, $booking_sms);

					//booking_confirm($booking_id);
					$this -> response(array(
						'status' => "1",
						"customer_id" => $customer_id,
						"jid" => $jid, //driver jid
						"customer_name" => $customer_name,
						"email" => $email,
						"pickup_latitude" => $pickup_latitude,
						"pickup_longitude" => $pickup_longitude,
						"pickup_location" => $cur_pickup_location,
						"driver_latitude" => $driver_latitude,
						"driver_longitude" => $driver_longitude,
						"driver_location" => $driver_loaction,
						"duration" => $duration,
						"distance" => $distance,
						// "extra_order" => $other_item_data,
						"comment" => $comment,
						"booking_id" => "$cur_booking_id",
						"customer_contact" => $contact,
						"destination_latitude" => $destination_latitude,
						"destination_longitude" => $destination_longitude,
						"destination_location" => $cur_destination_location,
						"customer_jid" => $customer_jid
					), 200);

				}
				elseif ($result['status'] == 1 && $result['type'] == 'reject')
				{
					//get booking autho transaction id
					$type = "Customer reject";
					//$this -> VOIDAUTHTRANSACTION($input_method['booking_id'],$type);
					//VOIDAUTHTRANSACTION($input_method['booking_id'], $type);
					if ($result['is_prebooking'] == 'Y')
					{
						//send mail to admin for pre-booking
						normal_prebooking_mail_to_admin($input_method['booking_id'], 'cancelled');
					}

					if ($result['action'] == "CUSTOMER_REJECTED")
					{
						//chk accept time
						$current_time = get_gmt_time();

						if ($result['accept_time'] && $result['is_companyuser'] == 'N')
						{

							$timedifference = round(abs(strtotime($current_time) - strtotime($result['accept_time'])) / 60, 2);
							if ($timedifference > 3)
							{
								//make payment methos nonce from token
								$braintree_user_token = $this -> Bookingapi_model -> user_braintree_token($input_method['user_id']);
								$paymentMethodNonce = paymentMethodNonce($braintree_user_token);

								if ($paymentMethodNonce['status'] == 0)
								{
									//update the Admin So he can check the issue
									$type = "-Customer Reject Booking ";
									PayMentFailMail($input_method['booking_id'], $paymentMethodNonce['message'], $type);

								}
								//make a nonce transaction and immediately submit it for settlement:
								$payment_data = array(
									"nonce" => $paymentMethodNonce['nonce'],
									"amount" => PENALTY_CHARGE
								);
								$customer_penalty_payment_detail = customer_penalty_charge($payment_data);

								if ($customer_penalty_payment_detail['status'] == 0)
								{
									$type = "-Customer Reject Booking";
									PayMentFailMail($input_method['booking_id'], $paymentMethodNonce['message'], $type);
								}
								$transction_id = $customer_penalty_payment_detail['transaction_id'];
								/*//Cut the 100 NOK from customer accounnt;
								 //get user braintree token
								 $braintree_user_token = $this -> Bookingapi_model ->
								 user_braintree_token($input_method['user_id']);
								 $braintree_result = Braintree_Transaction::sale([
								 'paymentMethodToken' => $braintree_user_token,
								 'amount' => PENALTY_CHARGE]);

								 if ($braintree_result -> success == '1')
								 {
								 $transction_id = $braintree_result -> transaction -> id;
								 $desc = "success";

								 }
								 else
								 {
								 $desc = $braintree_result -> message;
								 $transction_id = "";

								 }*/
								$data = array(
									"is_paid" => "1",
									"transaction_id" => $transction_id,
									"user_paid" => PENALTY_CHARGE,
									"basic_fare_charge" => PENALTY_CHARGE,
									"payment_type" => "PENALTY",
									"booking_id" => $input_method['booking_id'],
									"paymenton" => get_gmt_time(),
									"description" => $customer_penalty_payment_detail['message'],
									"createdon" => get_gmt_time(),
									"currency" => $customer_penalty_payment_detail['currency'],
									"status" => $customer_penalty_payment_detail['payment_status'],
									"nonce" => $customer_penalty_payment_detail['nonce']
								);
								$penalty_data = $this -> Bookingapi_model -> save_payment($data);
								if (!$penalty_data)
								{
									//send a mail to client or admin
								}
							}
						}
						elseif ($result['accept_time'] && $result['is_companyuser'] == 'Y')
						{
							$timedifference = round(abs(strtotime($current_time) - strtotime($result['accept_time'])) / 60, 2);
							if ($timedifference > 3)
							{
								//Send mail to admin
								SendMailToAdminNotShow($input_method['booking_id'], 'PENALTY');
							}
						}
					}

					$jid = makejid(@$result['driver_id'], DRIVER_JID_NAME);
					if ($result['driver_id'] > 0)
					{

						$message = "Kunden kansellert bestillingen:- " . $result['CBN'];
						//	send_sms($result['contact'], $message);
						$this -> response(array(
							'status' => "1",
							"notify_driver" => "1",
							"jid" => $jid,
							"CBN" => $result['CBN'],
							"bookingid" => $result['bookingid'],
							"message" => $result['message'],
							"message_customer" => $result['message_customer']
						), 200);
					}
					elseif ($result['status'] == 0 && $result['type'] == 'reject')
					{

						if ($result['is_prebooking'] == 'Y')
						{

							//send mail to admin for pre-booking
							normal_prebooking_mail_to_admin($input_method['booking_id'], 'cancelled');
						}

						$this -> response(array(
							'status' => "0",
							"notify_driver" => "0",
							//"jid" => $jid,
							"CBN" => ($result['CBN'] == "") ? "" : $result['CBN'],
							"bookingid" => $result['bookingid'],
							"message" => $result['message'],
							"message_customer" => $result['message_customer']
						), 200);

					}
					else
					{
						$this -> response(array(
							'status' => "1",
							"notify_driver" => "0",
							//"jid" => $jid,
							"CBN" => ($result['CBN'] == "") ? "" : $result['CBN'],
							"bookingid" => $result['bookingid'],
							"message" => $result['message'],
							"message_customer" => $result['message_customer']
						), 200);
					}

				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => $result['message']
					), 200);
				}
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => $result['message']
				), 200);
			}
		}

		/******************************************************************
		 **************CUSTOMER REJECT END**********************************
		 /******************************************************************/

		function timeout()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('timeout', $input_method);//booking_id
			$this -> param_validate($input_method, array("booking_id"));
			$result = $this -> Bookingapi_model -> timeout($input_method);
			$type = "No Any Driver Response";
			//$this -> VOIDAUTHTRANSACTION($input_method['booking_id'],$type);
			//VOIDAUTHTRANSACTION($input_method['booking_id'], $type);
			$this -> response(array(
				'status' => "1",
				"message" => DRIVER_BUSY_MESSAGE
			), 200);

			// $this->validate_param('driver-response',$input_method);
			// $result=$this->Bookingapi_model->driver_response($input_method);
		}

		function trip_start()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('trip-start', $input_method);
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id"
			));
			$result = $this -> Bookingapi_model -> trip_start($input_method);
			if ($result)
			{
				$jid = makejid($result, USER_JID_NAME);
				$this -> response(array(
					'status' => "1",
					"customer_jid" => $jid,
					"message" => "success"
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => CUSTOMER_NOT_APPROVE
				), 200);
			}
		}

		function trip_type()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('trip-end', $input_method);
			$this -> param_validate($input_method, array(
				"booking_id",
				"type",
			));
			if ($input_method['type'] == 'FIXED')
			{
				if ($input_method[price] == '')
				{
					$this -> response(array(
						'status' => "0",
						"message" => BILLING_AMOUNT_MISSING
					), 200);
				}
			}
			$update = $this -> Bookingapi_model -> trip_type($input_method);
			if ($update)
			{
				$this -> response(array(
					'status' => "1",
					"message" => SUCCESS
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => SERVER_ERROR
				), 200);

			}

		}

		/**********************************************************************
		 *****************************TRIP END FUNCTION START********************
		 /**********************************************************************/

		function trip_end()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('trip-end', $input_method);
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id",
				"km_driven",
				"journey_time"
			));
			//
			$this -> load -> model('Payment_model');
			$result = $this -> Bookingapi_model -> trip_end($input_method);
			$booking_id = $input_method['booking_id'];
			if ($result['status'] == 1)
			{

				$trip_detail = $result['trip_detail'];
				$trip_detail = $trip_detail[0];
				$cab_type = $trip_detail['cab_type'];
				$userid = $trip_detail['user_id'];
				$pickup_latitude = $trip_detail['pickup_latitude'];
				$pickup_longitude = $trip_detail['pickup_longitude'];
				$pickup_location = $trip_detail['pickup_location'];
				$destination_latitude = $trip_detail['destination_latitude'];
				$destination_longitude = $trip_detail['destination_longitude'];
				$driver_id = $trip_detail['driver_id'];
				$driver_name = $trip_detail['first_name'] . ' ' . $trip_detail['last_name'];
				$car_number = $trip_detail['plate_number'];
				$driver_profile_pic = $trip_detail['profile_pic'];
				$driver_contact = $trip_detail['contact'];
				$CBN = $trip_detail['booking_number'];
				$customer_phone = $trip_detail['country_code'] . $trip_detail['phone'];
				$destination_location = $trip_detail['destination_location'];
				$waiting_time_charge = "0.00";
				$billing_charge = "0.00";
				$discount_amount = $trip_detail['discount_amount'];
				$is_prebooking = $trip_detail['is_prebooking'];
				extract($input_method);
				$km_driven = number_format($km_driven, 2);
				$jid = makejid($userid, USER_JID_NAME);
				$fix_amount="0.00";
				//$other_item_data = array();
				if ($trip_detail['is_fixrate'] == "N")
				{
					//***************get fare detail**********************
					$fare_detail = $this -> Bookingapi_model -> getfare_detail($cab_type);
					// print_r($fare_detail);

					if ($fare_detail)
					{
						$other_charge = 0;
						$fare_detail = $fare_detail[0];
						$min_km = $fare_detail['min_km'];
						$min_charge = $fare_detail['min_charges'];
						$waiting_charge = $fare_detail['wait_time_charges'];
						//changes into journey time
						$charge_per_km = $fare_detail['charges_per_km'];
						// Cab drive less than 1 charge apply 1 km
						if ($km_driven >= 1)
						{
							$trip_charge = $km_driven * $charge_per_km;
						}
						else
						{
							$trip_charge = $charge_per_km;
						}
						//journey time less than 1 min apply charge 1 min

						if (($journey_time / 60) > 1)
						{
							//journey time in sec so convert it into min and calculate charge
							$journey_time_charge = ($journey_time / 60) * ($waiting_charge);
						}
						else
						{
							$journey_time_charge = $waiting_charge;
						}
						$journey_time_charge = number_format($journey_time_charge, 2);
						//check if this is pre-booking add extra pre-booking charge
						//PREBOOKING_CHARGEif()
						if ($is_prebooking == 'Y')
						{
							//get prebooking charge
							$prebooking_charge = $this -> Bookingapi_model -> GetPreBookingCharge();
							$min_charge = (int)$min_charge + (int)$prebooking_charge;
						}

					}
					else
					{

						$this -> response(array(
							'status' => "0",
							"message" => FARE_INFO_MISS
						), 200);
					}
				}
				else
				{
					//check the airport id
					if ($trip_detail['airport_id'] == 1)
					{
						$airport_first_firstclass = FIXRATE_FIRST;
						$airport_first_luxury = FIXRATE_LUXURY;
						$IsAirportBooking="Y";
					}
					elseif ($trip_detail['airport_id'] == 2)
					{
						$airport_first_firstclass = FIXRATE_FIRST_SECONDAIRPORT;
						$airport_first_luxury = FIXRATE_LUXURY_SECONDAIRPORT;
						$IsAirportBooking="Y";

					}
					else
					{
						$airport_first_firstclass = FIXRATE_FIRST_THIRDAIRPORT;
						$airport_first_luxury = FIXRATE_LUXURY_THIRDAIRPORT;
						$IsAirportBooking="Y";
					}
					if ($cab_type == 1)
					{
						$fix_amount=$min_charge = str_replace(',', '.', $airport_first_firstclass);
						$trip_charge = $journey_time_charge = $discount_amount = $waiting_charge = $charge_per_km = 0.00;
					}
					else
					{
						$fix_amount=$min_charge = str_replace(',', '.', $airport_first_luxury);
						$trip_charge = $journey_time_charge = $discount_amount = $waiting_charge = $charge_per_km = 0.00;
					}
					
				}
				$booking_type = $this -> Bookingapi_model -> GetBookingType($booking_id);
				if ($booking_type['is_fixrate'] == 'Y')
				{
					$is_fixrate = $booking_type['is_fixrate'];
					$min_charge=$fix_amount = $booking_type['fix_amount'];
				}
				//$min_charge = str_replace(',', '.', $trip_detail['fix_amount']);
				//$trip_charge = $journey_time_charge = $discount_amount = $waiting_charge = $charge_per_km = 0.00;

				$vat_charge = "";
				$billing_charge = $min_charge + $trip_charge + $journey_time_charge;
				$billing_charge = $billing_charge - $discount_amount;

				$data = array(
					"km_driven" => number_format($km_driven, 2, '.', ''),
					"km_charge" => $trip_charge,
					"basic_fare_charge" => $min_charge,
					"fix_amount"=>$fix_amount,
					"journey_time" => (int)$journey_time,
					"charges_per_km" => $charge_per_km,
					"waiting_charges_per_min" => $waiting_charge,
					"waiting_charge" => $journey_time_charge,
					"billing_amount" => number_format($billing_charge, 2, '.', ''),
					"booking_id" => "$booking_id",
					"other_charge" => "0",
					"discount_amount" => $discount_amount,
					"tip" => "0.00",
					"createdon" => get_gmt_time()
				);

				$payment = $this -> Bookingapi_model -> save_booking_payment($data);
				if ($payment)
				{
					//update driver status
					$driver_available = DRIVER_AVAILABLE;
					$data = array(
						"driver_id" => $driver_id,
						"status" => "$driver_available"
					);
					$this -> Bookingapi_model -> update_driver_status($data);

				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => SERVER_ERROR
					), 200);
				}
				// chk is company user
				$is_companyuser = $this -> Bookingapi_model -> iscompany_user($booking_id);
				if ($is_companyuser == Y)
				{
					recepet_send_pdf($input_method['booking_id']);
					$invoice = recepet_send_pdf_company($input_method['booking_id']);
					$data = array(
						"filename" => $invoice,
						"booking_id" => $input_method['booking_id']
					);
					$this -> Payment_model -> save_invoicepdf_name($data);
				}
				//get driver current status
				$driver_status_data = $this -> Bookingapi_model -> getdriverstatus($input_method['driver_id']);
				if ($driver_status_data == "BUSY")
				{
					$is_busy = "Y";
				}
				else
				{
					$is_busy = "N";
				}
				//get the booking type its fixed or normal rate booking
				$booking_type = $this -> Bookingapi_model -> GetBookingType($booking_id);
				//print_r($booking_type);
				if ($booking_type['is_fixrate'] == 'Y')
				{
					$is_fixrate = $booking_type['is_fixrate'];
					$billing_charge = $fix_amount = $booking_type['fix_amount'];
				}
				$this -> response(array(
					'status' => "1",
					"booking_id" => "$booking_id",
					"CBN" => $CBN,
					"basic_fare_charge" => $min_charge,
					"journey_time" => (int)$journey_time,
					"journey_time_charge" => $journey_time_charge,
					"km_driven" => number_format($km_driven, 2, '.', ''),
					"per_km_charge" => $charge_per_km,
					"km_driven_charge" => $trip_charge,
					"discount" => number_format($discount_amount, 2, '.', ''),
					"billing_charge" => number_format($billing_charge, 2, '.', ''),
					"customer_jid" => $jid,
					"pickup_location" => $pickup_location,
					"car_number" => $car_number,
					"driver_image" => base_url() . DRIVER_IMAGE_PATH . $driver_profile_pic,
					"destination_location" => $destination_location,
					"driver_name" => $driver_name,
					"driver_id" => "$driver_id",
					"is_busy" => $is_busy,
					"driver_contact" => $driver_contact,
					"is_fixratebooking" => $is_fixrate,
					"fix_amount" => $fix_amount,
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => $result['message']
				), 200);
			}
		}

		/************************TRIP END FUNCTION END***************************

		 /* function for advance cab booking */

		function ride_later()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('ride-later', $input_method);
			$this -> param_validate($input_method, array(
				"pickuplat",
				"pickuplong",
				"customerid",
				"destinationlat",
				"destinationlong",
				"pickup_time",
				"cab_type",
				"pickup_location",
				"destination_location"
			));
			$jid = makejid($input_method['customerid'], USER_JID_NAME);
			//check the cab is free or not
			if ($input_method['cab_type'] == 3)
			{
				//get luxury today pre-booking detail
				$luxury_pre_booking = $this -> Bookingapi_model -> get_luxury_pre_booking();
				if ($luxury_pre_booking)
				{
					foreach ($luxury_pre_booking as $pre_booking)
					{

						$less_time = strtotime($pre_booking['booking_time']) - (20 * 60);
						$max_time = strtotime($pre_booking['booking_time']) + (20 * 60);

						if (strtotime($input_method['pickup_time']) < $max_time)
						{
							if (strtotime($input_method['pickup_time']) > $less_time)
							{
								if (strtotime($input_method['pickup_time']) < $max_time)
								{
									$this -> response(array(
										'status' => "1",
										"message" => LUXURY_NOT_AVAILABLE
									), 200);
								}
							}
						}

					}
				}

			}
			//company user exist or not
			$user_exist = $this -> Bookingapi_model -> user_exist($input_method['customerid']);
			if ($user_exist['status'] == 0)
			{
				$this -> response(array(
					'status' => "0",
					"jid" => "",
					"message" => $user_exist['message']
				), 200);
				exit ;
			}
			elseif ($user_exist['status'] == 2)
			{
				$jid = makejid($input_method['customerid'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => $user_exist['message']
				), 200);
				exit ;
			}
			/*if ($user_exist['is_companyuser'] == 'N')
			 {

			 $data = array(
			 "token" => $user_exist['token'],
			 "is_companyuser" => $user_exist['is_companyuser']
			 );

			 $UserCardAuth = AuthUserCard($data);
			 if ($UserCardAuth['status'] == 1)
			 {
			 $UserCardAuthData = array(
			 "transaction_id" => $UserCardAuth['transaction_id'],
			 "status" => $UserCardAuth['transaction_status'],
			 "user_id" => $input_method['customerid'],
			 "createdon"=>get_gmt_time()
			 );
			 $SaveCardAuthID = $this -> Bookingapi_model -> SaveCardAuth($UserCardAuthData);
			 if (!$SaveCardAuthID)
			 {
			 $this -> response(array(
			 'status' => "0",
			 "jid" => $jid,
			 "message" => SERVER_ERROR
			 ), 200);
			 }

			 }
			 else
			 {
			 $jid = makejid($input_method['user_id'], USER_JID_NAME);
			 $this -> response(array(
			 'status' => "0",
			 "jid" => $jid,
			 //"message" => CARDAUTHFAIL
			 "message" => $UserCardAuth['message']
			 ), 200);
			 }
			 }*/
			if (@$input_method['coupon_code'] != "")
			{
				$coupon_detail = $this -> Bookingapi_model -> get_coupon_detail($input_method['coupon_code']);
				if ($coupon_detail)
				{
					$discount = $coupon_detail[0]['offer'];
					$input_method['discount_amount'] = $discount;
				}
				else
				{
					$discount = "0";
					$input_method['discount_amount'] = "0";
				}
			}
			else
			{
				$input_method['discount_amount'] = "0";
				$input_method['coupon_code'] = "";

			}
			//check this booking is fixed price(Airport) or not
			$input_method['pickup_latitude'] = $input_method['pickuplat'];
			$input_method['pickup_longitude'] = $input_method['pickuplong'];
			$input_method['destination_latitude'] = $input_method['destinationlat'];
			$input_method['destination_longitude'] = $input_method['destinationlong'];
			$IsFixRating = $this -> IsFixRateBooking($input_method);
			 if ($IsFixRating)
			 {
			 $input_method['is_fixrate'] = $IsFixRating['isairportbooking'];
			 $input_method['airport_id'] = $IsFixRating['airport'];
			 }
			//IF THERE IS ALREADY 5 Pre-BOOKING IN LAST 1 HR NOT ACCEPT THIS BOOKING DUE TO
			// DRIVER BUSY
			$IsDriverFree = $this -> DriverFreeForPreBooking($input_method['pickup_time']);
			if (!$IsDriverFree)
			{
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => PREBOOKINGLIMIT
				), 200);
			}

			$result = $this -> Bookingapi_model -> cab_booking_advance($input_method);
			if ($result)
			{
				$pickuptime = $input_method['pickup_time'];
				$booking_date = date('d/m-Y', strtotime($pickuptime));
				$booking_time = date('H:i', strtotime($pickuptime));
				$customerid = $input_method['customerid'];
				$mobile_number_data = $this -> Bookingapi_model -> get_customer_number($customerid);
				$phone = $mobile_number_data[0]['country_code'] . $mobile_number_data[0]['phone'];
				$get_message = PREBOOKING;
				$msg_replace = array(
					"<DATE>",
					"<TIME>",
					"<ADRESS>"
				);
				$detail = array(
					$booking_date,
					$booking_time,
					urldecode($input_method['pickup_location'])
				);
				//echo $phonenumber;
				$prebooking_sms = str_replace($msg_replace, $detail, $get_message);

				send_sms($phone, $prebooking_sms);
				/*	if ($user_exist['is_companyuser'] == 'N')
				 $LinkCardAuthBooking = $this -> Bookingapi_model ->
				 UpdateCardAuth($SaveCardAuthID, $result);
				 */
				//send_sms($phone,$message);
				//advance_booking_email($result);
				normal_prebooking_mail_to_admin($result, 'booking');
				$this -> response(array(
					'status' => "1",
					"message" => RIDE_LATER . $booking_date . ' || Kl ' . $booking_time
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => "something went wrong"
				), 200);
			}
		}

		/* fetch generinfo like item and fare chart */

		function general_infomation()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('ride-later', $input_method);
			// $this->param_validate($input_method, array("user_id"));
			$gen = $this -> Bookingapi_model -> fetch_general($input_method);
			$fare = $gen['fare'];
			$item = $gen['item'];
			$booking_alert = $gen['booking_alert'];
			// print_R($gen);
			if (count($booking_alert))
			{
				// print_r($booking_alert);
				$booking_alert = $booking_alert[0];
				$origins = $booking_alert['latitude'] . ',' . $booking_alert['longitude'];
				$destination = $booking_alert['pickup_latitude'] . ',' . $booking_alert['pickup_longitude'];
				$time_detail = gettimedetail($origins, $destination);
				if ($time_detail)
				{
					$duration = $time_detail['duration'];
					$distance = $time_detail['distance'];
				}
				else
				{
					$duration = "";
					$distance = "";
				}
				$current_status = $this -> Bookingapi_model -> get_booking_status($booking_alert['booking_id']);
				if ($current_status)
				{
					$status = $current_status;
				}
				else
				{
					$status = $booking_alert['status'];
				}
				$booking_detail = array(
					"CBN" => $booking_alert['booking_number'],
					"booking_id" => $booking_alert['booking_id'],
					"cab_type" => $booking_alert['title'],
					"car_number" => $booking_alert['plate_number'],
					"contact" => $booking_alert['contact'],
					"current_latitude" => $booking_alert['latitude'],
					"current_logitude" => $booking_alert['longitude'],
					"current_location" => getaddress($booking_alert['latitude'], $booking_alert['longitude']),
					"distance" => $distance,
					"duration" => $duration,
					"driver_id" => $booking_alert['driver_id'],
					"name" => $booking_alert['first_name'] . ' ' . $booking_alert['last_name'],
					"driver_image" => base_url() . DRIVER_IMAGE_PATH . $booking_alert['profile_pic'],
					"status" => $status,
					"jid" => makejid($booking_alert['driver_id'], "driver")
				);
			}
			else
			{
				$booking_detail = array();
			}

			$this -> response(array(
				'status' => "1",
				'fare' => $fare,
				'is_street_validation_required' => STREET_VALIDATION,

				//'order_extra' => $item,
				'current_booking' => $booking_detail
			), 200);

		}

		function customer_number($user_id)
		{
			$customer_number = $this -> Bookingapi_model -> get_customer_number($user_id);
			if ($customer_number)
			{
				return $customer_number;
			}
			else
			{
				return false;
			}
			// return $customer_number;
		}

		function driver_number($driver_id)
		{
			$driver_number = $this -> Bookingapi_model -> get_driver_number($driver_id);
			if ($driver_number)
			{
				return $driver_number;
			}
			else
			{
				return false;
			}
		}

		function driver_arrive()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id"
			));
			$driver_id = $input_method['driver_id'];
			$booking_id = $input_method['booking_id'];
			$driver_current_location = $this -> Bookingapi_model -> fetch_driver($input_method['driver_id']);
			$booking_location = $this -> Bookingapi_model -> get_booking_detail($input_method['booking_id']);
			$booking_pickup_lat = $booking_location[0]['pickup_latitude'];
			$booking_pickup_lng = $booking_location[0]['pickup_longitude'];
			$destination = $booking_pickup_lat . ',' . $booking_pickup_lng;
			$origins = $driver_current_location[0]['latitude'] . ',' . $driver_current_location[0]['longitude'];
			$current_booking = $this -> Bookingapi_model -> getdriver_currentbooking($driver_id, $booking_id);
			if ($current_booking)
			{
				for ($i = 0; $i < count($current_booking); $i++)
				{
					$pickup_origin = $current_booking[$i]['pickup_latitude'] . ',' . $current_booking[$i]['pickup_longitude'];
					$pickup_location = $current_booking[$i]['pickup_location'];
					$destination_origin = $current_booking[$i]['destination_latitude'] . ',' . $current_booking[$i]['destination_longitude'];
					$destination_location = $current_booking[$i]['destination_location'];
					$booking_id = $current_booking[$i]['id'];
					$booking_status = $current_booking[$i]['status'];
					if ($booking_status == 'TRIP_STARTED')
					{
						$time_detail = gettimedetail($origins, $destination_origin, 'now', $current_location, $destination_location, $booking_id);
						$distance = $distance + $time_detail['distance_m'];
						$duration = $duration + $time_detail['duration_sec'];
					}
					else
					{
						$time_detail = gettimedetail($origins, $pickup_origin, 'now', $current_location, $pickup_location, $booking_id);
						$distance = $distance + $time_detail['distance_m'];
						$duration = $duration + $time_detail['duration_sec'];

						//current pickup to current booking (driver running booking) destination
						$time_detail = gettimedetail($pickup_origin, $destination_origin, 'now', $pickup_location, $destination_location, $booking_id);
						// time should be distance + now
						$distance = $distance + $time_detail['distance_m'];
						$duration = $duration + $time_detail['duration_sec'];
						//driver new booking pickup location

					}
					$origins = $destination_origin;
				}
			}
			$time_detail = gettimedetail($origins, $destination, 'now', $pickup_location, $destination_location, $booking_id);
			$distance = $distance + $time_detail['distance_m'];
			$duration = $duration + $time_detail['duration_sec'];
			if ($duration)
			{
				$duration = format_time($duration);
				if ($duration == "")
				{
					$duration = "1 Min";
				}
				$distance = $distance / 1000;
			}
			else
			{
				$time_detail = gettimedetail($origins, $destination);
				if ($time_detail)
				{
					$duration = $time_detail['duration'];
					$distance = $time_detail['distance'];
					$duration = format_time($duration);
					if ($duration == "")
					{
						$duration = "1 Min";
					}
					$distance = $distance / 1000;
				}
				else
				{
					$duration = "";
					$distance = "";
				}
			}

			$this -> response(array(
				'status' => "1",
				'duration' => $duration,
				//'order_extra' => $item,
				'distance' => $distance
			), 200);
		}

		function IsFixRateBooking($booking_data)
		{
			$origins = $booking_data['pickup_latitude'] . ',' . $booking_data['pickup_longitude'];
			$destination = $booking_data['destination_latitude'] . ',' . $booking_data['destination_longitude'];

			//chk the pickup and destination loaction if its nearby Airport apply the fixed
			// price  For First Airport
			$airport_latitude = AIRPORT_LAT;
			$airport_longitude = AIRPORT_LNG;
			$isairportbooking = "N";
			//pickup lat.long
			$airport_origin = $airport_latitude . ',' . $airport_longitude;
			$time_detail = gettimedetail($origins, $airport_origin);
			if ($time_detail)
			{
				if (!key_exists('distance', $time_detail))
				{
					$isairportbooking = "N";
				}
				elseif (($time_detail['distance_m'] / 1000) <= 10)
				{
					$distance = $time_detail['distance'];
					$duration = $time_detail['duration'];
					$isairportbooking = "Y";
				}
			}
			//destination lat lng
			$time_detail = gettimedetail($destination, $airport_origin);
			if ($time_detail)
			{
				if (!key_exists('distance', $time_detail))
				{
					$isairportbooking = "N";
				}
				elseif ($time_detail['distance'] <= 10)
				{
					$distance = $time_detail['distance'];
					$duration = $time_detail['duration'];
					$isairportbooking = "Y";
				}
			}
			if ($isairportbooking == 'Y')
			{
				//return $isairportbooking;
				return array(
					"isairportbooking" => $isairportbooking,
					"airport" => 1
				);
			}
			else
			{
				//Check this booking is nearby Airport Sencond (Rygge Airport?)
				$airport_latitude = AIRPORT2_LAT;
				$airport_longitude = AIRPORT2_LNG;
				$isairportbooking = "N";
				//pickup lat.long
				$airport_origin = $airport_latitude . ',' . $airport_longitude;
				$time_detail = gettimedetail($origins, $airport_origin);
				if ($time_detail)
				{
					if (!key_exists('distance', $time_detail))
					{
						$isairportbooking = "N";
					}
					elseif (($time_detail['distance_m'] / 1000) <= 10)
					{
						$distance = $time_detail['distance'];
						$duration = $time_detail['duration'];
						$isairportbooking = "Y";
					}
				}
				//destination lat lng
				$time_detail = gettimedetail($destination, $airport_origin);
				if ($time_detail)
				{
					if (!key_exists('distance', $time_detail))
					{
						$isairportbooking = "N";
					}
					elseif (($time_detail['distance_m'] / 1000) <= 10)
					{
						$distance = $time_detail['distance'];
						$duration = $time_detail['duration'];
						$isairportbooking = "Y";
					}
				}
				if ($isairportbooking == 'Y')
				{
					//return $isairportbooking;
					return array(
						"isairportbooking" => $isairportbooking,
						"airport" => 2
					);
				}
				else
				{
					//check for third airport id
					$airport_latitude = AIRPORT3_LAT;
					$airport_longitude = AIRPORT3_LNG;
					$isairportbooking = "N";
					//pickup lat.long
					$airport_origin = $airport_latitude . ',' . $airport_longitude;
					$time_detail = gettimedetail($origins, $airport_origin);
					if ($time_detail)
					{
						if (!key_exists('distance', $time_detail))
						{
							$isairportbooking = "N";
						}
						elseif (($time_detail['distance_m'] / 1000) <= 10)
						{
							$distance = $time_detail['distance'];
							$duration = $time_detail['duration'];
							$isairportbooking = "Y";
						}
					}
					//destination lat lng
					$time_detail = gettimedetail($destination, $airport_origin);
					if ($time_detail)
					{
						if (!key_exists('distance', $time_detail))
						{
							$isairportbooking = "N";
						}
						elseif (($time_detail['distance_m'] / 1000) <= 10)
						{
							$distance = $time_detail['distance'];
							$duration = $time_detail['duration'];
							$isairportbooking = "Y";
						}
					}
					if ($isairportbooking == 'Y')
					{
						//return $isairportbooking;
						return array(
							"isairportbooking" => $isairportbooking,
							"airport" => 3
						);
					}
					else
					{
						return array(
							"isairportbooking" => $isairportbooking,
							"airport" => 0
						);

					}

				}

			}
		}

		function IsLastInvoicePaid($user_id)
		{
			$LastBookingStatus = $this -> Bookingapi_model -> LastInvoiceStatus($user_id);
			//print_r($LastBookingStatus);
			if ($LastBookingStatus)
			{
				if ($LastBookingStatus['status'] == 'TRIP_ENDED')
				{
					return false;
				}
				else
				{
					return true;
				}

			}
			else
			{
				return true;
			}
		}

		function IsAutoCustomerAccept($duration)
		{
			//First Check the duration in sec
			if (stripos($duration, "Sec") === true)
			{
				return true;
			}
			elseif (stripos($duration, "Min") == true)
			{
				$duration = strtolower($duration);
				$duration = str_replace("min", "", $duration);
				if ($duration <= 20)
				{
					return true;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}

		}

		function SendPacketToDriverCutomer($data)
		{
			$booking_id = $data['booking_id'];
			$customer_id = $data['user_id'];
			$type = "ACCEPT";
			$driver_id = $data['driver_id'];
			$params = array(
				'booking_id' => $booking_id,
				'user_id' => $customer_id,
				"type" => "accept",
				"driver_id" => $driver_id
			);
			$url = CUSTOMER_RESPONSE_API;
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_POSTFIELDS => $params,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_VERBOSE => true
			));
			$result = curl_exec($ch);
			curl_close($ch);

			if ($result)
			{
				//echo 'trip end';
				$get_result = json_decode($result);
				//print_r($get_result);
				$status = $get_result -> status;
				if ($status == '1')
				{

					$pluginurl = AUTO_ACCEPT_WEBSERVICE;
					$ch = curl_init();
					curl_setopt_array($ch, array(
						CURLOPT_URL => $pluginurl,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => $result,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_SSL_VERIFYPEER => false,
						CURLOPT_VERBOSE => true,
						CURLOPT_HTTPHEADER => array(
							'Authorization:T36HCEyK',
							'Content-Type: application/json',
							'Content-Length: ' . strlen($result)
						)
					));
					$plugin_result = curl_exec($ch);
					
					if ($plugin_result)
					{
						//echo 1;
					}
					if (!curl_errno($ch))
					{
						//echo 1;
						$info = curl_getinfo($ch);
						//print_r($info);

					}
					curl_close($ch);
				}
				//curl_close($ch);
			}
		}

		function DriverFreeForPreBooking($booking_time)
		{
			$IsAllow = $this -> Bookingapi_model -> DriverFreeForPreBooking($booking_time);
			if ($IsAllow)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}

	}
			?>
