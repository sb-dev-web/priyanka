<?php

	class Webcustomer_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}
		
		function save_user($input_method)
		{
			$country_code=$input_method['country_code'];
			$phone=$input_method['phone'];
			$email=$input_method['email'];
			$this -> delete_temp_user($country_code . $phone, $email);
			$input_arr=array("name"=>$input_method['name'],
			                 "email"=>$input_method['email'],
			                // "password"=>md5($input_method['password']),
			                 "phone"=>$input_method['phone'],
			                 "country_code"=>$input_method['country_code'],
			                 "login_type"=>"W",
			                 "is_social"=>"N",
			                 "varifycode"=>$input_method['varifycode'],
			                 "is_company"=>"N",
			                 "email_code"=>$input_method['email_code'],
			                 "pass"=>$input_method['password'],
			                 "mode"=>"NORMAL",
			                 "is_varify"=>"1",
						);
			$insert=$this->db->insert("user_temp",$input_arr);
			
			if($this->db->insert_id())
			{
				return $this->db->insert_id();
			}else
				{
					return false;
				}			
		}
		
		function delete_temp_user($phone, $email)
		{

			$query = "delete from user_temp where email='$email' or concat(country_code,phone)='$phone'";
			$this -> db -> query($query);
		}
		
		function GetUser($auth_token)
		{
			$this->db->select("*");
			$query =$this->db->get_where("user",array("auth_token"=>$auth_token,"is_active"=>"1","is_deleted"=>"0"));
			//echo $this->db->last_query();
			if($query->num_rows())
			{
				 $data =$query->row_array();
				return $data;
			}else
				{
					return false;
				}
		}
		
		function GetUserFromEmail($email)
		{
			$this->db->select("*");
			$query =$this->db->get_where("user",array("email"=>$email));
			if($query->num_rows()>0)
			{
				return $query->row_array();
			}else
				{
					return FALSE;
				}
		}

	}
?>
