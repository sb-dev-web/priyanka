<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Users
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Users</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
        
                    
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              
             
            </div>
            <div class="box-body record">
            	  <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <a href="<?php echo site_url('Generalinfo/AddUser');?>"><input type="button" class="btn btn-block btn-warning" value="Add" style="float:right; margin-right:7px;margin-top:8px; width:10%;"/></a><br/><br/>
             
            </div>
              
                 <img id="loading-image1" src="http://dev.privatedriverapp.com/app/images/loading.gif" style="display:none;" height="100x" width="100px"/>
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Date Of Birth</th>
                         <th>Email</th>
                        <th>Phone</th>
                        <th>Status</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                                        
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
               
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
     
      
 <!--pop up -->
 <div id="dialog" style="display: none">
        <div id="dvMap" style="height: 380px; width: 580px;">
        </div>
    </div>
      
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
     <script>
     $(document).ready(function() {
				var dataTable = $('#example2').DataTable({
					"processing": true,
					 "ordering": false,
					"serverSide": true,
					
					"ajax":{
						url :'<?php echo site_url('Generalinfo/GetUser');?>', // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".employee-grid-error").html("");
							$("#example2").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
							
						}
					}
				});
				});
     

       
    function changeStatus(id,loadingdiv,spanid)
{
  //alert();
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    if(spanValue=='Inactive')
    {  
       status = '1';
    }
    $.ajax({type:'POST',url: '<?php echo site_url('Generalinfo/cuser_change_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       statusfeedback(response,spanid,loadingdiv);
     }});
}
function statusfeedback(retstatus,spanid,loadingdiv) 
{
    //alert(retstatus);
    if(retstatus=='0')
    {
        $('#'+spanid).removeClass('label-success').addClass('label-warning');
        $('#'+spanid).text("Inactive");
    }
    else if(retstatus=='1')
    {

        $('#'+spanid).removeClass('label-warning').addClass('label-success');
        $('#'+spanid).text("Active");
    }
     $('#'+loadingdiv).css('display','none');
    
}
</script>
