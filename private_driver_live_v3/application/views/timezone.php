
<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           
          Time Zone Setting 
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active"></li>
          </ol>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         
                   
          
          
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"> </h3>
             
             
            </div>
            <div class="box-body">
                
         
            
           <label>Set Time Zone</label><br>
           <?php
          // echo '<pre>';
          // print_r($timezone);
           if(sizeof($timezone)>=1)
           {
                 $time=$timezone[0]->time_zone;
                
           }
           ?>
            <select id="timezoneselect">
                 <option value="">Please Select</option>
               
                 
                 
                 
                 
                <option value="1" <?php   if($time=='1')  {    echo 'selected'; }?>>GMT +1 Hour</option>
                <option value="2" <?php   if($time=='2')  {    echo 'selected'; }?>>GMT +2 Hours</option>
                
            </select>
           
          
          
            </div>
            <button type="submit" class="btn btn-primary" id="submit" name="submit" style="margin-left:300px;margin-top:-78px;height:32px;" onclick="gettimezone();">Submit</button>
            <br/>  <img id="loading-image" src="http://dev.privatedriverapp.com/app/images/loader19.gif" style="display:none;"/> <br/>
            <div id="showreport">
              <!-- <label style="display:block">Total Received:<a hreft=# onclick="getdriver('<?php echo $this->session->userdata('did'); ?>','1')"><?php echo $t_booking;?></a>&nbsp;&nbsp;&nbsp;&nbsp; Total Accept:<a href='#'onclick="getdriver('<?php echo $this->session->userdata('did'); ?>','2')"><?php echo $this->session->userdata('totalaccept');?></a>&nbsp;&nbsp;&nbsp;&nbsp; Total Reject:<a href='#'onclick="getdriver('<?php echo $this->session->userdata('did');?>','3')"><?php echo $this->session->userdata('driverreject');?></a></label>-->
             
            </div>
             <center><img id="loading-image1" src="http://dev.privatedriverapp.com/app/images/loading.gif" style="display:none;" height="100px" width="100px"/>
            </center>
            <div id="show_report_data"></div>
        

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy;2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
     <script src="<?php echo base_url();?>js/jquery.select2.js"></script>
<script>
   $("#timezoneselect").select2();
  function  gettimezone()
    {
       
        var time_zone=$('#timezoneselect').val();
    
     
         if (time_zone=='') {
       alert("Please select timezone");
       }
     
        
     else{
       $.ajax({
            type:'POST',
            url: '<?php echo site_url('Generalinfo/set_timezone');?>',
            data:{"timezone":time_zone},
             beforeSend: function() {
              $("#loading-image").show();
           },
           success: function(response){
                            //alert(data);
                            if(response == 1)
                            {
                              
                                setTimeout(function() {
                                    $.bootstrapGrowl("Timezone  saved successfully", {
                                        type: 'success',
                                        align: 'right',
                                        width: 'auto',
                                        allow_dismiss: true
                                    });
                                }, 1000);
                                window.setTimeout(function(){location.reload()},2000);
                            }
                           
                            
                            
                            else
                            {
                             
                                setTimeout(function() {
                                    $.bootstrapGrowl("Sorry Driver is not assgin", {
                                        type: 'danger',
                                        align: 'right',
                                        width: 'auto',
                                        allow_dismiss: true
                                    });
                                }, 1000); 
                            }
                            
                        }
            
            
            
            
            
            
       });
            
            
            
            
            
    }
    }
</script>
