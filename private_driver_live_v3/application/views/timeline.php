<style>
	.shift_end{
		color:#C03B44;
		padding-right: 5px;
		font-weight: bold;
	}
	.shift_start{
		color:#75CE66;
		padding-right: 5px;
		font-weight: bold;
		
	}
	.cab_number
	{
		color:#7F8C97;
		padding: 2px;
	}
	.cab_model
	{
		color:#BAC4CB;
		padding: 2px;
	}
</style>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='http://fonts.googleapis.com/css?family=Droid+Serif|Open+Sans:400,700' rel='stylesheet' type='text/css'>

<!--	<script src="js/modernizr.js"></script> <!-- Modernizr -->
  	
	<title>Private Driver Report</title>
</head>
<body>
	<!---<header>
		<h1>Responsive Vertical Timeline</h1>
	</header>-->
<?php //print_R($report_data); ?>
	<section id="cd-timeline" class="cd-container" style="min-height: 500px; overflow: auto; scroll-behavior: auto; ">
    <?php 
    $count=0;
    foreach($report_data as $report){
        
        
        ?>
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-picture">
				<img src="<?php echo base_url(); ?>images/cd-icon-picture.svg" alt="Picture">
			</div> <!-- cd-timeline-img -->

			<div class="cd-timeline-content">
				<span class="shift_start">Shift start</span>
				<!--<p><?php echo $report['cab_title']  ?><?php echo $report['cab_number'] ?><?php $report['car_model'] ?></p>
			-->
			<span class="cab_title"><?php echo $report['cab_title']  ?></span>
        				<span class="cab_number"><?php echo $report['cab_number'] ?>
        				<span class="cab_model"><?php $report['car_model'] ?></span>
				<span class="cd-date"><?php echo date("d-M-Y H:i:s",strtotime(gmt_to_norway($report['shift_start']))) ?></span>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->
        <?php  if($report['break_data']){ ?>
                
                 <?php foreach($report['break_data'] as $break){ ?>
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-picture">
				<img src="<?php echo base_url(); ?>images/cd-icon-picture.svg" alt="Picture">
			</div> <!-- cd-timeline-img -->

			<div class="cd-timeline-content">
				<h2>Break <?php echo $break['type'] ?></h2>
				<p><?php echo "break". $break['type']  ?><br /> <?php echo $break['shift_id'] ?></p>
				
				<span class="cd-date"><?php  echo date("d-M-Y H:i:s",strtotime(gmt_to_norway($break['break_time']))) ?></span>
			</div> <!-- cd-timeline-content -->
		</div>
                
        <?php } }
             if($report['shift_ended']!="")
             {?>
               <div class="cd-timeline-block">
        			<div class="cd-timeline-img cd-movie">
        			         <img src="<?php echo base_url(); ?>images/cd-icon-movie.svg" alt="Picture">
        			</div> <!-- cd-timeline-img -->
        			<div class="cd-timeline-content">
        				<span class="shift_end">Shift Ended</span>
        				<span class="cab_title"><?php echo $report['cab_title']  ?></span>
        				<span class="cab_number"><?php echo $report['cab_number'] ?>
        				<span class="cab_model"><?php $report['car_model'] ?></span>
        			
        			   <span class="cd-date"><?php echo date("d-M-Y H:i:s",strtotime(gmt_to_norway($report['shift_start']))) ?></span>
        			</div> <!-- cd-timeline-content -->
		        </div>
                
             <?php }
          ?>
<?php $count++;} ?>
		 <!-- cd-timeline-block -->
	</section> <!-- cd-timeline -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="<?php echo  base_url(); ?>js/main.js" type="text/javascript"></script>
</body>
</html>