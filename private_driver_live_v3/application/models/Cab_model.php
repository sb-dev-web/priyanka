<?php
class Cab_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

   
    
      function get_cab_details($limit,$offset=0)
    {
       
       $query=$this->db->query("select * from cab as c ,cab_type as ca where c.cab_type=ca.type_id and c.isdelete='0'  order by c.cab_id DESC limit $offset,$limit");
           
            return $query->result();
    }
    
    function get_cab_type()
    {
        $query = $this->db->get('cab_type');
        return $query->result();
    }
    
    function save_cab($id="")
    {
        $arr_feilds = array(
                            'cab_type'=>$_POST['cab_type'],
                            'cab_plate_no'=>$_POST['plate_number'],
                            'cab_model'=>$_POST['cab_model'],
                            'service_area'=>$_POST['service_area']
                            
                            
                            );
        
        if($id>0)
        {
            $this->db->where('cab_id',$id);
            //$this->db->set('create_on', GMT_DATE, FALSE);
            $this->db->update('cab',$arr_feilds);
            return $this->db->affected_rows();
        }
        else
        {
            //$this->db->set('create_on', GMT_DATE, FALSE);
            $this->db->insert('cab',$arr_feilds);
            //echo $this->db->last_query();
            return $this->db->insert_id();
        }
    }
    function get_cab($id)
    {
       if($id>0)
            $query=$this->db->query("select * from cab where cab_id=$id and isdelete='0'");
        //echo $this->db->last_query();exit;
        return $query->result(); 
    }
    
     function change_cab_status($id,$status)
    {
        if($status=="1")
        {
            $field_array=array("isactive"=>"1");
            $status=='1';
        }
        else
        {
            $field_array=array("isactive"=>"0");
            $status=='0';
        }
        $this->db->where('cab_id',$id);
        $this->db->update("cab",$field_array);
        return $status;
    }
     function delete_cab($id)
    {
           $this->db->where('cab_id',$id);
        $this->db->set('isdelete','1');
         $this->db->update("cab");
        return 1;
    }
    function Cab_count()
    {
       $cab=$this->db->query("select * from cab where isdelete='0'");
       $count=$cab->num_rows();
       return $count;
    }

}