<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Phonebooking extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url',
				'constants_helper',
				'function_helper'
			));
			$this -> load -> model('Phonebooking_model');
			$this -> load -> library(array(
				'session',
				'email',
				'pagination',
				'paginationlib',
				'Twilio',
				'curl',
				'Openfire'
			));
			//$this -> load -> library('xmpp', false);
			$user = $this -> session -> userdata('logged_in');
			if (!$user)
			{
				redirect('Welcome');
			}

		}

		public function Add()
		{

			$data['active_page'] = 'phone_booking';
			//get the all agency
			$agency = $this -> Phonebooking_model -> GetAgency();
			$customer = $this->Phonebooking_model->GetCustomer();
			$data['agency'] = $agency;
			$data['customer_data']=$customer;
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('phonebooking');
			$this -> load -> view('site-nav/footer');
		}

		public function saveagency()
		{
			//print_r($_POST);
			error_reporting(1);
			echo $savedata =$this->Phonebooking_model->SaveUser($_POST);

		}
		
		public function check_email()
		{
			$email=$_POST['email'];
			echo $this->Phonebooking_model->Checkemail($email);
		}

        function SaveBooking()
		{
			//print_r($_POST);
           echo $this->Phonebooking_model->SaveBooking($_POST);
		}

	}
