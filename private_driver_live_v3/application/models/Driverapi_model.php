<?php

	//define("GMT_DATE", "DATE_ADD(UTC_TIMESTAMP(), INTERVAL 0 HOUR)");
	class Driverapi_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		/* driver login */
		function check_login($input_method)
		{

			$username = $input_method['username'];
			$password = md5($input_method['password']);
			$device_id = @$input_method['device_id'];
			//$update_driver_token = $this->db->query("update `driver` set
			// `device_token`='$device_token' WHERE `email_id`='$username'");
			// $this->db->select('driver.driver_id,driver.first_name,driver.last_name,driver.city,driver.gender,driver.zipcode,driver.contact,driver.email_id,driver.profile_pic,driver.islogin,driver.latitude,driver.longitude,shift.s_id,shift.comment,shift.cab_id,shift.status,cab_image.cab_image,cab.cab_plate_no,cab.cab_model,cab_type.title,driver.device_id,shift.isbreak');
			$this -> db -> select("*");
			$this -> db -> where("email_id ='" . $username . "' AND password ='" . $password . "' AND driver.isactive='1' AND driver.isdelete ='0'");
			$this -> db -> from("driver");
			// $this->db->join("shift", "shift.driver_id=driver.driver_id and
			// shift.status='STARTED'",      "left");
			//$this->db->join("cab_image", "cab_image.shift_id=shift.s_id", "left");
			// $this->db->join("cab", "cab.cab_id=shift.cab_id", "left");
			// $this->db->join("cab_type", "cab.cab_type=cab_type.type_id", "left");
			$query = $this -> db -> get();
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$result = $query -> result_array();
				$islogin = $result[0]['islogin'];
				$last_login_device_id = $result[0]['device_id'];
				if ($islogin == 'Y' && $device_id != $last_login_device_id)
				{
					return -1;
				}
				$update_string = array(
					"islogin" => "Y",
					"device_id" => $device_id
				);
				$this -> db -> where("email_id ='" . $username . "' AND password ='" . $password . "' ");
				$this -> db -> update("driver", $update_string);

				//make driver status available
				$driver_status= $this -> driver_break_detail($result[0]['driver_id']);
				//chk driver on break or not
				$this -> db -> where("email_id ='" . $username . "' AND password ='" . $password . "' ");
				$update_status = array("status" => $driver_status);
				$this -> db -> update('driver', $update_status);
				// echo $this->db->last_query();
				return $query -> result();
			}
			else
			{
				return 0;
			}
		}

		/* saving temprorary image in image  history table */
		function save_cab_image($input_method, $file_name)
		{
			$date = get_gmt_time();

			$arr_field = array(
				'cab_image' => $input_method['image'],
				//'cab_id' => $input_method['cab_id'],
				'driver_id' => $input_method['driver_id'],
				'createdon' => $date,
			);
			$this -> db -> insert('image_history', $arr_field);
			//echo $this->db->last_query();
			return $this -> db -> insert_id();

		}

		/* chnaging driver password */
		function resetdriverPassword($input_method)
		{
			$driver_email = $input_method['driver_email'];
			$oldPass = $input_method['old_password'];
			$newPass = $input_method['new_password'];

			$this -> db -> select('id,password');
			$this -> db -> where('email_id', $driver_email);
			$query = $this -> db -> get('driver');
			if ($query -> num_rows() > 0)
			{

				$row = $query -> row();

				if ($row -> password == md5($oldPass))
				{

					$query_new = $this -> db -> query("update `driver` set `password`=md5('$newPass')  WHERE `email_id`='$driver_email'");
					return array(
						"status" => 1,
						"message" => DRIVER_PASSWORD_CHANGE
					);
				}
				else
				{
					return array(
						"status" => 0,
						"message" => DRIVER_OLD_PASSWORD_NOT_MATCH
					);
				}
			}
			else
			{
				return array(
					"status" => 0,
					"message" => DRIVER_EMAIL_NOT_MATCH
				);
			}

		}

		/*driver shift started */
		function driver_shift($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$cab_id = $input_method['cab_id'];
			$latitude = $input_method['latitude'];
			$longitude = $input_method['longitude'];
			$comment = @$input_method['comment'];
			$mobile_serial_number = $input_method['mobile_serial_number'];
			if ($comment == '')
			{
				$comment = "";
			}
			$date = get_gmt_time();
			$cab_image_ids = $input_method['cab_image_ids'];
			//check DRIVER LOGIN OR NOT
			$this -> db -> select("driver_id");
			$this -> db -> where("islogin", "Y");
			$this -> db -> where("driver_id", $driver_id);
			$this -> db -> from("driver");
			$query = $this -> db -> get();
			if (!$query -> num_rows())
			{
				return array(
					"status" => 0,
					"message" => DRIVER_NOT_LOGIN
				);
			}

			//********************CHK CAB
			// IMAGES**************************************************
			$fetch_image = $this -> db -> query("SELECT * from image_history where  id in ($cab_image_ids) ");
			//and cab_id=$cab_id
			if ($fetch_image -> num_rows() == 0)
			{
				return array(
					"status" => 0,
					"message" => CAB_IMAGE_NOT_FOUND
				);
			}

			//*****************CHK DRIVER SHIFT ALREADY START
			// **************************************
			$chk_shift = $this -> db -> query("select s_id from shift where driver_id='$driver_id' and status='STARTED'");
			if ($chk_shift -> num_rows() > 0)
			{
				$old_shift_data = $chk_shift -> result_array();
				$old_shift_id = $old_shift_data[0]['s_id'];
				/************************CHECK OLD SHIFT
				 * BREAK*************************************/
				$break_detail = $this -> db -> query("select id,type from break where shift_id=$old_shift_id order by id desc limit 1");
				if ($break_detail -> num_rows() > 0)
				{
					$break_data = $break_detail -> result_array();
					if ($break_data[0]['type'] == DRIVER_BREAK_START)
					{
						//*******************MAKE THE ENDED THE BREAK********************
						$shift_break_end = DRIVER_BREAK_END;
						$insert_array = array(
							"driver_id" => $driver_id,
							"break_time" => $date,
							"type" => "$shift_break_end",
							"shift_id" => $old_shift_id
						);
						$endbreak = $this -> db -> insert("break", $insert_array);
						//insert into driver status table
						$status_data=array("status"=>"BREAK_ENDED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"createdon"=>get_gmt_time()
						);
						$driver_process= $this->driver_status($status_data);
						if (!$endbreak)
						{
							return array(
								"status" => 0,
								"message" => OLD_BREAK_END_ISSUE
							);
						}
					}

				}

				/************************END OLD DRIVER
				 * SHIFT*************************************/
				$end_shift = $this -> db -> query("update shift set status='ENDED',endon='" . $date . "' where s_id=$old_shift_id");
				$status_data=array("status"=>"SHIFT_ENDED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"shift_id"=>$old_shift_id,
											"createdon"=>get_gmt_time()
						);
				$driver_process= $this->driver_status($status_data);
				if (!$end_shift)
				{
					return array(
						"status" => 0,
						"message" => OLD_SHIF_END_ISSUE
					);
				}
				//********************CHK CAB STATUS******************************************
				$chk_cab = $this -> db -> query("select s_id,driver_id from shift where cab_id=$cab_id and status='STARTED'");
				if ($chk_cab -> num_rows() > 0)
				{
					return array(
						"status" => 0,
						"message" => CABBUSY
					);

					/******TEMPORARY COMMENTED NOT NEEDED NOW...
					 $old_cab_shift_data = $chk_cab->result_array();
					 $old_cab_shift_id = $old_cab_shift_data[0]['s_id'];
					 $old_cab_driver_id = $old_cab_shift_data[0]['driver_id'];
					 //*********************END OLD SHIFT OF CAB*******************************
					 $end_cab_shift = $this->db->query("update shift set status='END' , endon='" .
					 $date .
					 "' where cab_id=$old_cab_shift_id");
					 $end_driver_shift = $this->db->query("update driver set status='Shift_End' where
					 driver_id=$old_cab_driver_id");
					 */
				}
				else
				{
					//***************************START NEW
					// SHIFT**************************************
					$data = array(
						"driver_id" => $driver_id,
						"cab_id" => $cab_id,
						"comment" => $comment,
						"latitude" => $latitude,
						"longitude" => $longitude,
						"createdon" => $date,
						//"mobile_serial_number" => $mobile_serial_number
					);
					$insert = $this -> db -> insert("shift", $data);
					$shift_id = $this -> db -> insert_id();
					$status_data=array("status"=>"SHIFT_STARTED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"shift_id"=>$shift_id,
											"createdon"=>get_gmt_time()
						);
					$driver_process= $this->driver_status($status_data);
				}
			}
			else
			{
				//********************CHK CAB STATUS******************************************
				$chk_cab = $this -> db -> query("select s_id,driver_id from shift where cab_id=$cab_id and status='STARTED'");
				if ($chk_cab -> num_rows() > 0)
				{
					return array(
						"status" => 0,
						"message" => CABBUSY
					);
					/******************************TEMP COMMENT THIS CODE********************
					 //due to client want to stop auto shift end of any cab..
					 $old_cab_shift_data = $chk_cab->result_array();
					 $old_cab_shift_id = $old_cab_shift_data[0]['s_id'];
					 $old_cab_driver_id = $old_cab_shift_data[0]['driver_id'];
					 //*********************END OLD SHIFT OF CAB*******************************
					 $end_cab_shift = $this->db->query("update shift set status='END' , endon='" .
					 $date .
					 "' where cab_id=$old_cab_shift_id");
					 $end_driver_shift = $this->db->query("update driver set status='Shift_End' where
					 driver_id=$old_cab_driver_id");
					 if ($end_cab_shift)
					 {
					 //****************************START NEW
					 // SHIFT**************************************
					 $data = array(
					 "driver_id" => $driver_id,
					 "cab_id" => $cab_id,
					 "comment" => $comment,
					 "latitude" => $latitude,
					 "longitude" => $longitude,
					 "createdon" => $date);
					 $insert = $this->db->insert("shift", $data);
					 $shift_id = $this->db->insert_id();
					 }
					 else
					 {
					 return false;
					 }
					 */
				}
				else
				{
					//***************************INSERT NEW
					// SHIFT**************************************
					$data = array(
						"driver_id" => $driver_id,
						"cab_id" => $cab_id,
						"comment" => $comment,
						"latitude" => $latitude,
						"longitude" => $longitude,
						"createdon" => $date,
						//"mobile_serial_number" => $mobile_serial_number
					);
					$insert = $this -> db -> insert("shift", $data);
					$shift_id = $this -> db -> insert_id();
					$status_data=array("status"=>"SHIFT_STARTED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"shift_id"=>$shift_id,
											"createdon"=>get_gmt_time()
						);
					$driver_process= $this->driver_status($status_data);

				}
			}
			// echo "shift".$shift_id;
			if ($shift_id)
			{
				//put the conditions here and explode the comma seperate here..

				//$car_busy = $this->db->query("update `cab` set `is_walkin`='Y' WHERE
				// `cab_id`='$cab_id'");
				$fetch_current_image = $this -> db -> query("SELECT * from image_history where  id in ($cab_image_ids)");
				$data = $fetch_current_image -> result();
				//print_r($data);

				/* foreach ($data as $fetch_data)
				 {
				 $cab_image = $fetch_data->cab_image;
				 $did = $fetch_data->driver_id;
				 $cid = $fetch_data->cab_id;
				 $insert_shift = $this->db->query("insert into cab_image
				 (cab_image,d_id,c_id,shift_id,createdon)
				 values('$cab_image',$did,$cid,$shift_id,'$date')");
				 }*/
				$insert_shift_cab_image = $this -> db -> query("insert into cab_image(cab_image,cab_id,driver_id,shift_id,createdon)(select cab_image,cab_id,driver_id,$shift_id,'$date' from image_history where id in ($cab_image_ids))");
				if ($insert_shift_cab_image)
				{
					$delete_image = $this -> db -> query("delete from image_history where id in ($cab_image_ids)");
					// echo $this->db->last_query();
					//get company mobile number
					$company_mobile = $this -> get_company_mobile($mobile_serial_number);
					//******************update driver status******************

					$data = array("status" => DRIVER_AVAILABLE,
						//"contact" => $company_mobile
					);
					$this -> db -> where("driver_id", $driver_id);
					$update = $this -> db -> update("driver", $data);
					//  echo $this->db->last_query();
					//  exit;
					if (!$update)
					{
						return array(
							"status" => 0,
							"message" => "update issue"
						);
						//return false; ///return array with error message...
					}
					return array(
						"status" => 1,
						"shift_id" => $shift_id
					);
				}
				else
				{
					return array(
						"status" => 0,
						"message" => CAB_IMAGE_SAVE_ISSUE
					);
				}
			}
			else
			{
				return array(
					"status" => 0,
					"message" => SERVER_ERROR
				);
			}
		}

		/* fetch availabel cab */
		function fetch_cab()
		{

			$query = $this -> db -> query("SELECT c.cab_id,ct.title,c.cab_model,c.cab_plate_no FROM `cab` c join cab_type ct on c.cab_type=ct.type_id where c.isactive='1' and c.isdelete='0'");
			if ($query -> num_rows() > 0)
			{
				return $query -> result();
			}
			else
			{
				return false;
			}
		}

		/*****************************START A SHIFT
		 * BREAK*****************************************************/

		function break_start($input_method)
		{
			// print_r($input_method);
			$driver_id = $input_method['driver_id'];
			$shift_id = $input_method['shift_id'];
			$gmdate = get_gmt_time();

			/************************CHECK THE LAST BREAK STARUS*******/
			$this -> db -> select("id,type");
			$this -> db -> where("shift_id", $shift_id);
			// $this->db->where("type", DRIVER_BREAK_START);
			$this -> db -> from("break");
			$this -> db -> order_by("id", "desc");
			$this -> db -> limit("1");
			$query = $this -> db -> get();
			if ($query -> num_rows() > 0)
			{
				$break_data = $query -> result_array();
				$last_type = $break_data[0]['type'];
				if ($last_type == DRIVER_BREAK_START)
				{
					return array(
						"status" => "1",
						"message" => SUCCESS
					);
				}
				else
				{
					//update the break type
					$break_data = array(
						"driver_id" => $driver_id,
						"shift_id" => $shift_id,
						"type" => DRIVER_BREAK_START,
						"break_time" => $gmdate
					);
					$break_entry = $this -> db -> insert("break", $break_data);
					$status_data=array("status"=>"BREAK_STARTED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"createdon"=>get_gmt_time()
						);
					$driver_process= $this->driver_status($status_data);
					// echo "hghgh";
					// echo $this->db->last_query();
					if ($break_entry)
					{
						//upadte driver status

						$data = array("status" => DRIVER_ON_BREAK);
						$this -> db -> where("driver_id", $driver_id);
						$update = $this -> db -> update("driver", $data);
						// echo $this->db->last_query();
						//update the shift table detail

						$data = array("isbreak" => "Y");
						$this -> db -> where("s_id", $shift_id);
						$this -> db -> where("driver_id", $driver_id);
						$update = $this -> db -> update("shift", $data);
						// echo $this->db->last_query();
						return array(
							"status" => 1,
							"message" => SUCCESS
						);
					}
					else
					{
						return array(
							"status" => 0,
							"message" => SERVER_ERROR
						);
					}
				}
			}
			else
			{
				$break_data = array(
					"driver_id" => $driver_id,
					"shift_id" => $shift_id,
					"type" => DRIVER_BREAK_START,
					"break_time" => $gmdate
				);
				$break_entry = $this -> db -> insert("break", $break_data);
                $status_data=array("status"=>"BREAK_STARTED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"createdon"=>get_gmt_time()
						);
					$driver_process= $this->driver_status($status_data);
				if ($break_entry)
				{
					//upadte driver status

					$data = array("status" => DRIVER_ON_BREAK);
					$this -> db -> where("driver_id", $driver_id);
					$update = $this -> db -> update("driver", $data);

					//update the shift table detail

					$data = array("isbreak" => "Y");
					$this -> db -> where("s_id", $shift_id);
					$this -> db -> where("driver_id", $driver_id);
					$update = $this -> db -> update("shift", $data);

					return array(
						"status" => 1,
						"message" => SUCCESS
					);
				}
				else
				{
					return array(
						"status" => 0,
						"message" => SERVER_ERROR
					);
				}

			}

		}

		/******************END A SHIFT BREAK
		 * *********************************************/
		function break_end($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$shift_id = $input_method['shift_id'];
			$gmdate = get_gmt_time();

			/************************CHECK THE LAST BREAK STARUS*******/
			$this -> db -> select("id,type");
			$this -> db -> where("shift_id", $shift_id);
			// $this->db->where("type", DRIVER_BREAK_END);
			$this -> db -> from("break");
			$this -> db -> order_by("id", "desc");
			$this -> db -> limit("1");
			$query = $this -> db -> get();
			if ($query -> num_rows() > 0)
			{
				$break_data = $query -> result_array();
				$last_type = $break_data[0]['type'];
				if ($last_type == DRIVER_BREAK_END)
				{
					return array(
						"status" => "1",
						"message" => SUCCESS
					);
				}
				else
				{
					//update the break type
					$break_data = array(
						"driver_id" => $driver_id,
						"shift_id" => $shift_id,
						"type" => DRIVER_BREAK_END,
						"break_time" => $gmdate
					);
					$break_entry = $this -> db -> insert("break", $break_data);
					$status_data=array("status"=>"BREAK_ENDED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"createdon"=>get_gmt_time()
						);
					$driver_process= $this->driver_status($status_data);
					if ($break_entry)
					{
						//upadte driver status

						$data = array("status" => DRIVER_AVAILABLE);
						$this -> db -> where("driver_id", $driver_id);
						$update = $this -> db -> update("driver", $data);

						//update the shift table detail

						$data = array("isbreak" => "N");
						$this -> db -> where("s_id", $shift_id);
						$this -> db -> where("driver_id", $driver_id);
						$update = $this -> db -> update("shift", $data);

						return array(
							"status" => 1,
							"message" => SUCCESS
						);
					}
					else
					{
						return array(
							"status" => 0,
							"message" => SERVER_ERROR
						);
					}
				}
			}
			else
			{
				$break_data = array(
					"driver_id" => $driver_id,
					"shift_id" => $shift_id,
					"type" => DRIVER_BREAK_END,
					"break_time" => $gmdate
				);
				$break_entry = $this -> db -> insert("break", $break_data);
				$status_data=array("status"=>"BREAK_ENDED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"createdon"=>get_gmt_time()
						);
					$driver_process= $this->driver_status($status_data);
				if ($break_entry)
				{
					//upadte driver status

					$data = array("status" => DRIVER_AVAILABLE);
					$this -> db -> where("driver_id", $driver_id);
					$update = $this -> db -> update("driver", $data);

					//update the shift table detail

					$data = array("isbreak" => "N");
					$this -> db -> where("s_id", $shift_id);
					$this -> db -> where("driver_id", $driver_id);
					$update = $this -> db -> update("shift", $data);

					return array(
						"status" => 1,
						"message" => SUCCESS
					);
				}
				else
				{
					return array(
						"status" => 0,
						"message" => SERVER_ERROR
					);
				}
			}

		}

		/* ending shift for driver */
		function shift_end($input_method)
		{
			$gmdate = get_gmt_time();
			$shift_id = $input_method['shift_id'];
			$driver_id = $input_method['driver_id'];
			$chek_shift = $this -> db -> query("select s_id from shift where s_id='$shift_id'");
			if ($chek_shift -> num_rows() > 0)
			{
				//chk this shift break status
				$this -> db -> select("id,type");
				$this -> db -> where("shift_id", $shift_id);
				$this -> db -> order_by("id", "desc");
				$this -> db -> limit(1);
				$this -> db -> from("break");
				$query = $this -> db -> get();
				if ($query -> num_rows() > 0)
				{
					$break_detail = $query -> result_array();
					if ($break_detail[0]['type'] == DRIVER_BREAK_START)
					{
						//end the last start break
						$break_data = array(
							"type" => DRIVER_BREAK_END,
							"shift_id" => $shift_id,
							"break_time" => $gmdate,
							"driver_id" => $driver_id
						);
						$query = $this -> db -> insert("break", $break_data);
						$status_data=array("status"=>"BREAK_ENDED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"createdon"=>get_gmt_time()
						);
					$driver_process= $this->driver_status($status_data);
						if (!$query)
						{
							return array(
								"status" => 0,
								"message" => OLD_BREAK_END_ISSUE
							);
						}
					}
				}

				$shift_data = $chek_shift -> result_array();
				$end_shift = $this -> db -> query("update `shift` set `status`='ENDED' ,`endon`='" . $gmdate . "' , isbreak='N' WHERE `s_id`='$shift_id'");
				$status_data=array("status"=>"SHIFT_ENDED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"shift_id"=>$shift_id,
											"createdon"=>get_gmt_time()
						);
					$driver_process= $this->driver_status($status_data);
				if ($end_shift)
				{
					//update driver status
					$driver_id = $input_method['driver_id'];
					$data = array("status" => DRIVER_SHIFT_END);
					$this -> db -> where("driver_id", $driver_id);
					$update = $this -> db -> update("driver", $data);
					if (!$update)
					{
						return array(
							"status" => 0,
							"message" => "Opps! Driver status not updated.."
						);
					}
					return array(
						"status" => 1,
						"message" => SUCCESS
					);

				}
				else
				{
					return array(
						"status" => 0,
						"message" => "opps! Your shift is not ended.."
					);
				}
			}
			else
			{
				return array(
					"status" => 0,
					"message" => SHIFT_NOT_EXIT
				);
			}
		}
        
        function driver_status($data)
		{
			$this->db->insert(driver_status,$data);
			if($this->db->last_query())
			{
				return true;
			}else
			{
				return false;
			}
		}

		/* start break of driver */

		/* get driver info */
		function get_driver_info($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$this -> db -> select('driver.driver_id,driver.first_name,driver.city,driver.gender,driver.zipcode,driver.last_name,driver.contact,driver.email_id,driver.profile_pic,driver.latitude,driver.longitude,shift.s_id,shift.comment,shift.cab_id,shift.status,cab_image.cab_image,cab.cab_plate_no,cab.cab_model,cab_type.title,shift.isbreak,cab_type.type_id,shift.mobile_serial_number');
			// $this->db->where("email_id ='" . $username . "' AND password ='" . $password
			// ."' ");
			$this -> db -> from("driver");
			$this -> db -> where("driver.driver_id ='" . $driver_id . "'");
			$this -> db -> join("shift", "shift.driver_id=driver.driver_id and shift.status='STARTED'", "left");
			$this -> db -> join("cab_image", "cab_image.shift_id=shift.s_id", "left");
			$this -> db -> join("cab", "cab.cab_id=shift.cab_id", "left");
			$this -> db -> join("cab_type", "cab.cab_type=cab_type.type_id", "left");
			$query = $this -> db -> get();
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> result();
			}
			else
			{
				return false;
			}
		}

		/* assign cab */
		function get_assign_cab($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$assign_cab = $this -> db -> query("select c.cab_plate_no from cab as c,shift as s where s.isend='N' and s.driver_id='$driver_id' and c.cab_id=s.cab_id");
			if ($assign_cab -> num_rows() > 0)
			{

				return $assign_cab -> result();
			}
			else
			{
				return false;
			}
		}

		/* update driver current location */
		function current_driver_location($input_method)
		{
			$last_date = gmdate("Y-m-d H:i:s");
			$driver_id = $input_method['driver_id'];
			$latitude = $input_method['lat'];
			$longitude = $input_method['lng'];
			$update_loc = $this -> db -> query("update `driver` set `latitude`='$latitude',`longitude`='$longitude',last_location_update='$last_date' WHERE `driver_id`='$driver_id'");
			$count = $this -> db -> affected_rows();
			if ($count > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		/* for get update drive rlocation */
		function get_driver_location($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$query = $this -> db -> query("select d.latitude,d.longitude,d.last_location_update,ca.type_id from shift as s join driver as d on s.driver_id=d.driver_id join cab as c on c.cab_id=s.cab_id join cab_type as  ca on ca.type_id=c.cab_type where d.driver_id='$driver_id' and s.status!='ENDED'");
			if ($query -> num_rows() > 0)
			{
				return $query -> result();
			}
			else
			{
				return false;
			}
		}

		function get_driver_status($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$this -> db -> select('shift.s_id,shift.status,shift.isbreak,cab.cab_model,cab.cab_plate_no');
			$this -> db -> where("driver_id", $driver_id);
			$this -> db -> from("shift");
			$this -> db -> join("cab", "cab.cab_id=shift.cab_id");
			$this -> db -> limit("1");
			$this -> db -> order_by("shift.s_id", "desc");
			$query = $this -> db -> get();
			// echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return 0;
			}

		}

		function get_next_booking($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$this -> db -> select('user.id as customer_id,cab_booking.status,user.name,user.email,user.phone,cab_booking.id,cab_booking.booking_number,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.comment,driver.latitude,driver.longitude');
			$this -> db -> where("cab_booking.driver_id", $driver_id);
			$this -> db -> where_in("cab_booking.status", array('CUSTOMER_ACCEPTED','TRIP_STARTED'));
			$this -> db -> from("cab_booking");
			$this -> db -> join("user", "user.id=cab_booking.user_id");
			$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
			$this -> db -> limit("1");
			//ORDER BY FIELD( `cab_booking`.status, 'TRIP_STARTED', 'CUSTOMER_ACCEPTED' ) 
			$this -> db -> order_by("FIELD( `cab_booking`.status, 'TRIP_STARTED', 'CUSTOMER_ACCEPTED' )");
			$query_normal = $this -> db -> get();
             //echo $this->db->last_query();
			//restro booking
			$this -> db -> select('restro_cab_booking.client_name as name,restro_cab_booking.status,restro_cab_booking.client_email as email,restro_cab_booking.client_contact_number as phone,restro_cab_booking.id,restro_cab_booking.booking_number,restro_cab_booking.pickup_latitude,restro_cab_booking.pickup_longitude,restro_cab_booking.pickup_location,driver.latitude,driver.longitude');
			$this -> db -> where("restro_cab_booking.driver_id", $driver_id);
			$this -> db -> where_in("restro_cab_booking.status", array('DRIVER_ACCEPTED','TRIP_STARTED'));
			$this -> db -> from("restro_cab_booking");

			$this -> db -> join("driver", "driver.driver_id=restro_cab_booking.driver_id");
			$this -> db -> limit("1");
		//	$this -> db -> order_by("restro_cab_booking.id");
		    $this -> db -> order_by("FIELD(`restro_cab_booking`.status, 'TRIP_STARTED', 'CUSTOMER_ACCEPTED' )");
			$query_restro = $this -> db -> get();
			if ($query_normal -> num_rows() > 0 && $query_restro -> num_rows() > 0)
			{
				$normal_booking = $query_normal -> result_array();
				$restro_booking = $query_restro -> result_array();
				if ($normal_booking[0]['booking_time'] > $restro_booking[0]['booking_time'])
				{
					return $normal_booking;
				}
				else
				{
					return $restro_booking;
				}
			}
			elseif ($query_normal -> num_rows() > 0)
			{
				return $normal_booking = $query_normal -> result_array();
			}
			elseif ($query_restro -> num_rows() > 0)
			{
				return $query_restro -> result_array();
			}
			else
			{
				return false;
			}
		}

		function driver_booking_detail($input_method)
		{
			$driver_id = $input_method['driver_id'];

			$pagenumber = $input_method['page_number'];
			$limit = 10;
			if ($pagenumber == 1)
			{
				$offset = 0;
			}
			else
			{
				$pagenumber = $pagenumber - 1;
				$offset = $pagenumber * 10;
			}
			$this -> db -> select('user.id as userid,user.name,user.company_name,user.email,user.phone,cab_booking.id as bookingid,cab_booking.status,cab_booking.is_prebooking,cab_booking.booking_time,cab_booking.id,cab_booking.booking_number,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.destination_latitude,destination_longitude,cab_booking.status,cab_booking.comment,driver.latitude,driver.longitude');
			$this -> db -> from("cab_booking");
			$this -> db -> join("user", "user.id=cab_booking.user_id");
			$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
			$this -> db -> where("cab_booking.driver_id", $driver_id);
			$this -> db -> where("cab_booking.status !=", "CUSTOMER_REJECTED");
			$this -> db -> where("cab_booking.status !=", "ADVANCE_BOOKING");
			$this -> db -> where("cab_booking.status !=", "CUSTOMER_NOT_RESPOND");
			$this -> db -> where("cab_booking.status !=", "CANCELLED");
			$this -> db -> order_by("cab_booking.id", "desc");
			$this -> db -> limit($limit, $offset);

			$query = $this -> db -> get();
			// echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			{
				return false;
			}
			// echo $this->db->last_query();
		}

		function driver_prebooking_detail($input_method)
		{
			$driver_id = $input_method['driver_id'];

			$pagenumber = $input_method['page_number'];
			$limit = 10;
			if ($pagenumber == 1)
			{
				$offset = 0;
			}
			else
			{
				$pagenumber = $pagenumber - 1;
				$offset = $pagenumber * 10;
			}
			$this -> db -> select('user.id as userid,user.name,user.company_name,user.email,user.phone,cab_booking.id as bookingid,cab_booking.status,cab_booking.is_prebooking,cab_booking.booking_time,cab_booking.id,cab_booking.booking_time,cab_booking.booking_number,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.destination_latitude,destination_longitude,cab_booking.status,cab_booking.comment,driver.latitude,driver.longitude,cab_booking.createdon');
			$this -> db -> from("cab_booking");
			$this -> db -> join("user", "user.id=cab_booking.user_id");
			$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
			$this -> db -> where("cab_booking.driver_id", $driver_id);
			$this -> db -> where("cab_booking.is_prebooking", "Y");
			$this -> db -> where("cab_booking.status", "DRIVER_ASSIGNED");
			$this -> db -> limit($limit, $offset);
			$query_normal = $this -> db -> get();

			//restro booking
			$this -> db -> select('restro_cab_booking.client_name as name,restro_cab_booking.client_email as email,restro_cab_booking.client_contact_number as phone,restro_cab_booking.is_prebooking,restro_cab_booking.booking_time,restro_cab_booking.id,restro_cab_booking.booking_number,restro_cab_booking.pickup_latitude,restro_cab_booking.pickup_longitude,restro_cab_booking.pickup_location,driver.latitude,driver.longitude,restro_cab_booking.createdon,restro_cab_booking.status,restro_cab_booking.id as bookingid,restro_cab_booking.restro_id');
			$this -> db -> where("restro_cab_booking.driver_id", $driver_id);
			$this -> db -> where("restro_cab_booking.status", "DRIVER_ASSIGNED");
			$this -> db -> where("restro_cab_booking.is_prebooking", "Y");
			$this -> db -> from("restro_cab_booking");

			$this -> db -> join("driver", "driver.driver_id=restro_cab_booking.driver_id");
			$this -> db -> limit($limit, $offset);
			$this -> db -> order_by("restro_cab_booking.id");
			$query_restro = $this -> db -> get();
			if ($query_normal -> num_rows() > 0 && $query_restro -> num_rows() > 0)
			{
				//$normal_pre_booking=$query_normal->re
				$pre_booking = array_merge($query_normal -> result_array(), $query_restro -> result_array());
				return $pre_booking;

			}
			elseif ($query_normal -> num_rows() > 0)
			{
				return $query_normal -> result_array();
			}
			elseif ($query_restro -> num_rows() > 0)
			{
				return $query_restro -> result_array();
			}
			else
			{
				return false;
			}
			// echo $this->db->last_query();
		}

		function customer_aware($input_method)
		{
			$booking_id = $input_method['booking_id'];
			$driver_id = $input_method['driver_id'];
			$status = $input_method['type'];
			$this -> db -> select("user.phone,user.country_code,user.device_token,user.email,user.id,cab_booking.booking_number,cab_booking.user_id,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.id,driver.*,cab.*,cab_type.title");
			$this -> db -> from("cab_booking");
			$this -> db -> join("user", "user.id=cab_booking.user_id");
			$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
			$this -> db -> join("shift", "shift.driver_id=driver.driver_id and shift.status!='ENDED'");
			$this -> db -> join("cab", "cab.cab_id=shift.cab_id");
			$this -> db -> join("cab_type", "cab.cab_type=cab_type.type_id");
			$this -> db -> where("cab_booking.id", $booking_id);
			$query = $this -> db -> get();
			//echo $this->db->last_query();
			if ($query -> result_array() > 0)
			{
				// ADDTHE DRIVER STATUS TABLE
				$date = get_gmt_time();
				$driver_status = array(
					"driver_id" => $driver_id,
					"booking_id" => $booking_id,
					"status" => $status,
					"createdon" => $date
				);
				$this -> db -> insert("driver_status", $driver_status);

				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function driver_unavailable($input_method)
		{
			/****************** Vales 1=
			 type =1 = make the Unavailable because use back button from device
			 type=2 make the TEMP_Hold because sent a new booking request to driver
			 type =-1 make Availble
			 */
			$type = $input_method['type'];
			//values 1,2,-1
			$driver_id = $input_method['driver_id'];
			if ($type == '1')
			{
				$driver_status = DRIVER_UNAVAILBLE;
			}
			elseif ($type == '2')
			{
				$driver_status = DRIVER_TEMP_HOLD;
			}
			elseif ($type == '-1')
			{

				$query = $this -> db -> query("SELECT `id` FROM `cab_booking` WHERE `driver_id` = $driver_id 
                                    AND (`status` = 'DRIVER_ACCEPTED' 
                                    OR `status` = 'CUSTOMER_ACCEPTED'
                                    OR `status` = 'TRIP_STARTED')LIMIT 1");
				// echo $this->db->last_query();
				if ($query -> num_rows() > 0)
				{
					$driver_status = "BUSY";
				}
				else
				{
					$driver_status = DRIVER_AVAILABLE;
				}
			}
			$this -> db -> where("driver_id", $driver_id);
			$update_data = array("status" => $driver_status);
			$update_query = $this -> db -> update('driver', $update_data);
			// echo $this->db->last_query();
			if ($update_query)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function get_driver_rating($driver_id)
		{
			$this -> db -> select_avg("rating");
			$this -> db -> where("driver_id", $driver_id);
			$query = $this -> db -> get("driver_rating");
			if ($query -> num_rows() > 0)
			{
				$data = $query -> result_array();
				return number_format($data[0]['rating'], 2);
			}
			else
			{
				return "0";
			}
			//$this->db->query("select AVG(rating) as rating from driver_rating where
			// driver_id=$driver_id");
		}

		function driver_ride_details($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$pagenumber = $input_method['page_number'];
			$booking_type = @$input_method['booking_type'];
			$limit = 10;
			if ($pagenumber == 1)
			{
				$offset = 0;
			}
			else
			{
				$pagenumber = $pagenumber - 1;
				$offset = $pagenumber * 10;
			}
			if ($booking_type != 'restro')
			{
				$this -> db -> select("b.id as booking_id,`booking_number`,`user_id`,`driver_id`,`pickup_latitude`,`pickup_longitude`,pickup_location,destination_location,`destination_latitude`,`destination_longitude`,`driver_assigned_date`,`status`,`booking_time`,`is_prebooking`,`plate_number`,u.name,u.company_name,u.phone,u.country_code,ct.title,'normal' as booking_type");
				$this -> db -> from("cab_booking as b");
				//$this->db->limit($offset,$limit);
				$this -> db -> join("user as u", "u.id=b.user_id");
				$this -> db -> join("cab_type ct", "ct.type_id=b.cab_type", "left");
				$this -> db -> where("b.driver_id", $driver_id);
				$this -> db -> where("b.status!=", "CUSTOMER_REJECTED");
				$this -> db -> where("b.status!=", "CUSTOMER_NOT_RESPOND");
				$this -> db -> limit($limit, $offset);
				$query = $this -> db -> get();
			}
			else
			{
				$query = $this -> db -> query("SELECT client_name,client_email,country_code,client_contact_number,pickup_latitude,pickup_longitude,pickup_location,is_prebooking,status,booking_time,booking_number,r.name,title,'restro' as booking_type,rb.id as booking_id FROM `restro_cab_booking` rb join restaurant r on r.id=rb.restro_id join cab_type t on t.type_id=rb.cab_type where rb.driver_id=$driver_id");
			}

			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}

		}

		function device_register($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$device_token = $input_method['device_token'];
			if ($device_token != "")
			{
				$update_string = array("device_token" => $device_token);
				$this -> db -> where("driver_id", $driver_id);
				$update = $this -> db -> update("driver", $update_string);
				if ($update)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		function update_login_status($input_method)
		{
			$driver_jid = $input_method['user_jid'];
			//driver-25@
			$type = $input_method['login_type'];
			$driver_id = $this -> get_driverid($driver_jid);
			$this -> db -> where("driver_id", $driver_id);
			$update_string = array("islogin" => $type);
			$update_string = $this -> db -> update("driver", $update_string);
			// echo $this->db->last_query();
			if ($update_string)
			{
				return true;
			}
			else
			{
				return false;
			}

		}

		function get_driverid($driver_jid)
		{
			$user_arr = explode('@', $driver_jid);
			$driver_detail_array = explode('-', $user_arr[0]);
			$driver_detail_array = array_reverse($driver_detail_array);
			return $driver_detail_array[0];
		}

		function driver_logout($input_method)
		{
			$driver_id = $input_method['driver_id'];
			//get driver active shift
			$shift_id = $this -> get_active_shift($driver_id);
			if ($shift_id)
			{
				//get there is any break start from this shift
				$isbreak_start = $this -> get_shift_detail($shift_id);
				if ($isbreak_start)
				{
					//end the break for particular shift
					$break_end = $this -> driver_break_end($shift_id, $driver_id);
					if ($break_end)
					{
						//End the shift and update driver status driver status
						$logout = $this -> driver_session_close($shift_id, $driver_id);
						if ($logout)
						{
							return array(
								"status" => 1,
								"message" => SUCCESS
							);
						}
						else
						{
							return array(
								"status" => 1,
								"message" => SERVER_ERROR
							);
						}
					}
					else
					{
						$logout = $this -> driver_session_close($shift_id, $driver_id);
						if ($logout)
						{
							return array(
								"status" => 1,
								"message" => SUCCESS
							);
						}
						else
						{
							return array(
								"status" => 1,
								"message" => SERVER_ERROR
							);
						}
					}
				}
				else
				{
					$logout = $this -> driver_session_close($shift_id, $driver_id);
					if ($logout)
					{
						return array(
							"status" => 1,
							"message" => SUCCESS
						);
					}
					else
					{
						return array(
							"status" => 1,
							"message" => SERVER_ERROR
						);
					}
				}

			}
			else
			{
				//set logout status
				$update_string = array(
					"islogin" => "N",
					"status" => "LOGOUT"
				);
				$this -> db -> where("driver_id", $driver_id);
				$update = $this -> db -> update("driver", $update_string);
				if ($update)
				{
					return array(
						"status" => 1,
						"message" => SUCCESS
					);
				}
				else
				{
					return array(
						"status" => 1,
						"message" => SERVER_ERROR
					);
				}

			}
		}

		function driver_break_end($shift_id, $driver_id)
		{

			$gmdate = get_gmt_time();
			$break_data = array(
				"type" => DRIVER_BREAK_END,
				"shift_id" => $shift_id,
				"break_time" => $gmdate,
				"driver_id" => $driver_id
			);
			$query = $this -> db -> insert("break", $break_data);
			echo $this -> db -> last_query();
			if ($query)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function driver_session_close($shift_id, $driver_id)
		{

			//END SHIFT
			$data = array(
				"status" => "ENDED",
				"isbreak" => "N",
				"endon"=>get_gmt_time(),
			);
			$this -> db -> where("s_id", $shift_id);
			$update = $this -> db -> update("shift", $data);

			$data = array(
				"status" => "LOGOUT",
				"islogin" => "N"
			);
			$this -> db -> where("driver_id", $driver_id);
			$update = $this -> db -> update("driver", $data);
			if ($update)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function get_active_shift($driver_id)
		{

			$this -> db -> select("s_id");
			$this -> db -> where("driver_id", $driver_id);
			$this -> db -> where("status", "STARTED");
			//$this->db->orber_by("id")
			$this -> db -> from("shift");
			$query = $this -> db -> get();
			if ($query -> num_rows())
			{
				$shift_detail = $query -> result_array();
				return $shift_detail[0]['s_id'];
			}
			else
			{
				return false;
			}

		}

		function get_shift_detail($shift_id)
		{

			$this -> db -> select("id,type");
			$this -> db -> where("shift_id", $shift_id);
			$this -> db -> order_by("id", "desc");
			$this -> db -> limit("1");
			//$this->db->where("type","STARTED");
			$this -> db -> from("break");
			$query = $this -> db -> get();
			if ($query -> num_rows())
			{
				$break_detail = $query -> result_array();
				if ($break_detail[0]['type'] == "STARTED")
				{
					return true;
				}
				else
				{
					return false;
				}

			}
			else
			{
				return false;
			}

		}

		function driver_cancel($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$booking_id = $input_method['booking_id'];
			//Check the booking status
			$this -> db -> select("*");
			$this -> db -> where("driver_id", $driver_id);
			$this -> db -> where("id", $booking_id);
			$this -> db -> from("cab_booking");
			$query = $this -> db -> get();
			if ($query -> num_rows())
			{
				$data = $query -> result_array();

				if ($data[0]['status'] != "TRIP_STARTED" && $data[0]['status'] != "FINISH" && $data[0]['status'] != "TRIP_ENDED" && $data[0]['status'] != 'CANCELLED')
				{
                     //comment this make issue in pre-booking cancel 
					//$query = $this -> db -> get_where("cab_booking_history", array("booking_id" => $booking_id));
					//if ($query -> num_rows())
					{
						$gmdate = get_gmt_time();
						$insert_data = array(
							"booking_id" => $booking_id,
							"driver_id" => $driver_id,
							"driver_cancel" => "DRIVER_CANCELLED",
							"driver_cancel_datetime" => $gmdate
						);
						$this -> db -> where("booking_id", $booking_id);
						$res = $this -> db -> update("cab_booking_history", $insert_data);
						
						 $status_data=array("status"=>"DRIVER_CANCELLED",
											"driver_id"=>$driver_id,
											"booking_type"=>"NONE",
											"booking_id"=>$booking_id,
											"createdon"=>$gmdate
						);
				        $driver_process= $this->driver_status($status_data);
						
						//update the booking status in booking table
						$update_status = array("status" => "DRIVER_CANCELLED");
						$this -> db -> where("id", $booking_id);
						$res = $this -> db -> update("cab_booking", $update_status);
						if ($res)
						{
							//make driver free
							$driver_available = DRIVER_AVAILABLE;
							$data = array('status' => "$driver_available");
							$this -> db -> where('driver_id', $driver_id);
							$update = $this -> db -> update('driver', $data);
							return array(
								"status" => 1,
								"message_customer" => CUSTOMER_NOTIFY,
								"message" => DRIVER_CANCEL
							);
						}
						else
						{
							return array(
								"status" => 0,
								"message" => SERVER_ERROR
							);
						}

					}

				}//DRIVER_CANCELLED
				else
				{
					return array(
						"status" => 0,
						"message" => DRIVER_NOT_CANCEL
					);
				}
			}
			else
			{
				return array(
					"status" => 0,
					"message" => BOOKING_NOTFOUND
				);
			}

		}

		function get_customer_detail($input_method)
		{
			$booking_id = $input_method['booking_id'];
			$this -> db -> select("user.phone,user.country_code,user.id");
			$this -> db -> where("cab_booking.id", $booking_id);
			$this -> db -> from("cab_booking");
			$this -> db -> join("user", "user.id=cab_booking.user_id");
			$query = $this -> db -> get();
			if ($query -> num_rows())
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function check_appversion($input_method)
		{
			$os_type = $input_method['os_type'];
			$app_mode = $input_method['app_mode'];
			$query = $this -> db -> get_where("appversion", array(
				"os_type" => $input_method['os_type'],
				"app_mode" => $input_method['app_mode']
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$version_detail = $query -> result_array();
				return $version_detail[0];
			}
			else
			{
				return false;
			}

		}

		function get_mobile_detail()
		{
			$query = $this -> db -> get_where("mobile_no", array("isdelete" => "0"));
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function get_company_mobile($serial_number)
		{
			$query = $this -> db -> get_where("mobile_no", array(
				"isdelete" => "0",
				"serial_number" => $serial_number
			));
			//  echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$result = $query -> row_array();
				return $result['country_code'] . $result['contact_no'];
			}
			else
			{
				return false;
			}
		}

		function customer_away($data)
		{
			$booking_id = $data['booking_id'];
			$driver_id = $data['driver_id'];
			$query = $this -> db -> query("SELECT card_info.braintree_token,cab_booking.user_id,u.country_code,u.phone,u.is_company,u.company_id FROM `cab_booking`
                                                             left join card_info on card_info.customer_id=cab_booking.user_id
                                                              join user u on u.id=cab_booking.user_id
                                                              WHERE cab_booking.id=$booking_id and driver_id=$driver_id and (cab_booking.status='CUSTOMER_ACCEPTED' or cab_booking.status='TRIP_STARTED' or is_prebooking='Y')");
			if ($query -> num_rows())
			{
				$result = $query -> row_array();
				return array(
					"status" => 1,
					"is_companyuser" => $result['is_company'],
					"comapny_id" => $result['company_id'],
					"token" => $result['braintree_token'],
					"user_id" => $result['user_id'],
					"country_code" => $result['country_code'],
					"phone" => $result['phone']
				);
			}
			else
			{
				return array(
					"status" => 0,
					"message" => BOOKING_NOTFOUND
				);
			}
		}

		function mobile_serial_number_exist($mobile_serial_number)
		{
			//chk this mobile serial number exist or not
			$query = $this -> db -> query("SELECT s_id FROM `shift`where mobile_serial_number='" . $mobile_serial_number . "' and status='STARTED'");
			if ($query -> num_rows() > 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		function accepted_booking($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$booking_id = $input_method['booking_id'];
			$this -> db -> select('user.id as customer_id,,user.name,user.email,user.phone,cab_booking.id,cab_booking.booking_number,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.comment,driver.latitude,driver.longitude');
			$this -> db -> where("cab_booking.driver_id", $driver_id);
			$this -> db -> where("cab_booking.id!=", $booking_id);
			$this -> db -> where("cab_booking.status", "CUSTOMER_ACCEPTED");
			$this -> db -> from("cab_booking");
			$this -> db -> join("user", "user.id=cab_booking.user_id");
			$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
			//$this->db->limit("1");
			$this -> db -> order_by("cab_booking.id");
			$query_normal = $this -> db -> get();

			//restro booking
			$this -> db -> select('restro_cab_booking.client_name as name,restro_cab_booking.client_email as email,restro_cab_booking.client_contact_number as phone,restro_cab_booking.id,restro_cab_booking.booking_number,restro_cab_booking.pickup_latitude,restro_cab_booking.pickup_longitude,restro_cab_booking.pickup_location,driver.latitude,driver.longitude');
			$this -> db -> where("restro_cab_booking.driver_id", $driver_id);
			$this -> db -> where("restro_cab_booking.id!=", $booking_id);
			$this -> db -> where("restro_cab_booking.status", "DRIVER_ACCEPTED");
			$this -> db -> from("restro_cab_booking");

			$this -> db -> join("driver", "driver.driver_id=restro_cab_booking.driver_id");
			//$this->db->limit("1");
			$this -> db -> order_by("restro_cab_booking.id");
			$query_restro = $this -> db -> get();

			if ($query_normal -> num_rows() > 0 && $query_restro -> num_rows() > 0)
			{
				$normal_booking = $query_normal -> result_array();
				$restro_booking = $query_restro -> result_array();
				if ($normal_booking[0]['booking_time'] > $restro_booking[0]['booking_time'])
				{
					return $normal_booking;
				}
				else
				{
					return $restro_booking;
				}
			}
			elseif ($query_normal -> num_rows() > 0)
			{
				return $normal_booking = $query_normal -> result_array();
			}
			elseif ($query_restro -> num_rows() > 0)
			{
				return $query_restro -> result_array();
			}
			else
			{
				return false;
			}
		}
        function driver_break_detail($driver_id)
		{
			$query =$this->db->query("SELECT type FROM `break` where driver_id=$driver_id order by id desc limit 1");
			if($query->num_rows()>0)
			{
				$data=$query->row_array();
				if($data['type']=="ENDED")
				{
					return "AVAILABLE";
				}else
					{
						return "BREAK";
					}
			}else
				{
					return "AVAILABLE";
				}
		}

	}
