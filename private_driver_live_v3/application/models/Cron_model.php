<?php

	class Cron_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		/* get pop up for complete booking */
		function Delete_cron()
		{
			$current_date = date('Y-m-d');
			$date = date_create($current_date);
			date_sub($date, date_interval_create_from_date_string("15 days"));
			$delete_date = date_format($date, "Y-m-d");
			$delete_date = $this -> db -> query("delete from logs where date(createdon)<'$delete_date'");
            echo $this->db->last_query();
		}

		function remaining_payment()
		{
			$today = gmdate("Y-m-d h:i:s");
		
			$where = array(
				//"date(booking_time)<=" => $today,
				"DATE_ADD(p.createdon, INTERVAL 10 MINUTE)<="=>$today,
				"b.status" => "TRIP_ENDED",
				"p.booking_type"=>"NORMAL",
				"description" =>"",
			);
			$this -> db -> select('b.id as booking_id,b.user_id,p.billing_amount,p.fix_amount');
			$this -> db -> from("cab_booking b");
			$this -> db -> join("payment_detail p", "p.booking_id=b.id");
			$this -> db -> where($where);
			// $this->db->limit("1");
			$query = $this -> db -> get();
			echo $this->db->last_query();
			//exit;
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}

		}
		
		function remaining_due_payment()
		{
			$today = gmdate("Y-m-d H:i:s");
		
			$where = array(
				//"date(booking_time)<=" => $today,
				"DATE_ADD(p.createdon, INTERVAL 10 MINUTE)<="=>$today,
				"b.status" => "TRIP_ENDED",
				"b.booking_type"=>"NORMAL",
				"description!=" =>"",
				"is_paid"=>"0",
			);
			$this -> db -> select('b.id as booking_id,b.user_id,p.billing_amount,p.fix_amount');
			$this -> db -> from("cab_booking b");
			$this -> db -> join("payment_detail p", "p.booking_id=b.id");
			$this -> db -> where($where);
			// $this->db->limit("1");
			$query = $this -> db -> get();
			echo $this->db->last_query();
			//exit;
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function ExpireCard()
		{

			$expair_date = date("m") . '/' . date("Y");
			$query = "select user.email from card_info join user on user.id=card_info.	customer_id where expire_date='$expair_date'";
			$res = $this -> db -> query($query);

			if ($res -> num_rows() > 0)
			{
				return $res -> result_array();
			}
			else
			{
				return false;
			}

		}
		
		

		function AuthPayment($type)
		{
			if($type==1){
			$query = $this -> db -> get_where('card_authorized', array("booking_id" => 0,"status"=>"authorized"));
			}elseif($type==2)
			{
				$query="SELECT a.*,b.is_prebooking,b.status,b.booking_time  FROM `card_authorized` a 
				join cab_booking b on b.id=a.booking_id WHERE a.`status` LIKE 'authorized' 
				and booking_id!=0 and date(booking_time)<curdate()";
				$query=$this->db->query($query);
			}
			
			//echo $this -> db -> last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}
		function UpdateVoidTransaction($voiddata)
		{
			$this->db->where("id",$voiddata['id']);
			$update_str=array("status"=>$voiddata['transaction_status'],"voidedon"=>get_gmt_time(),"message"=>$voiddata['message']);
			$update=$this->db->update("card_authorized",$update_str);
			//echo $this->db->last_query();
			if($update)
			{
				return true;
			}else
				{
					return false;
				}
			
			
		}

	}
?>
