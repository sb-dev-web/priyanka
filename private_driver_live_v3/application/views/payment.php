<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Payment Detail
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active"> Payment Detail</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
        
                  
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
             
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Booking No</th>
                        <th>Name</th>
                        <!--<th>Profile</th>-->
                        <th>Email</th>
                       <!-- <th>Gender</th>-->
                      
                       <!-- <th>Country</th>-->
                        <th>Total Amount</th>
                       
                        <th>Payment</th>
                       
                        <th>Email</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      
                      if(!empty($due_payment))
                      {
                       // echo sizeof($due_payment);
                        for($i=0;$i<sizeof($due_payment);$i++)
                        {
                          $name=$due_payment[$i]['name'];
                         
                      ?>
                      <tr>
                        <td><?php echo $due_payment[$i]['booking_number'];?></td>
                        <td><?php echo $name;?></td>
                        <td><?php echo $due_payment[$i]['email'];?></td>
                         <td><?php echo $due_payment[$i]['billing_amount'];?></td>
                        <!-- <td><a href="javascript:void(0);" onclick="changepaymentstatus('<?php echo $due_payment[$i]['id'];?>','loading_<?php echo $due_payment[$i]['id'];?>', 'span_<?php echo $due_payment[$i]['id'];?>')">
                                    <?php if($due_payment[$i]['is_paid']=='0'){?>
                                    <span id="span_<?php echo $due_payment[$i]['id'];?>" class="label label-warning">No</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $due_payment[$i]['id'];?>" class="label label-success">Yes</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $due_payment[$i]['id'];?>" style="display: none;">

                     </td>-->
                        <td><a href="javascript:void(0);"><span onclick="changepaymentstatus('<?php echo $due_payment[$i]['id'];?>');" class="label label-warning">No</span></a></td>
  
                        
                        
                                              
                      
                      <td>  <input id="btnShow" type="button" value="Send Mail" onclick="sendemail('<?php echo $due_payment[$i]['booking_number'].'_'.$name.'.pdf';?>');" /></td>
                      
                       
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      
                      else
                      {
                        
                        ?>
                        <tr>
                        <td><?php echo 'No data Found';?></td>
                        <td><?php echo 'No data Found';?></td>
                        <td><?php echo 'No data Found';?></td>
                        <td><?php echo 'No data Found';?></td>
                        <td><?php echo 'No data Found';?></td>
                        <td><?php echo 'No data Found';?></td>
                      
                     </tr>
                     <?php
                      }
                      ?>
                      
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
               
                   <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
              
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2015 <a href="#">Private Driver</a>.</strong> All rights reserved.
      </footer>
      
      <!-- Control Sidebar -->      
      
 <!--pop up for email  -->
 
 <div class="modal fade" id="open_emailbox" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width:519px;background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
               Mailing To Company
                <button style="color: white!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: 519px;max-height:155px; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="margin: 0px auto;width: 80%;">
                 
                <form id="sendmail" action="" method="post" autocomplete="off">
                    
                    <label for="emailid">Email Id</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                      <input type="type"  id="emailid" name="emailid"  size="45" placeholder="Enter Email Id"><br><br><br>
                                           <input type="hidden"  id="filename" name="filename"  size="45" placeholder="Enter File Name Ex.BookingNO_name"><br><br><br>
                
                    <div class="item" style="height: 80px!important;">
                      
                    </div>
                    
                    <div style="margin-top: -159px;margin-left:150px;">
                        <input  class="btn btn-info btn-lg" type="button" value="Send" id="send_email" name="send_email" style="width:59px;height:39px; "/>
                        <span id="loader_note" class="loader" style="display: none;">
                            <img src="http://dev.privatedriverapp.com/app/images/loader19.gif" alt=""/>
                        </span>
                    </div>
                </form>    
                </div>
            </div>
        </div> 
    </div>   
</div>
                 
 
      <!--ending pop up -->
 <!-- pop up for change payment status !-->  
 <div class="modal fade" id="open_payment" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: 490px; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
              Made Payment
                <button style="color: white!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: 490px;max-height:310px; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="margin: 18px auto;width: 80%;">
                 
                <form id="sendpstatus" action="" method="post" autocomplete="off">
                     <input type="hidden"  id="bookingno" name="bookingno">
                    <label for="description" style="margin-left:-11px;">Description</label><br>&nbsp;&nbsp;
                      <textarea rows="4" cols="38" name="description" id="description" style="margin-top:-45px;margin-left:70px;"required></textarea><br><br><br>
                      <label for="checkyes">Payment</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                      <input type="checkbox" name="pstatus" id="pstatus"  style="height:20px;"> <font size="5%">Yes</font><br>
  
                
                    <div class="item" style="height: 60px!important;">
                      
                    </div>
                    
                    <div style="margin-top:-48px;">
                        <input  class="btn btn-info btn-lg" type="button" value="Save" id="save_payment" name="save_payment" style="margin-left:140px;"/>
                        <span id="loader_note" class="loader" style="display: none;">
                            <img src="http://privatedriverapp.com/app/images/loader19.gif" alt=""/>
                        </span>
                    </div>
                </form>    
                </div>
            </div>
        </div> 
    </div>   
</div>  
 
 
 <!--end message !-->  
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>

<script type="text/javascript" src="<?=base_url();?>js/my_custom.js"></script>

  
      

<script>

function changepaymentstatus(id)
{
  
  $('#bookingno').val(id);
   $('#open_payment').modal({
        "backdrop" : "static"
         })
}

  $('#save_payment').click(function(){
    var c1 = document.getElementById('pstatus')
       
        //var payment_status=$('#pstatus').val();
        var description=$('#description').val();
        var bookingno=$('#bookingno').val();
        if (c1.checked)
        {
           var payment_status="1";  
        }
        else
        {
          var payment_status="0";
        }
        
       // alert(bookingno);
      
         validatepaymentstatus(payment_status,description,bookingno);
        });


/* send email to company */
 function sendemail(filename) {
         // alert(bid);
         //alert(filename);
         
          $('#filename').val(filename);
         $('#open_emailbox').modal({
                 "backdrop" : "static"
         })
             }
    
   $('#send_email').click(function(){
        var email=$('#emailid').val();
        var filename=$('#filename').val();
      
         validateemailissend(email,filename);
        });         
     
            
</script>
<script>
  
</script>

