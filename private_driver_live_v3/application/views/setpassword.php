<?php if($notallow==1)
{
    $formstyle="display: none";
    $errorstyle="";
}else
{
    $formstyle="";
    $errorstyle="display:none";
} ?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Private Driver</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500" />
        <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>css/login-form-elements.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/login-style.css" />


    </head>

    <body style="background: rgba(0, 0, 0, 0.3) none repeat scroll 0 0!important;>

        <!-- Top content -->
        <div class="top-content" ">
        	
            <div class="inner-bg">
                <div class="container">
                  <div class="logo-mini"><img /></div>
                    <div class="row">
                    
                        <div class="col-sm-6 col-sm-offset-3 form-box" id="form-data" style=" <?php echo $formstyle ?>">
							<div id="alert_login" style="display: none; font-weight: bold; background-color: #de615e; color: white; text-align: center; padding: 10px 0;">
                                Invalid Username or password !
                            </div><br />
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Endre passord for Private Driver</h3>
                            		<p>Skriv inn ditt nye passord:</p>
                        		</div>
                        		<div class="form-top-right" style="opacity: 1 !important">
                        			<img src="<?php echo base_url();?>images/logo95.png">
                        		</div>
                            </div>
                            <div class="form-bottom">
                            <?php //print_r($data); ?>
			                    <form role="form" action="" method="post" class="resetpassword-form">
			                    	<div class="form-group">`
			                    		<label class="sr-only" for="form-username">Email</label>
			                        	<input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username" value="<?php echo @$email ?>" readonly="">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Passord</label>
			                        	<input required="" type="password" name="form-password" placeholder="Passord..." class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn">Send</button>
                                    <input type="hidden" id="id" name="id" value="<?php echo $id ?>" />
                                    <input type="hidden" id="name" name="name" value="<?php echo $name ?>"/>
			                    </form>
		                    </div>
							<div id="loader" style="display:none; text-align:center;">
								<img src="<?php echo base_url(); ?>images/loader19.gif" />
							  </div>
                        </div>
                          <div class="col-sm-6 col-sm-offset-3 form-box" style="<?php echo $errorstyle ?>">
                          	<div class="form-top">
                        		<div class="form-top-left">
                        			
                            		<p>Linken er ikke lenger gyldig. Vennligst prøv igjen.</p>
                        		</div>
                        		<div class="form-top-right" style="opacity: 1 !important">
                        			<img src="<?php echo base_url();?>images/logo95.png">
                        		</div>
                            </div>
                          </div>
                            <div class="col-sm-6 col-sm-offset-3 form-box" id="success" style="display: none;">
                          	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Ditt passord er tilbakestilt</h3>
                            		<p>Vennligst logg inn med ditt nye passord</p>
                        		</div>
                        		<div class="form-top-right" style="opacity: 1 !important">
                        			<img src="<?php echo base_url();?>images/logo95.png">
                        		</div>
                            </div>
                          </div>
                          <div class="col-sm-6 col-sm-offset-3 form-box" id="Error" style="display: none;">
                          	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Opps! Server not support </h3>
                            		<p>Please try after sometime</p>
                        		</div>
                        		<div class="form-top-right">
                        			<img src="<?php echo base_url();?>images/logo500.png"height="80%" width="80%">
                        		</div>
                            </div>
                          </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-sm-6 col-sm-offset-3 social-login">
                        	<h3>...or login with:</h3>
                        	<div class="social-login-buttons">
	                        	<a class="btn btn-link-2" href="#">
	                        		<i class="fa fa-facebook"></i> Facebook
	                        	</a>
	                        	<a class="btn btn-link-2" href="#">
	                        		<i class="fa fa-twitter"></i> Twitter
	                        	</a>
	                        	<a class="btn btn-link-2" href="#">
	                        		<i class="fa fa-google-plus"></i> Google Plus
	                        	</a>
                        	</div>
                        </div>
                    </div>-->
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>plugins/backstretch/jquery.backstretch.min.js"></script>
        
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
        <script>
        var base_url = '<?php echo base_url();?>';        
        jQuery(document).ready(function() {
	            //Fullscreen background
               // $.backstretch(base_url+"images/backgrounds/tax_bg.jpg");
                
                /*
                    Form validation
                */
                $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
                	//$(this).removeClass('input-error');
                });
                
                $('.resetpassword-form').on('submit', function(e) {
                	e.preventDefault();
					e.preventDefault();
					
                	$(this).find('input[type="text"], input[type="password"], textarea').each(function(){
                		if( $(this).val() == "" ) {
                			$(this).addClass('input-error');
                		}
                		else {
                			$(this).removeClass('input-error');
							 var form_data=$('.resetpassword-form').serialize(); 
                                $.ajax({
                					 type: "POST",
                                    url: "<?php echo site_url('resetpassword/submit');?>",
                                    data: form_data,
                                    beforeSend: function(){
                                             //alert("Show loader");
                                             $('#loader').show();
                                          },
                                    success: function(data){
                						data=$.parseJSON(data)
                        			         $(".loader").hide();
                                			if(data.IsValid==1)
											{
                                                 $("#success").css("display","");
                                                 $("#form-data").css("display","none");
                                            }
											else{
                                                     $("#Error").css("display","");
                                				}
                                			}
                                });
								return false;
                		}
                	});
                	
                });
                
                
            });

        </script>
    </body>

</html>