<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->model('login_model');
        $this->load->library('session');
        $this->clear_cache();
    }
    
    /*function index()
    {
        $this->load->view('login');
    }*/

    function index()
    {
        $res = $this->is_logged_in();

        if ($res) {
            $this->success();
        } else {
            $this->load->view("login");
        }

    }

    function submit()
    {
        if (!$this->validate_login()) {
            $data = array("IsValid" => 0);
        } else {
            $data = array("IsValid" => 1);
        }
        echo json_encode($data);
    }

    

    
    
    
    function success()
    {
        //echo $this->session->userdata['logged_in'];
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $data['active_page']='index';
        $this->load->view('site-nav/header',$data);
        $this->load->view('index');
        $this->load->view('site-nav/footer');
    }

    function validate_login()
    {

        $result = $this->login_model->validate_login();
        if ($result == false) {
            return false;
        } else {

            $session_data = array(
                "id" => $result['id'],
                //"user_name" => $result['username'],
                "email"     => $result['email'],
                //"user_type" => $result['user_type'],
                "logged_in" => true);
            $this->session->set_userdata($session_data);
            return true;
        }

    }

    public function is_logged_in()
    {
        $user = $this->session->userdata('logged_in');
        return $user;
    }

    function logout()
    {
        $session_data = array(
            "email"     => "",
            "id" => "",
            'logged_in' => false);
        $this->session->unset_userdata($session_data);
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
    function edit_password()
    {
        $oldPass = $_POST['old_password'];
        $newPass = $_POST['new_password'];
        $confirmPass = $_POST['retype_password'];
        
        $userid = $this->session->userdata('id');
        $res=$resetPass=$this->login_model->resetPassword($userid,$oldPass,$newPass);
        echo $res;
    }
    
    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }
}
?>