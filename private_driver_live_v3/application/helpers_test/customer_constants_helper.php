<?php

define("ALREADY_REGISTER","Oops! It seems like you already registered.");

define("REGISTER_SUCCESS","Thanks for registration.");

define("EMAIL_NOT_EXIST","This email is not registered, please check the email address again.");

define("CUSTOMER_LOGIN_ERROR","Oops! Your email or password is wrong.Please try with correct information. ");

define("CUSTOMER_IMAGE_PATH","/images/user_profile/");

define("COMPANY_ACCOUNT_CREATE","Your account is created successfull.Need admin approval");

define("COMPANY_ALREADY_REGISTER","This Company have already registered.");

define("CUSTOMER_NOT_RESPOND","CUSTOMER_NOT_RESPOND");

define("NO_RIDE_EXIST","Opps! No any ride exist");

define("USER_NOT_EXIST","This user not exist Please login and use this app.");

define("CUSTOMER_CANCEL","Oops! This booking is cancelled by you.");

define("CUSTOMER_NOT_APPROVE","Customer not approve this booking.");

define("CUSTOMER_ALREADY_ACCEPTED","you already accepted this booking.");

define("CARD_INFO_MISSING","Please add your CC information.");

define("NO_OFFER_EXIST","Oops! Currently no any offer exist.");

define("PASSWORD_NOT_MATCH","oops! you old password not match.");

define("BRAINTREE_USER_ERROR","Oops! your new profile not saved with your CC info.");

define("CC_SUCCESS","Your credit card infomation updated successfully.");

define("PASSWORD_SUCCESS","Your password successfully updated.");

define("PROFILE_UPDATE","Your profile succseefully updated.");

define("COUPON_NOT_VAILD","Oops! This coupon is not vaild.");

define("COUPON_ALREADY_USED","Oops! you are already used this coupon.");


define("OTP_RESET_CODE","OTP successfully sent.");

define("ACCOUNT_VERIFY","Your account is already verified.");

define("NOT_APPROVE","Oops! Your Company user account need to approve by admin.");

define("EMAIL_NOT_VERIFY","Your Email address is not verified, Please verify your email address.");

define("APP_USER_MESSAGE","Your request accepted successfully. We will be in touch with you soon. ");

define("EMAIL_ALREADY_REGISTER","Oops! it seems like your <email> already has been registered. ");

define("MOBILE_ALREADY_REGISTER","Oops! it seems like this number <mobile> has already been registered. ");

define("APP_CODE_ALREADY_USED","Oops! This code already used Please make a new request for active code.");

define("APP_CODE_NOTEXIST","Your app code is not correct please check the app code.");

define("APP_USER_NOT_EXIST","Sorry your email id is not register for this app.");

define("FORGET_PASSWORD","We have sent you a link to reset Your password. Please check Your email");

define("FB_LOGIN","It looks like you previously have used Your Facebook account to log in. Try to log in With this");

define("EMAIL_NOT_EXIST_AS_COMPANYUSER","This email id already registered as a company user.");

define("MOBILE_NUMBER_VERIFIED","Your mobile number is successfully verified.");

define("USER_NOT_ACTIVE","Sorry your email id is not active mode.");

//define("ACOOUNT_NOTVERIFY","Your account is ")

?>