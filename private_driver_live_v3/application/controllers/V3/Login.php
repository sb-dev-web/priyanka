<?php defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Login extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library(array(
            'upload'));
        
        $this->load->helper(array('form','url','string'));
        $this->load->model('User_model');
    
    }
    

/* driver login function */
    function user_login()
    {
        $input_method = $this->webservices_inputs();
        $this->validate_param('login-driver', $input_method);
        $user_data = $this->User_model->check_login($input_method);
       
        if($user_data>0)
        {
            //echo 'hello';
        foreach($user_data as $user_detail)
        {
   $profile_image = ($user_detail->profile_pic == '') ? "" : base_url() .
                            '/images/profile/' . $user_detail->profile_pic;
                
                    $driver_id = $user_detail->driver_id;
                  $first_name=$user_detail->first_name;
                   $last_name=$user_detail->last_name;
                   $contact=$user_detail->contact;
                    $gender=$user_detail->gender;
                  
                    $zipcode=$user_detail->zipcode;
                    $city=$user_detail->city;
                    $email_id=$user_detail->email_id;
                   
               
                   

           $this->response(array('driver_id' => $driver_id,'status'=> 1), 200);
            }   
        }   
        
      else {
            $this->response(array('message' =>
                    'sorry no such a email exist, Please check your email again ', 'status' => 0),
                200);
        }
    }
/* uploading cab image when driver click */    
      function upload_cab_image()
      {
        $input_method = $this->webservices_inputs();
        $this->validate_param('cab-image', $input_method);
        if (isset($_FILES['cab_image']))
            {
                       
                if ($_FILES['cab_image']['error'] == 0)
                {
                   
                    $file_name = strtolower(random_string('alnum', 16));
                    $config['upload_path'] =  './images/shift';
                    $config['allowed_types'] = 'jpg|jpeg|gif|png';
                    $config['file_name'] = $file_name;
                   
                 
                  //$this->load->library('upload', $config);
                  
                       $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('cab_image'))
                    {
                        
                      //var_dump($_FILES);
                        $this->response(array('message' => $this->upload->display_errors(), 'status' =>
                                0), 200);
                    }
                    else
                    {
                        $data = array('upload_data' => $this->upload->data());
                        $input_method['image'] = $data['upload_data']['file_name'];
                         $user_data = $this->User_model->save_cab_image($input_method,$file_name);
                         if($user_data)
                         {
                             $this->response(array('message' =>
                             'your data is save ', 'status' => 1),200);
                         }
                         else
                         {
                           $this->response(array('message' =>
                           'something went wrong', 'status' => 0),200);  
                         }
                    }
                }
            }
      }
/* update driver password */

  function update_driver_password()
    {
        $input_method = $this->webservices_inputs();
        $this->validate_param('change-driver-password', $input_method);
        $user_data = $this->User_model->resetdriverPassword($input_method);
        if($user_data)
        {
         $this->response(array('message' =>
                           'your password is change successfully', 'status' => 1),200);     
        }
        else
        {
             $this->response(array('message' =>
                           'something went wrong', 'status' =>0),200);   
        }
}

/* shift start function */

 function shift_start()
{
        $input_method = $this->webservices_inputs();
        $this->validate_param('shift_started', $input_method);
        $user_data = $this->User_model->driver_shift($input_method);
        if($user_data==1)
        {
         $this->response(array('message' =>
                           'your today shift started already', 'status' => 0),200);     
        }
        elseif($user_data==2)
        {
             $this->response(array('message' =>
                           'cab is already assign please choose another cab', 'status' =>0),200);   
        }
       
          elseif($user_data==4)
        {
             $this->response(array('message' =>
                           'please click image of cab', 'status' =>0),200);   
        }
         elseif($user_data)
        {
             $this->response(array('shift_id' => $user_data, 'message' =>
                           'Your shift is started','status' =>1),200);   
        }
        else
        {
         $this->response(array('message' =>
                           'something went wrong', 'status' =>0),200);      
        }
}

/* fetch available cab */
function cab_list()
{
   $cab_data = $this->User_model->fetch_cab();
   if($cab_data)
   {
    
      foreach($cab_data as $cab)
        {
          $cab_detail[] = array( 
                        "cab_id" => $cid = $cab->type_id,
                        "cab_name"=>$cab_name=$cab->title
                        );       
                
        }
     $this->response(array('cab' =>$cab_detail, 'status' => 1), 200);
    }
    else
    {
       $this->response(array('message' =>'no cab availabel', 'status' =>0),200);       
    }
}

/* shift start_break,end_break,end shift functions */
function shifts_end()
{
  
        $input_method = $this->webservices_inputs();
        $this->validate_param('shift_end', $input_method);
        $type=$input_method['type'];
        if($type=='ES')
        {
               $end_shift = $this->User_model->end_shift($input_method);
               if($end_shift)
               {
                $this->response(array('message' =>'Your shift is end successfully', 'status' =>1),200);    
               }
               else
               {
                 $this->response(array('message' =>'something went wrong', 'status' =>0),200);  
               }
        }
        elseif($type=='SB')
        {
             $start_break = $this->User_model->start_break($input_method);
             if($start_break)
               {
                $this->response(array('message' =>'Your break is started now', 'status' =>1),200);    
               }
               else
               {
                 $this->response(array('message' =>'no shift found', 'status' =>0),200);  
               }
        }
        elseif($type=='EB')
        {
          $end_break = $this->User_model->end_break($input_method);
             if($end_break)
               {
                $this->response(array('message' =>'Your break is end now', 'status' =>1),200);    
               }
               else
               {
                 $this->response(array('message' =>'no shift found', 'status' =>0),200);  
               }  
        }
        else
        {
          $this->response(array('message' =>'something went wrong', 'status' =>0),200);     
        }
}

/* get driver infomation */
function get_driver()
{
   $input_method = $this->webservices_inputs();
  $this->validate_param('driver_info', $input_method);
   $driver_data = $this->User_model->get_driver_info($input_method);
   if($driver_data>0)
   {
  foreach($driver_data as $user_detail)
{
   $profile_image = ($user_detail->profile_pic == '') ? "" : base_url() .
                            '/images/profile/' . $user_detail->profile_pic;
                
                    $driver_id = $user_detail->driver_id;
                  $first_name=$user_detail->first_name;
                   $last_name=$user_detail->last_name;
                   $contact=$user_detail->contact;
                    $gender=$user_detail->gender;
                   $country=$user_detail->country;
                  $state=$user_detail->state;
                    $city=$user_detail->city;
                    $email_id=$user_detail->email_id;
                    $license_no=$user_detail->license_no;
  
}
 $cab_assign = $this->User_model->get_assign_cab($input_method);
 if($cab_assign>0)
 {
    
                
        $cab_plate= $cab_assign[0]->cab_plate_no; 


}
else
{
    $cab_plate='';
}
  $this->response(array('driver_id' => $driver_id,'first_name'=>$first_name,'last_name'=>$last_name,'contact'=>$contact ,'gender'=>$gender,'country'=>$country,'state'=>$state,'city'=>$city,'email_id'=>$email_id,'license_no'=>$license_no,'profile_image'=>$profile_image,'cab_plate'=>$cab_plate,'status'=> 1), 200);
   }
   else
   {
     $this->response(array('message' =>'something went wrong', 'status' =>0),200);
   }
}
/* update driver location*/
function update_driver_location()
{
     $input_method = $this->webservices_inputs();
 $this->validate_param('update-location', $input_method);
$driver_data = $this->User_model->current_driver_location($input_method);    
}
}