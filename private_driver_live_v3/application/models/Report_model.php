<?php
class Report_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }
	
	function get_customer()
	{
		//$company_id=$this -> session -> userdata('company_id');
		$query =$this->db->query("SELECT id,name FROM `user`");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}else
			{
				return false;
			}
		
	}
	
	function BookingDetail($date,$id)
	{
			
		$company_id=$this -> session -> userdata('company_id');	
		$date_arr=explode("to", $date);
		if($id!='ALL')
		{
			$con="and cab_booking.user_id=$id";
		}
		 $sql = "SELECT user.name,cab_booking.id,booking_number,pickup_location,destination_location,concat(driver.first_name,driver.last_name)driver,booking_time,cab_booking.status,payment_detail.billing_amount,payment_detail.pdf_url,payment_detail.description  
			FROM `cab_booking` 
			join user on user.id=cab_booking.user_id
			left join payment_detail on payment_detail.booking_id=cab_booking.id
			left join driver on driver.driver_id=cab_booking.driver_id where 1  $con and date(booking_time) between '".$date_arr[0]."' and '".$date_arr[1]."' order by cab_booking.booking_time desc ";
		$query=$this->db->query($sql);
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}else
			{
				return false;
			}
	}

   
    
   
}