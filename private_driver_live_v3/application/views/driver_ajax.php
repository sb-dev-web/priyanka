<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
 <div class="box-body record">
              <br>
                  <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <!--<th>Profile</th>-->
                        <th>Email</th>
                       <th>Rating</th>
                      
                       <!-- <th>Country</th>-->
                    
                        <th>Cab&nbsp;Assigned</th>
                        <th>Driver Location</th>
                         <th>Login&nbsp;Status</th>
                          <th>Shift&nbsp;Status</th>
                        <th>Status</th>
                        <th>Current Activity</th>
                        <th style="width: 10%; text-align: center;">Action</th>
                      
                      
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(sizeof($driver))
                      {
                       
                        for($i=0;$i<sizeof($driver);$i++)
                        {
                          $name=$driver[$i]->first_name.' '.$driver[$i]->last_name;
                          $cab_type=$driver[$i]->cab_type;
                          if($cab_type=='1')
                          {
                            $type='First';
                             }
                             else if($cab_type=='2')
                             {
                              $type='Business';
                             }
                             elseif($cab_type=='3')
                             {
                              $type='Luxury';
                             }
                             else
                             {
                              $type='';
                             }
                          
                      ?>
                      <tr>
                        <td><?php echo $name;?></td>
                        <!--<td><?php echo $driver[$i]->profile_pic;?></td>-->
                         <td><?php echo $driver[$i]->email_id;?></td>
                          <td><?php echo $DriverRating[$driver[$i]->driver_id] ?></td>
                      
                        <td><?php
                        if($driver[$i]->cab_model=='')
                        {
                          $cab='No Cab';
                          echo $cab;
                        }
                        else
                        {
                        $cab=$driver[$i]->cab_model;
                         echo $cab.'('.$type.')';
                        }
                       
                        ?></td>
                       
                       <!-- <td><?php echo $driver[$i]->country;?></td>-->
                        <!--<td><?php echo $driver[$i]->state;?></td>-->
                       
                        
                        <td>
                       <input id="btnShow" type="button" value="Show On Map" onclick="pop_upmap('<?php echo $driver[$i]->latitude;?>','<?php echo $driver[$i]->longitude;?>');" /></td>
                      <td><a href="javascript:void(0);" onclick="changeloginstatus('<?php echo $driver[$i]->driver_id;?>','loading1_<?php echo $driver[$i]->driver_id;?>', 'span1_<?php echo $driver[$i]->driver_id;?>')">
                                    <?php if($driver[$i]->islogin=='Y'){?>
                                    <span id="span1_<?php echo $driver[$i]->driver_id?>" class="label label-success">Logged In</span></a>
                                    <?php }else{?>
                                    <span id="span1_<?php echo $driver[$i]->driver_id?>" class="label label-warning">Logged Out</span></a>
                                    <?php }?>
                                    <br /><div id="loading1_<?php echo $driver[$i]->driver_id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                     </td>
                      
                       <!-- change shift status !-->
                      
                       <td>
                                    <?php if($driver[$i]->shift_status=='STARTED'){?>
                                   <a href="javascript:void(0);" onclick="changeshiftstatus('<?php echo $driver[$i]->s_id;?>','loading_<?php echo $driver[$i]->s_id;?>', 'span_<?php echo $driver[$i]->s_id;?>')">
                                    <span id="span_<?php echo $driver[$i]->s_id?>" class="label label-success">Started</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $driver[$i]->s_id?>" class="label label-warning">Ended</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $driver[$i]->s_id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                     </td>
                      
                      
                       <td><a href="javascript:void(0);" onclick="changeStatus('<?php echo $driver[$i]->driver_id;?>','loading_<?php echo $driver[$i]->driver_id;?>', 'span_<?php echo $driver[$i]->driver_id;?>')">
                                    <?php if($driver[$i]->isactive=='0'){?>
                                    <span id="span_<?php echo $driver[$i]->driver_id?>" class="label label-warning">Inactive</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $driver[$i]->driver_id?>" class="label label-success">Active</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $driver[$i]->driver_id;?>" style="display: none;"><img src="<?php echo base_url();?>/images/loader19.gif" /></div>

                     </td>
                       <td><?php  $check_status=$driver[$i]->status;
                        if($check_status=='LOGOUT')
                       {
                        echo 'Logged Out';
                       }
                       else if($check_status=='BREAK')
                       {
                        echo 'On Break';
                       }
                       else if($check_status=='RESTRO_BUSY')
                       {
                        echo 'Busy';
                       }
                         else if($check_status=='AVAILABLE')
                       {
                        echo 'Available';
                       }
                         else if($check_status=='LOGIN')
                       {
                        echo 'Logged In';
                       }
                         else if($check_status=='LOGIN')
                       {
                        echo 'Logged In';
                       }
                         else if($check_status=='TEMP_HOLD')
                       {
                        echo 'On Hold';
                       }
                        else if($check_status=='UN_AVAILABLE')
                       {
                        echo 'Not Availabel';
                       }
                       
                       ?></td>
                       <td>
                        
                       <a href="<?=site_url('Driver/add_driver').'/'.$driver[$i]->driver_id; ?>" title="Edit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-pencil-square-o fa-2x"></a></i>
                       <img src="<?php echo base_url();?>images/loader19.gif" id="image_<?=$driver[$i]->driver_id;?>" style="display: none;"/>
                    |&nbsp;<?php //echo $driver[$i]->isdelete=0; ?>
                       <a href="javascript:void(0);" onclick="changedeleteStatus('<?php echo $driver[$i]->driver_id;?>','delloading_<?php echo $driver[$i]->driver_id;?>', 'spandel_<?php echo $driver[$i]->driver_id;?>')">
                                    <?php if($driver[$i]->isdelete=='0'){?>
                                    <span id="spandel_<?php echo $driver[$i]->driver_id?>" class="fa fa-trash fa-2x" style="color:red;"></span></a>
                                    <?php }?>
                                    <br /><div id="delloading_<?php echo $driver[$i]->driver_id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

</td>
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      ?>
                      
                      
                      
                    </tbody>
                    <tfoot>
                       
                    </tfoot>
                  </table>
                  <?php
                  
                   ?>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
        
          </div>
                </div>