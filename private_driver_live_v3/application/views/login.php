<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Private Driver</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500" />
        <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css" />
        <!--<link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/font-awesome.min.css" />-->
		<link rel="stylesheet" href="<?php echo base_url();?>css/login-form-elements.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/login-style.css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <!--<link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png" />-->

    </head>
 <!--<div style="height:120px;width:100%;background-color:#f39c12; overflow:hidden;position: absolute;  z-index:0"><img src="http://localhost/latest_private_driver/app/images/logo500.png" height="90%" width="10%" style="float:left;"></div>
    --><body  background="taxi.jpg" style="overflow:hidden;">
  
        <!-- Top content -->
        <div class="top-content" style=" background-image: url("taxi.jpg");">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        
                    </div>
            <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
							<div id="alert_login" style="display: none; font-weight: bold; background-color: #de615e; color: white; text-align: center; padding: 10px 0;">
                                Invalid Username or password !
                            </div><br />
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login to Private Driver</h3>
                            		<p>Enter your username and password to log on:</p>
                        		</div>
                        		<div class="form-top-right" style="opacity: 7.0;">
                        			<img src="<?php echo base_url();?>images/logo500.png"height="80%" width="80%">
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="" method="post" class="login-form">
			                    	<div class="form-group">`
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn">Sign in!</button>
			                    </form>
		                    </div>
							<div id="loader" style="display:none; text-align:center;">
								<img src="<?php echo base_url(); ?>images/loader19.gif" />
							  </div>
                        </div>
                    </div>
                   
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="<?php echo base_url(); ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>plugins/backstretch/jquery.backstretch.min.js"></script>
        
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
        <script>
        var base_url = '<?php echo base_url();?>';        
        jQuery(document).ready(function() {
	            //Fullscreen background
                //$.backstretch(base_url+"images/backgrounds/tax_bg.jpg");
                
                /*
                    Form validation
                */
                $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
                	$(this).removeClass('input-error');
                });
                
                $('.login-form').on('submit', function(e) {
                	e.preventDefault();
					e.preventDefault();
					
                	$(this).find('input[type="text"], input[type="password"], textarea').each(function(){
                		if( $(this).val() == "" ) {
                			$(this).addClass('input-error');
                		}
                		else {
                			$(this).removeClass('input-error');
							 var form_data=$('.login-form').serialize(); 
                                $.ajax({
                					 type: "POST",
                                    url: "<?php echo site_url('login/submit');?>",
                                    data: form_data,
                                    beforeSend: function(){
                                             //alert("Show loader");
                                             $('#loader').show();
                                          },
                                    success: function(data){
                						data=$.parseJSON(data)
                        			         $(".loader").hide();
                                			if(data.IsValid==0)
											{
                                                  $('#loader').hide();
												  //alert("Failed to Login");
												  $('#alert_login').show().fadeOut(5500);
													$('.login-form')[0].reset();
                                            }
											else{
                                                     window.location='<?php echo site_url("welcome/onsuccess"); ?>';
                                				}
                                			}
                                });
								return false;
                		}
                	});
                	
                });
                
                
            });

        </script>
    </body>

</html>