<?php

	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	/* for push notification Iphone */
	function send_push($device_token_i, $message_text, $otherdata)
	{

		require_once APPPATH . 'third_party/ApnsPHP-master/ApnsPHP/Autoload.php';
		$push = new ApnsPHP_Push(ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, APPPATH . 'third_party/certificate/PD.pem');
		// Set the Root Certificate Autority to verify the Apple remote peer
		$push -> setRootCertificationAuthority(APPPATH . 'third_party/certificate/entrust_root_certification_authority.pem');

		// Instantiate a new Message with a single recipient
		$message = new ApnsPHP_Message();
		// echo "<pre>";
		//print_r($device_token_i);
		foreach ($device_token_i as $val)
		{
			//print_r($val);

			$push -> connect();
			$message -> addRecipient($val['token']);
			//$message->addRecipient('42c9c8df45f04de75c26f145f51d0ae9d9da426b131299ac7b439fc1680f250a');
			//$message_text=$message_text."UserId=>".$val['user_id'];

			$message -> setText($message_text);
			if (count($otherdata))
			{
				foreach ($otherdata as $key => $value)
				{
					//echo $key . $value;
					$message -> setCustomProperty($key, $value);
				}
			}

			//$message->setCustomIdentifier($otherdata);

			// Play the default sound
			$message -> setSound();
			$badge = (int)$val['badge'];
			$message -> setBadge($badge);

			// Add the message to the message queue
			$push -> add($message);
			
			$push -> send();
			
			//print_r($message);
			
			

			// Disconnect from the Apple Push Notification Service
			$push -> disconnect();

			// Examine the error message container
			// $aErrorQueue = $push->getErrors();
			if (!empty($aErrorQueue))
			{

				var_dump($aErrorQueue);
				//exit ;
			}
			else
			{
				//echo "success";
			}
		}

	}

	function send_push_Coupon($device_token_i, $message_text, $otherdata)
	{

		require_once APPPATH . 'third_party/ApnsPHP-master/ApnsPHP/Autoload.php';

		$push = new ApnsPHP_Push(ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, APPPATH . 'third_party/certificate/PD.pem');

		// Set the Root Certificate Autority to verify the Apple remote peer
		$push -> setRootCertificationAuthority(APPPATH . 'third_party/certificate/entrust_root_certification_authority.pem');

		$push -> connect();

		// Instantiate a new Message with a single recipient
		$message = new ApnsPHP_Message();

		//print_r($device_token);
		foreach ($device_token_i as $val)
		{
			// echo 'device'.$device_token_i;
			// echo $val;
			$message -> addRecipient($val);
		}

		$message -> setText($message_text);

		//$message->setCustomIdentifier($otherdata);

		// Play the default sound
		$message -> setSound();
		//$badge = (int)$val['badge'];
		//$message -> setBadge($badge);

		// Set the expiry value to 30 seconds
		//$message->setExpiry(86400);

		// Add the message to the message queue
		$push -> add($message);
		if (count($otherdata))
		{
			foreach ($otherdata as $key => $value)
			{
				$push -> setCustomProperty($key, $value);
			}
		}

		// Send all messages in the message queue
		$push -> send();
		// Disconnect from the Apple Push Notification Service
		$push -> disconnect();

		// Examine the error message container
		// $aErrorQueue = $push->getErrors();
		if (!empty($aErrorQueue))
		{
			//temp commented by raj
			//var_dump($aErrorQueue);
			exit ;
		}
		else
		{
			echo "success";
		}

	}

	/*for push notification andorid */
	/* for send andorid notification */
	function send_andorid_push($message, $registrationIDs)
	{
		// print_r($registrationIDs);
		$apiKey = "AIzaSyD3jrA7Wv96MVIR58RbhBisnturIqu0uY8";
		$url = 'http://android.googleapis.com/gcm/send';

		foreach ($registrationIDs as $registerId)
		{
			$fields = array(
				'registration_ids' => array($registerId['token']),
				'data' => array(
					"message" => $message,
					"tag" => $registerId['badge'],
					"title" => $registerId['type']
				),
			);
			//print_r($fields);
			//echo json_encode($fields);
			$headers = array(
				'Authorization: key=' . $apiKey,
				'Content-Type: application/json'
			);

			// Open connection
			$ch = curl_init();

			// Set the URL, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt($ch, CURLOPT_POST, true);
			// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

			// Execute post
			$result = curl_exec($ch);

			curl_close($ch);

		}
		//echo $result;

		return true;
	}

	function send_andorid_push_coupon($message, $registrationIDs)
	{
		// print_r($registrationIDs);
		$apiKey = "AIzaSyD3jrA7Wv96MVIR58RbhBisnturIqu0uY8";
		$url = 'http://android.googleapis.com/gcm/send';

		//foreach ($registrationIDs as $registerId)
		{
			$fields = array(
				'registration_ids' => $registrationIDs,
				'data' => array("message" => $message,
					//"tag" => $registerId['badge']
				),
			);
			//echo json_encode($fields);
			$headers = array(
				'Authorization: key=' . $apiKey,
				'Content-Type: application/json'
			);

			// Open connection
			$ch = curl_init();

			// Set the URL, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// curl_setopt($ch, CURLOPT_POST, true);
			// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

			// Execute post
			$result = curl_exec($ch);

			curl_close($ch);
		}
		//echo $result;

		return true;
	}
?>
