<?php

	class Appuser_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		function Add_appuser($input_method)
		{
			$data = array(
				"name" => $input_method['name'],
				"email" => $input_method['email'],
				"phone" => $input_method['phone'],
				"appcode" => $input_method['code']
			);
			//make inactive if same user apply again and agin
			$update_str = array("isactive" => "0");
			$this -> db -> where("email", $input_method['email']);
			$this -> db -> update("app_user", $update_str);
			$app_user = $this -> db -> insert("app_user", $data);
			if ($this -> db -> insert_id())
			{
				return $this -> db -> insert_id();
			}
			else
			{
				return false;
			}
		}

		function App_usercode($input_method)
		{
			//$email = $input_method['email'];
			$code = $input_method['code'];
			//chk this email active or not
			$query = $this -> db -> query("select * from app_user where  appcode='$code' and isapprove='1' ");
			if ($query -> num_rows() > 0)
			{
				return $query -> row_array();
			}
			else
			{
				   return false;
				/*$query = $this -> db -> query("select * from app_user where email='$email' ");
				if ($query -> num_rows() > 0)
				{
					return $query -> row_array();
				}
				else
				{
					return false;
				}*/

			}
		}

		function Appcode_deactive($appuser_id)
		{
			$this -> db -> query("update app_user set isactive=0 where id=$appuser_id");
		   // echo $this->db->last_query(); 
		}

	}
?>