<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Company extends CI_Controller
{

    function __construct()
    {
         parent::__construct();
        $this->load->helper(array(
            'form',
            'url',
            'function_helper',
			'braintree_function'));
        $this->load->model('Company_model');
        $this->load->library(array(
            'session',
            'email',
            'paginationlib',
            'Braintree_lib',
            'Pagination'));
       
         $user = $this->session->userdata('logged_in');
        if (!$user)
        {
            redirect('Welcome');
        }

    }
    

    public function index()
    {
       // $this->load->library('Ajax_pagination');
        // $this->Company_model->company_count();
        // $pagingconfig = $this->paginationlib->initPagination("/Company/ajaxPaginationData/", $this->
            // Company_model->company_count(), 10,3,array());
// 
// 
        // $this->ajax_pagination->initialize($pagingconfig);
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //$data['link'] = $this->pagination->create_links();
        // $data['link'] = $this->ajax_pagination->create_links();
        $company = $this->Company_model->fetch_total_company();
       
        $data['company'] = $company;
        $data['active_page'] = 'company';
        $this->load->view('site-nav/header', $data);
        $this->load->view('company');
        $this->load->view('site-nav/footer');
    }
    function ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/company/ajaxPaginationData/", $this->
            Company_model->driver_count(), 10, $page['post']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['driver'] = $this->Company_model->fetch_total_company($pagingconfig['per_page'],
            $page);

        //load the view
        $this->load->view('company_user_ajax', $data, false);
    }
    public function perPage()
    {
        $this->load->library('Ajax_pagination');
        return 2;
    }

   
    public function add_company()
    {
        //$driver=$this->Company_model->add_driver();
        //print_r($driver);
        //$data['driver']=$driver;
        @$company[0]->company_name =$company[0]->phone=$company[0]->email =$company[0]->address="";
        $company = "";
        if ($this->uri->segment(3) == false)
        {

            $id = 0;

        }
        else
        {
            $id = $this->uri->segment(3);
            $company = $this->Company_model->get_company($id);
        }
        $data['company'] = $company;
        $data['active_page'] = 'company';
        $this->load->view('site-nav/header', $data);
        $this->load->view('add_company');
        $this->load->view('site-nav/footer');
    }
	
	function VerifyBtAccount()
	{
		$credit_card_no=$_POST['cc_number'];
		$cvv_no=$_POST['cvv'];
		$month=$_POST['month'];
		$year=$_POST['year'];
		$name=$_POST['name'];
		$email=$_POST['email'];
		$phone=$_POST['phone'];
		$data = array(
					"name" => $name,
					"email" => $email,
					"phone" => $phone,
					"ccnumber" => $credit_card_no,
					"cvv_no" => $cvv_no,
					"exp_date" => $month.'/'.$year
				);
				//if ($credit_card_no != '4111111111111112')
				{
					$braintree_response = Create_withCreditCardAndVerification($data);
				}
				//print_r($braintree_response);
				//if($braintree_response['status']==1)
				{
					//save the detail in db 
					//$this->Comapny_model->SaveCCinfo($) 
					echo json_encode($braintree_response);
					
				}
	}
	
	function DeleteCC()
	{
		$customer_id=$_POST['id'];
		$deleteCrad =$this->Company_model->DeleteCompanyCard($customer_id);
		if($deleteCrad)
		{
			//delete from BT 
			
			DeleteCustomer($deleteCrad);
			$result=array("status"=>"1","message"=>"Comapny deleted found");
			echo json_encode($result);
			
			
		}else
			{
				 $result=array("status"=>"0","message"=>"Comapny Card Not found");
				 echo json_encode($result);
			}
	}

    public function submit_company()
    {
        $id = $_POST['id'];
        $res = $this->Company_model->save_company($id);
        if ($res)
        {
           	if(!$id){	
           	//send mail to compnay user 
           	$email=$_POST['email'];
           	SendMailToCompnay($email);
			}
           echo 1;

        }
        else
        {
            echo 0;
        }
        //echo $res;
    }
    function change_delete_status()
    {
        $id = $_POST['id'];
        echo $this->Company_model->delete_company($id);
    }
    
    /* get comapny user */
    function Get_company_user()
    {
        $company_id=$_POST['company_id'];
        $data['company_data']=$this->Company_model->get_company_user($company_id);
       
        $this->load->view('company_user_report', $data);
    }
    
    function check_email()
    {
        $email=$_POST['email_id'];
        echo $already_email=$this->Company_model->check_email($email);
    }

}
