<?php
class Appuseradmin_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }
    
    /*count app user */
     function app_user_count()
    {
         $query=$this->db->query("select * from app_user");
               return $query->num_rows(); 
    }
    /* fetch app user infomation */
    function Fetch_cuser_info($limit,$offset=0)
    {
         $query=$this->db->query("select * from app_user order by id DESC limit $offset,$limit");
       // echo $this->db->last_query();
        return $query->result(); 
    }
    

    /*appuser company */
    function get_app_user()
    {
        $get_company=$this->db->query("select * from app_user");
        $count=$get_company->num_rows();
        if($count>0)
        {
            return $get_company->result_array();
        }
        else
        {
            return array();
        }
    }
    
    /*appuser company */
    function get_appuser_approvecode($id)
    {
        $get_company=$this->db->query("select * from app_user where id='$id'");
        $count=$get_company->num_rows();
        if($count>0)
        {
            return $get_company->result_array();
        }
        else
        {
            return array();
        }
    }
    
    
    
     /* actrive app user */
    function Active_appuser($id,$status)
    {
      if($status=="1")
        {
            $field_array=array("isactive"=>"1");
            $status=='1';
        }
        else
        {
            $field_array=array("isactive"=>"0");
            $status=='0';
        }
        $this->db->where('id',$id);
        $this->db->update("app_user",$field_array);
        return $status;  
    }
    
    /* actrive app user */
    function Approve_appuser($id,$status)
    {
      if($status=="1")
        {
            $field_array=array("isapprove"=>"1");
            $status=='1';
        }
        else
        {
            $field_array=array("isapprove"=>"0");
            $status=='0';
        }
        $this->db->where('id',$id);
        $this->db->update("app_user",$field_array);
        return $status;  
    }
    
    	/* delete appuser */
	function delete_appuser($id)
    {

		$query = $this -> db -> query("delete from app_user where id='$id'");
		if ($this -> db -> affected_rows() > 0)
			return 1;
    }
    

}