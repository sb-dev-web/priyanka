<?php 
/*echo "is_emailverify=>".$is_emailverify;
echo "<br>"."phone=>".$phone_verify;
echo "<br>username=>".$name;*/
if($notallow==1)
{
    $successdiv="display: none";
	$alreadyverifydiv="display:none";
    $errordiv="";
}elseif($is_emailverify==1)
{
    $successdiv="display: none";
	$alreadyverifydiv="display:";
    $errordiv="display:none";
}else
	{
		$successdiv="display: ";
	$alreadyverifydiv="display:none";
    $errordiv="display:none";
	} ?>
<!DOCTYPE html>
<style>
	
.bounce {
	/*position: absolute;
	
	margin-left:-30px;
	
	
	animation: bounce 2s infinite;
	-webkit-animation: bounce 2s infinite;
	-moz-animation: bounce 2s infinite;
	-o-animation: bounce 2s infinite;*/
}
 
@-webkit-keyframes bounce {
	0%, 20%, 50%, 80%, 100% {-webkit-transform: translateY(0);}	
	40% {-webkit-transform: translateY(-30px);}
	60% {-webkit-transform: translateY(-15px);}
}
 
@-moz-keyframes bounce {
	0%, 20%, 50%, 80%, 100% {-moz-transform: translateY(0);}
	40% {-moz-transform: translateY(-30px);}
	60% {-moz-transform: translateY(-15px);}
}
 
@-o-keyframes bounce {
	0%, 20%, 50%, 80%, 100% {-o-transform: translateY(0);}
	40% {-o-transform: translateY(-30px);}
	60% {-o-transform: translateY(-15px);}
}
@keyframes bounce {
	0%, 20%, 50%, 80%, 100% {transform: translateY(0);}
	40% {transform: translateY(-30px);}
	60% {transform: translateY(-15px);}
}
p{
	font-size:22px;
	font-weight: bolder;
}
</style>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Private Driver</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500" />
        <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>css/login-form-elements.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>css/login-style.css" />


    </head>

    <body style="background: rgba(0, 0, 0, 0.3) none repeat scroll 0 0!important;>

        <!-- Top content -->
           <div class="top-content" style="">
        	
            <div class="inner-bg" >
                <div class="container">
                  
                  <!--  <div class="row">-->
                  	<div class="bounce ">
                    
                        <div class="col-sm-6 col-sm-offset-3 form-box" style="width: 550px; <?php echo $alreadyverifydiv ?>">
                          	<div class="form-top">
                        		<div class="form-top-left">
                        			<!-- <h3>Already Verified </h3> -->
                            		<p>  Din e-post er allerede verifisert   </p>
                        		</div>
                        		<div class="form-top-right" style="opacity: 1 !important;">
                        			<img src="<?php echo base_url();?>images/logo95.png">
                        		</div>
                            </div>
                          </div>
                            <div class="col-sm-6 col-sm-offset-3 form-box" id="success" style=" width: 550px; <?php echo $successdiv ?>">
                          	<div class="form-top">
                        		<div class="form-top-left">
                        			<!-- <h3>Email Account Verify</h3> -->
                            		<p> E-posten er verifisert. </p>
                        		</div>
                        		<div class="form-top-right" style="opacity: 1 !important;">
                        			<img src="<?php echo base_url();?>images/logo95.png">
                        		</div>
                            </div>
                          </div>
                          <div class="col-sm-6 col-sm-offset-3 form-box" id="Error" style="width: 550px;<?php echo $errordiv ?>">
                          	<div class="form-top">
                        		<div class="form-top-left">
                        			<!-- <h3>Opps! Error </h3> -->     
                        			<p>Vennligst sjekk din verifiseringslink og prøv igjen </p>
                        		</div>
                        		<div class="form-top-right" style="opacity: 1 !important;">
                        			<img src="<?php echo base_url();?>images/logo95.png">
                        		</div>
                            </div>
                          </div>
                    </div>
                   
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>plugins/backstretch/jquery.backstretch.min.js"></script>
        
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>