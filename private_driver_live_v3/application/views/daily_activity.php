<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Driver Activity
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Driver Activity</li>
          </ol>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         
                   
          
          
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"> Driver Activity</h3>
             
             
            </div>
            <div class="box-body">
                
             <label>Date</label>
            <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>                                              
              </div>
            <input type="text" class="form-control pull-left" id="bd" name="bd" onchange="getbookingdate();" style="width:25%;"/>
           
            
            </div>
            <br/> <br/>
            <div id="data1">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr No</th>
                        <th>Name</th>
                        <th>Total Accept</th>
                        <th>Total Cancelled</th>
                        <th>Total Finish</th>
                        <th>Advance Booking</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                 
                    //exit;
                   
                    for ($i = 0; $i < count($booklist); ++$i) { ?>
                    <tr>
                        <td><?php echo ($page+$i+1); ?></td>
                        <?php $name=$booklist[$i]->first_name.' '.$booklist[$i]->last_name;?>
                        <td><?php echo $name; ?></td>
                        <td><?php echo ($booklist[$i]->driveraccept ? $booklist[$i]->driveraccept:0); ?></td>
                        <td><?php echo ($booklist[$i]->cancelled ? $booklist[$i]->cancelled:0);?></td>
                        <td><?php echo ($booklist[$i]->tripend ? $booklist[$i]->tripend:0);?></td>
                        <td><?php echo ($booklist[$i]->advancebooking ? $booklist[$i]->advancebooking:0);?></td>
                    </tr>
                    <?php } ?>
                      
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $pagination; ?>
                    </div>
        
          </div><!-- /.box -
                  </div>
                </div>-->
           </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy;2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
      <!--<script type="text/javascript" src="<?=base_url();?>js/bootstrap-datepicker.js"></script>-->
<script>
     $(function() {
                    $( "#bd" ).datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true
                       
                        });
                   
                      });

</script>
<script>
    function  getbookingdate()
    {
        var booking_date=$('#bd').val();
         $.ajax({
            type:'POST',
            url: '<?php echo site_url('Activity/driver_booking_date');?>',
            data:{"booking_date":booking_date},
           
            success: function(response) {
              
                 $("#data1").html(response);
              
             }});
        
    }
    
    
</script>