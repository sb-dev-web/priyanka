<link rel="stylesheet" href="<?php echo base_url("plugins/bootstrap-datetimepicker/css/datetimepicker.css"); ?>">

<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Phone Booking</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Phone Booking</a></li>
      <li class="active">Add Booking</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="save_item" action="" method="post" enctype="multipart/form-data" autocomplete="off">
             <input type="hidden" value="20"  name="company_id" id="company_id" />
             <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Booking Type</label>
                      <select class="form-control select2-container" id="booking_type" name="booking_type" style="width: 175px">
                     	<option value="customer">Customer</option>
                     	<option value="Agency">Agency</option>
                     </select>
                  </div>
                </div>
              </div>
              <div class="agency_detail"  style="display: none">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Company Name</label>
                      <input type="text" class="form-control validate[required]" name="item_name" id="item_name" value="Private driver Agency" readonly="" placeholder="Item Name">
                  </div>
                </div>
              </div>
              
              <div class="row" id="agency_user">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Agency</label>
                     <select id="agency" name="agency" class="form-control" style="width: 150px">
                     	<?php 
                     	//$agency[]=array("id"=>1,"name"=>"agency1");
                     	//$agency[]=array("id"=>"2","name"=>"agency2");
                     	foreach($agency as $user){ ?>
                     	<option value=" <?php echo $user['id'] ?>"><?php echo $user['name'] ?></option>
                     	<?php } ?>
                     </select>
                  </div>  
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addAgency" type="button">Add New Agency</button>
                  </div>  
                </div>
              </div>
              </div>
              <div class="customer_detail">
              	<div class="row" id="">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Customer</label>
                     <select id="customer" name="customer_id" class="form-control" style="width: 250px">
                     	<?php 
                     	//$agency[]=array("id"=>1,"name"=>"agency1");
                     	//$agency[]=array("id"=>"2","name"=>"agency2");
                     	foreach($customer_data as $user){ ?>
                     	<option value=" <?php echo $user['id'] ?>"><?php echo $user['name'] ?></option>
                     	<?php } ?>
                     </select>
                  </div>  
                </div>
              </div>
              	
              </div>
              
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Pickup Location</label>
                      <input type="text" class="form-control validate[required]" name="pickup" id="pickup" value="" placeholder="Pickup Location">
                  </div>
                </div>
              </div>
              
              <div class="row">
                    <div class="">
                     <div class="col-sm-3">
                     	<label class="" for="address">Latitude</label>
                    <input class="form-control" name="pickup_latitude" id="pickup_latitude" type="text" placeholder="latitude" readonly="">
                    </div>
					<div class="col-sm-3">
						 <label class="col-sm-3 control-label" for="address">Longitude</label>
						<input class="form-control" name="pickup_longitude" id="pickup_longitude" type="text" placeholder="longitude" readonly="">
					</div>
                   </div>
                    </div>

              <div style="padding: 5px"></div>
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Destination Location</label>
                      <input type="text" class="form-control validate[required]" name="destination" id="destination" value="" placeholder="Destination Loaction">
                  </div>
                </div>
              </div>
              
               <div class="row">
                    <div class="">
                     <div class="col-sm-3">
                     	<label class="" for="address">Latitude</label>
                    <input class="form-control" name="destination_latitude" id="destination_latitude" type="text" placeholder="latitude" readonly="">
                    </div>
					<div class="col-sm-3">
						 <label class="col-sm-3 control-label" for="address">Longitude</label>
						<input class="form-control" name="destination_longitude" id="destination_longitude" type="text" placeholder="longitude" readonly="">
					</div>
                   </div>
                    </div>

  <div style="padding: 5px"></div>
 <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Comment</label>
                      <input type="text" class="form-control" name="comment" id="comment" value="" placeholder="Comment">
                  </div>
                </div>
              </div>
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Contact Detail</label>
                      <input type="text" class="form-control" name="contact_number" id="contact_number" value="" placeholder="contact Number">
                  </div>
                </div>
              </div>
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Booking Time</label>
                      <input type="text" class="form-control validate[required]" name="bookingtime" id="bookingtime"  placeholder="Booking Time">
                  </div>
                </div>
              </div>
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Cab Type</label>
                  
                     <select class="form-control xs" id="cab_type" name="cab_type" style="width: 175px">
                     	<option value="1">First</option>
                     	<option value="3">Luxury</option>
                     	
                     </select>
                     
                  </div>  
                </div>
              </div>
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Payment Mode</label>
                     <select class="form-control" id="payment_mode" name="payment_mode" style="width: 175px">
                     	<option value="CASH">Cash</option>
                     	<option value="INVOICE">Invoice</option>
                     	<option value="INVOICE">BT</option>
                     	
                     </select>
                  </div>  
                </div>
              </div>  
             
            <!-- <input class="form-control timepicker" id="timepicker" type="text"> -->
                         
             <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script> -->
  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>    
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDstwVta3Vzst1HWWuDeOdzV6n0G4G5ZO8&libraries=places"></script>
<!-- <script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>-->

     
    <script src="<?php echo base_url();?>js/jquery.select2.js"></script>
 <script src="<?php echo base_url();?>plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

  <script src="<?php echo base_url();?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
 
 
  
  
 <script type="text/javascript">
//var $ = jQuery.noConflict();
$("#agency").select2({
    placeholder: "Select a state",
 // allowClear: true
 });
 
 $("#booking_type").change(function(){
  	var type = $("#booking_type").val();
 	
 	if(type=='customer')
 	{
 		$(".agency_detail").css("display","none");
 		$(".customer_detail").css("display","");
 		$("#contact_number").css("display","none");
 	}else
 	{
 		$(".agency_detail").css("display","");
 		$(".customer_detail").css("display","none");
 		$("#contact_number").css("display","");
 	}
 	
 })

$( "arget" ).change(function() {
  alert( "Handler for .change() called." );
}); 	
 	
 	
$("#customer").change(function(){
 //alert(this.value);
})
 

$('#timepicker').timepicker();
$("#cab_type").select2();
$("#booking_type").select2();
$("#customer").select2();
$("#payment_mode").select2();
$('#bookingtime').datetimepicker();

$(document).ready(function(){
//$('#bookingtime').datepicker();
//$('#bookingtime').datetimepicker();
  
  //$('#datetimepicker').datetimepicker();
  //('#datetimepicker1').datetimepicker();

    function submit_form(formData,status)
    {
       	 $('#submit').prop('disabled', true);
        if(status==true)
        {
            var querystr = $("#save_item").serialize();
            
           // alert(querystr);
            
            $.ajax({
                url		:	"<?php echo site_url('Phonebooking/SaveBooking'); ?>",
                type	:	"POST",
                data	:	querystr,
                //async: false,
               // cache: false,
                //contentType: false,
                //processData: false,
                
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                  if(data)
                  {
                    $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl("Booking Successfully Created", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_item')[0].reset();
                  }
                  else
                  {
                    $("#loader").hide();
                    //window.location='<?php echo site_url('login/dashboard'); ?>';
                    //alert('failed to save');
                  }
                 }
            });
            return false;
        }
        
    }
            
   
    
  $("#save_item").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
      
     
});



 function formsubmit(){
 
 var agency_name=$("#agency_name").val();
 var email=$("#email").val();
 var country_code=$("#country_code").val();
 var phone_number=$("#phone").val();
 var password=$("#password").val();
 if(agency_name=="")
 {
 	$("#agency_name").css("border","1px red solid");
 	isvaild=0;
 }else
 {
 	$("#agency_name").css("border","1px #ccc solid");
 	isvaild=1;
 }
 if(email=='')
 {
 	$("#email").css("border","1px red solid");
 	isvaild=0;
 }else
 {
 	$("#email").css("border","1px #ccc solid");
 	isvaild=1;
 }
  if(country_code=='')
 {
 	$("#country_code").css("border","1px red solid");
 	isvaild=0;
 }else
 {
 	$("#country_code").css("border","1px #ccc solid");
 	isvaild=1;
 }
  if(phone_number=='')
 {
 	$("#phone").css("border","1px red solid");
 	isvaild=0;
 }else
 {
 	$("#phone").css("border","1px #ccc solid");
 	isvaild=1;
 }	
 /*if(password=='')
 {
 	$("#password").css("border","1px red solid");
 	isvaild=0;
 }else
 {
 	$("#password").css("border","1px #ccc solid");
 	isvaild=1;
 }*/
 if(agency_name && email &&  phone_number && country_code )
 {
 	//alert("ajax");
 	 var querystr = $("#add_agency").serialize();
 	 $.ajax({
                url		:	"<?php echo site_url('Phonebooking/saveagency'); ?>",
                type	:	"POST",
                data	:	querystr,
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                	//alert(data);
                	ResponseData=jQuery.parseJSON(data);
                  if(ResponseData.status == 1)
                  {
                    $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl(ResponseData.message, {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_item')[0].reset();
                    
                   
                    $("#agency").append("<option value='"+ResponseData.id+"'>"+ResponseData.name+"</option>");
                    $('#addAgency').modal('hide');
                  }
                  else
                  {
                    $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl(ResponseData.message, {
                            type: 'error',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_item')[0].reset();
                    
                  }
                 }
            });
            return false;
        
 }
 	
  
      }


function checkemail()
{
var email=$('#email').val();
 // var user_id=$("#id").val();
   $.ajax({
                url		:	"<?php echo site_url('Phonebooking/check_email'); ?>",
                type	:	"POST",
                data	:	{'email':email},
               
                
                beforeSend  :   function(){
                },   
                success	:	function(data){
                  if(data == 1)
                  {
                   
                    setTimeout(function() {
                        $.bootstrapGrowl("This email is already exist", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#email').val('');
                   $('#submit').prop('disabled', true);
                  }
                         
                          else
                       {
                   $('#submit').removeAttr('disabled');
                  }
                 }
            });
            return false;
	
	
}

//$("#country_code").change(function() 
function checkCountryCode()
{
	
  var contact=$("#country_code").val();
  var FirstChar=contact.charAt(0);
  if(FirstChar!="+")
  {
  
  	$("#country_code").val("");
  }
  
}//);


 
 
 var autocomplete = new google.maps.places.Autocomplete($("#destination")[0], {});
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
  var place = autocomplete.getPlace();
  $("#destination_latitude").val(place.geometry.location.lat());
  $("#destination_longitude").val(place.geometry.location.lng());
 });
 
 var autocomplete_pickup = new google.maps.places.Autocomplete($("#pickup")[0], {});
  google.maps.event.addListener(autocomplete_pickup, 'place_changed', function() {
  var place_pickup = autocomplete_pickup.getPlace();
  $("#pickup_latitude").val(place_pickup.geometry.location.lat());
  $("#pickup_longitude").val(place_pickup.geometry.location.lng());
 });

 

$("#contact_number").keydown(function (e) 
{
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>  


<div class="modal fade" id="addAgency" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
               Add Agency 
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
            	<div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="add_agency" action="" method="post" enctype="multipart/form-data" autocomplete="off">
            
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Agency Name</label>
                      <input type="text" class="form-control validate[required]" name="agency_name" id="agency_name" value="" placeholder="Agency Name">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Email</label>
                      <input type="text" class="form-control validate[required]" name="email" id="email" placeholder="Email" onchange="checkemail()">
                  </div>
                </div>
              </div>
                <!-- <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Password</label>
                      <input type="password" class="form-control validate[required]" name="password" id="password" placeholder="Password">
                  </div>
                </div>
              </div> -->
                <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Country Code</label>
                   <span><input type="text" onchange="checkCountryCode()" class="form-control validate[required]" name="country_code" id="country_code" placeholder="Country Code"></span>
                   </div>
                    </div>
                 <div class="col-sm-6">
                  <div class="form-group">
                  <label for="first name">Phone</label>
                   <span><input type="text"  class="form-control validate[required]" name="phone" id="phone" placeholder="Phone Number"></span>
                   
                </div>
              </div>
               </div>

                         
             <div class="box-footer">
                <button type="button" onclick="formsubmit()" class="btn btn-primary">Submit</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
                                    
                </div>
             
            </div>
        </div> 
    </div>          