#!/bin/sh
## Specify data base schemas to backup and credentials
DATABASES="pdapp_production"
SERVER="private-driver.csxljump76gz.eu-west-1.rds.amazonaws.com"
## Syntax databasename as per above _USER and _PW
## _USER is mandatory _PW is optional
pdapp_production_USER=pd_driver
pdapp_production_PW='PD2017#123!ABCDQWERT'


## Initialize some variables
DATE=$(date +%d-%m-%Y)
TIMESTAMP=`date +"_%d_%m_%Y_%H_%M_%S"`
BACKUP_DIRECTORY=/tmp/backups
##S3_CMD="/usr/bin/s3cmd --config /etc/s3cfg"

## Specify where the backups should be placed
S3_BUCKET_URL=s3://pd-database-backup/DatabaseBackup/$DATE/

## The script
cd /
mkdir -p $BACKUP_DIRECTORY
rm -rf $BACKUP_DIRECTORY/*

## Backup MySQL:s
for DB in $DATABASES
do
BACKUP_FILE=$BACKUP_DIRECTORY/${DB}${TIMESTAMP}.sql
if [  -n "${DB}_PW"  ]
then
  PASSWORD=$(eval echo \$${DB}_PW)
  USER=$(eval echo \$${DB}_USER)
  /usr/bin/mysqldump -v -u $USER --password=$PASSWORD -h $SERVER -r $BACKUP_FILE $DB 2>&1
else
  /usr/bin/mysqldump -v -u $USER -h $SERVER -r $BACKUP_FILE $DB 2>&1
fi
gzip $BACKUP_FILE 2>&1
##$S3_CMD put ${BACKUP_FILE}.gz $S3_BUCKET_URL 2>&1
aws s3 cp ${BACKUP_FILE}.gz $S3_BUCKET_URL
done

