
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Add Mobile No. Detail</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Mobile No. Details</a></li>
      <li class="active">Add Mobile No. Detail</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="save_contact" action="" method="post" enctype="multipart/form-data" autocomplete="off">
              
             <input type="hidden" value="<?=(isset($mobile[0]->id))?$mobile[0]->id:"";?>"  name="id"  />
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Country Code</label>
                     <select id="country_code" name="country_code" id="country_code" width="25%">
                      <?php
                      
                    for($i=0;$i<sizeof($mobile);$i++)
                        {
                         
                      
                      ?>
                      <option value="<?php echo $mobile[$i]->calling_code;?>" id="country_code" name="country_code"><?php echo '+'. $mobile[$i]->calling_code.' '.$mobile[$i]->long_name;?></option>
                      <?php
                        }
                        ?>
                     </select> 
                  </div>
                </div>
              </div>
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Mobile Serial No.</label>
                      <input type="text" class="form-control validate[required]" name="serial_no" value="<?=(isset($mobile[0]->serial_no))?$mobile[0]->serial_no:"";?>" id="serial_no" placeholder="Mobile serial no" onchange="check_mobile_serial(this.value);" style="width:150px;">
                  </div>  
                </div>
              </div>
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Sim No.</label>
                      <input type="text" class="form-control validate[required]" name="contact" value="<?=(isset($mobile[0]->country_code))?$mobile[0]->country_code:"";?>" id="contact" placeholder="Sim No." onchange="check_mobile(this.value);" style="width:150px;">
                  </div>  
                </div>
              </div>
              
              
             
              
             
              
              
              
              
              
              
              
              
              
              
              
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" id="submit" name="submit">Submit</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
 <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>
<!--<script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>
    
<script type="text/javascript">
//var $ = jQuery.noConflict();
$(document).ready(function(){

    function submit_form(formData,status)
    {
      
        if(status==true)
        {
           
            var querystr = $("#save_contact").serialize();
            
            
            
            $.ajax({
                url		:	"<?php echo site_url('Mobile_info/submit_contact'); ?>",
                type	:	"POST",
                data	:	querystr,
                //async: false,
               // cache: false,
                //contentType: false,
                //processData: false,
                
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                    // alert(data);
                  if(data > 1)
                  {
                    $("#loader").hide();
                 
                    setTimeout(function() {
                        $.bootstrapGrowl("Contact is added successfully", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_contact')[0].reset();
                    window.location='<?php echo site_url('Mobile_info'); ?>';
                  }
                  else
                  {
                    $("#loader").hide();
                    //window.location='<?php echo site_url('login/dashboard'); ?>';
                    //alert('failed to save');
                  }
                 }
            });
            return false;
        }
        
    }
            
      
    
  $("#save_contact").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});

function check_mobile_serial(serial_no)
{
   displayOverlay("Loading...");
  $.ajax({type:'POST',
                    url: "<?php echo site_url('Mobile_info/check_serial_no'); ?>",
                  data:{"serial_no":serial_no}, 
                  success: function(data){
                    if (data==1) {
                      //code
                      removeOverlay();
                      alert("Serial no. already in use, Try with different serial no. ");
                    }else{
                       removeOverlay();
                    }
                  }
                });

}
function check_mobile(mobile_no)
{
   displayOverlay("Loading...");
  $.ajax({type:'POST',
                    url: "<?php echo site_url('Mobile_info/check_mobile_no'); ?>",
                  data:{"mobile_no":mobile_no}, 
                  success: function(data){
                    if (data==1) {
                      //code
                      removeOverlay();
                      alert("Sim no. already in use, Try with different Sim no. ");
                    }else{
                       removeOverlay();
                    }
                  }
                });

}
function displayOverlay(text) {
    $("<table id='overlay'><tbody><tr><td>" + text + "</td></tr></tbody></table>").css({
        "position": "fixed",
        "top": "0px",
        "left": "0px",
        "width": "100%",
        "height": "100%",
        "background-color": "rgba(0,0,0,.5)",
        "z-index": "10000",
        "vertical-align": "middle",
        "text-align": "center",
        "color": "#fff",
        "font-size": "40px",
        "font-weight": "bold",
        "cursor": "wait"
    }).appendTo("body");
}

function removeOverlay() {
    $("#overlay").remove();
}

</script>

 
