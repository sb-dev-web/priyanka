<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Resetpassword extends CI_Controller
{

    function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->helper(array('form', 'url','constants_helper'));
        $this->load->model('Resetpassword_model');
        $this->load->library('Cipher');
         $this->load->library('xmpp', false);
        //$this->clear_cache();
    }

    function index()
    {
        $data['userid'] = ($_GET['userid']);
        $data['code'] = $_GET['code'];
        
        $cipher = new Cipher('PRIVATETAXI_APP');
         $data['userid'] =$encrypteduserid = $cipher->decrypt(($_GET['userid']));
        $data['code']= $encryptedcode = $cipher->decrypt($data['code']);
        $user_detail=$this->Resetpassword_model->get_user_detail($data);
        if($user_detail){
        $data['email']=$user_detail[0]['email'];
        $data['id']=$user_detail[0]['id'];
        $data['name']=$user_detail[0]['name'];
        $this->load->view('setpassword', $data);
        }
        else
        {
          $data['notallow']=1;
          $this->load->view('setpassword', $data);  
        }
    }
    function submit()
    {
      //  print_r($_POST);
        $set_password=$this->Resetpassword_model->set_user_password($_POST);
        if($set_password)
        {
            $update_password=$_POST['form-password'];
            $user_name=USER_JID_NAME.'-'.$_POST['id'];
            $name=$_POST['name'];
            	$param = array(
								$user_name,
                                $name,
								$update_password
								);
					
						$this->xmpp->api("update", $param);
            echo json_encode(array("IsValid"=>1));
        }else
        {
              echo json_encode(array("IsValid"=>0));
        }
        
    }
	
	function AccountVerify()
	{
		$data['userid'] = ($_GET['userid']);
        $data['code'] = $_GET['code'];
		$cipher = new Cipher('PRIVATETAXI_APP');
        $data['userid'] =$encrypteduserid = $cipher->decrypt(($_GET['userid']));
        $data['code']= $encryptedcode = $cipher->decrypt($data['code']);
        $user_detail=$this->Resetpassword_model->account_verify($data);
		//print_r($user_detail);
        if($user_detail['status']==1)
        {
        	
        $data['is_emailverify']=$user_detail['email_verify'];
        $data['phone_verify']=$user_detail['is_varify'];
        $data['name']=$user_detail['name'];
        $this->load->view('account_verify', $data);
        }
        else
        {
          $data['notallow']=1;
          $this->load->view('setpassword', $data);  
        }
	}
    


 }

?>