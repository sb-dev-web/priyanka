<?php

	class Booking_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		function cab_booking($input_method)
		{

			//print_r($input_method);
			$lat = $input_method['pickup_latitude'];
			$lng = $input_method['pickup_longitude'];
			$pickup_location = urldecode($input_method['pickup_location']);
			$userid = $input_method['user_id'];
			$destination_lat = $input_method['destination_latitude'];
			$detination_lng = $input_method['destination_longitude'];
			$destination_location = urldecode($input_method['destination_location']);
			$userid = $input_method['user_id'];
			$cab_type = $input_method['cab_type'];
			$comment = (@$input_method['comment'] == '') ? "" : @$input_method['comment'];
			$date = get_gmt_time();
			$coupon_code = $input_method['coupon_code'];
			$discount = $input_method['discount_amount'];
			$data = array(
				'user_id' => $userid,
				'createdon' => $date,
				'booking_time' => $date,
				"pickup_latitude" => $lat,
				"pickup_longitude" => $lng,
				"pickup_location" => $pickup_location,
				"destination_latitude" => $destination_lat,
				"destination_longitude" => $detination_lng,
				"destination_location" => $destination_location,
				"cab_type" => $cab_type,
				"comment" => $comment,
				"discount_amount" => $discount,
				"coupon" => $coupon_code
			);
			$str = $this -> db -> insert('cab_booking', $data);
			// echo "pp". $this->db->last_query();
			$booking_id = $this -> db -> insert_id();
			//save customer extra order
			//echo strip_slashes($input_method['extra_order']);
			if (!empty($input_method['extra_order']))
			{

				$extra_order = json_decode(strip_slashes($input_method['extra_order']), true);

				foreach ($extra_order as $order)
				{
					$order_data = array(
						"booking_id" => $booking_id,
						"user_id" => $userid,
						"item_id" => $order['item_id'],
						"item_price" => $order['item_price'],
						"quantity" => $order['quantity'],
						"createdon" => $date
					);
					$this -> db -> insert("booking_extra_order", $order_data);
					//echo $this->db->last_query();
				}
			}
			if ($booking_id)
			{
				$CBN = "PD" . sprintf("%05d", $booking_id);
				$this -> db -> query("update cab_booking set booking_number='" . $CBN . "' where id=$booking_id");
				return $booking_id;
				//send request to driver for new booking
				//search nearest driver

			}
			else
			{
				return false;
			}
		}

		/* booking in advance */
		function cab_booking_advance($input_method)
		{
			$lat = $input_method['pickuplat'];
			$lng = $input_method['pickuplong'];
			$userid = $input_method['customerid'];
			$destination_lat = $input_method['destinationlat'];
			$detination_lng = $input_method['destinationlong'];
			$date = get_gmt_time();
			$booking_time = $input_method['pickup_time'];
			$cab_type = $input_method['cab_type'];
			$destination_address = urldecode($input_method['destination_location']);
			$pickup_address = urldecode($input_method['pickup_location']);
			$data = array(
				'user_id' => $userid,
				'createdon' => $date,
				"pickup_latitude" => $lat,
				"pickup_longitude" => $lng,
				"destination_latitude" => $destination_lat,
				"destination_longitude" => $detination_lng,
				"destination_location" => $destination_address,
				"pickup_location" => $pickup_address,
				"booking_time" => $booking_time,
				"cab_type" => $cab_type,
				"is_prebooking" => 'Y'
			);
			$str = $this -> db -> insert('cab_booking', $data);
			// echo "pp". $this->db->last_query();
			$booking_id = $this -> db -> insert_id();
			if ($booking_id)
			{
				$CBN = "PD" . sprintf("%05d", $booking_id);
				$this -> db -> query("update cab_booking set booking_number='" . $CBN . "' where id=$booking_id");
				return $booking_id;
				//send request to driver for new booking
				//search nearest driver

			}
			else
			{
				return false;
			}
		}

		function driver_available($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$chk = $this -> db -> get_where('driver', array(
				'driver_id' => $driver_id,
				"status" => DRIVER_AVAILABLE
			));
			// $this->db->last_query();
			if ($chk -> num_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function get_booking_detail($bookingid)
		{

			//$query = $this->db->get_where('cab_booking', array('id' => $booking_id), 1);
			$query = $this -> db -> query("SELECT b.id as booking_id,b.pickup_latitude,
        b.pickup_longitude,b.user_id,u.name,u.phone,b.destination_latitude,b.destination_longitude,booking_number,pickup_location,destination_location,b.comment
         FROM `cab_booking` b
          JOIN user u ON u.id = b.user_id
          left join booking_extra_order o on o.booking_id=b.id 
           WHERE b.id =$bookingid");
			if ($query -> num_rows() > 0)
			{
				$data = $query -> result_array();
				return $data;
			}
			else
			{
				return false;
			}

		}

		function GetBookingDetail($booking_id)
		{
			$query = "SELECT user_id,pickup_location,destination_location,comment,os_type,booking_time,cab_type,pickup_latitude,pickup_longitude,destination_longitude,destination_latitude,pickup_placename,destination_placename FROM `cab_booking`  where cab_booking.id=$booking_id";
			$booking = $this -> db -> query($query);
			if ($booking -> num_rows() > 0)
			{
				$booking_detail = $booking -> row_array();
				$user_id= $booking_detail['user_id'];
				if($booking_detail['os_type']=='web')
				{
					$user_query ="SELECT name,concat(country_code,phone)phone ,email FROM `web_user` where id=".$user_id;
					$user_res= $this->db->query($user_query);
					$user_data=$user_res->row_array();
					$booking_detail['email']=$user_data['email'];
					$booking_detail['name']=$user_data['name'];
					$booking_detail['phone']=$user_data['phone'];
				}else
				{
					$user_query ="SELECT name,concat(country_code,phone)phone ,email FROM `user` where id=".$user_id;
					$user_res= $this->db->query($user_query);
					$user_data=$user_res->row_array();
					$booking_detail['email']=$user_data['email'];
					$booking_detail['name']=$user_data['name'];
					$booking_detail['phone']=$user_data['phone'];
				}
				return $booking_detail;

			}
			else
			{
				return FALSE;
			}

		}

		function booking_status($input_method)
		{
			$booking = $input_method['booking_id'];
			$driver_id = $input_method['driver_id'];
			$this -> db -> select('status');
			$booking_res = $this -> db -> get_where('cab_booking', array("id" => $booking));
			//echo $this->db->last_query();
			if ($booking_res -> num_rows() > 0)
			{
				$booking_status_data = $booking_res -> result_array();
				$booking_status_data = $booking_status_data[0];
				$booking_status = $booking_status_data['status'];
				if ($booking_status == 'WAITING')
				{
					$status = "WAITING";

					//chk driver rejected or any thing response for this booking
					$this -> db -> select('driver_response');
					$driver_response = $this -> db -> get_where('cab_booking_history', array(
						"booking_id " => $booking,
						"driver_id" => $driver_id,
						"booking_type" => "NORMAL"
					));
					// echo $this->db->last_query();
					if ($driver_response -> num_rows() > 0)
					{
						$driver_response_data = $driver_response -> result_array();
						$driver_response_data = $driver_response_data[0];
						$status = $driver_response_data['driver_response'];
						return $status;
					}
					//chk this driver available or not for booking request
					/* $driver_status = $this->db->query("SELECT d.status,b.id  FROM shift s left
					 * join cab_booking b on b.driver_id=s.driver_id and
					 b.id > $booking left join driver d on d.driver_id=b.driver_id where s.driver_id
					 =$driver_id and d.status='" .
					 DRIVER_AVAILABLE . "'");*/
					$driver_status = $this -> db -> query("SELECT b.status FROM cab_booking b where id='" . $booking . "'");
					// echo $this->db->last_query();
					if ($driver_status -> num_rows() > 0)
					{
						$driver_status_data = $driver_status -> result_array();
						$driver_status_data = $driver_status_data[0];
						$driver_status = $driver_status_data['status'];
						//$nextbooking_id = $driver_status_data['id'];
						//
						//                    if ($nextbooking_id > '0' || $driver_status !=
						// DRIVER_AVAILABLE)
						//                    {
						//                        $status = "DRIVER_BUSY";
						//                    }
						//                    else
						//                    {
						//                        $status = "WAITING";
						//                    }
						$status = $driver_status;
					}
					else
					{
						return $status;
					}

					return $status;
				}
				else
				{
					return $status = $booking_status;

				}
				//$query="select status from cab_booking where id=$id";
			}
			else
			{
				return $status = BOOKING_NOTFOUND;
			}
		}

		function save_booking_payment($data)
		{
			$payment = $this -> db -> insert("payment_detail", $data);
			//echo $this->db->last_query();
			$booking_Id = $data['booking_id'];
			if ($this -> db -> insert_id())
			{
				//update the booking status
				$update_data = array("status" => "TRIP_ENDED");
				$this -> db -> where("id", $booking_Id);
				$upadte = $this -> db -> update("cab_booking", $update_data);
				if ($upadte)
				{
					return true;
				}
				else
				{
					return false;
				}

			}
			return false;

		}

		/************************************************************
		 ********************TRIP END START******************************
		 ************************************************************/
		/* function for ride */
		function my_ride($input_method)
		{
			$customer_id = $input_method['customer_id'];
			$type = $input_method['type'];
			$pagenumber = @$input_method['page_number'];
			if ($pagenumber == "")
			{
				$pagenumber = 1;
			}
			//$pagenumber = $input_method['page_number'];
			$limit = 10;
			if ($pagenumber == 1)
			{
				$offset = 0;
			}
			else
			{
				$pagenumber = $pagenumber - 1;
				$offset = $pagenumber * 10;
			}

			if ($type == 'UPCOMING')
			{
				$query = $this -> db -> query("select *  from cab_booking  where user_id='$customer_id' and (status='TRIP_STARTED' OR status='ADVANCE_BOOKING'  OR status='WAITING' OR status='CUSTOMER_ACCEPTED') order by cab_booking.id desc limit $offset,$limit");
				return $query -> result();

			}
			elseif ($type == 'ALL')
			{
				$query = $this -> db -> query("select b.id,b.booking_number,b.driver_id,b.plate_number,b.pickup_latitude,b.pickup_longitude,b.destination_latitude,b.destination_longitude,b.pickup_location,b.destination_location,b.booking_time,b.status,pd.booking_id,pd.journey_time,pd.billing_amount,pd.km_driven,pd.waiting_time,pd.waiting_charge,pd.other_charge,pd.tip,pd.user_paid,pd.is_paid,ct.title 
                                       from cab_booking b 
                                       left join payment_detail pd on pd.booking_id=b.id 
                                       left join cab_type ct on b.cab_type=ct.type_id
                                       where user_id='$customer_id' and (status ='CUSTOMER_NOT_RESPOND' OR status='CUSTOMER_ACCEPTED' OR status='CANCELLED' OR  status='FINISH' OR status='TRIP_ENDED') 
                                       order by b.id desc limit $offset,$limit");
				// echo $this->db->last_query();
				return $query -> result();
			}
		}

		function getfare_detail($cab_type)
		{
			$result = $this -> db -> get_where("fare_info", array(
				"cab_type" => $cab_type,
				"type" => "NORMAL"
			));
			if ($result -> num_rows() > 0)
			{
				return $result -> result_array();
			}
			else
			{
				return false;
			}

		}

		/* work for admin get advacne booking detal */
		function fetch_advance_booking($limit, $offset = 0)
		{
			$date = date('Y-m-d');
			$result = $this -> db -> query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name,cab_booking.booking_type FROM `cab_booking` left join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and date(booking_time)>='$date' and (cab_booking.status  in ('CONFIRMED','DRIVER_ASSIGNED')) ORDER BY booking_time ASC  limit $offset,$limit");
			// echo $this->db->last_query();
			//$result = $this -> db -> query("Select
			// cab_booking.id,cab_booking.user_id,cab_booking.booking_time,cab_booking.booking_number,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.driver_id,cab_booking.status,
			// driver.first_name,driver.last_name from cab_booking LEFT JOIN driver on
			// driver.driver_id=cab_booking.driver_id LIMIT $offset,$limit");
			if ($result -> num_rows() > 0)
			{
				return $result -> result_array();
			}
			else
			{
				return false;
			}
		}

		function GetPreBooking()
		{
			$date = date('Y-m-d');
			$requestData = $_REQUEST;

			$columns = array(
				// datatable column index  => database column name
				0 => 'cab_booking.id',
				1 => 'pickup_location',
				2 => 'booking_time',
				3 => 'cab_booking.createdon',
				4 => 'cab_type',
			);
			$company_id = $this -> session -> userdata('company_id');
			//$company_id=4;
			$query = $this -> db -> get_where('cab_booking', array(
				"booking_type" => "NORMAL",
				"status" => "WAITING",
				"date(booking_time)>=" => $date
			));
			//	echo $this->db->last_query();

			$totalData = $query -> num_rows();
			//echo $totalFiltered = $totalData;
			//exit;

			$sql = "SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_type,cab_booking.booking_time,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name,cab_booking.booking_type,cab_booking.status FROM `cab_booking` left join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and date(booking_time)>='$date' and cab_booking.status in ('CONFIRMED') and booking_type='NORMAL'";
			//exit;
			if (!empty($requestData['search']['value']))
			{
				// if there is a search parameter, $requestData['search']['value'] contains
				// search parameter
				$sql .= " AND ( name LIKE '" . $requestData['search']['value'] . "%' ";
				$sql .= " OR booking_time LIKE '%" . $requestData['search']['value'] . "%' ";
				$sql .= " OR pickup_location LIKE '%" . $requestData['search']['value'] . "%' ";
				$sql .= " OR destination_location LIKE '%" . $requestData['search']['value'] . "%' )";
			}
			$query = $this -> db -> query($sql);
			//echo $this->db->last_query();
			//exit;
			$totalFiltered = $query -> num_rows();
			/* $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."
			 * ".$requestData['order'][0]['dir']."  ";*/
			$sql .= " order by booking_time asc limit " . $requestData['start'] . " ," . $requestData['length'] . "   ";

			$query = $this -> db -> query($sql);
			//echo  $this->db->last_query();
			$data = array();

			// preparing an array
			foreach ($query->result() as $row)
			{
				//print_R($row);
				$nestedData = array();
				$id = $row -> bid;
				$cab_type = $row -> cab_type;
				if ($cab_type == '1')
				{
					$cab_type = "First Class";
				}elseif($cab_type=='4')
				{
					$cab_type='Mini Bus';
				}
				else
				{
					$cab_type = "Luxury Van";
				}
				if ($row -> driver_id == 0)
				{
					$driver_name = "Not Yet Assgin";
					$class = "label label-warning";
					$lable = "Assign";

				}
				else
				{
					$driver_name = $row -> first_name . ' ' . $row -> last_name;
					$lable = "Assigned";
					$class = "label label-success";
				}

				$deletebrand = " <a href='#' id='$id' class='delete' onclick=" . "deleteemp($id)" . "  ><i class=" . '"fa fa-remove"' . "></i></a> ";
				$deletebrand .= "| <a href='employee/add/$id'  ><i class=" . '"fa fa-edit"' . "></i></a>";
				$action = "<button onclick=" . "Actiondata($row->bid,'Reject')" . " class='btn btn-block btn-danger btn-sm action' id='$row->bid' type='button'>Reject</button>";
				$action .= "<button onclick=" . "Actiondata($row->bid,'Confirm')" . " class='btn btn-block btn-success btn-sm action' id='$row->bid'type='button'>Confirm</button>";
				$onclick = "customer_detail($row->bid,'" . $row -> booking_type . "')";
				$booking_detail = "<a href='javascript:void(0);' onclick='$onclick'>" . $row -> booking_number . "</a>";

				$nestedData[] = $booking_detail;
				$nestedData[] = $row -> pickup_location;
				$nestedData[] = gmt_to_norway($row -> booking_time);
				$nestedData[] = $row -> createdon;
				$nestedData[] = $row -> name;

				//$nestedData[] = $row -> destination_location;
				$nestedData[] = $cab_type;
				//$nestedData[] = $this -> UserBookingCount($id);
				$nestedData[] = $action;
				$data[] = $nestedData;
			}

			$json_data = array(
				"draw" => intval($requestData['draw']), // for every request/draw by clientside ,
				// they send a number as a parameter, when they recieve a response/data they
				// first check the draw number, so we are sending same number in draw.
				"recordsTotal" => intval($totalData), // total number of records
				"recordsFiltered" => intval($totalFiltered), // total number of records after
				// searching, if there is no searching then totalFiltered = totalData
				"data" => $data // total data array
			);
			//print_r($json_data);
			return $json_data;
		}

		function fetch_newadvance_booking($limit, $offset = 0)
		{
			$date = date('Y-m-d');
			$result = $this -> db -> query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name,cab_booking.booking_type,cab_booking.status FROM `cab_booking` left join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and date(booking_time)>='$date' and cab_booking.status in ('WAITING') and booking_type in ('NORMAL','COMPANY') ORDER BY booking_time ASC  limit $offset,$limit");
			//echo $this->db->last_query();
			//exit;
			if ($result -> num_rows() > 0)
			{
				return $result -> result_array();
			}
			else
			{
				return false;
			}
		}

		function GetPreNewBooking()
		{
			$date = date('Y-m-d');
			$requestData = $_REQUEST;

			$columns = array(
				// datatable column index  => database column name
				0 => 'cab_booking.id',
				1 => 'pickup_location',
				2 => 'booking_time',
				3 => 'cab_booking.createdon',
				4 => 'cab_type',
			);
			$company_id = $this -> session -> userdata('company_id');
			//$company_id=4;
			$this -> db -> where_in('booking_type', array(
				'NORMAL',
				'COMPANY'
			));
			$query = $this -> db -> get_where('cab_booking', array(

				"status" => "WAITING",
				"date(booking_time)>=" => $date
			));
			//	echo $this->db->last_query();

			$totalData = $query -> num_rows();
			//echo $totalFiltered = $totalData;
			//exit;

			$sql = "SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.booking_type,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name,cab_booking.booking_type,cab_booking.status FROM `cab_booking` left join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and date(booking_time)>='$date' and cab_booking.status in ('WAITING') ";
			//and booking_type in ('NORMAL','COMPANY','')
			//exit;
			if (!empty($requestData['search']['value']))
			{
				// if there is a search parameter, $requestData['search']['value'] contains
				// search parameter
				$sql .= " AND ( name LIKE '" . $requestData['search']['value'] . "%' ";
				$sql .= " OR booking_time LIKE '%" . $requestData['search']['value'] . "%' ";
				$sql .= " OR pickup_location LIKE '%" . $requestData['search']['value'] . "%' ";
				$sql .= " OR destination_location LIKE '%" . $requestData['search']['value'] . "%' )";
			}
			$query = $this -> db -> query($sql);
			//echo $this->db->last_query();
			//exit;
			$totalFiltered = $query -> num_rows();
			/* $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."
			 * ".$requestData['order'][0]['dir']."  ";*/
			$sql .= " order by booking_time desc limit " . $requestData['start'] . " ," . $requestData['length'] . "   ";

			$query = $this -> db -> query($sql);
			//echo  $this->db->last_query();
			$data = array();

			// preparing an array
			foreach ($query->result() as $row)
			{
				//print_R($row);
				$nestedData = array();
				$id = $row -> bid;
				$cab_type = $row -> cab_type;
				if ($cab_type == '1')
				{
					$cab_type = "First Class";
				}elseif($cab_type=='4')
				{
					$cab_type="Mini Bus";
				}
				else
				{
					$cab_type = "Luxury Van";
				}

				$deletebrand = " <a href='#' id='$id' class='delete' onclick=" . "deleteemp($id)" . "  ><i class=" . '"fa fa-remove"' . "></i></a> ";
				$deletebrand .= "| <a href='employee/add/$id'  ><i class=" . '"fa fa-edit"' . "></i></a>";
				$action = "<button onclick=" . "Actiondata($row->bid,'Reject')" . " class='btn btn-block btn-danger btn-sm action' id='$row->bid' type='button'>Reject</button>";
				$action .= "<button onclick=" . "Actiondata($row->bid,'Confirm')" . " class='btn btn-block btn-success btn-sm action' id='$row->bid'type='button'>Confirm</button>";
				$onclick = "customer_detail($row->bid," . "\"$row->booking_type" . "\")";
				$booking_detail = "<a href='javascript:void(0);' onclick='$onclick'>" . $row -> booking_number . "</a>";

				$nestedData[] = $booking_detail;
				$nestedData[] = $row -> pickup_location;
				$nestedData[] = date('d-m-Y H:i', strtotime($row -> booking_time));
				$nestedData[] = gmt_to_norway(($row -> createdon));
				$nestedData[] = $row -> name;

				//$nestedData[] = $row -> destination_location;
				$nestedData[] = $cab_type;
				//$nestedData[] = $this -> UserBookingCount($id);
				$nestedData[] = $action;
				$data[] = $nestedData;
			}

			$json_data = array(
				"draw" => intval($requestData['draw']), // for every request/draw by clientside ,
				// they send a number as a parameter, when they recieve a response/data they
				// first check the draw number, so we are sending same number in draw.
				"recordsTotal" => intval($totalData), // total number of records
				"recordsFiltered" => intval($totalFiltered), // total number of records after
				// searching, if there is no searching then totalFiltered = totalData
				"data" => $data // total data array
			);
			//print_r($json_data);
			return $json_data;
			//echo json_encode($json_data);  // send data as json format
		}

		/*fetch count aqdvance booking */
		function count_advance_booking()
		{
			$date = date('Y-m-d');
			$result = $this -> db -> query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name FROM `cab_booking` join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and (cab_booking.status in ('CONFIRMED'))  and date(booking_time)>='$date'");
			// echo $this->db->last_query();
			return $result -> num_rows();

		}

		function GetNewPreBooking()
		{
			$requestData = $_REQUEST;

			$columns = array(
				// datatable column index  => database column name
				0 => 'name',
				1 => 'email',
				//2 => 'quantity',
				//3 => 'package.name',
			);
			$company_id = $this -> session -> userdata('company_id');
			//$company_id=4;
			$query = $this -> db -> get_where('user', array("company_id" => $company_id));
			//	echo $this->db->last_query();

			$totalData = $query -> num_rows();
			//echo $totalFiltered = $totalData;
			//exit;

			$sql = "SELECT user.name,user.email,user.country_code,user.phone,user.id,user.is_active FROM `user` where is_deleted='0'  and user.company_id=$company_id";
			//exit;
			if (!empty($requestData['search']['value']))
			{
				// if there is a search parameter, $requestData['search']['value'] contains
				// search parameter
				$sql .= " AND ( name LIKE '" . $requestData['search']['value'] . "%' ";
				$sql .= " OR email LIKE '%" . $requestData['search']['value'] . "%' ";
				$sql .= " OR phone LIKE '%" . $requestData['search']['value'] . "%' )";
			}
			$query = $this -> db -> query($sql);
			//echo $this->db->last_query();
			//exit;
			$totalFiltered = $query -> num_rows();

			$sql .= " ORDER BY  id asc limit " . $requestData['start'] . " ," . $requestData['length'] . "   ";

			$query = $this -> db -> query($sql);
			// $this->db->last_query();
			$data = array();

			// preparing an array
			foreach ($query->result() as $row)
			{
				$nestedData = array();
				$id = $row -> id;

				//	$package_count = $this -> UserPackageCount($id); onclick="."deleteemp(this)"."
				// $deletebrand=  "<a style=' cursor: pointer;' id='$id'
				// onclick='DeleteEntry_package($id,".'"category"'.")'><i class=".'"fa
				// fa-trash-o"'."></i></a> | <a href='Package/Add/$id'  ><i class=".'"fa
				// fa-edit"'."></i></a>";
				$deletebrand = " <a href='#' id='$id' class='delete' onclick=" . "deleteemp($id)" . "  ><i class=" . '"fa fa-remove"' . "></i></a> ";
				$deletebrand .= "| <a href='employee/add/$id'  ><i class=" . '"fa fa-edit"' . "></i></a>";

				$nestedData[] = $row -> name;
				$nestedData[] = $row -> email;
				$nestedData[] = ($row -> country_code) . $row -> phone;
				$nestedData[] = $this -> UserBookingCount($id);
				$nestedData[] = $deletebrand;
				$data[] = $nestedData;
			}

			$json_data = array(
				"draw" => intval($requestData['draw']), // for every request/draw by clientside ,
				// they send a number as a parameter, when they recieve a response/data they
				// first check the draw number, so we are sending same number in draw.
				"recordsTotal" => intval($totalData), // total number of records
				"recordsFiltered" => intval($totalFiltered), // total number of records after
				// searching, if there is no searching then totalFiltered = totalData
				"data" => $data // total data array
			);
			//print_r($json_data);
			return $json_data;
			//echo json_encode($json_data);  // send data as json format
		}

		/*fetch drive rfor admin work web */
		function fetch_driver()
		{
			$result = $this -> db -> query("select * from driver where isdelete='0'");
			if ($result -> num_rows() > 0)
			{
				return $result -> result_array();
			}
			else
			{
				return false;
			}

		}

		/* update advance booking admin*/
		function update_booking($bookingid, $driverid, $type)
		{
			$driver_assgin = get_gmt_time();
			if ($type == 'auto_accept')
			{
				$result = $this -> db -> query("Update cab_booking SET driver_assigned_date='$driver_assgin',driver_id=$driverid where id='$bookingid'");
			}
			else
			{
				$result = $this -> db -> query("Update cab_booking SET driver_assigned_date='$driver_assgin',driver_id=$driverid,status='DRIVER_ASSIGNED' where id='$bookingid'");
			}

			if ($result)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		function get_customer_number($user_id)
		{
			$this -> db -> select("country_code,phone");
			$this -> db -> where("id", $user_id);
			$query = $this -> db -> get("user");
			if ($query -> num_rows() > 0)
			{
				$data = $query -> result_array();
				return $data;
			}
			else
			{
				return false;
			}
		}

		function get_driver_number($driver_id)
		{
			$this -> db -> select("contact");
			$this -> db -> where("driver_id", $driver_id);
			$query = $this -> db -> get("driver");
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$data = $query -> result_array();
				return $data;
			}
			else
			{
				return false;
			}
		}

		/* fetch general info */
		function fetch_general($input_method)
		{
			$user_id = @$input_method['user_id'];
			$query = $this -> db -> query("select item_id,UPPER(item_name) item_name,item_price,item_code from item_info where isactive='1'");
			if ($query -> num_rows() > 0)
			{
				$item = $query -> result_array();
			}
			$fare = $this -> db -> query("select description,no_of_person as person_count,min_charges,min_km,charges_per_km,wait_time_charges,ride_later_time,cab_type.title,cab_type.type_id as cab_type from fare_info join cab_type on fare_info.cab_type=cab_type.type_id where fare_info.type='NORMAL' and cab_type.isactive='1'");
			if ($fare -> num_rows() > 0)
			{
				$fare_data = $fare -> result_array();
			}
			if ($user_id)
			{
				//get the ride info
				//get the current runing info

				$this -> db -> select("driver.driver_id,driver.profile_pic,driver.first_name,driver.last_name,driver.latitude,driver.longitude,cab_booking.id as booking_id,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.booking_number,cab_type.title,cab_booking.plate_number,driver.contact,cab_booking.status");
				$this -> db -> from("cab_booking");
				$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
				$this -> db -> join("cab_type", "cab_type.type_id=cab_booking.cab_type");
				// $this->db->join("driver_status", "driver_status.booking_id=cab_booking.id");
				$this -> db -> where("cab_booking.user_id", $user_id);
				$this -> db -> where("cab_booking.status", "TRIP_STARTED");
				$query = $this -> db -> get();
				// echo $this->db->last_query();
				if ($query -> num_rows() > 0)
				{
					$booking_alert = $query -> result_array();
				}
				else
				{
					$this -> db -> select("driver.driver_id,driver.profile_pic,driver.first_name,driver.last_name,driver.latitude,driver.longitude,cab_booking.id as booking_id,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.booking_number,cab_type.title,cab_booking.plate_number,driver.contact,cab_booking.status");
					$this -> db -> from("cab_booking");
					$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
					$this -> db -> join("cab_type", "cab_type.type_id=cab_booking.cab_type");
					$this -> db -> join("driver_status", "driver_status.booking_id=cab_booking.id");
					$this -> db -> where("cab_booking.user_id", $user_id);
					$this -> db -> where("cab_booking.status", "CUSTOMER_ACCEPTED");
					// echo $this->db->last_query();
					if ($query -> num_rows() > 0)
					{
						$booking_alert = $query -> result_array();
					}
					else
					{
						$booking_alert = array();
					}

				}

			}
			else
			{
				$booking_alert = array();
			}
			// print_r($booking_alert);
			return array(
				"item" => $item,
				"fare" => $fare_data,
				"booking_alert" => $booking_alert
			);
		}

		/*fetch completed booking */
		function fetch_completed_booking($limit, $offset = 0)
		{
			//c.status='FINISH' and
			$fetch_data = $this -> db -> query("select c.booking_number,u.name,c.booking_time,c.id,c.pickup_location,c.destination_location,p.billing_amount,p.km_driven,p.user_paid
             from cab_booking as c,payment_detail as p,user as u where p.booking_id=c.id and c.status in ('FINISH','TRIP_ENDED') and
              u.id=c.user_id order by c.booking_time DESC limit $offset,$limit");
			// echo $this->db->last_query();
			if ($fetch_data -> num_rows() > 0)
			{
				return $fetch_data -> result_array();
			}
			else
			{
				return false;
			}
		}

		/*fetch completed booking */
		function fetch_unpaid_booking()
		{
			//c.status='FINISH' and
			$fetch_data = $this -> db -> query("select c.booking_number,u.name,c.booking_time,c.id,c.pickup_location,c.destination_location,p.billing_amount,p.km_driven,p.user_paid
             from cab_booking as c,payment_detail as p,user as u where p.booking_id=c.id and c.status in ('TRIP_ENDED') and
              u.id=c.user_id order by c.booking_time DESC limit $offset,$limit");
			// echo $this->db->last_query();
			if ($fetch_data -> num_rows() > 0)
			{
				return $fetch_data -> result_array();
			}
			else
			{
				return false;
			}
		}

		/* fetch total cash */
		function total_cash()
		{
			$total_amount = $this -> db -> query("SELECT SUM(user_paid) as total_amount FROM `payment_detail`");
			if ($total_amount -> num_rows() > 0)
			{
				return $total_amount -> result_array();
			}
			else
			{
				return false;
			}
		}

		/* function for fetch comopany due payment */
		function company_due($limit, $offset = 0)
		{
			$total_due = $this -> db -> query("SELECT user.name,user.email,cab_booking.booking_number,p.billing_amount,p.id,p.is_paid FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where cab_booking.status='TRIP_ENDED' and user.is_company='Y' and p.is_paid='0'limit $offset,$limit");
			if ($total_due -> num_rows() > 0)
			{
				return $total_due -> result_array();
			}
			else
			{
				return false;
			}
		}

		/*fetch particular user payment for making pdf */
		function company_user_due($b_number)
		{
			//echo $uid;
			if ($b_number)
			{
				$total_due = $this -> db -> query("SELECT user.name,user.company_name, cab_booking.booking_number,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude,user.id as uid,user.name,user.email,p.is_paid,p.billing_amount,p.km_driven FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where cab_booking.status='TRIP_ENDED' and user.is_company='Y' and cab_booking.booking_number='$b_number'");
				//echo "<br>".$this->db->last_query();
				if ($total_due -> num_rows() > 0)
				{
					return $total_due -> result_array();
				}
				else
				{
					return false;
				}

			}
		}

		/* fetch count due user payment */
		function count_company_due()
		{
			$result = $this -> db -> query("SELECT user.name,user.email,cab_booking.booking_number,p.billing_amount,p.id,p.is_paid FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where cab_booking.status='TRIP_ENDED' and user.is_company='Y' and p.is_paid='0'");
			return $result -> num_rows();
		}

		/* chnage payment status */
		function change_payment_status($id, $status)
		{
			if ($status == "1")
			{
				$field_array = array("is_paid" => "1");
				$status == '1';
			}
			else
			{
				$field_array = array("is_paid" => "0");
				$status == '0';
			}
			$this -> db -> where('id', $id);
			$this -> db -> update("payment_detail", $field_array);
			//echo "<br>".$this->db->last_query();
			return $status;
		}

		/* fetch complete payment detail */

		function Complete_payment_detail($start_date, $end_date, $c_id, $limit, $offset = 0)
		{

			$complete_booking = $this -> db -> query("SELECT user.name,user.phone,user.company_name,user.email,cab_booking.booking_number,cab_booking.id as bid,p.billing_amount,p.km_driven,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude,cab_booking.pickup_location,cab_booking.destination_location FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where cab_booking.company_id='$c_id' and date(cab_booking.booking_time) BETWEEN '$start_date' AND '$end_date' ");
			// echo $this->db->last_query();
			// $complete_booking = $this->db->query("SELECT
			// user.name,user.phone,user.company_name,user.email,cab_booking.booking_number,p.billing_amount,p.km_driven,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude
			// FROM `cab_booking` join user on user.id=cab_booking.user_id left join
			// payment_detail p on p.booking_id=cab_booking.id where user.is_company='Y' and
			// p.is_paid='1' ");
			if ($complete_booking -> num_rows() > 0)
			{
				return $complete_booking -> result_array();
			}
			else
			{
				return false;
			}
		}

		/* fetch count complete payment detail */
		function count_compelte_payment($start_date, $end_date, $c_id)
		{

			$complete_booking = $this -> db -> query("SELECT user.name,user.phone,user.company_name,user.email,cab_booking.booking_number,p.billing_amount,p.km_driven,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where cab_booking.company_id='$c_id' and date(cab_booking.booking_time) BETWEEN '$start_date' AND '$end_date'");
			//echo $this->db->last_query();
			return $complete_booking -> num_rows();

		}

		/* fetch user detail on basis of if */
		function fetch_user_paymentdetail($id)
		{

			$fetch_user_detail = $this -> db -> query("SELECT cab_booking.booking_number,p.billing_amount,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where  user.id='$id' and p.is_paid='1'");
			if ($fetch_user_detail -> num_rows > 0)
			{
				return $fetch_user_detail -> result_array();
			}
			else
			{
				return false;
			}

		}

		/* update payment status */

		function update_payment($bno, $pstatus, $description)
		{
			$update_pstatus = $this -> db -> query("UPDATE payment_detail SET is_paid='$pstatus',description='$description' where id='$bno'");
			if ($update_pstatus)
			{
				return true;
			}
			else
			{
				return false;
			}

		}

		/* function for tracking driver */

		function fetch_completed_booking_count()
		{
			$fetch_data = $this -> db -> query("select c.id,c.booking_number, c.booking_time,c.pickup_latitude,c.pickup_longitude,c.destination_latitude,c.destination_longitude,p.billing_amount,p.km_driven,p.user_paid
from cab_booking as c,payment_detail as p where p.booking_id=c.id and c.status='FINISH'");
			if ($fetch_data -> num_rows() > 0)
			{
				return $fetch_data -> num_rows();
			}
			else
			{
				return 0;
			}
		}

		/* fetch current booking count */

		function fetch_current_booking_count()
		{
			$fetch_current = $this -> db -> query("select c.driver_id,c.id, c.booking_number,c.status,c.booking_time,d.first_name,d.last_name,d.contact,c.id from cab_booking as c,driver as d where d.driver_id=c.driver_id and c.status='TRIP_STARTED'");
			if ($fetch_current -> num_rows() > 0)
			{
				return $fetch_current -> num_rows();

			}
			else
			{
				return false;
			}
		}

		/* fetch current booking */
		function fetch_current_booking()
		{
			$fetch_current = $this -> db -> query("select c.driver_id,c.booking_number,c.status,c.booking_time,d.first_name,d.last_name,d.contact,c.id from cab_booking as c,driver as d where d.driver_id=c.driver_id and c.status='TRIP_STARTED'");
			if ($fetch_current -> num_rows() > 0)
			{
				return $fetch_current -> result_array();
			}
			else
			{
				return 0;
			}
		}

		function user_exist($user_id)
		{
			$this -> db -> select("user.id,user.is_company,card_info.braintree_customer_id");
			$this -> db -> where("user.id", $user_id);
			$this -> db -> from("user");
			$this -> db -> join("card_info", "card_info.customer_id=user.id", "left");
			$query = $this -> db -> get();
			if ($query -> num_rows())
			{
				$user_detail = $query -> result_array();
				if ($user_detail[0]['braintree_customer_id'] != "")
				{
					return array(
						"status" => 1,
						"message" => "success"
					);
				}
				elseif ($user_detail[0]['braintree_customer_id'] == "" && $user_detail[0]['is_company'] == "Y")
				{
					return array(
						"status" => 1,
						"message" => "success"
					);
				}
				else
				{
					return array(
						"status" => 2,
						"message" => CARD_INFO_MISSING
					);
				}

			}
			else
			{
				return array(
					"status" => 0,
					"message" => USER_NOT_EXIST
				);
			}
		}

		function get_cab_plate_number($driver_id)
		{
			$shift_start = DRIVER_SHIFT_START;
			$this -> db -> select("cab_plate_no");
			$this -> db -> from("shift");
			$this -> db -> join("cab", "cab.cab_id=shift.cab_id");
			$this -> db -> where("shift.status", "$shift_start");
			$this -> db -> where("shift.driver_id", $driver_id);
			$query = $this -> db -> get();
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$data = $query -> result_array();
				return $data[0]['cab_plate_no'];
			}
			else
			{
				return "";
			}
		}

		function get_coupon_detail($coupon_code)
		{
			//  $user_id=$input_method['user_id'];
			$today = date("Y-m-d");
			//$coupon_code=$input_method['coupon_code'];
			$this -> db -> select("*");
			$this -> db -> from("coupon");
			$this -> db -> where("code", $coupon_code);
			$this -> db -> where("isactive", "1");
			$this -> db -> where("start_date<=", $today);
			$this -> db -> where("expire_date>=", $today);
			$query = $this -> db -> get();
			// echo $this->db->last_query();
			if ($query -> num_rows())
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}

		}

		function driver_track($bid)
		{
			$track = $this -> db -> query("SELECT * FROM `driver_status` where booking_id='$bid' and (status='FIVE_MIN_AWAY' OR 'I_AM_HERE') limit 1 ");
			if ($track -> num_rows > 0)
			{
				return "Y";
			}
			else
			{
				return "N";
			}
		}

		function user_braintree_token($user_id)
		{
			$this -> db -> select("braintree_token");
			$this -> db -> from("card_info");
			$this -> db -> where("customer_id", $user_id);
			$this -> db -> where("isactive", "1");
			$query = $this -> db -> get();
			if ($query -> num_rows())
			{
				$token_data = $query -> result_array();

				return $token_data[0]['braintree_token'];
			}
			else
			{
				return false;
			}

		}

		function save_payment($data)
		{
			$this -> db -> insert("payment_detail", $data);
			//echo $this->db->last_query();
			return $this -> db -> insert_id();
		}

		/* fetch company detail */
		function get_company()
		{
			$get_company = $this -> db -> query("select * from company where isdelete='0'");
			$count = $get_company -> num_rows();
			if ($count >= 0)
			{
				return $get_company -> result();
			}
			else
			{
				return array();
			}
		}

		function get_luxury_pre_booking()
		{
			$today = date("Y-m-d");
			$query = $this -> db -> query("select id,booking_time,status from cab_booking where cab_type='3' and date(booking_time)='$today'");
			// echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function update_booking_status($data)
		{
			$query = $this -> db -> query("update cab_booking set status='" . $data['status'] . "' where id='" . $data['booking_id'] . "'");
			if ($query)
			{
				//make driver available
				$driver_status = DRIVER_AVAILABLE;
				$driver_id = $data['driver_id'];
				$this -> db -> where("driver_id", $driver_id);
				$update_data = array("status" => $driver_status);
				$update_query = $this -> db -> update('driver', $update_data);
				return true;
			}
			else
			{
				return false;
			}

		}

		function getdriver_currentbooking($driver_id)
		{
			$query = $this -> db -> query("SELECT pickup_latitude,pickup_longitude,destination_latitude,destination_longitude,status FROM `cab_booking` WHERE driver_id=$driver_id and status not in ('NOT_SHOW','FINISH','CANCELLED','TRIP_ENDED','DRIVER_CANCELLED','CUSTOMER_REJECTED','CUSTOMER_NOT_RESPOND','DRIVER_ASSIGNED','ADVANCE_BOOKING')");
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$result = $query -> result_array();
				$destination_latitude = $result[0]['destination_latitude'];
				$destination_longitude = $result[0]['destination_longitude'];
				$data = array(
					"destination_latitude" => $destination_latitude,
					"dda" => "12",
					"destination_longitude" => $destination_longitude,
					"status" => 1
				);
				return $data;
			}
			else
			{
				return array(
					"status" => 0,
					"message" => "No current booking"
				);
			}
		}

		function get_pre_booking($booking_no, $type = 'NORMAL')
		{
			if ($type == 'NORMAL')
			{
				$get_advance_booking = $this -> db -> query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.comment,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name FROM `cab_booking` left join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.id='$booking_no'");
			}
			else
			{
				$get_advance_booking = $this -> db -> query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.comment,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,web_user.id as uid,web_user.name,web_user.phone,web_user.email,d.driver_id,d.first_name,d.last_name FROM `cab_booking` LEFT JOIN web_user ON web_user.booking_id = cab_booking.id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and cab_booking.id='$booking_no'");
			}
			//echo $this->db->last_query();
			return $get_advance_booking -> result();
		}

		function getdriverstatus($driver_id)
		{
			$query = $this -> db -> query("select status from driver where driver_id=$driver_id");
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}

		}

		/*get previous driver */
		function get_previous_driver($bookingid)
		{
			$get_driver_no = $this -> db -> query("select d.contact,c.booking_number from cab_booking as c,driver as d where c.is_prebooking='Y' and id='$bookingid' and c.driver_id=d.driver_id and  c.status= 'DRIVER_ASSIGNED'");
			$count = $get_driver_no -> num_rows();
			if ($count)
			{
				return $get_driver_no -> result_array();
			}
			else
			{
				return array();
			}
		}

		/* get pop up for complete booking */
		function get_complete_booking($booking_no)
		{
			$get_complete_booking = $this -> db -> query("select c.booking_number,u.name,t.createdon,t.status,c.booking_time,c.pickup_location,c.destination_location,p.billing_amount,p.km_driven,p.user_paid
from cab_booking as c,payment_detail as p,user as u,driver_status as t where p.booking_id=c.id  and u.id=c.user_id and c.id='$booking_no' and t.booking_id=p.booking_id and t.status IN('TRIP_STARTED','TRIP_ENDED','NOT_SHOW')");
			return $get_complete_booking -> result();
		}

		function get_all_booking($booking_no)
		{
			$get_complete_booking = $this -> db -> query("Select cab_booking.id,cab_booking.user_id,cab_booking.booking_time,cab_booking.booking_number,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.is_prebooking,cab_booking.driver_id,cab_booking.status, driver.first_name,driver.last_name, user.id,`user`.`name`, payment_detail.billing_amount,payment_detail.user_paid from cab_booking LEFT JOIN driver on driver.driver_id=cab_booking.driver_id LEFT JOIN `user` on `user`.`id`=`cab_booking`.`user_id` LEFT JOIN payment_detail on payment_detail.booking_id=cab_booking.id where cab_booking.id='$booking_no'");
			return $get_complete_booking -> result();
		}

		function count_get_all_booking_details()
		{
			$date = date('Y-m-d');
			$result = $this -> db -> query("Select cab_booking.id,cab_booking.user_id,cab_booking.booking_time,cab_booking.booking_number,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.driver_id,cab_booking.status, driver.first_name,driver.last_name from cab_booking LEFT JOIN driver on driver.driver_id=cab_booking.driver_id");
			// echo $this->db->last_query();
			return $result -> num_rows();

		}

		function get_all_booking_details($limit, $offset = 0)
		{
			$date = date('Y-m-d');
			//$result = $this -> db -> query("SELECT cab_booking.id as
			// bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,user.id
			// as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name
			// FROM `cab_booking` join user on user.id=cab_booking.user_id left join driver d
			// on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and
			// date(booking_time)>='$date' and (cab_booking.status not in
			// ('CUSTOMER_REJECTED','DRIVER_CANCELLED','CANCELLED')) ORDER BY booking_time
			// ASC  limit $offset,$limit");
			// echo $this->db->last_query();
			$result = $this -> db -> query("Select cab_booking.id,cab_booking.user_id,cab_booking.booking_time,cab_booking.is_prebooking,cab_booking.booking_number,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.driver_id,cab_booking.status, driver.first_name,driver.last_name from cab_booking LEFT JOIN driver on driver.driver_id=cab_booking.driver_id ORDER BY (cab_booking.booking_time) DESC LIMIT $offset,$limit");
			if ($result -> num_rows() > 0)
			{
				return $result -> result_array();
			}
			else
			{
				return false;
			}
		}

		function GetDriverDetail($booking_id)
		{
			$query = $this -> db -> get_where("booking_driverdetail", array("booking_id" => $booking_id));
			if ($query -> num_rows() > 0)
			{
				return $query -> row_array();
			}
			else
			{
				return FALSE;
			}

		}

		function get_company_booking_pop($booking_no)
		{
			$get_company_booking = $this -> db -> query("SELECT user.name,user.phone,user.company_name,t.createdon,t.status,user.email,cab_booking.booking_number,cab_booking.id as bid,p.billing_amount,p.km_driven,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.status as booking_status FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id left join driver_status t on t.booking_id=p.booking_id  and t.status in('TRIP_STARTED','TRIP_ENDED')  where cab_booking.id='$booking_no'");
			//and t.status IN('TRIP_STARTED','TRIP_ENDED') and (t.status='TRIP_STARTED'
			//echo $this->db->last_query();
			return $get_company_booking -> result();
		}

		function UpdateStatus($input_method)
		{
			$booking_id = $input_method['id'];
			$status = $input_method['status'];
			$update = array("status" => $status);
			$this -> db -> where("id", $booking_id);
			$this -> db -> update("cab_booking", $update);

			//get the user contact detail for sms
			$query = $this -> db -> query("SELECT concat(country_code,phone) phone,pickup_location,booking_time,email FROM `cab_booking` join user on user.id=cab_booking.user_id  WHERE cab_booking.id=$booking_id");
			//echo $this->db->last_query();
			if ($query -> num_rows())
			{
				$data = $query -> row_array();
				return $data;
			}

		}

		function AutoAcceptBooking()
		{
			$this -> db -> select("user.name,user.phone,cab_booking.booking_number,cab_booking.id,cab_booking.booking_time,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.cab_type");
			$this -> db -> from("cab_booking");
			$this->db->order_by("cab_booking.id","desc"); 
			$this -> db -> join("user", "user.id=cab_booking.user_id");
			$this -> db -> where("cab_booking.driver_id", "1");
			$this -> db -> where("cab_booking.status", "CUSTOMER_ACCEPTED");
			$query = $this -> db -> get();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function UnpaidBooking()
		{
			//SELECT id,booking_number,user_id,booking_time FROM `cab_booking` WHERE
			// status='TRIP_ENDED'
			$this->db->order_by("booking_time","desc");
			$this->db->where("status","TRIP_ENDED");
			$query = $this -> db -> get_where("cab_booking");
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return FALSE;
			}

		}

	}
?>
