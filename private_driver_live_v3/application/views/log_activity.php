<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Daily Logs
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Daily Logs</li>
          </ol>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         
                   
          
          
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daily Logs</h3>
             
             
            </div>
            <div class="box-body">
                
             <label>Date</label> <label style="padding-left:70%;">Select api</label>
            <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>                                              
              </div>
            <input type="text" class="form-control pull-left" id="bd" name="bd" onchange="getbookingdate();" style="width:25%;"/>
           
            <select id="apiname" style="width:250px;margin-left:455px;height:35px;" onchange="getbookingdate(this.value);">
               
                
                <option value="api/Driver/user_login"  id="apinames" name="apinames">Driver Login</option>
                <option value="api/Booking/trip_start" id="apinames" name="apinames">Trip Start</option>
                <option value="api/Booking/trip_end" id="apinames" name="apinames">Trip End</option>
                <option value="api/Payment/make_payment" id="apinames" name="apinames">Payment</option>
                <option value="api/Driver/shift_start" id="apinames" name="apinames">Shift Start</option>
                <option value="api/Driver/shift_end" id="apinames" name="apinames">Shift End</option>
                 <option value="api/Driver/customer_aware" id="apinames" name="apinames">5 min away</option>
                  <option value="api/Customer/user_login" id="apinames" name="apinames">customer login</option>
                
            </select>
            </div>
            <br/> <br/>
            <div id="data1">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr No</th>
                        
                        <th>Request Parameters</th>
                        <th>Response</th>
                        <th>Time</th>
                       
                       <!-- <th>Country</th>-->
                        
                      
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                 
                    //exit;
                   
                    for ($i = 0; $i < count($loglist); ++$i) {
                     // print_r($loglist);
                      ?>
                    <tr>
                        <td><?php echo ($page+$i+1); ?></td>
                      
                       
                        <?php $params= $loglist[$i]->params;
                        $new_param= wordwrap($params, 40, "\n", true);
                        $response=$loglist[$i]->response;
                        $new_response= wordwrap($response, 40, "\n", true);
                        ?>
                        <td><?php echo $new_param;?></td>
                        <td><?php echo $new_response; ?></td>
                         <td><?php echo $loglist[$i]->createdon;?></td>
                    </tr>
                    <?php } ?>
                      
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $pagination; ?>
                    </div>
        
          </div><!-- /.box -
                  </div>
                </div>-->
           </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy;2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
      <script type="text/javascript" src="<?=base_url();?>js/bootstrap-datepicker.js"></script>
<script>
     $(function() {
                    $( "#bd" ).datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true
                       
                        });
                   
                      });

</script>
<script>
    function  getbookingdate()
    {
        var booking_date=$('#bd').val();
        var apiid=$('#apiname').val();
       
         $.ajax({
            type:'POST',
            url: '<?php echo site_url('Activity/log_activity');?>',
            data:{"date":booking_date,"api_name":apiid},
            success: function(response) {
              
                 $("#data1").html(response);
              
             }});
        
    }
    
   
   
</script>