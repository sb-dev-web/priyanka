<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'/third_party/tcpdf/tcpdf.php';

class Pdf extends TCPDF 
{

    var $htmlHeader;
	
	public function __construct($header='') 
	{
		global $htmlHeader;
		$htmlHeader=$header;
		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	}

    public function setHtmlHeader($htmlHeader) 
	{
        $this->htmlHeader = $htmlHeader;
    }
	

    public function Header() 
	{
		global $htmlHeader;
		$page=parent::PageNo(); 	
	/*
		include("config.php");
		$source=mysql_query("select * from carready where carid=8");
		$val=mysql_fetch_array($source);

		$carid=8;
		$query=mysql_query("SELECT cm.*,ass.*,dd.* FROM carmaster cm JOIN accessories ass ON 		         ass.carid=cm.id JOIN damagedetail dd ON dd.carid=cm.id WHERE cm.id=8");
	
	
	$carval=mysql_fetch_array($query);
	$datetime = $carval['createdon'];
    $datetm = strtotime($datetime);
    $date = date('d-m-Y', $datetm); // d.m.YYYY
    $time = date('H:i:s', $datetm); // HH:ss
	$platenumber=$carval['plate_number'];
	$ranumber=$carval['ra_number'];
	$placename=$carval['placename'];
	$humidity=$carval['humidity'];
	$temp=$carval['temperature'];

$htmlHeader = '<table>
<tr><td></td><td></td></tr>
<tr><td><img src="logo.png"/></td>
<td><strong>VEHICLE PRE-INSPECTION REPORT</strong></td></tr></table>
<div style="height:5px"></div>
<table>
<tr><td>Date:-'.$date .' </td><td>Time:-'.$time.' </td>
</tr></table>
<div style="height:5px;"></div>
<div style="background-color:#D2D2FF">
<table align="center" width="90%">
<tr>
<td align="left"  width="33%"><strong>LICENCE PLATE</strong></td>
<td  width="40%"><strong>RENTAL RECORD/AGREEMENT NUMBER</strong></td>
<td align="right" width="30%"><strong>LOCATION</strong></td>
</tr>
</table>
</div>
<table width="90%" align="center">
<tr><td align="left">'.$platenumber.'</td>
<td>'.$ranumber.'</td>
<td align="right">'.$placename.'</td>
</tr>
</table>
<div style="height:5px;"></div>
<div style="background-color:#D2D2FF";>
<table align="center" width="80%">
<tr><td align="center"><strong>WEATHER TODAY:'.$humidity. $temp.'&deg C</strong></td></tr></table>
</div>
<div align="center" style="font-family:Adobe Garamond Pro Bold, Bell MT">The terms and conditions of your record number above apply to this form</div><br />';
        $this->SetFont('helvetica', 'B', 8);*/
		
		$headerwithnumber = str_replace("<sheet_number>",$page,$htmlHeader);
		$this->writeHTML($headerwithnumber, true, false, false, false, '');
    }

}
?>