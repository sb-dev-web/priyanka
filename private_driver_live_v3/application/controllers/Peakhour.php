<?php
error_reporting(0);
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Peakhour extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url',
				'function'
			));
			$this -> load -> model('Peakhour_model');
			
			$this -> load -> library(array(
				'session',
				'email',
			
			));
			$user = $this -> session -> userdata('logged_in');
			if (!$user)
			{
				redirect('Welcome');
			}

		}

		function index()
		{
			if ((isset($_POST['date'])) && ($_POST['type']))
			{
				 $date = $_POST['date'];
				 $type=$_POST['type'];
				if($_POST['type']=='Driver')
				{
					$id = $_POST['driverid'];
				}else
					{
						$id = $_POST['customerid'];
					}
				
				$this -> pick_data($date, $id,$type);
			}
			else
			{   $data['active_page'] = 'peakhour';
				$data['driver'] = $this -> Peakhour_model -> get_driver();
				$data['customer']=$this->Peakhour_model->get_customer();
				
				$this -> load -> view('site-nav/header',$data);
				$this -> load -> view('peakhour', $data);
				$this -> load -> view('site-nav/footer');
			}
		}

		function pick_data($date, $id,$type)
		{
			if($type=='Driver')
			{
				$peakhourdata = $this -> Peakhour_model -> DriverPeakhour($date, $id);
			}else
				{
					$peakhourdata = $this -> Peakhour_model -> CustomerPeakhour($date, $id);
				}
			
			if ($peakhourdata)
			{
				//print_r($peakhourdata);
				$data['peakhour'] = $peakhourdata;
				  $data['active_page'] = 'peakhour';
				// echo "<pre>";
				//  print_r($timeline_data);
				
			}
			else {
				$data['peakhour'] = false;
				  $data['active_page'] = 'driver_timeline';
			}
             $this -> load -> view('peakhour_report', $data);
		}

		function getbookingdetail()
		{
			 $booking_no = $_POST['booking_no'];
			$data['get_data'] = $this -> Drivertimeline_model -> get_bookingdetail($booking_no);

			$this -> load -> view('booking_detail', $data);
		}

	}
?>