<?php
error_reporting(0);
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class DriverTimeline extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url',
				'function'
			));
			$this -> load -> model('Drivertimeline_model');
			//$this -> load -> model('Activity_2_model');
			$this -> load -> model('Driver_model');
			//$this -> load -> library('Ajax_pagination');
			//->load->library(array('session', 'pagination'));
			$this -> load -> library(array(
				'session',
				'email',
			//	'paginationlib',
			//	'Pagination'
			));
			$user = $this -> session -> userdata('logged_in');
			if (!$user)
			{
				redirect('Welcome');
			}

		}

		function index()
		{
			if ((isset($_POST['date'])) && ($_POST['driverid']))
			{
				 $date = $_POST['date'];
				$dates = explode('to', $date);
				$start_date = $dates[0];
				$end_date = $dates[1];
				$driver_id = $_POST['driverid'];
				$this -> drivertimeline_data($date, $driver_id);
			}
			else
			{   $data['active_page'] = 'driver_timeline';
				$data['fetch_driver'] = $this -> Driver_model -> get_driver_name();
				
				$this -> load -> view('site-nav/header',$data);
				$this -> load -> view('shift_report', $data);
				$this -> load -> view('site-nav/footer');
			}
		}

		function drivertimeline_data($start_date, $driver_id)
		{
			$shift_detail = $this -> Drivertimeline_model -> drivertime_report($start_date, $end_date, $driver_id);
			if ($shift_detail)
			{
				foreach ($shift_detail as $drivertimeline)
				{
					if ($drivertimeline['status'] == 'SHIFT_STARTED' || $drivertimeline['status'] == 'SHIFT_ENDED')
					{
						//get the shift detail
						$shift_detail = $this -> Drivertimeline_model -> get_shiftdetail($drivertimeline['shift_id']);
						if ($shift_detail)
						{
							$drivertimeline['cab_type'] = $shift_detail['title'];
							$drivertimeline['plate_number'] = $shift_detail['cab_plate_no'];
							$drivertimeline['cab_model'] = $shift_detail['cab_model'];
						}
					}
					$data[] = $drivertimeline;
				}

				$data['report_data'] = $data;
				  $data['active_page'] = 'driver_timeline';
				// echo "<pre>";
				//  print_r($timeline_data);
				
			}
			else {
				$data['report_data'] = false;
				  $data['active_page'] = 'driver_timeline';
			}
             $this -> load -> view('total_shift_report', $data);
		}

		function getbookingdetail()
		{
			 $booking_no = $_POST['booking_no'];
			$data['get_data'] = $this -> Drivertimeline_model -> get_bookingdetail($booking_no);

			$this -> load -> view('booking_detail', $data);
		}

	}
?>