<?php

	class Customer_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		/* customer already register */
		function check_exist_email($input_method)
		{

			if (isset($input_method['email']))
				$email1 = $input_method['email'];
			$query1 = "SELECT id,is_social,email,is_company,is_active from user where is_deleted='0' and email='$email1'";
			$query = $this -> db -> query($query1);
			if ($query -> num_rows() > 0)
			{
				return $query -> result();
			}
			else
			{
				$query1 = "SELECT id,is_social,email,is_company,is_active from user_temp where email='$email1'";
				$query = $this -> db -> query($query1);
				if ($query -> num_rows() > 0)
				{
					return $query -> result();
				}
				else
				{
					return false;
				}
			}

		}

		function check_exist_email_temp($input_method)
		{
			$email = $input_method['email'];
			$phone = "+" . $input_method['country_code'] . $input_method['phone'];
			//chk in temp table
			$query = "SELECT id,is_social,email,is_emailverify,is_varify from user_temp where email='$email' and concat(country_code,phone)='$phone'";
			$query = $this -> db -> query($query);
			if ($query -> num_rows() > 0)
			{

				return array(
					"status" => 1,
					"type" => "",
					"message" => SUCCESS
				);
			}
			else
			{
				$query = "SELECT id,is_social,email from user where email='$email'";
				$query = $this -> db -> query($query);
				if ($query -> num_rows() > 0)
				{

					return array(
						"status" => 0,
						"type" => "main",
						"message" => str_replace('<email>', $email, EMAIL_ALREADY_REGISTER)
					);
				}
				else
				{
					$query = "SELECT id,is_social,email from user where concat(country_code,phone)='$phone'";
					$query = $this -> db -> query($query);
					if ($query -> num_rows() > 0)
					{
						return array(
							"status" => 0,
							"type" => "main",
							"message" => str_replace('<mobile>', $phone, MOBILE_ALREADY_REGISTER)
						);
					}
					else
					{

						return array(
							"status" => 1,
							"type" => "",
							"message" => SUCCESS
						);
					}
				}

			}
		}

		/* native login user */
		function check_login($input_method)
		{
			$password = md5($input_method['password']);
			$this -> db -> select("user.*,card_info.credit_card_no,card_type,card_info.braintree_customer_id");
			$this -> db -> where('user.email', $input_method['email']);
			$this -> db -> where("user.password", $password);
			$this -> db -> where("is_active", '1');
			$this -> db -> where("is_deleted", '0');
			$this -> db -> from("user");
			$this -> db -> join("card_info", "card_info.customer_id=user.id", "left");

			$query = $this -> db -> get();
			//	echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> result();
			}
			else
			{
				$this -> db -> select("user_temp.*,card_info_temp.credit_card_no");
				$this -> db -> where('email', $input_method['email']);
				$this -> db -> where("password", $password);
				$this -> db -> where("is_active", '1');
				$this -> db -> where("is_active", '1');
				$this -> db -> from("user_temp");
				$this -> db -> join("card_info_temp", "card_info_temp.customer_id=user_temp.id", "left");
				$query = $this -> db -> get();
				if ($query -> num_rows() > 0)
				{
					return $query -> result();
				}
			}
		}

		function GetCompanyDetail($user_id)
		{
			$this -> db -> select("company.company_name,user_company.company_id,user_company.is_approved,user_company.company_name as usercompany");
			$this -> db -> from("user_company");

			$this -> db -> join("company", "company.id=user_company.company_id", "left");
			$this -> db -> where("user_company.user_id", $user_id);
			$this -> db -> where("user_company.is_delete", 0);

			$query = $this -> db -> get();
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> row_array();
			}
			else
			{
				return false;
			}

		}

		/* save customer registration information */
		function save_user_old($input_method, $braintree_status)
		{
			$name = rawurldecode($input_method['name']);
			$email = $input_method['email'];
			$dob = $input_method['dob'];
			$phone = $input_method['phone'];

			if ($input_method['is_social'] == 'Y')
			{
				$password = "";
			}
			else
			{
				$password = md5(@$input_method['password']);
				$input_method['is_social'] = 'N';
			}
			$credit_card_no = $input_method['credit_card_no'];
			$credit_card_no = credit_card_number($credit_card_no);
			$varifycode = $input_method['varifycode'];
			$country_code = "+" . $input_method['country_code'];
			$gmdate = get_gmt_time();
			$query = $this -> db -> query("insert into user (name,email,password,phone,country_code,dob,varifycode,is_social,createdon) VALUES('$name','$email','$password','$phone','$country_code','$dob',$varifycode,'" . $input_method['is_social'] . "','$gmdate')");
			//echo $this->db->last_query();
			$customer_id = $this -> db -> insert_id();
			if ($customer_id)
			{
				$gmdate = get_gmt_time();
				$bt_customer_id = $braintree_status['customer_id'];
				$bt_customer_token = $braintree_status['token'];
				$card_type = $braintree_status['cardtype'];
				$cc_data = array(
					"credit_card_no" => $credit_card_no,
					"customer_id" => $customer_id,
					"braintree_customer_id" => $bt_customer_id,
					"braintree_token" => $bt_customer_token,
					"card_type" => $card_type,
					"createdon" => $gmdate
				);
				$query = $this -> db -> insert("card_info", $cc_data);
				$user_cc_id = $this -> db -> insert_id();
				if ($user_cc_id)
				{
					return $customer_id;
				}
				return $customer_id;
			}
			/* insert credit card info */

		}

		function save_user($input_method, $braintree_status)
		{
			$name = rawurldecode($input_method['name']);
			$email = $input_method['email'];
			$dob = $input_method['dob'];
			$phone = $input_method['phone'];

			if ($input_method['is_social'] == 'Y')
			{
				$password = "";
				$is_emailverify = "1";
			}
			else
			{
				$password = md5(@$input_method['password']);
				$input_method['is_social'] = 'N';
				$pass = $input_method['password'];
				$is_emailverify = "0";
			}
			$credit_card_no = $braintree_status['last4'];
			$credit_card_no = credit_card_number($credit_card_no);
			$varifycode = $input_method['varifycode'];
			$country_code = "+" . $input_method['country_code'];
			$email_code = $input_method['email_code'];
			$gmdate = get_gmt_time();
			//delete user from temp if any one used the same email or phone
			$this -> delete_temp_user($country_code . $phone, $email);
			$query = $this -> db -> query("insert into user_temp (name,email,password,pass,phone,country_code,dob,varifycode,is_social,createdon,email_code,is_emailverify) VALUES('$name','$email','$password','$pass','$phone','$country_code','$dob',$varifycode,'" . $input_method['is_social'] . "','$gmdate',$email_code,'$is_emailverify')");
			//echo $this->db->last_query();
			$customer_id = $this -> db -> insert_id();
			if ($customer_id)
			{
				$gmdate = get_gmt_time();
				$bt_customer_id = $braintree_status['customer_id'];
				$bt_customer_token = $braintree_status['token'];
				$card_type = $braintree_status['cardtype'];
				$cc_data = array(
					"credit_card_no" => $credit_card_no,
					"customer_id" => $customer_id,
					"braintree_customer_id" => $bt_customer_id,
					"braintree_token" => $bt_customer_token,
					"card_type" => $card_type,
					"expire_date" => $braintree_status['expire_date'],
					"createdon" => $gmdate
				);
				$query = $this -> db -> insert("card_info_temp", $cc_data);
				//echo  $this->db->last_query();
				$user_cc_id = $this -> db -> insert_id();
				if ($user_cc_id)
				{
					return $customer_id;
				}
				return $customer_id;
			}
			/* insert credit card info */

		}

		function delete_temp_user($phone, $email)
		{

			$query = "delete from user_temp where email='$email' or concat(country_code,phone)='$phone'";
			$this -> db -> query($query);
		}

		function get_user($id = "")
		{
			if ($id > 0)
				$query = $this -> db -> query("select user.id, user.name,user.dob,user.company_name as user_company_name,user.email,user.phone,user.country_code,user.is_social,user.company_id,user.is_emailverify,user.mode,c.credit_card_no,company.company_name from user Left JOIN card_info as c on c.customer_id=user.id left join company  on company.id=user.company_id where user.id='$id'");
			return $query -> result();

		}

		/* fetch user information */
		function get_user_detail($input_method)
		{
			$email = $input_method['email'];
			$this -> db -> select("user.name,user.id,user.company_name,user.id as user_id,user.mode,user.dob,user.email,user.phone,user.country_code,user.is_varify,user.is_company,user.s_id,user.is_emailverify,card_info.credit_card_no,card_info.customer_id,user.varifycode,user.is_active,user.reciept_enable");
			$this -> db -> where('email', $email);
			$this -> db -> where('is_active', "1");
			$this -> db -> from("user");
			$this -> db -> join("card_info", "card_info.customer_id=user.id", "left");

			$query = $this -> db -> get();
			if ($query -> num_rows() > 0)
			{
				//echo $this->db->last_query();exit;
				return $query -> result();
			}
			else
			{
				return false;
			}

		}

		// temp function for user , Now i have impletement in the above functions
		function update_user($input_method)
		{
			$email = $input_method['email'];
			$name = $input_method['name'];
			$dob = @$input_method['dob'];
			$s_id = $input_method['s_id'];
			$this -> db -> where('email', $email);
			$query = $this -> db -> query("UPDATE user SET name='$name',dob='$dob',s_id='$s_id' where email='$email'");
			return $this -> db -> affected_rows();
		}

		/* update device */
		function update_device($input_method)
		{
			$update_array = array(
				"device_token" => $input_method['device_token'],
				"device_type" => $input_method['device_type']
			);
			$this -> db -> where('email', $input_method['email']);
			$query = $this -> db -> update('user', $update_array);
			return $this -> db -> affected_rows();
		}

		/**Save the company user detail in temp table
		 * */
		function SaveCompanyUser($input_method)
		{
			$country_code = "+" . $input_method['country_code'];
			$email = $input_method['email'];
			$phone = $input_method['phone'];
			$this -> delete_temp_user($country_code . $phone, $email);
			$input_data = array(
				"name" => $input_method['name'],
				"company_name" => $input_method['company_name'],
				"email" => $input_method['email'],
				"password" => md5($input_method['password']),
				"phone" => $input_method['phone'],
				"country_code" => "+" . $input_method['country_code'],
				"varifycode" => $input_method['varifycode'],
				"is_company" => '1',
				"email_code" => $input_method['email_code'],
				"is_emailverify" => '0',
				"pass" => $input_method['password'],
				"mode" => "COMPANY",
			);
			$company_user = $this -> db -> insert("user_temp", $input_data);
			if ($this -> db -> insert_id() > 0)
			{
				return $this -> db -> insert_id();
			}
			else
			{
				return false;
			}
		}

		/* saving company employee information */
		function save_userinfo_company($input_method)
		{
			$email = $input_method['email'];
			$name = rawurldecode($input_method['name']);
			$c_name = rawurldecode($input_method['company_name']);
			//$password = md5($input_method['password']);
			$phone = $input_method['phone'];
			$country_code = '+' . $input_method['country_code'];
			$varifycode = $input_method['varifycode'];
			$gmdate = get_gmt_time();
			$auth_token = $input_method['auth_token'];
			$this -> db -> trans_begin();

			$query = $this -> db -> query("insert into user (name,company_name,email,phone,is_active,is_company,country_code,varifycode,createdon,mode,auth_token,is_social) VALUES('$name','$c_name','$email','$phone','0','Y','" . $country_code . "','" . $varifycode . "','$gmdate','COMPANY','$auth_token','N')");
			$user_id = $this -> db -> insert_id();
			//echo $this->db->last_query();
			//save the company detail in company user table
			$data = array(
				"user_id" => $user_id,
				"company_name" => $c_name,
				"is_approved" => "N",
				"created_on" => get_gmt_time(),
			);
			$this -> db -> insert("user_company", $data);
			//echo $this->db->last_query();
			if ($this -> db -> insert_id())
			{

				//make oprnfire user
				/*$data = array(
				 "email" => $email,
				 "pass" => $input_method['password'],
				 "name" => $name,
				 "user_id"=>$user_id
				 );*/
				$this -> db -> trans_commit();
				$this -> db -> trans_complete();
				return $user_id;
				//   print_r($data);
				//	$chatuser = $this -> MakeOpenfireUser($data);
				/*if ($chatuser)
				 {
				 $this -> db -> trans_commit();
				 $this -> db -> trans_complete();

				 return $user_id;
				 }
				 else
				 {
				 $this -> db -> trans_rollback();
				 $this -> db -> trans_complete();
				 return false;
				 }*/

			}
			else
			{
				$this -> db -> trans_rollback();
				$this -> db -> trans_complete();
				return false;
			}
		}

		//*************Invoice detail***********************************
		function invoice_detail($input_method)
		{
			$user_id = $input_method['user_id'];
			$this -> db -> select("*");
			$this -> db -> where("user_id", $user_id);
			$this -> db -> where("cab_booking.company_id", '0');
			$this -> db -> where("cab_booking.status", TRIP_ENDED);
			$this -> db -> where("cab_booking.payment_mode", "BT");
			$this -> db -> where("payment_detail.description!=", "success");
			$this -> db -> where("payment_detail.is_paid", "0");
			$this -> db -> join("payment_detail", "payment_detail.booking_id=cab_booking.id");
			//$this->db->where("cab_booking.id", $booking_Id);
			$this -> db -> from("cab_booking");
			$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
			$query = $this -> db -> get();
			// echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function device_register($input_method)
		{
			//delete device token if other user used the same
			$update_arr = array("device_token" => "");
			$this -> db -> where("device_token", $input_method['device_token']);
			$delete = $this -> db -> update("user", $update_arr);

			$update_data = array(
				"device_type" => $input_method['device_type'],
				"device_token" => $input_method['device_token'],
			);
			$this -> db -> where("id", $input_method['user_id']);

			$update = $this -> db -> update("user", $update_data);
			// echo $this->db->last_query();
			if ($update)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function driver_rating($input_method)
		{
			//Check customer already rating the same booking
			$booking_id = $input_method['booking_id'];
			$user_id = $input_method['user_id'];
			$driver_id = $input_method['driver_id'];
			$rating = $input_method['rating'];
			$gmdate = get_gmt_time();
			$this -> db -> select("id");
			$query = $this -> db -> get_where("driver_rating", array("booking_id" => $booking_id));
			//$this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return true;
			}
			else
			{
				$data = array(
					"booking_id" => $booking_id,
					"driver_id" => $driver_id,
					"user_id" => $user_id,
					"rating" => $rating,
					"createdon" => $gmdate,
				);
				$query = $this -> db -> insert("driver_rating", $data);
				if ($query)
				{
					return $this -> db -> insert_id();
				}
				else
				{
					return false;
				}
			}
		}

		function account_varify_old($input_method)
		{
			$otp = $input_method['otp'];
			$user_id = $input_method['user_id'];
			//check this varify code is exist or not
			$query = $this -> db -> get_where("user", array(
				"id" => $user_id,
				"varifycode" => $otp
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				//varify this account
				$update_data = array("is_varify" => 1);
				$this -> db -> where("id", $user_id);
				$query = $this -> db -> update("user", $update_data);

				if ($query)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}

		}

		function account_varify($input_method)
		{
			$otp = $input_method['otp'];
			$user_id = $input_method['user_id'];
			//check this varify code is exist or not
			$query = $this -> db -> get_where("user_temp", array(
				"id" => $user_id,
				"varifycode" => $otp
			));
			//echo $this -> db -> last_query();
			if ($query -> num_rows() > 0)
			{
				//userdetail
				$user_detail = $query -> row_array();
				//varify this account
				$update_data = array("is_varify" => 1);
				$this -> db -> where("id", $user_id);
				$query = $this -> db -> update("user_temp", $update_data);

				if ($query)
				{
					return $user_detail;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$query = $this -> db -> get_where("user", array(
					"id" => $user_id,
					"varifycode" => $otp
				));
				//echo $this -> db -> last_query();
				if ($query -> num_rows() > 0)
				{
					//userdetail
					$user_detail = $query -> row_array();
					//varify this account
					$update_data = array("is_varify" => 1);
					$this -> db -> where("id", $user_id);
					$query = $this -> db -> update("user", $update_data);

					if ($query)
					{
						return $user_detail;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}

		}

		function customer_not_respond($input_method)
		{
			//print_r($input_method);
			$driver_id = $input_method['driver_id'];
			$booking_id = $input_method['booking_id'];
			//make driver availble
			$update_array = array("status" => DRIVER_AVAILABLE);
			$this -> db -> where("driver_id", $driver_id);
			$this -> db -> update("driver", $update_array);
			$update_array = array("status" => CUSTOMER_NOT_RESPOND);
			$this -> db -> where("id", $booking_id);
			$update = $this -> db -> update("cab_booking", $update_array);
			$user_info = $this -> db -> query("select u.country_code,u.phone from user as u,cab_booking as c where c.user_id=u.id and c.id='$booking_id'");

			if ($user_info -> num_rows())
			{
				return $user_info -> result_array();
			}
			else
			{
				return false;
			}

		}

		function ride_history($input_method)
		{
			$customer_id = $input_method['customer_id'];
			$pagenumber = @$input_method['page_number'];
			if ($pagenumber == "")
			{
				$pagenumber = 1;
			}

			$limit = 10;
			if ($pagenumber == 1)
			{
				$offset = 0;
			}
			else
			{
				$pagenumber = $pagenumber - 1;
				$offset = $pagenumber * 10 + 1;
			}

			$query = $this -> db -> query("select b.id,b.booking_number,b.driver_id,b.plate_number,b.pickup_latitude,
			                            b.pickup_longitude,b.destination_latitude,b.destination_longitude,b.pickup_location,b.destination_location,b.
			                            booking_time,b.status,
			                           pd.booking_id,pd.journey_time,pd.billing_amount,pd.km_driven,pd.waiting_time,
			                           pd.waiting_charge,pd.extra_time,extra_km_charge,extra_timecharge,pd.extra_km,
			                           pd.tip,pd.user_paid,pd.is_paid,pd.discount_amount,pd.km_charge,pd.basic_fare_charge,
			                           ct.title 
                                       from cab_booking b 
                                       left join payment_detail pd on pd.booking_id=b.id 
                                       left join cab_type ct on b.cab_type=ct.type_id
                                       where driver_id!=0 and user_id='$customer_id' and (b.status ='CUSTOMER_NOT_RESPOND' OR b.status='CUSTOMER_ACCEPTED' OR b.status='CANCELLED' OR  b.status='FINISH' OR b.status='TRIP_ENDED' OR b.status='NOT_SHOW') 
                                       order by b.id desc limit $offset,$limit");
			// echo $this->db->last_query();
			return $query -> result();
		}

		function upcoming_ride($input_method)
		{
			$customer_id = $input_method['customer_id'];
			$pagenumber = @$input_method['page_number'];
			if ($pagenumber == "")
			{
				$pagenumber = 1;
			}

			$limit = 10;
			if ($pagenumber == 1)
			{
				$offset = 0;
			}
			else
			{
				$pagenumber = $pagenumber - 1;
				$offset = $pagenumber * 10 + 1;
			}
			$current_time = get_gmt_time();
			$current_time = date('Y-m-d');
			$query = $this -> db -> query("select cab_booking.*,ct.title  from cab_booking  left join cab_type ct on cab_booking.cab_type=ct.type_id where user_id='$customer_id' and (status='WAITING' OR status='CUSTOMER_ACCEPTED' OR status='DRIVER_ASSIGNED' OR status='CONFIRMED') and date(booking_time) >= '$current_time' order by cab_booking.booking_time asc limit $offset,$limit");
			// echo $this->db->last_query();
			return $query -> result();

		}

		function driver_track($bid)
		{
			$track = $this -> db -> query("SELECT * FROM `driver_status` where booking_id='$bid' and (status='FIVE_MIN_AWAY' OR 'I_AM_HERE') limit 1 ");

			if ($track -> result_id -> num_rows > 0)
			{
				return "Y";
			}
			else
			{
				return "N";
			}
		}

		function get_driver_detail($driver_id)
		{
			$this -> db -> select("*");
			$this -> db -> from("driver");
			$this -> db -> where("driver_id", $driver_id);
			$query = $this -> db -> get();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}

		}

		function current_offer($input_method)
		{
			//return true;
			$today = date("Y-m-d");
			$user_id = $input_method['user_id'];
			$this -> db -> select("*");
			$this -> db -> from("coupon");
			$this -> db -> where("isactive", "1");
			$this -> db -> where("isdelete", "0");
			$this -> db -> where("start_date<=", "$today");
			$this -> db -> where("expire_date>=", "$today");
			$this -> db -> where("coupon_type", 'ALL');
			$query = $this -> db -> get();

			$this -> db -> select("*");
			$this -> db -> from("coupon");
			$this -> db -> where("isactive", "1");
			$this -> db -> where("isdelete", "0");
			$this -> db -> where("start_date<=", "$today");
			$this -> db -> where("expire_date>=", "$today");
			$this -> db -> where("coupon_type", 'SPECIFIC');
			$this -> db -> join("user_coupon", "user_coupon.coupon_id=coupon.id and user_id=$user_id");
			$query2 = $this -> db -> get();
			if ($query2 -> num_rows() > 0 && $query -> num_rows() > 0)
			{
				$data = $query -> result_array();
				$data2 = $query2 -> result_array();
				return array_merge($data, $data2);
			}

			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			if ($query2 -> num_rows() > 0)
			{
				return $query2 -> result_array();
			}
			else
			{
				return false;

			}
		}

		function GetUsedCoupon($user_id)
		{
			$query = "SELECT group_concat(coupon) coupon  FROM `cab_booking` WHERE user_id=$user_id and coupon!=''";
			$res = $this -> db -> query($query);
			if ($res -> num_rows())
			{
				//update the coupon view in user_notification table
				$where = array(
					"user_id" => $user_id,
					"type" => "CAMPAIGN"
				);
				$this -> db -> delete("user_notification", $where);
				$data = $res -> row_array();
				return $data['coupon'];
			}
			else
			{
				return false;
			}
		}

		function update_profile($input_method)
		{
			//  echo "dv";exit;
			$name = rawurldecode($input_method['name']);
			$country_code = '+' . $input_method['country_code'];
			$phone_number = $input_method['phone_number'];
			$user_id = $input_method['user_id'];
			$this -> db -> where("id", $user_id);
			$update_string = array("name" => $name,
				//"phone" => $phone_number,
				//"country_code" => $country_code
			);
			$update = $this -> db -> update("user", $update_string);
			if ($update)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function change_password($input_method)
		{
			$old_password = $input_method['old_password'];
			$new_password = $input_method['new_password'];
			$user_id = $input_method['user_id'];
			$this -> db -> select("id,name");
			$this -> db -> from("user");
			$this -> db -> where("id", $user_id);
			$this -> db -> where('password', md5($old_password));
			$query = $this -> db -> get();
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$this -> db -> trans_begin();
				//update the password
				$data = $query -> result_array();
				$this -> db -> where("id", $user_id);
				$update_string = array("password" => md5($new_password));
				$update = $this -> db -> update("user", $update_string);
				
				if ($update)
				{

					$user_name = USER_JID_NAME . '-' . $input_method['user_id'];
					$param = array(
						"username" => $user_name,
						"password" => $new_password,
					);

					$openfir_result = $this -> openfire -> openfire_action("PUT", $param, "users/$user_name");
					$openfir_result= json_decode($openfir_result,true);
					if ($openfir_result['status'] == 1)
					{
						$this -> db -> trans_commit();
						$this -> db -> trans_complete();
						return array(
							"status" => 1,
							"user_detail" => $data[0],
							"message" => SUCCESS
						);
					}
					else
					{
						$this->db->rollback();
						return array(
							"status" => 0,
							"message" => SERVER_ERROR
						);
					}

				}
				else
				{
					$this->db->rollback();
					return array(
						"status" => 0,
						"message" => SERVER_ERROR
					);
				}

			}
			else
			{
				return array(
					"status" => 0,
					"message" => PASSWORD_NOT_MATCH
				);
			}
		}

		function get_customer_braintree_id($user_id)
		{
			$this -> db -> select("braintree_customer_id,braintree_token,name,email,country_code,phone");
			$this -> db -> from("user");
			$this -> db -> join("card_info", "card_info.customer_id=user.id");
			$this -> db -> where("user.id", $user_id);
			$query = $this -> db -> get();
			// echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$user_detail = $query -> result_array();
				$customer_info = array(
					"braintree_customer_id" => $user_detail[0]['braintree_customer_id'],
					"email" => $user_detail[0]['email'],
					"name" => $user_detail[0]['name'],
					"country_code" => $user_detail[0]['country_code'],
					"phone" => $user_detail[0]['phone'],
					"token" => $user_detail[0]['braintree_token']
				);
				return $customer_info;
			}
			else
			{
				return false;
			}

		}

		function update_cc_info($input_method)
		{
			$user_id = $input_method['user_id'];
			$cc_number = $input_method['credit_card_no'];
			$cc_number = credit_card_number($cc_number);
			$update_string = array(
				"credit_card_no" => $cc_number,
				"expire_date" => $input_method['expire_date'],
				"card_type" => $input_method['cardtype'],
				"braintree_customer_id" => $input_method['customer_id'],
				"braintree_token" => $input_method['token'],
			);
			$this -> db -> where("customer_id", $user_id);
			$update = $this -> db -> update("card_info", $update_string);
			if ($update)
			{
				return true;
			}
			else
			{
				return false;
			}

		}

		function get_coupon_detail($input_method)
		{
			$user_id = $input_method['user_id'];
			$today = date("Y-m-d");
			$coupon_code = $input_method['coupon_code'];

			$query = "select coupon from cab_booking where user_id=$user_id and coupon ='$coupon_code' ";
			$query = $this -> db -> query($query);
			if ($query -> num_rows())
			{
				return false;
			}
			else
			{
				$this -> db -> select("*");
				$this -> db -> from("coupon");
				$this -> db -> where("code", $coupon_code);
				$this -> db -> where("isactive", "1");
				$this -> db -> where("isdelete", "0");
				$this -> db -> where("start_date<=", $today);
				$this -> db -> where("expire_date>=", $today);
				$query = $this -> db -> get();
				if ($query -> num_rows())
				{
					$result = $query -> result_array();
					if ($result[0]['coupon_type'] == "ALL")
					{
						return $result = $query -> result_array();
					}
					else
					{
						//check in user_coupon
						$coupon_id = $result[0]['id'];
						$query_coupon = $this -> db -> get_where("user_coupon", array(
							"coupon_id" => $coupon_id,
							"user_id" => $user_id
						));
						if ($query_coupon -> num_rows())
						{
							return $result = $query -> result_array();
						}
						else
						{
							return false;
						}
					}
				}

				else
				{
					return false;
				}
			}

		}

		function coupon_exist($input_method)
		{
			$coupon_code = $input_method['coupon_code'];
			$user_id = $input_method['user_id'];
			$today = date("Y-m-d");
			$this -> db -> select("*");
			$this -> db -> from("coupon");
			$this -> db -> where("code", $coupon_code);
			$this -> db -> where("isactive", "1");
			$this -> db -> where("start_date<=", $today);
			$this -> db -> where("expire_date>=", $today);
			$query = $this -> db -> get();
			if ($query -> num_rows())
			{
				$result = $query -> result_array();
				if ($result[0]['coupon_type'] == "ALL")
				{
					return $result = $query -> result_array();
				}
				else
				{
					//check in user_coupon
					$coupon_id = $result[0]['id'];
					$query = $this -> db -> get_where("user_coupon", array(
						"coupon_id" => $coupon_id,
						"user_id" => $user_id
					));
					if ($query -> num_rows())
					{
						return $result = $query -> result_array();
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				return false;
			}

		}

		function get_otp($input_method)
		{
			$user_id = $input_method['user_id'];
			$this -> db -> select("varifycode,country_code,phone,is_varify");
			$this -> db -> from("user");
			$this -> db -> where("id", $user_id);
			$query = $this -> db -> get();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				$user_id = $input_method['user_id'];
				$this -> db -> select("varifycode,country_code,phone,is_varify");
				$this -> db -> from("user_temp");
				$this -> db -> where("id", $user_id);
				$query = $this -> db -> get();
				if ($query -> num_rows() > 0)
				{
					return $query -> result_array();
				}
				else
				{
					return false;
				}
			}

		}

		/*resne otp dave into table */
		function resend_otp($input_method)
		{
			$userid = $input_method['user_id'];
			$verifycode = $input_method['verifycode'];
			$update_code = $this -> db -> query("update user_temp set varifycode='$verifycode' where id='$userid'");
			//update if compnay user direct update in user table
			$update_code = $this -> db -> query("update user set varifycode='$verifycode' where id='$userid' and is_varify='0' and is_company='Y'");
			if ($update_code)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/*get notification */
		function get_notification($input_method)
		{
			$device = $input_method['device_type'];
			$limit = $input_method['limit'];
			$offset = $input_method['offset'];
			if ($input_method['user_id'])
			{
				$delete = $this -> db -> query("delete from user_notification where user_id=" . $input_method['user_id']);
			}
			$last_month = date('Y-m-d', strtotime("-30 days"));
			$get_data = $this -> db -> query("select * from notification where (device='$device' or device='ALL') and date(createdon) >='$last_month' order by createdon DESC limit $offset,$limit");
			//echo $this->db->last_query();
			$count = $get_data -> num_rows();
			if ($count)
			{
				return $get_data -> result_array();
			}
			else
			{
				return array();
			}
		}

		function CheckContactDetail($input_method)
		{
			$country_code = '+' . $input_method['country_code'];
			$phone = $input_method['phone_number'];
			$query = "select id,country_code,phone from user where country_code='$country_code' and phone='$phone'";
			$res = $this -> db -> query($query);
			if ($res -> num_rows() > 0)
			{
				return $res -> row_array();

			}
			else
			{
				return false;
			}
		}

		function mobile_verify($input_method)
		{
			$otp = $input_method['otp'];
			$user_id = $input_method['user_id'];
			$country_code = '+' . $input_method['country_code'];
			$phone_number = $input_method['phone_number'];
			//check this varify code is exist or not
			$query = $this -> db -> get_where("user", array(
				"id" => $user_id,
				"varifycode" => $otp
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				//varify this account
				$update_data = array(
					"is_varify" => 1,
					"country_code" => $country_code,
					"phone" => $phone_number
				);
				$this -> db -> where("id", $user_id);
				$query = $this -> db -> update("user", $update_data);

				if ($query)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}

		}

		function AddCompany($input_method)
		{
			$user_id = $input_method['user_id'];
			$comapny_name = $input_method['company_name'];
			$data = array(
				"user_id" => $user_id,
				"company_name" => $comapny_name,
				"is_approved" => "N",
				"created_on" => get_gmt_time()
			);
			//check if already unapprove company exist
			$check = $this -> db -> get_where("user_company", array(
				"user_id" => $user_id,
				"is_approved" => "N",
				"is_delete" => "0"
			));
			if ($check -> num_rows() > 0)
			{
				$this -> db -> where("user_id", $user_id);
				$this -> db -> where("is_approved", "N");
				$query = $this -> db -> update("user_company", $data);
				//echo $this->db->last_query();
				$usercompanyid = true;

			}
			else
			{
				$query = $this -> db -> insert("user_company", $data);
				$usercompanyid = $this -> db -> insert_id();
			}

			// echo $this->db->last_query();
			if ($usercompanyid)
			{
				return $usercompanyid;
			}
			else
			{
				return false;
			}

		}

		function SwitchAccount($input_method)
		{
			$user_id = $input_method['user_id'];
			$mode = $input_method['mode'];
			$this -> db -> where("id", $user_id);
			$update = $this -> db -> update("user", array("mode" => $mode));
			if ($update)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function DeleteComapny($input_method)
		{
			$user_id = $input_method['user_id'];
			$company_id = $input_method['company_id'];
			//update the user table
			$update_str = array(
				"company_id" => 0,
				"mode" => "NORMAL"
			);
			$this -> db -> where("id", $user_id);
			$update = $this -> db -> update("user", $update_str);
			if ($update)
			{
				//update the user_company tbale
				$update_data = array(
					"deleted_on" => get_gmt_time(),
					"is_delete" => 1,
					"is_approved" => "N"
				);
				$this -> db -> where("company_id", $company_id, "user_id", $user_id);
				$update = $this -> db -> update("user_company", $update_data);
				if ($update)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return FALSE;
			}

		}

		function CheckCCDetail($user_id)
		{
			$where = array(
				"isactive" => '1',
				"customer_id" => $user_id
			);
			$query = $this -> db -> get_where("card_info", $where);
			if ($query -> num_rows() > 0)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}

		function GetUserData($user_id)
		{
			$where = array("id" => $user_id);
			$query = $this -> db -> get_where("user", $where);
			if ($query -> num_rows() > 0)
			{
				return $query -> row_array();
			}
			else
			{
				return FALSE;
			}

		}

		function SaveCCDetail($data)
		{

			$gmdate = get_gmt_time();
			$bt_customer_id = $data['customer_id'];
			$bt_customer_token = $data['token'];
			$card_type = $data['cardtype'];
			$credit_card_no = $data['last4'];
			$credit_card_no = credit_card_number($credit_card_no);

			$cc_data = array(
				"credit_card_no" => $credit_card_no,
				"customer_id" => $data['user_id'],
				"braintree_customer_id" => $bt_customer_id,
				"braintree_token" => $bt_customer_token,
				"card_type" => $card_type,
				"expire_date" => $data['expire_date'],
				"createdon" => $gmdate
			);

			$query = $this -> db -> insert("card_info", $cc_data);
			//echo $this->db->last_query();
			return $this -> db -> insert_id();
		}

		function MakeOpenfireUser($data)
		{
			//print_r($data);
			$user_id = $data['user_id'];
			$user_name = USER_JID_NAME . '-' . $user_id;
			$password = $data['pass'];
			if (@$data['is_social'] == 'N')
			{
				$password = $data['pass'];
				$password = trim($password);
				$is_social = "N";
			}
			else
			{
				$password = "123456";
				$password = trim($password);
				$is_social = "Y";
			}
			$name = $data['name'];
			$email = $data['email'];
			$param = array(
				"username" => $user_name,
				"name" => "$name",
				"password" => "$password",
				"email" => "$email"
			);

			$openfir_result = $this -> openfire -> openfire_action("POST", $param, "users/");
			$openfire_response = json_decode($openfir_result, true);
			//print_R($openfire_response);
			if ($openfire_response['status'] == 0)
			{
				return false;

			}
			else
			{
				return true;
			}
		}

		function SaveFavLocation($input_method)
		{
			extract($input_method);
			$insert_arr = array(
				"user_id" => $user_id,
				"latitude" => $latitude,
				"longitude" => $longitude,
				"address" => $address,
				"placename" => ($placename) ? $placename : "",
				"custom_name" => $custom_name,
				"location_type" => ($location_type) ? $location_type : "sublocality"
			);
			$this -> db -> insert("favourite_location", $insert_arr);
			$location_id = $this -> db -> insert_id();
			if ($location_id > 0)
			{
				return $location_id;
			}
			else
			{
				return false;
			}
		}

		function GetFavLocation_old($user_id)
		{
			$this -> db -> select("latitude,longitude,address,placename,custom_name");
			$query = $this -> db -> get_where("favourite_location", array("user_id" => $user_id));
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return FALSE;
			}
		}

		function GetCustomFavLocation($user_id)
		{
			$query = $this -> db -> get_where("favourite_location", array("user_id" => $user_id));
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();

			}
			else
			{
				return FALSE;
			}

		}

		function GetFavLocation($user_id)
		{
			$query = "SELECT pickup_location,destination_location,pickup_placename,destination_placename,pickup_latitude,pickup_longitude,destination_latitude,destination_longitude FROM `cab_booking` where user_id=$user_id group by pickup_location,destination_location order by id desc limit 10";
			$query = $this -> db -> query($query);
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return FALSE;
			}
		}

		function DeleteFavLocation($input_method)
		{
			$user_id = $input_method['user_id'];
			$location_id = $input_method['location_id'];

			$this -> db -> where(array(
				"user_id" => $user_id,
				"id" => $location_id
			));

			$this -> db -> delete("favourite_location");
		}

		function RecieptSetting($input_method)
		{
			extract($input_method);
			$this -> db -> where("id", $user_id);
			$query = $this -> db -> update("user", array("reciept_enable" => $reciept_enable));
			if ($query)
			{
				return true;
			}
			else
			{
				return false;
			}

		}

	}
?>
