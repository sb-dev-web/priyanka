<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class AutoAcceptBooking extends CI_Controller
	{

		function __construct()
		{
			
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url',
				'constants_helper',
				'function_helper'
			));
			$this -> load -> model('Booking_model');
			$this -> load -> library(array(
				'session',
				'email',
				'pagination',
				'paginationlib',
				'Twilio',
				'curl'
			));
			$this -> load -> library('xmpp', false);
			$user = $this -> session -> userdata('logged_in');
			if (!$user)
			{
				redirect('Welcome');
			}

		}

		public function index()
		{
			

			$AutoAcceptBooking = $this -> Booking_model -> AutoAcceptBooking();

			$data['bot_booking'] = $AutoAcceptBooking;
			$driver = $this -> Booking_model -> fetch_driver();
			
			$data['driver'] = $driver;
			$data['active_page'] = 'auto_accept_booking';
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('bot_booking');
			$this -> load -> view('site-nav/footer');
		}

		function GetPreBooking()
		{
			echo json_encode($this -> Booking_model -> GetPreBooking());
		}

		function advance_ajaxPaginationData()
		{
			$this -> load -> library('Ajax_pagination');
			$postdata = $this -> input -> post('data');
			$post = json_decode($postdata, true);
			$page = $post['page'];
			if (!$page)
			{
				$offset = 0;
			}
			else
			{
				$offset = $page;
			}

			$pagingconfig = $this -> paginationlib -> initPagination("/Booking/advance_ajaxPaginationData/", $this -> Booking_model -> count_advance_booking(), 10, $post['page']);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			//get the posts data
			$data['advance_booking'] = $this -> Booking_model -> fetch_advance_booking($pagingconfig['per_page'], $page);
			// $driver=$this->Booking_model->fetch_driver();
			//$data['driver']=$driver;
			//load the view
			$this -> load -> view('advance_booking_ajax', $data, false);
		}

		public function New_Pre_Booking()
		{
			$this -> load -> library('Ajax_pagination');
			// $this->Booking_model->count_newadvance_booking();
			$pagingconfig = $this -> paginationlib -> initPagination("/Booking/newadvance_ajaxPaginationData/", $this -> Booking_model -> count_advance_booking(), 10, 1, array());
			// print_r($pagingconfig);
			//exit;

			$this -> ajax_pagination -> initialize($pagingconfig);
			//$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['link'] = $this -> pagination -> create_links();
			$data['link'] = $this -> ajax_pagination -> create_links();
			//	$advance_booking=$this->Booking_model->fetch_newadvance_booking($pagingconfig['per_page'],
			// 0);

			// $data['advance_booking'] = $advance_booking;
			$driver = $this -> Booking_model -> fetch_driver();
			//$data['advance_booking']=$advance_booking;
			$data['driver'] = $driver;
			$data['active_page'] = 'booking';
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('newpre_booking');
			$this -> load -> view('site-nav/footer');
		}

		function GetNewBooking()
		{
			echo json_encode($this -> Booking_model -> GetPreNewBooking());
		}

		function newadvance_ajaxPaginationData()
		{
			$this -> load -> library('Ajax_pagination');
			$postdata = $this -> input -> post('data');
			$post = json_decode($postdata, true);
			$page = $post['page'];
			if (!$page)
			{
				$offset = 0;
			}
			else
			{
				$offset = $page;
			}

			$pagingconfig = $this -> paginationlib -> initPagination("/Booking/advance_ajaxPaginationData/", $this -> Booking_model -> count_advance_booking(), 10, $post['page']);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			//get the posts data
			$data['advance_booking'] = $this -> Booking_model -> fetch_advance_booking($pagingconfig['per_page'], $page);
			// $driver=$this->Booking_model->fetch_driver();
			//$data['driver']=$driver;
			//load the view
			$this -> load -> view('advance_booking_ajax', $data, false);
		}

		function booking_details()
		{
			$this -> load -> library('Ajax_pagination');
			$this -> Booking_model -> count_get_all_booking_details();
			$pagingconfig = $this -> paginationlib -> initPagination("/Booking/booking_details_ajaxPaginationData/", $this -> Booking_model -> count_get_all_booking_details(), 10, 1, array());

			$this -> ajax_pagination -> initialize($pagingconfig);
			//$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			//$data['link'] = $this->pagination->create_links();
			$data['link'] = $this -> ajax_pagination -> create_links();
			$advance_booking = $this -> Booking_model -> get_all_booking_details($pagingconfig['per_page'], 0);

			$data['advance_booking'] = $advance_booking;
			$driver = $this -> Booking_model -> fetch_driver();
			$data['advance_booking'] = $advance_booking;
			$data['driver'] = $driver;
			$data['active_page'] = 'booking_all';
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('booking_details');
			$this -> load -> view('site-nav/footer');
		}

		function booking_details_ajaxPaginationData()
		{
			$this -> load -> library('Ajax_pagination');
			$postdata = $this -> input -> post('data');
			$post = json_decode($postdata, true);
			$page = $post['page'];
			if (!$page)
			{
				$offset = 0;
			}
			else
			{
				$offset = $page;
			}

			$pagingconfig = $this -> paginationlib -> initPagination("/Booking/booking_details_ajaxPaginationData/", $this -> Booking_model -> count_get_all_booking_details(), 10, $post['page']);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			//get the posts data
			$data['advance_booking'] = $this -> Booking_model -> get_all_booking_details($pagingconfig['per_page'], $page);
			// $driver=$this->Booking_model->fetch_driver();
			//$data['driver']=$driver;
			//load the view
			$this -> load -> view('booking_details_ajax', $data, false);
		}

		public function perPage()
		{
			$this -> load -> library('Ajax_pagination');
			return 2;
		}

		/* advance booking assgin */
		function advance_booking()
		{
			$bookingid = @$_POST['booking_id'];
			$driverid = @$_POST['driverid'];

			/* if driver already assgin than send sms to previous driver */
			$previous_driver = $this -> Booking_model -> get_previous_driver($bookingid);
			if ($previous_driver)
			{
				$pre_driver_contact = $previous_driver[0]['contact'];
				$bookin_no = $previous_driver[0]['booking_number'];
				$message = NEW_DRIVER_ASSGIN . ' ' . $bookin_no;
				send_sms($pre_driver_contact, $message);
			}

			$update_advance = $this -> Booking_model -> update_booking($bookingid, $driverid);
			if ($update_advance)
			{

				$fetch_driver_no = $this -> Booking_model -> get_driver_number($driverid);

				$driver_contact = $fetch_driver_no[0]['contact'];

				$message = ADVACNE_BOOKING;
				send_sms($driver_contact, $message);
				//send a email to web customer
				$customer = $this -> Booking_model -> GetBookingDetail($bookingid);
				if ($customer)
				{
					//$this->NotifyCustomer($customer);
				}

				echo 1;
			}
			else
			{
				echo 0;
			}
		}

		/* function for completed_booking */

		function completed_booking()
		{

			$this -> load -> library('Ajax_pagination');
			$this -> Booking_model -> fetch_Completed_booking_count();
			$pagingconfig = $this -> paginationlib -> initPagination("/Booking/complete_ajaxPaginationData/", $this -> Booking_model -> fetch_Completed_booking_count(), 10, 1, array());

			$this -> ajax_pagination -> initialize($pagingconfig);
			//$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			//$data['link'] = $this->pagination->create_links();
			$data['link'] = $this -> ajax_pagination -> create_links();
			$completed_booking = $this -> Booking_model -> fetch_Completed_booking($pagingconfig['per_page'], 0);

			$data['completed_booking'] = $completed_booking;
			$data['active_page'] = 'completed_booking';
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('completed_booking');
			$this -> load -> view('site-nav/footer');

		}

		function unpaid_booking()
		{
			$completed_booking = $this -> Booking_model -> fetch_unpaid_booking();
			$data['unpaid_booking'] = $completed_booking;
			$data['active_page'] = 'completed_booking';
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('unpaid_booking');
			$this -> load -> view('site-nav/footer');

		}

		function complete_ajaxPaginationData()
		{
			$this -> load -> library('Ajax_pagination');
			$postdata = $this -> input -> post('data');
			$post = json_decode($postdata, true);

			$page = $post['page'];
			if (!$page)
			{
				$offset = 0;
			}
			else
			{
				$offset = $page;
			}

			$pagingconfig = $this -> paginationlib -> initPagination("/Booking/complete_ajaxPaginationData/", $this -> Booking_model -> fetch_Completed_booking_count(), 10, $post['page']);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			$data['completed_booking'] = $this -> Booking_model -> fetch_Completed_booking($pagingconfig['per_page'], $page);
			//print_r($data);
			$this -> load -> view('completed_booking_ajax', $data, false);
		}

		/* change payment status */
		function change_payment()
		{
			$id = $_POST['id'];
			$status = $_POST['status'];
			$payment_status = $this -> Booking_model -> change_payment_status($id, $status);
			echo $payment_status;

		}

		/* function for fetch current booking */

		function current_booking()
		{

			$this -> load -> library('Ajax_pagination');
			$this -> Booking_model -> fetch_current_booking_count();
			$pagingconfig = $this -> paginationlib -> initPagination("/Booking/current_ajaxPaginationData/", $this -> Booking_model -> fetch_current_booking_count(), 10, 1, array());
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			$current_booking = $this -> Booking_model -> fetch_current_booking($pagingconfig['per_page'], 0);
			$data['current_booking'] = $current_booking;
			$data['active_page'] = 'current_booking';
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('current_booking');
			$this -> load -> view('site-nav/footer');
		}

		function current_ajaxPaginationData()
		{
			$this -> load -> library('Ajax_pagination');
			$postdata = $this -> input -> post('data');
			$post = json_decode($postdata, true);
			$page = $post['page'];
			if (!$page)
			{
				$offset = 0;
			}
			else
			{
				$offset = $page;
			}

			$pagingconfig = $this -> paginationlib -> initPagination("/Booking/current_ajaxPaginationData/", $this -> Booking_model -> fetch_Completed_booking_count(), 10, $post['page']);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			$data['current_booking'] = $this -> Booking_model -> fetch_current_booking($pagingconfig['per_page'], $page);
			$this -> load -> view('current_booking_ajax', $data, false);
		}

		/* chnage current booking status */
		function change_trip_end()
		{
			$booking_id = $_POST['bookingid'];
			$driver_id = $_POST['driver_id'];
			$km_driven = $_POST['km_driven'];
			$journey_time = $_POST['journeytime'] * 60;

			$isadmin = 'Y';
			$params = array(
				'booking_id' => $booking_id,
				'driver_id' => $driver_id,
				'km_driven' => $km_driven,
				'journey_time' => $journey_time
			);
			$url = "https://privatedriverapp.com/app/api/Booking/trip_end";
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_POSTFIELDS => $params,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_VERBOSE => true
			));
			$result = curl_exec($ch);
			//curl_close($ch);
			//print_r($result);
			if ($result)
			{
				//echo 'trip end';
				$get_result = json_decode($result);
				//print_r($get_result);
				$status = $get_result -> status;
				if ($status == '1')
				{
					//echo 'hello';

					$pluginurl = TRIP_END_WEBSERVICE;
					$ch = curl_init();
					curl_setopt_array($ch, array(
						CURLOPT_URL => $pluginurl,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => $result,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_SSL_VERIFYPEER => false,
						CURLOPT_VERBOSE => true,
						CURLOPT_HTTPHEADER => array(
							'Authorization:T36HCEyK',
							'Content-Type: application/json',
							'Content-Length: ' . strlen($result)
						)
					));
					$plugin_result = curl_exec($ch);
					//echo $pluginurl;
					//echo $plugin_result;
					if ($plugin_result)
					{
						echo 1;
					}
					if (!curl_errno($ch))
					{
						echo 1;
						$info = curl_getinfo($ch);
						// print_r($info);
						//echo 'Took ' . $info['total_time'] . ' seconds to send a request to ' .
						// $info['url'];
					}
					curl_close($ch);
				}
				//curl_close($ch);
			}
			else
			{
				echo 1;
			}
		}

		function get_advance_booking()
		{
			$booking_no = $_POST['booking_no'];
			$type = $_POST['type'];
			$data['get_data'] = $this -> Booking_model -> get_pre_booking($booking_no, $type);

			$this -> load -> view('advance_pop_up', $data);
		}

		/* fetch complete booking */
		function get_completed_booking()
		{
			$booking_no = $_POST['booking_no'];
			$data['get_data'] = $this -> Booking_model -> get_complete_booking($booking_no);

			$this -> load -> view('completed_pop_up', $data);
		}

		function get_all_booking()
		{
			$booking_no = $_POST['booking_no'];

			$data['get_data'] = $this -> Booking_model -> get_all_booking($booking_no);
			$data['drivers'] = $this -> Booking_model -> GetDriverDetail($booking_no);

			$this -> load -> view('book_details_pop_up', $data);
		}

		function ConfirmPreBooking()
		{
			$data = $this -> Booking_model -> UpdateStatus($_POST);
			//print_r($data);
			$phone = $data['phone'];
			$time = date('d.m.Y h.i', strtotime($data['booking_time']));
			$address = $data['pickup_location'];
			$email = $data['email'];
			//$phone="+917737906424";

			if ($_POST['status'] == "REJECTED")
			{
				$message = REJECTSMS;
				send_sms($phone, $message);
				//PreBookingActionMail($email,$message,$_POST['status']);
				$customer = $this -> Booking_model -> GetBookingDetail($_POST['id']);
				if ($customer)
				{
					$this -> NotifyCustomer($customer, 'Reject');
				}

			}
			else
			{
				$message = ACCEPTSMS;
				$search = array(
					"<TIME>",
					"<ADDRESS>"
				);
				$replace = array(
					"$time",
					"$address"
				);
				$message = str_replace($search, $replace, $message);

				send_sms($phone, $message);
				//PreBookingActionMail($email,$message,$_POST['status']);
				$customer = $this -> Booking_model -> GetBookingDetail($_POST['id']);
				if ($customer)
				{
					$this -> NotifyCustomer($customer, 'Accept');
				}

			}

		}

		function NotifyCustomer($customer, $type)
		{
			extract($customer);
			$booking_time = $customer['booking_time'];
			$booking_time = date('d-M-Y', strtotime($booking_time));
			if ($type == "Accept")
			{
				$body = "Thank you for your order. We hereby confirm pick up at  $booking_time  . Please contact us on email: booking@privatedriver.no or call us: +47 40 69 79 99 for any questions or changes in Your order. Thank you for choosing Private Driver.";
			}
			else
			{
				$body = "We have recieved your booking and we are sorry to we have no availaibility at this time. Please contact us at email: booking@privatedriver.no or call us: +47 40 69 79 99 and we will try to find a solution. SIncerly Yours Private Driver A/S";

			}		$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';
			$config['charset'] = 'utf-8';
			$this -> email -> initialize($config);
			$this -> email -> from(ADMINMAIL_FROM, 'Private Driver');
			$this -> email -> to($email);
			//$ci -> email -> cc("priyanka@foxinfosoft.com");
			$this -> email -> subject(' Your Private Driver booking');
			$this -> email -> message($body);

			//$counter ++;
			$this -> email -> send();
		}

	}
