<?php

	defined('BASEPATH') or exit('No direct script access allowed');

	class Usercondition extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url'
			));
			$this -> load -> library(array(
				'session',
				'email'
			));

		}
         
		 public function en()
		{
			
			
			$this -> load -> view('tcen');
		
		}
		public function no()
		{
			$lang= $this->uri->segment(3) ;
			//echo $lang;
			
			$this -> load -> view('t&c');
		//	$this -> load -> view('site-nav/footer');
		}

	}
