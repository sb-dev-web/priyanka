<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Airport Price</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Airport Price</a></li>
      <li class="active">Airport Price</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="send_notify" action="" method="post" autocomplete="off">
             <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Select Airport</label>
                   <select class="form-control validate[required]" name="airport" id="airport">
                      <option value="">Select Type</option>
                      
                       <?php foreach($airport as $airport_name){?>
                       	<option value="<?php echo $airport_name['id'] ?>"><?php echo $airport_name['name'] ?></option>
                       	
                       <?php }?>        
                      
                    </select>
                    </div>  
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Airport Address</label>
                     <input type="text" class="form-control validate[required]" id="address"  name="address" placeholder="Address" value="" >
                    </div>  
                </div>
              </div>
                <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Zip Code</label>
                     <input type="text" class="form-control validate[required]" id="zipcode"  name="zipcode" placeholder="Email" value="" >
                    </div>  
                </div>
              </div>

              
               
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Message</label>
                    <textarea id="message" name="message" class="form-control validate[required]" rows="3" placeholder="Enter Your Message"></textarea>
                  </div>  
                </div>
              </div>
              
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>
    
<script type="text/javascript">
//var $ = jQuery.noConflict();
$(document).ready(function(){

    function submit_form(formData,status)
    {
        if(status==true)
        {
            var querystr = $("#send_notify").serialize();
            
            
            
            $.ajax({
                url		:	"<?php echo site_url('notification/push_notify'); ?>",
                type	:	"POST",
                data	:	querystr,
                  
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                  if(data == 1)
                  {
                    $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl("Notification Send successfully", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                   $('#send_notify')[0].reset();
                  }
                  else
                  {
                    $("#loader").hide();
                    //window.location='<?php echo site_url('login/dashboard'); ?>';
                    //alert('failed to save');
                  }
                 }
            });
            return false;
        }
        
    }
            
      
    
  $("#send_notify").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});

$("#airport").change(function(){
	alert(this.value);
	
	var airport_id=this.value;
	            $.ajax({
                url		:	"<?php echo site_url('Airportprice/getAirportDetail'); ?>",
                type	:	"POST",
                data	:	{"id":airport_id},
                beforeSend  :   function(){
                 $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                	alert(data);
                	
                  var obj = jQuery.parseJSON(data);
                  var address=obj.address;
                  var zip =obj.zip_code;
                  $("#address").val(address);
                  $("zipcode").val(zip_code);
                            	
                }})

	
})
</script>                 