<?php
error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Activity extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url','download','constants_helper'));
        $this->load->model('Activity_model');
        $this->load->model('Driver_model');
     
        //->load->library(array('session', 'pagination'));
        $this->load->library(array(
            'session',
            'email',
            'paginationlib',
            'Pagination'));
        $user = $this->session->userdata('logged_in');
        if (!$user)
        {
            redirect('Welcome');
        }

    }


    /* driver booking according to date */

    public function booking_report()
    {
       
      
       
       
        if ((isset($_POST['date'])))
        {

            $date = $_POST['date'];
            $dates = explode('to', $date);
            $start_date = $dates[0];
            $end_date = $dates[1];
            $this->Get_drivers_report($start_date,$end_date);
        }
        else
        {
            $data['driver_report']=array();
            $data['active_page'] = 'driver_activity';
            $this->load->view('site-nav/header', $data);
            $this->load->view('driver_report', $data);
            $this->load->view('site-nav/footer');
        } 
         

    }

    /* get driver report according booking date */
   function Get_drivers_report($starton,$endon)
	{
		//$shift_id = 241;
	   $this->load->library('Ajax_pagination');
		//get all active driver
		$driver_data = $this -> Activity_model -> active_driver();
       // print_r($driver_data);
		foreach ($driver_data as $driver)
		{
			//if($driver['driver_id']==77)
			 $workdetail = $this -> Activity_model -> working_time_mon($starton, $endon, $driver['driver_id']);
		   //echo "<pre>";
			//print_r($workdetail);
			if ($workdetail)
			{
				$last_driver_id = "";

				{
					

					$driver_id = $workdetail[0]['driver_id'];
			      // $break_hr = $this -> get_break_detail_mon($starton, $endon, $driver_id);
			      $working_hr = array_sum(array_column($workdetail, 'sectime'));
					$total_working_hr = $working_hr ;
					$time_str = $this -> format_time($total_working_hr);
					$other_detail = $this -> Activity_model -> get_other_detail($starton, $endon, $driver_id);
					//print_r($other_detail);
					$other_detail[0]['working_hr'] = $time_str;
					$driver_working_detail[]=$other_detail[0];
					
				}

			}
		}
$report_date=array(
			"s_date"=>$starton,
			"e_date"=>$endon
		);
		//print_r($report_date);
             $data['report_date']=$report_date;
            $data['driver_report']=$driver_working_detail;
           // echo '<pre>';
           
            $this->load->view('total_driver_report', $data);
           

		}

	function get_break_detail_mon($start_date, $end_date, $driver_id)
	{
		   $this->load->library('Ajax_pagination');
		$startdate = date('Y-m-d', strtotime($start_date));
		$enddate = date('Y-m-d', strtotime($end_date));
		$shift_break_detail = $this -> Activity_model -> shift_break_detail_mon($startdate, $enddate, $driver_id);
		if (!$shift_break_detail)
		{
			return 0;
		}

		$break_hr = 0;
		for ($j = 0; $j < count($shift_break_detail); $j++)
		{

			//print_r($shift_break_detail[$i]);
			$break_type = $shift_break_detail[$j]['type'];
			if ($j == 0 && $break_type != "STARTED")
			{
				$previous_day = strtotime("-1 day", strtotime($date));
				$break_start = date('Y-m-d', $previous_day);
				if ($break_type == "ENDED")
				{
					$break_end = $shift_break_detail[$j]['break_time'];
				}

				$break_hr += $this -> time_difference($break_end, $break_start, 1);
			}
			elseif ($j == count($shift_break_detail) && $break_type == 'STARTED')
			{
				$next_day = strtotime("-1 day", strtotime($date));
				$break_end = date('Y-m-d', $next_day);
				$break_start = $shift_break_detail['break_time'];

				$break_hr += $this -> time_difference($break_end, $break_start, 1);

			}
			else
			{
				if ($break_type == "STARTED")
					$break_start = $shift_break_detail[$j]['break_time'];
				$j = $j + 1;
				$break_type = $shift_break_detail[$j]['type'];
				if ($break_type == 'ENDED')
				{
					$break_end = $shift_break_detail[$j]['break_time'];
				}

				$break_hr += $this -> time_difference($break_end, $break_start, 1);
			}
		}
		return $break_hr;

	}    
    
    
    
    
    
    /* booking result according driver */
    public function booking_accordingdriver()
    {
		
        $did = $_POST['did'];
        $config['base_url'] = site_url('Activity/driver_activity');
        $config['total_rows'] = $this->Activity_model->count_driver($did);
        $config['per_page'] = "25";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //call the model function to get the department data
        $data['deptlist'] = $this->Activity_model->driver_booking($config["per_page"], $data['page'],
            $did);
        //print_r($data);

        $data['pagination'] = $this->pagination->create_links();
        //echo $data['pagination'];

        //load the department_view
        $this->load->view('driver_booking', $data);


    }

    /* fetch daily basisi total count report accept,decline */

    function booking_activity()
    {
        $booking_date = '0';
        $config['base_url'] = site_url('Activity/booking_activity');
        $config['total_rows'] = $this->Activity_model->count_drivers($booking_date);
        $config['per_page'] = "25";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //call the model function to get the department data
        $data['booklist'] = $this->Activity_model->Daily_report($config["per_page"], $data['page'],
            $booking_date);
        //print_r($data);

        $data['pagination'] = $this->pagination->create_links();

        //load the department_view
        $this->load->view('site-nav/header', $data);
        $this->load->view('daily_activity');
        $this->load->view('site-nav/footer');

    }


    /* functin for drive daily report accoording date */
    function driver_booking_date()
    {

        $booking_date = $_POST['booking_date'];
        $config['base_url'] = site_url('Activity/daily_activity');

        $config['total_rows'] = $this->Activity_model->count_drivers($booking_date);

        $config['per_page'] = "25";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);


        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //call the model function to get the department data
        $data['booklist'] = $this->Activity_model->Daily_report($config["per_page"], $data['page'],
            $booking_date);
        //print_r($data);

        $data['pagination'] = $this->pagination->create_links();
        //echo $data['pagination'];

        //load the department_view
        $this->load->view('driver_booking_date', $data);

    }

    /* checking log */

    public function log()
    {
        //pagination settings
        $date = "";
        $api_name = "";
        $config['base_url'] = site_url('Activity/log');
        $config['total_rows'] = $this->Activity_model->count_all_log($date, $api_name);
        $config['per_page'] = 500;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //call the model function to get the department data
        $data['loglist'] = $this->Activity_model->log($config["per_page"], $data['page'],
            $date, $api_name);
        //print_r($data);

        $data['pagination'] = $this->pagination->create_links();

        //load the department_view
        $this->load->view('site-nav/header', $data);
        $this->load->view('log_activity');
        $this->load->view('site-nav/footer');
        // $this->load->view('',$data);
    }


    /* fetch log according filter */
    /* functin for drive daily report accoording date */
    function log_activity()
    {

        $booking_date = $_POST['date'];
        $api_name = $_POST['api_name'];
        //echo $api_name;
        $config['base_url'] = site_url('Activity/log_activity');

        $config['total_rows'] = $this->Activity_model->count_all_log($booking_date, $api_name);

        $config['per_page'] = "25";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);


        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //call the model function to get the department data
        $data['loglist'] = $this->Activity_model->log($config["per_page"], $data['page'],
            $booking_date, $api_name);
        //print_r($data);

        $data['pagination'] = $this->pagination->create_links();
        //echo $data['pagination'];

        //load the department_view
        $this->load->view('daliy_log_activity', $data);

    }
    /* fetching data according driver id and type of status*/
    function booking_data($did, $status,$date)
    {
		   //echo $date;
		   $date_arr= explode('to', $date);
		  // print_r($date_arr);
		   $date=trim($date_arr[0]).'to'.trim($date_arr[1]);
         $this->load->library('Ajax_pagination');
        $extra_param = array("driver_id" => $did, "status" => $status, "date"=>trim($date),"start_date"=>trim($date_arr[0]),"end_date"=>trim($date_arr[1]));
        $count = $this->Activity_model->count_booking_report($did, $status);
        $pagingconfig = $this->paginationlib->initPagination("/Activity/booking_data_ajax",
            $count, 10, 3, $extra_param);
        // $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $this->ajax_pagination->initialize($pagingconfig);
        //$extra_param=json(array("driver_id"=>$did,"status"=>$status));
        $data['link'] = $this->ajax_pagination->create_links();
        $limit = 10;
        $offset = 0;
        $data['total_report'] = $this->Activity_model->driver_booking_report($did, $status,
            $offset, $limit,$date);
        // print_R($data);
		$data['time']=$date;
        $this->load->view('total_booking_report', $data);
    }
    function booking_data_ajax()
    {
		   $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
	
        $post = json_decode($postdata, true);
		//print_r($post);

        $did = $post['driver_id'];
        $status = $post['status'];
		//echo "daddd".$post['date'];
       $count = $this->Activity_model->count_booking_report($did, $status);
        $extra_param = array("driver_id" => $post['driver_id'], "status" => $post['status']);
        $pagingconfig = $this->paginationlib->initPagination("/Activity/booking_data_ajax",
            $count, 10, $post['page'], $extra_param);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $limit = $pagingconfig['per_page'];
        if (!$post['page'])
        {
            $offset = 0;
        }
        else
        {
            $offset = ($post['page'] - 1) * $limit;
            $offset = $post['page'];
        }
        $data['total_report'] = $this->Activity_model->driver_booking_report($did, $status,
            $offset, $limit,$post['date']);
        // print_r($data);

        //load the view
        $this->load->view('total_booking_report', $data, false);
    }

   

    /*shift_data */
    function shift_report()
    {
        if ((isset($_POST['date'])) && ($_POST['driverid']))
        {
            $date = $_POST['date'];
            $dates = explode('to', $date);
            $start_date = $dates[0];
            $end_date = $dates[1];
            $driver_id = $_POST['driverid'];

            $this->shift_data($start_date, $end_date, $driver_id);
        }
        else
        {
            $data['fetch_driver'] = $this->Driver_model->get_driver_name();
            $this->load->view('site-nav/header');
            $this->load->view('shift_report', $data);
            $this->load->view('site-nav/footer');
        }
    }

    /* this is for check shift history */
    function shift_data($start_date, $end_date, $driver_id)
    {
   $this->load->library('Ajax_pagination');

        $extra_param = array(
            "start_date" => trim($start_date),
            "end_date" => trim($end_date),
            "driver_id" => $driver_id);


        /*$count = $this->Activity_model->count_shift_report($start_date, $end_date, $driver_id);
        $pagingconfig = $this->paginationlib->initPagination("Activity/shift_data_ajax",
        $count, 10, 1, $extra_param);
        // $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $this->ajax_pagination->initialize($pagingconfig);
        //$extra_param=json(array("driver_id"=>$did,"status"=>$status));

        $data['link'] = $this->ajax_pagination->create_links();
        */
        $limit = 10;
        $offset = 0;
        $shift_detail = $this->Activity_model->driver_shift_report($start_date, $end_date,
            $driver_id);
        if ($shift_detail)
        {
            foreach ($shift_detail as $shift)
            {
                $break_detail = $this->Activity_model->shift_break_detail($shift['s_id']);

                if ($break_detail)
                {
                    foreach ($break_detail as $break)
                    {
                        $break_data[] = array(
                            "break_time" => $break['break_time'],
                            "type" => $break['type'],
                            "shift_id" => $break['shift_id']);
                    }

                }
                else
                {
                    $break_data = array();
                }
                $shift_data[] = array(
                    "shift_start" => $shift['createdon'],
                    "shift_ended" => $shift['endon'],
                    "comment" => $shift['comment'],
                    "cab_title" => $shift['title'],
                    // "cab_type"=>$shift['type'],
                    "cab_number" => $shift['cab_plate_no'],
                    "car_model" => $shift['cab_model'],
                    "break_data" => $break_data);

            }
        }
        $data['report_data'] = $shift_data;
        //echo "<pre>";
        // print_R($data);
        $this->load->view('total_shift_report', $data);

    }


    function shift_data_ajax()
    {
		   $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');

        $post = json_decode($postdata, true);

        $driver_id = $post['driver_id'];

        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $count = $this->Activity_model->count_shift_report($start_date, $end_date, $driver_id);
        $extra_param = array(
            "start_date" => "2015-07-01",
            "end_date" => "2015-09-11",
            "driver_id" => $driver_id);
        $pagingconfig = $this->paginationlib->initPagination("/Activity/shift_data_ajax",
            $count, 10, 3, $extra_param);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $limit = $pagingconfig['per_page'];
        if (!$post['page'])
        {
            $offset = 0;
        }
        else
        {
            $offset = ($post['page'] - 1) * $limit;
            $offset = $post['page'];
        }
        $data['total_report'] = $this->Activity_model->driver_shift_report($start_date,
            $end_date, $driver_id, $offset, $limit);


        //load the view
        $this->load->view('total_shift_report', $data, false);
    }

    /* for break report */
    function break_report()
    {
        if ((isset($_POST['date'])) && ($_POST['driverid']))
        {
            $date = $_POST['date'];
            $dates = explode('to', $date);
            $start_date = $dates[0];
            $end_date = $dates[1];
            $driver_id = $_POST['driverid'];

            $this->break_data($start_date, $end_date, $driver_id);
        }
        else
        {
            $data['fetch_driver'] = $this->Driver_model->get_driver_name();
            $data['active_page'] = 'break_report';
            $this->load->view('site-nav/header', $data);
            $this->load->view('break_report', $data);
            $this->load->view('site-nav/footer');
        }
    }

    /* this is for check shift history */
    function break_data($start_date, $end_date, $driver_id)
    {
		   $this->load->library('Ajax_pagination');
        $s_date = trim($start_date);
        $e_data = trim($end_date);

        $extra_param = array(
            "start_date" => $s_date,
            "end_date" => $e_data,
            "driver_id" => $driver_id);

        $count = $this->Activity_model->count_break_report($start_date, $end_date, $driver_id);
        $pagingconfig = $this->paginationlib->initPagination("/Activity/break_data_ajax",
            $count, 10, 1, $extra_param);
        // $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $this->ajax_pagination->initialize($pagingconfig);
        //$extra_param=json(array("driver_id"=>$did,"status"=>$status));

        $data['link'] = $this->ajax_pagination->create_links();

        $limit = 10;
        $offset = 0;
		$break_total=$this->Activity_model->total_break($start_date,$end_date,$driver_id);
		$data['break_sum']=$this->count_break($break_total);
		//print_r($data['break_sum']);
        $data['break_report'] = $this->Activity_model->driver_break_report($start_date,
            $end_date, $driver_id, $offset, $limit);
        $data['time']=$extra_param;
        $this->load->view('total_break_report', $data);

    }


    function break_data_ajax()
    {
		   $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');

        $post = json_decode($postdata, true);
	
        $driver_id = $post['driver_id'];

        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $count = $this->Activity_model->count_break_report($start_date, $end_date, $driver_id);
        $extra_param = array(
            "start_date" => $start_date,
            "end_date" => $end_date,
            "driver_id" => $driver_id);
        $pagingconfig = $this->paginationlib->initPagination("Activity/break_data_ajax",
            $count, 10, $post['page'], $extra_param);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $limit = $pagingconfig['per_page'];
        if (!$post['page'])
        {
            $offset = 0;
        }
        else
        {
            $offset = ($post['page'] - 1) * $limit;
            $offset = $post['page'];
        }
        $data['break_report'] = $this->Activity_model->driver_break_report($start_date,
            $end_date, $driver_id, $offset, $limit);


        //load the view
        $this->load->view('total_break_report', $data, false);
    }

    /**************************************************
    *shift functoin here start **********************
    ******************************************/
    function Shift_image()
    {
        if ((isset($_POST['date'])) && ($_POST['cab_id']))
        {
            $date = $_POST['date'];
            $dates = explode('to', $date);
            $start_date = $dates[0];
            $end_date = $dates[1];
            $cab_id = $_POST['cab_id'];


            $this->Get_shift_data($start_date, $end_date, $cab_id);
        }
        else
        {
            $data['fetch_cab'] = $this->Activity_model->get_cab();
            $data['active_page'] = 'shift_image';
            $this->load->view('site-nav/header', $data);
            $this->load->view('shift_image', $data);
            $this->load->view('site-nav/footer');
        }
    }

    /*for shift data */
    function Get_shift_data($start_date, $end_date, $cab_id)
    {
   $this->load->library('Ajax_pagination');
        $s_date = trim($start_date);
        $e_data = trim($end_date);
        $extra_param = array(
            "start_date" => $s_date,
            "end_date" => $e_data,
            "cab_id" => $cab_id);

        $count = $this->Activity_model->count_shift_image_report($start_date, $end_date,
            $cab_id);
        $pagingconfig = $this->paginationlib->initPagination("Activity/Get_shift_data_ajax",
            $count, 10, 3, $extra_param);
        // $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $this->ajax_pagination->initialize($pagingconfig);
        //$extra_param=json(array("driver_id"=>$did,"status"=>$status));

        $data['link'] = $this->ajax_pagination->create_links();

        $limit = 10;
        $offset = 0;
        $data['shift_image_report'] = $this->Activity_model->get_shift($start_date, $end_date,
            $cab_id, $offset, $limit);
		$data['time']=$extra_param;
        //print_R($data);
        $this->load->view('shift_image_data', $data);

    }
    /*get shift data ajax */
    function Get_shift_data_ajax()
    {
		   $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');

        $post = json_decode($postdata, true);

        $cab_id = $post['cab_id'];

        $start_date = trim($post['start_date']);
        $end_date = trim($post['end_date']);
        $count = $this->Activity_model->count_shift_image_report($start_date, $end_date,
            $cab_id);
        $extra_param = array(
            "start_date" => $start_date,
            "end_date" => $end_date,
            "cab_id" => $cab_id);
        $pagingconfig = $this->paginationlib->initPagination("Activity/Get_shift_data_ajax",
            $count, 10, $post['page'], $extra_param);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $limit = $pagingconfig['per_page'];
        if (!$post['page'])
        {
            $offset = 0;
        }
        else
        {
            $offset = ($post['page'] - 1) * $limit;
            $offset = $post['page'];
        }
        $data['shift_image_report'] = $this->Activity_model->get_shift($start_date, $end_date,
            $cab_id, $offset, $limit);


        //load the view
        $this->load->view('shift_image_data', $data, false);
    }


    /*function for get image */
    function Get_image()
    {
        $shift_id = $_POST['sid'];
        $data['image'] = $this->Activity_model->get_shift_image($shift_id);
        $this->load->view('shift_images', $data);

    }


    /******************************************
    *******Tips function *******************/

    function Tips()
    {
        if ((isset($_POST['date'])))
        {
            $date = $_POST['date'];
            $s_date = trim($date);
            $extra_param = array("start_date" => $s_date);
            $count = $this->Activity_model->Count_Tips_report($s_date);
            $pagingconfig = $this->paginationlib->initPagination("Activity/Get_tips_data_ajax",
                $count, 10, 3, $extra_param);
            $this->ajax_pagination->initialize($pagingconfig);
            $data['link'] = $this->ajax_pagination->create_links();
            $limit = 10;
            $offset = 0;
            $data['total_tips_report'] = $this->Activity_model->Get_tips($s_date, $offset, $limit);
            //print_R($data);

            $this->load->view('total_tips_report', $data);

        }
        else
        {
            $data['active_page'] = 'tips_report';
            $this->load->view('site-nav/header', $data);
            $this->load->view('tips_report');
            $this->load->view('site-nav/footer');
        }


    }


    /*get tips data ajax */
    function Get_tips_data_ajax()
    {
        $postdata = $this->input->post('data');

        $post = json_decode($postdata, true);
        $start_date = trim($post['start_date']);
        $count = $this->Activity_model->Count_tips_report($start_date);
        $extra_param = array("start_date" => $s_date);
        $pagingconfig = $this->paginationlib->initPagination("Activity/Get_shift_data_ajax",
            $count, 10, 3, $extra_param);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $limit = $pagingconfig['per_page'];
        if (!$post['page'])
        {
            $offset = 0;
        }
        else
        {
            $offset = ($post['page'] - 1) * $limit;
            $offset = $post['page'];
        }
        $data['shift_image_report'] = $this->Activity_model->Get_tips($start_date, $offset,
            $limit);


        //load the view
        $this->load->view('total_tips_report', $data, false);
    }


    /*function for geeting tips booking */
    function Get_tips_booking()
    {
        $driver_id = $_POST['driver_id'];
        $date = $_POST['date'];
        $data['fetch_booking'] = $this->Activity_model->Get_tips_booking($driver_id, $date);
        $this->load->view('booking_tips_report', $data);
    }
    
    
    
    
    /* daily report of driver function */
    function daily_booking_report()
    {
         if ((isset($_POST['date'])) && ($_POST['driverid']))
        {
            $date = $_POST['date'];
            $dates = explode('to', $date);
            $start_date = $dates[0];
            $end_date = $dates[1];
            $driver_id = $_POST['driverid'];
             
            $this->daily_booking_report_data($start_date, $end_date, $driver_id);
        }
        else
        {
             $data['active_page'] = 'daily_activity';
            $data['fetch_driver'] = $this->Driver_model->get_driver_name();
           
            $this->load->view('site-nav/header',$data);
            $this->load->view('daily_driver_activity', $data);
            
            $this->load->view('site-nav/footer');
        }
}


/* daily booking report data after selecting date and driver id */
function daily_booking_report_data($s_date,$e_date,$driver_id)
{
	   $this->load->library('Ajax_pagination');
    $workdetail = $this -> Activity_model -> working_time($s_date,$e_date,$driver_id);
		//print_r($workdetail);
		//exit;
	
		//exit;
        $shift_data=array();
		if ($workdetail)
		{
			foreach ($workdetail as $working_time)
			{
				//print_r($working_time);
				$shift_start = $working_time['createdon'];
				$shift_end = $working_time['endon'];
				$start_date = $working_time['start_date'];
				$end_date = $working_time['end_date'];
				$diff = $this -> date_difference($start_date, $end_date);
				$driver_id = $working_time['driver_id'];
				$shift_id = $working_time['s_id'];

				$daydiff = $diff;
				if (!$diff)
				{
					$daydiff = 1;
					$loop = 1;
				}
				else
				{
					$daydiff = $diff;
					$loop = $daydiff + 1;
				}
				//echo "<br/>Loop".$loop;
				$total_working_time=0;
				for ($i = 1; $i <= ($loop); $i++)
				{
					
					//$start_date
					if ($diff == 0 && $i == 1)
					{
						//echo "in same day ";
						$shift_hr = $this -> time_difference($shift_end, $shift_start);
						//break time
						$break_hr = $this -> get_break_detail($shift_id, $start_date);
						$total_working_time+= $shift_hr - $break_hr;
						$time_string = $this -> format_time($total_working_time);
						$report_data = $this -> Activity_model -> getreport_data($driver_id, date('Y-m-d', strtotime($shift_start)));
						//print_r($report_data);
						//echo "date". date('Y-m-d', strtotime($shift_start));
						$key = array_search(date('Y-m-d', strtotime($shift_start)), array_column($shift_data, 'date'));
						if ($key)
						{
							//echo "<br/>find key";
							//add last working hr
							$last_working_hr = $shift_data[$key]['working_time'];
							$new_working_hr = $total_working_time + $last_working_hr;
							$shift_data[$key] = array("start_date" => $end_date, "end_date" => $shift_end, "shift_hr" => $shift_hr, "shift_hr_format" => $this -> format_time($shift_hr), "break_time" => $break_hr, "break_time_format" => $this -> format_time($break_hr), "working_time" => $new_working_hr, "working_time_format" => $this -> format_time($new_working_hr), "shift_id" => $shift_id, "driver_id" => $driver_id, "date" => date('Y-m-d', strtotime($end_date)), "booking_count" => $report_data[0]['booking_count'], "tip" => $report_data[0]['tip'], "payment" => $report_data[0]['payment'], "rating" => $report_data[0]['rating']);

						}
						else
						{
							$shift_data[] = array(
							"start_date" => $shift_start,
							"end_date" => $shift_end,
							"shift_hr" => $shift_hr,
							"shift_hr_format"=>$this -> format_time($total_working_time),
							"break_time"=>$break_hr,
							"break_time_format"=>$this->format_time($break_hr),
							"working_time"=>$total_working_time,
							"working_time_format"=>$this->format_time($total_working_time),
							"shift_id"=>$shift_id,
							"driver_id"=>$driver_id,
							"date"=>date('Y-m-d',strtotime($shift_start)),
							"booking_count"=>$report_data[0]['booking_count'],
							"tip"=>$report_data[0]['tip'],
							"payment"=>$report_data[0]['payment'],
							"rating"=>$report_data[0]['rating'],
							"name"=>$report_data[0]['first_name'].'_'.$report_data[0]['last_name'],
							"s_date"=>$s_date,
						    "e_date"=>$e_date
						);
						}
						//$shift_data[] = array("start_date" => $shift_start, "end_date" => $shift_end, "shift_hr" => $shift_hr, "shift_hr_format" => $this -> format_time($total_working_time), "break_time" => $break_hr, "break_time_formate" => $this -> format_time($break_hr), "working_time" => $total_working_time, "working_time" => $this -> format_time($total_working_time), "shift_id" => $shift_id, "driver_id" => $driver_id, "date" => date('Y-m-d', strtotime($shift_start)), "booking_count" => $report_data[0]['booking_count'], "tip" => $report_data[0]['tip'], "payment" => $report_data[0]['payment'], "rating" => $report_data[0]['rating']);
					}
					elseif ($diff != 0 && $i == 1)
					{
						//echo "<br/>999==>";
						//echo "1073";
						$next_day = strtotime("+$i day", strtotime($shift_start));
						$next_day = date('Y-m-d', $next_day);
						$shift_hr = $this -> time_difference($next_day, $shift_start);
						$break_hr = $this -> get_break_detail($shift_id, $shift_start);
						$total_working_time+= $shift_hr - $break_hr;
						$time_string = $this -> format_time($total_working_time);
						$report_data = $this -> Activity_model -> getreport_data($driver_id, date('Y-m-d', strtotime($shift_start)));
						//	print_r($report_data);
						//echo "date". date('Y-m-d', strtotime($shift_start));
						$key = array_search(date('Y-m-d', strtotime($shift_start)), array_column($shift_data, 'date'));
						if ($key)
						{
							//add last working hr
							$last_working_hr = $shift_data[$key]['working_time'];
							$new_working_hr = $total_working_time + $last_working_hr;
								$shift_data[$key] = array("start_date" => $end_date, "end_date" => $shift_end, "shift_hr" => $shift_hr, "shift_hr_format" => $this -> format_time($shift_hr), "break_time" => $break_hr, "break_time_format" => $this -> format_time($break_hr), "working_time" => $new_working_hr, "working_time_format" => $this -> format_time($new_working_hr), "shift_id" => $shift_id, "driver_id" => $driver_id, "date" => date('Y-m-d', strtotime($end_date)), "booking_count" => $report_data[0]['booking_count'], "tip" => $report_data[0]['tip'], "payment" => $report_data[0]['payment'], "rating" => $report_data[0]['rating']);

						}
						else
						{
							$shift_data[] = array(
							"start_date" => $shift_start,
							"end_date" => $next_day,
							"shift_hr" => $shift_hr,
							"shift_hr_format"=>$this -> format_time($shift_hr),
							"break_time"=>$break_hr,
							"break_time_format"=>$this->format_time($break_hr),
							"working_time"=>$total_working_time,
							"working_time_format"=>$this->format_time($total_working_time),
							"shift_id"=>$shift_id,
							"driver_id"=>$driver_id,
							"date"=>date('Y-m-d',strtotime($shift_start)),
							"booking_count"=>$report_data[0]['booking_count'],
							"tip"=>$report_data[0]['tip'],
							"payment"=>$report_data[0]['payment'],
							"rating"=>$report_data[0]['rating'],
							"name"=>$report_data[0]['first_name'].'_'.$report_data[0]['last_name'],
								"s_date"=>$s_date,
						        "e_date"=>$e_date
							
						);
						}
						//$shift_data[] = array("start_date" => $shift_start, "end_date" => $next_day, "shift_hr" => $shift_hr, "shift_hr_format" => $this -> format_time($shift_hr), "break_time" => $break_hr, "break_time_format" => $this -> format_time($break_hr), "working_time" => $total_working_time, "working_time_format" => $this -> format_time($total_working_time), "shift_id" => $shift_id, "driver_id" => $driver_id, "date" => date('Y-m-d', strtotime($shift_start)), "booking_count" => $report_data[0]['booking_count'], "tip" => $report_data[0]['tip'], "payment" => $report_data[0]['payment'], "rating" => $report_data[0]['rating']);
					}
					elseif ($i == ($loop))
					{
						//echo "here";

						$shift_hr = $this -> time_difference($shift_end, $end_date);
						$break_hr = $this -> get_break_detail($shift_id, $shift_end);
						$total_working_time+= $shift_hr - $break_hr;
						$time_string = $this -> format_time($total_working_time);
						$report_data = $this -> Activity_model -> getreport_data($driver_id, date('Y-m-d', strtotime($end_date)));
						//chk particular date already have working hr for other shift
						date('Y-m-d', strtotime($end_date));
							$key = array_search(date('Y-m-d', strtotime($end_date)), array_column($shift_data, 'date'));
						if ($key)
						{
							//add last working hr
							$last_working_hr = $shift_data[$key]['working_time'];
							$new_working_hr = $total_working_time + $last_working_hr;
							$shift_data[$key] = array("start_date" => $end_date, "end_date" => $shift_end, "shift_hr" => $shift_hr, "shift_hr_format" => $this -> format_time($shift_hr), "break_time" => $break_hr, "break_time_format" => $this -> format_time($break_hr), "working_time" => $new_working_hr, "working_time_format" => $this -> format_time($new_working_hr), "shift_id" => $shift_id, "driver_id" => $driver_id, "date" => date('Y-m-d', strtotime($end_date)), "booking_count" => $report_data[0]['booking_count'], "tip" => $report_data[0]['tip'], "payment" => $report_data[0]['payment'], "rating" => $report_data[0]['rating']);

						}
						else
						{
							$shift_data[] = array(
							"start_date" => $end_date,
							"end_date" => $shift_end,
							"shift_hr" => $shift_hr,
							"shift_hr_format"=>$this -> format_time($shift_hr),
							"break_time"=>$break_hr,
							"break_time_format"=>$this->format_time($break_hr),
							"working_time"=>$total_working_time,
							"working_time_format"=>$this->format_time($total_working_time),
							"shift_id"=>$shift_id,
							"driver_id"=>$driver_id,
							"date"=>date('Y-m-d',strtotime($end_date)),
							"booking_count"=>$report_data[0]['booking_count'],
							"tip"=>$report_data[0]['tip'],
							"payment"=>$report_data[0]['payment'],
							"rating"=>$report_data[0]['rating'],
							"name"=>$report_data[0]['first_name'].'_'.$report_data[0]['last_name'],
								"s_date"=>$s_date,
						    "e_date"=>$e_date
						);
						}
					}
					else
					{

						$shift_start_date = $shift_start;
						$last_count = $i - 1;
						$shift_start_on = strtotime("+$last_count day", strtotime($shift_start));
						$next_day = strtotime("+1 day", $shift_start_on);
						$shift_start_on = date('Y-m-d', $shift_start_on);
						$next_day = date('Y-m-d', $next_day);

						$shift_hr = $this -> time_difference($next_day, $shift_start_on);
						//break time
						//$break_hr = $this -> get_break_detail($shift_id, $shift_start);
						$total_working_time = $shift_hr ;
						$time_string = $this -> format_time($total_working_time);
						$report_data = $this -> Activity_model -> getreport_data($driver_id, date('Y-m-d', strtotime($shift_start)));
						//print_r($report_data);
						date('Y-m-d', strtotime($shift_start_on));
						$key = array_search(date('Y-m-d', strtotime($shift_start_on)), array_column($shift_data, 'date'));
						if ($key)
						{
							//add last working hr
							$last_working_hr = $shift_data[$key]['working_time'];
							$new_working_hr = $total_working_time + $last_working_hr;
							$shift_data[$key] = array("start_date" => $end_date, "end_date" => $shift_end, "shift_hr" => $shift_hr, "shift_hr_format" => $this -> format_time($shift_hr), "break_time" => $break_hr, "break_time_format" => $this -> format_time($break_hr), "working_time" => $new_working_hr, "working_time_format" => $this -> format_time($new_working_hr), "shift_id" => $shift_id, "driver_id" => $driver_id, "date" => date('Y-m-d', strtotime($shift_start_on)), "booking_count" => $report_data[0]['booking_count'], "tip" => $report_data[0]['tip'], "payment" => $report_data[0]['payment'], "rating" => $report_data[0]['rating']);

						}
						else
						{
							$shift_data[] = array(
							"start_date" => $shift_start_on,
							"end_date" => $next_day,
							"shift_hr" => $shift_hr,
							"shift_hr_format"=>$this -> format_time($shift_hr),
							"break_time"=>$break_hr,
							"break_time_formate"=>$this->format_time($break_hr),
							//"break_time"=>$break_hr." ".$this->format_time($break_hr),
							"working_time"=>$total_working_time,
							"working_time_format"=>$this->format_time($total_working_time),
							"shift_id"=>$shift_id,
							"driver_id"=>$driver_id,
							"date"=>date('Y-m-d',strtotime($shift_start_on)),
							"booking_count"=>$report_data[0]['booking_count'],
							"tip"=>$report_data[0]['tip'],
							"payment"=>$report_data[0]['payment'],
							"rating"=>$report_data[0]['rating'],
							"name"=>$report_data[0]['first_name'].'_'.$report_data[0]['last_name'],
								"s_date"=>$s_date,
						    "e_date"=>$e_date
						);
						}

					}
				}
              
				//print_r ($shift_data);

				//exit;

			}
//echo "<pre>";
           // print_r($shift_data);
          // $data['shift_data']=$this->unique_multidim_array($shift_data,'date');
           // echo '<pre>';
           //print_r($data);
           $data['shift_data']=$shift_data;
            $this->load->view('daily_driver_report_data', $data);
           

		}
		else
			{
				 $this->load->view('daily_driver_report_data', $shift_data);
			}
		
}





function time_difference($endtime, $starttime)
	{
		//echo $endtime;
		$end_time = strtotime($endtime);
		$start_time = strtotime($starttime);
		return (abs($end_time - $start_time));
		

	}

	function format_time($sec)
	{
		$hours = floor($sec / 3600);
		$minutes = floor(($sec / 60) % 60);
		$seconds = $sec % 60;
		$duration = (!$hours) ? "" : $hours . " HR ";
		$duration .= (!$minutes) ? "" : $minutes . " Min ";
	
		return $duration;
	}

	function date_difference($start_date, $end_date)
	{
		//echo $start_date;
		$date1 = date_create($start_date);
		$date2 = date_create($end_date);
		$diff = date_diff($date1, $date2);
		return $diff -> format("%a");
	}
	function get_break_detail($shift_id,$date)
	{
		//echo "shift_id=>".$shift_id;
		//echo "<br/>start date=>".$date;
		   $this->load->library('Ajax_pagination');
        $date=date('Y-m-d',strtotime($date));
        $break_end="";
		$shift_break_detail =$this->Activity_model->shift_break_detail($shift_id,$date);
		if(!$shift_break_detail)
		{
			return 0;
		}
		
		 		$break_hr = 0;
				for ($j = 0; $j < count($shift_break_detail); $j++)
				{
					
					//print_r($shift_break_detail[$i]);
					$break_type = $shift_break_detail[$j]['type'];
					if($j==0 && $break_type!="STARTED")
					{
						$previous_day = strtotime("-1 day", strtotime($date));
						$break_start=date('Y-m-d',$previous_day);
						if($break_type=="ENDED")
						{
							$break_end=$shift_break_detail[$j]['break_time'];
						}
						
						
						$break_hr += $this -> time_difference($break_end, $break_start, 1);
					}elseif($j==count($shift_break_detail) && $break_type=='STARTED')
					{
					   $next_day = strtotime("-1 day", strtotime($date));
					   $break_end=date('Y-m-d',$next_day);
					   $break_start=$shift_break_detail['break_time'];
					   
						
					   $break_hr += $this -> time_difference($break_end, $break_start, 1);
					   
					}else 
					{
					   if($break_type == "STARTED")		
						$break_start = $shift_break_detail[$j]['break_time'];
						$j = $j + 1;
						$break_type = $shift_break_detail[$j]['type'];
						if ($break_type == 'ENDED')
						{
							$break_end = $shift_break_detail[$j]['break_time'];
						}
                       						
						$break_hr += $this -> time_difference($break_end, $break_start, 1);
					}
				}
                 return $break_hr;
	
}

function unique_multidim_array($array, $key){
    $temp_array = array();
    $i = 0;
    $key_array = array();
   
    foreach($array as $val){
        if(!in_array($val[$key],$key_array)){
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}


/* for getting all report and data according date and driver id total accept and total decline */
 function all_booking_report()
    {
        if ((isset($_POST['driverid'])) && ($_POST['status_type']) &&($_POST['date']))
        {

            $driver_id = $_POST['driverid'];
            $status = $_POST['status_type'];
			$date=$_POST['date'];
            $this->booking_data($driver_id, $status,$date);
        }
        else
        {
            $data['fetch_driver'] = $this->Driver_model->get_driver_name();
            $data['active_page'] = 'all_booking_report';
            $this->load->view('site-nav/header', $data);
            $this->load->view('driver_activity', $data);
            $this->load->view('site-nav/footer');
        }
    }
    
    
    
   public function Booking_date()
    {
        $booking_date = $_POST['date'];
        $driver_id = $_POST['driverid'];
        $data['driver_report'] = $this->Activity_model->get_all_driver_report($booking_date,$driver_id);
        $data['time']=$booking_date;
        $this->load->view('all_driver_report', $data);


    }
    
    
 /*turn over report month wise */
  function turn_over_report()
    {
       
        if ((isset($_POST['date'])))
        {
             
         $date = $_POST['date'];
            $dates = explode('to', $date);
            $start_date = $dates[0];
            $end_date = $dates[1];
            $this->Get_turnover_report($start_date,$end_date);
        }
        else
        {
            $data['turn_over_report']=array();
            $data['active_page'] = 'turn_over_report';
            $this->load->view('site-nav/header', $data);
            $this->load->view('turn_over_report', $data);
            $this->load->view('site-nav/footer');
        }
}





 /* get driver report according booking date */
      function Get_turnover_report($start_date,$end_date)
    {
		   $this->load->library('Ajax_pagination');
           $s_date=trim(date('Y-m',strtotime($start_date)));
           $e_date=trim(date('Y-m',strtotime($end_date)));
        $extra_param = array("start_date" => $s_date,"end_date"=>$e_date);
        $count = $this->Activity_model->count_turnover_report($s_date,$e_date);
        $pagingconfig = $this->paginationlib->initPagination("/Activity/turnover_report_data_ajax",
            $count, 10, 3, $extra_param);
        // $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $this->ajax_pagination->initialize($pagingconfig);
        //$extra_param=json(array("driver_id"=>$did,"status"=>$status));
        $data['link'] = $this->ajax_pagination->create_links();
        $limit = 10;
        $offset = 0;
        $data['turnover_report'] = $this->Activity_model->get_turnover_report($s_date,$e_date,$offset, $limit);
   $data['date']=array("s_date"=>$start_date,"e_date"=>$end_date);
        $this->load->view('total_turnover_report', $data);
    }
    
    function turnover_report_data_ajax()
    {
          $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');

        $post = json_decode($postdata, true);

        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
       $count = $this->Activity_model->count_turnover_report($start_date,$end_date);
        $extra_param = array("start_date" => $start_date,"end_date" => $end_date);
        $pagingconfig = $this->paginationlib->initPagination("/Activity/turnover_report_data_ajax",
            $count, 10, $post['page'], $extra_param);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $limit = $pagingconfig['per_page'];
        if (!$post['page'])
        {
            $offset = 0;
        }
        else
        {
            $offset = ($post['page'] - 1) * $limit;
            $offset = $post['page'];
        }
        $data['turnover_report'] = $this->Activity_model->get_turnover_report($start_date, $end_date,
            $offset, $limit);
         

        //load the view
        $this->load->view('total_turnover_report', $data);
    }

    /* user for sum of total break */
	function count_break($break)
	{
		
                                  $end_break='0';
                          for ($i = 0; $i < count($break); ++$i) {
                                 
                                  $type=$break[$i]['type'];
                                  if($type=='STARTED')
                                  {
                                              
                                       $break_type='Break Started';
                                       $date=$break[$i]['break_time'];
                                      $b_started= date("H:i:s",strtotime($date)+ 2*3600);
                                  }
                                  else
                                  {
                                      
                                        $break_type='Break Ended';           
                                  $date=$break[$i]['break_time'];
                                  $b_endtime= date("H:i:s",strtotime($date)+ 2*3600);
								   $end_time = strtotime($b_endtime);
		                            $start_time = strtotime($b_started);
		
			             $end_break+=abs($end_time - $start_time);
                                  }
                                   
						 
						  
                       
                             
						  }
						   $hours = floor($end_break / 3600);
                                  $minutes = floor(($end_break / 60) % 60);
                                  $seconds = $end_break % 60;
                                  $duration = (!$hours) ? "" : $hours . " HR ";
                                  $duration .= (!$minutes) ? "" : $minutes . " Min ";
                                
                    
                                return $duration;;
	
}
 function sendmailee()
 {
	
//	$header=$_POST['headers'];
	$html=$_POST['html_data'];
	$header=$_POST['header_data'];
	$name=$_POST['name'];
	$date=date('H:i:s');
	$html .= '<style>'.file_get_contents('http://privatedriverapp.com/app/css/style1t.css').'</style>';
	
	
   if($name=='')
	{
		$filename='Report'.'_'.$date.'.pdf';
	}
	else
	{
	$filename=$name.'_'.$date.'.pdf';
	}
    $ci =& get_instance();
    $ci->load->library('Pdf');
    $ci->load->library('email');
    
  
    $html = str_replace("'",'"',$html);
	//// create new PDF document
	//$pdf = new FoxPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf = new Pdf($header);
	//$yes=yes.png;
	//$no=no.png;
	// remove default header/footer
	//$pdf->setPrintHeader(false);
	//$pdf->setPrintFooter(false);
	
	// set document information
	//$pdf->setHtmlHeader($header);
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Billing Department');
	$pdf->SetTitle('Private Driver Taxi');
	$pdf->SetSubject('');
	$pdf->SetKeywords('');
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	//set margins PDF_MARGIN_TOP
	$pdf->SetMargins(10.0, 33.0, 10.0);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	
	//set auto page breaks
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
	
	//set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	// set font
	$pdf->SetFont('helvetica', '', 9);
	
	// add a page
	$pdf->AddPage();
	//echo $html;
	$pdf->writeHTML($html, true, false, true, false, "");
	
	//Close and output PDF document
	$pdf->Output('pdf_download/'.$filename, "F");
	//$pdf->Output($filename, "F");
    
    
	$filepath='pdf_download/';
  echo  $filename;




	
	

}
function download()
{
	 $file_name=$_GET['file'];
	 $file="http://privatedriverapp.com/app/pdf_download/".$file_name;
	 
	 header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
   // header('Cache-Control: must-revalidate');
    header('Pragma: public');
   // header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
	unlink("http://privatedriverapp.com/app/pdf_download/".$file_name);
    exit();
}

}