<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<div class="box-body record">
                  <table id="example" class="table table-bordered table-striped" style="font-size:13px;">
                    <thead>
                      <tr>
                        <th>Name</th>
                         <th>Email</th>
                         <th>Contact</th>
                         <th>Address</th>
                        <th>Status</th>
                        <th style="width: 10%; text-align: center;">Action</th>
                        <th>Delete</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(sizeof($restaurant))
                      {
                       
                        for($i=0;$i<sizeof($restaurant);$i++)
                        {
                          $name=$restaurant[$i]->name;
                          
                          
                      ?>
                      <tr>
                        <td><?php echo $name;?></td>
                        <td><?php echo $restaurant[$i]->email;?>
                        <td><?php echo $restaurant[$i]->contact_number;?></td>
                        <td><?php echo $restaurant[$i]->address;?></td>
                        
                       
                       
                      
                      
                       <td><a href="javascript:void(0);" onclick="changeStatus('<?php echo $restaurant[$i]->id;?>','loading_<?php echo $restaurant[$i]->id;?>', 'span_<?php echo $restaurant[$i]->id;?>')">
                                    <?php if($restaurant[$i]->isactive=='0'){?>
                                    <span id="span_<?php echo $restaurant[$i]->id?>" class="label label-warning">Inactive</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $restaurant[$i]->id?>" class="label label-success">Active</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $restaurant[$i]->id;?>" style="display: none;"><img src="<?php echo base_url();?>/images/loader19.gif" /></div>

                     </td>
                       <td><?php echo $restaurant[$i]->status;?></td>
                       <td>
                        
                       <a href="<?=site_url('Restaurant/add_restaurant').'/'.$restaurant[$i]->id; ?>" title="Edit"<i class="fa fa-pencil-square-o fa-2x"></a></i>
                       <img src="<?php echo base_url();?>images/loader19.gif" id="image_<?=$restaurant[$i]->id;?>" style="display: none;"/>
                      </td>
                      <td><a href="javascript:void(0);" onclick="changedeleteStatus('<?php echo $restaurant[$i]->id;?>','delloading_<?php echo $restaurant[$i]->id;?>', 'spandel_<?php echo $restaurant[$i]->id;?>')">
                                    <?php if($restaurant[$i]->isdelete=='1'){?>
                                    <span id="spandel_<?php echo $restaurant[$i]->id?>" class="fa fa-trash fa-2x" style="color:red;"></span></a>
                                    <?php }?>
                                    <br /><div id="delloading_<?php echo $restaurant[$i]->id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                      </td>
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      ?>
                      
                      
                      
                    </tbody>
                    <tfoot>
                       
                    </tfoot>
                  </table>
                  <?php
                  
                   ?>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
        
          </div>
                </div>