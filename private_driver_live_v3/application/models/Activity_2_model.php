<?php

class Activity_2_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    /* driver daliy basis report */

    function driver_reports($start, $limit)
    {
        $fetch_driver_report = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id limit  $limit,$start");
        // echo $this->db->last_query();
        $count = $fetch_driver_report->num_rows();
        if ($count > 0)
        {
            return $fetch_driver_report->result();
        }
        else
        {
            return false;
        }
    }
    /* conunting rows */

    function count_all()
    {
        $fetch_driver_report = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id ");
        $count = $fetch_driver_report->num_rows();
        return $count;
    }

    /* count according booking */
    function count_according_booking($bdate)
    {
        $fetch_driver_breport = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id where date(cab_booking.booking_time)='$bdate'");
        // echo $this->db->last_query();
        $count = $fetch_driver_breport->num_rows();
        return $count;
    }

    /* drive report according to date */
    function driver_bdate_reports($start, $limit, $bdate)
    {
        $fetch_driver_report = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id  where date(cab_booking.booking_time)='$bdate' limit  $limit,$start");
        // echo $this->db->last_query();
        $count = $fetch_driver_report->num_rows();
        if ($count > 0)
        {
            return $fetch_driver_report->result();
        }
        else
        {
            return false;
        }
    }

    /* funtion to count driver */
    function count_driver($did)
    {
        $fetch_driver = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id where cab_booking.driver_id='$did'");
        // echo $this->db->last_query();
        $count = $fetch_driver->num_rows();
        return $count;
    }

    /* fetch result according driver */
    function driver_booking($start, $limit, $did)
    {
        $fetch_driver_result = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id  where cab_booking.driver_id='$did' limit  $limit,$start");
        // echo $this->db->last_query();
        $count = $fetch_driver_result->num_rows();
        if ($count > 0)
        {
            return $fetch_driver_result->result();
        }
        else
        {
            return false;
        }
    }

    /* function fetch driver daliy report */
    function Daily_report($start, $limit, $booking_date)
    {
        if ($booking_date > 0)
        {
            $fetch_report_data = $this->db->query("select d.driver_id,d.first_name,d.last_name,count(case when c.status = 'TRIP_ENDED' then 1 else null end) as tripend,
            (case when c.status='DRIVER_ACCEPTED' then 1 else null end) as driveraccept,
            (case when c.status='CANCLLED' then 1 else null end) as cancelled,
            (case when c.status='ADVANCE_BOOKING' then 1 else null end) as advancebooking
            
 from cab_booking as c,driver as d where c.driver_id=d.driver_id and date(c.booking_time)='$booking_date' limit  $limit,$start");
        }
        else
        {
            $fetch_report_data = $this->db->query("select d.driver_id,d.first_name,d.last_name,count(case when c.status = 'TRIP_ENDED' then 1 else null end) as tripend,
            (case when c.status='DRIVER_ACCEPTED' then 1 else null end) as driveraccept,
            (case when c.status='CANCLLED' then 1 else null end) as cancelled,
            (case when c.status='ADVANCE_BOOKING' then 1 else null end) as advancebooking
            
 from cab_booking as c,driver as d where c.driver_id=d.driver_id  limit  $limit,$start");
        }
        $count_data = $fetch_report_data->num_rows();
        if ($count_data > 0)
        {
            return $fetch_report_data->result();
        }
        else
        {
            return false;
        }
    }

    /* function for count total driver */
    function count_drivers($booking_date)
    {
        if ($booking_date > 0)
        {
            $total_driver = $this->db->query("select d.driver_id,d.first_name,d.last_name,count(case when c.status = 'TRIP_ENDED' then 1 else null end) as tripend,
            (case when c.status='DRIVER_ACCEPTED' then 1 else null end) as driveraccept,
            (case when c.status='CANCLLED' then 1 else null end) as cancelled,
            (case when c.status='ADVANCE_BOOKING' then 1 else null end) as advancebooking
 from cab_booking as c,driver as d where c.driver_id=d.driver_id and date(c.booking_time)='$booking_date'");
        }
        else
        {
            $total_driver = $this->db->query("select d.driver_id,d.first_name,d.last_name,count(case when c.status = 'TRIP_ENDED' then 1 else null end) as tripend,
            (case when c.status='DRIVER_ACCEPTED' then 1 else null end) as driveraccept,
            (case when c.status='CANCLLED' then 1 else null end) as cancelled,
            (case when c.status='ADVANCE_BOOKING' then 1 else null end) as advancebooking
 from cab_booking as c,driver as d where c.driver_id=d.driver_id");
        }
        $total_count = $total_driver->num_rows();
        return $total_count;
    }

    /* count log */
    function count_all_log($date, $api_name)
    {

        if ($api_name != "" && $date > 0)
        {
            $condition = "where date(createdon)='$date'and uri='$api_name'";
        }
        else
            if ($api_name != "")
            {
                $condition = "where uri='$api_name'";
            }
            else
                if ($date > 0)
                {
                    $condition = "where date(createdon)='$date'";

                }
                else
                {
                    $condition = "";
                }
                $total_log = $this->db->query("select * from logs $condition");
        //echo $this->db->last_query();
        $total_count = $total_log->num_rows();
        return $total_count;
    }

    /* fetch log */
    function log($start, $limit, $date, $api_name)
    {

        if ($api_name != "" && $date > 0)
        {
            $condition = "where date(createdon)='$date'and uri='$api_name'";
        }
        else
            if ($api_name != "")
            {
                $condition = "where uri='$api_name'";
            }
            else
                if ($date > 0)
                {
                    $condition = "where date(createdon)='$date'";

                }
                else
                {
                    $condition = "";
                }
                $total_log = $this->db->query("select * from logs $condition limit $limit,$start");
        //echo $this->db->last_query();
        return $total_log->result();


    }
    /* function for know driver report*/
    function get_driver_report($booking_date, $driver_id)
    {
        $date = explode('to', $booking_date);
        $start_date = $date[0];
        $end_date = $date[1];
        $result = $this->db->query("select driver_id,SUM(case when driver_id='$driver_id' then 1 else null end) as totalbooking,
SUM(case when driver_response='DRIVER_REJECTED' then 1 else null end) as driverreject,
SUM(case when driver_response='DRIVER_ACCEPTED' then 1 else null end) as totalaccept        
 from cab_booking_history  where driver_id='$driver_id'and date(driver_datetime) BETWEEN '$start_date' AND '$end_date'");
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }
    }


    // count how many booking according status
    function count_booking_report($did, $status)
    {

        if ($status == 'total_recieve')
        {
            $total_driver = $this->db->query("select * FROM cab_booking_history where driver_id='$did'");
            //  echo $this->db->last_query();
        }
        else
            if ($status == 'DRIVER_ACCEPTED')
            {

                $total_driver = $this->db->query("SELECT booking_id,driver_datetime,driver_response from cab_booking_history where driver_response='$status' and driver_id='$did'");
                //echo $this->db->last_query();
            }
            else
                if ($status == 'DRIVER_REJECTED')
                {
                    $total_driver = $this->db->query("SELECT booking_id,driver_datetime,driver_response from cab_booking_history where driver_response='$status' and driver_id='$did'");
                    // echo $this->db->last_query();
                }
                else
                {
                    return false;
                }
                $total_count = $total_driver->num_rows();
        return $total_count;
    }

    /* total driver report */
    function driver_booking_report($did, $status, $offset, $limit)
    {
        if ($status == 'total_recieve')
        {
            $total_driver = $this->db->query("select booking_id,driver_datetime,driver_response FROM cab_booking_history where driver_id='$did' limit $offset,$limit");
            // echo $this->db->last_query();
        }
        else
            if ($status == 'DRIVER_ACCEPTED')
            {

                $total_driver = $this->db->query("SELECT booking_id,driver_datetime,driver_response from cab_booking_history where driver_response='$status' and driver_id='$did' limit $offset,$limit");
                //echo $this->db->last_query();
            }
            else
                if ($status == 'DRIVER_REJECTED')
                {
                    $total_driver = $this->db->query("SELECT booking_id,driver_datetime,driver_response from cab_booking_history where driver_response='$status' and driver_id='$did' limit $offset,$limit");
                    // $this->db->last_query();

                }
                else
                {
                    return false;
                }
                //echo $this->db->last_query();
                $total_count = $total_driver->num_rows();
        if ($total_count > 0)
        {
            return $total_driver->result_array();
        }
        else
        {
            return false;
        }
    }


    /* function for counting shift */
    function count_shift_report($start_date, $end_date, $driver_id)
    {

        $result = $this->db->query("select d.first_name,d.last_name,s.status,s.createdon,s.endon from driver as d,shift as s where s.driver_id=d.driver_id and s.driver_id='$driver_id' and date(s.createdon) BETWEEN '$start_date' and '$end_date'");
        $total_count = $result->num_rows();
        if ($total_count > 0)
        {
            return $total_count;
        }
        else
        {
            return 0;
        }
    }

    /* fetch all shift between date */
    function driver_shift_report($start_date, $end_date, $driver_id)
    {

        /*$result = $this->db->query("select d.first_name,d.last_name,s.status,s.createdon,s.endon from driver as d,shift as s where s.driver_id=d.driver_id and s.driver_id='$driver_id' and date(s.createdon) BETWEEN '$start_date' and '$end_date' limit $offset,$limit");
        //echo $this->db->last_query();
        $total_count = $result->num_rows();
        if ($total_count > 0)
        {
        return $result->result_array();
        }
        else
        {
        return false;
        }  */
        $where_condtion = array(
            "driver_id" => $driver_id,
            "createdon>=" => $start_date,
            "createdon<=" => $end_date);
        $this->db->select("*");
        $this->db->from("shift");
        $this->db->join("cab","cab.cab_id=shift.cab_id");
        $this->db->join("cab_type","cab_type.type_id=cab.cab_type");
        $this->db->where("driver_id",$driver_id);
        $this->db->where("createdon>=",$start_date);
        $this->db->where("createdon<=",$end_date);
        $query=$this->db->get();
        //$query = $this->db->get_where("shift", $where_condtion);
        echo $this->db->last_query();
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }else
        {
            return false;
        }
    }
    function shift_break_detail($shift_id)
    {
        $where_condition=array("shift_id"=>$shift_id);
        $query=$this->db->get_where("break",$where_condition);
        if($query->num_rows())
        {
            return $query->result_array();
        }else
        {
            return false;
        }
    }


    /* function for counting break */
    function count_break_report($start_date, $end_date, $driver_id)
    {

        $result = $this->db->query("select d.first_name,d.last_name,b.break_time,b.type,c.cab_plate_no FROM driver as d,break as b,shift as s,cab as c where b.driver_id=d.driver_id and b.shift_id=s.s_id and s.cab_id=c.cab_id and b.driver_id='$driver_id' and date(b.break_time) BETWEEN '$start_date' AND '$end_date'");
        $total_count = $result->num_rows();
        if ($total_count > 0)
        {
            return $total_count;
        }
        else
        {
            return 0;
        }
    }

    /* fetch all break between date */
    function driver_break_report($start_date, $end_date, $driver_id, $offset, $limit)
    {

        $result = $this->db->query("select d.first_name,d.last_name,b.break_time,b.type,c.cab_plate_no FROM driver as d,break as b,shift as s,cab as c where b.driver_id=d.driver_id and b.shift_id=s.s_id and s.cab_id=c.cab_id and b.driver_id='$driver_id' and date(b.break_time) BETWEEN '$start_date' AND '$end_date' limit $offset,$limit");
        //echo $this->db->last_query();
        $total_count = $result->num_rows();
        if ($total_count > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }
    }
    
    function driver_booking_detail($shift_starttime,$shif_endtime,$driver_id)
    {
    	 $query="SELECT driver_response,driver_cancel,pickup_location,destination_location,booking_id,ch.driver_id,ch.driver_cancel_datetime,ch.driver_datetime 
    	FROM `cab_booking_history` ch 
    	join cab_booking b on ch.booking_id=b.id 
    	where driver_datetime BETWEEN '$shift_starttime' and '$shif_endtime' and ch.driver_id=$driver_id";
		$res=$this->db->query($query);
		//echo $this->db->last_query();
		if($res->num_rows()>0)
		{
			return $res->result_array();
		}else
		{
			return false;
		}
    }
    function booking_process($booking_id)
    {
    	$query="select * from driver_status where booking_id=$booking_id";
    	$res=$this->db->query($query);
    	if($res->num_rows()>0)
		{
			return $res->result_array();
		}else
			{
				return false;
			}
    }
	
	function drivertime_report($starttime,$endtime,$driver_id)
	{
		$query="select * from driver_status where driver_id=$driver_id and date(createdon) between '$starttime' and '$endtime' ";
	    $res=$this->db->query($query);
		echo $this->db->last_query();
		if($res->num_rows()>0)
		{
			return $res->result_array();
		}else
		{
			return false;
		}
		
	}
	
	function get_shiftdetail($shift_id)
	{
		$query ="SELECT t.title,c.cab_model,c.cab_plate_no FROM `shift` s 
		join cab c on c.cab_id =s.cab_id 
		join cab_type t on t.type_id=c.cab_type 
		where s.s_id=$shift_id";
		$res=$this->db->query($query);
	//	echo $this->db->last_query();
		if($res->num_rows()>0)
		{
			return $res->row_array();
		}else
			{
				return false;
			}
		
	}
	function get_pre_booking($booking_no)
	{
		$get_advance_booking=$this->db->query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.comment,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name FROM `cab_booking` join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.id='$booking_no'");
		//echo $this->db->last_query();
		return $get_advance_booking->result();
	}

}
