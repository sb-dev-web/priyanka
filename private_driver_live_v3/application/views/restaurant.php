<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Restaurant
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Restaurant</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <a href="<?php echo site_url('Restaurant/add_restaurant');?>"><input type="button" class="btn btn-block btn-warning" value="Add" style="float:right; margin-right:7px;margin-top:-8px; width:10%;"/></a><br/><br/>
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example" class="table table-bordered table-striped" style="font-size:13px;">
                    <thead>
                      <tr>
                        <th>Name</th>
                         <th>Email</th>
                         <th>Contact</th>
                       
                        <th>Address</th>
                        <th>Status</th>
                        <th style="width: 10%; text-align: center;">Action</th>
                        <th>Delete</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(sizeof($restaurant))
                      {
                       
                        for($i=0;$i<sizeof($restaurant);$i++)
                        {
                          $name=$restaurant[$i]->name;
                          
                          
                      ?>
                      <tr>
                        <td><?php echo $name;?></td>
                        <td><?php echo $restaurant[$i]->email;?>
                        <td><?php echo $restaurant[$i]->contact_number;?></td>
                        <td><?php echo $restaurant[$i]->address;?></td>
                        
                       
                       
                      
                      
                       <td><a href="javascript:void(0);" onclick="changeStatus('<?php echo $restaurant[$i]->id;?>','loading_<?php echo $restaurant[$i]->id;?>', 'span_<?php echo $restaurant[$i]->id;?>')">
                                    <?php if($restaurant[$i]->isactive=='0'){?>
                                    <span id="span_<?php echo $restaurant[$i]->id?>" class="label label-warning">Inactive</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $restaurant[$i]->id?>" class="label label-success">Active</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $restaurant[$i]->id;?>" style="display: none;"><img src="<?php echo base_url();?>/images/loader19.gif" /></div>

                     </td>
                     
                       <td>
                        
                       <a href="<?=site_url('Restaurant/add_restaurant').'/'.$restaurant[$i]->id; ?>" title="Edit"><i class="fa fa-pencil-square-o fa-2x"></a></i>
                       <img src="<?php echo base_url();?>images/loader19.gif" id="image_<?=$restaurant[$i]->id;?>" style="display: none;"/>
                      </td>
                      <td><a href="javascript:void(0);" onclick="changedeleteStatus('<?php echo $restaurant[$i]->id;?>','delloading_<?php echo $restaurant[$i]->id;?>', 'spandel_<?php echo $restaurant[$i]->id;?>')">
                                    <?php if($restaurant[$i]->isdelete=='1'){?>
                                    <span id="spandel_<?php echo $restaurant[$i]->id?>" class="fa fa-trash fa-2x" style="color:red;"></span></a>
                                    <?php }?>
                                    <br /><div id="delloading_<?php echo $restaurant[$i]->id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                      </td>
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      ?>
                      
                      
                      
                    </tbody>
                    <tfoot>
                       
                    </tfoot>
                  </table>
                  <?php
                  
                   ?>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
        
          </div>
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      
      <!-- Control Sidebar -->      
      
 <!--pop up -->
 <div id="dialog" style="display: none">
  <span id="ui-id-1" class="ui-dialog-title">fgdgfg</span>
        <div id="dvMap" style="height: 380px; width: 580px; border:3px solid black;">
        </div>
    </div>
      
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <!-- <script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->

<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
   
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css">

       <script type="text/javascript">
      $(function () {
        $("#example1").dataTable();
       
      });
    </script>
       <script>
       function changeStatus(id,loadingdiv,spanid)
{
  //alert();
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    if(spanValue=='Inactive')
    {  
       status = '1';
    }
    $.ajax({type:'POST',url: '<?php echo site_url('Restaurant/change_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       statusfeedback(response,spanid,loadingdiv);
     }});
}
function statusfeedback(retstatus,spanid,loadingdiv) 
{
    //alert(retstatus);
    if(retstatus=='0')
    {
        $('#'+spanid).removeClass('label-success').addClass('label-warning');
        $('#'+spanid).text("Inactive");
    }
    else if(retstatus=='1')
    {

        $('#'+spanid).removeClass('label-warning').addClass('label-success');
        $('#'+spanid).text("Active");
    }
     $('#'+loadingdiv).css('display','none');
    
}
</script>
<script>
function changedeleteStatus(id,loadingdiv,spanid)
{
  //alert();
  var r=confirm("Are you sure you want to delete this Restaurant?");
  if (r==true) {
    
  
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    $.ajax({type:'POST',url: '<?php echo site_url('Restaurant/change_delete_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       $("#image_"+id).closest('tr').fadeOut('slow')
     }});
}

else
{
  
   
}
}
</script>

 