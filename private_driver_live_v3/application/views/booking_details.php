<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" id="check">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            All Bookings Details
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">All Bookings</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                         Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        	Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              
             
            </div>
            <div class="box-body record">
              <br>


            
            
     
            <div class="box-body">
              <div style="width:auto;position: relative; top:-36px;">
            <img src="<?php  echo base_url('images/printer.png') ?> " onclick="PrintElem('#printdiv')"  style="float:right" />     
     </div>
              <div id="printdiv">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th class="col-md-1" style="padding-right: 4px;">Booking #</th>
                        <th class="col-md-3">Booking Date Time</th>
                      	<th class="col-md-3">Pickup Address</th>
                      	<th class="col-md-3">Destination Address</th>
                      	<th class="col-md-3">Driver</th>
                      	<th class="col-md-1">Status</th>
                        
                        
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                   //  print_r($advance_booking);
					 $completed_booking=$advance_booking;
                      if(sizeof($completed_booking))
                      
                      {
                        for($i=0;$i<sizeof($completed_booking);$i++)
                        {
                         
                          
                         
                      ?>
                      <tr>
                        
                       
                      
                       <td><a href="javascript:void(0);" onclick="customer_detail('<?php echo $completed_booking[$i]['id'];?>');"><?php echo $completed_booking[$i]['booking_number'];?></a></td>
                      <?php if($completed_booking[$i]['is_prebooking']=='N'){ ?>
                       <td class="col-md-3" style="padding-right: 4px;"><?php  $timestamp = strtotime(gmt_to_norway($completed_booking[$i]['booking_time']));
                           echo  date('d F Y G:i', $timestamp);?></td>
                      <?php }else{ ?>
                      	 <td class="col-md-3" style="padding-right: 4px;"><?php  $timestamp = strtotime(($completed_booking[$i]['booking_time']));
                           echo  date('d F Y G:i', $timestamp);?></td>
                      	
                      <?php } ?>
                       <td class="col-md-3"><?php echo $completed_booking[$i]['pickup_location'];?></td>
                       <td class="col-md-3"><?php echo $completed_booking[$i]['destination_location'];?></td>
              		  <td class="col-md-3">
              		  	<?php 
              		  		if($completed_booking[$i]['first_name']){
              		  			echo ucwords($completed_booking[$i]['first_name']." ".$completed_booking[$i]['last_name']);
              		  		}
							else{
								echo "----";
							}
              		  	?>
              		  	</td>
              		   <td class="col-md-1" >
              		<?php 
              		   		$string=str_replace("_"," ",$completed_booking[$i]['status']);
							$lower_status = strtolower($string);
							$status = ucwords($lower_status);
							
							 if($completed_booking[$i]['status'] == "FINISH"){
							 	?><span class="label label-success">
							 		<?php echo $status."ed";?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "TRIP_ENDED"){
							 	?><span class="label label-success">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "DRIVER_ACCEPTED"){
							 	?><span class="label label-success">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "ADVANCE_BOOKING"){
							 	?><span class="label label-success">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "TRIP_STARTED"){
							 	?><span class="label label-success">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "CUSTOMER_ACCEPTED"){
							 	?><span class="label label-success">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "DRIVER_ASSIGNED"){
							 	?><span class="label label-success">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "CUSTOMER_REJECTED"){
							 	?><span class="label label-danger">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "DRIVER_REJECTED"){
							 	?><span class="label label-danger">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "DRIVER_CANCELLED"){
							 	?><span class="label label-danger">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							  else if($completed_booking[$i]['status'] == "CANCELLED"){
							 	?><span class="label label-danger">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							   else if($completed_booking[$i]['status'] == "TIMEOUT"){
							 	?><span class="label label-warning">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "REQUEST_SENT"){
							 	?><span class="label label-warning">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							    else if($completed_booking[$i]['status'] == "CUSTOMER_NOT_RESPOND"){
							 	?><span class="label label-warning">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "NOT_SHOW"){
							 	?><span class="label label-info">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }
							 else if($completed_booking[$i]['status'] == "WAITING"){
							 	?><span class="label label-warning">
							 		<?php echo $status;?>
							 	</span>
							 	<?php
							 }		
							
							
              		   	   ;?>
              		   	
              		   	</td>
                       
                      
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      ?>


                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
            </div>
                           <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
              
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
       <!-- open customer pop up -->
        <!-- open customer pop up -->
<div class="modal fade" id="open_customer" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
               Booking Details
                <button style="color:black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
               <div id="showreport" name="showreport"></div>
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
    </div>   
</div>
       
       
       
       
       
       

                
                 <div id="showreport" name="showreport"></div>
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
    </div>   
</div>
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      
     
      </aside><!-- /.control-sidebar -->

     

 <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo base_url();?>js/my_custom.js"></script>
<script>
 
             
              function customer_detail(booking_no)
        {
      
       $.ajax({
            type:'POST',
            url: '<?php echo site_url('Booking/get_all_booking');?>',
            data:{"booking_no":booking_no},
             beforeSend: function() {
              $("#loading-image").show();
           },
            success: function(response) {
            //alert(response);
             $("#showreport").html(response);
             $('#open_customer').modal({
                 "backdrop" : "static"
         })
              $("#loading-image").hide();
             }});
             }
          
          
        
</script>

<script type="text/javascript">
    function PrintElem(elem)
    {
      //alert('hiii');
    //	var elem="#printdiv";
        Popup($(elem).html());
    }

</script>
     

      