        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Fare
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Fare</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
             
               <h3 class="box-title">Fare Type</h3>
              <select id="fare" name="fare" onchange="get_data(this.value);" style="margin-left:9px;height:29px;width:123px;">
              
                <option value="NORMAL">Regular</option>
                <option value="RESTRO">Restaurant</option>
              </select>
             <!-- <a href="<?php echo site_url('Generalinfo/add_fare');?>"><input type="button" class="btn btn-block btn-warning" value="Add" style="float:right; margin-right:7px;margin-top:-8px; width:10%;"/></a><br/><br/>-->
             
            </div>
            <div class="box-body">
              <center><img id="loading-image1" src="http://dev.privatedriverapp.com/app/images/loading.gif" style="display:none;" height="100px" width="100px"/>
              </center> <table id="example1" class="table table-bordered table-striped" style="font-size:14px;">
                    <thead>
                      <tr>
                        <th>Cab Class</th>
                        <th>No of Person</th>
                        <th>Basic Fare</th>
                         <!--<th>Minimum K.M.</th>-->
                         <th>Charge Per K.M.</th>
                        <th>Charges Per Min</th>
                        <th>Booking Time (In Hours)</th>
                      
                        <th>Status</th>
                        <th style="width: 8%; text-align: left;">Action</th>
                      
                      
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(sizeof($fare))
                      {
                        //print_r($fare);
                        for($i=0;$i<sizeof($fare);$i++)
                        {
                          $name=$fare[$i]->title;
                          $status=$fare[$i]->isactive;
                          if($status==1)
                          {
                            
                          }
                          else
                          {
                            
                          }
                      ?>
                      <tr>
                        <td><?php echo $name;?></td>
                        
                         <td><?php echo $fare[$i]->no_of_person;?></td>
                           <td><?php echo $fare[$i]->min_charges;?></td>
                             <!-- <td><?php echo $fare[$i]->min_km;?></td>-->
                             <td> 
                             <?php  $data=$fare[$i]->charges_per_km;
                                 $check=number_format($data, 2, '.', '');
                           echo $check;
                           
                           ?>
                             
                            </td>
                            <td><?php $data1=$fare[$i]->wait_time_charges;
                            $check1=number_format($data1, 2, '.', '');
                           echo $check1;
                            
                            ?></td>
                            <td><?php echo $time=$fare[$i]->ride_later_time.' '.'Hours';
                            
                           
                            
                            
                            ?></td>
                           
                       <td><a href="javascript:void(0);" onclick="changeStatus('<?php echo $fare[$i]->id;?>','loading_<?php echo $fare[$i]->id;?>', 'span_<?php echo $fare[$i]->id;?>')">
                                    <?php if($fare[$i]->isactive=='0'){?>
                                    <span id="span_<?php echo $fare[$i]->id?>" class="label label-warning">Inactive</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $fare[$i]->id?>" class="label label-success">Active</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $fare[$i]->id;?>" style="display: none;"><img src="<?php echo base_url();?>/images/loader19.gif" /></div>

                     </td>
                       <td>
                        
                       <a href="<?=site_url('Generalinfo/add_fare').'/'.$fare[$i]->id; ?>" title="Edit"><i class="fa fa-pencil-square-o fa-2x"></a></i>&nbsp;&nbsp;<!--<a href="javascript:void(0);" onclick="delete_driver('<?=$fare[$i]->item_id; ?>')" title="Remove"><i class="fa fa-trash"></i></a> -->
                       <img src="<?php echo base_url();?>/images/loader19.gif" id="image_<?=$fare[$i]->id;?>" style="display: none;"/>
                      </td>
                      
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      ?>
                      
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      
     
 <!--pop up -->
 <div id="dialog" style="display: none">
        <div id="dvMap" style="height: 380px; width: 580px;">
        </div>
    </div>
      
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <!-- <script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->
<!--<script type="text/javascript" src="<?=base_url();?>js/jquery.dataTables.min.js"></script>-->
<!--<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>-->
<!--<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>-->
<!--<script type="text/javascript" src="<?=base_url();?>js/jquery.blockUI.js"></script>-->

    

       <script type="text/javascript">
    
    </script>
       <script>
       function changeStatus(id,loadingdiv,spanid)
{
  //alert();
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    if(spanValue=='Inactive')
    {  
       status = '1';
    }
    $.ajax({type:'POST',url: '<?php echo site_url('Generalinfo/change_fare_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       statusfeedback(response,spanid,loadingdiv);
     }});
}
function statusfeedback(retstatus,spanid,loadingdiv) 
{
    //alert(retstatus);
    if(retstatus=='0')
    {
        $('#'+spanid).removeClass('label-success').addClass('label-warning');
        $('#'+spanid).text("Inactive");
    }
    else if(retstatus=='1')
    {

        $('#'+spanid).removeClass('label-warning').addClass('label-success');
        $('#'+spanid).text("Active");
    }
     $('#'+loadingdiv).css('display','none');
    
}
</script>
<script>
  function get_data(value)
  {
    var type_name=value;
     $.ajax({
            type:'POST',
            url: '<?php echo site_url('Generalinfo/Particular_fare');?>',
            data:{"type":type_name},
             beforeSend: function() {
              $("#loading-image").show();
           },
            success: function(response) {
            
              $("#example1").html(response);
              $("#loading-image").hide();
             }});
    }
  
</script>

 