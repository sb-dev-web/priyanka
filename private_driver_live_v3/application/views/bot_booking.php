<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Auto Accept Booking
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">  Auto Accept Booking</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Booking #</th>
                      
                        
                        <th>Pickup Time</th>
                       
                        <th>Pickup Address</th>
                         <th>Cab Type</th>
                               
                        <th>Assgin</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                       
                      if(!empty($bot_booking))
                    
                    
                      {
                        for($i=0;$i<sizeof($bot_booking);$i++)
                        {//echo "<pre>";
                        // print_r($advance_booking[$i]);
                        // exit;
                          $name=$bot_booking[$i]['name'];
                         
                           $d_address=$bot_booking[$i]['destination_location'];
                           
                           $pick_address=$bot_booking[$i]['pickup_location'];
                           if($bot_booking[$i]['driver_id']==0)
                           {
                            $driver_name="Not Yet Assgin";
							$class="label label-warning";
							$lable="Assign";
                            
                           }
                           else
                           {
                            $driver_name=$bot_booking[$i]['first_name'].' '.$bot_booking[$i]['last_name'];
                            $lable="Assigned";
							$class="label label-success" ;
						   }
                        $booking_time=gmt_to_norway($bot_booking[$i]['booking_time']) ;
                        ?>
                      <tr>
                      	
                        <td><a href="javascript:void(0);" onclick="customer_detail('<?php echo $bot_booking[$i]['id'];?>','NORMAL');"><?php echo $bot_booking[$i]['booking_number'];?></a></td>
                       
                        
                        <td><?php  echo gmt_to_norway($bot_booking[$i]['booking_time']);?></td>
                        <td><?php echo $pick_address;?></td>
                                                <td><?php
                         if($bot_booking[$i]['cab_type']=='1')
                         {
                          echo $cab='FIRST Class';
                         }
                         elseif($bot_booking[$i]['cab_type']=='4')
                         {
                          echo $cab='Mini Bus';
                         }
                         else
                         {
                            echo $cab='LUXURY Van';
                         }
                         ?>
                         </td> 
                        <td><a href="javascript:void(0);" onclick="assigndriver('<?php echo $bot_booking[$i]['id'];?>')">
                        <span  class="<?php echo $class ?>"><?php echo $lable ?></span></a>
                                   
                     </td>
                      
                      </tr> 
                      
                      <?php
                      
                        }
                      }
                      else
                      {
                        
                      
                      ?>
                      <tr>
                        <td colspan="10"><center>No data Found</center></td>
                        
                      </tr>
                      <?php
                      }
                      ?>
 
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                 <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
              
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <!-- open customer pop up -->
<div class="modal fade" id="open_customer" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
              Pre Booking Detail
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
               <div id="showreport" name="showreport"></div>
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
       
            
            
<!-- modal for select driver -->



<div class="modal fade" id="open_driver" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
               Assgin Driver
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="text-align: center; margin: 0 auto; width: 80%;">
                <form id="saveDRIVER" action="" method="post" autocomplete="off">
                   
                    <input type="hidden" id="bookingid" name="bookingid"/>
                     <p>
                                <label>Driver</label></label>
                                <span class="formwrapper item">
                                  <select size="1" name="se_driver" id="se_driver"  class="chosen  validate[required]"  style="width:290px;height:30px;"> 
                                      <?php
									  
									  if(sizeof($driver))
									  {
										
										for($i=0;$i<sizeof($driver);$i++)
										{
											$name=$driver[$i]['first_name'].' '.$driver[$i]['last_name'];
									  ?>
									  <option value="<?php  echo $driver[$i]['driver_id'];?>"id="driver_id"><?php echo $name;?></option>
                                      <?php
										}
									  }
									  ?>
                                    </select>
                                </span>
                            </p>
                    <!--<div class="item" style="height: 70px!important;">
                      
                    </div>-->
                    
                    <div style="margin:12px;">
                        <input  class="btn btn-info btn-lg" type="button" value="Assgin" id="a_driver" name="a_driver"/>
                        <span id="loader_note" class="loader" style="display: none;">
                            <img src="http://dev.privatedriverapp.com/app/images/loader19.gif" alt=""/>
                        </span>
                    </div>
                </form>    
                </div>
            </div>
        </div> 
    </div>   


      
      
      
      
      
      
      
      
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
     
      </aside><!-- /.control-sidebar -->
 <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<!--     <script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>-->
<script type="text/javascript" src="<?=base_url();?>js/my_custom.js"></script>


<!-- choosen -->
    
      
       <script>
        /* function for assgin driver */
        function assigndriver(bid) {
         // alert(bid);
         
          $('#bookingid').val(bid);
        
         $('#open_driver').modal({
                 "backdrop" : "static"
         })
             }
        /* assgin drive ron sumbit click */
            $('#a_driver').click(function(){
        var bookingid=$('#bookingid').val();
        var driverid=$('#se_driver').val();
      
         assgindriver(bookingid,driverid);
        });
            
        /* get customer detail */
        function customer_detail(booking_no,type)
        {   
       $.ajax({
            type:'POST',
            url: '<?php echo site_url('Booking/get_advance_booking');?>',
            data:{"booking_no":booking_no,"type":type},
             beforeSend: function() {
              $("#loading-image").show();
           },
            success: function(response) {
            //alert(response);
             $("#showreport").html(response);
             $('#open_customer').modal({
                 "backdrop" : "static"
         })
              $("#loading-image").hide();
             }});
         }
         function assgindriver(bookingid,driverid)
{
    
    var driver_id=driverid;
  
         if(driver_id)
                {
                    $.ajax({
                        type: "POST",
                        url: "Booking/advance_booking",
                        data        :{'driverid':driver_id,booking_id:bookingid,'type':'auto_accept'},
                        beforeSend: function(){
                                
                              },
                        success: function(data){
                            //alert(data);
                            if(data == 1)
                            {
                              
                                setTimeout(function() {
                                    $.bootstrapGrowl("Driver is assgin successfully successfully", {
                                        type: 'success',
                                        align: 'right',
                                        width: 'auto',
                                        allow_dismiss: true
                                    });
                                }, 1000);
                                window.setTimeout(function(){location.reload()},2000);
                            }
                           
                            
                            
                            else
                            {
                             
                                setTimeout(function() {
                                    $.bootstrapGrowl("Sorry Driver is not assgin", {
                                        type: 'danger',
                                        align: 'right',
                                        width: 'auto',
                                        allow_dismiss: true
                                    });
                                }, 1000); 
                            }
                            
                        }
                    });
                }
        
        
   
}
          
          
        
            
         </script>
      
