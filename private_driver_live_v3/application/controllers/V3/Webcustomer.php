<?php

	defined('BASEPATH') or exit('No direct script access allowed');
	// This can be removed if you use __autoload() in config.php OR use Modular
	// Extensions
	require APPPATH . '/libraries/REST_Controller.php';
	//error_reporting(0);
	class Webcustomer extends REST_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> library(array(
				'upload',
				'Braintree_lib',
				'Twilio',
				'email'
			));
			$this -> load -> library('xmpp', false);
			$this -> load -> helper(array(
				'form',
				'url',
				'string',
				'constants_helper',
				'function_helper',
				'braintree_function'
			));

			$this -> load -> model(array(
				'V3/Webcustomer_model',
				'V3/Customer_model'
			));

		}

		/* register user */
		function register()
		{
			/* check that user is already register or not */
			$input_method = $this -> webservices_inputs();
			$auth = getallheaders();
			$apikey = $auth['Apikey'];
			if ($apikey != APIKEY)
			{
				$this -> response(array(
					'message' => AUTHERROR,
					'status' => "0"
				), 200);
			}

			$this -> param_validate($input_method, array(
				"name",
				"email",
				"country_code",
				"phone",
			));
			//check password lenght
			/*if (strlen($input_method['password']) < 6)
			{
				$this -> response(array(
					'message' => PASSWORDLENGHT,
					'status' => "0"
				), 200);
			}*/

			// check user is exist or not

			$user_data = $this -> Customer_model -> check_exist_email_temp($input_method);

			if ($user_data['status'] == 0)
			{
				$this -> response(array(
					'message' => $user_data['message'],
					'status' => "0"
				), 200);
			}
			else
			{

				$name = $input_method['name'];
				$email = $input_method['email'];
				$phone = $input_method['phone'];
				$country_code = str_replace("+", "", $input_method['country_code']);
				$phone_number = "+" . $country_code . $phone;

				$unique_number = random_number();
				$email_code = random_number();
				$input_method['varifycode'] = $unique_number;
				$input_method['email_code'] = $email_code;

				$user_id = $this -> Webcustomer_model -> save_user($input_method);
				if ($user_id)
				{
					$message = VARIFYSMS . $unique_number;
					//send_sms($phone_number, $message);
					AccountVerifyMail($email, $user_id, $email_code);

					$customer = array(

						"customer_id" => "$user_id",
						"name" => $name = $input_method['name'],
						"email" => $email = $input_method['email'],
						"phone" => $phone,
						'country_code' => $country_code,
					);

					$this -> response(array(
						'customer' => $customer,
						'message' => REGISTER_SUCCESS,
						'status' => "1"
					), 200);
				}
				else
				{
					$this -> response(array(
						'message' => SERVER_ERROR,
						'status' => 0
					), 200);
				}
				//exit;

			}
		}

		function login()
		{
			$input_method = $this -> webservices_inputs();
			$auth = getallheaders();
			$apikey = $auth['Apikey'];
			if ($apikey != APIKEY)
			{
				$this -> response(array(
					'message' => AUTHERROR,
					'status' => "0"
				), 200);
			}
			$this -> param_validate($input_method, array(

				"email",
				"password",
			));
			$user_data = $this -> Customer_model -> check_exist_email($input_method);
			if (sizeof($user_data) > 0)
			{

				$user_data = $this -> Customer_model -> check_login($input_method);
				if (sizeof($user_data) > 0)
				{

					// update the user device token
					foreach ($user_data as $login)
					{
						if ($login -> is_active == "0")
						{
							$this -> response(array(
								'message' => USER_NOT_ACTIVE,
								'status' => "0"
							), 200);
						}
						if ($login -> is_company == "Y" && $login -> is_approved == "N")
						{

							$this -> response(array(
								'message' => NOT_APPROVE,
								'status' => "0"
							), 200);
						}

						if ($login -> is_varify == 0)
						{
							$varify_code = $login -> varifycode;
							$phone = "+" . $login -> phone;
							$message = VARIFYSMS . $varify_code;
							send_sms($phone, $message);
							$this -> response(array(
								'message' => MOBILE_NOT_VERIFY,
								'status' => "0"
							), 200);
						}
						if ($login -> is_emailverify == 0)
						{
							AccountVerifyMail($login -> email, $login -> id, $login -> email_code);
							$this -> response(array(
								'message' => EMAIL_NOT_VERIFY,
								'status' => "0"
							), 200);

						}
						$phone = $login -> phone;
						$is_approved = "N";
						$company_id = $company_name = "";
						$user_id = $login -> id;
						//get user company detail
						$company_detail = $this -> Customer_model -> GetCompanyDetail($user_id);
						//print_r($company_detail);
						if ($company_detail)
						{
							$company_name = ($company_detail['company_name']) ? $company_detail['company_name'] : $company_detail['usercompany'];
							$is_approved = $company_detail['is_approved'];
							$company_id = $company_detail['company_id'];
						}
						if ($company_id && $login -> credit_card_no)
						{
							$user_type = "B";
						}
						elseif ($company_id)
						{
							$user_type = "C";
						}
						elseif ($login -> credit_card_no)
						{
							$user_type = "I";
						}
						else
						{
							$user_type = "I";
							//for company user
						}
						$customer = array(
							//"customer_jid" => makejid($login -> id, USER_JID_NAME),
							//"customer_id" => $userid = $login -> id,
							"name" => $name = $login -> name,
							"email" => $email = $login -> email,
							"dob" => $dob = $login -> dob,
							"phone" => $phone,
							"country_code" => $login -> country_code,
							"credit_card" => ($login->credit_card_no=="") ? "" : $login->credit_card_no,
							"user_type" => $user_type,
							"card_type"=>($login->card_type=="") ? "" : $login->card_type,
							"braintree_customer_id"=>($login->braintree_customer_id=="") ? "" : $login->braintree_customer_id,
							"auth_token" => $login -> auth_token
						);

					}
					//$this->Customer_model->update_device($input_method);

					$this -> response(array(
						'customer' => $customer,
						'message' => SUCCESS,
						'status' => "1"
					), 200);
				}
				else
				{
					$this -> response(array(
						'message' => CUSTOMER_LOGIN_ERROR,
						'status' => "0"
					), 200);
				}

			}
			else
			{
				$this -> response(array(
					'message' => EMAIL_NOT_EXIST,
					'status' => "0"
				), 200);
			}
		}

		function AddCreditCrad()
		{
			$input_method = $this -> webservices_inputs();
			$auth = getallheaders();
			$apikey = $auth['Apikey'];
			if ($apikey != APIKEY)
			{
				$this -> response(array(
					'message' => AUTHERROR,
					'status' => "0"
				), 200);
			}
			$this -> param_validate($input_method, array(
				"auth_token",
				"nonce",
			));
			$UserData = $this -> Webcustomer_model -> GetUser($input_method['auth_token']);
			if (!$UserData)
			{
				$this -> response(array(
					'message' => USER_NOT_ACTIVE,
					'status' => "0"
				), 200);
			}
			$CC = $this -> Customer_model -> CheckCCDetail($UserData['id']);
			if ($CC)
			{
				$this -> response(array(
					'message' => CC_ALREADY_ADD,
					'status' => "0"
				), 200);
			}
			else
			{
				//get user detail

				$data = array(
					"name" => $UserData['name'],
					"email" => $UserData['email'],
					"phone" => $UserData['country_code'] . $UserData['phone'],
					"nonce" => $input_method['nonce'],
				);

				//$braintree_response = Create_withCreditCardAndVerification($data);
				$braintree_response = CreateCustomer($data);

				if ($braintree_response['status'] == "1")
				{

					//save CC detail
					$braintree_response['user_id'] = $UserData['id'];
					$bt_customer_id = $braintree_response['customer_id'];
					$bt_customer_token = $braintree_response['token'];
					$card_type = $braintree_response['cardtype'];
					$SaveCC = $this -> Customer_model -> SaveCCDetail($braintree_response);
					if ($SaveCC)
					{
						$this -> response(array(
							'message' => SUCCESS,
							"credit_card" => credit_card_number($braintree_response['last4']),
							"braintree_customer_id" => $bt_customer_id,
							"card_type" => $card_type,
							"braintree_token" => $bt_customer_token,
							'status' => "1"
						), 200);
					}
					else
					{
						$this -> response(array(
							'message' => SERVER_ERROR,
							'status' => "0"
						), 200);
					}
				}
				else
				{
					$this -> response(array(
						'message' => $braintree_response['message'],
						'status' => "0"
					), 200);
				}
			}

		}

     function ForgetPassword()
		{
			$input_method = $this -> webservices_inputs();
			$auth = getallheaders();
			$apikey = $auth['Apikey'];
			if ($apikey != APIKEY)
			{
				$this -> response(array(
					'message' => AUTHERROR,
					'status' => "0"
				), 200);
			}
			$this -> param_validate($input_method, array(
				"email"
				
			));
			$UserData = $this -> Webcustomer_model -> GetUserFromEmail($input_method['email']);
			if (!$UserData)
			{
				$this -> response(array(
					'message' => USER_NOT_ACTIVE,
					'status' => "0"
				), 200);
			}
			$mail_sent = SendverificationCode($UserData['email'], $UserData['id']);
					if ($mail_sent)
					{
						$this -> response(array(
							'message' => FORGET_PASSWORD,
							'status' => 1
						), 200);
					}
					else
					{
						$this -> response(array(
							'message' => SERVER_ERROR,
							'status' => 0
						), 200);
					}
			
			
		}

	}
