<?php

class Activity_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    /* driver daliy basis report */

    function driver_reports($start, $limit)
    {
        $fetch_driver_report = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id limit  $limit,$start");
        // echo $this->db->last_query();
        $count = $fetch_driver_report->num_rows();
        if ($count > 0)
        {
            return $fetch_driver_report->result();
        }
        else
        {
            return false;
        }
    }
    /* conunting rows */

    function count_all()
    {
        $fetch_driver_report = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id ");
        $count = $fetch_driver_report->num_rows();
        return $count;
    }

    /* count according booking */
    function count_according_booking($bdate)
    {
        $fetch_driver_breport = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id where date(cab_booking.booking_time)='$bdate'");
        // echo $this->db->last_query();
        $count = $fetch_driver_breport->num_rows();
        return $count;
    }

    /* drive report according to date */
    function driver_bdate_reports($start, $limit, $bdate)
    {
        $fetch_driver_report = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id  where date(cab_booking.booking_time)='$bdate' limit  $limit,$start");
        // echo $this->db->last_query();
        $count = $fetch_driver_report->num_rows();
        if ($count > 0)
        {
            return $fetch_driver_report->result();
        }
        else
        {
            return false;
        }
    }

    /* funtion to count driver */
    function count_driver($did)
    {
        $fetch_driver = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id where cab_booking.driver_id='$did'");
        // echo $this->db->last_query();
        $count = $fetch_driver->num_rows();
        return $count;
    }

    /* fetch result according driver */
    function driver_booking($start, $limit, $did)
    {
        $fetch_driver_result = $this->db->query("select driver.first_name,driver.last_name,cab_booking.booking_number,cab_booking.booking_time,cab_booking.status,cab_booking.is_prebooking from cab_booking LEFT join  driver  on driver.driver_id=cab_booking.driver_id  where cab_booking.driver_id='$did' limit  $limit,$start");
        // echo $this->db->last_query();
        $count = $fetch_driver_result->num_rows();
        if ($count > 0)
        {
            return $fetch_driver_result->result();
        }
        else
        {
            return false;
        }
    }

    /* function fetch driver daliy report */
    function Daily_report($start, $limit, $booking_date)
    {
        if ($booking_date > 0)
        {
            $fetch_report_data = $this->db->query("select d.driver_id,d.first_name,d.last_name,count(case when c.status = 'TRIP_ENDED' then 1 else null end) as tripend,
            (case when c.status='DRIVER_ACCEPTED' then 1 else null end) as driveraccept,
            (case when c.status='CANCLLED' then 1 else null end) as cancelled,
            (case when c.status='ADVANCE_BOOKING' then 1 else null end) as advancebooking
            
 from cab_booking as c,driver as d where c.driver_id=d.driver_id and date(c.booking_time)='$booking_date' limit  $limit,$start");
        }
        else
        {
            $fetch_report_data = $this->db->query("select d.driver_id,d.first_name,d.last_name,count(case when c.status = 'TRIP_ENDED' then 1 else null end) as tripend,
            (case when c.status='DRIVER_ACCEPTED' then 1 else null end) as driveraccept,
            (case when c.status='CANCLLED' then 1 else null end) as cancelled,
            (case when c.status='ADVANCE_BOOKING' then 1 else null end) as advancebooking
            
 from cab_booking as c,driver as d where c.driver_id=d.driver_id  limit  $limit,$start");
        }
        $count_data = $fetch_report_data->num_rows();
        if ($count_data > 0)
        {
            return $fetch_report_data->result();
        }
        else
        {
            return false;
        }
    }

    /* function for count total driver */
    function count_drivers($booking_date)
    {
        if ($booking_date > 0)
        {
            $total_driver = $this->db->query("select d.driver_id,d.first_name,d.last_name,count(case when c.status = 'TRIP_ENDED' then 1 else null end) as tripend,
            (case when c.status='DRIVER_ACCEPTED' then 1 else null end) as driveraccept,
            (case when c.status='CANCLLED' then 1 else null end) as cancelled,
            (case when c.status='ADVANCE_BOOKING' then 1 else null end) as advancebooking
 from cab_booking as c,driver as d where c.driver_id=d.driver_id and date(c.booking_time)='$booking_date'");
        }
        else
        {
            $total_driver = $this->db->query("select d.driver_id,d.first_name,d.last_name,count(case when c.status = 'TRIP_ENDED' then 1 else null end) as tripend,
            (case when c.status='DRIVER_ACCEPTED' then 1 else null end) as driveraccept,
            (case when c.status='CANCLLED' then 1 else null end) as cancelled,
            (case when c.status='ADVANCE_BOOKING' then 1 else null end) as advancebooking
 from cab_booking as c,driver as d where c.driver_id=d.driver_id");
        }
        $total_count = $total_driver->num_rows();
        return $total_count;
    }

    /* count log */
    function count_all_log($date, $api_name)
    {

        if ($api_name != "" && $date > 0)
        {
            $condition = "where date(createdon)='$date'and uri='$api_name'";
        }
        else
            if ($api_name != "")
            {
                $condition = "where uri='$api_name'";
            }
            else
                if ($date > 0)
                {
                    $condition = "where date(createdon)='$date'";

                }
                else
                {
                    $condition = "";
                }
                $total_log = $this->db->query("select * from logs $condition");
        //echo $this->db->last_query();
        $total_count = $total_log->num_rows();
        return $total_count;
    }

    /* fetch log */
    function log($start, $limit, $date, $api_name)
    {

        if ($api_name != "" && $date > 0)
        {
            $condition = "where date(createdon)='$date'and uri='$api_name'";
        }
        else
            if ($api_name != "")
            {
                $condition = "where uri='$api_name'";
            }
            else
                if ($date > 0)
                {
                    $condition = "where date(createdon)='$date'";

                }
                else
                {
                    $condition = "";
                }
                $total_log = $this->db->query("select * from logs $condition limit $limit,$start");
        //echo $this->db->last_query();
        return $total_log->result();


    }
    /* function for know driver report*/
    function get_driver_report($start_date,$end_date,$offset,$limit)
    {
      
        $result = $this->db->query("SELECT driver.*,count(cab_booking.id),sum(payment_detail.billing_amount) as total_income, sum( payment_detail.tip ) AS tips FROM `driver` left join cab_booking on driver.driver_id=cab_booking.driver_id left join payment_detail on cab_booking.id=payment_detail.booking_id and  date(cab_booking.booking_time) BETWEEN '$start_date' AND '$end_date' GROUP BY driver.driver_id limit $offset,$limit");
      //echo $this->db->last_query();
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }
    }

    /* count driver report */
     
    function count_driver_report($start_date,$end_date)
    {
       
       
        $result = $this->db->query("SELECT driver.*,count(cab_booking.id),sum(payment_detail.billing_amount) as total_income, sum( payment_detail.tip ) AS tips FROM `driver` left join cab_booking on driver.driver_id=cab_booking.driver_id left join payment_detail on cab_booking.id=payment_detail.booking_id and  date(cab_booking.booking_time) BETWEEN $start_date AND $end_date GROUP BY driver.driver_id");
        // echo $this->db->last_query();
        $total_count = $result->num_rows();
        if ($result->num_rows() > 0)
        {
            return $total_count;
        }
        else
        {
            return $total_count;
        }
    }
    
    
    // count how many booking according status
    function count_booking_report($did, $status)
    {

        if ($status == 'total_recieve')
        {
            $total_driver = $this->db->query("select * FROM cab_booking_history where driver_id='$did'");
              //echo $this->db->last_query();
        }
        else
            if ($status == 'DRIVER_ACCEPTED')
            {

                $total_driver = $this->db->query("SELECT booking_id,driver_datetime,driver_response from cab_booking_history where driver_response='$status' and driver_id='$did'");
                //echo $this->db->last_query();
            }
            else
                if ($status == 'DRIVER_REJECTED')
                {
                    $total_driver = $this->db->query("SELECT booking_id,driver_datetime,driver_response from cab_booking_history where driver_response='$status' and driver_id='$did'");
                    // echo $this->db->last_query();
                }
                else
                {
                    return false;
                }
                $total_count = $total_driver->num_rows();
        return $total_count;
    }

    /* total driver report */
    function driver_booking_report($did, $status, $offset, $limit,$date)
    {
    	$date=str_replace("to","' and '" , $date);
        if ($status == 'total_recieve')
        {
        	//echo $date;
			$date=str_replace("to","and" , $date);
			
        	$query ="select b.booking_id,b.driver_datetime,b.driver_response,d.first_name,d.last_name,driver_rating.rating 
        	FROM cab_booking_history as b left join driver as d on d.driver_id=b.driver_id
        	 left join driver_rating on driver_rating.booking_id=b.booking_id
        	  where b.driver_id=$did and 
        	d.driver_id=b.driver_id  
        	order by b.id limit $offset,$limit";
            $total_driver = $this->db->query($query);
           // echo $this->db->last_query();and date(driver_datetime) BETWEEN '$date'
        }
        else
            if ($status == 'DRIVER_ACCEPTED')
            {
                $query="SELECT b.booking_id,b.driver_datetime,b.driver_response,d.first_name,d.last_name,driver_rating.rating 
                from cab_booking_history as b 
                left join driver as d on d.driver_id=b.driver_id 
                left join driver_rating on driver_rating.booking_id=b.booking_id 
                where b.driver_response='DRIVER_ACCEPTED' and b.driver_id=$did and 
                d.driver_id=b.driver_id 
                order by b.id  limit $offset,$limit";
				$total_driver=$this->db->query($query);
               // $total_driver = $this->db->query("SELECT b.booking_id,b.driver_datetime,b.driver_response,d.first_name,d.last_name from cab_booking_history as b,driver as d where b.driver_response='$status' and b.driver_id='$did' and d.driver_id=b.driver_id limit $offset,$limit");
               // echo $this->db->last_query();  and date(driver_datetime) BETWEEN '$date' 
            }
            else
                if ($status == 'DRIVER_REJECTED')
                {
                    $total_driver = $this->db->query("SELECT b.booking_id,b.driver_datetime,b.driver_response,d.first_name,d.last_name from cab_booking_history as b,driver as d where b.driver_response='$status' and b.driver_id='$did' and d.driver_id=b.driver_id and date(driver_datetime) BETWEEN '$date'  limit $offset,$limit");
                  // echo   $this->db->last_query();
                    
                }
                else
                {
                    return false;
                }
                //echo $this->db->last_query();
                $total_count = $total_driver->num_rows();
        if ($total_count > 0)
        {
            return $total_driver->result_array();
        }
        else
        {
            return false;
        }
    }
    
    
/* function for counting shift */
function count_shift_report($start_date,$end_date,$driver_id)
{
    
        $result = $this->db->query("select d.first_name,d.last_name,s.status,s.createdon,s.endon from driver as d,shift as s where s.driver_id=d.driver_id and s.driver_id='$driver_id' and date(s.createdon) BETWEEN '$start_date' and '$end_date'");
        $total_count = $result->num_rows();
         if ($total_count > 0)
        {
            return $total_count;
        }
        else
        {
            return 0;
        }
    }





/* function for counting break */
function count_break_report($start_date,$end_date,$driver_id)
{
    
        $result = $this->db->query("select d.first_name,d.last_name,b.break_time,b.type,c.cab_plate_no FROM driver as d,break as b,shift as s,cab as c where b.driver_id=d.driver_id and b.shift_id=s.s_id and s.cab_id=c.cab_id and b.driver_id='$driver_id' and date(b.break_time) BETWEEN '$start_date' AND '$end_date'");
        $total_count = $result->num_rows();
         if ($total_count > 0)
        {
            return $total_count;
        }
        else
        {
            return 0;
        }
    }

/* fetch all break between date */
function driver_break_report($start_date,$end_date,$driver_id,$offset, $limit)
{
  
        $result = $this->db->query("select d.first_name,d.last_name,b.break_time,b.type,c.cab_plate_no FROM driver as d,break as b,shift as s,cab as c where b.driver_id=d.driver_id and b.shift_id=s.s_id and s.cab_id=c.cab_id and b.driver_id='$driver_id' and date(b.break_time) BETWEEN '$start_date' AND '$end_date' limit $offset,$limit");
       // echo $this->db->last_query();
        $total_count = $result->num_rows();
        if ($total_count > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }  
}

/*function for getting shift */
function get_shift($start_date,$end_date,$cab_id,$offset,$limit)
{
    $get_shift_data=$this->db->query("select s.s_id,s.cab_id,d.first_name,d.last_name,c.cab_model,c.cab_plate_no,s.createdon from cab as c,driver as d,shift as s  where date(createdon) between '$start_date' and '$end_date' and s.cab_id='$cab_id' and s.cab_id=c.cab_id and s.driver_id=d.driver_id  limit $offset,$limit");
   //echo $this->db->last_query();
    $total_count=$get_shift_data->num_rows();
    if($total_count>0)
    {
        return $get_shift_data->result_array();
    }
    else
    {
        return false;
    }
 
}

/*count shift image report */
function count_shift_image_report($start_date,$end_date,$cab_id)
{
    $get_shift_count=$this->db->query("select s.s_id,s.cab_id,d.first_name,d.last_name,c.cab_model,c.cab_plate_no,s.createdon from cab as c,driver as d,shift as s  where date(createdon) between '$start_date' and '$end_date' and s.cab_id='$cab_id' and s.cab_id=c.cab_id and s.driver_id=d.driver_id");
   //echo $this->db->last_query();
    $total_count=$get_shift_count->num_rows();
    if($total_count>0)
    {
        return $total_count;
    }
    else
    {
        return 0;
    }
  
}



/*get shift image */
function get_shift_image($shift_id)
{
   $get_image=$this->db->query("select cab_image from cab_image WHERE shift_id='$shift_id'");
   //echo $this->db->last_query();
   $total_image=$get_image->num_rows();
   if($total_image>0)
   {
    return $get_image->result_array();
   }
   else
   {
    return false;
   }
}

function get_cab()
{
 $get_cab=$this->db->query("select * from cab where isdelete='0'");
    $total_cab=$get_cab->num_rows();
   if($total_cab>0)
   {
    return $get_cab->result_array();
   }
   else
   {
    return false;
   }   
}


/* function for couint tips*/
function Count_Tips_report($date)
{
    
        $result = $this->db->query("select sum(p.tip),p.createdon,c.driver_id,d.first_name,d.last_name FROM payment_detail as p,cab_booking as c,driver as d where c.id=p.booking_id and d.driver_id=c.driver_id and date(p.createdon)='$date' GROUP by c.driver_id");
        $total_count = $result->num_rows();
         if ($total_count > 0)
        {
            return $total_count;
        }
        else
        {
            return 0;
        }
}


/*function for getting tips */
function Get_tips($date,$offset,$limit)
{
    $result = $this->db->query("select sum(p.tip)as tips,d.driver_id,date(p.createdon) as createdon,c.driver_id,d.first_name,d.last_name FROM payment_detail as p,cab_booking as c,driver as d where c.id=p.booking_id and d.driver_id=c.driver_id and date(p.createdon)='$date' GROUP by c.driver_id limit $offset,$limit");
        $total_tips = $result->num_rows();
         if ($total_tips> 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }
}


/*get booking according tips */
function Get_tips_booking($driver_id,$date)
{
   $fetch_booking_tips=$this->db->query("select c.booking_number,p.tip,date(p.createdon) as date,cu.name from cab_booking as c,payment_detail as p,user as cu  where c.id=p.booking_id and date(p.createdon)='$date' and c.driver_id='$driver_id' and p.tip>0 and cu.id=c.user_id"); 
   $total_fetch_tips=$fetch_booking_tips->num_rows();
   if($total_fetch_tips>0)
   {
    return $fetch_booking_tips->result_array();
   }
   else
   {
    return false;
   }
}

 /* fetch all shift between date */
    function driver_shift_report($start_date, $end_date, $driver_id)
    {

        /*$result = $this->db->query("select d.first_name,d.last_name,s.status,s.createdon,s.endon from driver as d,shift as s where s.driver_id=d.driver_id and s.driver_id='$driver_id' and date(s.createdon) BETWEEN '$start_date' and '$end_date' limit $offset,$limit");
        //echo $this->db->last_query();
        $total_count = $result->num_rows();
        if ($total_count > 0)
        {
        return $result->result_array();
        }
        else
        {
        return false;
        }  */
        $where_condtion = array(
            "driver_id" => $driver_id,
            "createdon>=" => $start_date,
            "createdon<=" => $end_date);
        $this->db->select("*");
        $this->db->from("shift");
        $this->db->join("cab","cab.cab_id=shift.cab_id");
        $this->db->join("cab_type","cab_type.type_id=cab.cab_type");
        $this->db->where("driver_id",$driver_id);
        $this->db->where("createdon>=",$start_date);
        $this->db->where("createdon<=",$end_date);
        $query=$this->db->get();
        //$query = $this->db->get_where("shift", $where_condtion);
        //echo $this->db->last_query();
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }else
        {
            return false;
        }
    }
   
    
    
    
    
    
    
    
    /*driver daily report functions */
    function working_time($starton,$endon,$driver_id)
	{
		//$query=$this->db->query("SELECT `s_id`, `createdon`, `endon`, date( createdon ) start_date, date( endon ) end_date,driver_id FROM `shift` WHERE s_id=224 ");
	    $query=$this->db->query("SELECT `s_id`, `createdon`, `endon`, date( createdon ) start_date, date( endon ) end_date,driver_id FROM `shift` WHERE date(createdon) between '$starton' and '$endon' and date(endon)!='0000-00-00' and driver_id='$driver_id'");
		//echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}else
		{
			return false;
		}
		 
	}  
	function shift_break_detail($shift_id,$date)
	{
		$query=$this->db->get_where("break",array("shift_id"=>$shift_id,"date(break_time)"=>$date));
		//echo $this->db->last_query();
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}else
			{
				return false;
			}
	}
	function getreport_data($driver_id,$date)
	{
		$query=$this->db->query("SELECT count(b.id) booking_count,d.first_name,d.last_name,sum(user_paid) payment ,sum(p.tip) tip,avg(rating) rating FROM `cab_booking` b left join payment_detail p on p.booking_id=b.id
left join driver_rating r on r.booking_id=b.id
join driver d on d.driver_id=b.driver_id
WHERE b.driver_id=$driver_id and date(b.createdon)='".$date."'");
//echo $this->db->last_query();
         if($query->num_rows()>0)
         {
         	return $query->result_array();
         }
	}
	function saveworkingtime($working_deatil)
	{
		/*
		 * [start_date] => 2015-11-30 14:18:26
            [end_date] => 2015-12-01
            [shift_hr] => 34894
            [shift_hr_format] => 9 HR 41 Min 34 Sec 
            [break_time] => 4019
            [break_time_formate] => 1 HR 6 Min 59 Sec 
            [working_time] => 8 HR 34 Min 35 Sec 
		 * */
		 foreach ($working_deatil as $working_hr) {
		 	
			$data=array("shift_id"=>$working_hr['shift_id'],
						"driver_id"=>$working_hr['driver_id'],
						"working_date"=>$working_hr['date'],
						"total_working_time"=>$working_hr['shift_hr'],
						"net_working_time"=>$working_hr['working_time'],
						"net_working_time_format"=>$working_hr['working_time_format'],
						"createdon"=>get_gmt_time()
			);
			$workin_hr_id=$this->db->insert("working_detail",$data);
			 
		 }
		 if($workin_hr_id)
		 {
		 	return TRUE;
		 }else
		 	{
		 		return false;
		 	}
	}
    
    /* get all driver report */
     /* function for know driver report*/
    function get_all_driver_report($booking_date, $driver_id)
    {
        $date = explode('to', $booking_date);
        $start_date = $date[0];
        $end_date = $date[1];
        $result = $this->db->query("select driver_id,SUM(case when driver_id='$driver_id' then 1 else null end) as totalbooking,
SUM(case when driver_response='DRIVER_REJECTED' then 1 else null end) as driverreject,
SUM(case when driver_response='DRIVER_ACCEPTED' then 1 else null end) as totalaccept        
 from cab_booking_history  where driver_id='$driver_id'and date(driver_datetime) BETWEEN '$start_date' AND '$end_date'");
     echo $this->db->last_query();
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }
    }
    
    
    
    /* get turv over report */
    function count_turnover_report($s_date,$e_date)
    {
        $data=$this->db->query("select SUM(billing_amount),DATE_FORMAT(createdon,'%Y-%m') monthyr from payment_detail where DATE_FORMAT(createdon,'%Y-%m') BETWEEN '$s_date' and '$e_date' group by DATE_FORMAT(createdon,'%Y-%m')");
       $count=$data->num_rows();
         if ($count > 0)
        {
            return $count;
        }
        else
        {
            return 0;
        }
    }
    
    function get_turnover_report($s_date,$e_date)
    {
         $data=$this->db->query("select SUM(billing_amount) as totalbill,DATE_FORMAT(payment_detail.createdon,'%Y-%m') monthyr from payment_detail join cab_booking on cab_booking.id=payment_detail.booking_id  where DATE_FORMAT(payment_detail.createdon,'%Y-%m') BETWEEN '$s_date' and '$e_date' and cab_booking.payment_mode='BT' group by DATE_FORMAT(payment_detail.createdon,'%Y-%m')");
         //echo $this->db->last_query();
      
       $count=$data->num_rows();
          if ($count > 0)
        {
            return $data->result_array();
        }
        else
        {
            return array();
        }
    }
    
    
    
    function shift_break_detail_mon($start_date, $end_date, $driver_id)
	{
		//$query=$this->db->get_where("break",array("shift_id"=>$shift_id,"date(break_time)"=>$date));
		$query = $this -> db -> query("SELECT *
FROM `break`
WHERE `driver_id` = '" . $driver_id . "' and date(break_time) BETWEEN  '" . $start_date . "' and '" . $end_date . "'");
		//echo $this -> db -> last_query();
		if ($query -> num_rows() > 0)
		{
			return $query -> result_array();
		}
		else
		{
			return false;
		}
	}
    
    
    function working_time_mon($starton, $endon, $driver_id)
	{
		//$query=$this->db->query("SELECT `s_id`, `createdon`, `endon`, date( createdon )
		// start_date, date( endon ) end_date,driver_id FROM `shift` WHERE s_id=224 ");
		//   $query=$this->db->query("SELECT `s_id`, `createdon`, `endon`, date(
		// createdon ) start_date, date( endon ) end_date,driver_id FROM `shift` WHERE
		// date(createdon) between '$starton' and '$endon' and date(endon)!='0000-00-00'
		// ");//and driver_id in (44,43)
		$query = $this -> db -> query("SELECT `s_id`, `createdon`, `endon`, date( createdon ) start_date, date( endon ) end_date,driver_id,TIMESTAMPDIFF(SECOND, createdon, endon) as sectime FROM `shift`  WHERE date(createdon) between '$starton' and  '$endon' and date(endon)!='0000-00-00'  and driver_id=$driver_id ");
      
		if ($query -> num_rows() > 0)
		{
			return $query -> result_array();
		}
		else
		{
			return false;
		}

	}
    
    function get_other_detail($start_date, $end_date, $driver_id)
	{
		$query = $this -> db -> query("SELECT driver.first_name,driver.last_name, count(cab_booking.id) as booking ,sum(payment_detail.billing_amount) as total_income, sum( payment_detail.tip ) AS tips ,COUNT(cab_booking.id) as acceptbooking
FROM `driver`
left join cab_booking on driver.driver_id=cab_booking.driver_id 
left join payment_detail on cab_booking.id=payment_detail.booking_id and  date(cab_booking.booking_time) 

where date(cab_booking.createdon) BETWEEN '$start_date' and '$end_date' and driver.driver_id=$driver_id");
	//left join cab_booking_history bh on bh.driver_id=driver.driver_id and driver_response='DRIVER_ACCEPTED' and bh.booking_id=cab_booking.idecho 	$this->db->last_query();
		if ($query -> num_rows() > 0)
		{
				return $query->result_array();
		}else{
			return false;
		}
	}
    
    function active_driver()
	{
		$query=$this->db->query("SELECT driver_id FROM `driver` where isdelete='0'");
		if($query->num_rows()>0)
		{
			return $query->result_array();
			
		}else
			{
				return false;
			}
	}
    
    
    function total_break($start_date,$end_date,$driver_id,$offset, $limit)
{
  
        $result = $this->db->query("select d.first_name,d.last_name,b.break_time,b.type,c.cab_plate_no FROM driver as d,break as b,shift as s,cab as c where b.driver_id=d.driver_id and b.shift_id=s.s_id and s.cab_id=c.cab_id and b.driver_id='$driver_id' and date(b.break_time) BETWEEN '$start_date' AND '$end_date'");
        //echo $this->db->last_query();
        $total_count = $result->num_rows();
        if ($total_count > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }  
}
}
