<?php

	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Activity_2 extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url',
				'function'
			));
			$this -> load -> model('Activity_2_model');
			$this -> load -> model('Driver_model');
			//$this -> load -> library('Ajax_pagination');
			//->load->library(array('session', 'pagination'));
			$this -> load -> library(array(
				'session',
				'email',
				'paginationlib',
				'Pagination'
			));
			$user = $this -> session -> userdata('logged_in');
			if (!$user)
			{
				redirect('Welcome');
			}

		}

		/* driver booking according to date */

		public function Booking_date()
		{
			$booking_date = $_POST['date'];
			$driver_id = $_POST['driverid'];
			$data['driver_report'] = $this -> Activity_2_model -> get_driver_report($booking_date, $driver_id);

			$this -> load -> view('driver_report', $data);

		}

		/* booking result according driver */
		public function booking_accordingdriver()
		{
			$did = $_POST['did'];
			$config['base_url'] = site_url('Activity/driver_activity');
			$config['total_rows'] = $this -> Activity_2_model -> count_driver($did);
			$config['per_page'] = "25";
			$config["uri_segment"] = 3;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = floor($choice);
			//config for bootstrap pagination class integration
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$this -> pagination -> initialize($config);
			$data['page'] = ($this -> uri -> segment(3)) ? $this -> uri -> segment(3) : 0;

			//call the model function to get the department data
			$data['deptlist'] = $this -> Activity_2_model -> driver_booking($config["per_page"], $data['page'], $did);
			//print_r($data);

			$data['pagination'] = $this -> pagination -> create_links();
			//echo $data['pagination'];

			//load the department_view
			$this -> load -> view('driver_booking', $data);

		}

		/* fetch daily basisi total count report accept,decline */

		function booking_activity()
		{
			$booking_date = '0';
			$config['base_url'] = site_url('Activity/booking_activity');
			$config['total_rows'] = $this -> Activity_2_model -> count_drivers($booking_date);
			$config['per_page'] = "25";
			$config["uri_segment"] = 3;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = floor($choice);

			//config for bootstrap pagination class integration
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$this -> pagination -> initialize($config);
			$data['page'] = ($this -> uri -> segment(3)) ? $this -> uri -> segment(3) : 0;

			//call the model function to get the department data
			$data['booklist'] = $this -> Activity_2_model -> Daily_report($config["per_page"], $data['page'], $booking_date);
			//print_r($data);

			$data['pagination'] = $this -> pagination -> create_links();

			//load the department_view
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('daily_activity');
			$this -> load -> view('site-nav/footer');

		}

		/* functin for drive daily report accoording date */
		function driver_booking_date()
		{

			$booking_date = $_POST['booking_date'];
			$config['base_url'] = site_url('Activity/daily_activity');

			$config['total_rows'] = $this -> Activity_2_model -> count_drivers($booking_date);

			$config['per_page'] = "25";
			$config["uri_segment"] = 3;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = floor($choice);

			//config for bootstrap pagination class integration
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$this -> pagination -> initialize($config);
			$data['page'] = ($this -> uri -> segment(3)) ? $this -> uri -> segment(3) : 0;

			//call the model function to get the department data
			$data['booklist'] = $this -> Activity_2_model -> Daily_report($config["per_page"], $data['page'], $booking_date);
			//print_r($data);

			$data['pagination'] = $this -> pagination -> create_links();
			//echo $data['pagination'];

			//load the department_view
			$this -> load -> view('driver_booking_date', $data);

		}

		/* checking log */

		public function log()
		{
			//pagination settings
			$date = "";
			$api_name = "";
			$config['base_url'] = site_url('Activity/log');
			$config['total_rows'] = $this -> Activity_2_model -> count_all_log($date, $api_name);
			$config['per_page'] = 500;
			$config["uri_segment"] = 3;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = floor($choice);

			//config for bootstrap pagination class integration
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$this -> pagination -> initialize($config);
			$data['page'] = ($this -> uri -> segment(3)) ? $this -> uri -> segment(3) : 0;

			//call the model function to get the department data
			$data['loglist'] = $this -> Activity_2_model -> log($config["per_page"], $data['page'], $date, $api_name);
			//print_r($data);

			$data['pagination'] = $this -> pagination -> create_links();

			//load the department_view
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('log_activity');
			$this -> load -> view('site-nav/footer');
			// $this->load->view('',$data);
		}

		/* fetch log according filter */
		/* functin for drive daily report accoording date */
		function log_activity()
		{

			$booking_date = $_POST['date'];
			$api_name = $_POST['api_name'];
			//echo $api_name;
			$config['base_url'] = site_url('Activity/log_activity');

			$config['total_rows'] = $this -> Activity_2_model -> count_all_log($booking_date, $api_name);

			$config['per_page'] = "25";
			$config["uri_segment"] = 3;
			$choice = $config["total_rows"] / $config["per_page"];
			$config["num_links"] = floor($choice);

			//config for bootstrap pagination class integration
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';

			$this -> pagination -> initialize($config);
			$data['page'] = ($this -> uri -> segment(3)) ? $this -> uri -> segment(3) : 0;

			//call the model function to get the department data
			$data['loglist'] = $this -> Activity_2_model -> log($config["per_page"], $data['page'], $booking_date, $api_name);
			//print_r($data);

			$data['pagination'] = $this -> pagination -> create_links();
			//echo $data['pagination'];

			//load the department_view
			$this -> load -> view('daliy_log_activity', $data);

		}

		/* fetching data according driver id and type of status*/
		function booking_data($did, $status)
		{

			$extra_param = array(
				"driver_id" => $did,
				"status" => $status
			);
			$count = $this -> Activity_2_model -> count_booking_report($did, $status);
			$pagingconfig = $this -> paginationlib -> initPagination("Activity/booking_data_ajax", $count, 20, 1, $extra_param);
			// $config['cur_tag_open'] = '<li class="active"><a href="#">';
			$this -> ajax_pagination -> initialize($pagingconfig);
			//$extra_param=json(array("driver_id"=>$did,"status"=>$status));
			$data['link'] = $this -> ajax_pagination -> create_links();
			$limit = 20;
			$offset = 0;
			$data['total_report'] = $this -> Activity_2_model -> driver_booking_report($did, $status, $offset, $limit);
			$data['active_page'] = "activity";
			$this -> load -> view('total_booking_report', $data);
		}

		function booking_data_ajax()
		{
			$postdata = $this -> input -> post('data');

			$post = json_decode($postdata, true);

			$did = $post['driver_id'];
			$status = $post['status'];
			$count = $this -> Activity_2_model -> count_booking_report($did, $status);
			$extra_param = array(
				"driver_id" => $post['driver_id'],
				"status" => $post['status']
			);
			$pagingconfig = $this -> paginationlib -> initPagination("Activity/booking_data_ajax", $count, 20, $post['page'], $extra_param);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			//get the posts data
			$limit = $pagingconfig['per_page'];
			if (!$post['page'])
			{
				$offset = 0;
			}
			else
			{
				$offset = ($post['page'] - 1) * $limit;
				$offset = $post['page'];
			}
			$data['total_report'] = $this -> Activity_2_model -> driver_booking_report($did, $status, $offset, $limit);
			$data['active_page'] = "activity";

			//load the view
			$this -> load -> view('total_booking_report', $data, false);
		}

		function booking_report()
		{
			if ((isset($_POST['driverid'])) && ($_POST['status_type']))
			{

				$driver_id = $_POST['driverid'];
				$status = $_POST['status_type'];
				$this -> booking_data($driver_id, $status);
			}
			else
			{
				$data['fetch_driver'] = $this -> Driver_model -> get_driver_name();
				$data['active_page'] = "activity";
				$this -> load -> view('site-nav/header');
				$this -> load -> view('driver_activity', $data);
				$this -> load -> view('site-nav/footer');
			}
		}

		function DriverTimeline()
		{
			if ((isset($_POST['date'])) && ($_POST['driverid']))
			{
				$date = $_POST['date'];
				$dates = explode('to', $date);
				$start_date = $dates[0];
				$end_date = $dates[1];
				$driver_id = $_POST['driverid'];
				$this -> drivertimeline_data($start_date, $end_date, $driver_id);
			}
			else
			{
				$data['fetch_driver'] = $this -> Driver_model -> get_driver_name();
				$this -> load -> view('site-nav/header');
				$this -> load -> view('shift_report', $data);
				$this -> load -> view('site-nav/footer');
			}
		}

		function drivertimeline_data($start_date, $end_date, $driver_id)
		{
			$shift_detail = $this -> Activity_2_model -> drivertime_report($start_date, $end_date, $driver_id);
			if ($shift_detail)
			{
				foreach ($shift_detail as $drivertimeline)
				{
					if ($drivertimeline['status'] == 'SHIFT_STARTED' || $drivertimeline['status'] == 'SHIFT_ENDED')
					{
						//get the shift detail
						$shift_detail = $this -> Activity_2_model -> get_shiftdetail($drivertimeline['shift_id']);
						if ($shift_detail)
						{
							$drivertimeline['cab_type'] = $shift_detail['title'];
							$drivertimeline['plate_number'] = $shift_detail['cab_plate_no'];
							$drivertimeline['cab_model'] = $shift_detail['cab_model'];
						}
					}
					$data[] = $drivertimeline;
				}

				$timeline_data['report_data'] = $data;
				// echo "<pre>";
				//  print_r($timeline_data);
				$this -> load -> view('total_shift_report', $timeline_data);
			}
		}

		function get_bookingdetail()
		{
			$booking_no = $_POST['booking_no'];
			$data['get_data'] = $this -> Booking_model -> get_pre_booking($booking_no);

			$this -> load -> view('advance_pop_up', $data);
		}

		/*shift_data */
		function shift_report()
		{
			if ((isset($_POST['date'])) && ($_POST['driverid']))
			{
				$date = $_POST['date'];
				$dates = explode('to', $date);
				$start_date = $dates[0];
				$end_date = $dates[1];
				$driver_id = $_POST['driverid'];

				$this -> shift_data($start_date, $end_date, $driver_id);
			}
			else
			{
				$data['fetch_driver'] = $this -> Driver_model -> get_driver_name();
				$this -> load -> view('site-nav/header');
				$this -> load -> view('shift_report', $data);
				$this -> load -> view('site-nav/footer');
			}
		}

		/* this is for check shift history */
		function shift_data($start_date, $end_date, $driver_id)
		{

			$extra_param = array(
				"start_date" => trim($start_date),
				"end_date" => trim($end_date),
				"driver_id" => $driver_id
			);

			/*$count = $this->Activity_2_model->count_shift_report($start_date, $end_date,
			 * $driver_id);
			 $pagingconfig = $this->paginationlib->initPagination("Activity/shift_data_ajax",
			 $count, 10, 1, $extra_param);
			 // $config['cur_tag_open'] = '<li class="active"><a href="#">';
			 $this->ajax_pagination->initialize($pagingconfig);
			 //$extra_param=json(array("driver_id"=>$did,"status"=>$status));

			 $data['link'] = $this->ajax_pagination->create_links();
			 */
			$limit = 10;
			$offset = 0;
			$shift_detail = $this -> Activity_2_model -> driver_shift_report($start_date, $end_date, $driver_id);
			if ($shift_detail)
			{
				foreach ($shift_detail as $shift)
				{
					echo "<pre>";
					print_r($shift);
					$shift_starttime = $shift['createdon'];
					$shift_endtime = $shift['endon'];

					$break_detail = $this -> Activity_2_model -> shift_break_detail($shift['s_id']);
					$shift_booking_detail = $this -> Activity_2_model -> driver_booking_detail($shift_starttime, $shift_endtime, $driver_id);

					echo "shift booking";
					// print_r($shift_booking_detail);
					if ($break_detail)
					{
						foreach ($break_detail as $break)
						{
							$data[] = $break_data[] = array(
								"time" => $break['break_time'],
								"type" => $break['type'],
								"shift_id" => $break['shift_id']
							);
						}

					}
					else
					{
						$break_data = array();
					}
					if ($shift_booking_detail)
					{
						foreach ($shift_booking_detail as $booking_detail)
						{

							$booking_id = $booking_detail['booking_id'];
							$pickup_location = $booking_detail['pickup_location'];
							$destination_location = $booking_detail['destination_location'];
							$driver_response_time = $booking_detail['driver_datetime'];
							$booking[] = array(
								"time" => $driver_response_time,
								"pickup_location" => $pickup_location,
								"destination_location" => $destination_location,
								"booking_id" => booking_id
							);
							$booking_process = $this -> Activity_2_model -> booking_process($booking_id);
							print_r($booking_process);
							foreach ($booking_process as $booking_process_data)
							{

								$data[] = $booking_process_detail[] = array(
									"type" => $booking_process_data['status'],
									"time" => $booking_process_data['createdon'],
									"booking_id" => $booking_process_data['booking_id'],
									"pickup_location" => $pickup_location,
									"destination_location" => $destination_location,
								);

							}

						}
					}
					$shift_data[] = array(
						"shift_start" => $shift['createdon'],
						"shift_ended" => $shift['endon'],
						"comment" => $shift['comment'],
						"cab_title" => $shift['title'],
						// "cab_type"=>$shift['type'],
						"cab_number" => $shift['cab_plate_no'],
						"car_model" => $shift['cab_model'],
						"break_data" => $break_data
					);

				}
			}
			$data['report_data'] = $shift_data;
			//echo "<pre>";
			// print_R($data);
			$this -> load -> view('total_shift_report', $data);

		}

		function shift_data_ajax()
		{
			$postdata = $this -> input -> post('data');

			$post = json_decode($postdata, true);

			$driver_id = $post['driver_id'];

			$start_date = $post['start_date'];
			$end_date = $post['end_date'];
			$count = $this -> Activity_2_model -> count_shift_report($start_date, $end_date, $driver_id);
			$extra_param = array(
				"start_date" => "2015-07-01",
				"end_date" => "2015-09-11",
				"driver_id" => $driver_id
			);
			$pagingconfig = $this -> paginationlib -> initPagination("Activity/shift_data_ajax", $count, 10, 3, $extra_param);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			//get the posts data
			$limit = $pagingconfig['per_page'];
			if (!$post['page'])
			{
				$offset = 0;
			}
			else
			{
				$offset = ($post['page'] - 1) * $limit;
				$offset = $post['page'];
			}
			$data['total_report'] = $this -> Activity_2_model -> driver_shift_report($start_date, $end_date, $driver_id, $offset, $limit);

			//load the view
			$this -> load -> view('total_shift_report', $data, false);
		}

		/* for break report */
		function break_report()
		{
			if ((isset($_POST['date'])) && ($_POST['driverid']))
			{
				$date = $_POST['date'];
				$dates = explode('to', $date);
				$start_date = $dates[0];
				$end_date = $dates[1];
				$driver_id = $_POST['driverid'];

				$this -> break_data($start_date, $end_date, $driver_id);
			}
			else
			{
				$data['fetch_driver'] = $this -> Driver_model -> get_driver_name();
				$this -> load -> view('site-nav/header');
				$this -> load -> view('break_report', $data);
				$this -> load -> view('site-nav/footer');
			}
		}

		/* this is for check shift history */
		function break_data($start_date, $end_date, $driver_id)
		{

			$extra_param = array(
				"start_date" => "2015-07-01",
				"end_date" => "2015-09-11",
				"driver_id" => $driver_id
			);

			$count = $this -> Activity_2_model -> count_break_report($start_date, $end_date, $driver_id);
			$pagingconfig = $this -> paginationlib -> initPagination("Activity/break_data_ajax", $count, 10, 3, $extra_param);
			// $config['cur_tag_open'] = '<li class="active"><a href="#">';
			$this -> ajax_pagination -> initialize($pagingconfig);
			//$extra_param=json(array("driver_id"=>$did,"status"=>$status));

			$data['link'] = $this -> ajax_pagination -> create_links();

			$limit = 10;
			$offset = 0;
			$data['break_report'] = $this -> Activity_2_model -> driver_break_report($start_date, $end_date, $driver_id, $offset, $limit);
			//print_R($data);
			$this -> load -> view('total_break_report', $data);

		}

		function break_data_ajax()
		{
			$postdata = $this -> input -> post('data');

			$post = json_decode($postdata, true);

			$driver_id = $post['driver_id'];

			$start_date = $post['start_date'];
			$end_date = $post['end_date'];
			$count = $this -> Activity_2_model -> count_break_report($start_date, $end_date, $driver_id);
			$extra_param = array(
				"start_date" => "2015-07-01",
				"end_date" => "2015-09-11",
				"driver_id" => $driver_id
			);
			$pagingconfig = $this -> paginationlib -> initPagination("Activity/break_data_ajax", $count, 10, 3, $extra_param);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			//get the posts data
			$limit = $pagingconfig['per_page'];
			if (!$post['page'])
			{
				$offset = 0;
			}
			else
			{
				$offset = ($post['page'] - 1) * $limit;
				$offset = $post['page'];
			}
			$data['break_report'] = $this -> Activity_2_model -> driver_break_report($start_date, $end_date, $driver_id, $offset, $limit);

			//load the view
			$this -> load -> view('total_break_report', $data, false);
		}

		function Shift_image()
		{
			if ((isset($_POST['date'])) && ($_POST['driverid']) && ($_POST('shift_id')))
			{
				$date = $_POST['date'];
				$dates = explode('to', $date);
				$start_date = $dates[0];
				$end_date = $dates[1];
				$driver_id = $_POST['driverid'];

				$this -> break_data($start_date, $end_date, $driver_id);
			}
			else
			{
				$data['fetch_driver'] = $this -> Driver_model -> get_driver_name();
				$this -> load -> view('site-nav/header');
				$this -> load -> view('shift_image', $data);
				$this -> load -> view('site-nav/footer');
			}
		}
		function getbookingdetail()
		{
			 $booking_no = $_POST['booking_no'];
			$data['get_data'] = $this -> Activity_2_model -> get_pre_booking($booking_no);

			$this -> load -> view('advance_pop_up', $data);
		}

	}
