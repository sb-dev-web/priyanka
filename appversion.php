<?php
include_once ("constants.php");
include ('config.php');
header('Content-type: text/xml');
define("LATESTVERSION", "17.1");
define("ISREQUIRE", "Y");
//define("URL", "http://install.hertzvpi.ie/builds/U6LCV5bPG2");//yes 16.0
//define("URL","http://install.hertzvpi.ie/builds/C3Ir9TJRS2");//real link of 17.0
define("URL","http://install.hertzvpi.ie/builds/pdYya4th2j");//any bug in TAS 
define("VERSIONMESSAGE_REQUIRE",
    "A new version of the app is available, click Install to proceed or Later to reject.");
define("VERSIONMESSAGE_OPTIONAL",
    "A new version of the app is available, click Install to proceed.");
if (isset($_REQUEST['appversion']))
{
    //Update db from device app version
    //Chk alrady use same version or not
    $version = mysql_real_escape_string($_REQUEST['appversion']);
    $device_specifier = mysql_real_escape_string($_REQUEST['device_specifier']);
    if ($device_specifier != '')
    {
        $chk = "select id from tb_deviceversion_detail where device_specifier='" . $device_specifier .
            "' and version='" . $version . "'";
        $res = mysql_query($chk) or die("error");
        if (mysql_num_rows($res) == 0)
        {
            $insert = "insert into tb_deviceversion_detail(device_specifier,version,installdate)values('" .
                $device_specifier . "','" . $version . "'," . GMT_DATE . ") ";
            $res = mysql_query($insert);
        }
    }
    //chk version

    if ($version < LATESTVERSION && ISREQUIRE == 'Y')
    {
        $content = "<response><result>201</result><message>" . VERSIONMESSAGE_REQUIRE .
            "</message><version>" . LATESTVERSION . "</version><isrequire>" . ISREQUIRE .
            "</isrequire><downloadurl>" . URL . "</downloadurl></response>";

    }
    elseif ($version < LATESTVERSION)
    {
        $content = "<response><result>201</result><message>" . VERSIONMESSAGE_OPTIONAL .
            "</message><version>" . LATESTVERSION . "</version><isrequire>" . ISREQUIRE .
            "</isrequire><downloadurl>" . URL . "</downloadurl></response>";

    }
    else
    {
        $content = "<response><result>" . SUCCESS_CODE . "</result><message>" .
            SUCCESS_MESSAGE . "</message></response>";
    }
}
else
{
    $content = "<response><result>" . MISSING_PARAMETER_CODE . "</result><message>" .
        MISSING_PARAMETER_MESSAGE . "</message></response>";
}
echo $content;
?>
