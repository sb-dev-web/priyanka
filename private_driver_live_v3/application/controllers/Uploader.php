<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Uploader extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
         $this->load->helper(array('form','url'));
       
    }
    public function category()
    {
		$this->load->helper('constants_helper','driver_constants_helper');
        $fileupload=$_POST['file_upload_name'];
        
        $FileName			= strtolower($_FILES[$fileupload]['name']); //uploaded file name
    	$FileTitle			= uniqid(); // file title
    	$ext			    = pathinfo($FileName, PATHINFO_EXTENSION);
    	$RandNumber   		= rand(0, 9999999999); //Random number to make each filename unique.
    	$NewFileName        = $FileTitle.'_'.$RandNumber.'.'.$ext;
        
        $config['upload_path'] ='./images/profile/';
		//echo $config['upload_path'];
		//print_r($config);
        $config['allowed_types'] = 'jpg|png';
        $this->load->library('upload', $config);
        
        $error_flag=0;
         
        $config['file_name']=$NewFileName;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($fileupload))
		{
            $error_flag=1;
		    $error = array('error' => $this->upload->display_errors());
            echo "<pre>";
            print_R($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
            $profile_pic=DRIVER_IMAGE_PATH.$data['upload_data']['file_name'];
            $raw_name=$config['upload_path'].'/'.$data['upload_data']['raw_name'];
		}
         
        if($error_flag){
            $response['upload_flag']=0;
            echo json_encode($response);
            exit;
        }
        else{
            $response=array('upload_flag'=>1,"category_icon"=>$NewFileName);
            echo json_encode($response);
            exit;
        }
    }
	
	
	
	  public function license()
    {
		$this->load->helper('constants_helper','driver_constants_helper');
        $fileupload=$_POST['file_upload_name'];
        
        $FileName			= strtolower($_FILES[$fileupload]['name']); //uploaded file name
    	$FileTitle			= uniqid(); // file title
    	$ext			    = pathinfo($FileName, PATHINFO_EXTENSION);
    	$RandNumber   		= rand(0, 9999999999); //Random number to make each filename unique.
    	$NewFileName        = $FileTitle.'_'.$RandNumber.'.'.$ext;
        
        $config['upload_path'] ='./images/profile/';
		//echo $config['upload_path'];
		//print_r($config);
        $config['allowed_types'] = 'jpg|png';
        $this->load->library('upload', $config);
        
        $error_flag=0;
         
        $config['file_name']=$NewFileName;
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($fileupload))
		{
            $error_flag=1;
		    $error = array('error' => $this->upload->display_errors());
            echo "<pre>";
            print_R($error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
            $profile_pic=DRIVER_IMAGE_PATH.$data['upload_data']['file_name'];
            $raw_name=$config['upload_path'].'/'.$data['upload_data']['raw_name'];
		}
         
        if($error_flag){
            $response['upload_flag']=0;
            echo json_encode($response);
            exit;
        }
        else{
            $response=array('upload_flag'=>1,"category_icon"=>$NewFileName);
            echo json_encode($response);
            exit;
        }
    }
}
?>