<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	 function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->model('Welcome_model');
		 $this->load->library('session');
		 
    }
		
    
    
	
	 public function index()
	{
		$this->load->view('login');
        
	}
	
	public function onsuccess()
	{
		$user = $this->session->userdata('logged_in');
        if (!$user) {
            redirect('Welcome');
        }
		$driver=$this->Welcome_model->fetch_total_driver();
		$cab=$this->Welcome_model->fetch_total_cab();
		$booking=$this->Welcome_model->fetch_total_booking();
		$passanger=$this->Welcome_model->fetch_total_passanger();
		$advance_booking=$this->Welcome_model->fetch_advance_booking();
		$due_ammount=$this->Welcome_model->fetch_due_amount();
		$total_amount=$this->Welcome_model->fetch_total_amount();
		$Goldcustomer =$this->Welcome_model->GoldCustomer();
		$GoldDriver=$this->Welcome_model->GoldDriver();
		$data['driver']=$driver;
		$data['cab']=$cab;
		$data['passanger']=$passanger;
		$data['booking']=$booking;
		$data['advance_booking']=$advance_booking;
		$data['total_amount']=$total_amount;
		$data['due_ammount']=$due_ammount;
		$data['Goldcustomer']=$Goldcustomer;
		$data['GoldDriver']=$GoldDriver;
        $data['active_page'] = 'index';
        $this->load->view('site-nav/header', $data);
        $this->load->view('index');
		
        $this->load->view('site-nav/footer');
	}
	
	
}
