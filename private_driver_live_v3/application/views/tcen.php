<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title></title>
	
	<meta name="author" content="Marius Lunde">
	<meta name="created" content="20160216;131900000000000">
	<meta name="changed" content="20160217;190739501343626">
	<meta name="AppVersion" content="16.0000">
	<meta name="DocSecurity" content="0">
	<meta name="HyperlinksChanged" content="false">
	<meta name="LinksUpToDate" content="false">
	<meta name="ScaleCrop" content="false">
	<meta name="ShareDoc" content="false">
	<style type="text/css">
	
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<!--
		@page { margin: 2.5cm }
		p { margin: 0cm;
			margin-top:10px; 
			direction: ltr; 
			line-height: 50px; 
			text-align: justify;
			font-size: 30px;
			font-family: 'Lato', sans-serif;
			word-spacing: 10px;
			padding-left: 40px; 
			 }
		b{ font-size: 30px;font-family: 'Lato', sans-serif;}
		
		a:link { color: #0563c1; so-language: zxx }
	-->
	</style>
</head>
<body lang="nb-NO" link="#0563c1" dir="ltr">

<b>1. Accepting the terms</b></p>

<p>By fullfill the registration you are accepting these terms.</p><br />


<b>2. Prices</b>

<p>Our prices are a
result of strating price, number of minutes and number of kilometers.
Prices are adjusted without prior notification, and by using the
funcionallity ««Prisestimat» you will be able to see what the trip
will cost. This price is base don normal traffic and is built on
experience from this route, time of day, day of week and more. The
price estimate is not conclusive because special traffical incidents
can make travel time and route longer than normal.</p><br /><br />


<b>3. Minimum price</b></p>

<p>The minimum price
per tour is NOK 100,- independent of travelled time and length.</p><br />


	<b>4. Cancellation</b>

<p>If you cancels the
trip before it’s more than 3 minutes to estimated time of arrival,
you will not be charged with any costs. If you cancels the trip when
it is less than three minutes to estimated time of arrval you will be
charged with NOK 100,-</p><br />


	<b>5. No show</b>

<p>If you don’t show
up to the driver within five minutes after you have recieved a text
message that the car is on destination adress you will be charged
with NOK 100,00.</p><br />


	<b>6. Receipt</b>

<p>You will recieve a
specified receipt on email after each trip, including trips where you
are charged according to point 3 and 4.</p><br />


	<b>7. Refund</b>

<p>If you by any reason
are charged the incorrect amount, please contact us on email:
<a href="mailto:booking@privatedriver.no">booking@privatedriver.no</a> and
we will without any delay refund the overpaid amount. There can be
situations where you are charged a higher amount than the end price
indicates, dobbelt payment or you are wrongly charged for amounts
according to point 3 and 4 in these terms. Other complaints about our
services where you mean you are obliged to any compensation must be
explained in writing and no such incidents are automatically refunded
before we have considered your complaint.</p><br />


	<b>8. Your
	information and data</b>

<p>Private Driver A/S
shall take care of all your personal information in the most secure
way. They shall not be shared with a third party without your
approval. These data can be used to improve our services.  By using
our application you accept to recieve SMS and push notifications and
other type of messages through the appication with content including
a third party. These can be campaigns, discounts, prices, news and
similar.</p><br />

</body>
</html>