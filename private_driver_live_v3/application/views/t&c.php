<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title></title>
	
	<meta name="author" content="Marius Lunde">
	<meta name="created" content="20160215;200900000000000">
	<meta name="changedby" content="Marius Lunde">
	<meta name="changed" content="20160215;202700000000000">
	<meta name="AppVersion" content="16.0000">
	<meta name="DocSecurity" content="0">
	<meta name="HyperlinksChanged" content="false">
	<meta name="LinksUpToDate" content="false">
	<meta name="ScaleCrop" content="false">
	<meta name="ShareDoc" content="false">
	
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
		
	<style type="text/css">
	<!--
		@page { margin: 2.5cm }
		b{ font-size: 30px;font-family: 'Lato', sans-serif;}
		p { margin: 0cm; 
			margin-top:10px;
			direction: ltr; 
			line-height: 50px; 
			text-align: justify; 
			font-size: 30px;
			font-family: 'Lato', sans-serif;
			word-spacing: 10px;
			padding-left: 40px;}
		a:link { color: #0563c1; so-language: zxx }
	-->
	</style>
</head>
<body lang="nb-NO" link="#0563c1" dir="ltr">


	<b style="margin: 0;">1. Akseptere betingelsene</b>
<p>
	Gjennom å fullføre registreringen aksepterer du disse betingelser. 
</p><br />


	<b>2. Priser</b>

<p>
	Våre priser er et resultat av startpris, antall minutter og antall kilometer. Prisene
	justeres jevnlig, og ved hjelp av funksjonen «Prisestimat» vil du
	kunne se hva turen vil koste. Denne prisen er basert på normal
	trafikk og bygger på erfaring fra reiserute, tid på døgnet, ukedag
	med mer. Prisestimatet er ikke bindende da særskilte trafikale
	forhold kan gjøre reisetid og -rute lenger enn normalt.</p><br />
	

<b>3. Minimumspris</b>

<p>Minimumsprisen per
tur er kr. 100,- uavhengig av reisetid og lengde.</p><br />


<b>4. Kansellering</b>

<p>Dersom du
kansellerer turen før det er 3 minutter til ankomst påløper ingen
omkostninger. Kansellerer du når det er mindre enn tre minutter
igjen blir du belastet med kr. 100,-.</p><br />


<b>5. No show</b>

<p>Dersom du ikke
ankommer bilen innen fem minutter etter at du har mottatt melding om
at bilen er på angitt adresse, blir du belastet med kr. 100,-.</p><br />


<b>6. Kvittering</b>

<p style="margin: 0cm; line-height: 50px;margin-top: 10px;">Du vil motta
spesifisert kvittering på e-post etter hver tur, også de turer hvor
du blir belastet i.h.t. pkt. 3 og 4</p><br />


<b>7. Refusjon</b>

<p>Dersom du blir
belastet feil beløp av ulike grunner, ta kontakt med oss på e-post:
<a href="mailto:booking@privatedriver.no">booking@privatedriver.no</a> og
vi vil umiddelbart og uten opphold refundere det overskytende beløp.
Dette kan være tilfeller hvor du blir belastet høyere beløp enn
sluttpris indikerer, dobbeltbetaling eller dersom du feilaktig er
trukket for beløp i.h.t. pkt. 3 og 4 i disse betingelser. Andre
klager på våre tjenester som du mener du skal ha erstatning for, må
grunngis skriftlig og ingen refusjon gis automatisk før vi har
vurdert din klage.</p><br />


<b>8. Dine	opplysninger og data</b>

<p>Private Driver A/S
skal ivareta alle dine personlige opplysninger på sikreste vis. De
skal ikke deles med tredjeperson uten ditt samtykke. Dataene kan
benyttes til å forbedre våre tjenester.  Gjennom å benytte vår
applikasjon aksepterer du å motta SMS og meldinger via applikasjonen
med budskap som kan inkludere tredjepart. Dette kan være kampanjer,
rabatt, premier, nyheter og lignende.</p><br />
</body>
</html>