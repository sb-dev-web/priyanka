<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Completed Restaurant Booking
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Completed Booking</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              
             
            </div>
            <div class="box-body record">
              <br>


            
            
            
            <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Booking No</th>
                      
                        
                        
                       <th>Booking Time</th>
                       <!-- <th>Pickup_Address</th>
                        <th>Destination_Address</th>-->
                        <!--<th>Total Ammount</th>
                        <th>Total K.M.</th>-->
                        <th>User Paid</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                     
                      if(!empty($restaurant_booking))
                      
                      {
                        for($i=0;$i<sizeof($restaurant_booking);$i++)
                        {
                         
                         
                        
                          
                          
                         
                      ?>
                      <tr>
                        
                       
                      <td><a href="javascript:void(0);" onclick="restro_detail('<?php echo $restaurant_booking[$i]['id'];?>');"><?php echo $restaurant_booking[$i]['booking_number'];?></a></td>
                        
                        <td><?php
                        $timestamp = strtotime($restaurant_booking[$i]['booking_time']);
                           echo date('d F Y', $timestamp);
                      ?></td>
                       <!-- <td><?php echo $pick_address;?></td>
                          <td><?php echo $address;?></td>-->
                         
                         
                        
               <td><?php  $amount=$restaurant_booking[$i]['user_paid'];
                                if($amount>0)
                                {
                                  
                                ?>
                                <span class="label label-success">Yes</span>
                                 
                                <?php
                                }
                                else
                                {
                                ?>
                                 <span  class="label label-warning">No</span></a>
                                <?php
                                }
                                
                             
                          ?></td>
                       
                      
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      ?>


                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                           <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
              
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
           <!-- open customer pop up -->
<div class="modal fade" id="open_customer" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
              Complete Booking Detail
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="text-align: left; margin: 0 auto; width: 80%;">
               
                 
                    </div>
                     <div id="showreport" name="showreport"></div>
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
    </div>   
</div>
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      
     
      </aside><!-- /.control-sidebar -->

     
<script>
 /*  function restro_detail(tamount,userpaid,email,pickupaddress,kmdriven)
        {
          //alert(cname);
          // var sd="<div><div>Customer name:- <span style=margin-left:6px; float:right;>Xyz</div><br>\
         //  <div>Customer email:- xyz@gmail.com</div>";
           
           $('#customername').val(email);
           $('#totalamount').val(tamount);
           $('#userpaid').val(userpaid);
          $('#customerpickup').val(pickupaddress);
          // $('#customerdestination').val(destination);
           $('#customerkmdriven').val(kmdriven);
         $('#open_customer').modal({
                 "backdrop" : "static"
         })
             }*/
             
             
             
             
             
             
             
             
        function restro_detail(booking_no)
        {
     
       $.ajax({
            type:'POST',
            url: '<?php echo site_url('Restaurant/get_restaurant_completed_booking');?>',
            data:{"booking_no":booking_no},
             beforeSend: function() {
              $("#loading-image").show();
           },
            success: function(response) {
            //alert(response);
             $("#showreport").html(response);
             $('#open_customer').modal({
                 "backdrop" : "static"
         })
              $("#loading-image").hide();
             }});
             }
</script>




     

      