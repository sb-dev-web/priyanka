<?php

	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Cron extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this -> load -> library(array(
				'upload',
				'Twilio',
				'S3_lib',
				'Braintree_lib'
			));

			$this -> load -> helper(array(
				'form',
				'url',
				'string',
				'constants_helper',
				'function_helper',
				'braintree_function'
			));

			$this -> load -> model('Cron_model');

		}

		/* driver booking according to date */

		public function Delete_log_cron()
		{

			$delete_cron = $this -> Cron_model -> Delete_cron();

		}

		function Collect_payment()
		{
			$this -> load -> library('curl');
			//chk the normal user remaining bill (2 days ago)
			$remaining_payment = $this -> Cron_model -> remaining_payment($input_method);
			//echo "<pre>";
			//print_r($remaining_payment);
			if ($remaining_payment)
			{
				//

				foreach ($remaining_payment as $payment_detail)
				{
					//echo "there";
					extract($payment_detail);
					if($fix_amount!='0.00')
					{
						$billing_amount=$fix_amount;
					}
					//call the curl for make_payment api
					$this -> load -> library('curl');
					$url = "http://privatedriverapp.com/app/V3/Payment/make_payment";
					$post_data = array(
						"user_id" => $user_id,
						"booking_id" => $booking_id,
						"billing_amount" => $billing_amount,
						"iscron"=>"Y"
					);
					$output = $this -> curl -> simple_post($url, $post_data);
					

				}
			}

		}

        function CollectDuePayment()
		{
			$this -> load -> library('curl');
			//chk the normal user remaining bill (2 days ago)
			$remaining_payment = $this -> Cron_model -> remaining_due_payment($input_method);
			if ($remaining_payment)
			{
				//echo "<pre>";
               // print_r($remaining_payment);
				foreach ($remaining_payment as $payment_detail)
				{
					extract($payment_detail);
					//call the curl for make_payment api
					$this -> load -> library('curl');
					if($fix_amount!='0.00')
					{
						$billing_amount=$fix_amount;
					}
					$url = "http://privatedriverapp.com/app/V3/Payment/make_payment";
					$post_data = array(
						"user_id" => $user_id,
						"booking_id" => $booking_id,
						"billing_amount" => $billing_amount,
						"iscron"=>"Y"
					);
					$output = $this -> curl -> simple_post($url, $post_data);
					//print_r($output);

				}
			}
		}

		function ExpireCard()
		{
			//$this -> load -> library('curl');

			$expire_card = $this -> Cron_model -> ExpireCard();
			if ($expire_card)
			{
				foreach ($expire_card as $data)
				{
					echo "<br/>" . $email = $data['email'];
					UserCardExpireEmail($email);
				}
			}
			else
			{
				echo "no data";
			}
		}

		function AuthPaymentVoid()
		{
			error_reporting(1);
			$getAuthPayment = $this -> Cron_model -> AuthPayment(1);
			
			//print_r($getAuthPayment);
			//get pre booking auth token

			//print_r($getAuthPayment);
			foreach ($getAuthPayment as $transaction_data)
			{
				$data['transaction_id'] = $transaction_data['transaction_id'];
				$voiddetail = VoidedTransaction($data);
				$voiddetail['id'] = $transaction_data['id'];
				//update void transaction status
				$this -> Cron_model -> UpdateVoidTransaction($voiddetail);
				//exit;
			}

			$getAuthPayment = $this -> Cron_model -> AuthPayment(2);
			
			foreach ($getAuthPayment as $transaction_detail)
			{
				VOIDAUTHTRANSACTION($transaction_detail['booking_id'], "From Cron");
			}

		}
		
		function ClaimMail()
		{
			$DuePaymentDetail=$this->Cron_model->GetFailPayment();
			if($DuePaymentDetail)
			{
				foreach ($DuePaymentDetail as $FailPayment) {
					
				}
			}else
			{
				
			}
		}

	}
