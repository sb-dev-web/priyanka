function uploadCategory(url,formID,fileUpload,fileName,originalName,loading,filePreview)
{
       
        var formData = new FormData($('#'+formID)[0]);   
        var ele = document.getElementById(fileUpload);
        var original_name=ele.files[0].name;
        $("."+filePreview).text(original_name);
        var file_size=ele.files[0].size/1024;
        if(file_size>5500){
            $("."+filePreview).text("File Size Exceed");
            return false;
        }
        
        formData.append('file_upload_name', fileUpload);
        $.ajax({
            url: url, 
            type: 'POST',
            //Ajax events
            beforeSend: function(){category_beforeHandler(loading);},
            success: function(response) {
        
               setTimeout(function(){
                category_completeHandler(response,fileName,originalName,fileUpload,loading,original_name);
                },1500);
                },
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout:100000
        });
}

function uploadLicense(url,formID,fileUpload,fileName,originalName,loading,filePreview)
{
       
        var formData = new FormData($('#'+formID)[0]);   
        var ele = document.getElementById(fileUpload);
        var original_name=ele.files[0].name;
        $("."+filePreview).text(original_name);
        var file_size=ele.files[0].size/1024;
        if(file_size>5500){
            $("."+filePreview).text("File Size Exceed");
            return false;
        }
        
        formData.append('file_upload_name', fileUpload);
        $.ajax({
            url: url, 
            type: 'POST',
            //Ajax events
            beforeSend: function(){category_beforeHandler(loading);},
            success: function(response) {
        
               setTimeout(function(){
                category_completeHandler(response,fileName,originalName,fileUpload,loading,original_name);
                },1500);
                },
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout:100000
        });
}






function category_beforeHandler(loading)
{
   $('#'+loading).css('display','inline');
}
function category_completeHandler(response,fileName,originalName,fileUpload,loading,original_name)
{
    $('#'+loading).css('display','none');
    var temp=JSON.parse(response);
    //alert(temp.upload_flag);
    if(temp.upload_flag==1){
        $("#"+fileName).val(temp.category_icon);
        $("#"+originalName).val(original_name);
    }
}