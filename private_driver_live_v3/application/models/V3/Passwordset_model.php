<?php
	class Passwordset_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		function GetCompany($data)
		{
			$email = $data['code'];
			$this -> db -> select("id,company_name");
			$query = $this -> db -> get_where("company", array(
				"email" => $email,
				"password" => ""
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> row_array();
			}
			else
			{
				return false;
			}

		}

		function GetEmployee($data)
		{
			$email = $data['code'];
			$this -> db -> select("id,company_name,name");
			$query = $this -> db -> get_where("user_temp", array(
				"email" => $email,
				"password" => ""
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> row_array();
			}
			else
			{
				//from app register user direct save in user table
				$this -> db -> select("id,company_name,name");
				$query = $this -> db -> get_where("user", array(
					"email" => $email,
					"password" => ""
				));
				if ($query -> num_rows() > 0)
				{
					return $query -> row_array();
				}
				else
				{
					return false;
				}

			}

		}

		function SetPassword($input_method)
		{

			$password = $input_method['form-password'];
			$id = $input_method['id'];
			//get data from temp tabel
			$query = $this -> db -> get_where("user_temp", array("id" => $id));
			//echo $this -> db -> last_query();
			if ($query -> num_rows())
			{
				$this -> db -> trans_begin();

				$userdetail = $query -> row_array();
				$userdetail['password'] = md5($password);
				$userdetail['is_emailverify'] = 1;
				unset($userdetail['id']);
				unset($userdetail['pass']);
				$AuthToken = _generate_key();
				$userdetail['auth_token'] = $AuthToken;
				$insert = $this -> db -> insert("user", $userdetail);
				//echo $this -> db -> last_query();
				$user_id = $this -> db -> insert_id();
				if ($user_id)
				{
					//check if the company user make a entry in user_company table
					if ($userdetail['company_id'])
					{
						$user_company_data = array(
							"user_id" => $user_id,
							"company_name" => $userdetail['company_name'],
							"company_id" => $userdetail['company_id'],
							"is_approved" => 'Y',
							"created_on" => get_gmt_time()
						);
						$this -> db -> insert("user_company", $user_company_data);
						//echo $this -> db -> last_query();
						if (!$this -> db -> insert_id())
						{

							$this -> db -> trans_rollback();
							$this -> db -> trans_complete();

							return false;

						}
					}

					//make openfire user
					$user_name = USER_JID_NAME . '-' . $user_id;
					$name = $userdetail['name'];
					$email = $userdetail['email'];
					$param = array(
						"username" => $user_name,
						"name" => "$name",
						"password" => "$password",
						"email" => "$email"
					);
					//print_r($param);
					$openfir_result = $this -> openfire -> openfire_action("POST", $param, "users/");
					$openfire_response = json_decode($openfir_result, true);
					//print_R($openfire_response);
					if ($openfire_response['status'] == 0)
					{
						$this -> db -> trans_rollback();
						$this -> db -> trans_complete();

						//delete the user from temp user

						return false;

					}
					else
					{
						$this -> db -> trans_commit();
						$this -> db -> trans_complete();
						$this -> db -> delete("user_temp", array("id" => $id));
						//echo $this->db->last_query();

						return true;
					}

				}
				else
				{
					return false;
				}

			}
			else
			{
				$this -> db -> trans_begin();
				//when a user register by app
				$chk = $this -> db -> get_where("user", array("id" => $id));
				if ($chk -> num_rows() > 0)
				{
					$userdetail = $chk -> row_array();
					//update the password
					$this -> db -> where("id", $id);
					$update_arr = array("password" => md5($password),"is_emailverify"=>"1","is_active"=>"1","is_varify"=>"1");
					$update = $this -> db -> update("user", $update_arr);
					//make openfire user
					$user_name = USER_JID_NAME . '-' . $id;
					$name = $userdetail['name'];
					$email = $userdetail['email'];
					$param = array(
						"username" => $user_name,
						"name" => "$name",
						"password" => "$password",
						"email" => "$email"
					);
					//print_r($param);
					$openfir_result = $this -> openfire -> openfire_action("POST", $param, "users/");
					$openfire_response = json_decode($openfir_result, true);
					//print_R($openfire_response);
					if ($openfire_response['status'] == 0)
					{
						$this -> db -> trans_rollback();
						$this -> db -> trans_complete();

						//delete the user from temp user

						return false;

					}
					else
					{
						$this -> db -> trans_commit();
						$this -> db -> trans_complete();
						//$this -> db -> delete("user_temp", array("id" => $id));
						//echo $this->db->last_query();

						return true;
					}

				}
				else
				{
					return FALSE;
				}

			}

		}

	}
?>