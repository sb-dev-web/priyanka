<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Companies
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active"> Companies</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
        
                  
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <a href="<?php echo site_url('Company/add_company');?>"><input type="button" class="btn btn-block btn-warning" value="Add" style="float:right; margin-right:7px;margin-top:8px; width:10%;"/></a><br/><br/>
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example1" class="table table-bordered table-striped" style="font-size:15px;">
                    <thead>
                      <tr>
                        <th>Company</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Address</th>
                        <th>Total User</th>
                        <th>Action</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(sizeof($company))
                      {
                       
                        for($i=0;$i<sizeof($company);$i++)
                        {
                         
                          
                      ?>
                      <tr>
                        
                        <td><?php echo  $company[$i]->company_name;?></td>
                         <td><?php echo $company[$i]->email;?></td>
                        <td><?php echo  $company[$i]->phone;?></td>
                       <td><?php echo   $company[$i]->address;?></td>
                       <td>
                       <a href="javascript:void(0);" onclick="get_company_user_report('<?php echo $company[$i]->id?>','<?php echo $company[$i]->company_user;?>')"><b><?php echo $company[$i]->company_user;?></b>&nbsp;Users</a></td>           
                      <td> <a href="<?=site_url('Company/add_company').'/'.$company[$i]->id; ?>" title="Edit"><i class="fa fa-pencil-square-o fa-2x"></a></i>|&nbsp;<a href="javascript:void(0);" onclick="changedeleteStatus('<?php echo $company[$i]->id;?>','delloading_<?php echo $company[$i]->id;?>', 'spandel_<?php echo $company[$i]->id;?>')">
                                    <?php if($company[$i]->isdelete=='0'){?>
                                    <span id="spandel_<?php echo $company[$i]->id?>" class="fa fa-trash fa-2x" style="color:red;"></span></a>
                                    <?php }?>
                                    <br /><div id="delloading_<?php echo $company[$i]->id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                      </td>
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      ?>
                      
                      
                      
                    </tbody>
                    <tfoot>
                       
                    </tfoot>
                  </table>
                  <?php
                  
                   ?>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
                    
             <!-- modal for select driver -->



<div class="modal fade" id="open_company_user" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
              Company User
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none;max-height:auto; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="text-align: center; margin: 0 auto; height:auto;width:auto;">
                <div id="company_data">
                 
                </div>    
                </div>
            </div>
        </div> 
    </div>   
</div>         
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
        
          </div>
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      
      <!-- Control Sidebar -->      
      
 <!--pop up -->
 <div id="dialog" style="display: none">
  <span id="ui-id-1" class="ui-dialog-title">fgdgfg</span>
        <div id="dvMap" style="height: 380px; width: 580px; border:3px solid black;">
        </div>
    </div>
      
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <!-- <script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->

<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
   
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css">

      
      

<script type="text/javascript">
$( document ).ready(function() {
    var dataTable = $('#example1').DataTable();
});
function changedeleteStatus(id,loadingdiv,spanid)
{
  //alert();
  var r=confirm("Are you sure to delete this company?");
  if (r==true) {
   
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '1';
    $.ajax({type:'POST',url: '<?php echo site_url('Company/change_delete_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       $("#delloading_"+id).closest('tr').fadeOut('slow')
     }});
}

else
{
  
   

}}
</script>
 <script>
             
  function  get_company_user_report(cid,user)
    {
          if (user=='0') {
            alert('sorry no user found');
          }
          else
          {
               
          
         $('#open_company_user').modal({
                 "backdrop" : "static"
         })      
     
     $.ajax({
            type:'POST',
            url: '<?php echo site_url('Company/Get_company_user');?>',
            data:{"company_id":cid},
            success: function(response) {
           
             $("#company_data").html(response);
             }});
          }
    }
 </script>
 
   