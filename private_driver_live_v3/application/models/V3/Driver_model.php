<?php
class Driver_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    function fetch_total_driver($limit,$offset=0)
    {
        $query=$this->db->query("select driver.*,shift.s_id,shift.status as shift_status,c.cab_model,c.cab_type from driver left join shift on driver.driver_id=shift.driver_id and shift.status='STARTED'  left join cab c on c.cab_id=shift.cab_id where driver.isdelete='0' order by driver_id DESC limit $offset,$limit");       // echo $this->db->last_query();
       // echo $this->db->last_query();
        return $query->result();
    
    }
    function get_driver_name()
    {
        $query=$this->db->query("select driver.*,shift.s_id,shift.status as shift_status,c.cab_model,c.cab_type from driver left join shift on driver.driver_id=shift.driver_id and shift.status='STARTED'  left join cab c on c.cab_id=shift.cab_id where driver.isdelete='0' order by driver_id DESC");       // echo $this->db->last_query();
       // echo $this->db->last_query();
        return $query->result();
    }
    function driver_count()
    {
        $this->db->select("driver_id");
        $this->db->where("isactive","1");
        $this->db->where("isdelete","0");
        $this->db->from("driver");
        $query=$this->db->get();
          
        return ($query->result_id->num_rows);
         
       
        
    }
    function change_driver_status($id,$status)
    {
        if($status=="1")
        {
            $field_array=array("isactive"=>"1");
            $status=='1';
        }
        else
        {
            $field_array=array("isactive"=>"0");
            $status=='0';
        }
        $this->db->where('driver_id',$id);
        $this->db->update("driver",$field_array);
        return $status;
    }
    
   
    
    function save_drive($id="0")
    {
        /*$arr_feilds = array(
                            "first_name"=>$_POST['first_name'],
                            "last_name"=>$_POST['last_name'],
                            "gender"=>$_POST['optionsRadios'],
                            "country"=>$_POST['country'],
                            "state"=>$_POST['state'],
                            "city"=>$_POST['city'],
                            "email_id"=>$_POST['email_id'],
                            //"profile_pic"=>$_POST['driver_pic'],
                            "profile_pic"=>'abc.png',
                            "license_no"=>$_POST['license_no'],
                            "address"=>$_POST['address'],
                            "isactive"=>'1'
                            );*/
       
        
        if($id>0)
        {
            $contact_no=trim($_POST['contact']);
            $this->db->where('driver_id',$id);
            $arr_feilds = array('first_name'=> $_POST['first_name'],
                            'last_name'=>$_POST['last_name'],
                            'personal_contact'=>$contact_no,
                              'contact'=>$contact_no,
                            'gender'=>$_POST['optionsRadios'],
                            'zipcode'=>$_POST['zipcode'],
                            'city'=>$_POST['city'],
                            'license_image'=>$_POST['license_icon_name'],
                            //'email_id'=>$_POST['email_id'],
                            'profile_pic'=>$_POST['category_icon_name'],
                            'address'=>$_POST['address'],
                            'isactive'=>'1'
                           //   'service_area'=>$_POST['service_area']
                            );
            $this->db->update('driver',$arr_feilds);
            
            if($id>0 && (!empty($_POST['password'])))
            {
                 $this->db->where('driver_id',$id);
                $arr_feilds = array('password'=> md5($_POST['password'])
                          );
               $this->db->update('driver',$arr_feilds);
              
            }
            return 'updated';
        }
        else
        {
             $contact_no=trim($_POST['contact']); 
             $arr_feilds = array('first_name'=> $_POST['first_name'],
                            'last_name'=>$_POST['last_name'],
                            'personal_contact'=>$_POST['contact'],
                            'contact'=>$contact_no,
                            'gender'=>$_POST['optionsRadios'],
                             'zipcode'=>$_POST['zipcode'],
                            'city'=>$_POST['city'],
                            'email_id'=>$_POST['email_id'],
                            'password'=>md5($_POST['password']),
                            'profile_pic'=>$_POST['category_icon_name'],
                             'license_image'=>$_POST['license_icon_name'],
                            'address'=>$_POST['address'],
                            'isactive'=>'1'
                            //  'service_area'=>$_POST['service_area']
                            );
            //$this->db->set('create_on', GMT_DATE, FALSE);
            $this->db->insert('driver',$arr_feilds);
            //echo $this->db->last_query();
            $insert=$this->db->insert_id();
            
            return $insert;
             
        }
    }
    
    function get_driver($id)
    {
         if($id>0)
        
         
        $query = $this->db->query("select * from driver where driver_id='$id' and isdelete='0'");
        //echo $this->db->last_query();exit;
        return $query->result();
    }
    function delete_driver($id)
    {
           $this->db->where('driver_id',$id);
        $this->db->set('isdelete','1');
         $this->db->update("driver");
        return 1;
    }

    function check_driver_email($email)
    {
      
       $driver_email=$this->db->query("select * from driver where email_id='$email' and isdelete='0'");
       $count=$driver_email->num_rows();
       if($count>0)
       {
        return 1;
       }
       else
       {
        return 0;
        
       }
    }
    
     function change_login_status($id,$status)
    {
        if($status=="Y")
        {
            $field_array=array("islogin"=>"Y");
            $status=='Y';
        }
        else
        {
            $field_array=array("islogin"=>"N");
            $status=='N';
        }
        $this->db->where('driver_id',$id);
        $this->db->update("driver",$field_array);
        return $status;
    }
    
    /* change shift status */
     function change_shif_status($id,$status)
    {
       if($status=="ENDED")
        {
            $field_array=array("status"=>"ENDED");
            $status=='ENDED';
        }
        else
        {
            die;
        }
        $this->db->where('s_id',$id);
        $this->db->update("shift",$field_array);
        return $status; 
    }
    
    function DriverRating()
    {
    	$query="SELECT avg(rating) rating,driver.driver_id FROM driver left join `driver_rating` on driver.driver_id=driver_rating.driver_id group by driver .driver_id";
		$query=$this->db->query($query);
		return $query->result_array();
    }
	function NewBookingRequest($data)
	{
		$this->db->insert("driver_status",$data);
	}
}