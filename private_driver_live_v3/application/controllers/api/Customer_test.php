<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Customer_test extends REST_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->library(array('upload', 'Braintree_lib'));
        $this->load->library('xmpp', false);
        $this->load->helper(array(
            'form',
            'url',
            'string',
            'constants_helper',
            'function_helper',
            'braintree_function',
            'xml'));

        $this->load->model('Customer_model_test');

    }

    /* register user */
    function register_user()
    {
        /* check that user is already register or not */
        $input_method = $this->webservices_inputs();
        $this->validate_param('register-user', $input_method);

        // check user is exist or not

        $user_data = $this->Customer_model_test->check_exist_email($input_method);
        if (sizeof($user_data) > 0)
        {
            $this->response(array('message' => 'Oops! It seems like you already registered.',
                    'status' => 0), 200);
        }
        else
        {
            $name = $input_method['name'];
            $email = $input_method['email'];

            $phone = $input_method['phone'];
            $credit_card_no = $input_method['credit_card_no'];
            $cvv_no = $input_method['cvv_no'];
            $expire_date = $input_method['expire_date'];

            $data = array(
                "name" => $name,
                "email" => $email,
                "phone" => $phone,
                "ccnumber" => $credit_card_no,
                "cvv_no" => $cvv_no,
                "exp_date" => $expire_date);

            
            $braintree_response = Create_withCreditCardAndVerification($data);
          
            if ($braintree_response['status'] == "1")
            {
                //Insert into user and CC info table
                $user_id = $this->Customer_model_test->save_user($input_method,$braintree_response);
                if ($user_id)
                {
                    $user_name = 'user-' . $user_id;


                    if (@$input_method['is_social'] == 'N')
                    {
                        $password = $input_method['password'];
                        $password = trim($password);
                    }
                    else
                    {
                        $password = "123456";
                        $password = trim($password);
                    }


                    //make a braintree user


                    $param = array(
                        $user_name,
                        $password,
                        $name,
                        $email);
                       
                  $xmpp_response=  $this->xmpp->api("add", $param);
                 

                    $customer = array(

                        "customer_id" => $userid = "$user_id",
                        "name" => $name = $input_method['name'],
                        "email" => $email = $input_method['email'],
                        "dob" => $dob = $input_method['dob'],
                        "phone" => $phone = $input_method['phone'],

                        );


                    $this->response(array(
                        'customer' => $customer,
                        'message' => 'Registartion is successfull',
                        'status' => 1), 200);
                }
            }
            else
            {
                  $this->response(array(
                        
                        'message' => $braintree_response['message'],
                        'errorcode'=>$braintree_response['errorcode'],
                        'status' => 0), 200);
            }
            //exit;


        }

    }
    /* check social loign information */
    function social_login()
    {
        $input_method = $this->webservices_inputs();
        $this->validate_param('social-login', $input_method);
        $user_data = $this->Customer_model_test->check_exist_email($input_method);
        //print_r($user_data);
        if (sizeof($user_data))
        {
            $user_update = $this->Customer_model_test->update_user($input_method);
            $user_data = $this->Customer_model_test->get_user_detail($input_method);
          
            if ($user_data > 0)
            {
                foreach ($user_data as $user_detail)
                {
                    //priyanka
                    $is_registered = "Y";
                    $credit_card = '';
                    // $credit_card=$user_detail->credit_card_no;
                    //$cvv=$user_detail->cvv_no;
                    //$expire_date=$user_detail->expire_date;
                    if (0 == '')
                    {
                        $iscreditcard = 'N';
                    }
                    else
                    {
                        $iscreditcard = 'Y';
                    }

                    $customer = array(

                        "customer_id" => $userid = $user_detail->id,
                        "name" => $name = $user_detail->name,
                        "email" => $email = $user_detail->email,
                        "dob" => $dob = $user_detail->dob,
                        "sid" => $sid = $user_detail->s_id,
                        "phone" => $phone = $user_detail->phone,
                        "credit_card" => $user_detail->credit_card_no, //$credit_card=$user_detail->credit_card_no,
                        //"cvv" => $user_data->cvv_no, //$cvv=$user_detail->cvv_no,
                        //"expire_date" => , //$expire_date=$user_detail->expire_date
                        );
                }
                $this->response(array(
                    'customer' => $customer,
                    'is_registered' => $is_registered,
                    'status' => 1), 200);
            }
            else
            {
                
            }


        }
        else
        {
            $customer = array(
                "name" => $input_method['name'],
                "dob" => $input_method['dob'],
                "email" => $input_method['email'],
                "s_id" => $input_method['s_id']);
                
            
             
            $this->response(array(
                'customer' => $customer,
                'is_registered' => 'N',
                'status' => 1), 200);

        }
    }
    /* enter credit crad infomation */
    function credit_card_info()
    {
        $input_method = $this->webservices_inputs();
        $this->validate_param('credit_card', $input_method);
        $card_data = $this->Customer_model->card_detail($input_method);
        if (sizeof($card_data) > 0)
        {
            $this->response(array('message' => 'success', 'status' => 1), 200);
        }
        else
        {
            $this->response(array('message' => 'something went wrong', 'status' => 1), 200);
        }
    }

    function user_login()
    {
        $input_method = $this->webservices_inputs();
        $this->validate_param('user-login', $input_method);
        $user_data = $this->Customer_model->check_exist_email($input_method);
        if (sizeof($user_data) > 0)
        {


            $user_data = $this->Customer_model->check_login($input_method);
            if (sizeof($user_data) > 0)
            {

                // update the user device token
                foreach ($user_data as $login)
                {

                    $customer = array(

                        "customer_id" => $userid = $login->id,
                        "name" => $name = $login->name,
                        "email" => $email = $login->email,
                        "dob" => $dob = $login->dob,
                        "phone" => $phone = $login->phone);


                }
                $this->Customer_model->update_device($input_method);


                $this->response(array(
                    'customer' => $customer,
                    'message' => 'success',
                    'status' => 1), 200);
            }
            else
            {
                $this->response(array('message' => 'sorry invalid email or password ', 'status' =>
                        0), 200);
            }


        }
        else
        {
            $this->response(array('message' =>
                    'sorry no such a email exist, Please check your email again ', 'status' => 0),
                200);
        }
    }


    function forget_password()
    {
        date_default_timezone_set('UTC');
        $input_method = $this->webservices_inputs();
        $this->validate_param('forget-password', $input_method);
        $user_data = $this->Customer_model->check_exist_email($input_method);
        if (sizeof($user_data) > 0)
        {
            $login_type = $user_data[0]->login_type;
            if ($login_type == 'S')
            {

                if ($user_data[0]->login_with == 'F')
                    $social_login = 'Facebook';
                if ($user_data[0]->login_with == 'G')
                    $social_login = 'Google+';
                if ($user_data[0]->login_with == 'T')
                    $social_login = 'Twitter';
                $this->response(array('message' =>
                        "Oops! Its seems like you have previously logged in with your " . $social_login .
                        " account", 'status' => 0), 200);
            }
            else
            {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = true;
                $this->email->initialize($config);
                $this->email->from('info@privatedriverapp.no', 'Private Driver ');
                $this->email->to($user_data[0]->email);
                $this->email->subject('Forgot Password');
                $this->email->message('Your password is :- ' . $user_data[0]->password);
                if ($this->email->send())
                {
                    $this->response(array('message' =>
                            'We send your password on your email. Please check your mail', 'status' => 1),
                        200);
                }
                else
                {
                    $this->response(array('message' => 'Sorry something went wrong', 'status' => 0),
                        200);
                }

            }
        }
        else
        {
            $this->response(array('message' =>
                    'sorry no such a email exist, Please check your email again ', 'status' => 0),
                200);
        }
    }


    function getuser_detail()
    {
        $input_method = $this->webservices_inputs();
        $this->validate_param('getuser-detail', $input_method);
        $user_data = $this->Customer_model->get_user($input_method['user_id']);
        if ($user_data[0]->profile_pic != "")
            $user_data[0]->profile_pic = base_url() . '/images/user_profile/' . $user_data[0]->
                profile_pic;
        $this->response(array('data' => $user_data[0], 'status' => 1), 200);
    }

    /*register company */

    function company_register()
    {
        $input_method = $this->webservices_inputs();
        $this->validate_param('company-register', $input_method);
        $check_exisitng = $this->Customer_model->check_exist_email($input_method);

        if (sizeof($check_exisitng) <= 0)
        {


            $company_id = $this->Customer_model->save_userinfo_company($input_method);
            if ($company_id)
            {
                $user_name = 'user-' . $company_id;
                $password = $input_method['password'];


                $name = $input_method['name'];
                $email = $input_method['email'];

                $param = array(
                    $user_name,
                    $password,
                    $name,
                    $email);
                $this->xmpp->api("add", $param);


                $this->response(array('message' =>
                        'Your account is created successfull.Need admin approval', 'status' => "1"), 200);
            }
        }
        else
        {
            $this->response(array('message' => 'This company already registered with us ',
                    'status' => "0"), 200);
        }
    }

    /*****************Fare Estimate  *********************************/

    function fare_estimate()
    {
        $this->load->model('Booking_model');
        $input_method = $this->webservices_inputs();
        $this->validate_param('fare-estimate', $input_method);
        $cab_type = $input_method['cab_type'];
        $origins = $input_method['pickup_latitude'] . ',' . $input_method['pickup_longitude'];
        $destination = $input_method['destination_latitude'] . ',' . $input_method['destination_longitude'];
        $time_detail = gettimedetail($origins, $destination);
        $pickup_location = getaddress($input_method['pickup_latitude'], $input_method['pickup_longitude']);
        $destination_loaction = getaddress($input_method['destination_latitude'], $input_method['destination_longitude']);
        $fare_detail = $this->Booking_model->getfare_detail($cab_type);

        if ($time_detail)
        {
            $duration = $time_detail['duration'];
            $distance = $time_detail['distance'];
            $duration_sec = $time_detail['duration_sec'];
        }
        $fare_detail = $fare_detail[0];
        if ($distance > $fare_detail['min_km'])
        {
            $remaining_km = $distance - $fare_detail['min_km'];
            $trip_charge = $remaining_km * $fare_detail['charges_per_km'];
            $trip_charge = $fare_detail['min_charges'] + $trip_charge;
        }
        else
        {
            $trip_charge = $fare_detail['min_charges'];
        }
        $waiting_time = ($duration_sec) * (1 / 10);
        if ($waiting_time > 0)
        {
            $waiting_time_charge = $waiting_time * ($fare_detail['wait_time_charges'] / 60);
        }
        else
        {
            $waiting_time_charge = 0;
        }
        $billing_charge = $trip_charge + $waiting_time_charge;
        $this->response(array(
            "estimate_bill" => $billing_charge,
            "duration" => $duration,
            "distance" => $distance,
            "pickup_location" => $pickup_location,
            "destination_location" => $destination_loaction,
            'status' => "1"), 200);


    }
    //************************Last Panding Invoice Detail******************
    function invoice_detail()
    {
        $input_method = $this->webservices_inputs();
        $this->validate_param('invoice-detail', $input_method);
        $invoice = $this->Customer_model->invoice_detail($input_method);
        // echo "<pre>";
        // print_r($invoice);
        if ($invoice)
        {
            $invoice = $invoice[0];
            $booking_id = $invoice['id'];
            $CBN = $invoice['booking_number'];
            $pickup_latitude = $invoice['pickup_latitude'];
            $pickup_longitude = $invoice['pickup_longitude'];
            $pickup_location = getaddress($pickup_latitude, $pickup_longitude);
            $destination_latitude = $invoice['destination_latitude'];
            $destination_longitude = $invoice['destination_longitude'];
            $destination_location = getaddress($destination_latitude, $destination_longitude);
            $km_driven = $invoice['km_driven'];
            $journey_time = $invoice['journey_time'];
            $waiting_time = $invoice['waiting_time'];
            $waiting_charge = $invoice['waiting_charge'];
            $billing_charge = $invoice['billing_charge'];
            $othercharge = "0.0";
            $jid = makejid($invoice['user_id'], "user");
            $travel_charge = $billing_charge - $waiting_charge - $othercharge;
            $other_item[] = array(
                "item" => "pizza",
                "price" => "250",
                "qty" => 2);
            $this->response(array(
                'booking_id' => $booking_id,
                "CBN" => $CBN,
                "pickup_latitude" => $pickup_latitude,
                "pickup_longitude" => $pickup_longitude,
                "pickup_location" => $pickup_location,
                "destination_latitude" => $destination_latitude,
                "destination_latitude" => $destination_latitude,
                "destination_longitude" => $destination_longitude,
                "destination_location" => $destination_location,
                "km_driven" => $km_driven,
                "waiting_time" => $waiting_time,
                "waiting_change" => $waiting_charge,
                "billing_charge" => $billing_charge,
                "journey_time" => $journey_time,
                "customet_jid" => $jid,
                "travel_charge" => $travel_charge,
                "other_charge" => $othercharge,
                "other_charge_detail" => $other_item,
                'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => NO_INVOICE, 'status' => "0"), 200);
        }

    }
    
    function device_register()
    {
         $input_method = $this->webservices_inputs();
        $this->validate_param('device-register', $input_method);
        $device = $this->Customer_model_test->device_register($input_method);
        if($device)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
             $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }
    }


}
