<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- <!-- Content Wrapper. Contains page content -->
      <!-- <div class="content-wrapper"> -->
        <!-- Content Header (Page header) -->
      

        <!-- Main content -->
        <!-- <section class="content">
          <div class="maincontent">
            <div class="maincontentinner"> -->
        
                  
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              
             
            </div>
            <div class="box-body record">
              
                 <center><img id="loading-image1" src="http://dev.privatedriverapp.com/app/images/loading.gif" style="display:none;" height="100x" width="100px"/>
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        
                        <th>Booking number</th>
                        <th>Pickup Location</th>
                         <th>destination Location</th>
                         <th>Booking Time</th>
                        <th>Driver</th>
                         <th>Status</th>
                         <th>Amount</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                       <?php 
                      //print_r($Bookingdata);
                       if($Bookingdata){
                       	$total=0;
                       foreach($Bookingdata as $booking) {
                       	if($booking['status']=='FINISH'){ 
                       	 $total=$booking['user_paid']+$total;
						}
						   if($booking['is_prebooking']=='Y')
						   {
						   	$booking_time= date("d-m-Y H:i",strtotime($booking['booking_time']));
						   }else
						   {
						   	$booking_time= gmt_to_norway($booking['booking_time']);
							$booking_time= date("d-m-Y H:i ",strtotime($booking_time));
						   }
                       	?>
                       	<tr>
                       		
                       		   <td><a href="javascript:void(0);" onclick="customer_detail('<?php echo $booking['id'];?>');"><?php echo $booking['booking_number'];?></a></td>
                       		<td><?php echo $booking['pickup_location'] ?></td>
                       		<td><?php echo $booking['destination_location'] ?></td>
                       		<td><?php echo $booking_time ?></td>
                       		<td><?php echo $booking['name'] ?></td>
                       		 <td><?php echo ucwords(str_replace("_", " ", $booking['status'])) ?></td> 
                       		<td><?php echo $booking['user_paid']; ?></td>
                       	</tr>
                       	
                       <?php } }
					   ?>                 
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  <?php if($total)?>
                  <div style="float: right" ><span colspan="4" style="color: #01FF70;text-align: right ">TurnOver   </span><span colspan="2"><?php echo $total ?>  NOK</span></div>
               <?php ?>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
    
     
      
 <!--pop up -->
 <!-- open customer pop up -->
<div class="modal fade" id="open_customer" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
               Booking Details
                <button style="color:black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
               <div id="showreport_data" name="showreport"></div>
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
      
     <!-- <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>-->
           <script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script> 
     <script>

	var dataTable = $('#example2').DataTable()

	//var dataTable = $('#example2').datatable()

   function customer_detail(booking_no)
        {
      
       $.ajax({
            type:'POST',
            url: '<?php echo site_url('Booking/get_all_booking');?>',
            data:{"booking_no":booking_no},
             beforeSend: function() {
              $("#loading-image").show();
           },
            success: function(response) {
            //alert(response);
             $("#showreport_data").html(response);
             $('#open_customer').modal({
                 "backdrop" : "static"
         })
              $("#loading-image").hide();
              

             }});
             }
</script>
 
 