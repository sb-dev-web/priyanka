<?php

	defined('BASEPATH') or exit('No direct script access allowed');
	// This can be removed if you use __autoload() in config.php OR use Modular
	// Extensions
	require APPPATH . '/libraries/REST_Controller.php';
	//error_reporting(0);
	class Webbooking extends REST_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> library(array(
				'upload',
				'Twilio',
				'email'
			));
			$this -> load -> library('xmpp', false);
			$this -> load -> helper(array(
				'form',
				'url',
				'string',
				'constants_helper',
				'function_helper',
				'braintree_function'
			));

			$this -> load -> model(array(
				'V3/Webbooking_model',
				
			));

		}
		
		
		/* register user */
		function prebooking()
		{
			
			$input_method = $this -> webservices_inputs();
			$auth = getallheaders();
			$apikey = $auth['Apikey'];
			if ($apikey != APIKEY)
			{
				$this -> response(array(
					'message' => AUTHERROR,
					'status' => "0"
				), 200);
			}
			
			$this -> param_validate($input_method, array(
				//"name",
				//email",
				//country_code",
				//"phone",
				"auth_token",
				"pickup_latitude",
				"pickup_longitude",
				"destination_latitude",
				"destination_longitude",
				"pickup_location",
				"destination_location",
				"booking_time",
				"cab_type",
				"billing_amount"
			));
			
			
			//check the user exist or not 
			$userdetail =$this->Webbooking_model->GetUser($input_method['auth_token']);
			if(!$userdetail)
			{
				$this -> response(array(
					'message' => USER_NOT_ACTIVE,
					'status' => "0"
				), 200);
			}
			// check user is exist or not
            $input_method['user_id']=$userdetail['id'];//this is guest user
            $input_method['country_code']=str_replace("+", "", $input_method['country_code']);
			$input_method['country_code']="+".$input_method['country_code'];
			//check the booking time 
			if(strtotime($input_method['booking_time']) <= strtotime(gmt_to_norway(get_gmt_time())))
			{
				$this -> response(array(
					'message' => PREBOOKINGTIME,
					'status' => "0"
				), 200);
			}
			
			$web_booking = $this -> Webbooking_model -> pre_booking($input_method);
			if($web_booking)
			{
                normal_prebooking_mail_to_admin($web_booking, 'booking');
				$this -> response(array(
					'message' => WEBBOOKING,
					"booking_id"=>	$CBN = "PD" . sprintf("%05d", $web_booking),
					'status' => "1"
				), 200);
				
			}else
				{
					$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
				}
		}
		
		function CabHourlyRate()
		{
			$input_method = $this -> webservices_inputs();
			$auth = getallheaders();
			$apikey = $auth['Apikey'];
			if ($apikey != APIKEY)
			{
				$this -> response(array(
					'message' => AUTHERROR,
					'status' => "0"
				), 200);
			}
			$this -> param_validate($input_method, array(
				"cab_type",//1 for First class 3 for luxury
				"hour",
				
			));
			$cab_type=$input_method['cab_type'];
			$hour=$input_method['hour'];
			$CabRate =$this->Webbooking_model->CabHireRate($cab_type);
			if($CabRate)
			{
				
				$hour_rate=$CabRate['price'];
				$total_price=$CabRate['price']*$hour;
				$total_price=number_format($total_price,2,'.',',');
				$this -> response(array(
					'message' => SUCCESS,
					"cab_type"=>$cab_type,
					
					"hourly_price"=>$hour_rate,
					"hour"=>$hour,
					"total_price"=>$total_price,
					'status' => "1"
				), 200);
				
				
			}else
				{
					$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
				}
		}
		
		function CabHire()
		{
			$input_method = $this -> webservices_inputs();
			$auth = getallheaders();
			$apikey = $auth['Apikey'];
			if ($apikey != APIKEY)
			{
				$this -> response(array(
					'message' => AUTHERROR,
					'status' => "0"
				), 200);
			}
			$this -> param_validate($input_method, array(
				"name",
				"email",
				"country_code",
				"phone",
				"pickup_latitude",
				"pickup_longitude",
				"destination_latitude",
				"destination_longitude",
				"pickup_location",
				"destination_location",
				"booking_time",
				"cab_type",
				"billing_amount",
				"hour"
			));
			
			 $input_method['user_id']=1;//this is guest user
            $input_method['country_code']=str_replace("+", "", $input_method['country_code']);
			$input_method['country_code']="+".$input_method['country_code'];
			//check the booking time 
			if(strtotime($input_method['booking_time']) <= strtotime(gmt_to_norway(get_gmt_time())))
			{
				$this -> response(array(
					'message' => PREBOOKINGTIME,
					'status' => "0"
				), 200);
			}
			
			$hourly_booking = $this -> Webbooking_model -> HourlyBooking($input_method);
			if($hourly_booking)
			{
				 normal_prebooking_mail_to_admin($hourly_booking, 'booking');
				$this -> response(array(
					'message' => WEBBOOKING,
					"booking_id"=>	$CBN = "PD" . sprintf("%05d", $hourly_booking),
					'status' => "1"
				), 200);
				
			}else
				{
					$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
				}
			}
			
	
	
	}
