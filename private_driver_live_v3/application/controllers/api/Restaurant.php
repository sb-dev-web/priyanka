<?php

	defined('BASEPATH') or exit('No direct script access allowed');
	// This can be removed if you use __autoload() in config.php OR use Modular
	// Extensions
	require APPPATH . '/libraries/REST_Controller.php';
	error_reporting(0);
	class Restaurant extends REST_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> library(array(
				'upload',
				'Braintree_lib',
				'Twilio',
				'email'
			));
			$this -> load -> library('xmpp', false);
			$this -> load -> helper(array(
				'form',
				'url',
				'string',
				'function_helper',
				'constants_helper' // 'braintree_function'
			));

			$this -> load -> model('Restaurantapi_model');
			//
			$this -> load -> model('Bookingapi_model');
		}

		/* enter credit crad infomation */

		function restaurant_login()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('user-login', $input_method);
			$this -> param_validate($input_method, array(
				"email",
				"password"
			));

			$user_data = $this -> Restaurantapi_model -> check_exist_email($input_method);
			if ($user_data > 0)
			{
				$user_data = $this -> Restaurantapi_model -> check_login($input_method);
				if (!empty($user_data))
				{
					// update the user device token
					foreach ($user_data as $login)
					{
						if ($login -> isactive == "N")
						{
							$this -> response(array(
								'message' => NOT_APPROVE,
								'status' => "0"
							), 200);
						}
						//if($today)
						$restaurant = array(
							"restaurant_id" => $restaurant_id = $login['id'],
							"name" => $name = $login['name'],
							"j_id" => $j_id = 'restaurant-' . $login['id'],
							"address" => $address = $login['address'],
							"latitude" => $latitude = $login['latitude'],
							"longitude" => $longitude = $login['longitude'],
							"location" => $login['address'],
							"contact_number" => $login['contact_number']
						);
					}
					//$this->Customer_model->update_device($input_method);
					$this -> response(array(
						'restaurant' => $restaurant,
						'message' => SUCCESS,
						'status' => "1"
					), 200);
				}
				else
				{
					$this -> response(array(
						'message' => CUSTOMER_LOGIN_ERROR,
						'status' => "0"
					), 200);
				}
			}
			else
			{
				$this -> response(array(
					'message' => EMAIL_NOT_EXIST,
					'status' => "0"
				), 200);
			}
		}

		function nearby_search()
		{
			/* check that user is already register or not */
			$input_method = $this -> webservices_inputs();

			// $this->validate_param('nearby-search', $input_method);
			$this -> param_validate($input_method, array(
				"pickup_latitude",
				"pickup_longitude",
				"restro_id",
				"cab_type"
			));
			// check user is exist or not
			$user_exist = $this -> Restaurantapi_model -> restro_exist($input_method['restro_id']);
			if (!$user_exist)
			{
				$this -> response(array(
					'status' => "0",
					"message" => RESTRO_NOT_EXIST
				), 200);
				exit ;
			}

			$cab_detail = $this -> Restaurantapi_model -> nearby_booking_serach($input_method);
			//print_r($cab_detail);
			
			if ($cab_detail)
			{
				
					/*foreach ($cab_detail['driver_info'] as $data)
					 {
					 $profile_image = ($data['profile_pic'] == '') ? base_url() .
					DRIVER_DEFAULT_IMAGE : base_url() . $data['profile_pic'];
					 $origins = $data['latitude'] . ',' . $data['longitude'];
					 $destination = $input_method['pickup_latitude'] . ',' .
					$input_method['pickup_longitude'];
					 $time_detail = gettimedetail($origins, $destination);
					 $booking_id = $driver_detail['id'];
					 if ($time_detail)
					 {
					 $duration = $time_detail['duration'];
					 $distance = $time_detail['distance'];
					 $duration_sec = $time_detail['duration_sec'];
					 }
					 else
					 {
					 $duration = "";
					 $distance = $data['distance'];
					 }
					 //$gettime_detail = gettimedetail($ori);
					 $cab_data[] = array(
					 "driver_id" => $data['driver_id'],
					 "name" => $data['name'],
					 "contactno" => $data['contact'],
					 "email" => $data['email_id'],
					 "profile_image" => $profile_image,
					 "distance" => $distance,
					 "status" => $data['status'],
					 "latitude" => $data['latitude'],
					 "longitude" => $data['longitude'],
					 "cab_type" => $data['title'],
					 "car_number" => $data['cab_plate_no'],
					 "duration" => $duration,
					 "eta" => getETA($duration_sec),
					 );
					 }*/

					foreach ($cab_detail['driver_info'] as $data)
					{
						//print_r($data);
						$driver_id = $data['driver_id'];
						//GET DRIVER DISTANCE
						$origins = $data['latitude'] . ',' . $data['longitude'];
						$current_booking = $this -> Bookingapi_model -> getdriver_currentbooking($driver_id);
						//print_r($current_booking);
						$duration = $distance = "0";
						if ($current_booking)
						{

							for ($i = 0; $i < count($current_booking); $i++)
							{
								$pickup_origin = $current_booking[$i]['pickup_latitude'] . ',' . $current_booking[$i]['pickup_longitude'];
								$pickup_location = $current_booking[$i]['pickup_location'];
								$destination_origin = $current_booking[$i]['destination_latitude'] . ',' . $current_booking[$i]['destination_longitude'];
								$destination_location = $current_booking[$i]['destination_location'];
								$book_id = $current_booking[$i]['id'];
								if ($booking_status == 'TRIP_STARTED')
								{
									$time_detail = gettimedetail($origins, $destination_origin, 'now', "", "", $booking_id);
									$distance = $distance + $time_detail['distance_m'];
									$duration = $duration + $time_detail['duration_sec'];
								}
								else
								{
									$time_detail = gettimedetail($origins, $pickup_origin, 'now', "", "", $booking_id);
									$distance = $distance + $time_detail['distance_m'];
									$duration = $duration + $time_detail['duration_sec'];

									//current pickup to current booking (driver running booking) destination
									$time_detail = gettimedetail($pickup_origin, $destination_origin, 'now', "", "", $booking_id);
									// time should be distance + now
									$distance = $distance + $time_detail['distance_m'];
									$duration = $duration + $time_detail['duration_sec'];
									//driver new booking pickup location

								}
								$origins = $destination_origin;
							}
						}
						$newBookingPickupLocation = $input_method['pickup_latitude'] . ',' . $input_method['pickup_longitude'];
						$time_detail = gettimedetail($origins, $newBookingPickupLocation, 'now', "", "", $booking_id);
						$distance = $distance + $time_detail['distance_m'];
						$duration = $duration + $time_detail['duration_sec'];
						$duration_sec = $duration;
						if ($duration)
						{
							$duration = format_time($duration);
							$distance = $distance / 1000;
						}
						else
						{
							$time_detail = gettimedetail($origins, $newBookingPickupLocation);
							if ($time_detail)
							{
								$duration = $time_detail['duration_sec'];
								$distance = $time_detail['distance'];
								$duration = format_time($duration);
								$distance = $distance / 1000;
								$duration_sec = $duration;
							}
							else
							{
								$duration = "";
								$distance = "";
							}
						}
						$data['distance_google'] =  $distance;
						$data['duration_google'] = ($duration=="")? "1 Min" : $duration ;

						//DISTANCE CODE END HERE
						$profile_image = ($data['profile_pic'] == '') ? base_url() . DRIVER_DEFAULT_IMAGE : base_url() . $data['profile_pic'];
						// $driver_jid = 'driver-' . $data['driver_id'] . '@privatedriverapp.no';
						$driver_jid = makejid($data['driver_id'], DRIVER_JID_NAME);
						$driver_info[] = array(
							"driver_id" => $data['driver_id'],
							"name" => $data['name'],
							"contactno" => $data['contact'],
							"email" => $data['email_id'],
							"profile_image" => $profile_image,
							"distance" => $data['distance'],
							"status" => $data['status'],
							"latitude" => $data['latitude'],
							"longitude" => $data['longitude'],

							"cab_type" => $data['title'],
							"distance_linear" => $data['distance'],
							"distance" => $data['distance_google'],
							"duration_google" => $data['duration_google'],
							"car_number" => $data['cab_plate_no'],
							"duration" => $data['duration_google'],
							"eta" => getETA($duration_sec),
						);

					}
                  usort($driver_info, distance);
					$this -> response(array(
						'status' => 1,
						"booking_type" => "restro",
						"cab_data" => $driver_info
					), 200);
					// print_r($cap_detail);
				
			}
			else
			{
				$cap_data = array();
				$jid = makejid($input_method['user_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => CAB_NOT_EXIST
				), 200);
			}

		}

		function restaurant_booking_request()
		{
			/* check that user is already register or not */
			// $input_method = $this->webservices_inputs();
			//$this->validate_param('nearby-booking-search', $input_method);
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);
			$this -> param_validate($input_method, array(
				"pickup_latitude",
				"pickup_longitude",
				"restro_id",
				"cab_type",
				"pickup_location",
				//"client_email",
				"client_name",
				"client_contact_number",
				"country_code"
			));

			//insert into booking table
			$user_exist = $this -> Restaurantapi_model -> restro_exist($input_method['restro_id']);
			if (!$user_exist)
			{
				$this -> response(array(
					'status' => "0",
					"message" => RESTRO_NOT_EXIST
				), 200);
				exit ;
			}

			$result = $this -> Restaurantapi_model -> nearby_booking_serach($input_method);
			if (!$result)
			{
				//send sms to client number No cab found
				$client_phone = "+" . $input_method['country_code'] . $input_method['client_contact_number'];
				send_sms($client_phone, CAB_NOT_EXIST_SMS);
				$jid = makejid($input_method['restro_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => DRIVER_BUSY_MESSAGE
				), 200);
				exit ;
			}
			$cab_detail = $result['driver_info'];
			if (count($cab_detail) == 0)
			{
				//send sms driver busyy
				$client_phone = "+" . $input_method['country_code'] . $input_method['client_contact_number'];
				// send_sms($client_phone, DRIVER_NOT_ACCEPT);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => DRIVER_BUSY_MESSAGE
				), 200);
				exit ;
			}

			$booking_id = $this -> Restaurantapi_model -> restro_cab_booking($input_method);
			// check user is exist or not
			if (!$booking_id)
			{
				$jid = makejid($input_method['restro_id'], RESTRO_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => SERVER_ERROR
				), 200);
				exit ;
			}
			$booking_detail = $this -> Restaurantapi_model -> get_booking_detail($booking_id);

			$booking_detail = $booking_detail[0];
			// echo "<pre>";
			// print_r($booking_detail);
			$restro_id = $booking_detail['restro_id'];
			$restro_name = $booking_detail['name'];
			$restro_address = $booking_detail['address'];
			$restro_phone = $booking_detail['contact_number'];
			$lat = $booking_detail['latitude'];
			$lng = $booking_detail['longitude'];
			$client_country_code = $booking_detail['country_code'];
			$client_contact_number = $booking_detail['client_contact_number'];
			$client_name = $booking_detail['client_name'];
			$cab_type = $booking_detail['cab_type'];
			$booking_id = $booking_detail['booking_id'];
			$CBN = $booking_detail['booking_number'];
			$jid = makejid($input_method['restro_id'], RESTRO_JID_NAME);
			$client_phone = "+" . $input_method['country_code'] . $input_method['client_contact_number'];

			// $sms = BOOKINGSMS . " $booking_id";
			// send_sms($client_phone, $sms);

			$message = array(
				"restro_id" => $restro_id,
				"restro_name" => $restro_name,
				"restro_contact_number" => $restro_phone,
				"restro_address" => $restro_address,
				"CBN" => $CBN,
				"pick_up_latitude" => $lat,
				"pick_up_longitude" => $lng,
				"pick_up_location" => $restro_address,
				"client_country_code" => $client_country_code,
				"client_conatct_number" => $client_contact_number,
				"client_name" => $client_name,
				"cab_type" => $cab_type,
				"jid" => $jid,
				"booking_type" => "restro",
				"booking_id" => $booking_id,
			);

			//usort($cap_detail, array($this, 'distance'));
			if (count($cab_detail))
			{
				/*foreach ($cab_detail as $data)
				{
					$origins = $data['latitude'] . ',' . $data['longitude'];
					$destination = $input_method['pickup_latitude'] . ',' . $input_method['pickup_longitude'];
					$time_detail = gettimedetail($origins, $destination);
					$booking_id = $driver_detail['id'];
					if ($time_detail)
					{
						$duration = $time_detail['duration'];
						$distance = $time_detail['distance'];
					}
					else
					{
						$duration = "";
						$distance = $data['distance'];
					}
					$profile_image = ($data['profile_pic'] == '') ? base_url() . DRIVER_DEFAULT_IMAGE : base_url() . $data['profile_pic'];
					// $driver_jid = 'driver-' . $data['driver_id'] . '@privatedriverapp.no';
					$driver_jid = makejid($data['driver_id'], DRIVER_JID_NAME);
					$driver_info[] = array(
						"driver_id" => $data['driver_id'],
						"name" => $data['name'],
						"contactno" => $data['contact'],
						"email" => $data['email_id'],
						"profile_image" => $profile_image,
						"distance" => $distance,
						"duration" => $duration,
						"eta" => getETA($duration_sec),
						"status" => $data['status'],
						"latitude" => $data['latitude'],
						"longitude" => $data['longitude'],
						"jid" => $driver_jid,
						"cab_type" => $data['title'],
						"car_number" => $data['cab_plate_no']
					);

				}*/
				
					foreach ($cab_detail as $data)
					{
						$driver_id = $data['driver_id'];
						//GET DRIVER DISTANCE
						$origins = $data['latitude'] . ',' . $data['longitude'];
						$current_booking = $this -> Bookingapi_model -> getdriver_currentbooking($driver_id);
						$duration = $distance = "0";
						if ($current_booking)
						{

							for ($i = 0; $i < count($current_booking); $i++)
							{
								$pickup_origin = $current_booking[$i]['pickup_latitude'] . ',' . $current_booking[$i]['pickup_longitude'];
								$pickup_location = $current_booking[$i]['pickup_location'];
								$destination_origin = $current_booking[$i]['destination_latitude'] . ',' . $current_booking[$i]['destination_longitude'];
								$destination_location = $current_booking[$i]['destination_location'];
								$book_id = $current_booking[$i]['id'];
								if ($booking_status == 'TRIP_STARTED')
								{
									$time_detail = gettimedetail($origins, $destination_origin, 'now', "", "", $booking_id);
									$distance = $distance + $time_detail['distance_m'];
									$duration = $duration + $time_detail['duration_sec'];
								}
								else
								{
									$time_detail = gettimedetail($origins, $pickup_origin, 'now', "", "", $booking_id);
									$distance = $distance + $time_detail['distance_m'];
									$duration = $duration + $time_detail['duration_sec'];

									//current pickup to current booking (driver running booking) destination
									$time_detail = gettimedetail($pickup_origin, $destination_origin, 'now', "", "", $booking_id);
									// time should be distance + now
									$distance = $distance + $time_detail['distance_m'];
									$duration = $duration + $time_detail['duration_sec'];
									//driver new booking pickup location

								}
								$origins = $destination_origin;
							}
						}
						$newBookingPickupLocation = $input_method['pickup_latitude'] . ',' . $input_method['pickup_longitude'];
						$time_detail = gettimedetail($origins, $newBookingPickupLocation, 'now', "", "", $booking_id);
						$distance = $distance + $time_detail['distance_m'];
						$duration = $duration + $time_detail['duration_sec'];
						$duration_sec = $duration;
						if ($duration)
						{
							$duration = format_time($duration);
							$distance = $distance / 1000;
						}
						else
						{
							$time_detail = gettimedetail($origins, $newBookingPickupLocation);
							if ($time_detail)
							{
								$duration = $time_detail['duration_sec'];
								$distance = $time_detail['distance'];
								$duration = format_time($duration);
								$distance = $distance / 1000;
								$duration_sec = $duration;
							}
							else
							{
								$duration = "";
								$distance = "";
							}
						}
						$data['distance_google'] =  $distance;
						$data['duration_google'] = ($duration=="")? "1 Min" : $duration ;

						//DISTANCE CODE END HERE
						$profile_image = ($data['profile_pic'] == '') ? base_url() . DRIVER_DEFAULT_IMAGE : base_url() . $data['profile_pic'];
						// $driver_jid = 'driver-' . $data['driver_id'] . '@privatedriverapp.no';
						$driver_jid = makejid($data['driver_id'], DRIVER_JID_NAME);
						$driver_info[] = array(
							"driver_id" => $data['driver_id'],
							"name" => $data['name'],
							"contactno" => $data['contact'],
							"email" => $data['email_id'],
							"profile_image" => $profile_image,
							"distance" => $data['distance'],
							"status" => $data['status'],
							"latitude" => $data['latitude'],
							"longitude" => $data['longitude'],
                             "jid" => $driver_jid,
							"cab_type" => $data['title'],
							"distance_linear" => $data['distance'],
							"distance" => $data['distance_google'],
							"duration_google" => $data['duration_google'],
							"car_number" => $data['cab_plate_no'],
							"duration" => $data['duration_google'],
							"eta" => getETA($duration_sec),
						);

					}
             usort($driver_info, distance);
			  $driver_waiting_time = DRIVERWAITINGTIME + 5;
				$this -> response(array(
					'status' => "1",
					"message" => "success",
					'driver_waiting_time' => $driver_waiting_time,
					"driver_info" => $driver_info,
					"booking_detail" => $message
				), 200);
				// print_r($cap_detail);
			}
			else
			{
				// $cab_data = array();
				$jid = makejid($input_method['user_id'], USER_JID_NAME);
				$this -> response(array(
					'status' => "0",
					"jid" => $jid,
					"message" => DRIVER_BUSY_MESSAGE
				), 200);
			}

		}

		function restaurant_booking_status()
		{
			//$input_method = $this->webservices_inputs();
			// $this->validate_param('booking-status', $input_method);
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id"
			));
			$result = $this -> Restaurantapi_model -> booking_status($input_method);
			//print_r($result);
			//exit;
			if ($result)
			{
				$this -> response(array(
					'status' => "1",
					"booking_status" => $result
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"booking_status" => $result,
					"message" => BOOKING_CANCEL
				), 200);
			}
		}

		function restaurant_booking_timeout()
		{
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);
			$this -> param_validate($input_method, array("booking_id"));
			$result = $this -> Restaurantapi_model -> get_booking_detail($input_method['booking_id']);
			if ($result)
			{
				$client_data = $result[0];
				$client_phone = $client_data['country_code'] . $client_data['client_contact_number'];
				$result = $this -> Restaurantapi_model -> timeout($input_method['booking_id']);
				// send_sms($client_phone, DRIVER_NOT_ACCEPT);
				$this -> response(array(
					'status' => "1",
					"message" => DRIVER_NOT_ACCEPT_BOOKING,
					"customer_jid" => makejid($client_data['restro_id'], RESTRO_JID_NAME)
				), 200);

			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => BOOKING_NOTFOUND
				), 200);
			}

		}

		function restaurant_driver_response()
		{
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);
			// $input_method = $this->webservices_inputs();
			//$this->validate_param('driver-response', $input_method);
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id",
				"type"
			));
			$result = $this -> Restaurantapi_model -> driver_response($input_method);
			//   print_r($result);
			if ($result)
			{
				if ($result['status'] == "1" && $result['type'] == 'accept')
				{
					$driver_detail = $result['driver_detail'][0];
					$first_name = $driver_detail['first_name'];
					$driver_image = $driver_detail['profile_pic'];
					$last_name = $driver_detail['last_name'];
					$contact = $driver_detail['contact'];
					$profile_image = $driver_detail['profile_pic'];
					$current_lat = $driver_detail['latitude'];
					$current_lng = $driver_detail['longitude'];
					$current_location = getaddress($current_lat, $current_lng);
					// $jid = $driver_detail['driver_id'] . '@privatedriverapp.no';
					$driver_id = $driver_detail['driver_id'];
					$user_id = $driver_detail['user_id'];
					$pickup_latitude = $driver_detail['pickup_latitude'];
					$pickup_longitude = $driver_detail['pickup_longitude'];
					$restro_id = $driver_detail['restro_id'];

					$car_number = $driver_detail['cab_plate_no'];
					$cab_type = $driver_detail['title'];
					$CBN = $driver_detail['booking_number'];
					$customer_phone = $driver_detail['country_code'] . $driver_detail['phone'];
					$origins = $current_lat . ',' . $current_lng;
					$destination = $pickup_latitude . ',' . $pickup_longitude;
					$time_detail = gettimedetail($origins, $destination);
					$booking_id = $driver_detail['id'];
					$client_country_code = $driver_detail['country_code'];
					$client_phone = $driver_detail['client_contact_number'];
					//  $client_email = $driver_detail['client_email'];
					$client_name = $driver_detail['client_name'];
					$pickup_loaction = $driver_detail['pickup_location'];

					if ($time_detail)
					{
						$duration = $time_detail['duration'];
						$distance = $time_detail['distance'];
						$duration_sec = $time_detail['duration_sec'];
					}
					else
					{
						$duration = "";
						$distance = "";
					}
					$phonenumber = $client_country_code . $client_phone;
					$message = CBN_NUMBER . " " . $CBN . " " . DRIVER_DETAIL . " " . $first_name . ' ' . $last_name . " " . $contact;
					$message .= "ETA" . $duration;
					//  send_sms($phonenumber, $message);
					$get_message = RESTRO_BOOKING_SMS;
					$msg_replace = array(
						"<booking_no>",
						"<driver_name>",
						"<driver_contact>"
					);
					$detail = array(
						$CBN,
						$first_name,
						$contact
					);
					//echo $phonenumber;
					$booking_sms = str_replace($msg_replace, $detail, $get_message);
					send_sms($phonenumber, $booking_sms);
					$this -> response(array(
						'status' => "1",
						"driver_id" => "$driver_id",
						"pickup_latitude" => $pickup_latitude,
						"pickup_longitude" => $pickup_longitude,
						"pickup_location" => $pickup_loaction,
						"client_country_code" => $client_country_code,
						"client_contact_number" => $client_phone,
						//"client_email" => $client_email,
						"client_name" => $client_name,
						"name" => $first_name . ' ' . $last_name,
						"contact" => $contact,
						"driver_image" => base_url() . DRIVER_IMAGE_PATH . $driver_image,
						"current_latitude" => $current_lat,
						"current_longitude" => $current_lng,
						"current_location" => $current_location,
						"distance" => ucwords($distance),
						"duration" => ucwords($duration),
						"eta" => getETA($duration_sec),
						"CBN" => $CBN,
						"booking_id" => $booking_id,
						"booking_type" => "restro",
						"cab_type" => $cab_type,
						"customer_jid" => makejid($restro_id, RESTRO_JID_NAME),
						"car_number" => $car_number
					), 200);
				}
				elseif ($result['status'] == "1" && $result['type'] == 'reject')
				{
					$this -> response(array(
						'status' => "1",
						"message" => $result['message'],
					), 200);
				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => $result['message'],
					), 200);
				}
			}

			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => $result['message']
				), 200);
			}
		}

		function general_info()
		{
			$result = $this -> Restaurantapi_model -> restro_fare_detail();
			if ($result)
			{

				//print_r($result);
				$this -> response(array(
					'status' => "1",
					"restro_fare_detail" => $result,
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => "Restro fare info",
				), 200);
			}
		}

		function driver_unavailable()
		{
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);
			$this -> param_validate($input_method, array(
				"driver_id",
				"type"
			));
			$driver_status = $this -> Restaurantapi_model -> driver_unavailable($input_method);
			if ($driver_status)
			{
				$this -> response(array(
					'message' => SUCCESS,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}
		}

		function customer_aware()
		{
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id"
			));
			$input_method['type'] = "FIVE_MIN_AWAY";
			$customer_aware = $this -> Restaurantapi_model -> customer_aware($input_method);

			if ($customer_aware)
			{
				//send SMS and push notification if require
				$customer_aware = $customer_aware[0];
				$customer_phone = $customer_aware['country_code'] . $customer_aware['client_contact_number'];
				$message = FIVEMINAWAY;
				send_sms($customer_phone, $message);

				//$driver_jid = makejid($customer_aware['driver_id'], DRIVER_JID_NAME);
				//$customer_jid = makejid($customer_aware['user_id'], USER_JID_NAME);
				$pickup_latitude = $customer_aware['pickup_latitude'];
				$pickup_longitude = $customer_aware['pickup_longitude'];
				$driver_latitude = $customer_aware['latitude'];
				$driver_longitude = $customer_aware['longitude'];
				$origins = $driver_latitude . ',' . $driver_longitude;
				$destination = $pickup_latitude . ',' . $pickup_longitude;
				$time_detail = gettimedetail($origins, $destination);

				if ($time_detail)
				{
					$duration = $time_detail['duration'];
					$distance = $time_detail['distance'];
					$duration_sec = $time_detail['duration_sec'];
				}
				else
				{
					$duration = "";
					$distance = "";
					$duration_sec = "300";
				}
				$ETA = getETA($duration_sec);
				$this -> response(array(
					'message' => CUSTOMER_AWARE . $customer_aware['booking_number'] . " booking",
					'status' => "1",
					'driver_id' => $customer_aware['driver_id'],
					'name' => $customer_aware['first_name'] . ' ' . $customer_aware['last_name'],
					'contactno' => $customer_aware['contact'],
					'email' => $customer_aware['email'],
					'profile_image' => base_url() . DRIVER_IMAGE_PATH . $customer_aware['profile_pic'],
					'distance' => $distance,
					'duration' => $duration,
					'est' => $ETA,
					'latitude' => $customer_aware['latitude'],
					'longitude' => $customer_aware['longitude'],
					'jid' => $driver_jid,
					'cab_type' => $customer_aware['title'],
					//'customer_jid' => $customer_jid,
					'cab_number' => $customer_aware['cab_plate_no'],
					'booking_id' => $customer_aware['id'],
					'CBN' => $customer_aware['booking_number'],
					'client_name' => $customer_aware['client_name'],
					'client_email' => $customer_aware['client_email'],
					'client_phone_number' => $customer_phone
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => BOOKING_NOTFOUND,
					'status' => "0"
				), 200);
			}
		}

		function driver_arrive()
		{
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);

			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id"
			));
			$input_method['type'] = "I_AM_HERE";
			$customer_aware = $this -> Restaurantapi_model -> customer_aware($input_method);
			// print_r($customer_aware);
			if ($customer_aware)
			{
				$customer_phone = $customer_aware[0]['country_code'] . $customer_aware[0]['client_contact_number'];
				$message = ARRIVE;
				send_sms($customer_phone, $message);

				//send SMS and push notification if require

				//   $customer_id = $customer_aware['user_id'];
				//   $customer_jid = makejid($customer_id, USER_JID_NAME);
				$this -> response(array(
					'message' => CABARRIVE,
					//  "customer_jid" => $customer_jid,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => BOOKING_NOTFOUND,
					'status' => "0"
				), 200);
			}
		}

		function Restaurant_trip_start()
		{
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id"
			));
			$result = $this -> Restaurantapi_model -> trip_start($input_method);
			if ($result['status'] == 1)
			{
				$phone = $result['client_country_code'] . $result['client_contact_number'];
				// send_sms($phone, TRIP_START_SMS);
				$this -> response(array(
					'status' => "1",
					"message" => "success"
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => $result['message']
				), 200);
			}

		}

		function Restaurant_trip_end()
		{
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id",
				"km_driven",
				// "waiting_time",
				"journey_time",
				"payment_type"
			));
			if ($input_method['payment_type'] == "FIX")
			{
				if (empty($input_method['amount']))
				{
					$this -> response(array(
						'status' => "0",
						"message" => BILLING_AMOUNT_MISSING
					), 200);
					exit ;
				}
			}
			$result = $this -> Restaurantapi_model -> trip_end($input_method);

			$booking_id = $input_method['booking_id'];
			if ($result['status'] == 1)
			{

				$trip_detail = $result['trip_detail'];
				$trip_detail = $trip_detail[0];
				$cab_type = $trip_detail['cab_type'];
				$userid = $trip_detail['user_id'];
				$pickup_latitude = $trip_detail['pickup_latitude'];
				$pickup_longitude = $trip_detail['pickup_longitude'];
				$pickup_location = $trip_detail['pickup_location'];
				$driver_id = $trip_detail['driver_id'];
				$driver_name = $trip_detail['first_name'] . ' ' . $trip_detail['last_name'];
				$car_number = $trip_detail['plate_number'];
				$driver_profile_pic = $trip_detail['profile_pic'];
				$driver_contact = $trip_detail['contact'];
				$CBN = $trip_detail['booking_number'];
				$customer_phone = $trip_detail['country_code'] . $trip_detail['client_contact_number'];
				$waiting_time_charge = "0.00";
				$billing_charge = "0.00";
				//$discount_amount = $trip_detail['discount_amount'];
				extract($input_method);
				$km_driven = number_format($km_driven, 2);
				//$other_item_data = array();
				if ($input_method['payment_type'] == 'FIX')
				{
					$billing_charge = $input_method['amount'];
					$trip_charge = $input_method['amount'];
					$min_charge = "";
					$journey_time_charge = "";
					$charge_per_km = "";

				}
				else
				{
					//***************get fare detail**********************
					$fare_detail = $this -> Restaurantapi_model -> getfare_detail($cab_type);
					// print_r($fare_detail);
					if ($fare_detail)
					{
						$other_charge = 0;
						$fare_detail = $fare_detail[0];
						$min_km = $fare_detail['min_km'];
						$min_charge = $fare_detail['min_charges'];
						$waiting_charge = $fare_detail['wait_time_charges'];
						//changes into journey time
						$charge_per_km = $fare_detail['charges_per_km'];
						if ($km_driven >= 1)
						{
							$trip_charge = $km_driven * $charge_per_km;
						}
						else
						{
							$trip_charge = $charge_per_km;
						}
						if (($journey_time / 60) > 1)
						{
							//journey time in sec so convert it into min and calculate charge
							$journey_time_charge = ($journey_time / 60) * ($waiting_charge);
						}
						else
						{
							$journey_time_charge = $waiting_charge;
						}
						$journey_time_charge = number_format($journey_time_charge, 2);

					}
					else
					{

						$this -> response(array(
							'status' => "0",
							"message" => FARE_INFO_MISS
						), 200);
					}

					$vat_charge = "";

					$billing_charge = $min_charge + $trip_charge + $journey_time_charge;
					$billing_charge = $billing_charge - $discount_amount;
				}
				$data = array(

					"km_driven" => number_format($km_driven, 2, '.', ''),
					"km_charge" => $trip_charge,
					"basic_fare_charge" => $min_charge,
					"journey_time" => $journey_time,
					"waiting_charge" => $journey_time_charge,
					"billing_amount" => number_format($billing_charge, 2, '.', ''),
					"booking_id" => "$booking_id",
					"booking_type" => "Restro",
					"payment_type" => $input_method['payment_type'],
					"createdon" => get_gmt_time()
				);
				// print_r($data);
				$payment = $this -> Restaurantapi_model -> save_booking_payment($data);
				if ($payment)
				{
					//update driver status
					$driver_available = DRIVER_AVAILABLE;
					$data = array(
						"driver_id" => $driver_id,
						"status" => "$driver_available"
					);
					$this -> Restaurantapi_model -> update_driver_status($data);

				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => SERVER_ERROR
					), 200);
				}

				$this -> response(array(
					'status' => "1",
					"booking_id" => "$booking_id",
					"booking_type" => "restro",
					"CBN" => $CBN,
					"basic_fare_charge" => $min_charge,
					"journey_time" => $journey_time,
					"journey_time_charge" => $journey_time_charge,
					"km_driven" => $km_driven,
					"per_km_charge" => $charge_per_km,
					"km_driven_charge" => $trip_charge,
					"billing_charge" => $billing_charge,
					"pickup_location" => $pickup_location,
					"car_number" => $car_number,
					"driver_image" => base_url() . DRIVER_IMAGE_PATH . $driver_profile_pic,
					"destination_location" => $destination_location,
					"driver_name" => $driver_name,
					"driver_id" => "$driver_id",
					"driver_contact" => $driver_contact,
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => $result['message']
				), 200);
			}

		}

		function restaurant_booking__timeout()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('timeout', $input_method);//booking_id
			$this -> param_validate($input_method, array("booking_id"));
			$result = $this -> Restaurantapi_model -> timeout($input_method);
			$this -> response(array(
				'status' => "1",
				"message" => DRIVER_BUSY_MESSAGE
			), 200);
		}

		function driver_cancel()
		{
			$input = file_get_contents("php://input");
			$input_method = json_decode($input, true);
			$this -> param_validate($input_method, array(
				"driver_id",
				"booking_id"
			));
			$driver_cancel = $this -> Restaurantapi_model -> driver_cancel($input_method);
			if ($driver_cancel['status'] == 1)
			{
				//get custommer jid and contactdetail
				$phoneno = $driver_cancel['client_country_code'] . $driver_cancel['client_contact_number'];
				send_sms($phoneno, DRIVER_CANCEL_SMS);
				$this -> response(array(
					'message' => $driver_cancel['message'],
					"message_customer" => $driver_cancel['message_customer'],
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => $driver_cancel['message'],
					'status' => "0"
				), 200);
			}
		}

		function payment_done()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('user-login', $input_method);
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id"
			));
			$payment = $this -> Restaurantapi_model -> payment_done($input_method);
			if ($payment['status'] == 1)
			{
				$phone_no = $payment['client_country_code'] . $payment['client_contact_number'];
				// send_sms($phone_no, PAYMENT_DONE);
				$this -> response(array(
					'message' => "success",
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => $payment['message'],
					'status' => "0"
				), 200);
			}

		}

		function fare_estimate()
		{
			//$this->load->model('Booking_model');
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('fare-estimate', $input_method);
			$this -> param_validate($input_method, array(
				"pickup_latitude",
				"pickup_longitude",
				"destination_latitude",
				"destination_longitude",
				"pickup_location",
				// "destination_location",
				"cab_type"
			));
			$cab_type = $input_method['cab_type'];
			$origins = $input_method['pickup_latitude'] . ',' . $input_method['pickup_longitude'];
			$destination = $input_method['destination_latitude'] . ',' . $input_method['destination_longitude'];
			$time_detail = gettimedetail($origins, $destination);
			// $pickup_location = getaddress($input_method['pickup_latitude'],
			// $input_method['pickup_longitude']);
			$destination_loaction = getaddress($input_method['destination_latitude'], $input_method['destination_longitude']);
			$fare_detail = $this -> Restaurantapi_model -> getfare_detail($cab_type);

			if ($time_detail)
			{
				$duration = $time_detail['duration'];
				$distance = $time_detail['distance'];
				$duration_sec = $time_detail['duration_sec'];
			}
			$fare_detail = $fare_detail[0];
			$min_charge = $fare_detail['min_charges'];
			$journey_charge_per_min = $fare_detail['wait_time_charges'];
			$journey_charge = ($duration_sec / 60) * ($journey_charge_per_min);
			$trip_charge = $distance * $fare_detail['charges_per_km'];
			$billing_charge = $min_charge + $journey_charge + $trip_charge;
			$billing_charge_min = ($billing_charge - 25 < 0) ? "0" : $billing_charge - 25;
			$billing_charge_max = $billing_charge + 25;

			$this -> response(array(
				"estimate_bill" => round($billing_charge_min) . "-" . round($billing_charge_max) . " NOK",
				"duration" => $duration,
				"distance" => $distance,
				"pickup_location" => $input_method['pickup_location'],
				"destination_location" => $destination_loaction,
				'status' => "1"
			), 200);

		}

		function ride_later()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('ride-later', $input_method);
			$this -> param_validate($input_method, array(
				"pickuplat",
				"pickuplong",
				"restro_id",
				"pickup_time",
				"cab_type",
				"client_name",
				"client_contact_number",
				"country_code",
				"pickup_location"
			));
			$result = $this -> Restaurantapi_model -> cab_booking_advance($input_method);
			if ($result)
			{
				$pickuptime = $input_method['pickup_time'];
				$booking_date = date('d/m-Y', strtotime($pickuptime));
				$booking_time = date('H:i', strtotime($pickuptime));

				$get_message = PREBOOKING;
				$msg_replace = array(
					"<DATE>",
					"<TIME>",
					"<ADRESS>"
				);
				$detail = array(
					$booking_date,
					$booking_time,
					$input_method['pickup_location']
				);
				//echo $phonenumber;
				$prebooking_sms = str_replace($msg_replace, $detail, $get_message);
				$mobile_number = '+' . $input_method['country_code'] . $input_method['client_contact_number'];
				send_sms($mobile_number, $prebooking_sms);
				//advance_booking_email($result);
				prebooking_mail_to_admin($result);
				$this -> response(array(
					'status' => "1",
					"message" => RIDE_LATER . $booking_date . ' ' . $booking_time
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => "something went wrong"
				), 200);
			}
		}

		function check_appversion()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('fare-estimate', $input_method);
			$this -> param_validate($input_method, array(
				"os_type",
				"app_mode",
				"app_version",
			));
			$appversion = $this -> Restaurantapi_model -> check_appversion($input_method);

			if ($input_method['app_version'] < $appversion['version'])
			{
				$this -> response(array(
					'status' => "0",
					"link" => $appversion['link'],
					"is_require" => $appversion['is_required'],
					"app_version" => $appversion['version']
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "1",
					"message" => SUCCESS,
				), 200);
			}

		}

	}
