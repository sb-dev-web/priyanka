<?php
function make_thumb($src, $dest, $desired_height, $ext)
{
	if ($ext == 'png' || $ext == 'PNG' || $ext == '.png' || $ext == '.PNG')
	{
		$source_image = imagecreatefrompng($src);
	}
	else
	{
		$source_image = imagecreatefromjpeg($src);
	}
	$width = imagesx($source_image);
	$height = imagesy($source_image);

	/* find the "desired height" of this thumbnail, relative to the desired width  */
	//	$desired_height = floor($height * ($desired_width / $width));

	$desired_width = floor($width * ($desired_height / $height));

	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	/* new function for png*/
	imagealphablending($virtual_image, false);
	imagesavealpha($virtual_image, true);

	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

	/* create the physical thumbnail image to its destination */

	if ($ext == 'png' || $ext == 'PNG' || $ext == '.png' || $ext == '.PNG')
	{
		imagepng($virtual_image, $dest);
	}
	else
	{
		imagejpeg($virtual_image, $dest);
	}
}
 function get_thumb($image)
	{
		$image = explode('.', $image);
		$thumb_img = $image[0] . '_thumb.' . $image[1];
		return $thumb_img;
	}
	function sendadminmail($filename)
	{
		//mail()
	}

?>