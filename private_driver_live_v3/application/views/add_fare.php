<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php if($fare[0]->type=='NORMAL')
             {
        echo $fare[0]->title.' '.'class'.' ' .'Regular Fare';
             }
             else
        {     
    echo $fare[0]->title.'  '.'class'.' ' .'Restaurant Fare';
        }
     ?></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Fare</a></li>
      <li class="active">Add Fare</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="save_fare" action="" method="post" enctype="multipart/form-data" autocomplete="off">
             <input type="hidden" value="<?=(isset($fare[0]->id))?$fare[0]->id:"";?>"  name="id" id="id" />
              <!--<div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Description</label>
                      <textarea rows="4" cols="50" name="description" id="description"  placeholder="Description"><?=(isset($fare[0]->description))?$fare[0]->description:"";?></textarea>
                  </div>
                </div>
              </div>-->
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="Person">No Of Person</label>
                      <input type="text" class="form-control validate[required]" name="nop" value="<?=(isset($fare[0]->no_of_person))?$fare[0]->no_of_person:"";?>" id="nop" placeholder="No Of Person" style="width: 150px;">
                  </div>  
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="charges">Minimum Charges</label>
                       <input type="text" class="form-control validate[required]" name="min_charges" value="<?=(isset($fare[0]->min_charges))?$fare[0]->min_charges:"";?>" id="min_charges" placeholder="Minimum Charges" style="width: 150px;">
                  </div>  
                </div>
              </div>
              
                
              <!--<div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="km">Minimum Kilometers</label>
                       <input type="text" class="form-control validate[required]" name="min_km" value="<?=(isset($fare[0]->min_km))?$fare[0]->min_km:"";?>" id="min_km" placeholder="Minimum Kilometers" style="width: 150px;">
                  </div>  
                </div>
              </div>-->
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="charges">Charges Per Km</label>
                       <input type="text" class="form-control validate[required]" name="charge_km" value="<?=(isset($fare[0]->charges_per_km))?$fare[0]->charges_per_km:"";?>" id="charge_km" placeholder="Charge Per Kilometers" style="width: 150px;">
                  </div>  
                </div>
              </div>
              
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="charges">Journey Charges</label> &nbsp;(in min)
                       <input type="text" class="form-control validate[required]" name="Waittime_charges" value="<?=(isset($fare[0]->wait_time_charges))?$fare[0]->wait_time_charges:"";?>" id="Waittime_charges" placeholder="journey time charges" style="width: 150px;">
                  </div>  
                </div>
              </div>
              
                <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="charges">Pre Booking Time</label>
                       <input type="text" class="form-control validate[required]" name="booking_time" value="<?=(isset($fare[0]->ride_later_time))?$fare[0]->ride_later_time:"";?>" id="booking_time" placeholder="Ride Later Time" style="width: 150px;">
                  </div>  
                </div>
              </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>
    
<script type="text/javascript">
//var $ = jQuery.noConflict();
$(document).ready(function(){

    function submit_form(formData,status)
    {
        if(status==true)
        {
            var querystr = $("#save_fare").serialize();
            
            
            
            $.ajax({
                url		:	"<?php echo site_url('Generalinfo/submit_fare'); ?>",
                type	:	"POST",
                data	:	querystr,
                //async: false,
               // cache: false,
                //contentType: false,
                //processData: false,
                
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data)
                {
                  
                 var r = confirm("Fare information save successfully");
                 if (r==true) {
                 
                 
                    $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl("Your record is saved", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_fare')[0].reset();
                    window.location='<?php echo site_url('Generalinfo/Fare'); ?>';
                  }
                  else
                  {
                    $("#loader").hide();
                      $('#save_fare')[0].reset();
                    //window.location='<?php echo site_url('login/dashboard'); ?>';
                    //alert('failed to save');
                  }
                 }
            });
            return false;
        }
        
    }
            
      
    
  $("#save_fare").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});
</script>            