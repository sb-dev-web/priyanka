<?php
	class Notification_model  extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		/* notification */
		function fetch_device($device_type)
		{
			$fetch = $this -> db -> query("select * from user where device_type='$device_type'  and device_token!='' ");
			$count = $fetch -> num_rows();
			if ($count)
			{
				return $fetch -> result_array();
			}

		}

		function fetch_all_device()
		{
			$fetch = $this -> db -> query("select * from user where device_type='I' and device_token!='' ");//and id in (294,311,270,333)
			$count = $fetch -> num_rows();
			if ($count)
			{
				$iphone = $fetch -> result_array();
			}
			$fetch_data = $this -> db -> query("select * from user where device_type='A'  and device_token!='' ");
			$counts = $fetch_data -> num_rows();
			$andorid = array();
			if ($counts)
			{
				$andorid = $fetch_data -> result_array();
			}

			return array(
				"iphone" => $iphone,
				"andorid" => $andorid
			);
		}

		/* save notification */
		function save_notify($msg, $device)
		{
			$date = gmdate('Y-m-d H:i:s');

			$save_nof = $this -> db -> query("insert into notification (message,device,createdon) VALUES('$msg','$device','$date')");
			//echo $this->db->last_query();
			if ($save_nof)
			{

				//save user notification
				$notification_id = $this -> db -> insert_id();
				$this -> db -> select("id");
				if ($device == "ALL")
				{
					$where = array(
						"is_active" => "1",
						"is_deleted" => '0'
					);
				}
				else
				{
					$where = array(
						"device_type" => $device,
						"is_active" => "1",
						"is_deleted" => '0'
					);
				}

				$users_qry = $this -> db -> get_where("user", $where);
				//echo $this->db->last_query();
				if ($users_qry -> num_rows())
				{
					
					$users=$users_qry->result_array();
					//print_r($users);
					foreach ($users as $user)
					{
						$data = array(
							"notification_id" => $notification_id,
							"user_id" => $user['id'],
							"is_view" => "N",
							"type"=>"NOTIFICATION",
							"created_on" => get_gmt_time()
						);
                        //print_r($data);
						$insert = $this -> db -> insert("user_notification", $data);
						//echo $this->db->last_query();
					}
				}
				return 1;
			}
			else
			{
				return 0;
			}
			
			
		}
function GetBadge($user_id)
			{
				$query=$this->db->query("SELECT count(id) count FROM `user_notification` WHERE user_id=$user_id and type='NOTIFICATION' and is_view='N'");
			   //echo $this->db->last_query();
			    $data= $query->row_array();
			    return $data['count'];
			}

	}
