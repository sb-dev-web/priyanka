<?php
	class Resetpassword_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		/* fetch order information */
		function get_user_detail($data)
		{
			$userid = $data['userid'];
			$code = $data['code'];
			$table = $data['table'];
			if ($table == 'user')
			{
				$query = $this -> db -> query("Select email,id,name from user where id=$userid and password_recovercode='" . $code . "'");
			}
			else
			{
				$query = $this -> db -> query("Select email,id,name from user_temp where id=$userid and password_recovercode='" . $code . "'");
			}

			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}

		}

		function set_user_password($data)
		{
			$password = md5($data['form-password']);
			$userid = $data['id'];
			$email = $data['form-username'];
			//chk user
			$select = $this -> db -> get_where("user", array("email" => "$email"));
			if ($select -> num_rows() > 0)
			{
				$query = $this -> db -> query("update user set password='" . $password . "' , password_recovercode='' where id='" . $userid . "'");
			}
			else
			{
				$query = $this -> db -> query("update user_temp set password='" . $password . "' , pass='" . $data['form-password'] . "', password_recovercode='' where id='" . $userid . "'");
			}
			if ($query)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function account_verify($data)
		{
			$email_code = $data['code'];
			$user_id = $data['userid'];
			$query = "select id,is_varify,is_emailverify,name,email,pass,is_social,login_type from user_temp where id='$user_id' and email_code='$email_code'";
			$query = $this -> db -> query($query);
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				//verify the mail
				$data = $query -> row_array();
				$update_str = array("is_emailverify" => "1");
				$this -> db -> where("id", $data['id']);
				$this -> db -> update("user_temp", $update_str);
				//echo $this->db->last_query();
				$result = array(
					"is_verify" => $data['is_varify'],
					"email_verify" => $data['is_emailverify'],
					"username" => $data['name'],
					"user_id" => $data['id'],
					"pass" => $data['pass'],
					"message" => SUCCESS,
					"status" => 1,
					"account_type" => "NOT_VERIFY",
					"is_social" => $data['is_social'],
					"email" => $data['email'],
					"login_type" => $data['login_type']
				);
				return $result;
			}
			else
			{
				$query = "select id,is_varify,name,email,is_social from user where email_code='$email_code'";
				$query = $this -> db -> query($query);
				if ($query -> num_rows() > 0)
				{
					$data = $query -> row_array();
					$result = array(
						"is_verify" => $data['is_varify'],
						"email_verify" => $data['is_emailverify'],
						"username" => $data['name'],
						"user_id" => $data['id'],
						"message" => SUCCESS,
						"status" => 1,
						"is_social" => $data['is_social'],
						"account_type" => "VERIFY",
						"email" => $data['email'],
					);
					return $result;
				}
				else
				{

					return array(
						"status" => 0,
						"message" => EMAIL_VERIFY_CODE_INVAILD
					);
				}
			}
		}

		function VerifyUser($temp_user_id)
		{
			$query = "SELECT * FROM `user_temp` u left join card_info_temp c on u.id=c.customer_id where u.id=$temp_user_id";
			$query = $this -> db -> query($query);
			//echo $this->db->last_query();

			if ($query -> num_rows() > 0)
			{
				$query -> num_rows();
				$data = $query -> row_array();
				$this -> db -> trans_begin();
              $AuthToken = _generate_key();
				$user_detail = array(
					"name" => $data['name'],
					"company_name" => $data['company_name'],
					"dob" => $data['dob'],
					"email" => $data['email'],
					"password" => $data['password'],
					"phone" => $data['phone'],
					"country_code" => $data['country_code'],
					"s_id" => $data['s_id'],
					"login_type" => $data['login_type'],
					"is_social" => $data['is_social'],
					"device_token" => $data['device_token'],
					"is_company" => $data['is_company'],
					"company_id" => $data['company_id'],
					"email_code" => $data['email_code'],
					"is_varify" => 1,
					"is_emailverify" => 1,
					"createdon" => get_gmt_time(),
					"mode" => $data['mode'],
					"auth_token"=>$AuthToken
					
				);
				
				$insert_user = $this -> db -> insert("user", $user_detail);

				$user_id = $this -> db -> insert_id();
				$data['user_id'] = $user_id;

				//check if the web user there is no any cc detail
				if ($data['login_type'] == "W")
				{

					//delete the temp user
					$this -> db -> query("delete from user_temp where id=$temp_user_id");
					$openfireuser = $this -> MakeOpenfireUser($data);
					if ($openfireuser)
					{
						$this -> db -> trans_commit();
						$this -> db -> trans_complete();
						return $user_id;
					}
					else
					{
						$this -> db -> trans_rollback();
						$update_str = array("is_emailverify" => "0");
						$this -> db -> where("id", $temp_user_id);
						$this -> db -> update("user_temp", $update_str);
						return false;
					}

				}
				if ($data['is_company'] == "Y")
				{
					//Make A Entry In Company User Table
					$insert_data = array(
						"user_id" => $user_id,
						"company_name" => $data['company_name'],
						"company_id" => "0",
						"created_on" => get_gmt_time()
					);
					$user_company = $this -> db -> insert("user_company", $insert_data);
					if ($this -> db -> insert_id())
					{
						$this -> db -> query("delete from user_temp where id=$temp_user_id");
						$this -> db -> query("delete from card_info_temp where customer_id=$temp_user_id");
						//return $user_id;
						$openfireuser = $this -> MakeOpenfireUser($data);
						if ($openfireuser)
						{
							$this -> db -> trans_commit();
							$this -> db -> trans_complete();
							return $user_id;
						}
						else
						{
							$this -> db -> trans_rollback();
							$update_str = array("is_emailverify" => "0");
							$this -> db -> where("id", $temp_user_id);
							$this -> db -> update("user_temp", $update_str);
							return false;
						}
					}
					else
					{
						return false;
					}

				}
				if ($user_id)
				{

					$cc_detail = array(
						"customer_id" => $user_id,
						"credit_card_no" => $data['credit_card_no'],
						"card_type" => $data['card_type'],
						"braintree_customer_id" => $data['braintree_customer_id'],
						"braintree_token" => $data['braintree_token'],
						"expire_date" => $data['expire_date'],
						"isactive" => $data['isactive'],
						"createdon" => get_gmt_time(),
					);

					$card_id = $this -> db -> insert("card_info", $cc_detail);
					if ($card_id)
					{

						$this -> db -> query("delete from user_temp where id=$temp_user_id");
						$this -> db -> query("delete from card_info_temp where customer_id=$temp_user_id");
						$openfireuser = $this -> MakeOpenfireUser($data);
						if ($openfireuser)
						{
							$this -> db -> trans_commit();
							$this -> db -> trans_complete();
							return $user_id;
						}
						else
						{
							$this -> db -> trans_rollback();
							$update_str = array("is_emailverify" => "0");
							$this -> db -> where("id", $temp_user_id);
							$this -> db -> update("user_temp", $update_str);

							return false;
						}
						//return $user_id;
					}

					else
					{
						return false;
					}

				}
				else
				{
					return false;
				}

			}
			else
			{
				return false;
			}
		}

		function MakeOpenfireUser($data)
		{
			//print_r($data);
			$user_id = $data['user_id'];
			$user_name = USER_JID_NAME . '-' . $user_id;
			$password = $data['pass'];
			if (@$data['is_social'] == 'N')
			{
				$password = $data['pass'];
				$password = trim($password);
				$is_social = "N";
			}
			else
			{
				$password = "123456";
				$password = trim($password);
				$is_social = "Y";
			}
			$name = $data['name'];
			$email = $data['email'];
			$param = array(
				"username" => $user_name,
				"name" => "$name",
				"password" => "$password",
				"email" => "$email"
			);

			$openfir_result = $this -> openfire -> openfire_action("POST", $param, "users/");
			$openfire_response = json_decode($openfir_result, true);
		//print_R($openfire_response);
			if ($openfire_response['status'] == 0)
			{
				return false;

			}
			else
			{
				return true;
			}
		}

	}
