<?php

class Paginationlib
{

    function __construct()
    {
        $this->ci = &get_instance();
    }

    public function initPagination($url, $total_rows, $per_page, $uri_segment,$extra_param=array())
    {
        /*$config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config["base_url"] = site_url($url);
        
        $config["uri_segment"] = $uri_segment;*/
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = ceil($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['base_url'] =  $url;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active">';//<a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
      	$config['additional_param']  = $extra_param;
        $config['cur_page']=$uri_segment;
        $config['div'] = 'record';


        return $config;
    }
}

?>