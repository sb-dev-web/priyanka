<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            New Pre Booking
             <?php //print_r($advance_booking); ?>
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active"> New Pre Booking</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
             
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Booking #</th>
                       
                        <th>Pickup Address</th>
                         <th>Pickup Time</th>
                        <th>Booking Time</th>
                        <th>User</th>
                         <th>Cab Type</th>
                        <th>Action</th>
                      
                        
                      </tr>
                    </thead>
                    <tbody>
                    
 
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                 <div class="row">
                    
              
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <!-- open customer pop up -->
<div class="modal fade" id="open_customer" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
              Pre Booking Detail
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
               <div id="showreport" name="showreport"></div>
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
            
            
            
<!-- modal for select driver -->






      
      
      
      
      
      
      
      
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      </div>
     
      </aside><!-- /.control-sidebar -->
 <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<!--     <script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>-->
<script type="text/javascript" src="<?=base_url();?>js/my_custom.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>plugins/server_datatable/js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url();?>plugins/server_datatable/js/jquery.dataTables.js"></script>


<!-- choosen -->
<script type="text/javascript" language="javascript" >
			$(document).ready(function() {
				var dataTable = $('#example2').DataTable( {
					"processing": true,
					 "ordering": false,
					"serverSide": true,
					
					"ajax":{
						url :'<?php echo site_url('Booking/GetNewBooking');?>', // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".employee-grid-error").html("");
							$("#example2").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
							
						}
					}
				} );
				
				
				
				
			} );
			function Actiondata(data,action){
        	
        	//	var this=data;
        
        	//alert(data);
        	var text =$("#"+data).text();
        	//alert(action);
        	if(action=='Reject')
        	{
        		var status="REJECTED";
        	}else
        	{
        		var status="CONFIRMED";
        	}
        	$.ajax({type:'POST',url: '<?php echo site_url('Booking/ConfirmPreBooking');?>',
			
			data:{"id":data,"status":status},
			success: function(response) {
				 $('#'+data).closest("tr").remove();
			 //  statusfeedback(response,spanid,loadingdiv);
			 }});
        }
			</script>
     
      
       <script>
                   
        /* get customer detail */
        function customer_detail(booking_no,type)
        {
            //var type='NORMAL';  
          $.ajax({
            type:'POST',
            url: '<?php echo site_url('Booking/get_advance_booking');?>',
            data:{"booking_no":booking_no,"type":type},
             beforeSend: function() {
              $("#loading-image").show();
           },
            success: function(response) {
            //alert(response);
             $("#showreport").html(response);
             $('#open_customer').modal({
                 "backdrop" : "static"
         })
              $("#loading-image").hide();
             }});
       }
       
 function changeStatus(id,loadingdiv,spanid)
		{
		  
		  var r = confirm("Are you sure to confirm this booking?");
		if (r == true) 
		{
			var spanValue= $('#'+spanid).text();
			
			$('#'+loadingdiv).css('display','inline');
			status = 'CONFIRMED';
			if(spanValue=='Waiting')
			{  
			   status = 'CONFIRMED';
			}else
			{
				alert("in else");
			}
			$.ajax({type:'POST',url: '<?php echo site_url('Booking/ConfirmPreBooking');?>',
			
			data:{"id":id,"status":status},
			success: function(response) {
				 $('#'+loadingdiv).closest("tr").remove();
			 //  statusfeedback(response,spanid,loadingdiv);
			 }});
		     
		     
		}
		else
		{
		  
		}
		  
		  
		}
       
       function statusfeedbacks(retstatus,spanid,loadingdiv) 
{
    //alert(retstatus);
    if(retstatus=='N')
    {
        $('#'+spanid).removeClass('label-success').addClass('label-warning');
        $('#'+spanid).text("Logged Out");
    }
    else if(retstatus=='Y')
    {

        $('#'+spanid).removeClass('label-warning').addClass('label-success');
        $('#'+spanid).text("Logged In");
    }
     $('#'+loadingdiv).css('display','none');
    
}
</script>
          
          
        
            
       
