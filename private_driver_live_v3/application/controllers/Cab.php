<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cab extends CI_Controller {

	function __construct()
    {
        parent::__construct();
       $this->load->helper(array('form','url'));
	     $this->load->model('Cab_model');
		 $this->load->library(array(
            'session',
            'email',
            'paginationlib',
            'Pagination'));
		$user = $this->session->userdata('logged_in');
        if (!$user) {
            redirect('Welcome');
        }
		
    }
    
	 public function index()
    {
       $this->load->library('Ajax_pagination');
        $this->Cab_model->Cab_count();
        $pagingconfig = $this->paginationlib->initPagination("/Cab/ajaxPaginationData/", $this->
            Cab_model->Cab_count(), 10, 1,array());


        $this->ajax_pagination->initialize($pagingconfig);
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //$data['link'] = $this->pagination->create_links();
        $data['link'] = $this->ajax_pagination->create_links();
        $cab_details = $this->Cab_model->get_cab_details($pagingconfig['per_page'], 0);
       
        $data['cab_details'] = $cab_details;
        $data['active_page'] = 'Cab';
        $this->load->view('site-nav/header', $data);
        $this->load->view('cab');
        $this->load->view('site-nav/footer');
    }
    function ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Cab/ajaxPaginationData/", $this->
            Cab_model->Cab_count(), 10, $post['page']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['cab_details'] = $this->Cab_model->get_cab_details($pagingconfig['per_page'],
            $page);

        //load the view
        $this->load->view('cab_ajax', $data, false);
    }
    public function perPage()
    {
        $this->load->library('Ajax_pagination');
        return 2;
    }

	public function change_status()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$driver_status=$this->Cab_model->change_cab_status($id,$status);
	    echo $driver_status;
	}
	public function add_cab()
	{@$cab[0]->cab_type =$cab[0]->cab_palte_no=$cab[0]->cab_model="";
	    $cab="";
		 if ($this->uri->segment(3) == false) {
            $id = 0;
        } else {
            $id = $this->uri->segment(3);
            $cab = $this->Cab_model->get_cab($id);
        }
		$cab_type=$this->Cab_model->get_cab_type();
		//print_r($driver);
 	  $data['cab']=$cab;
		$data['cab_type']=$cab_type;
		$data['active_page'] = 'cab';
        $this->load->view('site-nav/header', $data);
        $this->load->view('add_cab');
		$this->load->view('site-nav/footer');
	}
	
	public function submit_cab()
	{
	    $id = $_POST['id'];
		$res = $this->Cab_model->save_cab($id);
		if($res)
		{
			echo 1;
		}
		else
		{
			echo 0;
		}
		//echo $res;
	}
	
	 function change_delete_status()
    {
        $id = $_POST['id'];
        echo $this->Cab_model->delete_cab($id);
    }
	
}
