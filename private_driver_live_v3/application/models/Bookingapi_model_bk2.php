<?php

class Bookingapi_model extends CI_Model {

	public function __construct() {
		$this -> load -> database();
	}

	function nearby_serach($input_method) {
		$lat = $input_method['pickup_latitude'];
		$lng = $input_method['pickup_longitude'];
		$userid = $input_method['user_id'];
		$type = $input_method['cab_type'];

		//get all available driver
		$query = $this -> db -> query("select * from (SELECT d.driver_id,concat(d.first_name,' ',d.last_name)as name,s.status,d.latitude,d.longitude,d.email_id,d.contact,d.profile_pic,
         ct.type_id,ct.title,c.cab_plate_no,( 3959 * acos( cos( radians($lat) ) 
        * cos( radians( d.latitude ) ) 
        * cos( radians( d.longitude ) - radians($lng) ) 
        + sin( radians($lat) ) 
        * sin( radians( d.latitude ) ) ) ) AS distance 
        FROM driver d
       join shift s on d.driver_id=s.driver_id and s.status='STARTED' join cab c on c.cab_id=s.cab_id join cab_type ct on ct.type_id=c.cab_type )sub
        where sub.distance < 50 and sub.type_id='" . $type . "'
        ORDER BY distance ");
		// echo  $this->db->last_query();
		//  exit;
		if ($query -> num_rows() > 0) {
			return $result = $query -> result_array();
		} else {
			return false;
		}

	}

	function cab_booking($input_method) {

		//print_r($input_method);
		$lat = $input_method['pickup_latitude'];
		$lng = $input_method['pickup_longitude'];
		$pickup_location = urldecode($input_method['pickup_location']);
		$userid = $input_method['user_id'];
		$destination_lat = $input_method['destination_latitude'];
		$detination_lng = $input_method['destination_longitude'];
		$destination_location = urldecode($input_method['destination_location']);
		$userid = $input_method['user_id'];
		$cab_type = $input_method['cab_type'];
		$comment = (@$input_method['comment'] == '') ? "" : @$input_method['comment'];
		$date = get_gmt_time();
		$coupon_code = $input_method['coupon_code'];
		$discount = $input_method['discount_amount'];
		$data = array('user_id' => $userid, 'createdon' => $date, 'booking_time' => $date, "pickup_latitude" => $lat, "pickup_longitude" => $lng, "pickup_location" => $pickup_location, "destination_latitude" => $destination_lat, "destination_longitude" => $detination_lng, "destination_location" => $destination_location, "cab_type" => $cab_type, "comment" => $comment, "discount_amount" => $discount, "coupon" => $coupon_code);
		$str = $this -> db -> insert('cab_booking', $data);
		// echo "pp". $this->db->last_query();
		$booking_id = $this -> db -> insert_id();
		//save customer extra order
		//echo strip_slashes($input_method['extra_order']);
		if (!empty($input_method['extra_order'])) {

			$extra_order = json_decode(strip_slashes($input_method['extra_order']), true);

			foreach ($extra_order as $order) {
				$order_data = array("booking_id" => $booking_id, "user_id" => $userid, "item_id" => $order['item_id'], "item_price" => $order['item_price'], "quantity" => $order['quantity'], "createdon" => $date);
				$this -> db -> insert("booking_extra_order", $order_data);
				//echo $this->db->last_query();
			}
		}
		if ($booking_id) {
			$CBN = "PD" . sprintf("%05d", $booking_id);
			$this -> db -> query("update cab_booking set booking_number='" . $CBN . "' where id=$booking_id");
			return $booking_id;
			//send request to driver for new booking
			//search nearest driver

		} else {
			return false;
		}
	}

	function nearby_booking_serach($input_method) {
		$lat = $input_method['pickup_latitude'];
		$lng = $input_method['pickup_longitude'];
		$userid = $input_method['user_id'];
		$destination_lat = $input_method['destination_longitude'];
		$detination_lng = $input_method['destination_longitude'];
		$pickup_location = urldecode($input_method['pickup_location']);
		$destination_location = urldecode($input_method['destination_location']);
		$type = $input_method['cab_type'];
		//$cab_type=

		//********************get all available driver*****************************<br />

		$query = $this -> db -> query("select * from (SELECT d.driver_id,d.islogin,concat(d.first_name,' ',d.last_name)as name,d.status,d.latitude,d.longitude,d.email_id,d.contact,d.profile_pic,
        ct.type_id,ct.title,c.cab_plate_no,( 3959 * acos( cos( radians($lat) ) 
        * cos( radians( d.latitude ) ) 
        * cos( radians( d.longitude ) - radians($lng) ) 
        + sin( radians($lat) ) 
        * sin( radians( d.latitude ) ) ) ) AS distance 
        FROM driver d
        join shift s on d.driver_id=s.driver_id and s.status='STARTED'
        join cab c on c.cab_id=s.cab_id 
        join cab_type ct on ct.type_id=c.cab_type )sub
        where sub.distance < 50 AND (sub.status = '" . DRIVER_AVAILABLE . "') and sub.islogin='Y' and sub.type_id='" . $type . "'
        ORDER BY distance ");
		// echo $this->db->last_query();
		// echo "<br/>";
		if ($query -> num_rows() > 0) {
			$result = $query -> result_array();
			$i = 0;
			foreach ($result as $driver_data) {
				// print_r($driver_data);

			}
		}
		//*********************************GET BUSY DRIVER**********************************
		$query = $this -> db -> query("select * from (SELECT d.driver_id,d.islogin,concat(d.first_name,' ',d.last_name)as name,d.status,d.latitude,d.longitude,d.email_id,d.contact,d.profile_pic, ct.title,ct.type_id,c.cab_plate_no,
         (( 3959 * acos( cos( radians($lat) ) * cos( radians( b.destination_latitude ) ) 
         * cos( radians( b.destination_longitude ) - radians($lng) ) + 
         sin( radians($lat) ) * sin( radians( b.destination_latitude ) ) ) ) 
		 +( 3959 * acos( cos( radians(d.latitude) ) * cos( radians( b.destination_latitude ) ) 
         * cos( radians( b.destination_longitude ) - radians(d.longitude) ) + 
         sin( radians(d.latitude) ) * sin( radians( b.destination_latitude ) ) ) ) ) as distance
		 
         FROM  driver d join shift s on d.driver_id=s.driver_id and s.status='STARTED' join cab c on c.cab_id=s.cab_id join cab_type ct on ct.type_id=c.cab_type  join cab_booking b on d.driver_id=b.driver_id group by driver_id )sub 
         where  sub.status='" . DRIVER_BUSY . "' and sub.distance<50 and islogin='Y' and sub.type_id='" . $type . "'  order by sub.distance ");
		// echo  $this->db->last_query();
		// exit;
		if ($query -> num_rows() > 0) {
			$result_busy = $query -> result_array();
		}
		$query = $this -> db -> query("select id,name,phone from user where id=$userid ");
		if ($query -> num_rows() > 0) {
			$user_detail = $query -> result_array();
		} else {
			$user_detail = array();
		}

		if (count(@$result) && count(@$result_busy)) {
			$cab_detail = array_merge($result, $result_busy);
		} elseif (count(@$result)) {
			$cab_detail = $result;
		} elseif (count(@$result_busy)) {
			$cab_detail = $result_busy;
		} else {
			return false;
		}
		////echo "<pre>";
		// print_r($cab_detail);
		return array("driver_info" => $cab_detail, "user_detail" => $user_detail);
	}

	/* booking in advance */
	function cab_booking_advance($input_method) {
		$lat = $input_method['pickuplat'];
		$lng = $input_method['pickuplong'];
		$userid = $input_method['customerid'];
		$destination_lat = $input_method['destinationlat'];
		$detination_lng = $input_method['destinationlong'];
		$date = get_gmt_time();
		$booking_time = $input_method['pickup_time'];
		$cab_type = $input_method['cab_type'];
		$destination_address = urldecode($input_method['destination_location']);
		$pickup_address = urldecode($input_method['pickup_location']);
		$data = array('user_id' => $userid, 'createdon' => $date, "pickup_latitude" => $lat, "pickup_longitude" => $lng, "destination_latitude" => $destination_lat, "destination_longitude" => $detination_lng, "destination_location" => $destination_address, "pickup_location" => $pickup_address, "booking_time" => $booking_time, "cab_type" => $cab_type, "is_prebooking" => 'Y');
		$str = $this -> db -> insert('cab_booking', $data);
		// echo "pp". $this->db->last_query();
		$booking_id = $this -> db -> insert_id();
		if ($booking_id) {
			$CBN = "PD" . sprintf("%05d", $booking_id);
			$this -> db -> query("update cab_booking set booking_number='" . $CBN . "' where id=$booking_id");
			return $booking_id;
			//send request to driver for new booking
			//search nearest driver

		} else {
			return false;
		}
	}

	function driver_available($input_method) {
		$driver_id = $input_method['driver_id'];
		$chk = $this -> db -> get_where('driver', array('driver_id' => $driver_id, "status" => DRIVER_AVAILABLE));
		// $this->db->last_query();
		if ($chk -> num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function get_booking_detail($bookingid) {

		//$query = $this->db->get_where('cab_booking', array('id' => $booking_id), 1);
		$query = $this -> db -> query("SELECT b.id as booking_id,b.pickup_latitude,
        b.pickup_longitude,b.user_id,u.name,u.phone,b.destination_latitude,b.destination_longitude,booking_number,pickup_location,destination_location,b.comment
         FROM `cab_booking` b
          JOIN user u ON u.id = b.user_id
          left join booking_extra_order o on o.booking_id=b.id 
           WHERE b.id =$bookingid");
		if ($query -> num_rows() > 0) {
			$data = $query -> result_array();
			return $data;
		} else {
			return false;
		}

	}

	function booking_status($input_method) {
		$booking = $input_method['booking_id'];
		$driver_id = $input_method['driver_id'];
		$this -> db -> select('status');
		$booking_res = $this -> db -> get_where('cab_booking', array("id" => $booking));
		//echo $this->db->last_query();
		if ($booking_res -> num_rows() > 0) {
			$booking_status_data = $booking_res -> result_array();
			$booking_status_data = $booking_status_data[0];
			$booking_status = $booking_status_data['status'];
			if ($booking_status == 'WAITING') {
				$status = "WAITING";

				//chk driver rejected or any thing response for this booking
				$this -> db -> select('driver_response');
				$driver_response = $this -> db -> get_where('cab_booking_history', array("booking_id " => $booking, "driver_id" => $driver_id,"booking_type"=>"NORMAL"));
				// echo $this->db->last_query();
				if ($driver_response -> num_rows() > 0) {
					$driver_response_data = $driver_response -> result_array();
					$driver_response_data = $driver_response_data[0];
					$status = $driver_response_data['driver_response'];
					return $status;
				}
				//chk this driver available or not for booking request
				/* $driver_status = $this->db->query("SELECT d.status,b.id  FROM shift s left join cab_booking b on b.driver_id=s.driver_id and
				 b.id > $booking left join driver d on d.driver_id=b.driver_id where s.driver_id =$driver_id and d.status='" .
				 DRIVER_AVAILABLE . "'");*/
				$driver_status = $this -> db -> query("SELECT b.status FROM cab_booking b where id='" . $booking . "'");
				// echo $this->db->last_query();
				if ($driver_status -> num_rows() > 0) {
					$driver_status_data = $driver_status -> result_array();
					$driver_status_data = $driver_status_data[0];
					$driver_status = $driver_status_data['status'];
					//$nextbooking_id = $driver_status_data['id'];
					//
					//                    if ($nextbooking_id > '0' || $driver_status != DRIVER_AVAILABLE)
					//                    {
					//                        $status = "DRIVER_BUSY";
					//                    }
					//                    else
					//                    {
					//                        $status = "WAITING";
					//                    }
					$status = $driver_status;
				} else {
					return $status;
				}

				return $status;
			} else {
				return $status = $booking_status;

			}
			//$query="select status from cab_booking where id=$id";
		} else {
			return $status = BOOKING_NOTFOUND;
		}
	}

	//Driver request response
	function driver_response($input_method) {
		$driver_id = $input_method['driver_id'];
		$booking_id = $input_method['booking_id'];
		$type = $input_method['type'];
		if ($type == 'accept') {
			/*****************************************************
			 //save in history table and update booking table
			 /*****************************************************/

			$date = get_gmt_time();
			$data = array('booking_id' => $booking_id, 'driver_id' => $driver_id, 'driver_response' => "DRIVER_ACCEPTED", 'driver_datetime' => $date, );
			$str = $this -> db -> insert('cab_booking_history', $data);

			if ($this -> db -> insert_id()) {

				//***********CHK BOOKING EXIST OR NOT *************************

				$this -> db -> select("status,id");
				$this -> db -> where("id", $booking_id);
				$this -> db -> where("status", "WAITING");
				$booking_status = $this -> db -> get("cab_booking");
				if (!$booking_status -> num_rows()) {
					/*****************************************************
					 //update driver status
					 /*****************************************************/
					$driver_available = DRIVER_AVAILABLE;
					$data = array('status' => "$driver_available");
					$this -> db -> where('driver_id', $driver_id);
					$update = $this -> db -> update('driver', $data);

					$result = array("type" => $type, "status" => 0, "message" => BOOKING_CANCEL);
					return $result;
					//return false;
				}

				/*****************************************************
				 //Assign driver for booking and set the cab plate number
				 /*****************************************************/

				$plate_number = $this -> get_cab_plate_number($driver_id);
				$driver_accepted = DRIVER_ACCEPTED;
				$data = array('driver_id' => $driver_id, 'status' => "$driver_accepted", "plate_number" => $plate_number, 'driver_assigned_date' => $date);

				$this -> db -> where('id', $booking_id);
				$update = $this -> db -> update('cab_booking', $data);

				//  echo "<br/>".$this->db->last_query();
				if ($update) {
					/*****************************************************
					 //update driver status
					 /*****************************************************/
					//added by sourabh //28/09//15
					$data = array('status' => "BUSY");
					//28/09//15
					//$data=array('status'=>"AVAILABLE");
					$this -> db -> where('driver_id', $driver_id);
					$update = $this -> db -> update('driver', $data);

					/*****************************************************
					 //get user(Customer)detail
					 /*****************************************************/

					$this -> db -> select("cab_booking.*,driver.*,cab.cab_plate_no,cab_type.title,user.phone,user.country_code");
					$this -> db -> from("cab_booking");
					$this -> db -> join('driver', 'driver.driver_id = cab_booking.driver_id');
					$this -> db -> join('shift', 'shift.driver_id=cab_booking.driver_id');
					$this -> db -> join('cab', 'cab.cab_id=shift.cab_id');
					$this -> db -> join('cab_type', 'cab_type.type_id=cab.cab_type');
					$this -> db -> join("user", "user.id=cab_booking.user_id");
					$this -> db -> where("cab_booking.id", $booking_id);
					$query = $this -> db -> get();
					//echo "<br/>".$this->db->last_query();
					if ($query -> num_rows() > 0) {

						$driver_detail = $query -> result_array();
						$result = array("type" => $type, "driver_detail" => $driver_detail, "status" => 1);
						return $result;
					} else {
						$result = array("type" => $type, "status" => 0, "message" => DRIVER_BUSY_MESSAGE);
						return $result;
						//return false;
					}

				} else {
					$result = array("type" => $type, "status" => 0, "message" => SERVER_ERROR);
					return $result;
					//return false;
				}

			} else {
				$result = array("type" => $type, "status" => 0, "message" => SERVER_ERROR);
				return $result;
			}
		} elseif ($type == 'reject') {
			//make driver Availible after reject Booking
			$query = $this -> db -> query("SELECT `id` FROM `cab_booking` WHERE `driver_id` = $driver_id 
                                    AND (`status` = 'DRIVER_ACCEPTED' 
                                    OR `status` = 'CUSTOMER_ACCEPTED'
                                    OR `status` = 'TRIP_STARTED')LIMIT 1");
			// echo $this->db->last_query();
			if ($query -> num_rows() > 0) {
				$driver_status = "BUSY";
			} else {
				$driver_status = DRIVER_AVAILABLE;
			}
			//$driver_available = DRIVER_AVAILABLE;
			$data = array('status' => $driver_status);
			$this -> db -> where('driver_id', $driver_id);
			$update = $this -> db -> update('driver', $data);

			$date = get_gmt_time();
			$driver_rejected = DRIVER_REJECTED;
			$data = array('booking_id' => $booking_id, 'driver_id' => $driver_id, 'driver_response' => "$driver_rejected", 'driver_datetime' => $date, );
			$str = $this -> db -> insert('cab_booking_history', $data);

			if ($id = $this -> db -> insert_id()) {
				//$driver_available = DRIVER_AVAILABLE;
				//                $data = array('status' => "$driver_rejected");
				//                $this->db->where('id', $booking_id);
				//                $update = $this->db->update('cab_booking', $data);
				$result = array("type" => $type, "status" => 1, "message" => "success");
				return $result;
			} else {
				$result = array("type" => $type, "status" => 0, "message" => SERVER_ERROR);
				return $result;
			}
		}

	}

	function customer_response($input_method) {
		$booking_id = $input_method['booking_id'];
		$type = $input_method['type'];
		$user_id = $input_method['user_id'];

		if ($type == 'accept') {

			/*********************CHECK CUSTOMER HAVE RIGHT TO ACCEPT THIS BOOKING ************
			 **********************************************************************************
			 ********************************************************************************/
			$customer_not_respond = CUSTOMER_NOT_RESPOND;
			$driver_id = $input_method['driver_id'];
			$this -> db -> select("status,id");
			$this -> db -> where("id", $booking_id);
			//$this->db->where("status", "DRIVER_ACCEPTED");
			// $this->db->where_or("status","$customer_not_respond");
			$booking_status = $this -> db -> get("cab_booking");

			if ($booking_status -> num_rows()) {
				/*****************************************************
				 //update driver status
				 /*****************************************************/
				$booking_status_detail = $booking_status -> result_array();
				$booking_status_detail = $booking_status_detail[0];
				if ($booking_status_detail['status'] == 'CUSTOMER_NOT_RESPOND') {
					/*$driver_available = DRIVER_AVAILABLE;
					$data = array('status' => "$driver_available");
					$this -> db -> where('driver_id', $driver_id);
					$update = $this -> db -> update('driver', $data);*/
			        $update=		$this->driver_status($driver_id);
                    if($update){
					$result = array("type" => $type, "status" => 0, "message" => CUSTOMERTIMEOUT);
					}else{
							$result = array("type" => $type, "status" => 0, "message" => SERVER_ERROR);
					}
					return $result;
				} elseif ($booking_status_detail['status'] == 'CANCELLED') {
					/*$driver_available = DRIVER_AVAILABLE;
					$data = array('status' => "$driver_available");
					$this -> db -> where('driver_id', $driver_id);
					$update = $this -> db -> update('driver', $data);*/
                    $update=		$this->driver_status($driver_id);
                    if($update){
				    $result = array("type" => $type, "status" => 0, "message" => CUSTOMER_CANCEL);
					}else{
							$result = array("type" => $type, "status" => 0, "message" => SERVER_ERROR);
					}
					return $result;
					
					
				} elseif ($booking_status_detail['status'] == 'CUSTOMER_ACCEPTED') {
					$result = array("type" => $type, "status" => 0, "message" => CUSTOMER_ALREADY_ACCEPTED);
					return $result;
				} elseif ($booking_status_detail['status'] == "DRIVER_CANCELLED") {
					$result = array("type" => $type, "status" => 0, "message" => DRIVER_CANCEL);
					return $result;
				}
				//else
				//                {
				//                    $result = array(
				//                        "type" => $type,
				//                        "status" => 0,
				//                        "message" => CUSTOMER_CANCEL);
				//                    return $result;
				//                }

			}

			/*************UPDTAE BOOKING TABLE AND HISTORY TABLE STATUS*****************/

			$date = get_gmt_time();
			$history_data = array("user_response" => "CUSTOMER_ACCEPTED", "user_datetime" => $date);
			$this -> db -> where("driver_id", $driver_id);
			$this -> db -> where("booking_id", $booking_id);
			$update = $this -> db -> update("cab_booking_history", $history_data);

			$data = array("status" => "CUSTOMER_ACCEPTED", "customerresponse" => $date);
			$this -> db -> where("id", $booking_id);
			$this -> db -> where("user_id", $user_id);
			$update = $this -> db -> update("cab_booking", $data);

			if ($update) {
				//get customer detail
				$customer_detail = $this -> db -> query("SELECT u.id,u.name,u.phone,u.country_code,d.first_name,d.last_name,b.driver_id,b.booking_number,d.contact,u.email,b.pickup_latitude,b.pickup_longitude,b.destination_latitude,b.destination_longitude,b.pickup_location,b.destination_location,d.latitude,d.longitude,b.id as booking_id,d.driver_id,b.comment FROM `cab_booking` b 
                                    join user u on u.id=b.user_id
                                    join driver d on d.driver_id=b.driver_id
                                    where b.id=$booking_id ");
				if ($customer_detail -> num_rows() > 0) {
					$customer_detail = $customer_detail -> result_array();
					$result = array("type" => $type, "status" => 1, "customer_detail" => $customer_detail);
					return $result;
				} else {
					$result = array("type" => $type, "status" => 1, "message" => BOOKING_NOTFOUND);
					return $result;
				}

			} else {
				$result = array("type" => $type, "status" => 1, "message" => SERVER_ERROR);
				return $result;
			}

		} else {
			/*************************CHK CURRENT BOOKING STATUS*************/
			$this -> db -> select('cab_booking.id,cab_booking.status,cab_booking.driver_id,cab_booking.booking_number,driver.contact');
			$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id", "left");
			$this -> db -> where("cab_booking.id", $booking_id);
			$booking_status = $this -> db -> get("cab_booking");
			if (!$booking_status -> num_rows()) {
				/*****************************************************
				 //update driver status
				 /*****************************************************/
				/*$driver_available = DRIVER_AVAILABLE;
				 $data = array('status' => $driver_available);
				 $this->db->where('driver_id', $driver_id);
				 $update = $this->db->update('driver', $data);
				 */
				$result = array("status" => 0, "type" => $type, "message" => BOOKING_NOTFOUND, "message_customer" => BOOKING_NOTFOUND);
				return $result;
			}
			// print_r($booking_status);
			$booking_status_detail = $booking_status -> result_array();

			$driver_id = $booking_status_detail[0]['driver_id'];
			if ($booking_status_detail[0]['status'] == 'WAITING' || $booking_status_detail[0]['status'] == 'DRIVER_ACCEPTED' || $booking_status_detail[0]['status'] == 'CUSTOMER_ACCEPTED' || $booking_status_detail[0]['status'] == 'CANCELLED' || $booking_status_detail[0]['status'] == 'CUSTOMER_REJECTED') {

				$currentbooking_status = $booking_status_detail[0]['status'];
				$date = get_gmt_time();
				if ($currentbooking_status != "CUSTOMER_ACCEPTED") {
					$DB_status = "CUSTOMER_REJECTED";
					$accept_time = "";
					$history_data = array("user_response" => $DB_status, "user_datetime" => $date);
					$this -> db -> where("driver_id", $driver_id);
					$this -> db -> where("booking_id", $booking_id);
					$update = $this -> db -> update("cab_booking_history", $history_data);

				} else {
					$DB_status = "CANCELLED";
					//CANCLLED
					//get customer accept booking time

					$this -> db -> select("user_datetime");
					$this -> db -> from("cab_booking_history");
					$this -> db -> where("booking_id", $booking_id);
					$query = $this -> db -> get();
					$accept_data = $query -> row();
					$accept_time = $accept_data -> user_datetime;

					$history_data = array("user_cancel" => $DB_status, "user_cancel_time" => $date);
					$this -> db -> where("driver_id", $driver_id);
					$this -> db -> where("booking_id", $booking_id);
					$update = $this -> db -> update("cab_booking_history", $history_data);

				}

				/**********UPADTE BOOKING HISTORY TABLE *********************/

				$data = array("status" => $DB_status, "customerresponse" => $date);
				$this -> db -> where("id", $booking_id);
				$this -> db -> where("user_id", $user_id);
				$update = $this -> db -> update("cab_booking", $data);
				/*$driver_available = DRIVER_AVAILABLE;
				$data = array("status" => $driver_available);
				$this -> db -> where("driver_id", $driver_id);
				$update = $this -> db -> update("driver", $data);*/
				$update=$this->driver_status($driver_id);
				//  echo  $this->db->last_query();
				if ($update) {
					$result = array("type" => $type, "booking_status" => $booking_status_detail[0]['status'], "driver_id" => $driver_id, "CBN" => $booking_status_detail[0]['booking_number'], "bookingid" => $booking_status_detail[0]['id'], "status" => 1, "action" => $DB_status, "accept_time" => $accept_time, "contact" => $booking_status_detail['0']['contact'], "message" => BOOKING_REJECTED_DRIVER, "message_customer" => BOOKING_REJECTED_CUSTOMER);
					return $result;
				}
			} else {
				$result = array("type" => $type, "booking_status" => $booking_status_detail[0]['status'], "driver_id" => $driver_id, "CBN" => $booking_status_detail[0]['booking_number'], "bookingid" => $booking_status_detail[0]['id'], "status" => 0, "action" => $DB_status, "accept_time" => $accept_time, "contact" => $booking_status_detail['0']['contact'], "message" => BOOKING_NOTREJECTED, "message_customer" => BOOKING_REJECTED_CUSTOMER);
				return $result;
			}

		}
	}

	/************************************************************
	 ********************TIMEOUT function Start*******************
	 /***********************************************************/
	function timeout($input_method) {
		$booking_id = $input_method['booking_id'];
		$data = array("status" => "TIMEOUT");
		$this -> db -> where("id", $booking_id);
		$update = $this -> db -> update("cab_booking", $data);
		$this -> db -> last_query();
		if ($update) {
			return true;
		} else {
			return false;
		}

	}

	/************************************************************
	 ********************TIMEOUT function End*******************
	 /***********************************************************/

	/************************************************************
	 ********************TRIP START******************************
	 ************************************************************/
	function trip_start($input_method) {
		$driver_Id = $input_method['driver_id'];
		$booking_Id = $input_method['booking_id'];
		//***************Check Booking condtion
		$this -> db -> select("id,status,driver_id,user_id,is_prebooking");
		$this -> db -> where("driver_id", $driver_Id);
		$this -> db -> where("id", $booking_Id);
		//$this->db->where("status", "CUSTOMER_ACCEPTED");
		$result = $this -> db -> get('cab_booking');
		$date = get_gmt_time();
		if ($result -> num_rows() > 0) {

			$user_detail = $result -> result_array();
			$userid = $user_detail[0]['user_id'];
			$status = $user_detail[0]['status'];
			$is_prebooking = $user_detail[0]['is_prebooking'];
			if ($status == "CUSTOMER_ACCEPTED" || $is_prebooking == 'Y') {
				$data = array("status" => "TRIP_STARTED");
				$where_Condition = array("driver_id" => $driver_Id);
				$this -> db -> where("id", $booking_Id);
				$update = $this -> db -> update("cab_booking", $data);
				$driver_status = array("driver_id" => $driver_Id, "booking_id" => $booking_Id, "status" => "TRIP_STARTED", "createdon" => $date);
				$this -> db -> insert("driver_status", $driver_status);
				if ($update) {

					return $userid;
				}
			} elseif ($status == "TRIP_STARTED") {
				//echo 'hello';

				return $userid;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	/************************************************************
	 ********************TRIP END START******************************
	 ************************************************************/

	function trip_end($input_method) {
		$driver_Id = $input_method['driver_id'];
		$booking_Id = $input_method['booking_id'];
		$km_driven = $input_method['km_driven'];
		$journey_time = $input_method['journey_time'];
		// $waiting_time = $input_method['waiting_time'];
		//***************Check Booking condtion*********
		$this -> db -> select("`cab_booking`.`id`, `cab_booking`.`status`, `cab_booking`.`driver_id`,cab_booking.booking_number,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.plate_number,cab_booking.discount_amount, `cab_type`, `user_id`, `pickup_latitude`, `pickup_longitude`, `destination_latitude`, `destination_longitude`, `driver`.`driver_id`, driver.first_name,driver.last_name, contact,driver.profile_pic,user.country_code,user.phone");
		//$this->db->select("*");
		$this -> db -> where("cab_booking.driver_id", $driver_Id);
		$this -> db -> where("cab_booking.id", $booking_Id);
		//$this->db->where("cab_booking.status", "TRIP_STARTED");
		$this -> db -> from("cab_booking");
		$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
		$this -> db -> join("user", "user.id=cab_booking.user_id");
		$result = $this -> db -> get();

		if ($result -> num_rows() > 0) {
			$trip_data = $result -> result_array();
			// echo $trip_data[0]['status'] ;
			if ($trip_data[0]['status'] == 'TRIP_STARTED') {
				// ADDTHE DRIVER STATUS TABLE
				$date = get_gmt_time();
				$driver_status = array("driver_id" => $driver_Id, "booking_id" => $booking_Id, "status" => "TRIP_ENDED", "createdon" => $date);
				$this -> db -> insert("driver_status", $driver_status);

				//UPDATE DRIVER STATUS IN SHIFT TABLE...if already accept booking make busy 
			$query = $this->db->query("SELECT `id` FROM `cab_booking` WHERE `driver_id` = $driver_Id
                                    AND (`status` = 'DRIVER_ACCEPTED' 
                                    OR `status` = 'CUSTOMER_ACCEPTED'
                                    OR `status` = 'TRIP_STARTED')LIMIT 1");
            //echo $this->db->last_query();
            if ($query->num_rows() > 0)
            {
                $driver_status = "BUSY";
            }
            else
            {
                $driver_status = DRIVER_AVAILABLE;
            }
			//$driver_available = DRIVER_AVAILABLE;
			
				$data = array("status" => $driver_status);
				$this -> db -> where("driver_id", $driver_Id);
				$update = $this -> db -> update("driver", $data);
              //   echo $this->db->last_query();
				if ($update) {
					return array("status" => 1, "trip_detail" => $result -> result_array());

				} else {
					$result = array("status" => 0, "message" => "SERVER_ERROR");
					return $result;
				}
			} elseif ($trip_data[0]['status'] == 'TRIP_ENDED' || $trip_data[0]['status'] == 'FINISH') {
				$result = array("status" => 0, "message" => ALREADY_TRIP_END);
				return $result;
			} else {
				$result = array("status" => 0, "message" => TRIP_ENDED_ERROR);
				return $result;
			}
		} else {
			$result = array("status" => 0, "message" => BOOKING_NOTFOUND);
			return $result;
		}

	}

	function get_extra_order($booking_id) {
		$this -> db -> select("booking_extra_order.item_id,booking_extra_order.item_price,booking_extra_order.quantity,item_info.item_name");
		$this -> db -> where("booking_id", $booking_id);
		$this -> db -> from("booking_extra_order");
		$this -> db -> join("item_info", "item_info.item_id=booking_extra_order.item_id");
		$query = $this -> db -> get();
		// echo $this->db->last_query();
		if ($query -> num_rows()) {
			return $query -> result_array();
		} else {
			return false;
		}
	}

	function save_booking_payment($data) {
		$payment = $this -> db -> insert("payment_detail", $data);
		//echo $this->db->last_query();
		$booking_Id = $data['booking_id'];
		if ($this -> db -> insert_id()) {
			//update the booking status
			$update_data = array("status" => "TRIP_ENDED");
			$this -> db -> where("id", $booking_Id);
			$upadte = $this -> db -> update("cab_booking", $update_data);
			if ($upadte) {
				return true;
			} else {
				return false;
			}

		}
		return false;

	}

	function update_driver_status($data) {
		$driver_id = $data['driver_id'];
		$status = $data['status'];
		$upadte_data = array("status", $status);
		$this -> db -> where("driver_id", $driver_id);
		$update = $this -> db -> update("driver", $data);
		if ($update) {
			return true;
		} else {
			return false;
		}
	}
	//check the driver booking detail and make chnages in status
	function driver_status($driver_id)
	{
			$query = $this->db->query("SELECT `id` FROM `cab_booking` WHERE `driver_id` = $driver_id
                                    AND (`status` = 'DRIVER_ACCEPTED' 
                                    OR `status` = 'CUSTOMER_ACCEPTED'
                                    OR `status` = 'TRIP_STARTED')LIMIT 1");
            //echo $this->db->last_query();
            if ($query->num_rows() > 0)
            {
                $driver_status = "BUSY";
            }
            else
            {
                $driver_status = DRIVER_AVAILABLE;
            }
			//$driver_available = DRIVER_AVAILABLE;
			
				$data = array("status" => $driver_status);
				$this -> db -> where("driver_id", $driver_Id);
				$update = $this -> db -> update("driver", $data);
				if($update)
				{
					return true;
				}else
					{
						return false;
					}
	}

	/************************************************************
	 ********************TRIP END START******************************
	 ************************************************************/
	/* function for ride */
	function my_ride($input_method) {
		$customer_id = $input_method['customer_id'];
		$type = $input_method['type'];
		$pagenumber = @$input_method['page_number'];
		if ($pagenumber == "") {
			$pagenumber = 1;
		}
		//$pagenumber = $input_method['page_number'];
		$limit = 10;
		if ($pagenumber == 1) {
			$offset = 0;
		} else {
			$pagenumber = $pagenumber - 1;
			$offset = $pagenumber * 10;
		}

		if ($type == 'UPCOMING') {
			$query = $this -> db -> query("select *  from cab_booking  where user_id='$customer_id' and (status='TRIP_STARTED' OR status='ADVANCE_BOOKING'  OR status='WAITING' OR status='CUSTOMER_ACCEPTED') order by cab_booking.id desc limit $offset,$limit");
			return $query -> result();

		} elseif ($type == 'ALL') {
			$query = $this -> db -> query("select b.id,b.booking_number,b.driver_id,b.plate_number,b.pickup_latitude,b.pickup_longitude,b.destination_latitude,b.destination_longitude,b.pickup_location,b.destination_location,b.booking_time,b.status,pd.booking_id,pd.journey_time,pd.billing_amount,pd.km_driven,pd.waiting_time,pd.waiting_charge,pd.other_charge,pd.tip,pd.user_paid,pd.is_paid,ct.title 
                                       from cab_booking b 
                                       left join payment_detail pd on pd.booking_id=b.id 
                                       left join cab_type ct on b.cab_type=ct.type_id
                                       where user_id='$customer_id' and (status ='CUSTOMER_NOT_RESPOND' OR status='CUSTOMER_ACCEPTED' OR status='CANCELLED' OR  status='FINISH' OR status='TRIP_ENDED') 
                                       order by b.id desc limit $offset,$limit");
			// echo $this->db->last_query();
			return $query -> result();
		}
	}

	function getfare_detail($cab_type) {
		$result = $this -> db -> get_where("fare_info", array("cab_type" => $cab_type, "type" => "NORMAL"));
		if ($result -> num_rows() > 0) {
			return $result -> result_array();
		} else {
			return false;
		}

	}

	/* work for admin get advacne booking detal */
	function fetch_advance_booking($limit, $offset = 0) {  $date = date('Y-m-d');
		$result = $this -> db -> query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name FROM `cab_booking` join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and date(booking_time)>='$date' ORDER BY booking_time ASC  limit $offset,$limit");
		// echo $this->db->last_query();
		if ($result -> num_rows() > 0) {
			return $result -> result_array();
		} else {
			return false;
		}
	}

	/*fetch count aqdvance booking */
	function count_advance_booking() {
		$date = date('Y-m-d');
		$result = $this -> db -> query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name FROM `cab_booking` join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and date(booking_time)>='$date'");
		// echo $this->db->last_query();
		return $result -> num_rows();

	}

	/*fetch drive rfor admin work web */
	function fetch_driver() {
		$result = $this -> db -> query("select * from driver");
		if ($result -> num_rows() > 0) {
			return $result -> result_array();
		} else {
			return false;
		}

	}

	/* update advance booking admin*/
	function update_booking($bookingid, $driverid) {
		$driver_assgin = get_gmt_time();

		$result = $this -> db -> query("Update cab_booking SET driver_assigned_date='$driver_assgin',driver_id=$driverid,status='DRIVER_ASSIGNED' where id='$bookingid'");

		if ($result) {
			return 1;
		} else {
			return 0;
		}
	}

	function get_customer_number($user_id) {
		$this -> db -> select("country_code,phone");
		$this -> db -> where("id", $user_id);
		$query = $this -> db -> get("user");
		if ($query -> num_rows() > 0) {
			$data = $query -> result_array();
			return $data;
		} else {
			return false;
		}
	}

	function get_driver_number($driver_id) {
		$this -> db -> select("contact");
		$this -> db -> where("driver_id", $driver_id);
		$query = $this -> db -> get("driver");
		//echo $this->db->last_query();
		if ($query -> num_rows() > 0) {
			$data = $query -> result_array();
			return $data;
		} else {
			return false;
		}
	}

	/* fetch general info */
	function fetch_general($input_method) {
		$user_id = @$input_method['user_id'];
		$query = $this -> db -> query("select item_id,UPPER(item_name) item_name,item_price,item_code from item_info where isactive='1'");
		if ($query -> num_rows() > 0) {
			$item = $query -> result_array();
		}
		$fare = $this -> db -> query("select description,no_of_person as person_count,min_charges,min_km,charges_per_km,wait_time_charges,ride_later_time,cab_type.title,cab_type.type_id as cab_type from fare_info join cab_type on fare_info.cab_type=cab_type.type_id where fare_info.type='NORMAL' and cab_type.isactive='1'");
		if ($fare -> num_rows() > 0) {
			$fare_data = $fare -> result_array();
		}
		if ($user_id) {
			//get the ride info
			//get the current runing info

			$this -> db -> select("driver.driver_id,driver.profile_pic,driver.first_name,driver.last_name,driver.latitude,driver.longitude,cab_booking.id as booking_id,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.booking_number,cab_type.title,cab_booking.plate_number,driver.contact,cab_booking.status");
			$this -> db -> from("cab_booking");
			$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
			$this -> db -> join("cab_type", "cab_type.type_id=cab_booking.cab_type");
			// $this->db->join("driver_status", "driver_status.booking_id=cab_booking.id");
			$this -> db -> where("cab_booking.user_id", $user_id);
			$this -> db -> where("cab_booking.status", "TRIP_STARTED");
			$query = $this -> db -> get();
			// echo $this->db->last_query();
			if ($query -> num_rows() > 0) {
				$booking_alert = $query -> result_array();
			} else {
				$this -> db -> select("driver.driver_id,driver.profile_pic,driver.first_name,driver.last_name,driver.latitude,driver.longitude,cab_booking.id as booking_id,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.booking_number,cab_type.title,cab_booking.plate_number,driver.contact,cab_booking.status");
				$this -> db -> from("cab_booking");
				$this -> db -> join("driver", "driver.driver_id=cab_booking.driver_id");
				$this -> db -> join("cab_type", "cab_type.type_id=cab_booking.cab_type");
				$this -> db -> join("driver_status", "driver_status.booking_id=cab_booking.id");
				$this -> db -> where("cab_booking.user_id", $user_id);
				$this -> db -> where("cab_booking.status", "CUSTOMER_ACCEPTED");
				// echo $this->db->last_query();
				if ($query -> num_rows() > 0) {
					$booking_alert = $query -> result_array();
				} else {
					$booking_alert = array();
				}

			}

		} else {
			$booking_alert = array();
		}
		// print_r($booking_alert);
		return array("item" => $item, "fare" => $fare_data, "booking_alert" => $booking_alert);
	}

	/*fetch completed booking */
	function fetch_completed_booking($limit, $offset = 0) {
		$fetch_data = $this -> db -> query("select c.booking_number,u.name,c.booking_time,c.pickup_location,c.destination_location,p.billing_amount,p.km_driven,p.user_paid
from cab_booking as c,payment_detail as p,user as u where p.booking_id=c.id and c.status='FINISH' and u.id=c.user_id limit $offset,$limit");
		// echo $this->db->last_query();
		if ($fetch_data -> num_rows() > 0) {
			return $fetch_data -> result_array();
		} else {
			return false;
		}
	}

	/* fetch total cash */
	function total_cash() {
		$total_amount = $this -> db -> query("SELECT SUM(user_paid) as total_amount FROM `payment_detail`");
		if ($total_amount -> num_rows() > 0) {
			return $total_amount -> result_array();
		} else {
			return false;
		}
	}

	/* function for fetch comopany due payment */
	function company_due($limit, $offset = 0) {
		$total_due = $this -> db -> query("SELECT user.name,user.email,cab_booking.booking_number,p.billing_amount,p.id,p.is_paid FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where cab_booking.status='TRIP_ENDED' and user.is_company='Y' and p.is_paid='0'limit $offset,$limit");
		if ($total_due -> num_rows() > 0) {
			return $total_due -> result_array();
		} else {
			return false;
		}
	}

	/*fetch particular user payment for making pdf */
	function company_user_due($b_number) {
		//echo $uid;
		if ($b_number) {
			$total_due = $this -> db -> query("SELECT user.name,user.company_name, cab_booking.booking_number,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude,user.id as uid,user.name,user.email,p.is_paid,p.billing_amount,p.km_driven FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where cab_booking.status='TRIP_ENDED' and user.is_company='Y' and cab_booking.booking_number='$b_number'");
			//echo "<br>".$this->db->last_query();
			if ($total_due -> num_rows() > 0) {
				return $total_due -> result_array();
			} else {
				return false;
			}

		}
	}

	/* fetch count due user payment */
	function count_company_due() {
		$result = $this -> db -> query("SELECT user.name,user.email,cab_booking.booking_number,p.billing_amount,p.id,p.is_paid FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where cab_booking.status='TRIP_ENDED' and user.is_company='Y' and p.is_paid='0'");
		return $result -> num_rows();
	}

	/* chnage payment status */
	function change_payment_status($id, $status) {
		if ($status == "1") {
			$field_array = array("is_paid" => "1");
			$status == '1';
		} else {
			$field_array = array("is_paid" => "0");
			$status == '0';
		}
		$this -> db -> where('id', $id);
		$this -> db -> update("payment_detail", $field_array);
		//echo "<br>".$this->db->last_query();
		return $status;
	}

	/* fetch complete payment detail */

	function Complete_payment_detail($start_date, $end_date, $c_id, $limit, $offset = 0) {

		$complete_booking = $this -> db -> query("SELECT user.name,user.phone,user.company_name,user.email,cab_booking.booking_number,p.billing_amount,p.km_driven,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude,cab_booking.pickup_location,cab_booking.destination_location FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where user.is_company='Y' and user.company_id='$c_id' and date(cab_booking.booking_time) BETWEEN '$start_date' AND '$end_date' limit $offset,$limit");

		// $complete_booking = $this->db->query("SELECT user.name,user.phone,user.company_name,user.email,cab_booking.booking_number,p.billing_amount,p.km_driven,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where user.is_company='Y' and p.is_paid='1' ");
		if ($complete_booking -> num_rows() > 0) {
			return $complete_booking -> result_array();
		} else {
			return false;
		}
	}

	/* fetch count complete payment detail */
	function count_compelte_payment($start_date, $end_date, $c_id) {

		$complete_booking = $this -> db -> query("SELECT user.name,user.phone,user.company_name,user.email,cab_booking.booking_number,p.billing_amount,p.km_driven,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where user.is_company='Y' and user.company_id='$c_id' and date(cab_booking.booking_time) BETWEEN '$start_date' AND '$end_date'");
		return $complete_booking -> num_rows();

	}

	/* fetch user detail on basis of if */
	function fetch_user_paymentdetail($id) {

		$fetch_user_detail = $this -> db -> query("SELECT cab_booking.booking_number,p.billing_amount,cab_booking.booking_time,cab_booking.pickup_latitude,cab_booking.pickup_longitude,cab_booking.destination_latitude,cab_booking.destination_longitude FROM `cab_booking` join user on user.id=cab_booking.user_id left join payment_detail p on p.booking_id=cab_booking.id where  user.id='$id' and p.is_paid='1'");
		if ($fetch_user_detail -> num_rows > 0) {
			return $fetch_user_detail -> result_array();
		} else {
			return false;
		}

	}

	/* update payment status */

	function update_payment($bno, $pstatus, $description) {
		$update_pstatus = $this -> db -> query("UPDATE payment_detail SET is_paid='$pstatus',description='$description' where id='$bno'");
		if ($update_pstatus) {
			return true;
		} else {
			return false;
		}

	}

	/* function for tracking driver */

	function fetch_completed_booking_count() {
		$fetch_data = $this -> db -> query("select c.id,c.booking_number, c.booking_time,c.pickup_latitude,c.pickup_longitude,c.destination_latitude,c.destination_longitude,p.billing_amount,p.km_driven,p.user_paid
from cab_booking as c,payment_detail as p where p.booking_id=c.id and c.status='FINISH'");
		if ($fetch_data -> num_rows() > 0) {
			return $fetch_data -> num_rows();
		} else {
			return 0;
		}
	}

	/* fetch current booking count */

	function fetch_current_booking_count() {
		$fetch_current = $this -> db -> query("select c.driver_id,c.id, c.booking_number,c.status,c.booking_time,d.first_name,d.last_name,d.contact,c.id from cab_booking as c,driver as d where d.driver_id=c.driver_id and c.status='TRIP_STARTED'");
		if ($fetch_current -> num_rows() > 0) {
			return $fetch_current -> num_rows();

		} else {
			return false;
		}
	}

	/* fetch current booking */
	function fetch_current_booking() {
		$fetch_current = $this -> db -> query("select c.driver_id,c.booking_number,c.status,c.booking_time,d.first_name,d.last_name,d.contact,c.id from cab_booking as c,driver as d where d.driver_id=c.driver_id and c.status='TRIP_STARTED'");
		if ($fetch_current -> num_rows() > 0) {
			return $fetch_current -> result_array();
		} else {
			return 0;
		}
	}

	function user_exist($user_id) {
		$this -> db -> select("user.id,user.is_company,card_info.braintree_customer_id");
		$this -> db -> where("user.id", $user_id);
		$this -> db -> from("user");
		$this -> db -> join("card_info", "card_info.customer_id=user.id", "left");
		$query = $this -> db -> get();
		if ($query -> num_rows()) {
			$user_detail = $query -> result_array();
			if ($user_detail[0]['braintree_customer_id'] != "") {
				return array("status" => 1, "message" => "success");
			} elseif ($user_detail[0]['braintree_customer_id'] == "" && $user_detail[0]['is_company'] == "Y") {
				return array("status" => 1, "message" => "success");
			} else {
				return array("status" => 2, "message" => CARD_INFO_MISSING);
			}

		} else {
			return array("status" => 0, "message" => USER_NOT_EXIST);
		}
	}

	function get_cab_plate_number($driver_id) {
		$shift_start = DRIVER_SHIFT_START;
		$this -> db -> select("cab_plate_no");
		$this -> db -> from("shift");
		$this -> db -> join("cab", "cab.cab_id=shift.cab_id");
		$this -> db -> where("shift.status", "$shift_start");
		$this -> db -> where("shift.driver_id", $driver_id);
		$query = $this -> db -> get();
		//echo $this->db->last_query();
		if ($query -> num_rows() > 0) {
			$data = $query -> result_array();
			return $data[0]['cab_plate_no'];
		} else {
			return "";
		}
	}

	function get_coupon_detail($coupon_code) {
		//  $user_id=$input_method['user_id'];
		$today = date("Y-m-d");
		//$coupon_code=$input_method['coupon_code'];
		$this -> db -> select("*");
		$this -> db -> from("coupon");
		$this -> db -> where("code", $coupon_code);
		$this -> db -> where("isactive", "1");
		$this -> db -> where("start_date<=", $today);
		$this -> db -> where("expire_date>=", $today);
		$query = $this -> db -> get();
		// echo $this->db->last_query();
		if ($query -> num_rows()) {
			return $query -> result_array();
		} else {
			return false;
		}

	}

	function driver_track($bid) {
		$track = $this -> db -> query("SELECT * FROM `driver_status` where booking_id='$bid' and (status='FIVE_MIN_AWAY' OR 'I_AM_HERE') limit 1 ");
		if ($track -> num_rows > 0) {
			return "Y";
		} else {
			return "N";
		}
	}

	function user_braintree_token($user_id) {
		$this -> db -> select("braintree_token");
		$this -> db -> from("card_info");
		$this -> db -> where("customer_id", $user_id);
		$this -> db -> where("isactive", "1");
		$query = $this -> db -> get();
		if ($query -> num_rows()) {
			$token_data = $query -> result_array();

			return $token_data[0]['braintree_token'];
		} else {
			return false;
		}

	}

	function save_payment($data) {
		$this -> db -> insert("payment_detail", $data);
		//echo $this->db->last_query();
		return $this -> db -> insert_id();
	}

	/* fetch company detail */
	function get_company() {
		$get_company = $this -> db -> query("select * from company where isdelete='1'");
		$count = $get_company -> num_rows();
		if ($count >= 0) {
			return $get_company -> result();
		} else {
			return array();
		}
	}

	function get_luxury_pre_booking() {
		$today = date("Y-m-d");
		$query = $this -> db -> query("select id,booking_time,status from cab_booking where cab_type='3' and date(booking_time)='$today'");
		// echo $this->db->last_query();
		if ($query -> num_rows() > 0) {
			return $query -> result_array();
		} else {
			return false;
		}
	}

	function update_booking_status($data) {
		$query = $this -> db -> query("update cab_booking set status='" . $data['status'] . "' where id='" . $data['booking_id'] . "'");
		if ($query) {
			//make driver available
			$driver_status = DRIVER_AVAILABLE;
			$driver_id = $data['driver_id'];
			$this -> db -> where("driver_id", $driver_id);
			$update_data = array("status" => $driver_status);
			$update_query = $this -> db -> update('driver', $update_data);
			return true;
		} else {
			return false;
		}

	}

	function getdriver_currentbooking($driver_id) {
		$query = $this -> db -> query("SELECT pickup_latitude,pickup_longitude,destination_latitude,destination_longitude,status FROM `cab_booking` WHERE driver_id=$driver_id and status not in ('NOT_SHOW','FINISH','CANCELLED','TRIP_ENDED','DRIVER_CANCELLED','CUSTOMER_REJECTED','CUSTOMER_NOT_RESPOND','DRIVER_ASSIGNED','ADVANCE_BOOKING')");
		//echo $this->db->last_query();
		if ($query -> num_rows() > 0) {
			$result = $query -> result_array();
			$destination_latitude = $result[0]['destination_latitude'];
			$destination_longitude = $result[0]['destination_longitude'];
			$data = array("destination_latitude" => $destination_latitude, "dda"=>"12","destination_longitude" => $destination_longitude, "status" => 1);
			return $result;
		} else {
			return array("status" => 0, "message" => "No current booking");
		}
	}
	
	
	function get_pre_booking($booking_no)
	{
		$get_advance_booking=$this->db->query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name FROM `cab_booking` join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.is_prebooking='Y' and  cab_booking.booking_number='$booking_no'");
		return $get_advance_booking->result();
	}
	function getdriverstatus($driver_id)
	{
		//$query =$this->db->query("select status from driver where driver_id=$driver_id");
		$query = $this -> db -> query("SELECT pickup_latitude,pickup_longitude,destination_latitude,destination_longitude,status FROM `cab_booking` WHERE driver_id=$driver_id and status not in ('NOT_SHOW','FINISH','CANCELLED','TRIP_ENDED','DRIVER_CANCELLED','CUSTOMER_REJECTED','CUSTOMER_NOT_RESPOND','DRIVER_ASSIGNED','ADVANCE_BOOKING')");
	
		if($query->num_rows()>0)
		{
			return  "BUSY";
			//return $query->result_array();
		}else{
			return false;
		}
		
	}
	
	/*get previous driver */
	function get_previous_driver($bookingid)
	{
		$get_driver_no=$this->db->query("select d.contact,c.booking_number from cab_booking as c,driver as d where c.is_prebooking='Y' and id='$bookingid' and c.driver_id=d.driver_id and  c.status= 'DRIVER_ASSIGNED'");
		$count=$get_driver_no->num_rows();
		if($count)
		{
			return $get_driver_no->result_array();
		}
		else
		{
			return array();
		}
	}
	
	function iscompany_user($booking_id)
	{
		$query=$this->db->query("select u.id,is_company from cab_booking b join user u  on u.id=b.user_id where b.id=$booking_id");
		if($query->num_rows()>0)
		{
			$data=$query->result_array();
			return $data[0]['is_company'];
		}else{
			return false;
		}
	}

}
?>
