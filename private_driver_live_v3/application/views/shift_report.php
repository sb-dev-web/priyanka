<link rel="stylesheet" type="text/css" href="<?php echo base_url("plugins/daterangepicker/daterangepicker.css");?>">
<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Driver Timeline Report
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Driver Timeline Report</li>
          </ol>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         
                   
          
          
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
             
             
            </div>
            <div class="box-body">
                
             <label>Date</label> <label style="padding-left:25%;">Select Driver</label>
             
            <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>                                              
              </div>
            <input type="text" class="form-control pull-left" id="bd" name="bd"  style="width:25%;" value="<?php echo $this->session->userdata('booking_date');?>"/>
           <span style="margin-left: 5px"></span>
            <select id="driverselect" style="width:250px; height:35px;" >
                 <option value="">Please Select</option>
                <?php
                   for ($j = 0; $j< count($fetch_driver); ++$j) {
                ?>
                 <option value="<?php echo $fetch_driver[$j]->driver_id;?>"><?php echo $fetch_driver[$j]->first_name.' '. $fetch_driver[$j]->last_name;?></option>
            <?php
              }
              ?>
            </select>
            <button type="submit" class="btn btn-primary" id="submit" name="submit" style="margin-left:35px;height:32px;" onclick="getbookingdata();">Submit</button>
           
            </div>
            
              <br/>
               <span id="loader_note" class="loader" style="display: none;">
                            <img src="http://dev.privatedriverapp.com/app/images/loader19.gif" alt=""/>
                        </span>
            <div id="showreport" style="padding-top: 15px; background-color: #e9f0f5;">
                  
            </div>
            
            <div id="show_report_data"></div>
        

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy;2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
 <script src="<?php echo base_url();?>js/jquery.select2.js"></script>

<script>
  
     $(function() {
     	$('#bd').datepicker({ dateFormat: 'yyyy-mm-dd', autoclose: true });

                  
                      });

</script>
<script>
    $("#driverselect").select2();
  function  getbookingdata()
    {
        var booking_date=$('#bd').val();
        var driverid=$('#driverselect').val();
       
        if (driverid=="") {
        alert('Please Select Driver');
        }
        else if(booking_date=="")
        {
            alert("Please select the date range");
        }
    else
    {
     
       $.ajax({
            type:'POST',
            url: '<?php echo site_url('DriverTimeline');?>',
            data:{"date":booking_date,"driverid":driverid},
             beforeSend: function() {
             //	alert();
              $("#loader_note").css('display',"inline");
           },
            success: function(response) {
            $("#loader_note").css("display","none");
             $("#showreport").html(response);
             }});
    }
    }
</script>
