<?php

class Customerreport_model extends CI_Model {

	public function __construct() {
		$this -> load -> database();
	}



function CustomerBookingReport($booking_date, $user_id)
    {
        $date = explode('to', $booking_date);
        $start_date = $date[0];
        $end_date = $date[1];
		$query="select count(id) booking_count,user_id, status from cab_booking where user_id='$user_id'and date(createdon) BETWEEN '$start_date ' AND '$end_date' and status in('FINISH','CANCELLED','TRIP_ENDED','CUSTOMER_REJECTED') group by STATUS";
        $result = $this->db->query($query);
      //echo $this->db->last_query();
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }
    }
	
	function BooingDetail_()
	{
		 $customer_id=$_POST['customer_id'];
		   $type=$_POST['type'];
		   $date=$_POST['date'];
		   $date = explode('to', $date);
        $start_date = $date[0];
        $end_date = $date[1];
		  
		  if($type=='reject')
		  {
		  	$con=" and cab_booking.status='CUSTOMER_REJECTED'";
		  }elseif($type=='complete')
		  {
		  		$con=" and cab_booking.status in ('FINISH','TRIP_ENDED')";
		  }else
		  	{
		  		$con=" and cab_booking.status='CANCELLED'";
		  	}
		  
		  $query="SELECT cab_booking.id ,payment_detail.user_paid, is_prebooking,cab_booking.booking_number, cab_booking.id, `pickup_location`, `destination_location`, cab_booking.booking_type, cab_booking.booking_time, concat(first_name, ' ' ,last_name) name,cab_booking.status
FROM `cab_booking`

left JOIN `driver` ON `cab_booking`.`driver_id`=`driver`.`driver_id`
left join payment_detail on payment_detail.booking_id=cab_booking.id 
WHERE `cab_booking`.`user_id` = '$customer_id'
AND `booking_time` >= '$start_date'
AND `booking_time` <= '$end_date' $con ";
		  $query= $this->db->query($query);
		 // echo $this->db->last_query();
		  if($query->num_rows()>0)
		  {
		  	return $query->result_array();
		  }else{
		  	return false;
		  }
		   
	}

     		function BooingDetail()
		{
			$requestData = $_REQUEST;

			$columns = array(
				// datatable column index  => database column name
				0 => '',
				1 => 'dob',
				2 => 'email',
				4 => 'phone',
				5=>"status"
			);
			
			//$company_id=4;
			$query = $this -> db -> get_where('user');
			//	echo $this->db->last_query();

			$totalData = $query -> num_rows();
			//echo $totalFiltered = $totalData;
			//exit;

			$sql = "SELECT booking_number, `id`, `pickup_location`, `destination_location`, `booking_type`, booking_time,is_prebooking, concat(first_name, ' ' ,last_name)
FROM `cab_booking`

JOIN `driver` ON `cab_booking`.`driver_id`=`driver`.`driver_id`

WHERE `cab_booking`.`user_id` = '$customer_id'
AND `booking_time` >= '$start_date'
AND `booking_time` <= '$end_date'  ";
			//exit;
			if (!empty($requestData['search']['value']))
			{
				// if there is a search parameter, $requestData['search']['value'] contains
				// search parameter
				$sql .= " AND ( booking_number LIKE '" . $requestData['search']['value'] . "%' ";
				$sql .= " OR pickup_location LIKE '%" . $requestData['search']['value'] . "%' ";
				$sql .= " OR destination_location LIKE '%" . $requestData['search']['value'] . "%' ";
				$sql .= " OR first_name LIKE '%" . $requestData['search']['value'] . "%' )";
			}
			$query = $this -> db -> query($sql);
			//echo $this->db->last_query();
			//exit;
			$totalFiltered = $query -> num_rows();

			$sql .= " ORDER BY  id asc limit " . $requestData['start'] . " ," . $requestData['length'] . "   ";

			$query = $this -> db -> query($sql);
			// $this->db->last_query();
			$data = array();

			// preparing an array
			foreach ($query->result() as $row)
			{
				//print_r($row);
				$nestedData = array();
				$id = $row -> id;
				$company_name=($row->company_name=='') ?"--" : $row->company_name;
	$onclick='changeStatus('.$row->id.',"loading_'.$row->id.'","span_'.$row->id.'")';
				$action="<a href='javascript:void(0);' onclick='$onclick'>";
                                  if($row->is_active=='0'){
                             $action.="<span id='span_$row->id' class='label label-warning'>Inactive</span></a>";
                                    }else{
                                $action.="<span id='span_$row->id' class='label label-success'>Active</span></a>";
                                }
                               $action.=" <br /><div id='loading_$row->id;' style='display: none;'><img src= 'base_url()/images/loader19.gif' /></div>
				";
				
				if($row->is_prebooking=='Y')
				{
					$bookingtime= $row->booking_time;
				}else
					{
						$bookingtime=get_norway_time($row->booking_time);
					}
				$nestedData[] = $row -> booking_number." ( ". $company_name ." )";
				$nestedData[] = $row -> pickup_location;
				$nestedData[] = $row -> destination_location;
				$nestedData[] = $row -> driver_name;
				$nestedData[]=$bookingtime;
				//$nestedData[] = $this -> UserBookingCount($id);
				//$nestedData[] = $action;
				$data[] = $nestedData;
			}

			$json_data = array(
				"draw" => intval($requestData['draw']), // for every request/draw by clientside ,
				// they send a number as a parameter, when they recieve a response/data they
				// first check the draw number, so we are sending same number in draw.
				"recordsTotal" => intval($totalData), // total number of records
				"recordsFiltered" => intval($totalFiltered), // total number of records after
				// searching, if there is no searching then totalFiltered = totalData
				"data" => $data // total data array
			);
			//print_r($json_data);
			return $json_data;
			//echo json_encode($json_data);  // send data as json format
		}


	function drivertime_report($starttime,$endtime,$driver_id)
	{
		//$query="select * from driver_status where driver_id=$driver_id and date(createdon)  between '$starttime' and '$endtime' and status not in ('FIVE_MIN_AWAY','I_AM_HERE ') ";
	    $query="select * from driver_status where driver_id=$driver_id and date(createdon) = '$starttime' and status not in ('FIVE_MIN_AWAY','I_AM_HERE ') ";
	    $res=$this->db->query($query);
		
		//echo $this->db->last_query();
		if($res->num_rows()>0)
		{
			return $res->result_array();
		}else
		{
			return false;
		}
		
	}
	function get_shiftdetail($shift_id)
	{
		$query ="SELECT t.title,c.cab_model,c.cab_plate_no FROM `shift` s 
		join cab c on c.cab_id =s.cab_id 
		join cab_type t on t.type_id=c.cab_type 
		where s.s_id=$shift_id";
		$res=$this->db->query($query);
	//	echo $this->db->last_query();
		if($res->num_rows()>0)
		{
			return $res->row_array();
		}else
			{
				return false;
			}
		
	}
}