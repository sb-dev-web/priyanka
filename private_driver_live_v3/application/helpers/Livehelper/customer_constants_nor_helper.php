<?php

define("ALREADY_REGISTER","Oops! Det virker som om du allerede er registrert.");

define("REGISTER_SUCCESS","Takk for registrering.");

define("EMAIL_NOT_EXIST","Denne e-posten er ikke registrert, vennligst sjekk e-postadressen igjen.");

define("CUSTOMER_LOGIN_ERROR","E-Post eller passord er feil.Vennligst prøv igjen.");

define("CUSTOMER_IMAGE_PATH","/images/user_profile/");

define("COMPANY_ACCOUNT_CREATE","Søknad er mottatt. Du vil motta en e-post når søknaden er godkjent.");

define("COMPANY_ALREADY_REGISTER","Denne e-postadressen er allerede i bruk. Vennligst velg en annen.");

define("CUSTOMER_NOT_RESPOND","CUSTOMER_NOT_RESPOND");

define("NO_RIDE_EXIST","Oops ! Ingen noen ride eksisterer.");

define("USER_NOT_EXIST","Denne brukeren ikke eksisterer Vennligst logg inn og bruke dette programmet.");

define("CUSTOMER_CANCEL","Oops! Denne bestillingen kanselleres av deg.");

define("CUSTOMER_NOT_APPROVE","Kunden ikke godkjenne denne bestillingen.");

define("CUSTOMER_ALREADY_ACCEPTED","du allerede har akseptert dette booking.");

define("CARD_INFO_MISSING","Vennligst legg til din CC informasjon.");

define("NO_OFFER_EXIST","Det er for tiden ingen kampanjer.");

define("PASSWORD_NOT_MATCH","Oops! du gamle passordet ikke samsvarer.");

define("BRAINTREE_USER_ERROR","Oops! den nye profilen ikke lagres sammen med CC info.");

define("CC_SUCCESS","Kredittkort infomation oppdatert.");

define("PASSWORD_SUCCESS","Passordet ditt er oppdatert.");

define("PROFILE_UPDATE","Profilen din er oppdatert.");

define("COUPON_NOT_VAILD","Ugyldig kampanjekode");

define("COUPON_ALREADY_USED","Hei. Du har allerede benyttet denne kampanjen.");

define("OTP_RESET_CODE","OTP sendt.");

define("ACCOUNT_VERIFY","Kontoen er allerede bekreftet.");

define("NOT_APPROVE","Oops! Katalog brukerkonto må godkjenne av admin.");

define("EMAIL_NOT_VERIFY","Dine e-postadresse er ikke verifisert. Vennligst verifiser din e-postadresse");

define("APP_USER_MESSAGE","Din forespørsel er mottatt. Vi vil komme raskt tilbake til deg.. ");

define("EMAIL_ALREADY_REGISTER","Oops! det virker som din <email> allerede er registrert. ");

define("MOBILE_ALREADY_REGISTER","Oops! det virker som din <mobile> allerede er registrert. ");

define("APP_CODE_ALREADY_USED","Oops! Denne koden allerede brukt Vennligst foreta en ny forespørsel om aktiv kode");

define("APP_CODE_NOTEXIST","Appkoden er ikke riktig vennligst sjekk appkoden");

define("APP_USER_NOT_EXIST","Sorry din e-id ikke er å registrere deg for dette programmet.");

define("FORGET_PASSWORD","Vi har sendt deg link for å resette passordet. Vennligst sjekk e-posten din");

define("FB_LOGIN","Det ser ut som du tidligere har logget deg inn med Facebook-kontoen din. Prøv å logge deg inn med denne");

define("EMAIL_NOT_EXIST_AS_COMPANYUSER","Denne e-postadressen er allerede i bruk på din firmakonto");

define("MOBILE_NUMBER_VERIFIED","Ditt telefonnummer er verifisert.");

define("USER_NOT_ACTIVE","Sorry din e-id er ikke aktiv modus.");

define("COMPANY_ADDED","Vi har mottatt din registrering av firmakonto. Du vil motta beskjed fra oss når denne er godkjent og kan tas i bruk");

define("COMPANY_DELETE","Firmakontoen er slettet");

define("CC_ALREADY_ADD","Din Kredittkort allerede lagt");

?>
