<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Current Booking
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Current Booking</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
        
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
             
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Booking No</th>
                      
                        
                        
                       <th>Booking Time</th>
                       <!-- <th>Pickup_Address</th>
                        <th>Destination_Address</th>-->
                        <th>Driver Name</th>
                        <th>Driver Contact</th>
                        <th>Status</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                     
                    
                     if(!empty($current_booking))
                     {
                      if(sizeof($current_booking))
                      
                      {
                        for($i=0;$i<sizeof($current_booking);$i++)
                        {
                        $name=$current_booking[$i]['first_name'].' '.$current_booking[$i]['last_name'];
                      ?>
                      <tr>
                        
                       
                      
                        <td><?php echo $current_booking[$i]['booking_number'];?></td>
                        <td><?php echo $current_booking[$i]['booking_time'];?></td>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $current_booking[$i]['contact'];?></td>
                        
                          <td><a href="javascript:void(0);"><span onclick="changetripstatus('<?php echo $current_booking[$i]['id'];?>','<?php echo $current_booking[$i]['driver_id'];?>');" class="label label-warning">Trip Strated</span></a></td>
                        
                       
                      
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                     }
                     else{
                      ?>
                     <tr>
                      
                              <td colspan="10">No Data Found</td>
                     </tr>
                     <?php
                     }?>
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                                             <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
              
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      <!-- model for change trip status !-->
      <div class="modal fade" id="open_changetrip" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: 572px; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
             Trip End Detail
             <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: 572px; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="text-align: left; margin: 0 auto; width: 95%;">
               
                <input type="hidden" id="driverid">                       
                 <input type="hidden" id="bookingid">
                 
                 Journey Time:-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="journey_time" style="width:247px;font-size:15px;font-weight:bold;margin-left:44px;" placeholder="Journery Time" onchange="calljourney(this.value);"><label style="margin-left:456px;margin-top:-22px;">In Minutes</label><br/><br/>   
                 K.M. Driven:-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="km_driven" style="width:247px;font-size:15px;font-weight:bold;margin-left:19px;" placeholder="Total K.M." onchange="checkkm(this.value);"><br/><br/> 
                    
                    <div class="item" style="height: 55px!important;">
                      
                    </div>
                    
                    <div style="margin-top:-18px;">
                        <input  class="btn btn-info btn-lg" type="button" value="Submit" id="trip_end" name="trip_end" style="margin-left:200px;width:86px;height:40px;margin-top:-46px;"/><br/>
                        <span id="loader_note" class="loader" style="display: none;">
                            <img src="http://dev.privatedriverapp.com/app/images/loader19.gif" alt=""/>
                        </span>
                    </div>
                        
                      
                 
                </div>
            </div>
        </div> 
    </div>   
</div> 
      
     
      </aside><!-- /.control-sidebar -->
      <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
 <script type="text/javascript" src="<?=base_url();?>js/my_custom.js"></script> 
   <script>
    function  changetripstatus(id,driver_id)
{

   $('#bookingid').val(id);
   $('#driverid').val(driver_id);
   $('#open_changetrip').modal({
        "backdrop" : "static"
         })
}

 $('#trip_end').click(function(){
   
       
        var bookingid=$('#bookingid').val();
        var driverid=$('#driverid').val();
         var journeytime=$('#journey_time').val();
         var kmdriven=$('#km_driven').val();
        
       // alert(bookingno);
      
         validatetripend(bookingid,driverid,journeytime,kmdriven);
        });



   </script>
   <script>

   </script>




     

      