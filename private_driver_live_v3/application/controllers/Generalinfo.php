<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generalinfo extends CI_Controller {

	function __construct()
    {
        parent::__construct();
       $this->load->helper(array('form','url'));
	     $this->load->model('General_model');
		  $this->load->helper(array('function_helper','constants_helper','notification_helper'));
	   $this->load->library(array('session',
            'email',
            'paginationlib',
            'Pagination'));
	   $this->load->helper(array('function_helper','constants_helper','notification_helper'));
		$user = $this->session->userdata('logged_in');
        if (!$user) {
            redirect('Welcome');
        }
		
    }
    
	public function Order()
	{
		$order=$this->General_model->fetch_order_info();
		//print_r($driver);
		$data['order']=$order;
		$data['active_page'] = 'order';
        $this->load->view('site-nav/header',$data);
        $this->load->view('order');
		$this->load->view('site-nav/footer');
	}
/*add item */

/* change order status */
public function change_order_status()
{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$order_status=$this->General_model->change_order_status($id,$status);
	    echo $order_status;
	}
public function add_item()
	{
		//$driver=$this->Driver_model->add_driver();
		//print_r($driver);
		//$data['driver']=$driver;
		@$order[0]->item_name = $order[0]->item_code=$order[0]->item_price="";
        $order="";
		if ($this->uri->segment(3) == false) {
			
            $id = 0;
			
        } else {
            $id = $this->uri->segment(3);
            $order = $this->General_model->get_order($id);
        }
        $data['order'] = $order;
		$data['active_page'] = 'order';
        $this->load->view('site-nav/header', $data);
        $this->load->view('add_item');
		$this->load->view('site-nav/footer');
	}
	
/* save item */
public function submit_order()
	{
        $id = $_POST['id'];
		$res = $this->General_model->save_order($id);
		if($res)
		{
			echo 1;
			
		}
		else
		{
			echo 0;
		}
		
	}
	/* change order delete */
	public function change_order_delete_status()
	{
		 $id = $_POST['id'];
       echo $this->General_model->delete_item($id);
	}
	
	/* fare info */
	public function Fare()
	{
		$fare=$this->General_model->fetch_fare_info();
		//print_r($driver);
		$data['fare']=$fare;
		$data['active_page'] = 'fare';
        $this->load->view('site-nav/header',$data);
        $this->load->view('fare');
		$this->load->view('site-nav/footer');
	}
	
	/*get particular fare */
	public function Particular_fare()
	{
		 $type=$_POST['type'];
		$fare=$this->General_model->fetch_particular_fare($type);
		$data['fare']=$fare;
		$data['active_page'] = 'fare';
       // $this->load->view($data);
        $this->load->view('part_fare', $data, false);
		
	}
	
	
	/* for save fare information */
	public function add_fare()
	{
		//$driver=$this->Driver_model->add_driver();
		//print_r($driver);
		//$data['driver']=$driver;
		@$fare[0]->description = $fare[0]->nop=$fare[0]->min_charges=$fare[0]->min_km=$fare[0]->charge_km=$fare[0]->Waittime_charges=$fare[0]->booking_time="";
        $fare="";
		if ($this->uri->segment(3) == false) {
			
            $id = 0;
			
        } else {
            $id = $this->uri->segment(3);
            $fare = $this->General_model->get_fare($id);
        }
        $data['fare'] = $fare;
		$data['active_page'] = 'fare';
        $this->load->view('site-nav/header', $data);
        $this->load->view('add_fare');
		$this->load->view('site-nav/footer');
	}
	/* submit fare */
	/* save item */
public function submit_fare()
	{
        $id = $_POST['id'];
		$res = $this->General_model->save_fare($id);
		if($res)
		{
			echo 1;
			
		}
		else
		{
			echo 0;
		}
		
	}
/* change fare status */
	public function change_fare_status()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$fare_status=$this->General_model->change_fare_status($id,$status);
	    echo $fare_status;	
	}
	
	/* active company user */
	function cuser_change_status()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
	
		
		
		$cuser_status=$this->General_model->Active_companyuser($id,$status);
	  	if($status=='1')
		{
		//$get_email=$this->General_model->get_email($id);
		//company_email($get_email);
		}	
	    echo $cuser_status;	
	}
	
	

	
public function Campagin()
{
	$this->load->library('Ajax_pagination');
        $this->General_model->coupon_count();
        $pagingconfig = $this->paginationlib->initPagination("/Generalinfo/ajaxPaginationData/", $this->
            General_model->coupon_count(), 20, 1,array());


        $this->ajax_pagination->initialize($pagingconfig);
        
        $data['link'] = $this->ajax_pagination->create_links();
        $campagin = $this->General_model->Fetch_coupon_info($pagingconfig['per_page'], 0);
	      ;
		//print_r($driver);
		$data['campagin']=$campagin;
		$data['active_page'] = 'campagin';
        $this->load->view('site-nav/header',$data);
        $this->load->view('coupon');
		$this->load->view('site-nav/footer');
	
}
 function ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Generalinfo/ajaxPaginationData/", $this->
            General_model->coupon_count(),10,$post['page']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['campagin'] = $this->General_model->Fetch_coupon_info($pagingconfig['per_page'],
            $page);

        //load the view
        $this->load->view('coupon_ajax', $data, false);
    }
	
	/* delete campagin by admin */
	function delete_campagin()
	{
	
         $id = $_POST['id'];
         $get_delete=$this->General_model->Delete_coupon($id);
     echo $get_delete;
	}
	
    public function perPage()
    {
        $this->load->library('Ajax_pagination');
        return 2;
    }
/* chnage coupon status */
function Coupon_status()
{

		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$coupon_status=$this->General_model->Update_coupon_status($id,$status);
	    echo $coupon_status;	
	
		
}


/* add coupon */
 public function add_coupon()
    {
        
        @$coupon[0]->title = $coupon[0]->offer = $coupon[0]->description= $coupon[0]->
            code= $coupon[0]->start_date= $coupon[0]->end_date ="";
        $coupon = "";
        if ($this->uri->segment(3) == false)
        {

            $id = 0;

        }
        else
        {
            $id = $this->uri->segment(3);
			//echo "coupon id=>".$id;
            $coupon = $this->General_model->get_coupon($id);
        }
		$data['coupon'][0]->id=$id;
        $data['coupon'] = $coupon;
        $data['active_page'] = 'coupon';
		$data['users']=$this->General_model->GetUser();
		$data['id']=$id;
        $this->load->view('site-nav/header', $data);
        $this->load->view('add_coupon');
        $this->load->view('site-nav/footer');
    }
	
	/* add coupon */
	  function Submit_coupon()
    {
     	$id='';
      $res = $this->General_model->save_coupon($id);
        if ($res)
        {
        	//echo "d";
            if ($res == 'updated')
            {
                
                echo '1';

            }
            else
            {
                
              echo '1';

            }


        }
		else
		{
			echo 0;
		}
}


/* fetch company user infomation */
	public function Company_user()
	{
		$this->load->library('Ajax_pagination');
        $this->General_model->company_user_count();
        $pagingconfig = $this->paginationlib->initPagination("/Generalinfo/companyuser_ajaxPaginationData/", $this->
            General_model->company_user_count(),10, 1,array());


        $this->ajax_pagination->initialize($pagingconfig);
        
        $data['link'] = $this->ajax_pagination->create_links();
        $user = $this->General_model->Fetch_cuser_info($pagingconfig['per_page'], 0);
	    $company=$this->General_model->get_company();
		$data['company']=$company;
		//print_r($company);
		$data['user']=$user;
		$data['active_page'] = 'company_user';
        $this->load->view('site-nav/header',$data);
        $this->load->view('user');
		$this->load->view('site-nav/footer');
		
	}

/* ajax pagination for count company user */
function companyuser_ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Generalinfo/companyuser_ajaxPaginationData/", $this->
            General_model->company_user_count(),10,$post['page']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['user'] = $this->General_model->Fetch_cuser_info($pagingconfig['per_page'],
            $page);
     $company=$this->General_model->get_company();
		$data['company']=$company;
        //load the view
        $this->load->view('company_user_ajax', $data, false);
    }
/* fetch normal user information */
public function User()
	{
		
			$this->load->library('Ajax_pagination');
        $this->General_model->user_count();
        //$pagingconfig = $this->paginationlib->initPagination("/Generalinfo/user_ajaxPaginationData/", $this->
       // General_model->user_count(),10, 1,array());
       // $this->ajax_pagination->initialize($pagingconfig);
       // $data['link'] = $this->ajax_pagination->create_links();
      //  $user = $this->General_model->Fetch_user_info($pagingconfig['per_page'], 0);
		//print_r($driver);
		//$data['user']=$user;
		$data['active_page'] = 'user';
        $this->load->view('site-nav/header',$data);
        $this->load->view('normal_user');
		$this->load->view('site-nav/footer');
	}
	
	function GetUser()
	{
	  echo  json_encode($user=$this->General_model->GetUserDetail());		
	 
	}
	
	function AddUser()
	{
		$data['active_page'] = 'user';
		$data['company_detail']=$this->General_model->GetCompany();
		$data['user_detail']=$this->General_model->GetNonCompanyUser();
        $this->load->view('site-nav/header',$data);
        $this->load->view('add_user');
		$this->load->view('site-nav/footer');
	}
	
	function check_email()
	{
		//print_r($_POST);
		echo $this->General_model->check_email($_POST);
	}
	
	function check_phone()
	{
		//print_r($_POST);
		echo $this->General_model->check_phone($_POST);
	}
	
	function SaveUser()
	{
		 //               print_r($_POST);
		echo $this->General_model->AddUser($_POST);
	}
	
/* user ajax pagination */
function user_ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Generalinfo/user_ajaxPaginationData/", $this->
            General_model->user_count(),10,$post['page']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['user'] = $this->General_model->Fetch_user_info($pagingconfig['per_page'],$page);

        //load the view
        $this->load->view('user_ajax', $data, false);
    }	
	
function get_customer_birthday()
{
	$date=date('Y-m-d');
	$get_customer=$this->General_model->Fetch_customer_birthday($date);
     
	if($get_customer)
	{

	 $html='<table id="example2" style="font-family:Trebuchet MS, Arial, Helvetica, sans-serif;width:30%;border-collapse: collapse">
                                        <thead>
                                            <tr>
                                                <th style="font-size: 1.1em;text-align:left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942; color: #ffffff;">
																				<center>Name</center></th>
                                                
                                                <th style="font-size: 1.1em;text-align:left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942; color: #ffffff;"><center>Email</center></th>
                                                <th style="font-size: 1.1em;text-align:left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942; color: #ffffff;"><center>contact</center></th>
                                                 </tr>	';
												 
	 foreach($get_customer as $data)
	 {
	
	
		$html.='<tr style=" color:"#000000;background-color: #EAF2D3;">
		      <td style=" font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;">
	'.$data['name'].'
			  </td>
			  <td style=" font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;">
	'.$data['email'].'
			  </td>
			   <td style=" font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;">
'.$data['phone'].'
			  </td>
			  </tr>';
                   
				                               
	 }
	
	  $config['protocol'] = 'smtp';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';

			$config['smtp_host'] = EMAIL_HOST;
			$config['smtp_user'] = EMAIL_USERNAME;
			$config['smtp_pass'] = EMAIL_PASSWORD;
			$config['smtp_port'] = EMAIL_PORT;
			$config['email_smtp_crypto'] = 'tls';
			$config['email_newline'] = '\r\n';

			
			$ci -> email -> clear();
			$ci -> email -> initialize($config);
$ci -> email -> set_newline("\r\n");
        $this->email->from(ADMINMAIL_FROM, 'Private Driver App');
        $this->email->to('post@privatedriver.no');
		$this->email->bcc('priyanka@foxinfosoft.com');
        $this->email->subject('Customer Birthday Report');
        $this->email->message($html);
        
        //$counter ++;
        if ($this->email->send())
        {
            echo '1';
        }
        else
        {
            //echo $filename;
            show_error($this->email->print_debugger());

        }
	
	
	
	}

}

/* function for assign company */
function assign_company()
{
	$getcid=$_POST['c_id'];
	$getuid=$_POST['u_id'];
	$cname=$_POST['cname'];
	echo $company_user=$this->General_model->c_user($getcid,$getuid,$cname);
	
}



/*function for timezone setting */
function timezonesetting()
{
	   $data['timezone']=$this->General_model->get_time_zone();
		$data['active_page'] = 'timezone';
       	$this->load->view('site-nav/header',$data);
        $this->load->view('timezone');
		$this->load->view('site-nav/footer');
}

function prebooking_charge()
{
	   $data['price']=$this->General_model->get_charge();
		$data['active_page'] = 'prebooking_charge';
       	$this->load->view('site-nav/header',$data);
        $this->load->view('perbooking_charge');
		$this->load->view('site-nav/footer');
}

function set_timezone()
{
	$timezone=$_POST['timezone'];
	$update=$this->General_model->update_time_zone($timezone);
	echo $update;
}
function set_charge()
{
	$timezone=$_POST['timezone'];
	$update=$this->General_model->update_charge($timezone);
	echo $update;
}

function check_coupon_code()
{
	$coupon=$_POST['code'];
	echo $this->General_model->check_coupon($coupon);
}


}