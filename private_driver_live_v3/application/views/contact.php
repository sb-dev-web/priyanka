<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mobile No. Details
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active"> Mobile No. Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <a href="<?php echo site_url('Mobile_info/add_mobile');?>"><input type="button" class="btn btn-block btn-warning" value="Add" style="float:right; margin-right:7px;margin-top:8px; width:10%;"/></a><br/><br/>
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example" class="table table-bordered table-striped" style="font-size:15px;">
                    <thead>
                      <tr>
                        <th>Country Code</th>
                        <!--<th>Profile</th>-->
                         <th>Mobile Serial No.</th>
                        <th>Sim No.</th>
                       <!-- <th>Gender</th>-->
                      
                       <!-- <th>Country</th>-->
                        <th>Delete</th>
                        
                     
                      
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(sizeof($mobile))
                      {
                       
                        for($i=0;$i<sizeof($mobile);$i++)
                        {
                          
                          
                      ?>
                      <tr>
                        <td><?php echo $mobile[$i]->country_code;?></td>
                        <td><?php echo $mobile[$i]->serial_number;?></td>
                         <td><?php echo $mobile[$i]->contact_no;?></td>
                        
                        
                       
                       
                        
                       
                   <td><a href="javascript:void(0);" onclick="changedeleteStatus('<?php echo $mobile[$i]->id;?>','delloading_<?php echo $mobile[$i]->id;?>', 'spandel_<?php echo $mobile[$i]->id;?>')">
                            <img src="<?php echo base_url();?>images/loader19.gif" id="image_<?=$mobile[$i]->id;?>" style="display: none;"/>        
                                    <?php if($mobile[$i]->isdelete=='0'){?>
                                    <span id="spandel_<?php echo $mobile[$i]->id?>" class="fa fa-trash fa-2x" style="color:red;"></span></a>
                                    <?php }?>
                                    <br /><div id="delloading_<?php echo $mobile[$i]->id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                      </td>
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      else
                      {
                      ?>
                      <td>NO Data Found</td>
                       <td>NO Data Found</td>
                        <td>NO Data Found</td>
                      <?php
                      }
                      ?>
                      
                    </tbody>
                    <tfoot>
                       
                    </tfoot>
                  </table>
                  <?php
                  
                   ?>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
        
          </div>
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      
      <!-- Control Sidebar -->      
      
 <!--pop up -->
 <div id="dialog" style="display: none">
  <span id="ui-id-1" class="ui-dialog-title">fgdgfg</span>
        <div id="dvMap" style="height: 380px; width: 580px; border:3px solid black;">
        </div>
    </div>
      
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <!-- <script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->

<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
   
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css">

       
    
<script>
function changedeleteStatus(id,loadingdiv,spanid)
{
 if (confirm("Are you sure to delete this mobile no? ") == false) 
    {
         return;
    }
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    $.ajax({type:'POST',url: '<?php echo site_url('Mobile_info/change_delete_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
   
       $("#image_"+id).closest('tr').fadeOut('slow')
     }});
}
   
</script>

 
  
  