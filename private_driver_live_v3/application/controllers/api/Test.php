<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Test extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array(
            'upload',
            'Braintree_lib',
            'S3_lib'));
    //    $this->load->library('Openfire');
        $this->load->helper(array(
                    'form',
                    'url',
                    'string',
                    'constants_helper',
                    'function_helper'));
        
                $this->load->model('Test_model');
        //
    }

    function testCreate_withCreditCardAndVerification()
    {
        $result = Braintree_Customer::create(array(
            'firstName' => 'Michal',
            'lastName' => 'Jones',
            'email' => 'mike.jones@example.com',
            'phone' => '+914195551234',
            'creditCard' => array(
                'number' => '4012000033330125',
                'expirationDate' => '05/18',
                'cvv' => '445',
                'cardholderName' => 'Michal Jones',
                'options' => array('verifyCard' => true))));

        echo "<pre>";
        print_r($result);
        if ($result->success)
        {
            echo "<pre>";
            //print_r($result->customer);
            echo "Customer ID: " . $result->customer->id;
            echo "Payment Token: " . $result->customer->creditCards[0]->token;
            //do your stuff like insert customer in to database with the customerID and Payment Token
        }
        else
        {
            print_r($result->errors);
            echo ("Verfication Status: " . $result->creditCardVerification->status);
            echo ("<br>Error Code: " . $result->creditCardVerification->
                processorResponseCode);
            echo ("<br>Error Message: " . $result->creditCardVerification->
                processorResponseText);
            echo ("<br>CVV Response Code: " . $result->creditCardVerification->
                cvvResponseCode);
            echo ("<br>AVS Error Code: " . $result->creditCardVerification->
                avsErrorResponseCode);
            echo ("<br>POSTAL CODE RESPONSE: " . $result->creditCardVerification->
                avsPostalCodeResponseCode);
            echo ("<br>AVS Stree Address Response Code: " . $result->creditCardVerification->
                avsStreetAddressResponseCode);
        }
    }
    function param()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("driver_id", "user_id"));
    }
    function adduser()
    {
        $param = array(
            "username" => $_POST['username'],
            "password" => $_POST['pass'],
            "email" => $_POST['email'],
            "name" => $_POST['name']);
        echo $openfir_result = $this->openfire->openfire_action("POST", $param, "users/");
    }
    function updateuser()
    {
        $param = array(
            "username" => $_POST['username'],
            "password" => $_POST['pass'],
            "email" => $_POST['email'],
            "name" => $_POST['name']);
        echo $openfir_result = $this->openfire->openfire_action("PUT", $param, "users/" .
            $_POST['username'] . "");
    }
    function verify()
    {
        echo "df";
        echo "<pre>";
        $collection = Braintree_CreditCardVerification::search([Braintree_CreditCardVerificationSearch::
            paymentMethodToken()->is('6wzcg6'), ]);
        print_r($collection);
    }

    function nonce()
    {
        $result = Braintree_PaymentMethodNonce::create('5pjmk2');
        echo "nonce" . $nonce = $result->paymentMethodNonce->nonce;
    }

    function verifiy2()
    {
        echo "<pre>";
        //86fd4626-b53a-4f0f-804e-304735cb3129
        /*$result = Braintree_PaymentMethod::update('6wzcg6', ['paymentMethodNonce' =>
        "38df211c-cd8b-41d3-b602-b614eb226070", 'options' => ['verifyCard' => true]]);*/
        $result = Braintree_PaymentMethod::update('5pjmk2', ['paymentMethodNonce' =>
            "e7626aad-0508-4e00-8009-4f58b0dfc849", /* 'options' => [
            'verifyCard' => true
            ],*/['billingAddress' => ['firstName' => 'Drew', 'lastName' => 'Smith',
            'company' => 'Smith Co.', 'streetAddress' => '1 E Main St', 'region' => 'IL']]]);

        print_r($result);
    }
    function card_type()
    {
    }
    function multipleValueNode_creditCardType()
    {
        $result = Braintree_Customer::create(array('creditCard' => array(
                'cardholderName' => "Joe Smith",
                'number' => "4000111111111115",
                'expirationDate' => "12/2016",
                'options' => array('verifyCard' => true),
                ), ));
        echo "<pre>";
        print_r($result);
        $creditCardVerification = $result->creditCardVerification;

        $collection = Braintree_CreditCardVerification::search(array(Braintree_CreditCardVerificationSearch::
                id()->is($creditCardVerification->id), Braintree_CreditCardVerificationSearch::
                creditCardCardType()->is($creditCardVerification->creditCard['cardType'])));
        $this->assertEquals(1, $collection->maximumCount());
        $this->assertEquals($creditCardVerification->id, $collection->firstItem()->id);

        $collection = Braintree_CreditCardVerification::search(array(Braintree_CreditCardVerificationSearch::
                id()->is($creditCardVerification->id), Braintree_CreditCardVerificationSearch::
                creditCardCardType()->in(array($creditCardVerification->creditCard['cardType'],
                    Braintree_CreditCard::CHINA_UNION_PAY))));
        $this->assertEquals(1, $collection->maximumCount());
        $this->assertEquals($creditCardVerification->id, $collection->firstItem()->id);

        $collection = Braintree_CreditCardVerification::search(array(Braintree_CreditCardVerificationSearch::
                id()->is($creditCardVerification->id), Braintree_CreditCardVerificationSearch::
                creditCardCardType()->is(Braintree_CreditCard::CHINA_UNION_PAY)));
        $this->assertEquals(0, $collection->maximumCount());
    }
    function file_upload()
    {
        //function s3()
        {

         //  echo  $config['upload_path'] = base_url(). '/temp/';
          //echo   $config['upload_path'] = 'image';
            $config['upload_path'] = 'http://localhost:1337/private_taxi_dev/images/shift/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000000';
            $config['max_width'] = '1024000';
            $config['max_height'] = '768000';

            $this->load->library('upload', $config);
            // $this->upload->initialize($config);

            if (!$this->upload->do_upload('cab_image'))
            {
                $error = array('error' => $this->upload->display_errors());

                print_r($error);
                echo 'failure';
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                $fn = $data['file_name'];
               echo "AWS=>". $check=$this->s3->putObjectFile( $config['upload_path'].$fn,AWS_BUCKET ,$fn,S3::ACL_PUBLIC_READ);
                $type = substr($fn, strrpos($fn, '.') + 1);

                $this->load->library('s3');
                $temp_file_path = $_SERVER["DOCUMENT_ROOT"] . "/temp/" . $data['file_name'];
                $contents = read_file($temp_file_path); // will this blow up or timeout for large files?!
                $newFileName = uniqid() . "." . substr($temp_file_path, strrpos($temp_file_path,
                    '.') + 1);
                $contentPath = AWS_BUCKET;

                $this->s3->putObject($newFileName, $contents, $contentPath, 'private', $type);
                echo 'success';
            }
        }
    }
    function cab_image()
    {
        if (isset($_FILES['cab_image']))
        {

            if ($_FILES['cab_image']['error'] == 0)
            {
                $file_name = strtolower(random_string('alnum', 16));
                $config['upload_path'] = './images/shift/';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['file_name'] = $file_name;


                //$this->load->library('upload', $config);

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('cab_image'))
                {

                    //var_dump($_FILES);
                    $this->response(array('message' => $this->upload->display_errors(), 'status' =>
                            0), 200);
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                     print_r($data);
                    echo  $fn=$data['upload_data']['file_name'];
                      $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
                      echo "AWS=>". $check=$s3->putObjectFile( $config['upload_path'].$fn,AWS_BUCKET ,$fn,S3::ACL_PUBLIC_READ);
                     exit;
                    $input_method['image'] = $data['upload_data']['file_name'];
                    $cab_image_id = $this->Driverapi_model->save_cab_image($input_method, $file_name);
                    $source = $config['upload_path'] . $input_method['image'];
                    $desc = $config['upload_path'] . $file_name . "_thumb" . $data['upload_data']['file_ext'];
                    $ext = $data['upload_data']['file_ext'];
                    $width = 200;
                    make_thumb($source, $desc, $width, $ext);
                    if ($cab_image_id)
                    {

                        $this->response(array(
                            'message' => CAB_IMAGE_SAVE,
                            "cab_image_id" => $cab_image_id,
                            'status' => 1), 200);
                    }
                    else
                    {
                        $this->response(array('message' => SERVER_ERROR, 'status' => 0), 200);
                    }
                }
            }
        }
    }
  function Test_mail()
  {
  	     $input_method = $this->webservices_inputs();
  	echo $email=$input_method['email'];
	echo "<br/>".$lang =$input_method['lang'];
	//exit;
	//recepet_send_pdf('296',"normal",$email);
	company_email_test($email,$lang);
  }
  
  function Test_mail_check()
  {
  	     $input_method = $this->webservices_inputs();
  	 $email=$input_method['email'];
	recepet_send_pdf_company('296',"normal",$email);
          recepet_send_pdf('296',"normal",$email);
  }
  
  function Refund()
  {
  	//$result = Braintree_Transaction::refund('fyjqzdj', '3100.00');
	echo "<pre>";
	print_r($result);
  }
  
  function DriverLocation()
  {
  	$locations=$this->Test_model->GetLocation();
	//print_r($locations);
	foreach($locations as $val)
	{
		$data=unserialize($val['params']);
		$location_array[]=$data;
		$location.=$data['lat'].' , '.$data['lng'] .'|';
		
	}
	
	echo $location;
	echo json_encode($location_array);
	
  }

}

?>
