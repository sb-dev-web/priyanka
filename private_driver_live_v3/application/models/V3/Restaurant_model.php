<?php
class Restaurant_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    function fetch_total_restaurant($limit,$offset=0)
    {
        $query=$this->db->query("select * from restaurant where isdelete='1' order by id DESC limit $offset,$limit");       // echo $this->db->last_query();
       // echo $this->db->last_query();
        return $query->result();
    
    }
   
    function restaurant_count()
    {
        $this->db->select("id");
        $this->db->where("isactive","1");
        $this->db->where("isdelete","1");
        $this->db->from("restaurant");
        $query=$this->db->get();
          
        return ($query->result_id->num_rows);
         
       
        
    }
    function change_restaurant_status($id,$status)
    {
        if($status=="1")
        {
            $field_array=array("isactive"=>"1");
            $status=='1';
        }
        else
        {
            $field_array=array("isactive"=>"0");
            $status=='0';
        }
        $this->db->where('id',$id);
        $this->db->update("restaurant",$field_array);
        return $status;
    }
    
    function save_restaurant($id="0")
    {
       
        
        if($id>0)
        {
            $this->db->where('id',$id);
            //$this->db->set('create_on', GMT_DATE, FALSE);
              $arr_feilds = array('name'=> $_POST['name'],
                            'address'=>$_POST['address'],
                            'latitude'=>$_POST['latitude'],
                            'longitude'=>$_POST['longitude'],
                            'contact_number'=>$_POST['contact'],
                            'isactive'=>'1'
                            );
            $this->db->update('restaurant',$arr_feilds);
            
            if($id>0 && (!empty($_POST['password'])))
            {
                 $this->db->where('id',$id);
                $arr_feilds = array('password'=> md5($_POST['password'])
                          );
               $this->db->update('restaurant',$arr_feilds);
              
            }
            return 'updated';
        }
        else
        {
            
             $arr_feilds =  array('name'=> $_POST['name'],
                            'address'=>$_POST['address'],
                            'latitude'=>$_POST['latitude'],
                            'longitude'=>$_POST['longitude'],
                            'contact_number'=>$_POST['contact'],
                            'password'=>md5($_POST['password']),
                            'email'=>$_POST['email'],
                            'isactive'=>'1'
                            );
            //$this->db->set('create_on', GMT_DATE, FALSE);
            $this->db->insert('restaurant',$arr_feilds);
            //echo $this->db->last_query();
            $insert=$this->db->insert_id();
            
            return $insert;
             
        }
    }
    
    function get_restaurant($id)
    {
         if($id>0)
            $this->db->where('id',$id);
         
        $query = $this->db->get("restaurant");
        //echo $this->db->last_query();exit;
        return $query->result();
    }
    function delete_restaurant($id)
    {
           $this->db->where('id',$id);
        $this->db->set('isdelete','0');
         $this->db->update("restaurant");
        return 1;
    }

    function check_restaurant_email($email)
    {
      
       $driver_email=$this->db->query("select * from restaurant where email='$email' and isdelete='1'");
       $count=$driver_email->num_rows();
       if($count>0)
       {
        return 1;
       }
       else
       {
        return 0;
        
       }
    }
    
    
    /* restro boking count */
    function Bookings_count()
    {
        $fetch_data = $this->db->query("select c.id,c.booking_number, c.booking_time,c.pickup_latitude,c.pickup_longitude,p.billing_amount,p.km_driven,p.user_paid from restro_cab_booking as c,payment_detail as p where p.booking_id=c.id and c.status='FINISH' and p.booking_type='RESTRO'");
        if ($fetch_data->num_rows() > 0)
        {
            return $fetch_data->num_rows();
        }
        else
        {
            return 0;
        }
    }
    
    
    function fetch_total_bookings($limit,$offset = 0)
    {
        $fetch_data = $this->db->query("select c.id,c.booking_number,c.client_email,c.pickup_location,c.booking_time,c.pickup_latitude,c.pickup_longitude,p.billing_amount,p.km_driven,p.user_paid from restro_cab_booking as c,payment_detail as p where p.booking_id=c.id and c.status='FINISH' and p.booking_type='RESTRO' Order by c.booking_time DESC limit $offset,$limit");
        //echo $this->db->last_query();
        if ($fetch_data->num_rows() > 0)
        {
            return $fetch_data->result_array();
        }
        else
        {
            return false;
        }
   
}


/*fetch count aqdvance booking */
	function count_advance_booking()
	{            $date=date('Y-m-d');
		$result=$this->db->query("SELECT restro_cab_booking.id as bid,restro_cab_booking.booking_number,restro_cab_booking.booking_time,restro_cab_booking.pickup_location,restaurant.id as rid,restaurant.name,restaurant.contact_number,restaurant.email,d.driver_id,d.first_name,d.last_name FROM `restro_cab_booking` join restaurant on restaurant.id=restro_cab_booking.restro_id left join driver d on d.driver_id=restro_cab_booking.driver_id where restro_cab_booking.is_prebooking='Y' and date(booking_time)>='$date'");
		return $result->num_rows();
		
	}
    
    
    
    /*function for fecth advance booking */
    function fetch_advance_booking($limit,$offset=0)
    {   $date=date('Y-m-d');
        $result = $this->db->query("SELECT restro_cab_booking.id as bid,restro_cab_booking.area,restro_cab_booking.createdon,restro_cab_booking.booking_number,restro_cab_booking.booking_time,restro_cab_booking.pickup_location,restaurant.id as rid,restaurant.name,restaurant.contact_number,restro_cab_booking.client_name,restro_cab_booking.client_contact_number,restro_cab_booking.country_code as rcode,restaurant.email,d.driver_id,d.first_name,d.last_name FROM `restro_cab_booking` join restaurant on restaurant.id=restro_cab_booking.restro_id left join driver d on d.driver_id=restro_cab_booking.driver_id where restro_cab_booking.is_prebooking='Y' and date(booking_time)>='$date' ORDER BY date(booking_time) ASC limit $offset,$limit");
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }
    }
    
    
     /*fetch drive rfor admin work web */
    function fetch_driver()
    {
        $result = $this->db->query("select * from driver");
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }

    }
    
     /* update advance booking admin*/
    function update_booking($bookingid, $driverid)
    {
        $driver_assgin = get_gmt_time();
      
         $get_palte_no=$this->db->query("select c.cab_plate_no from shift as s,cab as c where c.cab_id=s.cab_id and s.status='started' and s.driver_id='$driverid'");
         $get_no=$get_palte_no->row();
         $plate_no=$get_no->cab_plate;
        
        $result = $this->db->query("Update restro_cab_booking SET driver_assigned_date='$driver_assgin',plate_number='$plate_no',driver_id=$driverid,status='DRIVER_ASSIGNED' where id='$bookingid'");
         //echo $this->db->last_query();
        if ($result)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
     function get_driver_number($driver_id)
    {
        $this->db->select("contact");
        $this->db->where("driver_id", $driver_id);
        $query = $this->db->get("driver");
        //echo $this->db->last_query();
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
            return $data;
        }
        else
        {
            return false;
        }
    }
	
    function get_rcomplete_booking($b_no)
    {
        $fetch_data = $this->db->query("select c.booking_number,t.createdon,t.status,c.booking_time,c.pickup_location,p.billing_amount,p.km_driven,p.user_paid from restro_cab_booking  as c,payment_detail as p,driver_status as t where p.booking_type='RESTRO' and t.booking_type='RESTRO' and c.status='FINISH' and p.booking_id=c.id and c.id='$b_no'  and
 t.booking_id=c.id and t.status IN('TRIP_STARTED','TRIP_ENDED')");
      // echo $this->db->last_query();
        return $fetch_data->result();
    }
}