<?php
class Mobile_info_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    function fetch_total_contact($limit,$offset=0)
    {
        $query=$this->db->query("select * from mobile_no where isdelete='0' order by id DESC limit $offset,$limit");       // echo $this->db->last_query();
       // echo $this->db->last_query();
        return $query->result();
    
    }
   
    function contact_count()
    {
        $this->db->select("id");
       
        $this->db->where("isdelete","0");
        $this->db->from("mobile_no");
        $query=$this->db->get();
          
        return ($query->result_id->num_rows);
         
       
        
    }
    
    
    function save_contact($id="0")
    {
   
       $date=get_gmt_time();
        
        if($id>0)
        {
            $this->db->where('id',$id);
            //$this->db->set('create_on', GMT_DATE, FALSE);
              $arr_feilds = array('country_code'=> '+'.$_POST['country_code'],
                            'serial_number'=>$_POST['serial_no'],
                             'contact_no'=>$_POST['contact'],
                            'updatedon'=>$date
                            
                            );
            $this->db->update('mobile_no',$arr_feilds);
            
            
        }
        else
        {
            echo $_POST['country_code'];
             
              $arr_feilds = array('country_code'=>'+'. $_POST['country_code'],
                            'serial_number'=>$_POST['serial_no'],
                             'contact_no'=>$_POST['contact'],
                            'createdon'=>$date
                            
                            );
            //$this->db->set('create_on', GMT_DATE, FALSE);
            $this->db->insert('mobile_no',$arr_feilds);
           // echo $this->db->last_query();
            $insert=$this->db->insert_id();
            
            return $insert;
             
        }
    }
    
    function get_contact($id)
    {
         if($id>0)
            $this->db->where('id',$id);
         
        $query = $this->db->get("mobile_no");
        //echo $this->db->last_query();exit;
        return $query->result();
    }
    function delete_contact($id)
    {
           $this->db->where('id',$id);
        $this->db->set('isdelete','1');
         $this->db->update("mobile_no");
        return 1;
    }
    
    /*get all country code */
    function country_code()
    {
       $get_no=$this->db->query("select * from country_t");
       return $get_no->result();
    }
    function check_serial_no()
    {
        $serial_no = trim($_POST['serial_no']);
           $this->db->where('serial_number',$serial_no);
        $this->db->where('isdelete','0');
       $query = $this->db->get("mobile_no");
       if($query->num_rows()>0){
        
        return 1;
       }else{
        return 0;
       }
    }
    function check_mobile_no()
    {
        $mobile_no = trim($_POST['mobile_no']);
           $this->db->where('contact_no',$mobile_no);
        $this->db->where('isdelete','1');
       $query = $this->db->get("mobile_no");
       if($query->num_rows()>0){
        
        return 1;
       }else{
        return 0;
       }
    }
    
}
