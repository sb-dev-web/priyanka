<?php
	error_reporting(1);
	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Customerreport extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url',
				'function'
			));
			$this -> load -> model('Peakhour_model');

			$this -> load -> model('Customerreport_model');

			$this -> load -> library(array(
				'session',
				'email',
			));
			$user = $this -> session -> userdata('logged_in');
			if (!$user)
			{
				redirect('Welcome');
			}

		}

		function index()
		{
			if ((isset($_POST['date'])) && ($_POST['custmer_id']))
			{
				$date = $_POST['date'];
				$dates = explode('to', $date);
				$start_date = $dates[0];
				$end_date = $dates[1];
				$driver_id = $_POST['driverid'];
				$this -> drivertimeline_data($date, $driver_id);
			}
			else
			{
				$data['active_page'] = 'customer_report';
				$data['customer'] = $this -> Peakhour_model -> get_customer();

				$this -> load -> view('site-nav/header', $data);
				$this -> load -> view('customer_report', $data);
				$this -> load -> view('site-nav/footer');
			}
		}

		public function CustomerBooking_data()
		{
			$booking_date = $_POST['date'];
			$customer_id = $_POST['customer_id'];
			$data['CustomerBookingData'] = $this -> Customerreport_model -> CustomerBookingReport($booking_date, $customer_id);
			$data['time'] = $booking_date;
			//print_r($data);
			$this -> load -> view('customer_reportdata', $data);

		}

		function BookingDetail1()
		{
			$bookingdata = $this -> Customerreport_model -> BooingDetail_($_POST);
			$data['Bookingdata'] = $bookingdata;
			//print_r($data);
			$this -> load -> view('customer_reportview', $data);

		}
		
		/*function GetBookingDetail()
		{
			$booking_id=$_POST['booking_no'];
			echo "bookimh".$booking_id;
		}*/

		function BookingDetail()
		{
			

			$Booking = json_encode($this -> Customerreport_model -> BooingDetail($_POST));
			// print_r($Booking);
			//echo  json_encode($user=$this->General_model->GetUserDetail());

		}

		function getbookingdetail()
		{
			$booking_no = $_POST['booking_no'];
			$data['get_data'] = $this -> Drivertimeline_model -> get_bookingdetail($booking_no);
			$this -> load -> view('booking_detail', $data);
		}

	}
?>