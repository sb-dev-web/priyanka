<?php

class Openfire
{
    public $host;
    public $port;
    public $http_code;
    function __construct()
    {
        $this->host = "haven4u.com";
        $this->port = "9090";
        $ci = &get_instance();
        $ci->load->helper(array('constants'));
        //$ci->load->library('xml');

    }


    function openfire_action($type, $param, $endpoint)
    {
        /**
         *@type DELETE this type use delete api calling
         *@type UPDATE This type use to update the user update
         *@type POST this is use to add user 
         */
        // echo "<pre>";
        $url = "http://" . $this->host . ":" . $this->port . "/plugins/restapi/v1/" . $endpoint;
        $openfire_response = self::openfire_response($url, $param, $type);
        $finall_result = self::openfire_response_parse($openfire_response, $type);
        return json_encode($finall_result);
    }

    function openfire_response($url, $fields, $type = "POST")
    {
        $fields_string = "";
        $data_string = json_encode($fields);
        $headers = array('Content-Type:application/json',
                "Authorization:0BnOfPf3S6l6dk81");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if ($type == 'PUT')
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); // note the PUT here
            // curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        }
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $result = curl_exec($ch);
        $result_info = (curl_getinfo($ch));
        $this->http_code = $GLOBALS['http_code'] = $result_info['http_code'];
       // print_r($result_info);
        curl_close($ch);
        //print_R($result);
        return $result;


    }

    function openfire_response_parse($result, $type)
    {
        if ($type == "POST")
        {
            if (!$result)
            {
                return array(
                    "status" => 0,
                    "exception" => "Servererror",
                    "message" => $GLOBALS['http_code'] . $this->http_code . " Error");
            }
           return  $xml_parse = self::xml_data_parse($result);
        }
        elseif ($type == "PUT")
        {
            if ($this->http_code == "200")
            {
                return array("status" => 1, "message" => "success");
            }
            else
            {
                return  $xml_parse = self::xml_data_parse($result);
            }
        }
    }

    function xml_data_parse($result)
    {
        $dom = new DOMDocument;
        $dom->loadXML($result);
        if (!$dom)
        {
            //die("dom error");
            return array(
                "status" => 0,
                "exception" => "Servererror",
                "message" => "Server not support");
        }
        else
        {
            $response = simplexml_import_dom($dom);
            if (key_exists("exception", $response))
            {
                $exception = array((string )$response->exception[0]);
                $message = array((string )$response->message[0]);
                $result = array(
                    "status" => 0,
                    "exception" => $exception[0],
                    "message" => $message[0],
                    );
                return $result;
                //   return json_encode($result);
            }
            else
            {
                return array("status" => 1, "message" => "success");

            }
        }
    }

}

?>