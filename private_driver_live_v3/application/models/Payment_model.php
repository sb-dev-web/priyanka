<?php

class Payment_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }
    //*********************CHECK BILLING CHARGE***********************************
    function check_billing_charge($input_method)
    {
        $booking_id = $input_method['booking_id'];
        $amount = $input_method['billing_amount'];
        $query = $this->db->get_where("cab_booking", array("id" => $booking_id));
        $this->db->select("cab_booking.id,cab_booking.status,payment_detail.id,payment_detail.billing_amount,card_info.braintree_token,cab_booking.discount_amount");
        $this->db->from("cab_booking");
        $this->db->join("payment_detail", "payment_detail.booking_id=cab_booking.id");
        $this->db->join("card_info", "card_info.customer_id=cab_booking.user_id");
        $this->db->where("cab_booking.id", $input_method['booking_id']);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0)
        {
            //echo "<pre>";
            return $query->result_array();
        }
        else
        {
            return false;
        }


    }
    function make_payment($input_method)
    {
        $tip = "0.00";

        if (@$input_method['tip'] != "")
        {
            $tip = @$input_method['tip'];
            $tip=number_format($tip,2);
        }
        $date = get_gmt_time();
        $user_paid = $input_method['billing_amount'] + $tip;
        $transaction_id = $input_method['transaction_id'];
        $nonce=$input_method['nonce'];
        $desc = $input_method['desc'];
        $currency=$input_method['currency'];
		$status=$input_method['status'];
        if ($transaction_id != "")
        {
            $update_data = array(
                "user_paid" => $user_paid,
                "tip" => $tip,
                "is_paid" => "1",
                "paymenton" => $date,
                "transaction_id" => $transaction_id,
                "nonce"=>$nonce,
                "status"=>$status,
                "currency"=>$currency,
                "description" => $desc);
        }
        else
        {
                $update_data = array(
               // "user_paid" => $user_paid,
               // "tip" => $tip,
                "is_paid" => "0",
                //"paymenton" => $date,
                //"transaction_id" => $transaction_id,
                "description" => $desc);
        }
        $this->db->where("id", $input_method['payment_id']);

        $update = $this->db->update("payment_detail", $update_data);
       // echo $this->db->last_query();
        if ($update)
        {
            if ($transaction_id != "")
            {
                //***************MAKE BOOKING FINISH**************************
                $update = array("status" => "FINISH");
                $this->db->where("id", $input_method['booking_id']);
                $this->db->update("cab_booking", $update);
                return array("status" => 1, "message" => "success");
            }
            else
            {
                return array("status" => 0, "message" => $desc);
            }

        }
        else
        {
            return array("status" => 0, "message" => SERVER_ERROR);
        }
        //echo $this->db->last_query();

    }
    function save_invoicepdf_name($data)
	{
		  $update_data=array("pdf_url"=>$data['filename']);
		  $this->db->where("booking_id", $data['booking_id']);
           
        $update = $this->db->update("payment_detail", $update_data);
		// echo $this->db->last_query();
		if($update)
		{
			return true;
		}else
			{
				return false;
			}
	}
	function remaining_payment()
	{
		$today=date("Y-m-d");
		$date =date('Y-m-d', strtotime($today. ' - 2 day'));
		/*
		 * SELECT b.id,b.user_id,p.billing_amount FROM `cab_booking` b 
		 * join payment_detail p on p.booking_id=b.id 
		 * where date(booking_time) <="2016-02-14" and b.status='TRIP_ENDED'
		 * */
		 $where=array("date(booking_time)<="=>$date,"b.status"=>"TRIP_ENDED");
		 $this->db->select('b.id as booking_id,b.user_id,p.billing_amount');
		 $this->db->from("cab_booking b");
		 $this->db->join("payment_detail p","p.booking_id=b.id");
		 $this->db->where($where); 
		// $this->db->limit("1");
		 $query=$this->db->get();
		 //echo $this->db->last_query();
		 if($query->num_rows()>0)
		 {
		 	return $query->result_array();
		 }else
		 	{
		 		return false;
		 	}
		 
	}
	function ExpireCard()
	{
		
       $expair_date=date("m").'/'.date("Y");			
	   $query="select user.email from card_info join user on user.id=card_info.	customer_id where expire_date='$expair_date'";
		$res=$this->db->query($query);
		echo $this->db->last_query();
		if($res->num_rows()>0)
		{
			return $res->result_array();
		}else
			{
				return false;
			}
		
		
	}
}

?>