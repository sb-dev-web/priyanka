<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<style>
	.shift_end{
		color:#C03B44;
		padding-right: 5px;
		font-weight: bold;
	}
	.shift_start{
		color:#75CE66;
		padding-right: 5px;
		font-weight: bold;
		
	}
	.cab_number
	{
		color:#303E49;
		padding: 2px;
	}
	.cab_model
	{
		color:#BAC4CB;
		padding: 2px;
	}
	.break_started, .shift_ended,.trip_ended,.driver_cancelled,.booking_rejected
	{
		color:#C03B44;
		padding-right: 5px;
		font-weight: bold;
	}
	.break_ended,.shift_started,.break_ended,.trip_started,.booking_accepted
	{
		color:#75CE66;
		padding-right: 5px;
		font-weight: bold;
	}
	.five_min_away,.i_am_here
	{
		color:#303E49;
		padding-right: 5px;
		font-weight: bold;
	}
	
</style>
<?php 
$driver_process=array("SHIFT_STARTED"=>"Shift Start",
					   "BREAK_STARTED"=>"Break Start",
					   "BREAK_ENDED"=>"Break End",
					   "BOOKING_ACCEPTED"=>"Booking Accept",
					   "FIVE_MIN_AWAY"=>"Five Min Away",
					   "I_AM_HERE"=>"I Am Here",
					   "TRIP_STARTED"=>"Trip Start",
					   "TRIP_ENDED"	=>"Trip End",
					   "BOOKING_ASSIGN"=>"Pre-Booking Assign",
					   "DRIVER_CANCELLED"=>"Booking Cancel",
					   "SHIFT_ENDED"=>"Shift Ended",
					   "BOOKING_REJECTED"=>"Booking Reject"
					   
);

$driver_icon=array("SHIFT_STARTED"=>"cd-picture",
					   "BREAK_STARTED"=>"cd-movie",
					   "BREAK_ENDED"=>"cd-picture",
					   "BOOKING_ACCEPTED"=>"cd-picture",
					   "FIVE_MIN_AWAY"=>"cd-location",
					   "I_AM_HERE"=>"cd-location",
					   "TRIP_STARTED"=>"cd-picture",
					   "TRIP_ENDED"	=>"cd-movie",
					   "BOOKING_ASSIGN"=>"cd-picture",
					   "DRIVER_CANCELLED"=>"cd-movie",
					   "SHIFT_ENDED"=>"cd-movie",
					   "BOOKING_REJECTED"=>"cd-movie"
					   
);

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='http://fonts.googleapis.com/css?family=Droid+Serif|Open+Sans:400,700' rel='stylesheet' type='text/css'>

<!--	<script src="js/modernizr.js"></script> <!-- Modernizr -->
  	
	<title>Private Driver Report</title>
</head>
<body>
	<!---<header>
		<h1>Responsive Vertical Timeline</h1>
	</header>-->
<?php if(!$report_data)   {?>
    	<div class="break_started" align "center" style="color: #C03B44; size: 15px; padding-left: 25px">
    	Driver Activity Not Found
    	</div>
  <?php exit; }  ?>
	<section id="cd-timeline" class="cd-container" style="min-height: 500px; overflow: auto; scroll-behavior: auto; ">
    <?php 
    
    $count=0;
    foreach($report_data as $report){
               
        ?>
		<div class="cd-timeline-block">
			<div class="cd-timeline-img <?php echo $driver_icon[$report['status']] ?>">
				<img src="<?php echo base_url(); ?>images/<?php echo $report['status']?>.svg" alt="Picture">
			</div> <!-- cd-timeline-img -->

			<div class="cd-timeline-content">
				<span class="<?php echo strtolower($report['status']) ?>"><?php echo $driver_process[$report['status']] ?></span>
				<!--<p><?php echo $report['cab_title']  ?><?php echo $report['cab_number'] ?><?php $report['car_model'] ?></p>
			-->
			<?php if($report['status']=='SHIFT_STARTED' || $report['status']=='SHIFT_ENDED') {?>
			<span class="cab_title"><?php echo $report['cab_type']  ?></span>
        				<span class="cab_number"><?php echo $report['plate_number'] ?></span>
        				<span class="cab_model"><?php $report['cab_model'] ?></span>
        				<?php } ?>
        		<?php if($report['status']=='BOOKING_REJECTED' || $report['status']=='BOOKING_ASSIGN' 
        		         || $report['status']=='TRIP_ENDED' || $report['status']=='TRIP_STARTED' 
        		         || $report['status']=='DRIVER_CANCELLED' || $report['status']=='BOOKING_ACCEPTED'
						 || $report['status']=='I_AM_HERE' || $report['status']=='FIVE_MIN_AWAY') {?>
        		<span class="cab_number"> <a href="javascript:void(0);" onclick="booking_detail(<?php echo $report['booking_id'] ?>)"><?php echo  $CBN = "PD" . sprintf("%05d", $report['booking_id']); ?></a></span>
        				<?php } ?>		
				<span class="cd-date"><?php echo date("d-M-Y H:i",strtotime(gmt_to_norway($report['createdon']))) ?></span>
			</div> <!-- cd-timeline-content -->
		</div> 
		
<?php $count++;} ?>
		 <!-- cd-timeline-block -->
	
	</section> <!-- cd-timeline -->
	<!-------BOOKING DETAIL----->
	 <div class="modal fade" id="open_customer1" tabindex="-1" role="dialog" aria-hidden="true" >
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
               Booking Detail
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
               <div id="showreport1" name="showreport"></div>
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 

</body>
</html>


<script type="text/javascript">
	
	     function booking_detail(booking_no)
        {
        	//var booking_no=1087;
        	
           $.ajax({
            type:'POST',
            url: '<?php echo site_url('DriverTimeline/getbookingdetail');?>',
            data:{"booking_no":booking_no},
             beforeSend: function() {
               $("#loading-image").show();
             },
            success: function(response) {
           // alert(response);
             $("#showreport1").html(response);
             $('#open_customer1').modal({
                 "backdrop" : "static"
               })
              $("#loading-image").hide();
           }});
        }
          
	
	
</script>
