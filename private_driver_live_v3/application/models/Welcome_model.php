<?php

class Welcome_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    function fetch_total_driver()
    {
        $query=$this->db->query("select count(driver_id) as total_driver from driver where isdelete='0'");
        //echo $this->db->last_query();
        return $query->result();
    
    }
    function fetch_total_cab()
    {
        $query=$this->db->query("select count(cab_id) as total_cab from cab where isdelete='0'");
        //echo $this->db->last_query();
        return $query->result();
    
    }
     function fetch_total_passanger()
    {
        $query=$this->db->query("select count(id) as total_passanger from user");
        //echo $this->db->last_query();
        return $query->result();
    
    }
     function fetch_total_booking()
    {
        $query=$this->db->query("select count(id) as total_booking from cab_booking");
        //echo $this->db->last_query();
        return $query->result();
    
    }
    function fetch_advance_booking()
    {
        $date=date('Y-m-d');
        $query=$this->db->query("select count(id) as advance_booking from cab_booking WHERE is_prebooking='Y' and date(booking_time)>='$date' and status in('WAITING')");
       
        return $query->result();
    }
    function fetch_due_amount()
    {
        $query=$this->db->query("SELECT sum(billing_amount) as due_amount FROM payment_detail where is_paid='0'");
        return $query->result();
    }
    function fetch_total_amount()
    {
        $query=$this->db->query("SELECT sum(billing_amount) as billing_amount FROM payment_detail where is_paid='1'");
        return $query->result();
    }
	
	function GoldCustomer()
	{
		$query="SELECT count(cab_booking.id) count ,user_id,name,company_name,email FROM `cab_booking` join user on user.id=cab_booking.user_id where  status in ('TRIP_ENDED','FINISH') group by user_id 
ORDER BY count(cab_booking.id)  DESC limit 0,5";
$query =$this->db->query($query);
return $query->result_array();
	}
	
	function GoldDriver()
	{
		$query="SELECT count(cab_booking.id) count ,user_id,first_name,last_name,contact,email_id FROM `cab_booking` join driver on driver.driver_id=cab_booking.driver_id where  cab_booking.status in ('TRIP_ENDED','FINISH') group by cab_booking.driver_id 
ORDER BY count(cab_booking.id)  DESC limit 0,5";
        $query =$this->db->query($query);
		 return $query->result_array();
	}
	
	
	
}

?>