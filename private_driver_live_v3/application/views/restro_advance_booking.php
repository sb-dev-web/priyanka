<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Pre Booking
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active"> Pre Booking</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Booking Number</th>
                      
                        
                        <th>Pickup Time</th>
                       
                        <th>Pickup_Address</th>
                        <th>Booking Time</th>
                       <th>Cab Type</th>
                       <th>Area</th>
                        <th>Assgined Driver</th>
                        <th>Assgin</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                       
                      if(!empty($advance_booking))
                      
                      {
                        for($i=0;$i<sizeof($advance_booking);$i++)
                        {
                         
                          $name=$advance_booking[$i]['name'];
                         
                         
                           
                           $pick_address=$advance_booking[$i]['pickup_location'];
                           if($advance_booking[$i]['driver_id']==0)
                           {
                            $driver_name="Not Yet Assgin";
                            
                           }
                           else
                           {
                            $driver_name=$advance_booking[$i]['first_name'].' '.$advance_booking[$i]['last_name'];
                           }
                           $client_no=$advance_booking[$i]['rcode'].$advance_booking[$i]['client_contact_number'];
                           $booking_time=date('d-M-Y H : i : s', strtotime($advance_booking[$i]['booking_time']));
                         
                      ?>
                      <tr>
                        <td><a href="javascript:void(0);" onclick="customer_detail('<?php echo $advance_booking[$i]['email'];?>','<?php echo $advance_booking[$i]['contact_number'];?>','<?php echo ucfirst($name);?>','<?php echo $pick_address;?>','<?php echo $booking_time;?>','<?php echo $client_no; ?>','<?php echo $advance_booking[$i]['client_name'];?>');"><?php echo $advance_booking[$i]['booking_number'];?></a></td>
                       
                        
                        <td><?php  $timestamp = gmt_to_norway($advance_booking[$i]['booking_time']);
                           echo $timestamp;?></td>
                        <td><?php echo $pick_address;?></td>
                        <td><?php
                        $created = gmt_to_norway($advance_booking[$i]['createdon']);
                           echo $created;
                        
                        ?></td>
                         <td><?php
                         if($advance_booking[$i]['cab_type']=='1')
                         {
                          echo $cab='FIRST Class';
                         }
                         elseif($advance_booking[$i]['cab_type']=='2')
                         {
                          echo $cab='BUSINESS Class';
                         }
                         else
                         {
                            echo $cab='LUXURY Class';
                         }
                         ?>
                         </td> 
                         <td><?php echo ($advance_booking[$i]['area']==1) ? "Oslo" : "Akershus" ?></td>
                          <td><?php echo $driver_name;?></td>
                          
                        
              
                       <td><a href="javascript:void(0);" onclick="assigndriver('<?php echo $advance_booking[$i]['bid'];?>')">
                        <span  class="label label-warning">Assign</span></a>
                                   
                     </td>
                      
                      </tr> 
                      
                      <?php
                      
                        }
                      }
                      else
                      {
                        
                      
                      ?>
                      <tr>
                        <td colspan="10"><center>No data Found</center></td>
                        
                      </tr>
                      <?php
                      }
                      ?>
 
                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                 <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
              
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <!-- open customer pop up -->
<div class="modal fade" id="open_customer" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
              Pre Booking Detail
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="text-align: left; margin: 0 auto; width: 80%;">
               
                 Name:- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input id="customername" style="width:247px;border:none; font-size:18px;font-weight:bold;float:right;"><br/><br/>                       
                   Email:- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input id="customeremail" style="width:247px;border:none; font-size:18px;font-weight:bold;float:right;"><br/><br/> 
                <!-- Contact:- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input id="customerphone" style="width:247px;border:none; font-size:18px;font-weight:bold;float:right;"><br/><br/>--> 
                Customer:- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input id="clientname" style="width:247px;border:none; font-size:18px;font-weight:bold;float:right;"><br/><br/> 
                Contact:- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input id="clientno" style="width:247px;border:none; font-size:18px;font-weight:bold;float:right;"><br/><br/> 
                <br>
                 Pickup    :-&nbsp;&nbsp;&nbsp;&nbsp;
                 <!--<input id="customerpickup" style="width:247px;border:none; font-size:18px;font-weight:bold;margin-left:44px;">--><textarea rows="3" cols="50" id="customerpickup" name="customerpickup" style="width:247px;border:none; font-size:18px;font-weight:bold;margin-left:155px;margin-top:-9px;float:right;"></textarea> <br/><br/>   
               
                 Booking :-&nbsp;<input id="customerbookingtime" style="width:247px;margin-top:-40px;border:none; font-size:18px;font-weight:bold;margin-left:53px;float:right"> <br/><br/> 
                   
                    <div class="item" style="height: 19px!important;">
                      
                    </div>
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
    </div>   
</div>
            
            
            
<!-- modal for select driver -->



<div class="modal fade" id="open_driver" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
               Assgin Driver
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="text-align: center; margin: 0 auto; width: 80%;">
                <form id="saveDRIVER" action="" method="post" autocomplete="off">
                   
                    <input type="hidden" id="bookingid" name="bookingid"/>
                     <p>
                                <label>Driver</label></label>
                                <span class="formwrapper item">
                                  <select size="1" name="se_driver" id="se_driver"  class="chosen  validate[required]"  style="width:290px;height:30px;"> 
                                      <?php
									  
									  if(sizeof($driver))
									  {
										
										for($i=0;$i<sizeof($driver);$i++)
										{
											$name=$driver[$i]['first_name'].' '.$driver[$i]['last_name'];
									  ?>
									  <option value="<?php  echo $driver[$i]['driver_id'];?>"id="driver_id"><?php echo $name;?></option>
                                      <?php
										}
									  }
									  ?>
                                    </select>
                                </span>
                            </p>
                    <div class="item" style="height: 70px!important;">
                      
                    </div>
                    
                    <div style="margin-top: 30px;">
                        <input  class="btn btn-info btn-lg" type="button" value="Assgin" id="a_driver" name="a_driver"/>
                        <span id="loader_note" class="loader" style="display: none;">
                            <img src="http://dev.privatedriverapp.com/app/images/loader19.gif" alt=""/>
                        </span>
                    </div>
                </form>    
                </div>
            </div>
        </div> 
    </div>   
</div>

      
      
      
      
      
      
      
      
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
     
      </aside><!-- /.control-sidebar -->
 <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<!--     <script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>-->
<script type="text/javascript" src="<?=base_url();?>js/my_custom.js"></script>


<!-- choosen -->

     
      
       <script>
        /* function for assgin driver */
        function assigndriver(bid) {
         // alert(bid);
         
          $('#bookingid').val(bid);
        
         $('#open_driver').modal({
                 "backdrop" : "static"
         })
             }
        /* assgin drive ron sumbit click */
            $('#a_driver').click(function(){
        var bookingid=$('#bookingid').val();
        var driverid=$('#se_driver').val();
      
          restrovalidateassgindriver(bookingid,driverid);
        });
            
        /* get customer detail */
        function customer_detail(cemail,cphone,cname,pick_address,bookingtime,client_no,client_name)
        {
          //alert(cname);
          // var sd="<div><div>Customer name:- <span style=margin-left:6px; float:right;>Xyz</div><br>\
         //  <div>Customer email:- xyz@gmail.com</div>";
           
           $('#customername').val(cname);
           $('#customeremail').val(cemail);
           $('#customerphone').val(cphone);
          $('#customerpickup').val(pick_address);
           $('#customerbookingtime').val(bookingtime);
           $('#clientno').val(client_no);
           $('#clientname').val(client_name);
         $('#open_customer').modal({
                 "backdrop" : "static"
         })
             }
          
          
        
            
         </script>