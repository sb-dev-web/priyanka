<?php
if(isset($driver[0]->driver_id))
   {
   $password='';
   }
   else
   {
     $password=' validate[required]';
   }
   ?>

<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Driver</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Driver</a></li>
      <li class="active">Add Driver</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="save_driver" action="" method="post" enctype="multipart/form-data" autocomplete="off">
              
             <input type="hidden" value="<?=(isset($driver[0]->driver_id))?$driver[0]->driver_id:"";?>"  name="id"  />
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">First Name</label>
                      <input type="text" class="form-control validate[required]" name="first_name" id="first_name" value="<?=(isset($driver[0]->first_name))?$driver[0]->first_name:"";?>" placeholder="First Name">
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Last Name</label>
                      <input type="text" class="form-control validate[required]" name="last_name" value="<?=(isset($driver[0]->last_name))?$driver[0]->last_name:"";?>" id="last_name" placeholder="Last Name">
                  </div>  
                </div>
              </div>
              
               <?php
              if(isset($driver[0]->email_id))
              {
               
              }
              else
              {
                ?>
                <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email_id">Email</label>
                      <input type="email" class="form-control validate[required, custom[email]]" id="email_id" value="" name="email_id" placeholder="Enter email" onchange="check_email();">
                  </div>  
                </div>
              </div>
                <?php
              }
              ?>
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email_id">Contact</label>
                      <input type="contact" class="form-control validate[required]" id="contact" value="<?=(isset($driver[0]->contact))?($driver[0]->contact):"";?>" name="contact" placeholder="Enter contact" >
                  <p class="help">ex. +47123456799</p>
                  </div>  
                </div>
              </div>
             
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="Password">Password</label>
                      <input type="password" class="form-control <?php echo $password;?>" id="password" value="" name="password" placeholder="Enter Password">
                  </div>  
                </div>
              </div>
            
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="gender">Gender</label><br/>
                      <input type="radio" name="optionsRadios" id="male" value="m" <?php if(isset($driver[0]->gender)){ if($driver[0]->gender=='m'){ echo 'checked';}else{echo "";}}?> checked> Male
                      &nbsp;&nbsp;<input type="radio" name="optionsRadios" id="female"  value="f" <?php if(isset($driver[0]->gender)){if($driver[0]->gender=='f'){ echo 'checked';}else{echo "";}}?> > Female
                  </div>  
                </div>
              </div>
              <!--<div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Contact</label>
                    <input type="type" class="form-control validate[required]" id="contact" name="contact" value="<?=(isset($driver[0]->contact))?$driver[0]->contact:"";?>" placeholder="Enter contact">
                  </div>  
                </div>
              </div>-->
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Address</label>
                   
                   <input type="type" class="form-control validate[required]" id="address" name="address" value="<?=(isset($driver[0]->address))?$driver[0]->address:"";?>" placeholder="Enter Address">
                  </div>  
                </div>
              </div>
              
             <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Zipcode</label>
                    <input type="type" class="form-control validate[required]" id="zipcode" name="zipcode" value="<?=(isset($driver[0]->zipcode))?$driver[0]->zipcode:"";?>" placeholder="Enter Zipcode">
                  </div>  
                </div>
              </div>
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">City</label>
                    <input type="type" class="form-control validate[required]" id="city" name="city" value="<?=(isset($driver[0]->city))?$driver[0]->city:"";?>" placeholder="Enter City">
                  </div>  
                </div>
              </div>
              
             
           
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Driver Photo</label>
                			    <div class="fileupload fileupload-new" data-provides="fileupload">
                    				<div class="input-append">
                    				<div class="uneditable-input span3">
                    				    <i class="iconfa-file fileupload-exists"></i>
                    				    <span class="fileupload-preview"></span>
                    				</div>
                    				<span class="btn btn-file"><span class="fileupload-new">Select file</span>
                    				<span class="fileupload-exists">Change</span>
                    				<input type="file" id="category_icon" name="category_icon" onchange="uploadCategory('<?php echo site_url().'Uploader/category'; ?>','save_driver','category_icon','category_icon_name','original_name','file_loading','fileupload-preview')" /></span>
                    				<?php
                                    if($driver[0]->profile_pic!="")
                                    {
                                    ?>
                                     <img src="<?php  echo base_url()."images/profile/".$driver[0]->profile_pic; ?>" width="50" height="35"/>
                                     <?php
                                     }
                                     ?>
                                    <input type="hidden" id="category_icon_name" name="category_icon_name" value="<?=(isset($driver[0]->profile_pic))?$driver[0]->profile_pic:"";?>" />
                                    <input type="hidden" id="original_name" name="original_name" value="" />
                    				</div>
                                    <div id="file_loading" style="display: none;">
                                        <img src="<?php echo base_url(); ?>images/loader19.gif" />
                                    </div>
                                    
                			    </div>
                  </div>
                </div>
              </div>
           
           <!-- uploading file -->
           <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Driver License Image</label>
                			    <div class="fileupload fileupload-new" data-provides="fileupload">
                    				<div class="input-append">
                    				<div class="uneditable-input span3">
                    				    <i class="iconfa-file fileupload-exists"></i>
                    				    <span class="fileupload-preview1"></span>
                    				</div>
                    				<span class="btn btn-file"><span class="fileupload-new">Select file</span>
                    				<span class="fileupload-exists">Change</span>
                    				<input type="file" id="license_icon" name="license_icon" onchange="uploadLicense('<?php echo site_url().'Uploader/license'; ?>','save_driver','license_icon','license_icon_name','original_name_id','file_loading1','fileupload-preview1')" /></span>
                    				<?php
                                    if(($driver[0]->license_image)!="")
                                    {
                                    ?>
                                     <img src="<?php  echo base_url()."images/profile/".$driver[0]->license_image; ?>" width="50" height="35"/>
                                     <?php
                                     }
                                     ?>
                                    <input type="hidden" id="license_icon_name" name="license_icon_name" value="<?=(isset($driver[0]->license_image))?$driver[0]->license_image:"";?>" />
                                    <input type="hidden" id="original_name_id" name="original_name_id" value="" />
                    				</div>
                                    <div id="file_loading1" style="display: none;">
                                        <img src="<?php echo base_url(); ?>images/loader19.gif" />
                                    </div>
                                    
                			    </div>
                  </div>
                </div>
              </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" id="submit" name="submit">Submit</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
 <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>
<!--<script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>
    
<script type="text/javascript">
//var $ = jQuery.noConflict();
$(document).ready(function(){

    function submit_form(formData,status)
    {
        if(status==true)
        {
            var querystr = $("#save_driver").serialize();
            
            
            
            $.ajax({
                url		:	"<?php echo site_url('Driver/submit_driver'); ?>",
                type	:	"POST",
                data	:	querystr,
                //async: false,
               // cache: false,
                //contentType: false,
                //processData: false,
                
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                 var r = confirm("Driver is added successfully"); 
                
                    if(r==true) // 
                    {
                      
                    
                    $("#loader").hide();
                 
                    setTimeout(function() {
                        $.bootstrapGrowl("Driver is added successfully", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_driver')[0].reset();
                    window.location='<?php echo site_url('driver'); ?>';
                  }
                  else
                  {
                    $("#loader").hide();
                     $('#save_driver')[0].reset();
                    //window.location='<?php echo site_url('login/dashboard'); ?>';
                    //alert('failed to save');
                  }
                 }
            });
            return false;
        }
        
    }
            
      
    
  $("#save_driver").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});
</script>

<script>

$("#contact").change(function() {
  var contact=$("#contact").val();
  var FirstChar=contact.charAt(0);
  if(FirstChar!="+")
  {
  
  	$("#contact").val("");
  }
});

function check_email()
{
  var email=$('#email_id').val();
   $.ajax({
                url		:	"<?php echo site_url('Driver/check_email'); ?>",
                type	:	"POST",
                data	:	{'email_id':email},
               
                
                beforeSend  :   function(){
                                   
                                    //alert('show loader');
                                },   
                success	:	function(data){
                  if(data == 1)
                  {
                   
                    setTimeout(function() {
                        $.bootstrapGrowl("This email is already exist", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                   $('#submit').prop('disabled', true);
                  }
                  
                   
                          else
                       {
                   $('#submit').removeAttr('disabled');;
                  }
                 }
            });
            return false;
}

 

</script>     
