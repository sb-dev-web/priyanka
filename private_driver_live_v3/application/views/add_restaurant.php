  <script type="text/javascript" src="<?=base_url();?>js/my_custom.js"></script>
<?php
if(isset($restaurant[0]->id))
   {
   $password='';
   }
   else
   {
     $password=' validate[required]';
   }
   ?>

<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Restaurant</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Restaurant</a></li>
      <li class="active">Add Restaurant</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="save_restaurant" action="" method="post" enctype="multipart/form-data" autocomplete="off">
              
             <input type="hidden" value="<?=(isset($restaurant[0]->id))?$restaurant[0]->id:"";?>"  name="id"  />
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Restaurant  Name</label>
                      <input type="text" class="form-control validate[required,custom[onlyLetterSp]]" name="name" id="name" value="<?=(isset($restaurant[0]->name))?$restaurant[0]->name:"";?>" placeholder="Name">
                  </div>
                </div>
              </div>
              
              
              
               <?php
              if(isset($restaurant[0]->email))
              {
               
              }
              else
              {
                ?>
                <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email_id">Restaurant  Email</label>
                      <input type="email" class="form-control validate[required, custom[email]]" id="email" value="" name="email" placeholder="Enter email" onchange="check_email();">
                  </div>  
                </div>
              </div>
                <?php
              }
              ?>
              
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="contact">Contact</label>
                      <input type="contact" class="form-control validate[required]" id="contact" value="<?=(isset($restaurant[0]->contact_number))?$restaurant[0]->contact_number:"";?>" name="contact" placeholder="Enter contact" onchange="check_contact(this.value);">
                  </div>  
                </div>
              </div>
             
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="Password">Password</label>
                      <input type="password" class="form-control <?php echo $password;?>" id="password" value="" name="password" placeholder="Enter Password" onchange="check_password();">
                  </div>  
                </div>
              </div>
            
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="Password">Confirm Password</label>
                      <input type="password" class="form-control <?php echo $password;?>" id="c_password" value="" name="c_password" placeholder="Enter Password" onchange="check_password();">
                  </div>  
                </div>
              </div>
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Address</label>
                    <textarea id="address" name="address" class="form-control validate[required]" rows="3" placeholder="Enter ..."><?php if(isset($restaurant[0]->address)) {echo $restaurant[0]->address; } else { echo "";}?></textarea>
                  </div>  
                </div>
              </div>
              
             <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">latitude</label>
                    <input type="text" class="form-control validate[required]" id="latitude" name="latitude" value="<?=(isset($restaurant[0]->latitude))?$restaurant[0]->latitude:"";?>" placeholder="Enter Latitude"><input type="button" class="btn btn-primary" value="Cilck Here" onclick="map();"style="margin-top:-57px;margin-left: 320px;">
                  </div>  
                </div>
              </div>
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">longitude</label>
                    <input type="type" class="form-control validate[required]" id="longitude" name="longitude" value="<?=(isset($restaurant[0]->longitude))?$restaurant[0]->longitude:"";?>" placeholder="Enter Longitude">
                  </div>  
                </div>
              </div>
              
              
            
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" id="submit" name="submit">Submit</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
         <!-- <div class="modal fade" id="open_map" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
             Restaurant address
                <button style="color: white!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -5px;border:2px solid black;">
           
                <iframe src="<?php echo base_url();?>index.php/Map/map_view"></iframe>
                
               
                  
                   
                    
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
    </div>   
</div> -->
            
            
            
            
            
            
            
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
 <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>
<!--<script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>js/my_custom.js"></script>
<script type="text/javascript">
//var $ = jQuery.noConflict();
$(document).ready(function(){
 
    function submit_form(formData,status)
    {
        if(status==true)
        {
            var querystr = $("#save_restaurant").serialize();
            
            
            
            $.ajax({
                url		:	"<?php echo site_url('Restaurant/submit_restaurant'); ?>",
                type	:	"POST",
                data	:	querystr,
                //async: false,
               // cache: false,
                //contentType: false,
                //processData: false,
                
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                  var r = confirm("Restaurant is added successfully"); 
                
                    if(r==true) // 
                    {
                    $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl("Restaurant is added successfully", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_restaurant')[0].reset();
                    window.location='<?php echo site_url('Restaurant'); ?>';
                  }
                  else
                  {
                    $("#loader").hide();
                      $('#save_restaurant')[0].reset();
                    //window.location='<?php echo site_url('login/dashboard'); ?>';
                    //alert('failed to save');
                  }
                 }
            });
            return false;
        }
        
    }
            
      
    
  $("#save_restaurant").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});
</script>

<script>

function check_email()
{
  var email=$('#email').val();
   $.ajax({
                url		:	"<?php echo site_url('Restaurant/check_email'); ?>",
                type	:	"POST",
                data	:	{'email':email},
               
                
                beforeSend  :   function(){
                                   
                                    //alert('show loader');
                                },   
                success	:	function(data){
                  if(data == 1)
                  {
                   
                    setTimeout(function() {
                        $.bootstrapGrowl("This email is already exist", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                   $('#submit').prop('disabled', true);
                  }
                  
                   
                          else
                       {
                   $('#submit').removeAttr('disabled');
                  }
                 }
            });
            return false;
                  }
 

</script>

<script>
  function check_password()
{
  var password=$('#c_password').val();
  var password1=$('#password').val(); 
                  if(password==password1)
                  {
                   
                     $('#submit').removeAttr('disabled');; 
                  }
                  else
                  {
                     setTimeout(function() {
                        $.bootstrapGrowl("Password not match", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                   $('#submit').prop('disabled', true);
                  }
                  
            return false;
                  }
 
  
</script>
<script>
  function  map()
  {
    javascript:window.open("<?php echo base_url();?>index.php/Map/map_view","","width=500,height=500scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes");
 
  }
</script>