<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Drivers
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Drivers</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <a href="<?php echo site_url('Driver/add_driver');?>"><input type="button" class="btn btn-block btn-warning" value="Add" style="float:right; margin-right:7px;margin-top:4px; width:10%;"/></a><br/><br/>
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <!--<th>Profile</th>-->
                        <th>Email</th>
                       <th>Rating</th>
                      
                       <!-- <th>Country</th>-->
                      
                        <th>Cab&nbsp;Assigned</th>
                        <th>Driver Location</th>
                         <th>Login&nbsp;Status</th>
                          <th>Shift&nbsp;Status</th>
                        <th>Status</th>
                        <th>Current Activity</th>
                        <th style="width: 10%; text-align: center;">Action</th>
                      
                      
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(sizeof($driver))
                      {
                       
                        for($i=0;$i<sizeof($driver);$i++)
                        {
                          $name=$driver[$i]->first_name.' '.$driver[$i]->last_name;
                          $cab_type=$driver[$i]->cab_type;
                          if($cab_type=='1')
                          {
                            $type='First';
                             }
                             else if($cab_type=='2')
                             {
                              $type='Business';
                             }
                             elseif($cab_type=='3')
                             {
                              $type='Luxury';
                             }
                             else
                             {
                              $type='';
                             }
                          
                      ?>
                      <tr>
                        <td style="font-size:14px;"><?php echo $name;?></td>
                        <!--<td><?php echo $driver[$i]->profile_pic;?></td>-->
                         <td><?php echo $driver[$i]->email_id;?></td>
                         <td><?php echo $DriverRating[$driver[$i]->driver_id] ?></td>
                        <td><?php
                        if($driver[$i]->cab_model=='')
                        {
                          $cab='No Cab';
                          echo $cab;
                        }
                        else
                        {
                        $cab=$driver[$i]->cab_model;
                         echo $cab.'('.$type.')';
                        }
                       
                        ?></td>
                       
                       <!-- <td><?php echo $driver[$i]->country;?></td>-->
                        <!--<td><?php echo $driver[$i]->state;?></td>-->
                       
                        
                        <td>
                       <input id="btnShow" type="button" value="Show On Map" onclick="pop_upmap('<?php echo $driver[$i]->latitude;?>','<?php echo $driver[$i]->longitude;?>');" /></td>
                      <td><a href="javascript:void(0);" onclick="changeloginstatus('<?php echo $driver[$i]->driver_id;?>','loading1_<?php echo $driver[$i]->driver_id;?>', 'span1_<?php echo $driver[$i]->driver_id;?>')">
                                    <?php if($driver[$i]->islogin=='Y'){?>
                                    <span id="span1_<?php echo $driver[$i]->driver_id?>" class="label label-success">Logged In</span></a>
                                    <?php }else{?>
                                    <span id="span1_<?php echo $driver[$i]->driver_id?>" class="label label-warning">Logged Out</span></a>
                                    <?php }?>
                                    <br /><div id="loading1_<?php echo $driver[$i]->driver_id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                     </td>
                      
                       <!-- change shift status !-->
                      
                      <td>
                                    <?php if($driver[$i]->shift_status=='STARTED'){?>
                                   <a href="javascript:void(0);" onclick="changeshiftstatus('<?php echo $driver[$i]->s_id;?>','loading_<?php echo $driver[$i]->s_id;?>', 'span_<?php echo $driver[$i]->s_id;?>')">
                                    <span id="span_<?php echo $driver[$i]->s_id?>" class="label label-success">Started</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $driver[$i]->s_id?>" class="label label-warning">Ended</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $driver[$i]->s_id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                     </td>
                      
                      
                       <td><a href="javascript:void(0);" onclick="changeStatus('<?php echo $driver[$i]->driver_id;?>','loading_<?php echo $driver[$i]->driver_id;?>', 'span_<?php echo $driver[$i]->driver_id;?>')">
                                    <?php if($driver[$i]->isactive=='0'){?>
                                    <span id="span_<?php echo $driver[$i]->driver_id?>" class="label label-warning">Inactive</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $driver[$i]->driver_id?>" class="label label-success">Active</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $driver[$i]->driver_id;?>" style="display: none;"><img src="<?php echo base_url();?>/images/loader19.gif" /></div>

                     </td>
                       <td><?php  $check_status=$driver[$i]->status;
                       if($check_status=='LOGOUT')
                       {
                        echo 'Logged Out';
                       }
                       else if($check_status=='BREAK')
                       {
                        echo 'On Break';
                       }
                       else if($check_status=='RESTRO_BUSY')
                       {
                        echo 'Busy';
                       }
                         else if($check_status=='AVAILABLE')
                       {
                        echo 'Available';
                       }
                         else if($check_status=='LOGIN')
                       {
                        echo 'Logged In';
                       }
                         else if($check_status=='LOGIN')
                       {
                        echo 'Logged In';
                       }
                         else if($check_status=='TEMP_HOLD')
                       {
                        echo 'On Hold';
                       }
                        else if($check_status=='UN_AVAILABLE')
                       {
                        echo 'Not Availabel';
                       }
                         else if($check_status=='BUSY')
                       {
                        echo 'Busy';
                       }
                       ?></td>
                       <td>
                        
                       <a href="<?=site_url('Driver/add_driver').'/'.$driver[$i]->driver_id; ?>" title="Edit"><i class="fa fa-pencil-square-o fa-2x"></a></i>
                       <img src="<?php echo base_url();?>images/loader19.gif" id="image_<?=$driver[$i]->driver_id;?>" style="display: none;"/>
                    |&nbsp;
                      <a href="javascript:void(0);" onclick="changedeleteStatus('<?php echo $driver[$i]->driver_id;?>','delloading_<?php echo $driver[$i]->driver_id;?>', 'spandel_<?php echo $driver[$i]->driver_id;?>')">
                                    <?php if($driver[$i]->isdelete=='0'){?>
                                    <span id="spandel_<?php echo $driver[$i]->driver_id?>" class="fa fa-trash fa-2x" style="color:red;"></span></a>
                                    <?php }?>
                                    <br /><div id="delloading_<?php echo $driver[$i]->driver_id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                      </td>
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      ?>
                      
                      
                      
                    </tbody>
                    <tfoot>
                       
                    </tfoot>
                  </table>
                  <?php
                  
                   ?>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
        
          </div>
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      
      <!-- Control Sidebar -->      
      
 <!--pop up -->
 <div id="dialog" style="display: none">
  <span id="ui-id-1" class="ui-dialog-title"></span>
        <div id="dvMap" style="margin-top:-345px;height: 380px; width: 580px; border:3px solid black;">
        </div>
    </div>
      
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <!-- <script type="text/javascript" src="<?=base_url();?>js/jquery.jgrowl.js"></script>-->

<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.bootstrap-growl.min.js"></script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
   
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css">

       <script type="text/javascript">
      $(function () {
        $("#example1").dataTable();
       
      });
    </script>
       <script>
       function changeStatus(id,loadingdiv,spanid)
{
  //alert();
  var r = confirm("Are you sure you want to change status?");
    if (r == true) {
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    if(spanValue=='Inactive')
    {  
       status = '1';
    }
    $.ajax({type:'POST',url: '<?php echo site_url('Driver/change_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       statusfeedback(response,spanid,loadingdiv);
     }});
}
else
{
  
}
       }
function statusfeedback(retstatus,spanid,loadingdiv) 
{
    //alert(retstatus);
    if(retstatus=='0')
    {
        $('#'+spanid).removeClass('label-success').addClass('label-warning');
        $('#'+spanid).text("Inactive");
    }
    else if(retstatus=='1')
    {

        $('#'+spanid).removeClass('label-warning').addClass('label-success');
        $('#'+spanid).text("Active");
    }
     $('#'+loadingdiv).css('display','none');
    
}
</script>
<script>
function changedeleteStatus(id,loadingdiv,spanid)
{
  
    var r = confirm("Are you sure you want to delete this driver");
    if (r == true) {
       var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    $.ajax({type:'POST',url: '<?php echo site_url('Driver/change_delete_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       $("#image_"+id).closest('tr').fadeOut('slow')
     }});
    } else {
       
    }
   
}
   
</script>

  <script type="text/javascript">
      
            function pop_upmap(lat,long)
            {
                
                $("#dialog").dialog({
                    modal: true,
                    title: "Google Map",
                    width: 600,
                    hright: 450,
                    buttons: {
                        Close: function () {
                            $(this).dialog('close');
                        }
                    },
                   
                   
                    open: function () {
                        var mapOptions = {
                            center: new google.maps.LatLng(lat,long),
                            zoom: 18,
                           
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        }
                        var map = new google.maps.Map($("#dvMap")[0], mapOptions);
                        var marker = new google.maps.Marker({
                       position: new google.maps.LatLng(lat,long),
                       icon:'car.png',
                       map: map
                      
                   });
                    }
                });
            }
       

</script>
    <script>
       function changeloginstatus(id,loadingdiv,spanid)
{
  //alert();
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = 'N';
    if(spanValue=='No')
    {  
       status = 'Y';
    }
    $.ajax({type:'POST',url: '<?php echo site_url('Driver/change_login_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       statusfeedbacks(response,spanid,loadingdiv);
     }});
}
function statusfeedbacks(retstatus,spanid,loadingdiv) 
{
    //alert(retstatus);
    if(retstatus=='N')
    {
        $('#'+spanid).removeClass('label-success').addClass('label-warning');
        $('#'+spanid).text("Logged Out");
    }
    else if(retstatus=='Y')
    {

        $('#'+spanid).removeClass('label-warning').addClass('label-success');
        $('#'+spanid).text("Logged In");
    }
     $('#'+loadingdiv).css('display','none');
    
}
</script>
    <script>
  function changeshiftstatus(id,loadingdiv,spanid)
  {
    var answer = confirm ("Are you sure for ending this shift ?")
   if (answer)
    {
       var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = 'ENDED';
    if(spanValue=='No')
    {  
       status = 'STARTED';
    }
    $.ajax({type:'POST',url: '<?php echo site_url('Driver/change_shift_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       shifstatusfeedbacks(response,spanid,loadingdiv);
     }});
    }
    else
    {
  }
  }
  
  
  function shifstatusfeedbacks(retstatus,spanid,loadingdiv) 
{
    //alert(retstatus);
    if(retstatus=='ENDED')
    {
        $('#'+spanid).removeClass('label-success').addClass('label-warning');
        $('#'+spanid).text("Ended");
    }
    else if(retstatus=='STARTED')
    {

        $('#'+spanid).removeClass('label-warning').addClass('label-warning');
        $('#'+spanid).text("Started");
    }
     $('#'+loadingdiv).css('display','none');
    
}
 </script>   