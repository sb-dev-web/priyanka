<?php

	defined('BASEPATH') or exit('No direct script access allowed');
	require APPPATH . '/libraries/REST_Controller.php';

	class Payment extends REST_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> library(array(
				'upload',
				'Braintree_lib',
				'Twilio'
			));
			$this -> load -> library('xmpp', false);
			$this -> load -> helper(array(
				'form',
				'url',
				'string',
				'constants_helper',
				'function_helper',
				'braintree_function'
			));

			$this -> load -> model('Payment_model');
			$this -> load -> model('Bookingapi_model');

		}

		function make_payment()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('make-payment', $input_method);
			$this -> param_validate($input_method, array(
				"user_id",
				"booking_id",
				"billing_amount",
			));
			$billing_amount = $input_method['billing_amount'];
			$tip = @$input_method['tip'];

			$booking_detail = $this -> Payment_model -> check_billing_charge($input_method);
			if (!$booking_detail)
			{
				$this -> response(array(
					"message" => BOOKING_NOTFOUND,
					'status' => 0
				), 200);
			}
			
			$booking_detail = $booking_detail[0];
			$billing_charge = $booking_detail['billing_amount'];
			$booking_status = $booking_detail['status'];
			$input_method['payment_id'] = $booking_detail['id'];
			if ($booking_status == 'FINISH')
			{
				$this -> response(array(
					"message" => ALREADY_PAID,
					'status' => 2
				), 200);
			}
			if ($booking_status == 'TRIP_STARTED')
			{
				$this -> response(array(
					"message" => TRIP_NOT_END,
					'status' => 0
				), 200);
			}
			//Void the auth transaction 
			$type = "Customer Booking Complete";
			VOIDAUTHTRANSACTION($input_method['booking_id'], $type);

			$braintree_user_token = $booking_detail['braintree_token'];
			$discount_amount = $booking_detail['discount_amount'];
			$paid_amount = $billing_charge + $tip;
			//make a paymentMethodNonce
			$paymentMethodNonce = paymentMethodNonce($braintree_user_token);
			//print_r($paymentMethodNonce);
			if ($paymentMethodNonce['status'] == 0)
			{
				$type="booking payment(nonce) fail";
				PayMentFailMail($input_method['booking_id'], $paymentMethodNonce['message'],$type);
				$this -> response(array(
					"message" => $paymentMethodNonce['message'],
					'status' => 0
				), 200);
			}
           //make a nonce transaction and immediately submit it for settlement:
			$payment_data = array(
				"nonce" => $paymentMethodNonce['nonce'],
				"amount" => $paid_amount
			);
			$result = MakePayment($payment_data);
   
			if ($result -> success == '1')
			{
				$transction_id = $result -> transaction -> id;
				$input_method['transaction_id'] = $transction_id;
				$input_method['desc'] = "success";
				$input_method['nonce']=$paymentMethodNonce['nonce'];
				$input_method['status']=$result->transaction->status;
				$input_method['currency']=$result->transaction->currencyIsoCode;
				$make_payment = $this -> Payment_model -> make_payment($input_method);
			}
			else
			{
				$input_method['desc'] = $result -> message;
				$input_method['transaction_id'] = "";
				$input_method['nonce']=$paymentMethodNonce['nonce'];
				$input_method['status']='fail';
				$input_method['currency']="";
				$make_payment = $this -> Payment_model -> make_payment($input_method);
				//send mail to Admin for fail transaction
				$type="booking payment fail";
				PayMentFailMail($input_method['booking_id'], $result -> message);

			}
			if ($make_payment['status'] == 1)
			{

				$filename = recepet_send_pdf($input_method['booking_id']);
				$data = array(
					"filename" => $filename,
					"booking_id" => $input_method['booking_id']
				);
				$this -> Payment_model -> save_invoicepdf_name($data);
				$this -> response(array(
					"message" => PAYMENT_SUCCESS,
					'status' => 1
				), 200);
			}
			else
			{
				$this -> response(array(
					"message" => $make_payment['message'],
					'status' => 0
				), 200);
			}

		}

		function Collect_payment()
		{
			$this -> load -> library('curl');
			//chk the normal user remaining bill (2 days ago)
			$remaining_payment = $this -> Payment_model -> remaining_payment($input_method);
			if ($remaining_payment)
			{
				//echo "<pre>";
				
				foreach ($remaining_payment as $payment_detail)
				{
					extract($payment_detail);
					//call the curl for make_payment api
					$this -> load -> library('curl');
					$url = "http://privatedriverapp.com/app/api/Payment/make_payment";
					$post_data = array(
						"user_id" => $user_id,
						"booking_id" => $booking_id,
						"billing_amount" => $billing_amount
					);
					$output = $this -> curl -> simple_post($url, $post_data);
					//print_r($output);

				}
			}

		}
		
		function ExpireCard()
		{
			//$this -> load -> library('curl');
					
			$expire_card = $this -> Payment_model -> ExpireCard();
			if($expire_card)
			{
				foreach(	$expire_card as $data)
				{
					echo "<br/>".$email=$data['email'];
					UserCardExpireEmail($email);
				}
			}else
				{
					echo "no data";
				}
		}

	}
?>
