<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Cabs</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Cabs</a></li>
      <li class="active">Add Cab</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="save_cab_details" action="" method="post" autocomplete="off">
            <input type="hidden" value="<?=(isset($cab[0]->cab_id))?$cab[0]->cab_id:"";?>"  name="id"  />
              
                 <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="cab_no">Cab Model</label>
                      <input type="type" class="form-control validate[required]" id="cab_model" name="cab_model" value="<?=(isset($cab[0]->cab_model))?$cab[0]->cab_model:"";?>" placeholder="Enter Cab Model">
                  </div>  
                </div>
              </div>
             
              
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="cab_no">Cab License Plate Number</label>
                      <input type="type" class="form-control validate[required]" id="plate_number" name="plate_number" value="<?=(isset($cab[0]->cab_plate_no))?$cab[0]->cab_plate_no:"";?>" placeholder="Enter Cab Plate Number">
                  </div>  
                </div>
              </div>
           
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Cab Class</label>
                    <select class="form-control validate[required]" name="cab_type" id="cab_type">
                      <option value="">Select Type</option>
                      <?php
                        if(sizeof($cab_type))
                        {
                           for($i=0;$i<sizeof($cab_type);$i++)
                           {
                      ?>
                        <option value="<?php echo $cab_type[$i]->type_id;?>" <?php if(isset($cab[0]->cab_type)){if($cab[0]->cab_type==$cab_type[$i]->type_id){ echo "selected";}} ?> ><?php echo $cab_type[$i]->title; ?></option>
                      <?php
                           }
                        }
                      ?>
                      
                    </select>
                  </div>  
                </div>
              </div>
                <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Service area</label>
                   
                   	 <select class="form-control validate[required]" name="service_area" id="service_area">
                      <option value="">Select Service Area</option>
                   	<option value="1" <?php if($cab[0]->service_area=='1'){ ?> selected <?php } ?>>Oslo</option>
                   	<option value="2" <?php if($cab[0]->service_area=='2'){ ?> selected <?php } ?>>Akershus</option>
                   </select>
                   
                  </div>  
                </div>
              </div>
              
              <!-- <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>File Upload</label>
                			    <div class="fileupload fileupload-new" data-provides="fileupload">
                    				<div class="input-append">
                    				<div class="uneditable-input span3">
                    				    <i class="iconfa-file fileupload-exists"></i>
                    				    <span class="fileupload-preview"></span>
                    				</div>
                    				<span class="btn btn-file"><span class="fileupload-new">Select file</span>
                    				<span class="fileupload-exists">Change</span>
                    				<input type="file" id="category_icon" name="category_icon" onchange="uploadCategory('<?php echo site_url().'Uploader/category'; ?>','save_cab_details','category_icon','category_icon_name','original_name','file_loading','fileupload-preview')" /></span>
                    				<?php
                                    if(isset($cab[0]->cab_image))
                                    {
                                    ?>
                                     <img src="<?php  echo base_url()."images/profile/".$cab[0]->cab_image; ?>" width="50" height="35"/>
                                     <?php
                                     }
                                     ?>
                                    <input type="hidden" id="category_icon_name" name="category_icon_name" value="" />
                                    <input type="hidden" id="original_name" name="original_name" value="" />
                    				</div>
                                    <div id="file_loading" style="display: none;">
                                        <img src="<?php echo base_url(); ?>images/loader19.gif" />
                                    </div>
                                    
                			    </div>
                  </div>
                </div>
              </div>-->
              
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>
    
<script type="text/javascript">
//var $ = jQuery.noConflict();
$(document).ready(function(){

    function submit_form(formData,status)
    {
        if(status==true)
        {
            var querystr = $("#save_cab_details").serialize();
            
            
            
            $.ajax({
                url		:	"<?php echo site_url('Cab/submit_cab'); ?>",
                type	:	"POST",
                data	:	querystr,
                //async: false,
               // cache: false,
                //contentType: false,
                //processData: false,
                
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
				   var r = confirm("Cab is added successfully"); 
                   if(r==true) // 
                    {
                      
                    $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl("Cab is added successfully", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                   $('#save_cab_details')[0].reset();
				    window.location='<?php echo site_url('cab'); ?>';
                  }
                  else
                  {
                    $("#loader").hide();
					$('#save_cab_details')[0].reset();
                    //window.location='<?php echo site_url('login/dashboard'); ?>';
                    //alert('failed to save');
                  }
                 }
            });
            return false;
        }
        
    }
            
      
    
  $("#save_cab_details").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});
</script>                 