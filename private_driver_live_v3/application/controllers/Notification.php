<?php

	defined('BASEPATH') or exit('No direct script access allowed');

	class Notification extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url',
				'notification_helper',
				'constants_helper',
				'function_helper'
			));
			$this -> load -> model('Notification_model');
			$this -> load -> library(array('session'));

			$user = $this -> session -> userdata('logged_in');
			if (!$user)
			{
				//redirect('Welcome');
			}

		}

		public function index()
		{

			$this -> load -> view('site-nav/header', TRUE);
			$this -> load -> view('send_notification');
			$this -> load -> view('site-nav/footer');
		}

		/*send push notification function */
		function push_notify()
		{
			error_reporting(1);
			$device_type = $_POST['device_type'];
			$message_text = $_POST['message'];
			$save_notify = $this -> Notification_model -> save_notify($message_text, $device_type);
			$device = $this -> Notification_model -> fetch_all_device();
			if ($device_type == 'ALL')
			{
				if ($device['iphone'])
				{

					foreach ($device['iphone'] as $device_token)

					{
						//$device_tokens[] = $device_token['device_token'];

						$badge = $this -> Notification_model -> GetBadge($device_token['id']);
						//$badge=1;
						$device_tokens[] = array(
							"user_id" => $device_token['id'],
							"badge" => $badge,
							"token" => $device_token['device_token'],
							"username"=>$device_token['name']
						);

					}
					// print_r($device_tokens);
					$otherdata=array("type"=>"Notification");
					send_push($device_tokens, $message_text,$otherdata);
				}
				elseif ($device['andorid'])
				{

					$device_typ = $device[0]['device_type'];

					foreach ($device['andorid'] as $device_token)
					{
						//$registration[] = $device_token['device_token'];
						$badge = $this -> Notification_model -> GetBadge($device_token['id']);
						//$badge=1;
						$registration[] = array(
							"user_id" => $device_token['id'],
							"badge" => $badge,
							"token" => $device_token['device_token'],
							"type"=>"Notification",
						);

					}
					send_andorid_push($message_text, $registration);
				}
				/*save into notofication table */

				echo 1;
			}
			

			$device_typ = $device[0]['device_type'];
			if ($device_type == 'A')
			{
				//echo "Android";
				$device = $this -> Notification_model -> fetch_all_device();
				foreach ($device['andorid'] as $device_token)
					{
						//$registration[] = $device_token['device_token'];
						$badge = $this -> Notification_model -> GetBadge($device_token['id']);
						//$badge=1;
						$registration[] = array(
							"user_id" => $device_token['id'],
							"badge" => $badge,
							"token" => $device_token['device_token'],
							"type"=>"Notification",
						);

					}

				
				send_andorid_push($message_text, $registration);
				echo 1;

			}
			elseif ($device_type == 'I')
			{
               $device = $this -> Notification_model -> fetch_all_device();
				foreach ($device['iphone'] as $device_token)

				{
					//$device_tokens[] = $device_token['device_token'];

					$badge = $this -> Notification_model -> GetBadge($device_token['id']);
					//$badge=1;
					$device_tokens[] = array(
						"user_id" => $device_token['id'],
						"badge" => $badge,
						"token" => $device_token['device_token'],
						"username"=>$device_token['name']
					);

				}
				$otherdata=array("type"=>"Notification");
				send_push($device_tokens, $message_text,$otherdata);
				/*save into notofication table */
				//$save_notify = $this -> Notification_model -> save_notify($message_text,
				// $device_type);
				echo 1;
			}
		}

	}
