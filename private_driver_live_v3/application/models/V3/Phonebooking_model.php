<?php

	class Phonebooking_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		function GetAgency()
		{
			$this -> db -> select("id,name");
			$res = $this -> db -> get_where("user", array("company_id" => 20));
			if ($res -> num_rows() > 0)
			{
				return $res -> result_array();
			}
			else
			{
				return false;
			}
		}

		function GetUser($auth_token)
		{
			$this -> db -> select("*");
			$query = $this -> db -> get_where("user", array(
				"auth_token" => $auth_token,
				"is_active" => "1",
				"is_deleted" => "0"
			));
			if ($query -> num_rows())
			{
				$data = $query -> row_array();
				return $data;
			}
			else
			{
				return false;
			}
		}

		function SaveUser($input_method)
		{
			$name = $input_method['agency_name'];
			$email = $input_method['email'];
			$country_code = $input_method['country_code'];
			$phone = $input_method['phone'];
			$password = rand(111, 999);
			$AuthToken = _generate_key();
			$insert_arr = array(
				"name" => $name,
				"email" => $email,
				"password" => md5($password),
				"phone" => $phone,
				"country_code" => $country_code,
				"login_type" => "W",
				"is_social" => "N",
				"is_active" => "1",
				"is_company" => "Y",
				"is_varify" => "1",
				"createdon" => get_gmt_time(),
				"company_id" => "20",
				"is_emailverify" => "1",
				"mode" => "COMPANY",
				"auth_token" => $AuthToken,
			);

			$this -> db -> trans_begin();
			$insert_user = $this -> db -> insert("user", $insert_arr);

			$user_id = $this -> db -> insert_id();
			$data['user_id'] = $user_id;

			//check if the web user there is no any cc detail
			// if ($data['login_type'] == "W")
			// {
			//
			// //delete the temp user
			// $this -> db -> query("delete from user_temp where id=$temp_user_id");
			// $openfireuser = $this -> MakeOpenfireUser($data);
			// if ($openfireuser)
			// {
			// $this -> db -> trans_commit();
			// $this -> db -> trans_complete();
			// $response = array(
			// "status" => "1",
			// "message" => "Agency Successfully Added",
			// "id"=>$user_id,
			// "name"=>$name,
			// );
			// return json_encode($response);
			// }
			// else
			// {
			// $this -> db -> trans_rollback();
			//
			// $response = array(
			// "status" => "0",
			// "message" => "Server not support"
			// );
			// return json_encode($response);
			// }
			//
			// }
			//Make A Entry In Company User Table
			$insert_data = array(
				"user_id" => $user_id,
				"company_name" => 'Private driver Agency',
				"company_id" => "20",
				"created_on" => get_gmt_time()
			);
			$user_company = $this -> db -> insert("user_company", $insert_data);
			if ($this -> db -> insert_id())
			{
				$data = array(
					"user_id" => $user_id,
					"name" => $name,
					"email" => $email,
					"pass" => $password
				);
				$openfireuser = $this -> MakeOpenfireUser($data);
				if ($openfireuser)
				{
					$this -> db -> trans_commit();
					$this -> db -> trans_complete();
					$response = array(
						"status" => "1",
						"message" => "Agency Successfully Added",
						"id" => $user_id,
						"name" => $name,
					);
					return json_encode($response);
				}
				else
				{
					$this -> db -> trans_rollback();
					$response = array(
						"status" => "0",
						"message" => "Server not support"
					);
					return json_encode($response);
				}
			}
			else
			{
				$response = array(
					"status" => "0",
					"message" => "Server not support"
				);
				return json_encode($response);
			}

		}

		function MakeOpenfireUser($data)
		{
			//print_r($data);
			$user_id = $data['user_id'];
			//define("USER_JID_NAME","user_local-");
			$user_name = USER_JID_NAME . '-' . $user_id;
			//$user_name = 'user_local' . '-' . $user_id;
			$password = $data['pass'];
			//if (@$data['is_social'] == 'N')
			{
				$password = $data['pass'];
				$password = trim($password);
				$is_social = "N";
			}
			// else
			// {
			// $password = "123456";
			// $password = trim($password);
			// $is_social = "Y";
			// }
			$name = $data['name'];
			$email = $data['email'];
			$param = array(
				"username" => $user_name,
				"name" => "$name",
				"password" => "$password",
				"email" => "$email"
			);

			$openfir_result = $this -> openfire -> openfire_action("POST", $param, "users/");
			$openfire_response = json_decode($openfir_result, true);
			//print_R($openfire_response);
			if ($openfire_response['status'] == 0)
			{
				return false;

			}
			else
			{
				return true;
			}
		}

		function Checkemail($email)
		{
			$query = $this -> db -> get_where("user", array("email" => $email,
				//"id!=" => $input_method['user_id']
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		function SaveBooking($input_method)
		{
			$conatct_number = $input_method['contact_number'];
			$conatct_number = str_replace("+", "", $conatct_number);
			$conatct_number = "+" . $conatct_number;
			$insert_array = array(
				"user_id" => $input_method['agency'],
				"pickup_latitude" => $input_method['pickup_latitude'],
				"pickup_longitude" => $input_method['pickup_longitude'],
				"pickup_location" => $input_method['pickup'],
				"destination_latitude" => $input_method['destination_latitude'],
				"destination_longitude" => $input_method['destination_longitude'],
				"destination_location" => $input_method['destination'],
				"booking_time" => $input_method['bookingtime'],
				"cab_type" => $input_method['cab_type'],
				"is_prebooking" => "Y",
				"status" => "CONFIRMED",
				"os_type" => "web",
				"company_id" => $input_method['company_id'],
				"booking_type" => "WEB_PRE_BOOKING",
				"payment_mode" => $input_method['payment_mode'],
				"contact_number" => $conatct_number,
				"comment" => $input_method['comment']
			);

			$insert = $this -> db -> insert("cab_booking", $insert_array);
			if ($booking_id = $this -> db -> insert_id())
			{
				$CBN = "PD" . sprintf("%05d", $booking_id);
				$this -> db -> query("update cab_booking set booking_number='" . $CBN . "' where id=$booking_id");

				$to = $conatct_number;

				$time = date('d-M-Y h:i', strtotime($input_method['bookingtime']));
				$message = ACCEPTSMS;
				$search = array(
					"<TIME>",
					"<ADDRESS>"
				);
				$replace = array(
					"$time",
					"$address"
				);
				$message = str_replace($search, $replace, $message);

				send_sms($phone, $message);
				//$message=BOOKING
				send_sms($to, $message);
				return $booking_id;
				//return true;
			}
			else
			{
				return false;
			}
		}

	}
?>
