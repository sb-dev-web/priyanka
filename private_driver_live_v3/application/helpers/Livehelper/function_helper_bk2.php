<?php

	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	function gettimedetail($origins, $destination,$time="now")
	{
		
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $origins . "&destinations=" . $destination . "&mode=driving&language=en-EN&sensor=false&departure_time=".$time."&key=AIzaSyCVsicLfkurjnPxeejere1RJ5RssjIYkIo";
		$result = file_get_contents($url);
		$json_data = json_decode($result, true);
		//print_r($json_data);
		if ($json_data['status'] == 'OK')
		{
			$distance = $json_data['rows'][0]['elements'][0]['distance']['text'];
			$duration = $json_data['rows'][0]['elements'][0]['duration_in_traffic']['text'];
			$duration_sec = $json_data['rows'][0]['elements'][0]['duration_in_traffic']['value'];
			$distance_m = $json_data['rows'][0]['elements'][0]['distance']['value'];
			
			return array(
				"distance" => $distance,
				"duration" => $duration,
				"duration_sec" => $duration_sec,
				"distance_m" => $distance_m
			);
		}
		else
		{
			return false;
		}

		//print_r($result);
	}

	function getaddress($lat, $lng)
	{
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false';
		$json = @file_get_contents($url);
		$data = json_decode($json);
		$status = $data -> status;
		if ($status == "OK")
			return $data -> results[0] -> formatted_address;
		else
			return false;
	}

	function makejid($user_id, $type)
	{
		//$host_name=$_SERVER['SERVER_NAME'];
		$host_name = "privatedriverapp.com";
		$jid = $type . '-' . $user_id . '@' . $host_name;
		return $jid;

	}

	function send_sms($to, $message)
	{
		$ci = &get_instance();
		$ci -> load -> helper(array('constants'));

		$account_sid = ACCOUNT_SID;
		$auth_token = SMS_AUTH_TOKEN;
		try
		{
			$client = new Services_Twilio($account_sid, $auth_token);
			//$to;
			$data = $client -> account -> messages -> create(array(
				'To' => $to,
				'From' => "+14805682991",
				'Body' => $message,
			));
		}
		catch (exception $e)
		{
			//echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}

	function random_number()
	{
		return (rand(111111, 999999));
	}

	function make_thumb($src, $dest, $desired_width, $ext)
	{
		try
		{

			if ($ext == 'png' || $ext == 'PNG' || $ext == '.png' || $ext == '.PNG')
			{
				$source_image = imagecreatefrompng($src);
			}
			else
			{
				$source_image = imagecreatefromjpeg($src);
			}

			$width = imagesx($source_image);
			$height = imagesy($source_image);

			/* find the "desired height" of this thumbnail, relative to the desired width  */
			$desired_height = floor($height * ($desired_width / $width));

			/* create a new, "virtual" image */
			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);

			/* copy source image at a resized size */
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

			/* create the physical thumbnail image to its destination */

			if ($ext == 'png' || $ext == 'PNG' || $ext == '.png' || $ext == '.PNG')
			{
				imagepng($virtual_image, $dest);
			}
			else
			{
				imagejpeg($virtual_image, $dest);
			}
		}
		catch (exception $e)
		{
			//log
		}

		//return $virtual_image;
	}

	function get_thumb($image)
	{
		$image = explode('.', $image);
		$thumb_img = $image[0] . '_thumb.' . $image[1];
		return $thumb_img;
	}

	function get_gmt_time()
	{
		return gmdate("Y-m-d H:i:s");
	}

	/* function demo */

	function recipet_send($bookingid)
	{

		$ci = &get_instance();
		$ci -> load -> library('email');
		$ci -> load -> library('Pdf');
		$ci -> load -> database();
		/* $sql = "select
		 * cu.title,card.credit_card_no,c.driver_id,d.first_name,d.last_name,d.profile_pic,
		 * p.km_driven,p.km_charge,p.journey_time,p.waiting_time,p.waiting_charge,p.billing_amount,u.name,u.email,u.company_name,c.booking_time,c.booking_number,c.pickup_location,
		 * c.destination_location, f.min_charges
		 from payment_detail p,user as u,cab_booking as c,cab_type as cu,card_info as
		card,driver as d,fare_info as f  where c.id='$bookingid' and
		p.booking_id='$bookingid' and u.id=c.user_id and c.cab_type=cu.type_id and
		card.customer_id=c.user_id and c.driver_id=d.driver_id and
		cu.type_id=f.cab_type";
		 */
		$sql = "select cu.title,card.credit_card_no,b.driver_id,d.first_name,d.last_name,d.profile_pic,p.tip,p.id, p.km_driven,p.km_charge,
            p.journey_time,p.waiting_time,p.waiting_charge,p.billing_amount,u.name,u.email,u.company_name,b.booking_time,
            b.booking_number,b.pickup_location, b.destination_location, f.min_charges,p.user_paid 
            from payment_detail p
            join cab_booking b on b.id=p.booking_id 
            join user u on u.id=b.user_id 
            join driver d on d.driver_id=b.driver_id 
            join fare_info  f on f.cab_type=b.cab_type and type='NORMAL' 
            join cab_type cu on cu.type_id=b.cab_type 
            join card_info card on card.customer_id=u.id where b.id='$bookingid'";
		$data = $ci -> db -> query($sql);

		if ($data -> num_rows() > 0)
		{
			$get_driver_status = $ci -> db -> query("select * from driver_status where booking_id='$bookingid'");
			// $get_status=$get_driver_status->row();
			$get_data = $get_driver_status -> result();
			foreach ($get_data as $time_status)
			{
				$c_status = $time_status -> status;
				if ($c_status == 'TRIP_STARTED')
				{
					$s_time = gmt_to_norway($time_status -> createdon);
					$start = date("H:i", strtotime($s_time));

				}
				elseif ($c_status == 'TRIP_ENDED')
				{
					$e_time = gmt_to_norway($time_status -> createdon);
					$end = date("H:i", strtotime($e_time));
				}
				else
				{

					$s_time = '';

				}
			}
			$date = date('d.m.Y');
			$booking_data = $data -> result_array();
			$basic_price = number_format($booking_data[0]['min_charges'], 2, ',', '');
			$km_charge = number_format($booking_data[0]['km_charge'], 2, ',', '');
			$driver_name = $booking_data[0]['first_name'] . ' ' . $booking_data[0]['last_name'];
			$profile_pic = $booking_data[0]['profile_pic'];
			$car = $booking_data[0]['title'];
			$card = $booking_data[0]['credit_card_no'];
			$credit_card = credit_card_number($card);
			$cbn = $booking_data[0]['booking_number'];
			$km = number_format($booking_data[0]['km_driven'], 2, ',', '');
			$j_time = $booking_data[0]['journey_time'];
			//$w_time = $booking_data[0]['waiting_time'];
			$w_charge = number_format($booking_data[0]['waiting_charge'], 2, ',', '');
			$billing_amount = number_format($booking_data[0]['billing_amount'], 2, ',', '');
			$name = $booking_data[0]['name'];
			$email = $booking_data[0]['email'];
			$booking_time = $booking_data[0]['booking_time'];
			$company_name = $booking_data[0]['company_name'];
			$pickup_location = $booking_data[0]['pickup_location'];
			$destination_location = $booking_data[0]['destination_location'];
			$service_tax = 8 / 100 * ($booking_data[0]['billing_amount']);
			$tax = number_format($service_tax, 2, ',', '');
			$tip = number_format($booking_data[0]['tip'], 2, ',', '');
			$user_paid = number_format($booking_data[0]['user_paid'], 2, ',', '');
			if ($company_name != "")
			{
				$cname = $booking_data[0]['company_name'];
				$title = "company_name";
			}
			else
			{
				$cname = "";
				$title = "";
			}
			$header = ' <html>
<head>';

			$body = '<html><body><div id="bill" name="bill" style="width:400px;height:660px;  border-style: solid;border-color:#8E6540;border-width:6px;">    

<center><img src="http://privatedriverapp.com/app/logo50.png" style="margin-top:10px;">


<br><label<b><Font size="5%">Private Driver A/s</font></b></label><br>
<label>Org no:&nbsp;<b>913341856</b></label><br>
<label><b>KVITTERING</b></label>
<br>

</center>
<br>
<label style="margin-left:10px;">Dato:&nbsp;</label>' . $date . ' <br>   
<label style="margin-left:10px;">Kvitteringsnummer:&nbsp;</label>' . $cbn . '<br><br>
<div style="width:100%;;padding:10px;"><div style="width:30%;float:left;">Fra:</div><div style="width:62%;float:right;margin-right:31px;">' . $pickup_location . '</div></div><br><br>
<div style="margin-left:10px;width:100%;"><div style="width:30%;float:left;">till:</div><div style="width:62%;float:right;margin-right:31px;">' . $destination_location . '</div></div><br><br>
<div style="margin-left:10px;"><div style="align:center;margin-right:20px;">Start:<span style="margin-right:28px;float:right;margin-top:2px;">' . $start . '</span></div></div><br>
<div style="margin-left:10px;"><div style="align:center;margin-right:20px;">Slut:<span style="margin-left:283px;margin-top:2px">' . $end . '</span></div></div><br><br>
<div style="margin-left:10px;"><div style="align:center;margin-right:20px;"><b>PRIS-SPESIFIKASION:</b><br><br>
<div style="align:center;margin-right:20px;">Grunnpris:<span style="margin-left:67px;">NOK</span><span style="float:right;margin-top:2px">' . $basic_price . '</span></div><br>
<div style="align:center;margin-right:20px;">Km:<span style="margin-left:102px;">NOK</span><span style="float:right;margin-top:2px;">' . $km_charge . '</span></div><br>
<div style="align:center;margin-right:20px;">Tid:<span style="margin-left:103px;">NOK</span><span style="float:right;margin-top:2px;">' . $w_charge . '</span></div><br>
<div style="align:center;margin-right:20px;">Tips:<span style="margin-left:96px;">NOK</span><span style="float:right;margin-top:2px;">' . $tip . '</span></div><br>
<div style="align:center;margin-right:20px;">Total:<span style="margin-left:92px;">NOK</span><span style="float:right;margin-top:2px;">' . $user_paid . '</span></div><br>
<div style="align:center;margin-right:20px;">Herav mva 10%:<span style="margin-left:37px;">NOK</span><span style="float:right;">' . $tax . '</span></div><br>
<div style="align:center;margin-right:20px;">KM:<span style="float:right;margin-top:2px;">' . $km . '</span></div><br>
<div style="align:center;margin-right:20px;">Tid:<span style="float:right;margin-top:2px;">' . $j_time . '</span></div><br>
<div style="align:center;margin-right:20px;">Beralingsmate:<span style="float:right;margin-top:2px;">' . $credit_card . '</span></div><br>
<div style="align:center;margin-right:20px;">Takk for at du valgte Private Driver</div><br>
<div style="align:center;margin-right:20px;">www.privatedriver.no</div></body></html>';
			$file = $name . '_' . $cbn;
			$filename = $file . '.pdf';
			/*sendmail($header,$body,$filename,$email);*/
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';
			$ci -> email -> initialize($config);
			$ci -> email -> from(ADMINMAIL_FROM, 'Private Taxi Bill');
			$ci -> email -> to($email);
			$ci -> email -> cc("priyanka@foxinfosoft.com");
			$ci -> email -> subject('bill');
			$ci -> email -> message($body);
			// $ci->email->attach($filepaths);

			//$counter ++;
			if ($ci -> email -> send())
			{
				//echo '1';
			}
			else
			{
				//echo $filename;
				//show_error($ci->email->print_debugger());

			}

		}
	}

	function recepet_send_pdf($bookingid, $type = 'normal', $email_test = "")
	{

		$ci = &get_instance();
		$ci -> load -> library('email');
		$ci -> load -> library('Pdf');
		$ci -> load -> database();
		
		$sql = "select cu.title,card.credit_card_no,b.driver_id,d.first_name,d.last_name,d.profile_pic,p.tip,p.id, p.km_driven,p.km_charge,
            p.journey_time,p.waiting_time,p.waiting_charge,p.billing_amount,u.name,u.email,u.company_name,b.booking_time,u.is_company,cp.email as company_email,
            b.booking_number,b.pickup_location, b.destination_location, p.basic_fare_charge,p.charges_per_km,p.waiting_charges_per_min,p.user_paid
            from payment_detail p
            join cab_booking b on b.id=p.booking_id 
            join user u on u.id=b.user_id 
            join driver d on d.driver_id=b.driver_id 
          
            join cab_type cu on cu.type_id=b.cab_type 
            left  join card_info card on card.customer_id=u.id
            left join company cp on  cp.id=u.company_id where b.id=$bookingid";
		$data = $ci -> db -> query($sql);
		//echo $ci->db->last_query();
		if ($data -> num_rows() > 0)
		{
			$get_driver_status = $ci -> db -> query("select * from driver_status where booking_id='$bookingid'");
			// $get_status=$get_driver_status->row();
			$get_data = $get_driver_status -> result();
			foreach ($get_data as $time_status)
			{
				$c_status = $time_status -> status;
				if ($c_status == 'TRIP_STARTED')
				{
					$s_time = gmt_to_norway($time_status -> createdon);
					$start = date("H:i", strtotime($s_time));

				}
				elseif ($c_status == 'TRIP_ENDED')
				{
					$e_time = gmt_to_norway($time_status -> createdon);
					$end = date("H:i", strtotime($e_time));
				}
				else
				{

					$s_time = '';

				}
			}
			$date = date('d.m.Y');
			$booking_data = $data -> result_array();
			$basic_price = number_format($booking_data[0]['basic_fare_charge'], 2, ',', '');
			$km_charge = number_format($booking_data[0]['km_charge'], 2, ',', '');
			$driver_name = $booking_data[0]['first_name'] . ' ' . $booking_data[0]['last_name'];
			$profile_pic = $booking_data[0]['profile_pic'];
			$car = $booking_data[0]['title'];
			$card = $booking_data[0]['credit_card_no'];
			$credit_card = credit_card_number($card);
			$cbn = $booking_data[0]['booking_number'];
			$km = number_format($booking_data[0]['km_driven'], 2, ',', '');
			$j_time = $booking_data[0]['journey_time'];
			//$w_time = $booking_data[0]['waiting_time'];
			$w_charge = number_format($booking_data[0]['waiting_charge'], 2, ',', '');
			$billing_amount = number_format($booking_data[0]['billing_amount'], 2, ',', '');
			$name = $booking_data[0]['name'];
			$email = $booking_data[0]['email'];
			$booking_time = $booking_data[0]['booking_time'];
			$company_name = $booking_data[0]['company_name'];
			$pickup_location = $booking_data[0]['pickup_location'];
			$destination_location = $booking_data[0]['destination_location'];
			$service_tax_free = 100 / VAT * ($booking_data[0]['billing_amount']);
			$service_tax = $booking_data[0][billing_amount] - $service_tax_free;
			$tax = number_format($service_tax, 2, ',', '');
			$tip = number_format($booking_data[0]['tip'], 2, ',', '');
			$user_paid = number_format($booking_data[0]['user_paid'], 2, ',', '');
			$is_company = $booking_data[0]['is_company'];

			$per_km_charges = number_format($booking_data[0]['charges_per_km'], 2, ',', '');

			$journey_time_change = number_format($booking_data[0]['waiting_charges_per_min'], 2, ',', '');
			$msg_replace = array(
				"Sec",
				"Min",
				"Hr"
			);
			$detail = array(
				'Sek',
				'Minutt',
				'Hr'
			);
			$j_time = str_replace($msg_replace, $detail, format_time($j_time));
			if(!$j_time)
			{
				$j_time="0 Sek";
			}
			if ($company_name != "")
			{
				$cname = $booking_data[0]['company_name'];
				$title = "company_name";
				$username = $cname;
			}
			else
			{
				$cname = "";
				$title = "";
				$username = $booking_data[0]['name'];
			}
			if ($is_company == 'Y')
			{
				$cname = $booking_data[0]['company_name'];
				$title = "company_name";
				$username = $booking_data[0]['name'];
				$credit_card = "--";
				$user_paid = $billing_amount;
				//$email=$booking_data[0]['company_email'];

			}
			else
			{
				$cname = "";
				$title = "";
				$username = $booking_data[0]['name'];

			}

			if ($type != "normal")
			{
				$basic_price = "100.00";
				$start = "--";
				$end = "--";
				$tax = "10.00";
			}

			$header = '<html>
	<body>
		<div style="border:8px solid ; border-color:  #8E6540 ; width: 700px" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold; color:#000"  >
			<table align="center" width="100%" cellpadding="3" cellspacing="3"style="font-family:Arial, Helvetica, sans-serif; font-size:25px; font-weight:bold; color:#999999"  >
				<tr>
					<td align="center"><img src="http://privatedriverapp.com/app/logo100.jpeg" align="center"/> </td></tr>
					<tr><td align="center">Private Driver A/S</td></tr>
					<tr><td style="font-size:20px;" align="center">Org no: <b>913341856</b></td></tr>
					<tr><td style="font-size:20px;" align="center"><b>KVITTERING</b></td></tr>
					</table>
					<br />';
			$html = '<table align="center" border="0px" width="90%" cellpadding="3" cellspacing="3" style="font-family:Arial, Helvetica, sans-serif; font-size:17px; color:#000">
					<tr>
					<td align="left">Dato:</td>
					<td colspan="2"  align="left">' . $date . '</td>
					</tr><tr>
						<td align="left">Kvitteringsnummer:</td>
					<td colspan="2" align="left" >' . $cbn . '</td>
					</tr>
					<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
					</tr>
					<tr>
					<td align="left">Fra:</td>
					<td colspan="2" align="left">' . $pickup_location . '</td>
					</tr>
					<tr>
					<td align="left">Til:</td>
					<td colspan="2" align="left">' . $destination_location . '</td>
					</tr>
					<tr>
					<td align="left">Start:</td>
					<td colspan="2" align="left">' . $start . '</td>
					</tr>
					<tr>
					<td align="left">Slutt:</td>
					<td colspan="2" align="left">' . $end . '</td>
					</tr>
					<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
					</tr>
					<tr><td colspan="2" align="left" ><b>PRIS-SPESIFIKASION:</b></td></tr>
					<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
					</tr>
					<tr>
					<td align="left">Grunnpris:</td>
					<td align="left">NOK</td>
					<td align="right">' . $basic_price . '</td>
					</tr>
					<tr>
					<td align="left">Km á kr (' . $per_km_charges . '):</td>
					<td align="left">NOK</td>
					<td align="right">' . $km_charge . '</td>
					</tr>
					<tr>
					<td align="left">Tid  á kr (' . $journey_time_change . ')per minutt:</td>
					<td align="left">NOK</td>
					<td align="right">' . $w_charge . '</td>
					</tr>
					<tr>
					<td align="left">Subtotal:</td>
					<td align="left">NOK</td>
					<td align="right">' . $billing_amount . '</td>
					</tr>
					<tr>
					<td align="left">Herav mva 10%:</td>
					<td align="left">NOK</td>
					<td align="right">' . $tax . '</td>
					</tr>
					<tr>
					<td align="left">Tips:</td>
					<td align="left">NOK</td>
					<td align="right">' . $tip . '</td>
					</tr>
					<tr>
					<td align="left">Total:</td>
					<td align="left">NOK</td>
					<td align="right">' . $user_paid . '</td>
					</tr>
					
					<tr>
					<td colspan="2" align="left" >KM:</td>
					
					<td align="right">' . $km . '</td>

					</tr>
					<tr>
					<td  colspan="2"  align="left">Tid:</td>
					
					<td align="right">' . $j_time . '</td>

					</tr>
					<tr style="height:5px;"> <td style="font-size:xx-small;">&nbsp;</td>
				</tr>
				<tr>
					<td align="left">Betalingsmåte:</td>
					<td colspan="2" align="left">' . $credit_card . '</td>
				</tr>
				<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
				</tr>
				<tr>
					<td align="left" colspan="3">Takk for at du valgte Private Driver</td>
				</tr>
				<tr>
					<td align="left" colspan="2">www.privatedriver.no</td>
				</tr>

			</table>
		</div>
	</body>
</html>';
			$file = 'Invoice' . '_' . $cbn;
			$filename = $file . '.pdf';
			$body = $header . $html;
			$filename = sendmail($header, $html, $filename);
			$text="<p>Hei.</p><p>Vedlagt finner du kvittering for en av deres ansattes tur med oss.</p>";
			//$body = $text . $header . $html;
			/*sendmail($header,$body,$filename,$email);*/
			$body=$text;
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';
			$config['priority'] = 1;
			$config['charset'] = 'utf-8';
			$config['smtp_host'] = "vps.privatedriverapp.com";
			//465//vps.privatedriverapp.com
			$config['smtp_user'] = "post@privatedriverapp.com";
			$config['smtp_pass'] = "T@xi@2015";
			$config['smtp_port'] = "465";

			//unset($ci->email);
			$ci -> email -> clear();
			$ci -> email -> initialize($config);
			$ci -> email -> from(ADMINMAIL_FROM, 'Private Driver');
			$ci -> email -> to($email);
			//$ci->email->cc("priyanka@foxinfosoft.com");
			$ci -> email -> bcc($email_test);

			$ci -> email -> subject('Kvittering Private Driver A/S');
			$ci -> email -> reply_to('post@privatedriver.no', 'Private Driver');
			$messageid = md5(uniqid(time())) . "@privatedriverapp.com";
			$ci -> email -> set_header('Message-ID', $messageid);
			$ci -> email -> message($body);
			$ci -> email -> attach(INVOICE_PATH.$filename);

			//$counter ++;
			if ($ci -> email -> send())
			{
				//echo '1';
				$ci -> email -> clear();
				//	unset($ci->email);
			}
			else
			{
				//echo $filename;
				//show_error($ci->email->print_debugger());

			}
              return $filename;
		}
	}

	function recepet_send_pdf_company($bookingid, $type = 'normal', $email_test = "")
	{

		$ci = &get_instance();
		$ci -> load -> library('email');
		$ci -> load -> library('Pdf');
		$ci -> load -> database();
	////waiting_charges_per_min,charges_per_km,basic_fare_charge
		$sql = "select cu.title,card.credit_card_no,b.driver_id,d.first_name,d.last_name,d.profile_pic,p.tip,p.id, p.km_driven,p.km_charge,
            p.journey_time,p.waiting_time,p.waiting_charge,p.billing_amount,u.name,u.email,u.company_name,b.booking_time,u.is_company,cp.email as company_email,
            b.booking_number,b.pickup_location, b.destination_location, p.basic_fare_charge,p.charges_per_km,p.waiting_charges_per_min,p.user_paid
            from payment_detail p
            join cab_booking b on b.id=p.booking_id 
            join user u on u.id=b.user_id 
            join driver d on d.driver_id=b.driver_id 
         
            join cab_type cu on cu.type_id=b.cab_type 
            left  join card_info card on card.customer_id=u.id
            left join company cp on  cp.id=u.company_id where b.id=$bookingid";
		$data = $ci -> db -> query($sql);
		//echo $ci->db->last_query();
		if ($data -> num_rows() > 0)
		{
			$get_driver_status = $ci -> db -> query("select * from driver_status where booking_id='$bookingid'");
			// $get_status=$get_driver_status->row();
			$get_data = $get_driver_status -> result();
			foreach ($get_data as $time_status)
			{
				$c_status = $time_status -> status;
				if ($c_status == 'TRIP_STARTED')
				{
					$s_time = gmt_to_norway($time_status -> createdon);
					$start = date("H:i", strtotime($s_time));

				}
				elseif ($c_status == 'TRIP_ENDED')
				{
					$e_time = gmt_to_norway($time_status -> createdon);
					$end = date("H:i", strtotime($e_time));
				}
				else
				{

					$s_time = '';

				}
			}
			$date = date('d.m.Y');
			$booking_data = $data -> result_array();
			$basic_price = number_format($booking_data[0]['basic_fare_charge'], 2, ',', '');
			$km_charge = number_format($booking_data[0]['km_charge'], 2, ',', '');
			$driver_name = $booking_data[0]['first_name'] . ' ' . $booking_data[0]['last_name'];
			$profile_pic = $booking_data[0]['profile_pic'];
			$car = $booking_data[0]['title'];
			$card = $booking_data[0]['credit_card_no'];
			$credit_card = credit_card_number($card);
			$cbn = $booking_data[0]['booking_number'];
			$km = number_format($booking_data[0]['km_driven'], 2, ',', '');
			$j_time = $booking_data[0]['journey_time'];
			//$w_time = $booking_data[0]['waiting_time'];
			$w_charge = number_format($booking_data[0]['waiting_charge'], 2, ',', '');
			$billing_amount = number_format($booking_data[0]['billing_amount'], 2, ',', '');
			$name = $booking_data[0]['name'];
			$email = $booking_data[0]['email'];
			$booking_time = $booking_data[0]['booking_time'];
			$company_name = $booking_data[0]['company_name'];
			$pickup_location = $booking_data[0]['pickup_location'];
			$destination_location = $booking_data[0]['destination_location'];
			$service_tax_free = 100 / VAT * ($booking_data[0]['billing_amount']);
			$service_tax = $booking_data[0][billing_amount] - $service_tax_free;
			$tax = number_format($service_tax, 2, ',', '');
			$tip = number_format($booking_data[0]['tip'], 2, ',', '');
			$user_paid = number_format($booking_data[0]['user_paid'], 2, ',', '');
			$is_company = $booking_data[0]['is_company'];

			$per_km_charges = number_format($booking_data[0]['charges_per_km'], 2, ',', '');

			$journey_time_change = number_format($booking_data[0]['waiting_charges_per_min'], 2, ',', '');
			$msg_replace = array(
				"Sec",
				"Min",
				"Hr"
			);
			$detail = array(
				'Sek',
				'Minutt',
				'Hr'
			);
			$j_time = str_replace($msg_replace, $detail, format_time($j_time));
			if(!$j_time)
			{
				$j_time="0 Sek";
			}
			if ($company_name != "")
			{
				$cname = $booking_data[0]['company_name'];
				$title = "company_name";
				$username = $cname;
			}
			else
			{
				$cname = "";
				$title = "";
				$username = $booking_data[0]['name'];
			}
			if ($is_company == 'Y')
			{
				$cname = $booking_data[0]['company_name'];
				$title = "company_name";
				$username = $booking_data[0]['name'];
				$company_email = $booking_data[0]['company_email'];
				$text = "<p>Hei.</p><p>Vedlagt finner du kvittering for en av deres ansattes tur med oss.</p>
			";
			}
			else
			{
				$cname = "";
				$title = "";
				$username = $booking_data[0]['name'];

			}

			if ($type != "normal")
			{
				$basic_price = "100.00";
				$start = "--";
				$end = "--";
				$tax = "10.00";
			}

			$header = '<html>
	<body>
		<div style="border:8px solid ; border-color:  #8E6540 ; width: 700px" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold; color:#000"  >
		
			<table align="center" width="100%" cellpadding="3" cellspacing="3"style="font-family:Arial, Helvetica, sans-serif; font-size:25px; font-weight:bold; color:#999999"  >
				<tr>
					<td align="center"><img src="http://privatedriverapp.com/app/logo100.jpeg" align="center"/> </td>
						
					</tr>
					<tr><td align="center" colspan="2">Private Driver A/S</td>
				
					</tr>
					<tr><td style="font-size:20px;" align="center" >Org no: <b>913341856</b></td>
					
					</tr>
					<tr><td style="font-size:20px;" align="center" ><b>KVITTERING</b></td></tr>
					</table>
					
							
					<br />';
			$html = '<table align="center" border="0px" width="90%" cellpadding="3" cellspacing="3" style="font-family:Arial, Helvetica, sans-serif; font-size:17px; color:#000">
					<tr>
					<td align="left">Dato:</td>
					<td align="left">' . $date . '</td>
					<td><img src="http://privatedriverapp.com/app/unpaid-stamp.png"/></td>
					</tr><tr>
						<td align="left">Kvitteringsnummer:</td>
					<td colspan="2" align="left" >' . $cbn . '</td>
					</tr>
				    <tr>
					<td align="left">Navn:</td>
					<td colspan="2" align="left">' . $username . '</td>
					</tr>
					<tr>
					<td align="left">Fra:</td>
					<td colspan="2" align="left">' . $pickup_location . '</td>
					</tr>
					<tr>
					<td align="left">Til:</td>
					<td colspan="2" align="left">' . $destination_location . '</td>
					</tr>
					<tr>
					<td align="left">Start:</td>
					<td colspan="2" align="left">' . $start . '</td>
					</tr>
					<tr>
					<td align="left">Slutt:</td>
					<td colspan="2" align="left">' . $end . '</td>
					</tr>
					<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
					</tr>
					<tr><td colspan="2" align="left" ><b>PRIS-SPESIFIKASION:</b></td></tr>
					<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
					</tr>
					<tr>
					<td align="left">Grunnpris:</td>
					<td align="left">NOK</td>
					<td align="right">' . $basic_price . '</td>
					</tr>
					<tr>
					<td align="left">Km á kr (' . $per_km_charges . '):</td>
					<td align="left">NOK</td>
					<td align="right">' . $km_charge . '</td>
					</tr>
					<tr>
					<td align="left">Tid  á kr (' . $journey_time_change . ')per minutt:</td>
					<td align="left">NOK</td>
					<td align="right">' . $w_charge . '</td>
					</tr>
					<tr>
					<td align="left">Subtotal:</td>
					<td align="left">NOK</td>
					<td align="right">' . $billing_amount . '</td>
					</tr>
					<tr>
					<td align="left">Herav mva 10%:</td>
					<td align="left">NOK</td>
					<td align="right">' . $tax . '</td>
					</tr>
					<tr>
					<td align="left">Tips:</td>
					<td align="left">NOK</td>
					<td align="right">' . $tip . '</td>
					</tr>
					<tr>
					<td align="left">Total:</td>
					<td align="left">NOK</td>
					<td align="right">' . $billing_amount . '</td>
					</tr>
					
					<tr>
					<td colspan="2" align="left" >KM:</td>
					
					<td align="right">' . $km . '</td>

					</tr>
					<tr>
					<td  colspan="2"  align="left">Tid:</td>
					
					<td align="right">' . $j_time . '</td>

					</tr>
					<tr style="height:5px;"> <td style="font-size:xx-small;">&nbsp;</td>
				</tr>
				
				<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
				</tr>
				<tr>
					<td align="left" colspan="3">Takk for at du valgte Private Driver</td>
				</tr>
				<tr>
					<td align="left" colspan="2">www.privatedriver.no</td>
				</tr>

			</table>
		</div>
	</body>
</html>';
			$footer = "<p>Med vennlig hilsen</p>";
			$file = 'Invoice_company' . '_' . $cbn;
			$filename = $file . '.pdf';
			//$body = $header . $html;
			//$body=$text;
			$filename = sendmail($header, $html, $filename);
			//$body = $text . $header . $html . $footer;
			$body=$text;
			/*sendmail($header,$body,$filename,$email);*/
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';
			$config['priority'] = 1;
			$config['charset'] = 'utf-8';
			$config['smtp_host'] = "vps.privatedriverapp.com";
			//465//vps.privatedriverapp.com
			$config['smtp_user'] = "post@privatedriverapp.com";
			$config['smtp_pass'] = "T@xi@2015";
			$config['smtp_port'] = "465";

			// unset($ci->email);
			$ci -> email -> clear();
			$ci -> email -> initialize($config);
			$ci -> email -> from(ADMINMAIL_FROM, 'Private Driver');
			$ci -> email -> to($company_email);
			$ci->email->cc("priyanka@foxinfosoft.com");
			// $ci->email->bcc("priyanka@foxinfosoft.com");

			$ci -> email -> subject('Kvittering Private Driver A/S');
			$ci -> email -> reply_to('post@privatedriver.no', 'Private Driver');
			$messageid = md5(uniqid(time())) . "@privatedriverapp.com";
			$ci -> email -> set_header('Message-ID', $messageid);
			$ci -> email -> message($body);
			$ci -> email -> attach(INVOICE_PATH.$filename);

			//$counter ++;
			if ($ci -> email -> send())
			{
				//echo '1';
				$ci -> email -> clear();
				//unset($ci->email);
			}
			else
			{
				//echo $filename;
				//show_error($ci->email->print_debugger());

			}
             return $filename;
		}
	}

	/* send mail after trip end */
	function sendmail($header, $html, $filename)
	{
		$ci = &get_instance();
		$ci -> load -> library('Pdf');
		$ci->load->library('S3_lib');
		$ci->load->helper('constants_helper');
		$html = str_replace("'", '"', $html);
		$pdf = new Pdf($header);
		$pdf -> SetCreator(PDF_CREATOR);
		$pdf -> SetAuthor('Billing Department');
		$pdf -> SetTitle('Private Driver Taxi');
		$pdf -> SetSubject('');
		$pdf -> SetKeywords('');

		// set default monospaced font
		$pdf -> SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins PDF_MARGIN_TOP
		$pdf -> SetMargins(10.0, 85.0, 10.0);
		$pdf -> SetHeaderMargin(PDF_MARGIN_HEADER);

		//set auto page breaks
		$pdf -> SetAutoPageBreak(true, PDF_MARGIN_FOOTER);

		//set image scale factor
		$pdf -> setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set font
		$pdf -> SetFont('helvetica', '', 9);

		// add a page
		$pdf -> AddPage();
		//echo $html;
		$pdf -> writeHTML($html, true, false, true, false, "");

		//Close and output PDF document
		$pdf -> Output('pdf_download/' . $filename, "F");
		//$pdf->Output($filename, "F");
		$filepath = 'pdf_download/';
      
		 $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
        $aws_upload = $s3->putObjectFile($filepath . $filename, AWS_BUCKET, 'invoice/'.$filename,S3::ACL_PUBLIC_READ);
		if($aws_upload)
		{
			unlink('pdf_download/'.$filename);
			return $filename; 
		}else
			{
				 return $filename;
			}
          
		exit ;
	}

	/* function for advance booking email to customer */

	function advance_booking_email($booking_id)
	{
		$ci = &get_instance();
		$ci -> load -> library('email');
		$ci -> load -> database();
		$sql = "select a.booking_number,a.pickup_location,a.booking_time,a.createdon,a.destination_location,c.title,f.description,f.charges_per_km,f.wait_time_charges,u.name,u.email from cab_booking as a,fare_info as f,cab_type as c,user as u where a.id='$booking_id' and f.cab_type=a.cab_type and c.type_id=a.cab_type and a.user_id=u.id";
		$data = $ci -> db -> query($sql);
		if ($data -> num_rows() > 0)
		{
			$advance_booking_data = $data -> result_array();
			$b_no = $advance_booking_data[0]['booking_number'];
			$pickup_location = $advance_booking_data[0]['pickup_location'];
			$destination_location = $advance_booking_data[0]['destination_location'];
			$cab_title = $advance_booking_data[0]['title'];
			$description = $advance_booking_data[0]['description'];
			$charges_per_km = $advance_booking_data[0]['charges_per_km'];
			$wait_time_charges = $advance_booking_data[0]['wait_time_charges'];
			$name = $advance_booking_data[0]['name'];
			$p_time = $advance_booking_data[0]['booking_time'];
			$b_time = $advance_booking_data[0]['createdon'];
			$email = $advance_booking_data[0]['email'];
			$body = '<html>
<body style="font-family:\'Calibri\'; font-size:16px;">
<div id="main-div"style="background-color:#D7DF01; width:45%; height:130%">
 <br>
 <center>
 <div id ="inline" style="background-color:white; width:90%;height:90%; padding-top:15px;padding-left:24px;padding-bottom:24px;padding-top:24px; broder:1px solid black; border-radius: 25px;">

 <p align="left">Booking NO:- <b>' . $b_no . '</b>
<img src= "http://privatedriverapp.com/app/logo500.png" height="38px;" width="45px;" style="margin-left:255px;"></p>
<br>
<br>

<p align="left">Hi ' . $name . ',</p>
<p align="left">Thank you for using PrivateCabs! Your Cab booking is <br>confirmed.</p>
<p align="left"><small>BOOKING DETAILS </small></p>
<hr style="width:512px;">
<font style="font-family:Times New Roman;font-size:10px;">
<table border="0"  cellspacing="12" style="margin-left:40px;">
 
 <tr>
  <td>Booking time </td><td>' . $b_time . '</td></tr>
  <tr><td>Pickup time</td><td>' . $p_time . '</td></tr>
  <tr><td>Pickup Address</td><td>' . $pickup_location . '</td></tr>
  <tr><td>Destination Address</td><td>' . $destination_location . '</td></tr>
   <tr><td>Cab Type</td><td>' . $title . '</td></tr>
  
</table>
</font>
<p align="left"><small>FARE DETAILS </small></p>
  <hr style="width:512px;">
<font style="font-family:Times New Roman;font-size:12px;">
  <table border="0"  cellspacing="12" style="margin-left:-25px;">
 <tr>
  <td>Basic Charge</td><td>' . $description . '</td></tr>
  <tr><td>Per Km Charges</td><td>' . $wait_time_charges . '</td></tr>
  
</table>
</font>
<br>
<br>
<p align="left">*Please refer your Booking No for all communication about this booking. </p>
</center>
 </div>
</body>
';

			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';
			$ci -> email -> initialize($config);
			$ci -> email -> from(ADMINMAIL_FROM, 'pd');
			$ci -> email -> to($email);
			$ci -> email -> subject('Confirm Advance Booking');
			$ci -> email -> message($body);

			//$counter ++;
			if ($ci -> email -> send())
			{
				// echo '1';
			}
			else
			{
				//echo $filename;
				//show_error($ci->email->print_debugger());

			}

		}
		else
		{
			die ;
		}
	}

	/* booking confrimation email */
	function booking_confirm($booking_id)
	{
		$ci = &get_instance();
		$ci -> load -> library('email');
		$ci -> load -> database();
		$sql = "select d.first_name,d.last_name,d.contact, a.booking_number,a.pickup_location,a.booking_time,a.createdon,a.destination_location,a.plate_number,c.title,f.description,f.charges_per_km,f.wait_time_charges,u.name,u.email from cab_booking as a,fare_info as f,cab_type as c,user as u,driver as d where a.id='$booking_id' and f.cab_type=a.cab_type and c.type_id=a.cab_type and a.user_id=u.id and d.driver_id=a.driver_id";
		$data = $ci -> db -> query($sql);
		if ($data -> num_rows() > 0)
		{
			$booking_confirmation = $data -> result_array();
			$b_no = $booking_confirmation[0]['booking_number'];
			$pickup_location = $booking_confirmation[0]['pickup_location'];
			$destination_location = $booking_confirmation[0]['destination_location'];
			$cab_title = $booking_confirmation[0]['title'];
			$description = $booking_confirmation[0]['description'];
			$charges_per_km = $booking_confirmation[0]['charges_per_km'];
			$wait_time_charges = $booking_confirmation[0]['wait_time_charges'];
			$name = $booking_confirmation[0]['name'];
			$p_time = $booking_confirmation[0]['booking_time'];
			$b_time = $booking_confirmation[0]['createdon'];
			$email = $booking_confirmation[0]['email'];
			$driver_name = $booking_confirmation[0]['first_name'] . ' ' . $booking_confirmation[0]['last_name'];
			$driver_contact = $booking_confirmation[0]['contact'];
			$plateno = $booking_confirmation[0]['plate_number'];
			$title = $booking_confirmation[0]['title'];

			$body = '<html>
<body style="font-family:\'Calibri\'; font-size:16px;">
<div id="main-div"style="background-color:#D7DF01; width:45%; height:130%">
 <br>
 <center>
 <div id ="inline" style="background-color:white; width:90%;height:90%; padding-top:15px;padding-left:24px;padding-bottom:24px;padding-top:24px; broder:1px solid black; border-radius: 25px;">

<p align="left">Booking NO:- <b>' . $b_no . '</b> <img src="logo500.png" height="38px;" width="45px;" style="margin-left:255px;"></p>
<br>
<br>

<p align="left">Hi ' . $name . ',</p>
<p align="left">Thank you for using PrivateCabs! Your Cab booking is <br>confirmed.</p>
<p align="left"><small>BOOKING DETAILS </small></p>
<hr style="width:512px;">
<font style="font-family:Times New Roman;font-size:10px;">
<table border="0"  cellspacing="12" style="margin-left:40px;">
 
 <tr>
  <td>Booking time </td><td>' . $b_time . '</td></tr>
  <tr><td>Pickup time</td><td>' . $p_time . '</td></tr>
  <tr><td>Pickup Address</td><td>' . $pickup_location . '</td></tr>
   <tr><td>Cab Type</td><td>' . $title . '</td></tr>
   <tr><td>Cab Detail</td><td>' . $plateno . '</td></tr>
   <tr><td>Driver Details</td><td>' . $driver_name . ' ' . $driver_contact . '</td></tr>
</table>
</font>
<p align="left"><small>FARE DETAILS </small></p>
  <hr style="width:512px;">
<font style="font-family:Times New Roman;font-size:12px;">
  <table border="0"  cellspacing="12" style="margin-left:-25px;">
 <tr>
  <td>Minimum bill</td><td>' . $description . '</td></tr>
  <tr><td>Waiting Charges</td><td>' . $wait_time_charges . '</td></tr>
  
</table>
</font>
<br>
<br>
<p align="left">*Please refer your Booking No for all communication about this booking. </p>
</center>
 </div>
</body>';

			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';
			$ci -> email -> initialize($config);
			$ci -> email -> from(ADMINMAIL_FROM, 'pd');
			$ci -> email -> to($email);
			$ci -> email -> subject('Confirm Booking');
			$ci -> email -> message($body);
			$ci -> email -> send();
			//$counter ++;
			if ($ci -> email -> send())
			{
				echo '1';
			}
			else
			{
				//echo $filename;
				show_error($ci -> email -> print_debugger());

			}

		}
		else
		{
			die ;
		}
	}

	function SendverificationCode($email, $userid)
	{

		$ci = &get_instance();
		$cipher = $ci -> load -> library('Cipher');
		$ci -> load -> library('email');
		$ci -> load -> database();
		$uniquecode = rand(1000, 99999);
		$to = $email;
		//$registrationcode = $registrationcode;
		$userid = $userid;
		$cipher = new Cipher('PRIVATETAXI_APP');

		$encrypteduserid = $cipher -> encrypt($userid);
		$encryptedcode = $cipher -> encrypt($uniquecode);
		// $uniquecode = md5($uniquecode);
		$subject = 'Private Driver App Password Reset';
		$md5id = md5($userid);

		$message = "<p>Hi,</p>";
		$message .= "We received your password change request.<br/><br/><p>Please <a href='http://dev.privatedriverapp.com/app/resetpassword?userid=$encrypteduserid&code=$encryptedcode&id=$md5id'>click here</a> to reset password</p>";
		//&registerid=$registrationcode
		// More headers
		$headers .= 'From: support@private.co' . "\r\n";
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = true;
		$config['mailtype'] = 'html';
		$ci -> email -> initialize($config);
		$ci -> email -> from(ADMINMAIL_FROM, 'Private Driver');
		$ci -> email -> to($to);
		$ci -> email -> subject($subject);
		$ci -> email -> message($message);
		$ci -> email -> send();
		// $mail_sent = @mail($to, $subject, $message, $headers);
		$updatequery = "update user set password_recovercode= '" . $uniquecode . "' where id='$userid'";

		$data = $ci -> db -> query($updatequery);

		if ($data)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function send_mail_to_admin($company_user_id)
	{

		$ci = &get_instance();
		$ci -> load -> library('email');
		$ci -> load -> database();
		$where_condtion = array("id" => $company_user_id);
		$query = $ci -> db -> get_where("user", $where_condtion);

		if ($query -> num_rows())
		{
			$user_data = $query -> result_array();
			$user_data = $user_data[0];
			$user_name = $user_data['name'];
			$company_name = $user_data['company_name'];
			$email = $user_data['email'];
			$dob = $user_data['dob'];
			$country_code = $user_data['country_code'];
			$phone = $user_data['phone'];
			$is_social = $user_data['is_social'];
			if ($is_social == "Y")
			{
				$loginby = "Facebook";
			}
			else
			{
				$loginby = "Native";
			}

			// <p><strong>DOB</strong>&nbsp;&nbsp;$dob </p>

			$mail_body = "<html><body ><div style='border: solid #f39c12; width: 600px; padding: 15px; height: 350px;'>
<div style='float: right;'><img src='" . base_url() . "images/logo250.png'/> </div>
<div style='float: left;'><p><strong>A new $company_name company user register.</strong></p>
                      <br/>
                    <p><strong>User Name</strong>&nbsp;&nbsp;$user_name </p>
                    <p><strong>email</strong>&nbsp;&nbsp;$email </p>
                   
                    <p><strong>Contact number</strong>&nbsp;&nbsp;$country_code $phone </p>
                    <p><strong>Register By</strong>&nbsp;&nbsp;$loginby</p>
                    <br/>
                    <p>Thanks</p></div></div></body><html>";

			$subject = "A new Company User Registered and Needs your Approval!";
			$email = ADMINMAIL;
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';
			$ci -> email -> initialize($config);
			$ci -> email -> from(ADMINMAIL_FROM, 'Private Driver');
			$ci -> email -> to($email);
			$ci -> email -> subject($subject);
			$ci -> email -> message($mail_body);

			//$counter ++;
			if ($ci -> email -> send())
			{
				// echo '1';
			}
			else
			{
				// echo "error";
				show_error($ci -> email -> print_debugger());

			}

		}

	}

	function prebooking_mail_to_admin($booking_id)
	{

		$ci = &get_instance();
		$ci -> load -> library('email');
		$ci -> load -> database();
		$where_condtion = array("id" => $company_user_id);
		$query = $ci -> db -> query("SELECT *
FROM `restro_cab_booking` rb
JOIN restaurant r ON r.id = rb.restro_id
JOIN cab_type ON type_id = rb.cab_type
WHERE rb.id =$booking_id");

		if ($query -> num_rows())
		{
			$user_data = $query -> result_array();
			$user_data = $user_data[0];
			$client_name = $user_data['client_name'];
			$pickup_location = $user_data['pickup_loaction'];
			$booking_time = $user_data['booking_time'];
			$restro_name = $user_data['name'];
			$restro_address = $user_data['address'];
			$restro_contact = $user_data['contact_number'];
			$cab_type = $user_data['title'];
			$country_code = $user_data['country_code'];
			$phone = $user_data['client_contact_number'];
			$mail_body = "<html><body ><div style='border: solid #f39c12; width: 600px; padding: 15px; height: 350px;'>
<div style='float: right;'><img src='" . base_url() . "images/logo125.png' /> </div>
<div style='float: left;'><p><strong>A New Restaurant Pre Booking from  $restro_name [ $phone ] </strong></p>
                      <br/>
                    <p><strong>Client Name</strong>&nbsp;&nbsp;$client_name </p>
                    <p><strong>Client Contact Detail</strong>&nbsp;&nbsp;$country_code $phone </p>
                   
                    <p><strong>Pickup Location</strong>&nbsp;&nbsp;$restro_address </p>
                    <p><strong>Pickup Time</strong>&nbsp;&nbsp;" . ($booking_time) . "</p>
                     <p><strong>Cab Type</strong>&nbsp;&nbsp;$cab_type</p>
                    <br/>
                     <p>Best Regards With</p>
					<p>Private Driver A/S<p></div></div></body><html>";

			$subject = "A new pre-booking from restaurant!";
			$email = ADMINMAIL;
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';
			$ci -> email -> initialize($config);
			$ci -> email -> from(ADMINMAIL_FROM, 'Private Driver');
			$ci -> email -> to($email);
			$ci -> email -> cc("priyanka@foxinfosoft.com");
			$ci -> email -> subject($subject);
			$ci -> email -> message($mail_body);
			if ($ci -> email -> send())
			{
				// echo '1';
			}

		}

	}

	function normal_prebooking_mail_to_admin($booking_id)
	{

		$ci = &get_instance();
		$ci -> load -> library('email');
		$ci -> load -> database();
		$where_condtion = array("id" => $company_user_id);
		$query = $ci -> db -> query("SELECT pickup_location, destination_location, booking_time, name, country_code, phone, title
FROM `cab_booking` b
JOIN cab_type t ON b.cab_type = t.type_id
JOIN user u ON u.id = b.user_id
WHERE b.id =$booking_id");

		if ($query -> num_rows())
		{
			$user_data = $query -> result_array();
			$user_data = $user_data[0];
			$user_name = $user_data['name'];
			$pickup_location = $user_data['pickup_location'];
			$booking_time = $user_data['booking_time'];
			$contact = $user_data['country_code'] . $user_data['phone'];
			$cab_type = $user_data['title'];
			$destination_location = $user_data['destination_location'];
			$mail_body = "<html><body ><div style='border: solid #f39c12; width: 600px; padding: 15px; height: 350px;'>
<div style='float: right;'><img src='" . base_url() . "images/logo125.png'/> </div>
<div style='float: left;'><p><strong>A New Regular Pre Booking </strong></p>
                      <br/>
                    <p><strong>Name</strong>&nbsp;&nbsp;$user_name </p>
                    <p><strong>Contact Detail</strong>&nbsp;&nbsp;$contact </p>
                   
                    <p><strong>Pickup Location</strong>&nbsp;&nbsp;$pickup_location </p>
                    <p><strong>Destination Location</strong>&nbsp;&nbsp;$destination_location </p>
                    <p><strong>Pickup Time</strong>&nbsp;&nbsp;" . ($booking_time) . "</p>
                    <p><strong>Cab Type</strong>&nbsp;&nbsp;$cab_type</p>
                    <br/>
                    <p>Best Regards With</p>
					<p>Private Driver A/S<p></div></div></body><html>";

			$subject = "A new pre-booking from regular customer!";
			$email = ADMINMAIL;
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';
			$ci -> email -> initialize($config);
			$ci -> email -> from(ADMINMAIL_FROM, 'Private Driver');
			$ci -> email -> to($email);
			$ci -> email -> cc("priyanka@foxinfosoft.com");
			$ci -> email -> subject($subject);
			$ci -> email -> message($mail_body);
			if ($ci -> email -> send())
			{
				// echo '1';
			}

		}

	}

	function credit_card_number($cc_number)
	{
		$last_digit = substr($cc_number, -4);
		//sprintf("%06d", $last_digit);
		$count = strlen($cc_number);
		return str_pad($last_digit, $count, "X", STR_PAD_LEFT);
	}

	function getETA($addsec)
	{
		$norway_time = gmt_to_norway(gmdate("Y-m-d H:i:s"));
		$timestamp = strtotime($norway_time) + $addsec;
		return date("d-m-Y h:i:s", $timestamp);
	}

	function gmt_to_norway($datetime)
	{
		$ci = &get_instance();
		$ci -> load -> database();
		$query = $ci -> db -> query("SELECT time_zone from timezone");
		$data = $query -> result();

		$time = $data[0] -> time_zone;

		return date('d-M-Y H:i:s', strtotime($datetime) + $time * 3600);
	}

	function format_time($sec)
	{
		$hours = floor($sec / 3600);
		$minutes = floor(($sec / 60) % 60);
		$seconds = $sec % 60;
		$duration = (!$hours) ? "" : $hours . " Hr ";
		$duration .= (!$minutes) ? "" : $minutes . " Min ";
		//$duration .= (!$seconds) ? "" : $seconds . " Sec ";
		return $duration;
	}
	
	function current_time()
	{
		return strtotime(date("Y-m-d h:i:s"));
	}

	/* function for company email */
	function company_email($get_email)
	{
		$ci = &get_instance();
		$ci -> load -> library('email');

		$body = "<html><body ><div style='border: solid #f39c12; width: 600px; padding: 15px; height: 240px;'>
<div style='float: right;'><img src='" . base_url() . "images/logo125.png'/> </div>
<div style='float: left;margin-top:-122px;'><p><strong>Velkommen som kunde hos.</strong></p>
                      
                    <p><strong>Du kan nå begynne å bruke firmakontoen:</strong></p>
                    <p><strong> Logg deg inn med e-post og passord.</strong></p>
					 <p><strong>Vi håper du blir fornøyd med våre tjenester.Med vennlig hilsen</strong></p>
                    <p>Best Regards With</p>
					<p>Private Driver A/S<p></div></div></body><html>";

		/* $config['protocol'] = 'sendmail';
		 $config['mailpath'] = '/usr/sbin/sendmail';
		 $config['charset'] = 'iso-8859-1';
		 $config['wordwrap'] = true;
		 $config['mailtype'] = 'html';*/
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = true;
		$config['mailtype'] = 'html';
		$config['priority'] = 1;
		$config['charset'] = 'utf-8';
		$config['smtp_host'] = "vps.privatedriverapp.com";
		//465//vps.privatedriverapp.com
		$config['smtp_user'] = "post@privatedriverapp.com";
		$config['smtp_pass'] = "T@xi@2015";
		$config['smtp_port'] = "465";
		$ci -> email -> initialize($config);

		$messageid = md5(uniqid(time())) . "@privatedriverapp.com";
		$ci -> email -> set_header('Message-ID', $messageid);

		$ci -> email -> from(ADMINMAIL_FROM, 'Private Driver');
		$ci -> email -> to($get_email);
		$ci -> email -> cc('saurabh.devera18@gmail.com');
		$ci -> email -> subject('Account Activation');
		$ci -> email -> message($body);

		//$counter ++;
		if ($ci -> email -> send())
		{
			echo '1';
		}
		else
		{
			//echo $filename;
			show_error($ci -> email -> print_debugger());

		}

	}
    function company_invoice_recreate($bookingid,$type="normal")
	{
		$ci = &get_instance();
		$ci -> load -> library('email');
		$ci -> load -> library('Pdf');
		$ci -> load -> database();
	////waiting_charges_per_min,charges_per_km,basic_fare_charge
		$sql = "select cu.title,card.credit_card_no,b.driver_id,d.first_name,d.last_name,d.profile_pic,p.tip,p.id, p.km_driven,p.km_charge,
            p.journey_time,p.waiting_time,p.waiting_charge,p.billing_amount,u.name,u.email,u.company_name,b.booking_time,u.is_company,cp.email as company_email,
            b.booking_number,b.pickup_location, b.destination_location, p.basic_fare_charge,p.charges_per_km,p.waiting_charges_per_min,p.user_paid
            from payment_detail p
            join cab_booking b on b.id=p.booking_id 
            join user u on u.id=b.user_id 
            join driver d on d.driver_id=b.driver_id 
         
            join cab_type cu on cu.type_id=b.cab_type 
            left  join card_info card on card.customer_id=u.id
            left join company cp on  cp.id=u.company_id where b.id=$bookingid";
		$data = $ci -> db -> query($sql);
		//echo $ci->db->last_query();
		if ($data -> num_rows() > 0)
		{
			$get_driver_status = $ci -> db -> query("select * from driver_status where booking_id='$bookingid'");
			// $get_status=$get_driver_status->row();
			$get_data = $get_driver_status -> result();
			foreach ($get_data as $time_status)
			{
				$c_status = $time_status -> status;
				if ($c_status == 'TRIP_STARTED')
				{
					$s_time = gmt_to_norway($time_status -> createdon);
					$start = date("H:i", strtotime($s_time));

				}
				elseif ($c_status == 'TRIP_ENDED')
				{
					$e_time = gmt_to_norway($time_status -> createdon);
					$end = date("H:i", strtotime($e_time));
				}
				else
				{

					$s_time = '';

				}
			}
			$date = date('d.m.Y');
			$booking_data = $data -> result_array();
			$basic_price = number_format($booking_data[0]['basic_fare_charge'], 2, ',', '');
			$km_charge = number_format($booking_data[0]['km_charge'], 2, ',', '');
			$driver_name = $booking_data[0]['first_name'] . ' ' . $booking_data[0]['last_name'];
			$profile_pic = $booking_data[0]['profile_pic'];
			$car = $booking_data[0]['title'];
			$card = $booking_data[0]['credit_card_no'];
			$credit_card = credit_card_number($card);
			$cbn = $booking_data[0]['booking_number'];
			$km = number_format($booking_data[0]['km_driven'], 2, ',', '');
			$j_time = $booking_data[0]['journey_time'];
			//$w_time = $booking_data[0]['waiting_time'];
			$w_charge = number_format($booking_data[0]['waiting_charge'], 2, ',', '');
			$billing_amount = number_format($booking_data[0]['billing_amount'], 2, ',', '');
			$name = $booking_data[0]['name'];
			$email = $booking_data[0]['email'];
			$booking_time = $booking_data[0]['booking_time'];
			$company_name = $booking_data[0]['company_name'];
			$pickup_location = $booking_data[0]['pickup_location'];
			$destination_location = $booking_data[0]['destination_location'];
			$service_tax_free = 100 / VAT * ($booking_data[0]['billing_amount']);
			$service_tax = $booking_data[0][billing_amount] - $service_tax_free;
			$tax = number_format($service_tax, 2, ',', '');
			$tip = number_format($booking_data[0]['tip'], 2, ',', '');
			$user_paid = number_format($booking_data[0]['user_paid'], 2, ',', '');
			$is_company = $booking_data[0]['is_company'];

			$per_km_charges = number_format($booking_data[0]['charges_per_km'], 2, ',', '');

			$journey_time_change = number_format($booking_data[0]['waiting_charges_per_min'], 2, ',', '');
			$msg_replace = array(
				"Sec",
				"Min",
				"Hr"
			);
			$detail = array(
				'Sek',
				'Minutt',
				'Hr'
			);
			$j_time = str_replace($msg_replace, $detail, format_time($j_time));
			if ($company_name != "")
			{
				$cname = $booking_data[0]['company_name'];
				$title = "company_name";
				$username = $cname;
			}
			else
			{
				$cname = "";
				$title = "";
				$username = $booking_data[0]['name'];
			}
			if ($is_company == 'Y')
			{
				$cname = $booking_data[0]['company_name'];
				$title = "company_name";
				$username = $booking_data[0]['name'];
				$company_email = $booking_data[0]['company_email'];
				$text = "<p>Hei.</p><p>Vedlagt finner du kvittering for en av deres ansattes tur med oss.</p>
			";
			}
			else
			{
				$cname = "";
				$title = "";
				$username = $booking_data[0]['name'];

			}

			if ($type != "normal")
			{
				$basic_price = "100.00";
				$start = "--";
				$end = "--";
				$tax = 8.00;
			}

			$header = '<html>
	<body>
		<div style="border:8px solid ; border-color:  #8E6540 ; width: 700px" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; font-weight:bold; color:#000"  >
		
			<table align="center" width="100%" cellpadding="3" cellspacing="3"style="font-family:Arial, Helvetica, sans-serif; font-size:25px; font-weight:bold; color:#999999"  >
				<tr>
					<td align="center"><img src="http://privatedriverapp.com/app/logo100.jpeg" align="center"/> </td>
						
					</tr>
					<tr><td align="center" colspan="2">Private Driver A/S</td>
				
					</tr>
					<tr><td style="font-size:20px;" align="center" >Org no: <b>913341856</b></td>
					
					</tr>
					<tr><td style="font-size:20px;" align="center" ><b>KVITTERING</b></td></tr>
					</table>
					
							
					<br />';
			$html = '<table align="center" border="0px" width="90%" cellpadding="3" cellspacing="3" style="font-family:Arial, Helvetica, sans-serif; font-size:17px; color:#000">
					<tr>
					<td align="left">Dato:</td>
					<td align="left">' . $date . '</td>
					<td><img src="http://privatedriverapp.com/app/unpaid-stamp.png"/></td>
					</tr><tr>
						<td align="left">Kvitteringsnummer:</td>
					<td colspan="2" align="left" >' . $cbn . '</td>
					</tr>
				    <tr>
					<td align="left">Navn:</td>
					<td colspan="2" align="left">' . $username . '</td>
					</tr>
					<tr>
					<td align="left">Fra:</td>
					<td colspan="2" align="left">' . $pickup_location . '</td>
					</tr>
					<tr>
					<td align="left">Til:</td>
					<td colspan="2" align="left">' . $destination_location . '</td>
					</tr>
					<tr>
					<td align="left">Start:</td>
					<td colspan="2" align="left">' . $start . '</td>
					</tr>
					<tr>
					<td align="left">Slutt:</td>
					<td colspan="2" align="left">' . $end . '</td>
					</tr>
					<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
					</tr>
					<tr><td colspan="2" align="left" ><b>PRIS-SPESIFIKASION:</b></td></tr>
					<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
					</tr>
					<tr>
					<td align="left">Grunnpris:</td>
					<td align="left">NOK</td>
					<td align="right">' . $basic_price . '</td>
					</tr>
					<tr>
					<td align="left">Km á kr (' . $per_km_charges . '):</td>
					<td align="left">NOK</td>
					<td align="right">' . $km_charge . '</td>
					</tr>
					<tr>
					<td align="left">Tid  á kr (' . $journey_time_change . ')per minutt:</td>
					<td align="left">NOK</td>
					<td align="right">' . $w_charge . '</td>
					</tr>
					<tr>
					<td align="left">Subtotal:</td>
					<td align="left">NOK</td>
					<td align="right">' . $billing_amount . '</td>
					</tr>
					<tr>
					<td align="left">Herav mva 10%:</td>
					<td align="left">NOK</td>
					<td align="right">' . $tax . '</td>
					</tr>
					<tr>
					<td align="left">Tips:</td>
					<td align="left">NOK</td>
					<td align="right">' . $tip . '</td>
					</tr>
					<tr>
					<td align="left">Total:</td>
					<td align="left">NOK</td>
					<td align="right">' . $billing_amount . '</td>
					</tr>
					
					<tr>
					<td colspan="2" align="left" >KM:</td>
					
					<td align="right">' . $km . '</td>

					</tr>
					<tr>
					<td  colspan="2"  align="left">Tid:</td>
					
					<td align="right">' . $j_time . '</td>

					</tr>
					<tr style="height:5px;"> <td style="font-size:xx-small;">&nbsp;</td>
				</tr>
				
				<tr style="height:5px;">
					<td style="font-size:xx-small;">&nbsp;</td>
				</tr>
				<tr>
					<td align="left" colspan="3">Takk for at du valgte Private Driver</td>
				</tr>
				<tr>
					<td align="left" colspan="2">www.privatedriver.no</td>
				</tr>

			</table>
		</div>
	</body>
</html>';
			$footer = "<p>Med vennlig hilsen</p>";
			$file = 'Invoice_company' . '_' . $cbn;
			$filename = $file . '.pdf';
			$body = $header . $html;
		return 	$filename = sendmail($header, $html, $filename);
		
	}
	}
?>
