<?php

class Peakhour_model extends CI_Model {

	public function __construct() {
		$this -> load -> database();
	}



function get_bookingdetail($booking_no)
	{
		$get_advance_booking=$this->db->query("SELECT cab_booking.id as bid,cab_booking.booking_number,cab_booking.booking_time,cab_booking.comment,cab_booking.createdon,cab_booking.cab_type,cab_booking.pickup_location,cab_booking.destination_location,cab_booking.status,user.id as uid,user.name,user.phone,user.email,d.driver_id,d.first_name,d.last_name FROM `cab_booking` join user on user.id=cab_booking.user_id left join driver d on d.driver_id=cab_booking.driver_id where cab_booking.id='$booking_no'");
		//echo $this->db->last_query();
		return $get_advance_booking->result();
	}
	
	function get_driver()
	{
		$this->db->select("driver_id,first_name,last_name");
		$this->db->where("isdelete","0");
		$query=$this->db->get("driver");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}else
			{
				return false;
			}
				
	}
	
	function get_customer()
	{
		$this->db->select("id,name");
		$this->db->where("is_active","1");
		$query=$this->db->get("user");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}else
			{
				return false;
			}
	}
	
	function DriverPeakhour($date,$driver_id)
	{
	   	$query ="SELECT count(cab_booking.id) booking_count,sum(payment_detail.billing_amount) amount,cab_booking.status,payment_detail.description,hour(booking_time) hour FROM `cab_booking` left join payment_detail on payment_detail.booking_id=cab_booking.id WHERE driver_id='$driver_id' and date(cab_booking.booking_time)='$date' group by hour(booking_time)";
		$res=$this->db->query($query);
		if($res->num_rows()>0)
		{
			return $res->result_array();
		}else
			{
				return FALSE;
			}
	}
	
	function CustomerPeakHour($date,$user_id)
	{
		 $query ="SELECT count(cab_booking.id) booking_count,sum(payment_detail.billing_amount) amount,cab_booking.status,payment_detail.description,hour(booking_time) hour FROM `cab_booking` left join payment_detail on payment_detail.booking_id=cab_booking.id WHERE user_id='$user_id' and date(cab_booking.booking_time)='$date' group by hour(booking_time)";
		$res=$this->db->query($query);
		if($res->num_rows()>0)
		{
			return $res->result_array();
		}else
			{
				return FALSE;
			}
	}
	function drivertime_report($starttime,$endtime,$driver_id)
	{
		//$query="select * from driver_status where driver_id=$driver_id and date(createdon)  between '$starttime' and '$endtime' and status not in ('FIVE_MIN_AWAY','I_AM_HERE ') ";
	    $query="select * from driver_status where driver_id=$driver_id and date(createdon) = '$starttime' and status not in ('FIVE_MIN_AWAY','I_AM_HERE ') ";
	    $res=$this->db->query($query);
		
		//echo $this->db->last_query();
		if($res->num_rows()>0)
		{
			return $res->result_array();
		}else
		{
			return false;
		}
		
	}
	function get_shiftdetail($shift_id)
	{
		$query ="SELECT t.title,c.cab_model,c.cab_plate_no FROM `shift` s 
		join cab c on c.cab_id =s.cab_id 
		join cab_type t on t.type_id=c.cab_type 
		where s.s_id=$shift_id";
		$res=$this->db->query($query);
	//	echo $this->db->last_query();
		if($res->num_rows()>0)
		{
			return $res->row_array();
		}else
			{
				return false;
			}
		
	}
}