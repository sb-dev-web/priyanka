<?php

	defined('BASEPATH') or exit('No direct script access allowed');
	// This can be removed if you use __autoload() in config.php OR use Modular
	// Extensions
	require APPPATH . '/libraries/REST_Controller.php';
	//error_reporting(0);
	class Customer extends REST_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> library(array(
				'upload',
				'Braintree_lib',
				'Twilio',
				'email'
			));

			$this -> load -> library('Openfire');
			$this -> load -> library('xmpp', false);
			$this -> load -> helper(array(
				'form',
				'url',
				'string',
				'constants_helper',
				'function_helper',
				'braintree_function'
			));

			$this -> load -> model(array(
				'V3/Customer_model',
				'General_model',
				'V3/Resetpassword_model',
				'V3/Bookingapi_model'
			));

		}

		/* register user */
		function register_user_old()
		{
			/* check that user is already register or not */
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('register-user', $input_method);
			$this -> param_validate($input_method, array(
				"name",
				"email",
				"country_code",
				"phone",
				"credit_card_no",
				"expire_date",
				"cvv_no"
			));
			// check user is exist or not

			$user_data = $this -> Customer_model -> check_exist_email_temp($input_method);
			if ($user_data['status'] == 0)
			{
				$this -> response(array(
					'message' => $user_data['message'],
					'status' => "0"
				), 200);
			}
			else
			{
				$name = $input_method['name'];
				$email = $input_method['email'];

				$phone = $input_method['phone'];
				$country_code = $input_method['country_code'];
				$phone_number = "+" . $country_code . $phone;
				$credit_card_no = $input_method['credit_card_no'];
				$cvv_no = $input_method['cvv_no'];
				$expire_date = $input_method['expire_date'];

				$data = array(
					"name" => $name,
					"email" => $email,
					"phone" => $phone,
					"ccnumber" => $credit_card_no,
					"cvv_no" => $cvv_no,
					"exp_date" => $expire_date
				);
				if ($credit_card_no != '4111111111111112')
				{
					$braintree_response = Create_withCreditCardAndVerification($data);
				}
				else
				{
					$braintree_response['customer_id'] = '123';
					$braintree_response['token'] = '123xyz';
					$braintree_response['cardtype'] = 'test';
					$braintree_response['status'] = '1';
				}
				if ($braintree_response['status'] == "1")
				{
					//Insert into user and CC info table
					$unique_number = random_number();
					$email_code = random_number();
					$input_method['varifycode'] = $unique_number;
					$input_method['email_code'] = $email_code;
					$user_id = $this -> Customer_model -> save_user($input_method, $braintree_response);
					if ($user_id)
					{
						$user_name = USER_JID_NAME . '-' . $user_id;

						if (@$input_method['is_social'] == 'N')
						{
							$password = $input_method['password'];
							$password = trim($password);
							$is_social = "N";
						}
						else
						{
							$password = "123456";
							$password = trim($password);
							$is_social = "Y";
						}

						//make a braintree user

						$param = array(
							$user_name,
							$password,
							$name,
							$email
						);

						//$xmpp_response = $this -> xmpp -> api("add", $param);

						$message = VARIFYSMS . $unique_number;
						// echo $message;
						//exit;

						send_sms($phone_number, $message);
						if (@$input_method['is_social'] != "Y")
						{
							AccountVerifyMail($email, $user_id, $email_code);
						}
						$customer = array(

							"customer_id" => $userid = "$user_id",
							"customer_jid" => makejid($userid, USER_JID_NAME),
							"name" => $name = $input_method['name'],
							"email" => $email = $input_method['email'],
							"dob" => $dob = $input_method['dob'],
							"phone" => $phone,
							'country_code' => $country_code,
							"credit_card" => credit_card_number($credit_card_no),
							"is_social" => $is_social,
							"is_mobile_varify" => "0" // "code"=>$unique_number,
							// "messge"=>$message,

						);

						$this -> response(array(
							'customer' => $customer,
							'message' => REGISTER_SUCCESS,
							'status' => "1"
						), 200);
					}
					else
					{
						$this -> response(array(

							'message' => $braintree_response['message'],
							'errorcode' => $braintree_response['errorcode'],
							'status' => 0
						), 200);
					}
					//exit;

				}
				else
				{
					$this -> response(array(

						'message' => $braintree_response['message'],
						'errorcode' => $braintree_response['errorcode'],
						'status' => "0"
					), 200);
				}

			}
		}

		/* register user */
		function register_user()
		{
			/* check that user is already register or not */
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('register-user', $input_method);
			$this -> param_validate($input_method, array(
				"name",
				"email",
				"country_code",
				"phone",
				'nonce'
			));
			// check user is exist or not

			$user_data = $this -> Customer_model -> check_exist_email_temp($input_method);
			if ($user_data['status'] == 0)
			{
				$this -> response(array(
					'message' => $user_data['message'],
					'status' => "0"
				), 200);
			}
			else
			{
				$name = $input_method['name'];
				$email = $input_method['email'];

				$phone = $input_method['phone'];
				$country_code = $input_method['country_code'];
				$phone_number = "+" . $country_code . $phone;
				$credit_card_no = $input_method['credit_card_no'];
				$cvv_no = $input_method['cvv_no'];
				$expire_date = $input_method['expire_date'];

				$data = array(
					"name" => $name,
					"email" => $email,
					"phone" => $phone,
					"nonce" => $input_method['nonce'],
				);
				if ($credit_card_no != '4111111111111112')
				{
					//$braintree_response = Create_withCreditCardAndVerification($data);
					$braintree_response = CreateCustomer($data);
				}
				else
				{
					$braintree_response['customer_id'] = '123';
					$braintree_response['token'] = '123xyz';
					$braintree_response['cardtype'] = 'test';
					$braintree_response['status'] = '1';
				}
				if ($braintree_response['status'] == "1")
				{
					//Insert into user and CC info table
					$unique_number = random_number();
					$email_code = random_number();
					$input_method['varifycode'] = $unique_number;
					$input_method['email_code'] = $email_code;
					$user_id = $this -> Customer_model -> save_user($input_method, $braintree_response);
					if ($user_id)
					{
						$user_name = USER_JID_NAME . '-' . $user_id;

						if (@$input_method['is_social'] == 'N')
						{
							$password = $input_method['password'];
							$password = trim($password);
							$is_social = "N";
						}
						else
						{
							$password = "123456";
							$password = trim($password);
							$is_social = "Y";
						}

						//make a braintree user

						$message = VARIFYSMS . $unique_number;
						// echo $message;
						//exit;

						send_sms($phone_number, $message);
						if (@$input_method['is_social'] != "Y")
						{
							AccountVerifyMail($email, $user_id, $email_code);
						}
						$customer = array(
							//"customer_jid"=>makejid($user_detail->user_id,USER_JID_NAME), //19 oc add new
							// tag for andorid ---line commented by raj temporary
							//"customer_jid"=>$user_name."@privatedriverapp.com", //5 dec. hard coded by raj
							// as makejid is not correct..
							"customer_id" => $userid = "$user_id",
							"customer_jid" => makejid($userid, USER_JID_NAME),
							"name" => $name = $input_method['name'],
							"email" => $email = $input_method['email'],
							"dob" => $dob = $input_method['dob'],
							"phone" => $phone,
							'country_code' => $country_code,
							"credit_card" => credit_card_number($credit_card_no),
							"is_social" => $is_social,
							"is_mobile_varify" => "0" // "code"=>$unique_number,
							// "messge"=>$message,

						);

						$this -> response(array(
							'customer' => $customer,
							'message' => REGISTER_SUCCESS,
							'status' => "1"
						), 200);
					}
					else
					{
						$this -> response(array(

							'message' => $braintree_response['message'],
							'errorcode' => $braintree_response['errorcode'],
							'status' => 0
						), 200);
					}
					//exit;

				}
				else
				{
					$this -> response(array(

						'message' => $braintree_response['message'],
						'errorcode' => $braintree_response['errorcode'],
						'status' => "0"
					), 200);
				}

			}
		}

		/* check social loign information */
		function social_login()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('social-login', $input_method);
			$this -> param_validate($input_method, array(
				"name",
				"email",
				"s_id",
			));

			$user_data = $this -> Customer_model -> check_exist_email($input_method);
			//print_r($user_data);
			if (($user_data))
			{
				//if account register as company user send a error
				if ($user_data[0] -> is_company == 'Y')
				{
					$this -> response(array(
						'message' => EMAIL_NOT_EXIST_AS_COMPANYUSER,
						'status' => "0"
					), 200);
					exit ;
				}
				if ($user_data[0] -> is_active == '0')
				{
					$this -> response(array(
						'message' => USER_NOT_ACTIVE,
						'status' => "0"
					), 200);

				}

				$user_update = $this -> Customer_model -> update_user($input_method);
				$user_data = $this -> Customer_model -> get_user_detail($input_method);
				//print_r($user_data);
				if (($user_data) > 0)
				{
					foreach ($user_data as $user_detail)
					{

						$is_registered = "Y";
						$credit_card = '';

						if ($user_detail -> credit_card_no == "")
						{
							$iscreditcard = 'N';
						}
						else
						{
							$iscreditcard = 'Y';
						}
						if ($user_detail -> is_varify == 0)
						{
							$varifycode = $user_detail -> varifycode;
							$phone = $user_detail -> phone;
							$country_code = $user_detail -> country_code;
							$phone_number = "+" . $country_code . $phone;
							$message = VARIFYSMS . $varifycode;
							send_sms($phone_number, $message);
						}
						$is_approved = "";
						$user_type = "";
						$company_id = $company_name = "";
						$user_id = $user_detail -> user_id;
						//get user company detail
						$company_detail = $this -> Customer_model -> GetCompanyDetail($user_id);
						//print_r($company_detail);
						if ($company_detail)
						{
							$company_name = ($company_detail['company_name']) ? $company_detail['company_name'] : $company_detail['usercompany'];
							$is_approved = $company_detail['is_approved'];
							$company_id = $company_detail['company_id'];
						}
						if ($company_id && $user_detail -> credit_card_no)
						{
							$user_type = "B";
						}
						elseif ($company_id)
						{
							$user_type = "C";
						}
						elseif ($user_detail -> credit_card_no)
						{
							$user_type = "I";
						}
						$header = getallheaders();
						//print_r($header);
						if (isset($header['os-type']) == 'IOS')
						{
							$customer_jid = makejid($login -> id, USER_JID_NAME);
						}
						else
						{
							$customer_jid = USER_JID_NAME . '-' . $login -> id;
						}

						$customer = array(
							"customer_jid" => $customer_jid,
							"customer_id" => $userid = $user_detail -> user_id,
							"name" => $name = $user_detail -> name,
							"email" => $email = $user_detail -> email,
							"dob" => $dob = $user_detail -> dob,
							"sid" => $sid = $user_detail -> s_id,
							"phone" => $phone = $user_detail -> phone,
							"country_code" => $user_detail -> country_code,
							"credit_card" => ($user_detail -> credit_card_no) ? $user_detail -> credit_card_no : "",
							// //$credit_card=$user_detail->credit_card_no,
							"is_social" => "Y",
							"is_mobile_varify" => "$user_detail->is_varify",
							"mode" => $user_detail -> mode,
							'company_name' => $company_name,
							"is_approved" => $is_approved,
							"company_id" => $company_id,
							"user_type" => $user_type,
							"is_receipt_enable" => $user_detail -> reciept_enable
						);
					}
					$this -> response(array(
						'customer' => $customer,
						'is_registered' => $is_registered,
						'status' => "1"
					), 200);
				}
				else
				{
					//email exist in temp_table but not in main
					$customer = array(

						"name" => $input_method['name'],
						"dob" => $input_method['dob'],
						"email" => $input_method['email'],
						"s_id" => $input_method['s_id']
					);

					$this -> response(array(
						'customer' => $customer,
						'is_registered' => 'N',
						'status' => 1
					), 200);
				}

			}
			else
			{
				$customer = array(

					"name" => $input_method['name'],
					"dob" => $input_method['dob'],
					"email" => $input_method['email'],
					"s_id" => $input_method['s_id']
				);

				$this -> response(array(
					'customer' => $customer,
					'is_registered' => 'N',
					'status' => 1
				), 200);

			}
		}

		/* enter credit crad infomation */

		function user_login()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('user-login', $input_method);
			$this -> param_validate($input_method, array(
				"email",
				"password",
			));
			$user_data = $this -> Customer_model -> check_exist_email($input_method);
			if (sizeof($user_data) > 0)
			{
				$user_data = $this -> Customer_model -> check_login($input_method);
				if (sizeof($user_data) > 0)
				{
					// update the user device token
					foreach ($user_data as $login)
					{
						if ($login -> is_active == "0")
						{
							$this -> response(array(
								'message' => USER_NOT_ACTIVE,
								'status' => "0"
							), 200);
						}
						if ($login -> is_company == "Y" && $login -> is_approved == "N")
						{

							$this -> response(array(
								'message' => NOT_APPROVE,
								'status' => "0"
							), 200);
						}

						if ($login -> is_varify == 0)
						{
							$varify_code = $login -> varifycode;
							$phone = "+" . $login -> phone;
							$message = VARIFYSMS . $varify_code;
							send_sms($phone, $message);
						}
						if ($login -> is_emailverify == 0)
						{
							AccountVerifyMail($login -> email, $login -> id, $login -> email_code);
						}
						$phone = $login -> phone;
						$is_approved = "N";
						$company_id = $company_name = "";
						$user_id = $login -> id;
						//get user company detail
						$company_detail = $this -> Customer_model -> GetCompanyDetail($user_id);
						//print_r($company_detail);
						if ($company_detail)
						{
							$company_name = ($company_detail['company_name']) ? $company_detail['company_name'] : $company_detail['usercompany'];
							$is_approved = $company_detail['is_approved'];
							$company_id = $company_detail['company_id'];
						}
						if ($company_id && $login -> credit_card_no)
						{
							$user_type = "B";
						}
						elseif ($company_id)
						{
							$user_type = "C";
						}
						elseif ($login -> credit_card_no)
						{
							$user_type = "I";
						}
						else
						{
							$user_type = "I";
							//for company user
						}
						$header = getallheaders();
						//print_r($header);
						if (isset($header['os-type']) == 'IOS')
						{
							$customer_jid = makejid($login -> id, USER_JID_NAME);
						}
						else
						{
							$customer_jid = USER_JID_NAME . '-' . $login -> id;
						}
						$customer = array(
							"customer_jid" => $customer_jid,
							"customer_id" => $userid = $login -> id,
							"name" => $name = $login -> name,
							"email" => $email = $login -> email,
							"dob" => $dob = $login -> dob,
							"phone" => $phone,
							"country_code" => $login -> country_code,
							"is_social" => "N",
							"is_mobile_varify" => "$login->is_varify",
							"credit_card" => "$login->credit_card_no",
							"is_emailverify" => ($login -> is_emailverify) ? "1" : $login -> is_emailverify,
							"iscompany_user" => ($login -> is_company == 'Y') ? "1" : "0",
							"mode" => $login -> mode,
							'company_name' => $company_name,
							"is_approved" => $is_approved,
							"company_id" => $company_id,
							"is_receipt_enable" => $login -> reciept_enable,
							"user_type" => $user_type,
						);

					}
					//$this->Customer_model->update_device($input_method);

					$this -> response(array(
						'customer' => $customer,
						'message' => SUCCESS,
						'status' => "1"
					), 200);
				}
				else
				{
					$this -> response(array(
						'message' => CUSTOMER_LOGIN_ERROR,
						'status' => "0"
					), 200);
				}

			}
			else
			{
				$this -> response(array(
					'message' => EMAIL_NOT_EXIST,
					'status' => "0"
				), 200);
			}
		}

		function forget_password()
		{
			error_reporting(1);
			//date_default_timezone_set('UTC');
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('forget-password', $input_method);
			$this -> param_validate($input_method, array("email"));

			$user_data = $this -> Customer_model -> check_exist_email($input_method);
			 print_r($user_data);
			if ($user_data)
			{

				$login_type = $user_data[0] -> is_social;
				if ($login_type == 'Y')
				{

					$this -> response(array(
						'message' => FB_LOGIN,
						'status' => 0
					), 200);
				}

				else
				{

					$mail_sent = SendverificationCode($user_data[0] -> email, $user_data[0] -> id);
					if ($mail_sent)
					{
						$this -> response(array(
							'message' => FORGET_PASSWORD,
							'status' => 1
						), 200);
					}
					else
					{
						$this -> response(array(
							'message' => SERVER_ERROR,
							'status' => 0
						), 200);
					}

				}
			}
			else
			{
				$this -> response(array(
					'message' => EMAIL_NOT_EXIST,
					'status' => 0
				), 200);
			}
		}

		function getuser_detail()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('getuser-detail', $input_method);
			$this -> param_validate($input_method, array("user_id"));
			$user_data = $this -> Customer_model -> get_user($input_method['user_id']);
			if ($user_data[0] -> profile_pic != "")
				$user_data[0] -> profile_pic = base_url() . CUSTOMER_IMAGE_PATH . $user_data[0] -> profile_pic;
			if ($user_data[0] -> company_id == 0)
			{
				$is_company_verify = "N";
			}
			else
			{
				$is_company_verify = "Y";
			}
			if ($user_data[0] -> user_company_name == "")
			{
				$is_company_added = "N";
			}
			else
			{
				$is_company_added = "Y";
			}
			if ($user_data[0] -> company_name == "")
			{
				$user_data[0] -> company_name = "";
			}
			$user_data[0] -> is_company_added = $is_company_added;
			$user_data[0] -> is_company_verify = $is_company_verify;
			$this -> response(array(
				'data' => $user_data[0],
				'status' => "1"
			), 200);
		}

		/*register company */

		function company_register()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('company-register', $input_method);
			$this -> param_validate($input_method, array(
				"name",
				"company_name",
				"email",
				//"password",
				"phone"
			));

			$user_data = $this -> Customer_model -> check_exist_email_temp($input_method);
			if ($user_data['status'] == 0)
			{
				$this -> response(array(
					'message' => $user_data['message'],
					'status' => "0"
				), 200);
			}
			else
			{

				$unique_number = random_number();
				$input_method['varifycode'] = $unique_number;
				$auth_token = _generate_key();
				$input_method['auth_token'] = $auth_token;
				$companyuser_id = $this -> Customer_model -> save_userinfo_company($input_method);
				if ($companyuser_id)
				{
					//$user_name = USER_JID_NAME . '-' . $company_id;
					//$password = $input_method['password'];
					$email = $input_method['email'];
					//AccountPasswordSet($email, $companyuser_id, $unique_number);
					//CompanyAddNewUser($email);

					$name = $input_method['name'];
					$email = $input_method['email'];
					send_mail_to_admin($companyuser_id);
					$this -> response(array(
						'message' => COMPANY_ACCOUNT_CREATE,
						'status' => "1"
					), 200);
				}
				else
				{
					$this -> response(array(
						'message' => SERVER_ERROR,
						'status' => "0"
					), 200);
				}
			}

		}

		function company_register_new()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('company-register', $input_method);
			$this -> param_validate($input_method, array(
				"name",
				"company_name",
				"email",
				"password",
				"phone",
				"country_code"
			));

			$user_data = $this -> Customer_model -> check_exist_email_temp($input_method);
			if ($user_data['status'] == 0)
			{
				$this -> response(array(
					'message' => $user_data['message'],
					'status' => "0"
				), 200);
			}
			else
			{

				$unique_number = random_number();
				$input_method['varifycode'] = $unique_number;
				$email = $input_method['email'];
				$email_code = random_number();
				$input_method['email_code'] = $email_code;

				$company_userid = $this -> Customer_model -> SaveCompanyUser($input_method);
				if ($company_userid)
				{
					/*$user_name = USER_JID_NAME . '-' . $company_id;
					 $password = $input_method['password'];

					 $name = $input_method['name'];
					 $email = $input_method['email'];

					 $param = array(
					 $user_name,
					 $password,
					 $name,
					 $email
					 );
					 $this -> xmpp -> api("add", $param);*/

					/*send_mail_to_admin($company_id);*/
					//send OTP
					$country_code = $input_method['country_code'];
					$phone = $input_method['phone'];
					$phone_number = "+" . $country_code . $phone;
					$message = VARIFYSMS . $unique_number;
					send_sms($phone_number, $message);

					AccountVerifyMail($email, $company_userid, $email_code);
					$this -> response(array(
						'message' => COMPANY_ACCOUNT_CREATE,
						'status' => "1"
					), 200);
				}
				else
				{
					$this -> response(array(
						'message' => SERVER_ERROR,
						'status' => "0"
					), 200);
				}
			}

		}

		/*****************Fare Estimate  *********************************/

		function fare_estimate()
		{
			//$this -> load -> model('Bookingapi_model');
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('fare-estimate', $input_method);
			$this -> param_validate($input_method, array(
				"pickup_latitude",
				"pickup_longitude",
				"destination_latitude",
				"destination_longitude",
				"pickup_location",
				"destination_location",
				"cab_type"
			));
			$cab_type = $input_method['cab_type'];
			$isairportbooking = 0;
			$origins = $input_method['pickup_latitude'] . ',' . $input_method['pickup_longitude'];
			$destination = $input_method['destination_latitude'] . ',' . $input_method['destination_longitude'];

			//chk the pickup and destination loaction if its nearby Airport apply the fixed
			// price
			$isairportbooking = IsFixRateBooking($input_method);
			//print_r($isairportbooking);
			if ($isairportbooking['isairportbooking'] == 'Y')
			{

				$time_detail = gettimedetail($origins, $destination);
				if ($time_detail)
				{
					$duration = $time_detail['duration'];
					$distance = $time_detail['distance_m'];
					$duration_sec = $time_detail['duration_sec'];
				}
				$airport = $isairportbooking['airport'];
				//echo $cab_type;
				if ($cab_type == '1')
				{
					if ($airport == 1)
					{
						$billing_charge = FIXRATE_FIRST;
					}
					elseif ($airport == '2')
					{
						$billing_charge = FIXRATE_FIRST_SECONDAIRPORT;
					}
					elseif ($airport == '3')
					{

						$billing_charge = FIXRATE_FIRST_THIRDAIRPORT;
					}

				}
				else
				{
					if ($airport == 1)
					{
						$billing_charge = FIXRATE_LUXURY;
					}
					elseif ($airport == 2)
					{
						$billing_charge = FIXRATE_LUXURY_SECONDAIRPORT;
					}
					elseif ($airport == 3)
					{
						$billing_charge = FIXRATE_LUXURY_THIRDAIRPORT;
					}
				}
				$this -> response(array(
					"estimate_bill" => $billing_charge . " NOK",
					"duration" => $duration,
					"distance" => $distance,
					"pickup_location" => $input_method['pickup_location'],
					"destination_location" => $input_method['destination_location'],
					'status' => "1",
					"basic_charge" => "", //min
					"charge_per_min" => "",
					"charge_per_km" => "",
					"total_km_charge" => "",
					"total_time_charge" => "",
					//"min"=>$
				), 200);
			}
			else
			{
				$fare_detail = $this -> Bookingapi_model -> getfare_detail($cab_type);

				$time_detail = gettimedetail($origins, $destination);
				if ($time_detail)
				{
					$duration = $time_detail['duration'];
					$distance = $time_detail['distance_m'];
					$duration_sec = $time_detail['duration_sec'];
				}
				$fare_detail = $fare_detail[0];

				$min_charge = $fare_detail['min_charges'];
				$journey_charge_per_min = $fare_detail['wait_time_charges'];
				$km_drive = ($distance / 1000);
				if ($km_drive > 1)
				{
					$km_charge = $km_drive * $fare_detail['charges_per_km'];
				}
				else
				{
					$km_charge = $fare_detail['charges_per_km'];
				}
				$journey_time = ($duration_sec / 60);
				if ($journey_time > 1)
				{
					$journey_charge = $journey_time * $journey_charge_per_min;
				}
				else
				{

					$journey_charge = $journey_time * $journey_charge_per_min;
				}

				$journey_charge = ceil($journey_charge);
				$km_charge = ceil($km_charge);

				$billing_charge = $min_charge + $journey_charge + $km_charge;
				$km_charge = number_format($km_charge, 2, '.', '');
				$journey_charge = number_format($journey_charge, 2, '.', '');
				$billing_charge = number_format($billing_charge, 2, '.', '');

				$this -> response(array(
					"estimate_bill" => $billing_charge . " NOK",
					"duration" => $duration,
					"distance" => $km_drive,
					"pickup_location" => $input_method['pickup_location'],
					"destination_location" => $input_method['destination_location'],
					'status' => "1",
					"basic_fare" => $min_charge, //min
					"charge_per_min" => $journey_charge_per_min, //journey_charge_per
					"charge_per_km" => $fare_detail['charges_per_km'],
					"total_km_charge" => "$km_charge",
					"total_time_charge" => "$journey_charge", //journey_charge
					//"min"=>$
				), 200);
			}
		}

		//************************Last Panding Invoice Detail******************
		function invoice_detail()
		{
			$input_method = $this -> webservices_inputs();
			// $this->validate_param('invoice-detail', $input_method);
			$this -> param_validate($input_method, array("user_id"));
			$invoice = $this -> Customer_model -> invoice_detail($input_method);
			// echo "<pre>";
			// print_r($invoice);
			if ($invoice)
			{
				$invoice = $invoice[0];
				$booking_id = $invoice['booking_id'];
				$CBN = $invoice['booking_number'];
				$pickup_latitude = $invoice['pickup_latitude'];
				$pickup_longitude = $invoice['pickup_longitude'];
				$pickup_location = $invoice['pickup_location'];
				$destination_latitude = $invoice['destination_latitude'];
				$destination_longitude = $invoice['destination_longitude'];
				$destination_location = $invoice['destination_location'];
				$km_driven = $invoice['km_driven'];
				$km_driven_charge = $invoice['km_charge'];
				$journey_time = $invoice['journey_time'];
				$journey_charge = $invoice['waiting_charge'];
				$billing_charge = $invoice['billing_amount'];
				$basic_fare_charge = $invoice['basic_fare_charge'];
				$driver_id = $invoice['driver_id'];
				$driver_name = $invoice['first_name'] . ' ' . $invoice['last_name'];
				$driver_contact = $invoice['contact'];
				$car_number = $invoice['plate_number'];
				$driver_profile = $invoice['profile_pic'];
				$jid = makejid($invoice['user_id'], USER_JID_NAME);
				// $travel_charge = $billing_charge - $waiting_charge - $othercharge;
				$discount = $invoice['discount_amount'];
				$extra_km = $invoice['extra_km'];
				$extra_km_charge = $invoice['extra_km_charge'];
				$extra_time = $invoice['extra_time'];
				$extra_timecharge = $invoice['extra_timecharge'];
				$payment_mode = $invoice['payment_mode'];
				$booking_type = $invoice['booking_type'];
				$fix_amount = $invoice['fix_amount'];
				if ($fix_amount != '0.00')
				{
					$billing_charge = $fix_amount;
				}
				$this -> response(array(
					'booking_id' => $booking_id,
					"CBN" => $CBN,
					"pickup_latitude" => $pickup_latitude,
					"pickup_longitude" => $pickup_longitude,
					"pickup_location" => $pickup_location,
					"destination_latitude" => $destination_latitude,
					"destination_latitude" => $destination_latitude,
					"destination_longitude" => $destination_longitude,
					"destination_location" => $destination_location,
					"km_driven" => $km_driven,
					"km_driven_charge" => (string)$km_driven_charge,
					"journey_time" => $journey_time,
					"journey_time_charge" => $journey_charge,
					"basic_fare_charge" => $basic_fare_charge,
					"discount" => $discount,
					"billing_charge" => (string)$billing_charge, //number_format($billing_charge, 2),
					"customet_jid" => $jid,
					"driver_image" => base_url() . DRIVER_IMAGE_PATH . $driver_profile,
					"car_number" => $car_number,
					"driver_name" => $driver_name,
					"driver_id" => $driver_id,
					"driver_contact" => $driver_contact,
					"extra_timecharge" => $extra_timecharge,
					"extra_time" => $extra_time,
					"extra_km" => $extra_km,
					"extra_km_charge" => $extra_km_charge,
					"payment_mode" => $payment_mode,
					"booking_type" => $booking_type,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => NO_INVOICE,
					'status' => "0"
				), 200);
			}

		}

		function device_register()
		{
			$input_method = $this -> webservices_inputs();
			//$this->validate_param('device-register', $input_method);
			$this -> param_validate($input_method, array(
				"user_id",
				"device_type",
				"device_token"
			));
			$device = $this -> Customer_model -> device_register($input_method);
			if ($device)
			{
				$this -> response(array(
					'message' => SUCCESS,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}
		}

		function driver_rating()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"driver_id",
				"rating",
				"booking_id",
				"user_id",
			));
			$driver_rating = $this -> Customer_model -> driver_rating($input_method);
			if ($driver_rating)
			{
				$this -> response(array(
					'message' => SUCCESS,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}
		}

		function account_varify()
		{
			//$this -> load -> model('Resetpassword_model');
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"otp"
			));
			$user_detail = $this -> Customer_model -> account_varify($input_method);
			if ($user_detail)
			{
				$input_method['email'] = $user_detail['email'];
				$input_method['password'] = $user_detail['pass'];

				if ($user_detail['is_emailverify'] == "1")
				{

					//make this user verify
					$user_id = $this -> Resetpassword_model -> VerifyUser($user_detail['id']);
					$user_name = USER_JID_NAME . '-' . $user_id;
					if (@$user_detail['is_social'] == 'N')
					{
						$password = $user_detail['pass'];
						$password = trim($password);
						$is_social = "N";
						//$input_method['password']="";
					}
					else
					{
						$password = "123456";
						$password = trim($password);
						$is_social = "Y";
					}
					$name = $user_detail['name'];
					$email = $user_detail['email'];

					//make a Openfire user

					$user_data = $this -> Customer_model -> get_user_detail($input_method);
					if ($user_data)
					{
						$login = $user_data[0];
						if ($login -> is_company == 'Y' && $login -> is_active == '0')
						{
							$this -> response(array(
								'message' => NOT_APPROVE,
								'status' => "0"
							), 200);
						}
						$company_detail = $this -> Customer_model -> GetCompanyDetail($login -> user_id);
						//print_r($company_detail);
						if ($company_detail)
						{
							$company_name = ($company_detail['company_name']) ? $company_detail['company_name'] : $company_detail['usercompany'];
							$is_approved = $company_detail['is_approved'];
							$company_id = $company_detail['company_id'];
						}
						if ($company_id && $login -> credit_card_no)
						{
							$user_type = "B";
						}
						elseif ($company_id)
						{
							$user_type = "C";
						}
						elseif ($login -> credit_card_no)
						{
							$user_type = "I";
						}
						$header = getallheaders();
						//print_r($header);
						if (isset($header['os-type']) == 'IOS')
						{
							$customer_jid = makejid($login -> id, USER_JID_NAME);
						}
						else
						{
							$customer_jid = USER_JID_NAME . '-' . $login -> id;
						}
						$customer = array(
							"customer_jid" => $customer_jid,
							"customer_id" => $login -> user_id,
							"name" => $name = $login -> name,
							"email" => $email = $login -> email,
							"dob" => $dob = $login -> dob,
							"phone" => $login -> phone,
							"country_code" => $login -> country_code,
							"is_social" => $is_social,
							"is_mobile_varify" => "$login->is_varify",
							"credit_card" => "$login->credit_card_no",
							"is_emailverify" => ($login -> is_emailverify) ? "1" : $login -> is_emailverify,
							"iscompany_user" => ($login -> is_company == 'Y') ? "1" : "0",
							"is_active" => $login -> is_active,
							"mode" => $login -> mode,
							'company_name' => ($company_name) ? $company_name : "",
							"is_approved" => ($is_approved) ? $is_approved : "",
							"company_id" => ($company_id) ? $company_id : "",
							"user_type" => $user_type,
						);
					}
					else
					{
						$this -> response(array(
							'message' => SERVER_ERROR,
							'status' => "0"
						), 200);
					}
				}
				else
				{
					$customer = array();
				}

				$this -> response(array(
					'message' => ($user_detail['is_emailverify'] == "0") ? EMAIL_NOT_VERIFY : SUCCESS,
					"is_emailverify" => $user_detail['is_emailverify'],
					"customer" => $customer,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => VARIFYERROR,
					'status' => "0"
				), 200);
			}

		}

		function customer_not_responded()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"booking_id",
				"driver_id"
			));
			$booking_id = $input_method['booking_id'];
			$CBN = "PD" . sprintf("%05d", $booking_id);
			$makedriver_free = $this -> Customer_model -> customer_not_respond($input_method);
			if ($makedriver_free)
			{
				//VOID THE AUTH PAYMENT
				//	$type="Customer Not Responded";
				//VOIDAUTHTRANSACTION($input_method['booking_id'], $type);
				$makedriver_free = $makedriver_free[0];
				$customer_contact = $makedriver_free['country_code'] . $makedriver_free['phone'];
				$message = CUSTOMERTIMEOUTSMS . $CBN;
				//send_sms($customer_contact, $message);
				$this -> response(array(
					'message' => SUCCESS,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}
		}

		function ride_history()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"customer_id",
				"page_number"
			));
			$ride_history = $this -> Customer_model -> ride_history($input_method);
			//  print_r($ride_history);
			if ($ride_history)
			{
				foreach ($ride_history as $data)
				{
					$pickup_lat = $data -> pickup_latitude;
					$pickup_long = $data -> pickup_longitude;
					$source_data = $data -> pickup_location;
					$destination_lat = $data -> destination_latitude;
					$destination_long = $data -> destination_longitude;
					$destination = $data -> destination_location;
					$booking_date = $data -> booking_time;
					$CBN = $data -> booking_number;
					$booking_id = $data -> id;
					$driver_id = $data -> driver_id;
					$trackable = $this -> Customer_model -> driver_track($booking_id);
					$status = $data -> status;
					$time = time($booking_date);
					$bookdate = date('d-M-Y', strtotime($booking_date));
					$car_number = ($data -> plate_number == '') ? "" : $data -> plate_number;
					$booking_time = date('H:i A', strtotime($booking_date));

					$journey_time = $data -> journey_time;
					$cab_type = $data -> title;
					$ride_status = $data -> status;
					//$wait_time = $data->waiting_time;
					$billing_amount = $data -> billing_amount;
					$journey_charge = $data -> waiting_charge;
					$order_extra_charge = $data -> other_charge;
					$tip = $data -> tip;
					$total_pay = ($data -> user_paid == '') ? "" : $data -> user_paid;
					$is_paid = $data -> is_paid;
					$km_driven = $data -> km_driven;
					$km_driven_charge = $data -> km_charge;
					$discount = $data -> discount_amount;
					$basic_fare_charge = $data -> basic_fare_charge;

					$extra_time = ($data -> extra_time) ? $data -> extra_time : "";
					$extra_km = ($data -> extra_km) ? $data -> extra_km : "";
					$extra_tmcharge = ($data -> extra_timecharge) ? $data -> extra_timecharge : "";
					$extra_kmcharge = ($data -> extra_km_charge) ? $data -> extra_km_charge : "";

					if ($status == 'CUSTOMER_ACCEPTED' && $trackable == 'Y')
					{
						$trackable = "Y";
						$driver_detail = $this -> Customer_model -> get_driver_detail($driver_id);
						if ($driver_detail)
						{
							$driver_detail = $driver_detail[0];
							$origins = $driver_detail['latitude'] . ',' . $driver_detail['longitude'];
							$destination = $pickup_lat . ',' . $pickup_long;
							$time_detail = gettimedetail($origins, $destination);
							if ($time_detail)
							{
								$duration = $time_detail['duration'];
								$distance = $time_detail['distance'];
							}
							else
							{
								$duration = "";
								$distance = "";
							}
							$driver_data = array(
								"driver_id" => $driver_detail['driver_id'],
								"name" => $driver_detail['first_name'] . ' ' . $driver_detail['last_name'],
								"contact" => $driver_detail['contact'],
								"profile_image" => base_url() . DRIVER_IMAGE_PATH . $driver_detail['profile_pic'],
								"latitude" => $driver_detail['latitude'],
								"longitude" => $driver_detail['longitude'],
								"distance" => "$distance",
								"duration" => "$duration",
								"car_number" => $car_number,
								"CBN" => $CBN,
							);
							//exit;
						}
					}
					else
					if ($status == 'CUSTOMER_ACCEPTED' && $trackable == 'N')
					{
						continue;
					}

					else
					{
						// continue;
						$trackable = "N";
						$driver_data = array();

					}
					$display_status = "";
					$my_ride[] = array(
						'source_lat' => $pickup_lat,
						'source_lng' => $pickup_long,
						'source' => ($source_data == "") ? "" : $source_data,
						'destination' => ($destination == "") ? "" : $destination,
						'date' => $bookdate,
						'time' => $booking_time,
						'cab_type' => $cab_type,
						'journey_time' => "$journey_time",
						'journey_charge' => ($journey_charge == '') ? "" : $journey_charge,
						'CBN' => $CBN,
						'is_trackable' => $trackable,
						'booking_id' => "$booking_id",
						"status" => $status,
						"journey_distance" => ($km_driven == '') ? "" : $km_driven,
						"km_driven" => "$km_driven",
						"km_charge" => "$km_driven_charge",
						"basic_fare_charge" => "$basic_fare_charge",
						"travel_charge" => ($km_driven_charge == '') ? "" : (string )($km_driven_charge),
						"discount" => ($discount == "") ? "" : (string)($discount),
						"tip" => ($tip == '') ? "" : $tip,
						"billing_amount" => ($billing_amount == "") ? "" : $billing_amount,
						"total_pay" => ($total_pay == '') ? "" : $total_pay,
						"display_status" => $display_status,
						"car_number" => $car_number,
						"driver_detail" => $driver_data,
						"extra_km" => $extra_km,
						"extra_kmcharge" => $extra_kmcharge,
						"extra_time" => $extra_time,
						"extra_timecharge" => $extra_tmcharge,
					);

				}
				$this -> response(array(
					'status' => "1",
					'ride' => $my_ride
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "1",
					"message" => NO_RIDE_EXIST,
					'ride' => array()
				), 200);
			}
		}

		function upcoming_ride()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"customer_id",
				"page_number"
			));
			$upcoming_ride = $this -> Customer_model -> upcoming_ride($input_method);
			if ($upcoming_ride)
			{
				foreach ($upcoming_ride as $data)
				{
					$pickup_lat = $data -> pickup_latitude;
					$pickup_long = $data -> pickup_longitude;
					$source_data = $data -> pickup_location;
					$destination_lat = $data -> destination_latitude;
					$destination_long = $data -> destination_longitude;
					$destination = $data -> destination_location;
					$booking_date = $data -> booking_time;
					$CBN = $data -> booking_number;
					$booking_id = $data -> id;
					$driver_id = $data -> driver_id;
					$trackable = $this -> Customer_model -> driver_track($booking_id);
					$status = $data -> status;
					$time = time($booking_date);
					$cab_type = $data -> title;
					$bookdate = date('d-M-Y', strtotime($booking_date));
					$car_number = ($data -> plate_number == '') ? "" : $data -> plate_number;
					$booking_time = date('H:i A', strtotime($booking_date));
					$journey_time = "";
					/*$travel_charge = $ride_status = $wait_time = $waiting_change = $travel_charge =
					 $order_extra_charge = $tip = $km_driven = $total_pay = $is_paid = $cab_type =
					 "";

					 */
					$display_status = "";
					if ($trackable == "Y")
					{
						continue;
					}
					$my_ride[] = array(
						'source_lat' => $pickup_lat,
						'source_lng' => $pickup_long,
						'source' => ($source_data == "") ? "" : $source_data,
						'destination' => ($destination == "") ? "" : $destination,
						'date' => $bookdate,
						'time' => $booking_time,
						'cab_type' => $cab_type,
						'journey_time' => "$journey_time",
						'CBN' => $CBN,
						'booking_id' => "$booking_id",
						"status" => $status,
					);

				}
				$this -> response(array(
					'status' => "1",
					'ride' => $my_ride
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "1",
					"message" => NO_RIDE_EXIST,
					'ride' => array()
				), 200);
			}
		}

		function campaigns_detail()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array("user_id", ));
			$offer_detail = $this -> Customer_model -> current_offer($input_method);

			if ($offer_detail)
			{
				// user used coupon

				$used_coupon = $this -> Customer_model -> GetUsedCoupon($input_method['user_id']);
				$used_coupon = explode(',', $used_coupon);
				foreach ($offer_detail as $campaigns_detail)
				{
					if (in_array($campaigns_detail['code'], $used_coupon))
					{

						continue;
					}
					$coupon_detail[] = array(
						"title" => $campaigns_detail['title'],
						"offer" => "Rabatt kr. " . $campaigns_detail['offer'],
						"code" => $campaigns_detail['code'],
						"desc" => $campaigns_detail['description'],
						"start_date" => date('d-m-Y', strtotime($campaigns_detail['start_date'])),
						"expire_date" => date('d-m-Y', strtotime($campaigns_detail['expire_date']))
					);
				}
				if ($coupon_detail)
				{
					$this -> response(array(
						'status' => "1",
						"campaigns" => $coupon_detail,
					), 200);

				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => NO_OFFER_EXIST,
						'campaigns' => array()
					), 200);
				}
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => NO_OFFER_EXIST,
					'campaigns' => array()
				), 200);
			}
		}

		function update_profile_old()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"name",
				"country_code",
				"phone_number"
			));

			$update_profile = $this -> Customer_model -> update_profile($input_method);
			if ($update_profile)
			{

				$braintree_customer_id = $this -> Customer_model -> get_customer_braintree_id($input_method['user_id']);
				if ($braintree_customer_id)
				{
					$braintree_info['braintree_customer_id'] = $braintree_customer_id['braintree_customer_id'];
					$braintree_info['name'] = $input_method['name'];
					$braintree_info['phone'] = '+' . $input_method['country_code'] . $input_method['phone_number'];
					$braintree_response = UpdateExistingCustomerDetail($braintree_info);
					if ($braintree_response['status'] == 1)
					{
						$this -> response(array(
							'status' => "1",
							"message" => PROFILE_UPDATE,
							"name" => $input_method['name'],
							"country_code" => $input_method['country_code'],
							"phone_number" => $input_method['phone_number']
						), 200);
					}
					else
					{
						$this -> response(array(
							'status' => "0",
							"message" => $braintree_response['message'],
							'campaigns' => array()
						), 200);
					}
				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => "Braintree customer id not get",
						'campaigns' => array()
					), 200);
				}
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => SERVER_ERROR,
					'campaigns' => array()
				), 200);
			}
		}

		function update_profile()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"name",
				"country_code",
				"phone_number"
			));
			//chk mobile number
			$contact_detail = $this -> Customer_model -> CheckContactDetail($input_method);
			if ($contact_detail)
			{
				if ($input_method['user_id'] == $contact_detail['id'])
				{
					$update_profile = $this -> Customer_model -> update_profile($input_method);
					if ($update_profile)
					{
						$this -> response(array(
							'status' => "1",
							"message" => PROFILE_UPDATE,
							"name" => $input_method['name'],
							"country_code" => $input_method['country_code'],
							"phone_number" => $input_method['phone_number'],
							"mobile_verification_required" => "N"
						), 200);
					}
					else
					{
						$this -> response(array(
							'status' => "0",
							"message" => SERVER_ERROR,
							'campaigns' => array()
						), 200);
					}
				}
				else
				{
					$phone = $input_method['country_code'] . $input_method['phone_number'];
					$error_msg = str_replace('<mobile>', $phone, MOBILE_ALREADY_REGISTER);
					$this -> response(array(
						'status' => "0",
						"message" => $error_msg,
						'campaigns' => array()
					), 200);
				}
			}
			else
			{
				$update_profile = $this -> Customer_model -> update_profile($input_method);
				if ($update_profile)
				{
					//send OTP
					$unique_number = random_number();
					$input_method['verifycode'] = $unique_number;
					$country_code = '+' . $input_method['country_code'];
					$phone = $input_method['phone_number'];
					/*this for enter resned otp into table 19 oct*/
					$otp_resend = $this -> Customer_model -> resend_otp($input_method);
					$message = VARIFYSMS . $unique_number;
					$to = $country_code . $phone;
					send_sms($to, $message);
					$this -> response(array(
						'status' => "1",
						"message" => PROFILE_UPDATE,
						"name" => $input_method['name'],
						"country_code" => $input_method['country_code'],
						"phone_number" => $input_method['phone_number'],
						"mobile_verification_required" => "Y"
					), 200);

				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => SERVER_ERROR,
						'campaigns' => array()
					), 200);
				}
			}

		}

		function change_password()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"old_password",
				"new_password"
			));
			$password_update = $this -> Customer_model -> change_password($input_method);
			//print_r($password_update);
			if ($password_update['status'] == 1)
			{
				$this -> response(array(
					'status' => "1",
					"message" => PASSWORD_SUCCESS,
				), 200);
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => $password_update['message'],
				), 200);
			}
		}

		function update_cc_detail_old()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"credit_card_no",
				"expire_date",
				"cvv_no"
			));

			$customer_detail = $this -> Customer_model -> get_customer_braintree_id($input_method['user_id']);

			if ($customer_detail)
			{
				$braintree_info['braintree_customer_id'] = $customer_detail['braintree_customer_id'];
				$braintree_info['name'] = $customer_detail['name'];
				$braintree_info['phone'] = $customer_detail['country_code'] . $customer_detail['phone'];
				$braintree_info['cvv_no'] = $input_method['cvv_no'];
				$braintree_info['expire_date'] = $input_method['expire_date'];
				$braintree_info['credit_card_no'] = $input_method['credit_card_no'];
				$braintree_info['token'] = $customer_detail['token'];

				$braintree_response = Update_withUpdatingExistingCreditCard($braintree_info);
				//  print_r($braintree_response);
				if ($braintree_response['status'] == 1)
				{

					//update cc number in DB
					$update = $this -> Customer_model -> update_cc_info($input_method);
					if ($update)
					{
						$this -> response(array(
							'status' => "1",
							"message" => CC_SUCCESS
						), 200);
					}
					else
					{
						$this -> response(array(
							'status' => "0",
							"message" => SERVER_ERROR,
						), 200);
					}
				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => $braintree_response['message'],
					), 200);
				}
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => "Braintree customer id not get",
				), 200);
			}
			//Update_withUpdatingExistingCreditCard($data);
		}

		function update_cc_detail()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"nonce"
			));

			$customer_detail = $this -> Customer_model -> get_customer_braintree_id($input_method['user_id']);

			if ($customer_detail)
			{
				$braintree_info['braintree_customer_id'] = $customer_detail['braintree_customer_id'];
				$braintree_info['nonce'] = $input_method['nonce'];
				$braintree_info['token'] = $customer_detail['token'];

				//$braintree_response = Update_withUpdatingExistingCreditCard($braintree_info);
				//  print_r($braintree_response);
				$braintree_response = UpdateUserCard($braintree_info);
				if ($braintree_response['status'] == 1)
				{

					//update cc number in DB
					$braintree_response['user_id'] = $input_method['user_id'];
					$update = $this -> Customer_model -> update_cc_info($braintree_response);
					if ($update)
					{
						$this -> response(array(
							'status' => "1",
							"message" => CC_SUCCESS,
							"credit_card" => credit_card_number($braintree_response['credit_card_no'])
						), 200);
					}
					else
					{
						$this -> response(array(
							'status' => "0",
							"message" => SERVER_ERROR,
						), 200);
					}
				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => $braintree_response['message'],
					), 200);
				}
			}
			else
			{
				/*$this -> response(array(
				 'status' => "0",
				 "message" => "Braintree customer id not get",
				 ), 200);*/
				$UserData = $this -> Customer_model -> GetUserData($input_method['user_id']);

				$data = array(
					"name" => $UserData['name'],
					"email" => $UserData['email'],
					"phone" => $UserData['country_code'] . $UserData['phone'],
					"nonce" => $input_method['nonce'],
				);
				if ($credit_card_no != '4111111111111112')
				{
					//$braintree_response = Create_withCreditCardAndVerification($data);
					$braintree_response = CreateCustomer($data);
					//print_r($braintree_response);

				}
				else
				{
					$braintree_response['customer_id'] = '123';
					$braintree_response['token'] = '123xyz';
					$braintree_response['cardtype'] = 'test';
					$braintree_response['status'] = '1';
				}
				if ($braintree_response['status'] == "1")
				{

					//save CC detail
					$braintree_response['user_id'] = $input_method['user_id'];
					$SaveCC = $this -> Customer_model -> SaveCCDetail($braintree_response);
					if ($SaveCC)
					{
						$this -> response(array(
							'message' => SUCCESS,
							"credit_card" => credit_card_number($braintree_response['credit_card_no']),
							'status' => "1"
						), 200);
					}
					else
					{
						$this -> response(array(
							'message' => SERVER_ERROR,
							'status' => "0"
						), 200);
					}
				}
				else
				{
					$this -> response(array(
						'message' => $braintree_response['message'],
						'status' => "0"
					), 200);
				}

			}
			//Update_withUpdatingExistingCreditCard($data);
		}

		function apply_coupon()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"coupon_code",
			));
			$is_coupon_exist = $this -> Customer_model -> coupon_exist($input_method);
			if ($is_coupon_exist)
			{
				$coupon_detail = $this -> Customer_model -> get_coupon_detail($input_method);
				if ($coupon_detail)
				{
					//print_r($coupon_detail);
					$coupon_detail = $coupon_detail[0];
					$coupon_info = array(
						"title" => $coupon_detail['title'],
						"offer" => "Rabatt kr. " . $coupon_detail['offer'],
						"desc" => $coupon_detail['description'],
						"start_date" => date('d-m-Y', strtotime($coupon_detail['start_date'])),
						"expire_date" => date('d-m-Y', strtotime($coupon_detail['expire_date']))
					);

					$this -> response(array(
						'status' => "1",
						"message" => SUCCESS,
						"coupon_detail" => $coupon_info
					), 200);

				}
				else
				{
					$this -> response(array(
						'status' => "0",
						"message" => COUPON_ALREADY_USED,
					), 200);
				}
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => COUPON_NOT_VAILD,
				), 200);
			}
		}

		function resend_otp()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array("user_id"));
			$otp_send = $this -> Customer_model -> get_otp($input_method);
			if ($otp_send)
			{
				$user_detail = $otp_send[0];
				if ($user_detail['is_varify'] == "1")
				{
					$this -> response(array(
						'status' => "0",
						"message" => ACCOUNT_VERIFY,
					), 200);
				}
				else
				{
					/*chnage made 19 oct resend otp is not generate */
					$unique_number = random_number();
					$input_method['verifycode'] = $unique_number;
					$country_code = $user_detail['country_code'];
					$phone = $user_detail['phone'];
					/*this for enter resned otp into table 19 oct*/
					$otp_resend = $this -> Customer_model -> resend_otp($input_method);
					$message = VARIFYSMS . $unique_number;
					$to = $country_code . $phone;
					send_sms($to, $message);

					$this -> response(array(
						'status' => "1",
						"message" => OTP_RESET_CODE,
					), 200);
				}
			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => USER_NOT_EXIST,
				), 200);
			}
		}

		function get_customer_birthday()
		{
			// echo 'hello';
			$date = date('Y-m-d');
			$get_customer = $this -> General_model -> Fetch_customer_birthday($date);

			if ($get_customer)
			{

				$html = '<table style="font-family:Trebuchet MS, Arial, Helvetica, sans-serif;width:50%;border-collapse: collapse">
                                        <thead>
                                            <tr>
                                                <th style="font-size: 1.1em;text-align:left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942; color: #ffffff;">
																				<center>Name</center></th>
                                                
                                                <th style="font-size: 1.1em;text-align:left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942; color: #ffffff;"><center>Email</center></th>
                                                <th style="font-size: 1.1em;text-align:left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942; color: #ffffff;"><center>contact</center></th>
                                                 </tr>	';

				foreach ($get_customer as $data)
				{

					$html .= '<tr style=" color:"#000000;background-color: #EAF2D3;">
		      <td style=" font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;">
	' . urldecode($data['name']) . '
			  </td>
			  <td style=" font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;">
	' . $data['email'] . '
			  </td>
			   <td style=" font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;">
' . $data['phone'] . '
			  </td>
			  </tr>';

				}
				$html .= "  </table> <p>Best Regards With</p>
			<p>Private Driver A/S<p> ";

				$email = ADMINMAIL;
				$config['protocol'] = 'smtp';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';

			$config['smtp_host'] = EMAIL_HOST;
			$config['smtp_user'] = EMAIL_USERNAME;
			$config['smtp_pass'] = EMAIL_PASSWORD;
			$config['smtp_port'] = EMAIL_PORT;
			$config['email_smtp_crypto'] = 'tls';
			$config['email_newline'] = '\r\n';

			
			$this -> email -> clear();
			$this -> email -> initialize($config);
			$ci -> email -> set_newline("\r\n");	
				$this -> email -> from(ADMINMAIL_FROM, 'Private Driver');
				$this -> email -> to($email);

				//$ci -> email -> subject($subject);
				$this -> email -> subject('Customer Birthday Report');
				$this -> email -> message($html);
				$this -> email -> send();

			}

		}

		/*get notification */
		function get_notification()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"device_type",
				"page"
			));
			$limit = 20;
			$offset = ($input_method['page'] - 1) * $limit;
			$input_method['limit'] = $limit;
			$input_method['offset'] = $offset;
			$notify = $this -> Customer_model -> get_notification($input_method);

			if ($notify)
			{
				foreach ($notify as $notifydata)
				{
					$date = gmt_to_norway($notifydata['createdon']);
					$notification[] = array(
						"notification" => $notifydata['message'],
						"notification_date" => $date
					);
				}

				$this -> response(array(
					'status' => "1",
					"notification" => $notification,
				), 200);

			}
			else
			{
				$this -> response(array(
					'status' => "0",
					"message" => NOANY_NOTIFICATION,
				), 200);
			}
		}

		function mobile_verify()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"otp",
				"country_code",
				"phone_number"
			));
			$account_varify = $this -> Customer_model -> mobile_verify($input_method);
			if ($account_varify)
			{
				$this -> response(array(
					'message' => MOBILE_NUMBER_VERIFIED,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => VARIFYERROR,
					'status' => "0"
				), 200);
			}

		}

		function SwitchAccount()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"mode",
			));
			$account_switch = $this -> Customer_model -> SwitchAccount($input_method);
			if ($account_switch)
			{
				//get user detail
				$user_detail = $this -> Customer_model -> get_user($input_method['user_id']);
				//print_r($user_detail);
				$is_approved = "N";
				$company_id = $company_name = "";
				//$user_id = $login -> id;
				//get user company detail
				$company_detail = $this -> Customer_model -> GetCompanyDetail($user_id);
				//print_r($company_detail);
				if ($company_detail)
				{
					$company_name = ($company_detail['company_name']) ? $company_detail['company_name'] : $company_detail['usercompany'];
					$is_approved = $company_detail['is_approved'];
					$company_id = $company_detail['company_id'];
				}
				$user_detail[0] -> is_approved = $is_approved;
				$user_detail[0] -> company_name = $company_name;
				$user_detail[0] -> company_id = $company_id;

				$this -> response(array(

					'customer' => $user_detail[0],
					'status' => "1"
				), 200);

			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}
		}

		function AddCompany()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"company_name",
			));
			$comapny_account = $this -> Customer_model -> AddCompany($input_method);
			if ($comapny_account)
			{
				send_mail_to_admin($input_method['user_id']);
				$this -> response(array(
					'message' => COMPANY_ADDED,
					'status' => "1"
				), 200);

			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}

		}

		function DeleteCompany()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"company_id",
			));

			$delete_company = $this -> Customer_model -> DeleteComapny($input_method);
			if ($delete_company)
			{
				$this -> response(array(
					'message' => COMPANY_DELETE,
					'status' => "1"
				), 200);

			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}
		}

		function BraintreeClientId()
		{
			$BtClientID = BraintreeTokan();
			if ($BtClientID)
			{
				$this -> response(array(
					'client_id' => $BtClientID,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}

		}

		function AddCreditCard()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"nonce",
			));
			//chk if card already add
			$CC = $this -> Customer_model -> CheckCCDetail($input_method['user_id']);
			if ($CC)
			{
				$this -> response(array(
					'message' => CC_ALREADY_ADD,
					'status' => "0"
				), 200);
			}
			else
			{
				//get user detail
				$UserData = $this -> Customer_model -> GetUserData($input_method['user_id']);

				$data = array(
					"name" => $UserData['name'],
					"email" => $UserData['email'],
					"phone" => $UserData['country_code'] . $UserData['phone'],
					"nonce" => $input_method['nonce'],
				);
				if ($credit_card_no != '4111111111111112')
				{
					//$braintree_response = Create_withCreditCardAndVerification($data);
					$braintree_response = CreateCustomer($data);
					//print_r($braintree_response);

				}
				else
				{
					$braintree_response['customer_id'] = '123';
					$braintree_response['token'] = '123xyz';
					$braintree_response['cardtype'] = 'test';
					$braintree_response['status'] = '1';
				}
				if ($braintree_response['status'] == "1")
				{

					//save CC detail
					$braintree_response['user_id'] = $input_method['user_id'];
					$SaveCC = $this -> Customer_model -> SaveCCDetail($braintree_response);
					if ($SaveCC)
					{
						$this -> response(array(
							'message' => SUCCESS,
							"credit_card" => credit_card_number($braintree_response['credit_card_no']),
							'status' => "1"
						), 200);
					}
					else
					{
						$this -> response(array(
							'message' => SERVER_ERROR,
							'status' => "0"
						), 200);
					}
				}
				else
				{
					$this -> response(array(
						'message' => $braintree_response['message'],
						'status' => "0"
					), 200);
				}

			}
		}

		function favourite_location()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"latitude",
				"longitude",
				//"placename",
				"address",
				"custom_name"
			));

			$AddFavLocation = $this -> Customer_model -> SaveFavLocation($input_method);
			if ($AddFavLocation)
			{
				$this -> response(array(
					'message' => LOCATION_ADDED,
					'status' => "1",
					"location_id" => $AddFavLocation
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}

		}

		function get_favourite_location_old()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array("user_id", ));
			$favLocation = $this -> Customer_model -> GetFavLocation($input_method['user_id']);
			if ($favLocation)
			{
				$this -> response(array(
					'location' => $favLocation,
					'status' => "1"
				), 200);
			}
			else

			{
				$this -> response(array(
					'message' => NO_ANY_FAV_LOACTION,
					'status' => "0"
				), 200);
			}
		}

		function get_favourite_location()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array("user_id", ));
			$customFavLocation = $this -> Customer_model -> GetCustomFavLocation($input_method['user_id']);
			if ($customFavLocation)
			{

				foreach ($customFavLocation as $location)
				{
					extract($location);
					$favplace[] = array(
						"latitude" => $latitude,
						"longitude" => $longitude,
						"placename" => ($placename) ? $placename : "",
						"location" => $address,
						"custom_name" => $custom_name,
						"location_type" => $location_type,
						"id" => $id
					);
				}
			}
			$favLocation = $this -> Customer_model -> GetFavLocation($input_method['user_id']);
			if ($favLocation)
			{
				foreach ($favLocation as $location)
				{
					extract($location);
					$favplace[] = array(
						"latitude" => $pickup_latitude,
						"longitude" => $pickup_longitude,
						"placename" => ($pickup_placename) ? $pickup_placename : "",
						"location" => $pickup_location,
						"custom_name" => "",
						"location_type" => "sublocality",
						"id" => ""
					);
					$favplace[] = array(
						"latitude" => $destination_latitude,
						"longitude" => $destination_longitude,
						"placename" => ($destination_placename) ? $destination_placename : "",
						"location" => $destination_location,
						"custom_name" => "",
						"location_type" => "sublocality",
						"id" => "",
					);

				}

				$this -> response(array(
					'location' => $favplace,
					'status' => "1"
				), 200);
			}
			else

			{
				$this -> response(array(
					'message' => NO_ANY_FAV_LOACTION,
					'status' => "0"
				), 200);
			}
		}

		function Reciept_Setting()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"reciept_enable"
			));
			$setRecipt = $this -> Customer_model -> RecieptSetting($input_method);
			if ($setRecipt)
			{
				$this -> response(array(
					'location' => SUCCESS,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'location' => SERVER_ERROR,
					'status' => "0"
				), 200);
			}
		}

		function delete_favourite_location()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"user_id",
				"location_id"
			));
			$this -> Customer_model -> DeleteFavLocation($input_method);
			$this -> response(array(
				'message' => SUCCESS,
				'status' => "1"
			), 200);
		}

		function testmail()
		{
			error_reporting(1);
			$ci = &get_instance();
			$ci -> load -> library('email');
			$ci -> load -> database();

			$cipher = $ci -> load -> library('Cipher');
			echo $userid = 1;
			echo $encrypteduserid = $cipher -> encrypt($userid);
			echo "dfcd" . $encryptedcode = $cipher -> encrypt($encrypteduserid);
			exit ;
			$config['protocol'] = 'smtp';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';

			$config['smtp_host'] = EMAIL_HOST;
			$config['smtp_user'] = EMAIL_USERNAME;
			$config['smtp_pass'] = EMAIL_PASSWORD;
			$config['smtp_port'] = EMAIL_PORT;
			$config['email_smtp_crypto'] = 'tls';
			$config['email_newline'] = '\r\n';
			print_r($config);
			$subject = "Test mail";
			$ci -> email -> initialize($config);
			$ci -> email -> set_newline("\r\n");
			$ci -> email -> from(ADMINMAIL_FROM, 'Private Driver App');
			$ci -> email -> to("priyanka@foxinfosoft.com");
			$ci -> email -> reply_to(ADMINMAIL, 'Private Driver App');
			$ci -> email -> cc("priyanka@foxinfosoft.com");
			$ci -> email -> subject($subject);
			$ci -> email -> message($mail_body);
			if ($ci -> email -> send())
			{
				echo "1";
			}
			else
			{
				show_error($ci -> email -> print_debugger());
			}
		}

	}
