<?php

	class Restaurantapi_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		/* customer already register */
		function check_exist_email($input_method)
		{
			// $email1 = '';
			if (isset($input_method['email']))
				$email = $input_method['email'];
			$query1 = "SELECT id,email from restaurant where email='$email'";
			$query = $this -> db -> query($query1);
			if ($query -> num_rows())
			{
				return $query -> num_rows();
			}
			else
			{
				return false;
			}

		}

		/* native login user */
		function check_login($input_method)
		{
			$email = $input_method['email'];
			$password = $input_method['password'];
			$where_condition = array(
				"email" => $email,
				"password" => md5($password)
			);
			$query = $this -> db -> get_where("restaurant", $where_condition);
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function restro_exist($restro_id)
		{
			$where_condition = array(
				"id" => $restro_id,
				"isactive" => "1"
			);
			$query = $this -> db -> get_where("restaurant", $where_condition);
			if ($query -> num_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function nearby_booking_serach($input_method)
		{
			$lat = $input_method['pickup_latitude'];
			$lng = $input_method['pickup_longitude'];
			$userid = $input_method['restro_id'];
			$pickup_location = $input_method['pickup_location'];

			$type = $input_method['cab_type'];
			$service_area = $input_method['area'];
			//********************get all available driver*****************************<br />

			$query = $this -> db -> query("select * from (SELECT d.driver_id,d.islogin,concat(d.first_name,' ',d.last_name)as name,d.status,d.latitude,d.longitude,d.email_id,d.contact,d.profile_pic,
        ct.type_id,ct.title,c.cab_plate_no,( 6371 * acos( cos( radians($lat) ) 
        * cos( radians( d.latitude ) ) 
        * cos( radians( d.longitude ) - radians($lng) ) 
        + sin( radians($lat) ) 
        * sin( radians( d.latitude ) ) ) ) AS distance 
        FROM driver d
        join shift s on d.driver_id=s.driver_id and s.status='STARTED'
        join cab c on c.cab_id=s.cab_id and c.service_area=$service_area
        join cab_type ct on ct.type_id=c.cab_type )sub
        where sub.distance < 100 AND (sub.status = '" . DRIVER_AVAILABLE . "' or sub.status = '" . DRIVER_BUSY . "') and sub.islogin='Y' and sub.type_id='" . $type . "'
        ORDER BY distance ");
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$result = $query -> result_array();
			}
			if (count(@$result))
			{
				$cab_detail = $result;
			}

			else
			{
				return false;
			}
			////echo "<pre>";
			// print_r($cab_detail);
			return array("driver_info" => $cab_detail);
		}

		function restro_cab_booking($input_method)
		{
			$insert_data = array(
				"restro_id" => $input_method['restro_id'],
				"client_name" => $input_method['client_name'],
				// "client_email" => $input_method['client_email'],
				"client_contact_number" => $input_method['client_contact_number'],
				"country_code" => "+" . $input_method['country_code'],
				"pickup_latitude" => $input_method['pickup_latitude'],
				"pickup_longitude" => $input_method['pickup_longitude'],
				"pickup_location" => $input_method['pickup_location'],
				"booking_time" => get_gmt_time(),
				"cab_type" => $input_method['cab_type'],
				"area" => $input_method['area']
			);
			$query = $this -> db -> insert("restro_cab_booking", $insert_data);
			//echo $this->db->last_query();
			// print_r($query);
			if ($this -> db -> insert_id())
			{
				$booking_id = $this -> db -> insert_id();
				$CBN = "PD" . sprintf("%05d", $booking_id);
				$this -> db -> query("update restro_cab_booking set booking_number='" . $CBN . "' where id=$booking_id");
				//  echo $this->db->last_query();
				return $booking_id;
				$user_detail = $query -> insert_id();
			}
			else
			{
				return false;
			}

		}

		function get_booking_detail($booking_id)
		{
			//  $booking_id = $input_method['booking_id'];
			$this -> db -> select("restaurant.name,restaurant.id as restro_id,restaurant.address,restaurant.latitude,restaurant.longitude,restaurant.contact_number,restro_cab_booking.booking_number,restro_cab_booking.client_name,restro_cab_booking.country_code,restro_cab_booking.client_contact_number,restro_cab_booking.driver_id,restro_cab_booking.cab_type,restro_cab_booking.id as booking_id");
			$this -> db -> from("restaurant");
			$this -> db -> join("restro_cab_booking", "restro_cab_booking.restro_id=restaurant.id");
			$this -> db -> where("restro_cab_booking.id", $booking_id);
			$query = $this -> db -> get();
			if ($query -> num_rows() > 0)
			{
				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function timeout($booking_id)
		{

			$data = array("status" => "TIMEOUT");
			$this -> db -> where("id", $booking_id);
			$update = $this -> db -> update("restro_cab_booking", $data);
			if ($update)
			{
				return true;
			}
			else
			{
				return false;
			}

		}

		function booking_status($input_method)
		{
			$booking = $input_method['booking_id'];
			$driver_id = $input_method['driver_id'];
			$this -> db -> select('status');
			$booking_res = $this -> db -> get_where('restro_cab_booking', array("id" => $booking));
			//echo $this->db->last_query();
			if ($booking_res -> num_rows() > 0)
			{
				$booking_status_data = $booking_res -> result_array();
				$booking_status_data = $booking_status_data[0];
				$booking_status = $booking_status_data['status'];
				if ($booking_status == 'WAITING')
				{
					$status = "WAITING";

					//chk driver rejected or any thing response for this booking
					$this -> db -> select('driver_response');
					$driver_response = $this -> db -> get_where('cab_booking_history', array(
						"booking_id " => $booking,
						"driver_id" => $driver_id,
						"booking_type" => "RESTRO"
					));

					if ($driver_response -> num_rows() > 0)
					{
						$driver_response_data = $driver_response -> result_array();
						$driver_response_data = $driver_response_data[0];
						$status = $driver_response_data['driver_response'];
						return $status;
					}
					return $status;
				}
				else
				{
					return $status = $booking_status;

				}
				//$query="select status from cab_booking where id=$id";
			}
			else
			{
				return $status = BOOKING_NOTFOUND;
			}
		}

		function nearby_serach($input_method)
		{
			$lat = $input_method['pickup_latitude'];
			$lng = $input_method['pickup_longitude'];
			$userid = $input_method['user_id'];
			$type = $input_method['cab_type'];

			//get all available driver
			$query = $this -> db -> query("select * from (SELECT d.driver_id,concat(d.first_name,' ',d.last_name)as name,s.status,d.latitude,d.longitude,d.email_id,d.contact,d.profile_pic,
         ct.type_id,ct.title,c.cab_plate_no,( 6371 * acos( cos( radians($lat) ) 
        * cos( radians( d.latitude ) ) 
        * cos( radians( d.longitude ) - radians($lng) ) 
        + sin( radians($lat) ) 
        * sin( radians( d.latitude ) ) ) ) AS distance 
        FROM driver d
       join shift s on d.driver_id=s.driver_id and s.status='STARTED' join cab c on c.cab_id=s.cab_id join cab_type ct on ct.type_id=c.cab_type )sub
        where sub.distance < 50 and sub.type_id='" . $type . "'
        ORDER BY distance ");
			// echo  $this->db->last_query();
			//  exit;
			if ($query -> num_rows() > 0)
			{
				return $result = $query -> result_array();
			}
			else
			{
				return false;
			}

		}

		function driver_response($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$booking_id = $input_method['booking_id'];
			$type = $input_method['type'];
			if ($type == 'accept')
			{
				/*****************************************************
				 //save in history table and update booking table
				 /*****************************************************/

				$date = get_gmt_time();
				$data = array(
					'booking_id' => $booking_id,
					'booking_type' => "RESTRO",
					'driver_id' => $driver_id,
					'driver_response' => "DRIVER_ACCEPTED",
					'driver_datetime' => $date,
				);
				$str = $this -> db -> insert('cab_booking_history', $data);

				if ($this -> db -> insert_id())
				{

					//***********CHK BOOKING EXIST OR NOT *************************

					$this -> db -> select("status,id");
					$this -> db -> where("id", $booking_id);
					$this -> db -> where("status", "WAITING");
					$booking_status = $this -> db -> get("restro_cab_booking");
					if (!$booking_status -> num_rows())
					{
						/*****************************************************
						 //update driver status
						 /*****************************************************/
						$driver_available = DRIVER_AVAILABLE;
						$data = array('status' => "$driver_available");
						$this -> db -> where('driver_id', $driver_id);
						$update = $this -> db -> update('driver', $data);

						$result = array(
							"type" => $type,
							"status" => 0,
							"message" => BOOKING_CANCEL
						);
						return $result;
						//return false;
					}

					/*****************************************************
					 //Assign driver for booking and set the cab plate number
					 /*****************************************************/

					$plate_number = $this -> get_cab_plate_number($driver_id);
					$driver_accepted = DRIVER_ACCEPTED;
					$data = array(
						'driver_id' => $driver_id,
						'status' => "$driver_accepted",
						"plate_number" => $plate_number,
						'driver_assigned_date' => $date
					);

					$this -> db -> where('id', $booking_id);
					$update = $this -> db -> update('restro_cab_booking', $data);

					//  echo "<br/>".$this->db->last_query();
					if ($update)
					{
						/*****************************************************
						 //update driver status
						 /*****************************************************/

						$data = array('status' => "RESTRO_BUSY");
						$this -> db -> where('driver_id', $driver_id);
						$update = $this -> db -> update('driver', $data);

						/*****************************************************
						 //get user(Customer)detail
						 /*****************************************************/

						$this -> db -> select("restro_cab_booking.*,driver.*,cab.cab_plate_no,cab_type.title,restaurant.contact_number");
						$this -> db -> from("restro_cab_booking");
						$this -> db -> join('driver', 'driver.driver_id = restro_cab_booking.driver_id');
						$this -> db -> join('shift', 'shift.driver_id=restro_cab_booking.driver_id');
						$this -> db -> join('cab', 'cab.cab_id=shift.cab_id');
						$this -> db -> join('cab_type', 'cab_type.type_id=cab.cab_type');
						$this -> db -> join("restaurant", "restaurant.id=restro_cab_booking.restro_id");
						$this -> db -> where("restro_cab_booking.id", $booking_id);
						$query = $this -> db -> get();
						//echo "<br/>".$this->db->last_query();
						if ($query -> num_rows() > 0)
						{

							$driver_detail = $query -> result_array();
							$result = array(
								"type" => $type,
								"driver_detail" => $driver_detail,
								"status" => 1
							);
							return $result;
						}
						else
						{
							$result = array(
								"type" => $type,
								"status" => 0,
								"message" => DRIVER_BUSY_MESSAGE
							);
							return $result;
							//return false;
						}

					}
					else
					{
						$result = array(
							"type" => $type,
							"status" => 0,
							"message" => SERVER_ERROR
						);
						return $result;
						//return false;
					}

				}
				else
				{
					$result = array(
						"type" => $type,
						"status" => 0,
						"message" => SERVER_ERROR
					);
					return $result;
				}
			}
			elseif ($type == 'reject')
			{
				//make driver Availible after reject Booking
				$query = $this -> db -> query("SELECT `id` FROM `cab_booking` WHERE `driver_id` = $driver_id 
                                    AND (`status` = 'DRIVER_ACCEPTED' 
                                    OR `status` = 'CUSTOMER_ACCEPTED'
                                    OR `status` = 'TRIP_STARTED')LIMIT 1");
				// echo $this->db->last_query();
				if ($query -> num_rows() > 0)
				{
					$driver_status = "BUSY";
				}
				else
				{
					$driver_status = DRIVER_AVAILABLE;
				}
				//$driver_available = DRIVER_AVAILABLE;
				$data = array('status' => $driver_status);
				$this -> db -> where('driver_id', $driver_id);
				$update = $this -> db -> update('driver', $data);

				$date = get_gmt_time();
				$driver_rejected = DRIVER_REJECTED;
				$data = array(
					'booking_id' => $booking_id,
					'booking_type' => "RESTRO",
					'driver_id' => $driver_id,
					'driver_response' => "$driver_rejected",
					'driver_datetime' => $date,
				);
				$str = $this -> db -> insert('cab_booking_history', $data);

				if ($id = $this -> db -> insert_id())
				{

					$result = array(
						"type" => $type,
						"status" => 1,
						"message" => "success"
					);
					return $result;
				}
				else
				{
					$result = array(
						"type" => $type,
						"status" => 0,
						"message" => SERVER_ERROR
					);
					return $result;
				}
			}

		}

		/* function timeout($input_method)
		 {
		 $booking_id = $input_method['booking_id'];
		 $data = array("status" => "TIMEOUT");
		 $this->db->where("id", $booking_id);
		 $update = $this->db->update("restro_cab_booking", $data);
		 // $this->db->last_query();
		 if ($update)
		 {
		 return true;
		 }
		 else
		 {
		 return false;
		 }

		 }*/
		function get_cab_plate_number($driver_id)
		{
			$shift_start = DRIVER_SHIFT_START;
			$this -> db -> select("cab_plate_no");
			$this -> db -> from("shift");
			$this -> db -> join("cab", "cab.cab_id=shift.cab_id");
			$this -> db -> where("shift.status", "$shift_start");
			$this -> db -> where("shift.driver_id", $driver_id);
			$query = $this -> db -> get();
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$data = $query -> result_array();
				return $data[0]['cab_plate_no'];
			}
			else
			{
				return "";
			}
		}

		function restro_fare_detail()
		{
			$fare = $this -> db -> query("select description,no_of_person as person_count,min_charges,min_km,charges_per_km,wait_time_charges,ride_later_time,cab_type.title,cab_type.type_id as cab_type from fare_info join cab_type on fare_info.cab_type=cab_type.type_id where fare_info.type='RESTRO' and cab_type.isactive='1'");
			$this -> db -> last_query();
			if ($fare -> num_rows() > 0)
			{
				return $fare_data = $fare -> result_array();
			}
			else
			{
				return false;
			}
		}

		function driver_unavailable($input_method)
		{
			/****************** Vales 1=
			 type =1 = make the Unavailable because use back button from device
			 type=2 make the TEMP_Hold because sent a new booking request to driver
			 type =-1 make Availble
			 */
			$type = $input_method['type'];
			//values 1,2,-1
			$driver_id = $input_method['driver_id'];
			if ($type == '1')
			{
				$driver_status = DRIVER_UNAVAILBLE;
			}
			elseif ($type == '2')
			{
				$driver_status = DRIVER_TEMP_HOLD;
			}
			elseif ($type == '-1')
			{

				$query = $this -> db -> query("SELECT `id` FROM `cab_booking` WHERE `driver_id` = $driver_id 
                                    AND (`status` = 'DRIVER_ACCEPTED' 
                                    OR `status` = 'CUSTOMER_ACCEPTED'
                                    OR `status` = 'TRIP_STARTED')LIMIT 1");
				$restro_booking_query = $this -> db -> query("SELECT `id` FROM `restro_cab_booking` WHERE `driver_id` = $driver_id 
                                    AND (`status` = 'DRIVER_ACCEPTED' 
                                    OR `status` = 'CUSTOMER_ACCEPTED'
                                    OR `status` = 'TRIP_STARTED')LIMIT 1");
				// echo $this->db->last_query();
				if ($query -> num_rows() > 0 || $restro_booking_query -> num_rows() > 0)
				{
					$driver_status = "BUSY";
				}
				else
				{
					$driver_status = DRIVER_AVAILABLE;
				}
			}
			$this -> db -> where("driver_id", $driver_id);
			$update_data = array("status" => $driver_status);
			$update_query = $this -> db -> update('driver', $update_data);
			// echo $this->db->last_query();
			if ($update_query)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function customer_aware($input_method)
		{
			$booking_id = $input_method['booking_id'];
			$driver_id = $input_method['driver_id'];
			$status = $input_method['type'];
			$this -> db -> select("restro_cab_booking.*,driver.*,cab.*,cab_type.title");
			$this -> db -> from("restro_cab_booking");
			//$this->db->join("user", "user.id=cab_booking.user_id");
			$this -> db -> join("driver", "driver.driver_id=restro_cab_booking.driver_id");
			$this -> db -> join("shift", "shift.driver_id=driver.driver_id and shift.status!='END'");
			$this -> db -> join("cab", "cab.cab_id=shift.cab_id");
			$this -> db -> join("cab_type", "cab.cab_type=cab_type.type_id");
			$this -> db -> where("restro_cab_booking.id", $booking_id);
			$query = $this -> db -> get();
			//echo $this->db->last_query();
			if ($query -> result_array() > 0)
			{
				// ADDTHE DRIVER STATUS TABLE
				$date = get_gmt_time();
				$driver_status = array(
					"driver_id" => $driver_id,
					"booking_id" => $booking_id,
					"status" => "FIVE_MIN_AWAY",
					"createdon" => $date
				);
				$this -> db -> insert("driver_status", $driver_status);

				return $query -> result_array();
			}
			else
			{
				return false;
			}
		}

		function trip_start($input_method)
		{
			$driver_Id = $input_method['driver_id'];
			$booking_Id = $input_method['booking_id'];
			//***************Check Booking condtion
			$this -> db -> select("id,status,driver_id,restro_id,client_name,country_code,client_contact_number");
			$this -> db -> where("driver_id", $driver_Id);
			$this -> db -> where("id", $booking_Id);
			//$this->db->where("status", "CUSTOMER_ACCEPTED");
			$result = $this -> db -> get('restro_cab_booking');
			$date = get_gmt_time();
			if ($result -> num_rows() > 0)
			{

				$user_detail = $result -> result_array();
				$restro_id = $user_detail[0]['restro_id'];
				$status = $user_detail[0]['status'];

				$data = array("status" => "TRIP_STARTED");
				$where_Condition = array("driver_id" => $driver_Id);
				$this -> db -> where("id", $booking_Id);
				$update = $this -> db -> update("restro_cab_booking", $data);
				$driver_status = array(
					"driver_id" => $driver_Id,
					"booking_id" => $booking_Id,
					"booking_type" => "RESTRO",
					"status" => "TRIP_STARTED",
					"createdon" => $date
				);
				$this -> db -> insert("driver_status", $driver_status);
				if ($update)
				{

					return array(
						"status" => 1,
						"client_contact_number" => $user_detail[0]['client_contact_number'],
						"client_country_code" => $user_detail[0]['country_code'],
						// "client_email" => $user_detail[0]['client_email'],
						"client_name" => $user_detail[0]['client_name']
					);
				}
			}
			else
			{
				return array(
					"status" => 0,
					"message" => BOOKING_NOTFOUND
				);
			}

		}

		function trip_end($input_method)
		{
			$driver_Id = $input_method['driver_id'];
			$booking_Id = $input_method['booking_id'];
			$km_driven = $input_method['km_driven'];
			$journey_time = $input_method['journey_time'];
			$waiting_time = $input_method['waiting_time'];
			//***************Check Booking condtion*********
			$this -> db -> select("`restro_cab_booking`.`id`, `restro_cab_booking`.`status`, `restro_cab_booking`.`driver_id`,
        restro_cab_booking.booking_number,restro_cab_booking.pickup_location,restro_cab_booking.plate_number,
        restro_cab_booking.discount_amount, `cab_type`,  `pickup_latitude`, `pickup_longitude`,`driver`.`driver_id`, driver.first_name,driver.last_name, contact,driver.profile_pic,
        restro_cab_booking.client_name,restro_cab_booking.country_code,restro_cab_booking.client_contact_number");
			//$this->db->select("*");
			$this -> db -> where("restro_cab_booking.driver_id", $driver_Id);
			$this -> db -> where("restro_cab_booking.id", $booking_Id);
			//$this->db->where("cab_booking.status", "TRIP_STARTED");
			$this -> db -> from("restro_cab_booking");
			$this -> db -> join("driver", "driver.driver_id=restro_cab_booking.driver_id");

			$result = $this -> db -> get();

			if ($result -> num_rows() > 0)
			{
				$trip_data = $result -> result_array();
				// echo $trip_data[0]['status'] ;
				if ($trip_data[0]['status'] == 'TRIP_STARTED')
				{
					// ADDTHE DRIVER STATUS TABLE
					$date = get_gmt_time();
					$driver_status = array(
						"driver_id" => $driver_Id,
						"booking_id" => $booking_Id,
						"booking_type" => "RESTRO",
						"status" => "TRIP_ENDED",
						"createdon" => $date
					);
					$this -> db -> insert("driver_status", $driver_status);

					//UPDATE DRIVER STATUS IN SHIFT TABLE...
					$driver_available = DRIVER_AVAILABLE;
					$data = array("status" => "$driver_available");
					$this -> db -> where("driver_id", $driver_Id);
					$update = $this -> db -> update("driver", $data);

					if ($update)
					{
						return array(
							"status" => 1,
							"trip_detail" => $result -> result_array()
						);

					}
					else
					{
						$result = array(
							"status" => 0,
							"message" => "SERVER_ERROR"
						);
						return $result;
					}
				}
				elseif ($trip_data[0]['status'] == 'TRIP_ENDED' || $trip_data[0]['status'] == 'FINISH')
				{
					$result = array(
						"status" => 0,
						"message" => ALREADY_TRIP_END
					);
					return $result;
				}
				else
				{
					$result = array(
						"status" => 0,
						"message" => TRIP_ENDED_ERROR
					);
					return $result;
				}
			}
			else
			{
				$result = array(
					"status" => 0,
					"message" => BOOKING_NOTFOUND
				);
				return $result;
			}

		}

		function getfare_detail($cab_type)
		{
			$result = $this -> db -> get_where("fare_info", array(
				"cab_type" => $cab_type,
				"type" => "RESTRO"
			));
			if ($result -> num_rows() > 0)
			{
				return $result -> result_array();
			}
			else
			{
				return false;
			}

		}

		function save_booking_payment($data)
		{
			$payment = $this -> db -> insert("payment_detail", $data);
			//echo $this->db->last_query();
			$booking_Id = $data['booking_id'];
			if ($this -> db -> insert_id())
			{
				//update the booking status
				$update_data = array("status" => "TRIP_ENDED");
				$this -> db -> where("id", $booking_Id);
				$upadte = $this -> db -> update("restro_cab_booking", $update_data);
				if ($upadte)
				{
					return true;
				}
				else
				{
					return false;
				}

			}
			return false;

		}

		function update_driver_status($data)
		{
			$driver_id = $data['driver_id'];
			$status = $data['status'];
			$upadte_data = array(
				"status",
				$status
			);
			$this -> db -> where("driver_id", $driver_id);
			$update = $this -> db -> update("driver", $data);
			if ($update)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function driver_cancel($input_method)
		{
			$driver_id = $input_method['driver_id'];
			$booking_id = $input_method['booking_id'];
			//Check the booking status
			$this -> db -> select("*");
			$this -> db -> where("driver_id", $driver_id);
			$this -> db -> where("id", $booking_id);
			$this -> db -> from("restro_cab_booking");
			$query = $this -> db -> get();
			if ($query -> num_rows())
			{
				$data = $query -> result_array();

				if ($data[0]['status'] != "TRIP_STARTED" && $data[0]['status'] != "FINISH" && $data[0]['status'] != "TRIP_ENDED" && $data[0]['status'] != 'CANCELLED')
				{

					$query = $this -> db -> get_where("cab_booking_history", array(
						"booking_id" => $booking_id,
						"booking_type" => "RESTRO"
					));
					if ($query -> num_rows())
					{
						$gmdate = get_gmt_time();
						$insert_data = array(
							"booking_id" => $booking_id,
							"driver_id" => $driver_id,
							"booking_type" => "RESTRO",
							"driver_cancel" => "DRIVER_CANCELLED",
							"driver_cancel_datetime" => $gmdate
						);
						$this -> db -> where("booking_id", $booking_id);
						$res = $this -> db -> update("cab_booking_history", $insert_data);
						//update the booking status in booking table
						$update_status = array("status" => "DRIVER_CANCELLED");
						$this -> db -> where("id", $booking_id);
						$res = $this -> db -> update("restro_cab_booking", $update_status);
						if ($res)
						{
							//make driver free
							$driver_available = DRIVER_AVAILABLE;
							$updatedata = array('status' => "$driver_available");
							$this -> db -> where('driver_id', $driver_id);
							$update = $this -> db -> update('driver', $updatedata);
							return array(
								"status" => 1,
								"message_customer" => CUSTOMER_NOTIFY,
								"message" => DRIVER_CANCEL,
								"client_contact_number" => $data[0]['client_contact_number'],
								"client_country_code" => $data[0]['country_code']
							);
						}
						else
						{
							return array(
								"status" => 0,
								"message" => SERVER_ERROR
							);
						}

					}

				}//DRIVER_CANCELLED
				else
				{
					return array(
						"status" => 0,
						"message" => DRIVER_NOT_CANCEL
					);
				}
			}
			else
			{
				return array(
					"status" => 0,
					"message" => BOOKING_NOTFOUND
				);
			}

		}

		function payment_done($input_method)
		{
			$booking_id = $input_method['booking_id'];
			$driver_id = $input_method['driver_id'];
			$this -> db -> select('client_contact_number,country_code,status');
			$this -> db -> where("driver_id", $driver_id);
			$this -> db -> where("id", $booking_id);
			$this -> db -> from('restro_cab_booking');
			$query = $this -> db -> get();
			if ($query -> num_rows() > 0)
			{
				$data = $query -> result_array();
				if ($data[0]['status'] == "TRIP_ENDED")
				{
					$query = $this -> db -> query("update restro_cab_booking set status='FINISH' where id=$booking_id");
					$query = $this -> db -> query("update payment_detail set user_paid=billing_amount,is_paid='1',description='success' where booking_id=$booking_id and `booking_type` = 'RESTRO'");
					if ($query)
					{
						return array(
							"status" => 1,
							"message" => "success",
							"client_country_code" => $data[0]['country_code'],
							"client_contact_number" => $data[0]['client_contact_number']
						);
					}
					else
					{
						return array(
							"status" => 0,
							"message" => SERVER_ERROR,
						);
					}
				}
				else
				{
					return array(
						"status" => 0,
						"message" => TRIP_NOT_ENDED,
					);
				}
			}
			else
			{
				return array(
					"status" => 0,
					"message" => BOOKING_NOTFOUND
				);
			}

		}

		function cab_booking_advance($input_method)
		{
			$lat = $input_method['pickuplat'];
			$lng = $input_method['pickuplong'];
			$restro_id = $input_method['restro_id'];
			$date = get_gmt_time();
			$booking_time = $input_method['pickup_time'];
			$cab_type = $input_method['cab_type'];
			$pickup_address = $input_method['pickup_location'];
			$client_name = $input_method['client_name'];
			$country_code = '+' . $input_method['country_code'];
			$contact_number = $input_method['client_contact_number'];
			$data = array(
				'restro_id' => $restro_id,
				'client_name' => $client_name,
				'country_code' => $country_code,
				'client_contact_number' => $contact_number,
				'createdon' => $date,
				"pickup_latitude" => $lat,
				"pickup_longitude" => $lng,
				"pickup_location" => $pickup_address,
				"booking_time" => $booking_time,
				"cab_type" => $cab_type,
				"status" => 'ADVANCE_BOOKING',
				"is_prebooking" => 'Y',
				"area" => $input_method['area']
			);
			$str = $this -> db -> insert('restro_cab_booking', $data);
			// echo "pp". $this->db->last_query();
			$booking_id = $this -> db -> insert_id();
			if ($booking_id)
			{
				$CBN = "PD" . sprintf("%05d", $booking_id);
				$this -> db -> query("update restro_cab_booking set booking_number='" . $CBN . "' where id=$booking_id");
				return $booking_id;
				//send request to driver for new booking
				//search nearest driver

			}
			else
			{
				return false;
			}
		}

		function check_appversion($input_method)
		{
			$os_type = $input_method['os_type'];
			$app_mode = $input_method['app_mode'];
			$query = $this -> db -> get_where("appversion", array(
				"os_type" => $input_method['os_type'],
				"app_mode" => $input_method['app_mode']
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$version_detail = $query -> result_array();
				return $version_detail[0];
			}
			else
			{
				return false;
			}

		}

	}
?>
