<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Appuser extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url'
			));
			$this -> load -> model('Appuseradmin_model');
			$this -> load -> library(array(
				'session',
				'email',
				'paginationlib',
				'Pagination'
			));
			$this -> load -> helper(array(
				'function_helper',
				'constants_helper'
			));
			$user = $this -> session -> userdata('logged_in');
			if (!$user)
			{
				redirect('Welcome');
			}

		}

		/* fetch app user infomation */
		public function app_user()
		{
			$this -> load -> library('Ajax_pagination');
			$this -> Appuseradmin_model -> app_user_count();
			$pagingconfig = $this -> paginationlib -> initPagination("/Appuser/app_user_ajaxPaginationData/", $this -> Appuseradmin_model -> App_user_count(), 10, 1, array());

			$this -> ajax_pagination -> initialize($pagingconfig);

			$data['link'] = $this -> ajax_pagination -> create_links();
			$user = $this -> Appuseradmin_model -> Fetch_cuser_info($pagingconfig['per_page'], 0);
			$company = $this -> Appuseradmin_model -> get_app_user();
			$data['company'] = $company;
			//print_r($company);
			$data['user'] = $user;
			$data['active_page'] = 'app_user';
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('app_user_view');
			$this -> load -> view('site-nav/footer');

		}

		/* ajax pagination for count app user */
		function app_user_ajaxPaginationData()
		{
			$this -> load -> library('Ajax_pagination');
			$postdata = $this -> input -> post('data');
			$post = json_decode($postdata, true);
			$page = $post['page'];
			if (!$page)
			{
				$offset = 0;
			}
			else
			{
				$offset = $page;
			}

			$pagingconfig = $this -> paginationlib -> initPagination("/Appuser/app_user_ajaxPaginationData/", $this -> Appuseradmin_model -> app_user_count(), 10, $post['page']);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			//get the posts data
			$data['user'] = $this -> Appuseradmin_model -> Fetch_cuser_info($pagingconfig['per_page'], $page);
			$company = $this -> Appuseradmin_model -> get_app_user();
			$data['company'] = $company;
			//load the view
			$this -> load -> view('app_user_view_ajax', $data, false);
		}

		/* active company user */
		function cuser_change_status()
		{
			$id = $_POST['id'];
			$status = $_POST['status'];

			$cuser_status = $this -> Appuseradmin_model -> Active_appuser($id, $status);
			if ($status == '1')
			{
				//$get_email=$this->Appuseradmin_model->get_email($id);
				//company_email($get_email);
			}
			echo $cuser_status;
		}
		function change_appuserdelete_status()
    {
    	
        $id = $_POST['id'];
        echo $this->Appuseradmin_model->delete_appuser($id);
    }

		function cuser_change_statusapprove()
		{
			$id = $_POST['id'];
			$status = $_POST['status'];
			$cuser_status = $this -> Appuseradmin_model -> Approve_appuser($id, $status);
			if ($status == '1')
			{
				$get_appuser = $this -> Appuseradmin_model -> get_appuser_approvecode($id);
				//$userid=$get_appuser[0]['id'];
				$email = $get_appuser[0]['email'];
				$appcode = $get_appuser[0]['appcode'];
				Approve_statuscode_Mail($email,$appcode);
			}
			echo $cuser_status;
		}

	}
