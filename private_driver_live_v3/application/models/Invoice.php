<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Invoice extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
       $this->load->helper(array('form','url','function_helper'));
	     $this->load->model('Booking_model');
		 $this->load->model('Payment_model');
		
	$this->load->library(array('session','pagination','paginationlib','email','Pdf'));

		$user = $this->session->userdata('logged_in');
        if (!$user) {
            redirect('Welcome');
        }
	
    }
  
public function due_cuser_payment()
{
  $this->load->library('Ajax_pagination');
  $this->Booking_model->count_company_due();
        $pagingconfig = $this->paginationlib->initPagination("/Invoice/duecuser_ajaxPaginationData/",
						$this->Booking_model->count_company_due(),10, 1,array());
		 $this->ajax_pagination->initialize($pagingconfig);
         $data['link'] = $this->ajax_pagination->create_links();
        $due_payment=$this->Booking_model->company_due($pagingconfig['per_page'], 0);
        $data['due_payment'] = $due_payment;
		$data['active_page'] = 'due_payment';
		$this->load->view('site-nav/header', $data);
		$this->load->view('payment');
	    $this->load->view('site-nav/footer');
    
}

/* due_cuser ajaxpagination */
function duecuser_ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Invoice/duecuser_ajaxPaginationData/", $this->Booking_model->count_company_due(),10, $post['page']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
         $data['due_payment'] = $this->Booking_model->company_due($pagingconfig['per_page'],$page);
        $this->load->view('payment_ajax', $data, false);
    }

  public function perPage()
    {
        $this->load->library('Ajax_pagination');
        return 2;
    }
/* function for fetch complete payment detail */

function company_activity()
{
	if ((isset($_POST['date'])) && ($_POST['companyid']))
        {

            $date = $_POST['date'];
            $dates = explode('to', $date);
            $start_date = trim($dates[0]);
            $end_date = trim($dates[1]);
            $company_id = $_POST['companyid'];
			
			$this->company_date($start_date, $end_date,$company_id);
        }
        else
        {
            $data['fetch_company'] = $this->Booking_model->get_company();
            $data['active_page'] = 'company_activity';
            $this->load->view('site-nav/header', $data);
            $this->load->view('company_activity', $data);
            $this->load->view('site-nav/footer');
        }
}


function complete_payment()
{
	
	   $this->load->library('Ajax_pagination');
        $this->Booking_model->count_compelte_payment();
        $pagingconfig = $this->paginationlib->initPagination("/Invoice/completepayment_ajaxPaginationData/", $this->
            Booking_model->count_compelte_payment(),10, 1,array());
        $this->ajax_pagination->initialize($pagingconfig);
       $data['link'] = $this->ajax_pagination->create_links();
        $fetch_complete=$this->Booking_model->Complete_payment_detail($pagingconfig['per_page'], 0);
        $data['fetch_complete']=$fetch_complete;
		$data['active_page'] = 'payment';
        $this->load->view('site-nav/header', $data);
        $this->load->view('complete_payment');
		$this->load->view('site-nav/footer');
}

/*complete payment ajax pagination */
/* due_cuser ajaxpagination */







/* fetch particular user onclick for complete payment */
function fetch_particular_user()
{
	$userid=@$_POST['user_id'];
	$fetch_paymentdetail=$this->Booking_model->fetch_user_paymentdetail($userid);
	$data['fetch_paymentdetail']=$fetch_paymentdetail;
		
		$data['active_page'] = 'complete_payment';
        $this->load->view('site-nav/header', $data);
        $this->load->view('userpayment_detail');
		$this->load->view('site-nav/footer');
}


/* send email */
 function sendemail()
 {
	$filepath=base_url().'pdf_download/';
  $to=@$_POST['emailid'];
  $filename=$filepath.$_POST['filename'];
  
// $to = "saurabh@squarebits.in";
	
$body='<body style="font-family:\'Calibri\'; font-size:16px;"><p>Dear Customer,<p>
<p>
Bill</p>
<p>
 </p>

<p>Kind regards,</p>

<p> Group</p>
<br/>

---------------
<p>
This message (including attachments) may contain information that is privileged, confidential or protected from disclosure. If you are not the intended recipient, you are hereby notified that dissemination, disclosure, copying, distribution or use of this message or any information contained in it is strictly prohibited. If you have received this message in error, please immediately notify the sender by reply e-mail and delete this message from your computer. Although we have taken steps to ensure that this e-mail and attachments are free from any virus, we advise that in keeping with good computing practice the recipient should ensure they are actually virus free.</p>
<br/>
---------------
</body>';
	
	
	
 $config['protocol'] = 'smtp';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';

			$config['smtp_host'] = EMAIL_HOST;
			$config['smtp_user'] = EMAIL_USERNAME;
			$config['smtp_pass'] = EMAIL_PASSWORD;
			$config['smtp_port'] = EMAIL_PORT;
			$config['email_smtp_crypto'] = 'tls';
			$config['email_newline'] = '\r\n';

			
			$ci -> email -> clear();
			$ci -> email -> initialize($config);
$ci -> email -> set_newline("\r\n");
                $this->email->from(ADMINMAIL_FROM, 'private Driver App');
                $this->email->to($to);
                $this->email->subject('bill');
                $this->email->message($body);
				$this->email->attach($filename);
                $this->email->send();
	//$counter ++;
	  if($this->email->send())
         {
          echo '1';
         }
         else
        {
			echo $filename;
         //show_error($this->email->print_debugger());
		 
        }

}

/* change payment status */
function change_payment_status()
{
	$bookingno=$_POST['bno'];
	$payment_status=$_POST['p_status'];
	$description=$_POST['description'];
	$update_payment=$this->Booking_model->update_payment($bookingno,$payment_status,$description);
	if($update_payment=='1')
	{
		echo 1;
	}
	else
	{
		echo 0;
	}
}

/*get conmpany record accoring date */
function company_date($start_date,$end_date,$company_id)
{
	   $this->load->library('Ajax_pagination');
       $count= $this->Booking_model->count_compelte_payment($start_date,$end_date,$company_id);
        $pagingconfig = $this->paginationlib->initPagination("/Invoice/completepayment_ajaxPaginationData/", 
        $count,10, 1,array("start_date"=>"$start_date","end_date"=>"$end_date","company_id"=>$company_id));
        $this->ajax_pagination->initialize($pagingconfig);
         $data['link'] = $this->ajax_pagination->create_links();
        $fetch_complete=$this->Booking_model->Complete_payment_detail($start_date,$end_date,$company_id,$pagingconfig['per_page'], 0);
        $data['fetch_complete']=$fetch_complete;
		$data['active_page'] = 'payment';
    
        $this->load->view('complete_payment',$data);
		
		

}

/* pagination for complete company booking */
function completepayment_ajaxPaginationData()
    {
         $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
		
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }
		$start_date=$post['start_date'];
		$end_date=$post['end_date'];
		$company_id=$post['company_id'];
       $count= $this->Booking_model->count_compelte_payment($start_date,$end_date,$company_id);
        $pagingconfig = $this->paginationlib->initPagination("/Invoice/completepayment_ajaxPaginationData/", $count,10, $page, array("start_date"=>"$start_date","end_date"=>"$end_date","company_id"=>$company_id));
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
         $data['fetch_complete'] = $this->Booking_model->Complete_payment_detail($start_date,$end_date,$company_id,$pagingconfig['per_page'],$page);
        $this->load->view('complete_payment_ajax', $data, false);
    }

function get_company_booking()
{
	error_reporting(1);
    $booking_no=$_POST['booking_no'];
	$data['get_data']=$this->Booking_model->get_company_booking_pop($booking_no);
	$this->load->view('company_booking_pop_up',$data);

}
}