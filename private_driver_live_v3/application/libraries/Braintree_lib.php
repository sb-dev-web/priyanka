<?php

class Braintree_lib
{
    public function __construct()
    {
        $ci = &get_instance();
        $ci->load->helper(array('constants'));
        require_once APPPATH . 'third_party/braintree_env.php';
        Braintree_Configuration::environment(BTENVIRONMENT);
        Braintree_Configuration::merchantId(MERCHANTID);
        Braintree_Configuration::publicKey(PUBLICKEY);
        Braintree_Configuration::privateKey(PRIVATEKEY);
    }
}

?>