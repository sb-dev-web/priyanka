<?php

	if (!defined('BASEPATH'))
		exit('No direct script access allowed');

	function testCreate_withCreditCardAndVerification()
	{
		$result = Braintree_Customer::create(array(
			'firstName' => 'Michal',
			'lastName' => 'Jones',
			'email' => 'mike.jones@example.com',
			'phone' => '+914195551234',
			'creditCard' => array(
				'number' => '4012000033330125',
				'expirationDate' => '05/18',
				'cvv' => '445',
				'cardholderName' => 'Michal Jones',
				'options' => array('verifyCard' => true)
			)
		));

		if ($result -> success)
		{
			echo "<pre>";
			//print_r($result->customer);
			echo "Customer ID: " . $result -> customer -> id;
			echo "Payment Token: " . $result -> customer -> creditCards[0] -> token;
			//do your stuff like insert customer in to database with the customerID and
			// Payment Token
		}
		else
		{
			print_r($result -> errors);
			echo("Verfication Status: " . $result -> creditCardVerification -> status);
			echo("<br>Error Code: " . $result -> creditCardVerification -> processorResponseCode);
			echo("<br>Error Message: " . $result -> creditCardVerification -> processorResponseText);
			echo("<br>CVV Response Code: " . $result -> creditCardVerification -> cvvResponseCode);
			echo("<br>AVS Error Code: " . $result -> creditCardVerification -> avsErrorResponseCode);
			echo("<br>POSTAL CODE RESPONSE: " . $result -> creditCardVerification -> avsPostalCodeResponseCode);
			echo("<br>AVS Stree Address Response Code: " . $result -> creditCardVerification -> avsStreetAddressResponseCode);
		}
	}

	function Create_withCreditCardAndVerification($data)
	{
		$result = Braintree_Customer::create(array(
			'firstName' => $data['name'],
			'lastName' => '',
			'email' => $data['email'],
			'phone' => $data['phone'],
			'creditCard' => array(
				'number' => $data['ccnumber'],
				'expirationDate' => $data['exp_date'],
				'cvv' => $data['cvv_no'],
				'cardholderName' => $data['name'],
				'options' => array('verifyCard' => true)
			)
		));
		//  print_r($result);

		if ($result -> success)
		{

			return array(
				"status" => 1,
				"customer_id" => $result -> customer -> id,
				"token" => $result -> customer -> creditCards[0] -> token,
				"cardtype" => $result -> customer -> creditCards[0] -> cardType
			);

		}
		else
		{
			//print_r($result->errors);
			if (@$result -> creditCardVerification -> status == 'processor_declined' || @$result -> creditCardVerification -> processorResponseCode == '2000')
			{
				$message = "Failed to accept Credit Card, Reason: " . $result -> message;
				return array(
					"status" => 0,
					"message" => $result -> creditCardVerification -> status,
					"message" => $message,
					"errorcode" => $result -> creditCardVerification -> processorResponseCode
				);
			}
			else
			{
				return array(
					"status" => 0,
					"message" => $result -> message,
					"errorcode" => BT_ERROR
				);
			}

		}
	}

	function Update_withUpdatingExistingCreditCard($data)
	{

		$result = Braintree_Customer::update($data['braintree_customer_id'], array(
			'firstName' => $data['name'],
			'lastName' => '',
			'creditCard' => array(
				'number' => $data['credit_card_no'],
				'expirationDate' => $data['expire_date'],
				'cvv' => $data['cvv_no'],
				'cardholderName' => $data['name'],
				'options' => array(
					'updateExistingToken' => $data['token'],
					'verifyCard' => true
				)
			)
		));
		// echo "<pre>";

		// print_r($result);
		//echo $result->message;
		if ($result -> success)
		{
			return array(
				"status" => 1,
				"customer_id" => $result -> customer -> id,
				"token" => $result -> customer -> creditCards[0] -> token
			);
		}
		else
		{
			if (@$result -> creditCardVerification -> status == 'processor_declined' || @$result -> creditCardVerification -> processorResponseCode == '2000')
			{
				return array(
					"status" => 0,
					"message" => $result -> creditCardVerification -> status,
					"errorcode" => $result -> creditCardVerification -> processorResponseCode
				);
			}
			else
			{
				return array(
					"status" => 0,
					"message" => $result -> message,
					"errorcode" => BT_ERROR
				);
			}
		}
		//$this->assertEquals(true, $result->success);
		//    $this->assertEquals('New First', $result->customer->firstName);
		//    $this->assertEquals('New Last', $result->customer->lastName);
		//    $this->assertEquals(1, sizeof($result->customer->creditCards));
		//    $creditCard = $result->customer->creditCards[0];
		//    $this->assertEquals('411111', $creditCard->bin);
		//    $this->assertEquals('11/2014', $creditCard->expirationDate);
		//    $this->assertEquals('New Cardholder', $creditCard->cardholderName);
	}

	function UpdateExistingCustomerDetail($data)
	{

		try
		{
			$updateResult = Braintree_Customer::update($data['braintree_customer_id'], array(
				'firstName' => $data['name'],
				'lastName' => '',
				'phone' => $data['phone'],
			));
		}
		catch(Exception $e)
		{
			return array(
				"status" => 0,
				"message" => BRAINTREE_USER_ERROR
			);
		}

		if ($updateResult -> success)
		{
			return array(
				"status" => 1,
				"message" => "success"
			);
		}
		else
		{
			return array(
				"status" => 0,
				"message" => BRAINTREE_USER_ERROR
			);
		}
	}

	function customer_penalty_charge($data)
	{
		//$braintree_user_token = $data['nonce'];
		/*$result = Braintree_Transaction::sale([
		 'paymentMethodToken' => $braintree_user_token,
		 'amount' => PENALTY_CHARGE]);*/
		$result = Braintree_Transaction::sale([
		'amount' => $data['amount'],
		'paymentMethodNonce' => $data['nonce'],
		'options' => ['submitForSettlement' => True]]);

		if ($result -> success == '1')
		{
			$transction_id = $result -> transaction -> id;
			$desc = "success";
			return array(
				"status" => 1,
				"transaction_id" => $transction_id,
				"currency" => $result -> transaction -> currencyIsoCode,
				"payment_status" => $result -> transaction -> status,
				"nonce" => $data['nonce'],
				"message" => "success"
			);

		}
		else
		{
			$desc = $result -> message;
			$transction_id = "";
			return array(
				"status" => 0,
				"message" => $desc,
				"nonce" => $data['nonce'],
				"payment_status" => "fail",
				"currency" => "",
				"transaction_id" => "",
			);
		}

	}

	function AuthUserCard($data)
	{
		/*$braintree_user_token = $data['token'];
		 $result = Braintree_Transaction::sale([
		 'paymentMethodToken' => $braintree_user_token,
		 'amount' => 500]);
		 //  print_r($result);
		 if ($result -> success == '1')
		 {
		 $transction_id = $result -> transaction -> id;
		 $status = $result -> transaction -> status;
		 return array(
		 "status" => 1,
		 "transaction_id" => $transction_id,
		 "transaction_status" => $status
		 );

		 }
		 else
		 {
		 $desc = $result -> message;
		 $transction_id = "";
		 $status = $result -> transaction -> status;
		 return array(
		 "status" => 0,
		 "message" => $desc,
		 "transaction_status" => $status
		 );
		 }*/

	}

	function DeleteCustomer($customer_id)
	{
		try
		{
			$result = Braintree_Customer::delete($customer_id);
			//print_r($result);
			if ($result -> success)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			return true;
		}
	}

	function VoidedTransaction($data)
	{
		$transaction_id = $data['transaction_id'];
		$result = Braintree_Transaction::void($transaction_id);

		if ($result -> success == '1')
		{

			$status = $result -> transaction -> status;
			$transaction_id = $result -> transaction -> id;
			return array(
				"transaction_id" => $transaction_id,
				"transaction_status" => $status,
				"message" => "Success",
				"status" => 1
			);

		}
		else
		{
			$message = $result -> message;
			return array(
				"transaction_id" => $transaction_id,
				"transaction_status" => "",
				"message" => $message,
				"status" => 0,
			);
		}
	}

	function paymentMethodNonce($token)
	{
		try
		{
			$result = Braintree_PaymentMethodNonce::create($token);
		}
		catch(Exception $e)
		{
			return array(
				"status" => 2,
				"message" => BRAINTREE_USER_ERROR
			);
		}
		if ($result -> success == '1')
		{
			return array(
				"status" => 1,
				"nonce" => $result -> paymentMethodNonce -> nonce
			);
		}
		else
		{
			return array(
				"status" => 0,
				"message" => $result -> message
			);
		}

	}

	function MakePayment($data)
	{

		$result = Braintree_Transaction::sale([
		'amount' => $data['amount'],
		'paymentMethodNonce' => $data['nonce'],
		'options' => ['submitForSettlement' => True]]);
		return $result;
	}

	function BraintreeTokan()
	{
		try
		{
			return $clientToken = Braintree_ClientToken::generate();
		}
		catch(Exception $e)
		{
			return FALSE;
		}

	}

	function CreateCustomer($data)
	{

		$result = Braintree_Customer::create([
		'firstName' => $data['name'],
		'lastName' => '--',
		'email' => $data['email'],
		'phone' => $data['phone'],
		'creditCard' => [
		'paymentMethodNonce' => $data['nonce'],
		'options' => ['verifyCard' => true]]]);

		//print_r($result);
		if ($result -> success == '1')
		{
			return array(
				"status" => 1,
				"customer_id" => $result -> customer -> id,
				"token" => $result -> customer -> creditCards[0] -> token,
				"last4" => $result -> customer -> creditCards[0] -> last4,
				"cardtype" => $result -> customer -> creditCards[0] -> cardType,
				"exp_month" => $result -> customer -> creditCards[0] -> expirationMonth,
				"exp_year" => $result -> customer -> creditCards[0] -> expirationYear,
				"expire_date" => $result -> customer -> creditCards[0] -> expirationMonth . '/' . $result -> customer -> creditCards[0] -> expirationYear
			);
		}
		else
		{
			//print_r($result->errors);
			if (@$result -> creditCardVerification -> status == 'processor_declined' || @$result -> creditCardVerification -> processorResponseCode == '2000')
			{
				$message = "Failed to accept Credit Card, Reason: " . $result -> message;
				return array(
					"status" => 0,
					"message" => $result -> creditCardVerification -> status,
					"message" => $message,
					"errorcode" => $result -> creditCardVerification -> processorResponseCode
				);
			}
			else
			{
				return array(
					"status" => 0,
					"message" => $result -> message,
					"errorcode" => BT_ERROR
				);
			}

		}

	}

	function UpdateUserCard($data)
	{
		

		$updateResult = Braintree_Customer::update($data['braintree_customer_id'], ['creditCard' => [
		'paymentMethodNonce' => $data['nonce'],
		'options' => [
		'updateExistingToken' => $data['token'],
		'verifyCard' => true]]]);
//print_r($data);
		//print_r($updateResult);
		if ($updateResult -> success == '1')
		{
			return array(
				"status" => 1,
				"customer_id" => $updateResult -> customer -> id,
				"token" => $updateResult -> customer -> creditCards[0] -> token,
				"credit_card_no" => $updateResult -> customer -> creditCards[0] -> last4,
				"cardtype" => $updateResult -> customer -> creditCards[0] -> cardType,
				"exp_month" => $updateResult -> customer -> creditCards[0] -> expirationMonth,
				"exp_year" => $updateResult -> customer -> creditCards[0] -> expirationYear,
				"expire_date" => $updateResult -> customer -> creditCards[0] -> expirationDate
			);
		}
		else
		{
			//print_r($result->errors);
			if (@$updateResult -> creditCardVerification -> status == 'processor_declined' || @$updateResult -> creditCardVerification -> processorResponseCode == '2000')
			{
				$message = "Failed to accept Credit Card, Reason: " . $result -> message;
				return array(
					"status" => 0,
					"message" => $updateResult -> creditCardVerification -> status,
					"message" => $message,
					"errorcode" => $updateResult -> creditCardVerification -> processorResponseCode
				);
			}
			else
			{
				return array(
					"status" => 0,
					"message" => $updateResult -> message,
					"errorcode" => BT_ERROR
				);
			}

		}

	}
?>