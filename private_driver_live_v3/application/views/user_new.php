<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Company User
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active"></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         
                 
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <!--<a href="<?php echo site_url('Cab/add_cab');?>"><input type="button" class="btn btn-block btn-warning" value="Add" style="float:right; margin-right:7px;margin-top:-8px; width:10%;"/></a><br/><br/>
             -->
            </div>
            <div class="box-body record">
              <br>
                  <table id="example2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Company Name</th>
                         <th>Email</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Company Assgined</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
              
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
     
      
<!-- pop up for change payment status !-->  
 <div class="modal fade" id="open_assign" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: 490px; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
              Company Assign
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: 490px;max-height:310px; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="margin: 18px auto;width: 80%;">
                 
                <form id="sendpstatus" action="" method="post" autocomplete="off">
                     <input type="hidden"  id="userid" name="userid">
                   <p>
                                <label>Company</label></label>
                                <span class="formwrapper item">
                                  <select size="1" name="companyid" id="companyid"  class="chosen  validate[required]"  style="width:290px;height:30px;"> 
                                      <?php
									
									  if(sizeof($company))
									  {
										
										for($i=0;$i<sizeof($company);$i++)
										{
											 $name=$company[$i]['company_name'];
											
									  ?>
									  <option value="<?php  echo $company[$i]['id'];?>"id="companyid"><?php echo $name;?></option>
                                      <?php
										}
									  }
									  ?>
                                    </select>
                                </span>
                            </p>
  
                
                    <div class="item" style="height: 60px!important;">
                      
                    </div>
                    
                    <div style="margin-top:-48px;">
                        <input  class="btn btn-info btn-lg" type="button" value="Save" id="save_assign" name="save_assign" style="margin-left:140px;"/>
                        <span id="loader_note" class="loader" style="display: none;">
                            <img src="http://dev.privatedriverapp.com/app/images/loader19.gif" alt=""/>
                        </span>
                    </div>
                </form>    
                </div>
            </div>
        </div> 
    </div>   
</div>  
      
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
   <!-- <script src="<?php echo base_url() ?>js/jquery-1.11.0.min.js"></script> -->

<script type="text/javascript" src="<?=base_url();?>js/my_custom.js"></script>

<script type="text/javascript" language="javascript" src="<?php echo base_url();?>plugins/server_datatable/js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url();?>plugins/server_datatable/js/jquery.dataTables.js"></script>

<script type="text/javascript" language="javascript" >
			$(document).ready(function() {
				var dataTable = $('#example2').DataTable( {
					"processing": true,
					"serverSide": true,
					"ajax":{
						url :'<?php echo site_url('Generalinfo/Getuser');?>', // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".employee-grid-error").html("");
							$("#example2").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
							
						}
					}
				} );
				
				
			} );  


 
       <script>
  function changeStatus(id,loadingdiv,spanid)
{
  //alert();
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    if(spanValue=='Inactive')
    {  
       status = '1';
    }
    $.ajax({type:'POST',url: '<?php echo site_url('Generalinfo/cuser_change_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       statusfeedback(response,spanid,loadingdiv);
     }});
}
function statusfeedback(retstatus,spanid,loadingdiv) 
{
    //alert(retstatus);
    if(retstatus=='0')
    {
        $('#'+spanid).removeClass('label-success').addClass('label-warning');
        $('#'+spanid).text("Inactive");
    }
    else if(retstatus=='1')
    {

        $('#'+spanid).removeClass('label-warning').addClass('label-success');
        $('#'+spanid).text("Active");
    }
     $('#'+loadingdiv).css('display','none');
    
}
</script>
<script>
  

function changeassignstatus(id)
{
  
  $('#userid').val(id);
   $('#open_assign').modal({
        "backdrop" : "static"
         })
}

  $('#save_assign').click(function(){
   
       
        //var payment_status=$('#pstatus').val();
        var cid=$('#companyid').val();
        var uid=$('#userid').val();
        
       // alert(bookingno);
      
         validateassigncompany(cid,uid);
        });


</script>
<Script>
  function validateassigncompany(cid,uid)
{
    
    
  
    //alert(n_id);
  
   
   
       
            
                    $.ajax({
                        type: "POST",
                        url: "assign_company",
                        data        :{'c_id':cid,'u_id':uid},
                        beforeSend: function(){
                                
                              },
                        success: function(data){
                            //alert(data);
                            if(data == 1)
                            {
                              
                                setTimeout(function() {
                                    $.bootstrapGrowl("company is assign successfully", {
                                        type: 'success',
                                        align: 'right',
                                        width: 'auto',
                                        allow_dismiss: true
                                    });
                                }, 1000);
                                window.setTimeout(function(){location.reload()},2000);
                            }
                           
                            
                            
                            else
                            {
                             
                                setTimeout(function() {
                                    $.bootstrapGrowl("Sorry something wrong", {
                                        type: 'danger',
                                        align: 'right',
                                        width: 'auto',
                                        allow_dismiss: true
                                    });
                                }, 1000); 
                            }
                            
                        }
                    });
                }
                    
        
  function Pagination(TableID,Path)
{
	//alert();
		var dataTable = $('#example2').DataTable( {
					"processing": true,
					"serverSide": true,
					"ordering": false,
					"ajax":{
						url :Path, // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".employee-grid-error").html("");
							$("#example2").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
							
						}
					}
				} );
}
 


</Script>