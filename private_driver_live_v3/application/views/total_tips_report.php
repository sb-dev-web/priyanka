

 <div id="data1" class="record">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                     
                        <th>Name</th>
                        <th>Date</th>
                        <th>Total Tips</th>
                       
                       <!-- <th>Country</th>-->
                        
                      
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                    //print_r($total_report);
                    //exit;
                    if(sizeof($total_tips_report)>1)
                    {
                    for ($i = 0; $i < count($total_tips_report); ++$i) {
                        $date=$total_tips_report[$i]['createdon'];
                        ?>
                    <tr>
                       <td> <a href="javascript:void(0);" onclick="get_tips_report('<?php echo $total_tips_report[$i]['driver_id'];?>','<?php echo $date;?>')"><?php echo $total_tips_report[$i]['first_name'].' '.$total_tips_report[$i]['last_name']; ?></a>
                        </td>
                        <td><?php
                        $timestamp = strtotime($date);
                           echo date('d F Y', $timestamp);
                         ?></td>
                        <td><?php echo $total_tips_report[$i]['tips'];?></td>
                    </tr>
                    <?php }
                    }
                    else
                    {
                    ?>
                    <tr>
                      <td>No Data Found</td>
                       <td>No Data Found</td>
                        <td>No Data Found</td>
                    </tr>  
                    <?php
                    }
                    ?>
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
               
                
            <div class="row">
        <div class="col-md-12 text-right">
             <?php echo $link ; ?>
         
   </div>
  
  
  <!-- modal for select driver -->



<div class="modal fade" id="open_tips" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
               Tips Report
                <button style="color: black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
                <div style="text-align: center; margin: 0 auto; height:80%;width: 80%;">
                <div id="tips_data">
                 
                </div>    
                </div>
            </div>
        </div> 
    </div>   
</div>
  
  
 
 <script>
             
  function  get_tips_report(driver_id,date)
    {
               
         $('#open_tips').modal({
                 "backdrop" : "static"
         })      
     
     $.ajax({
            type:'POST',
            url: '<?php echo site_url('Activity/Get_tips_booking');?>',
            data:{"date":date,"driver_id":driver_id},
            success: function(response) {
            //alert('response');
             $("#tips_data").html(response);
             }});
    }
 </script>

