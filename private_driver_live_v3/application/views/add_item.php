<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Order</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Order</a></li>
      <li class="active">Add Item</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="save_item" action="" method="post" enctype="multipart/form-data" autocomplete="off">
             <input type="hidden" value="<?=(isset($order[0]->item_id))?$order[0]->item_id:"";?>"  name="id" id="id" />
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Name</label>
                      <input type="text" class="form-control validate[required]" name="item_name" id="item_name" value="<?=(isset($order[0]->item_name))?$order[0]->item_name:"";?>" placeholder="Item Name">
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Code</label>
                      <input type="text" class="form-control validate[required]" name="item_code" value="<?=(isset($order[0]->item_code))?$order[0]->item_code:"";?>" id="item_code" placeholder="Item Code">
                  </div>  
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email_id">Price</label>
                       <input type="text" class="form-control validate[required]" name="item_price" value="<?=(isset($order[0]->item_price))?$order[0]->item_price:"";?>" id="item_price" placeholder="Item Price">
                  </div>  
                </div>
              </div>
              
               
              
              
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/uploader.js"></script>
    
<script type="text/javascript">
//var $ = jQuery.noConflict();
$(document).ready(function(){

    function submit_form(formData,status)
    {
        if(status==true)
        {
            var querystr = $("#save_item").serialize();
            
            
            
            $.ajax({
                url		:	"<?php echo site_url('Generalinfo/submit_order'); ?>",
                type	:	"POST",
                data	:	querystr,
                //async: false,
               // cache: false,
                //contentType: false,
                //processData: false,
                
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                  if(data == 1)
                  {
                    $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl("Your record is saved", {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_item')[0].reset();
                  }
                  else
                  {
                    $("#loader").hide();
                    //window.location='<?php echo site_url('login/dashboard'); ?>';
                    //alert('failed to save');
                  }
                 }
            });
            return false;
        }
        
    }
            
      
    
  $("#save_item").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});
</script>            