
<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tips Reports
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active"></li>
          </ol>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         
                   
          
          
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
             
             
            </div>
            <div class="box-body">
                
             <label>Date</label> 
            <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>                                              
              </div>
            <input type="text" class="form-control pull-left" id="bd" name="bd" onchange="getbookingdata();" style="width:25%;" value=""/> 
           <br/><img id="loading-image" src="http://dev.privatedriverapp.com/app/images/loader19.gif" style="display:none;"/>
            </div>
            <br/> <br/>
            <div id="showreport">
            
             
            </div>
            <div id="show_report_data"></div>
        

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy;2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    

<script>
  
     $(function() {
                    $( "#bd" ).datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true
                       
                        });
                   
                      });


</script>
<script>
  
  function  getbookingdata()
    {
        var date=$('#bd').val();
       
       $.ajax({
            type:'POST',
            url: '<?php echo site_url('Activity/Tips');?>',
            data:{"date":date},
             beforeSend: function() {
              $("#loading-image").show();
           },
            success: function(response) {
            
             $("#showreport").html(response);
              $("#loading-image").hide();
             }});
    }
    
</script>
