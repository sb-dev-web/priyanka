
<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Turn Over Reports
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Turn Over Report</li>
          </ol>
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         
                   
          
          
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
             
             
            </div>
            <div class="box-body">
                
             <label>Date</label> <br>
            <div class="input-group">
           
                                                      
              
            <input type="text" class="form-control pull-left" id="bd" name="bd"  style="width:115%;" value=""/>
            </div>
            
          
     
            </div>
            <button type="submit" class="btn btn-primary" id="submit" name="submit" style="margin-left:270px;margin-top:-73px;height:32px;" onclick="getbookingdata();">Submit</button>
            <br/><img id="loading-image" src="http://dev.privatedriverapp.com/app/images/loader19.gif" style="display:none;"/> <br/>
            <center><img id="loading-image1" src="http://dev.privatedriverapp.com/app/images/loading.gif" style="display:none;" height="100x" width="100px"/>
            <div id="showreport">
             
            </div>
            
            </center>
             
            <div id="show_report_data"></div>
        

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy;2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    

<script>
  
     $(function() {
                    $( "#bd" ).daterangepicker({
                          locale: {
                         format: 'yy-mm-dd'
                      },
                             autoclose: true
                     
                        //}).on('hide.daterangepicker', function (ev, picker) {
  //$('.table-condensed tbody tr:nth-child(2) td').click();
             
                      });
                   
                      });

</script>
<script>
  
  function  getbookingdata()
    {
        var booking_date=$('#bd').val();
       
        if (booking_date=='') {
       alert("Please select Date range");
       }
     else{
       $.ajax({
            type:'POST',
            url: '<?php echo site_url('Activity/turn_over_report');?>',
            data:{"date":booking_date},
           timeout: 3000,
             
             beforeSend: function() {
              $("#loading-image").show();
           },
            success: function(response) {
            
             $("#showreport").html(response);
              $("#loading-image").hide();
             }
             });
    }
    }
</script>
