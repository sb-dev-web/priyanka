<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

	function __construct()
    {
        parent::__construct();
       $this->load->helper(array('form','url', 'constants_helper','function_helper'));
	     $this->load->model('Booking_model');
	$this->load->library(array('session', 'email','pagination','paginationlib','Twilio','curl'));
	 $this->load->library('xmpp', false);
		$user = $this->session->userdata('logged_in');
        if (!$user) {
            redirect('Welcome');
        }
		
    }
    
	public function Pre_booking()
	{
		$this->load->library('Ajax_pagination');
        $this->Booking_model->count_advance_booking();
        $pagingconfig = $this->paginationlib->initPagination("/Booking/advance_ajaxPaginationData/", $this->
            Booking_model->count_advance_booking(),10, 1,array());


        $this->ajax_pagination->initialize($pagingconfig);
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //$data['link'] = $this->pagination->create_links();
        $data['link'] = $this->ajax_pagination->create_links();
       	$advance_booking=$this->Booking_model->fetch_advance_booking($pagingconfig['per_page'], 0);
       
        $data['advance_booking'] = $advance_booking;
		$driver=$this->Booking_model->fetch_driver();
		$data['advance_booking']=$advance_booking;
		$data['driver']=$driver;
		$data['active_page'] = 'booking';
        $this->load->view('site-nav/header', $data);
        $this->load->view('booking');
		$this->load->view('site-nav/footer');
	}
	
	
	  function advance_ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Booking/advance_ajaxPaginationData/", $this->
           Booking_model->count_advance_booking(),10, $post['page']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['advance_booking'] = $this->Booking_model->fetch_advance_booking($pagingconfig['per_page'],$page);
       // $driver=$this->Booking_model->fetch_driver();
		//$data['driver']=$driver;
        //load the view
        $this->load->view('advance_booking_ajax', $data, false);
    }
    public function perPage()
    {
        $this->load->library('Ajax_pagination');
        return 2;
    }
		
/* advance booking assgin */	
	function advance_booking()
	{
		$bookingid=@$_POST['booking_id'];
		$driverid=@$_POST['driverid'];
	
	   /* if driver already assgin than send sms to previous driver */
	   $previous_driver=$this->Booking_model->get_previous_driver($bookingid);
	   if($previous_driver)
	   {
		$pre_driver_contact=$previous_driver[0]['contact'];
		$bookin_no=$previous_driver[0]['booking_number'];
			$message=NEW_DRIVER_ASSGIN.' '.$bookin_no;
		send_sms($pre_driver_contact,$message);
	   }
	
	  $update_advance=$this->Booking_model->update_booking($bookingid,$driverid);
	  if($update_advance)
	  {
	
		$fetch_driver_no=$this->Booking_model->get_driver_number($driverid);
		
		$driver_contact=$fetch_driver_no[0]['contact'];
		
		 $message =ADVACNE_BOOKING;
        send_sms($driver_contact,$message);
		
		
		echo 1;
	  }
	  else
	  {
		echo 0;
	  }
	}
	
/* function for completed_booking */

function completed_booking()
{

       $this->load->library('Ajax_pagination');
        $this->Booking_model->fetch_Completed_booking_count();
        $pagingconfig = $this->paginationlib->initPagination("/Booking/complete_ajaxPaginationData/", $this->
            Booking_model->fetch_Completed_booking_count(),10,1,array());


        $this->ajax_pagination->initialize($pagingconfig);
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //$data['link'] = $this->pagination->create_links();
        $data['link'] = $this->ajax_pagination->create_links();
        	$completed_booking=$this->Booking_model->fetch_Completed_booking($pagingconfig['per_page'], 0);
       
        $data['completed_booking'] = $completed_booking;
		$data['active_page'] = 'completed_booking';
		$this->load->view('site-nav/header', $data);
		$this->load->view('completed_booking');
	    $this->load->view('site-nav/footer');
	
}

 function complete_ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
		
         $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Booking/complete_ajaxPaginationData/", $this->
           Booking_model->fetch_Completed_booking_count(),10, $post['page']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
         $data['completed_booking'] = $this->Booking_model->fetch_Completed_booking($pagingconfig['per_page'],$page);
         //print_r($data);
        $this->load->view('completed_booking_ajax', $data, false);
    }
   









/* change payment status */
function change_payment()
{ 
         $id=$_POST['id'];
		$status=$_POST['status'];
		$payment_status=$this->Booking_model->change_payment_status($id,$status);
	    echo $payment_status;
	
}







/* function for fetch current booking */

function current_booking()
{
	
	   $this->load->library('Ajax_pagination');
        $this->Booking_model->fetch_current_booking_count();
        $pagingconfig = $this->paginationlib->initPagination("/Booking/current_ajaxPaginationData/", $this->Booking_model->fetch_current_booking_count(),10, 1,array());
		 $this->ajax_pagination->initialize($pagingconfig);
         $data['link'] = $this->ajax_pagination->create_links();
        $current_booking=$this->Booking_model->fetch_current_booking($pagingconfig['per_page'], 0);
        $data['current_booking'] = $current_booking;
		$data['active_page'] = 'current_booking';
		$this->load->view('site-nav/header', $data);
		$this->load->view('current_booking');
	    $this->load->view('site-nav/footer');
}

function current_ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Booking/current_ajaxPaginationData/", $this->Booking_model->fetch_Completed_booking_count(),10, $post['page']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
         $data['current_booking'] = $this->Booking_model->fetch_current_booking($pagingconfig['per_page'],$page);
        $this->load->view('current_booking_ajax', $data, false);
    }







/* chnage current booking status */
function change_trip_end()
{
	$booking_id=$_POST['bookingid'];
	$driver_id=$_POST['driver_id'];
	$km_driven=$_POST['km_driven'];
	$journey_time=$_POST['journeytime'] * 60;

	
	$isadmin='Y';
	  $params = array('booking_id'=>$booking_id, 'driver_id'=>$driver_id, 'km_driven'=>$km_driven,'journey_time'=>$journey_time);
	 $url = "https://privatedriverapp.com/app/api/Booking/trip_end";
                        $ch = curl_init();
                        curl_setopt_array($ch, array(
                        CURLOPT_URL => $url,
                        CURLOPT_POSTFIELDS => $params,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_VERBOSE => true
                        ));
                        $result = curl_exec($ch);
						//curl_close($ch);
						//print_r($result);
						if($result)
						{
							//echo 'trip end';
							$get_result=json_decode($result);
							//print_r($get_result);
							$status=$get_result->status;
							if($status=='1')
							{
								//echo 'hello';
								
								$pluginurl ="http://privatedriverapp.com:9090/plugins/privatedriverlive/service/endtrip";
								$ch = curl_init();
								curl_setopt_array($ch, array(
								CURLOPT_URL => $pluginurl,
								CURLOPT_CUSTOMREQUEST => "POST",
								CURLOPT_POSTFIELDS => $result,
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_SSL_VERIFYPEER => false,
								CURLOPT_VERBOSE => true,
								CURLOPT_HTTPHEADER => array('Authorization:T36HCEyK',
															'Content-Type: application/json',                                                                                
                                                          'Content-Length: ' . strlen($result))
								));
								$plugin_result = curl_exec($ch);
								//echo $pluginurl;
								//echo $plugin_result;
								if($plugin_result)
								{
									echo 1;
								}
								if(!curl_errno($ch))
								{
									echo 1;
								 $info = curl_getinfo($ch);
								// print_r($info);
								 //echo 'Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url'];
								}
								curl_close($ch);
							}
							//curl_close($ch);
						}
						else
						{
							echo 1;
						}
}

function get_advance_booking()
{
	$booking_no=$_POST['booking_no'];
	$data['get_data']=$this->Booking_model->get_pre_booking($booking_no);
	
	$this->load->view('advance_pop_up',$data);
}
/* fetch complete booking */
function get_completed_booking()
{
	$booking_no=$_POST['booking_no'];
	$data['get_data']=$this->Booking_model->get_complete_booking($booking_no);
	
	$this->load->view('completed_pop_up',$data);
}
}
