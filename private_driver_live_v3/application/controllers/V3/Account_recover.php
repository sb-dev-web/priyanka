<?php

	defined('BASEPATH') or exit('No direct script access allowed');
	require APPPATH . '/libraries/REST_Controller.php';

	class account_recover extends REST_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> library(array(
				'upload',
				'Braintree_lib',
				'Twilio',
				'Cipher',
				'email'
			));
			$this -> load -> library('Openfire', false);
			$this -> load -> helper(array(
				'form',
				'url',
				'string',
				'constants_helper',
				'function_helper',
				'braintree_function'
			));

			$this -> load -> model('V3/Payment_model');
			$this -> load -> model('V3/Userbk_model');

		}

		function test_Bt()
		{
			echo "<pre>";
			$collection = Braintree_Customer::all();
			// echo "count==> ".count($collection['customer']);
			foreach ($collection as $customer)
			{
				print_r($customer);
			}
		}

		function updatecard()
		{
			//customer id 11190576
			//email priyanka@foxinfosoft.com
			//token 5dmy6r
			//last 0004
			/////////////////////////////////////
			//customer id 36845792
			//token 6wvn5m
			//last 1111
			$data = array(
				"nonce" => "0a6f8659-f42d-0662-1945-1fa7ea54336c",
				"braintree_customer_id" => "11190576",
				"token" => "5dmy6r"
			);
			print_r(UpdateUserCard($data));
		}

		function makeNonce()
		{
			echo "dsf";
			//print_r(paymentMethodNonce('6wvn5m'));
		}

		/*
		 function Customer_fill()
		 {
		 echo "<pre>";
		 error_reporting(1);

		 $collection = Braintree_Customer::all();
		 // echo "count==> ".count($collection['customer']);
		 foreach ($collection as $customer)
		 {
		 //echo $password = rand(11111,99999);
		 $password = $this -> randomKey(5);

		 //	print_r($customer->creditCards);

		 //print_r($customer);
		 //print_r($customer->createdA->DateTime);
		 //echo $customer->createdAt->DateTime->date;
		 //exit;
		 //echo "<br/>====".$customer->createdAt->DateTime['date'];
		 //$dd= $customer->createdAt->__toArray(true);
		 //print_r($dd);
		 //print_r($customer->createdAt[0]->date);
		 $i = 0;
		 foreach ($customer->createdAt as $value)
		 {
		 if ($i > 0)
		 {
		 continue;
		 }
		 $createdon = $value;
		 $i++;

		 }
		 //	exit;
		 $user_data = array(
		 "name" => urldecode($customer -> firstName),
		 //"lastName"=>$customer->lastName,
		 "email" => $customer -> email,
		 "phone" => $customer -> phone,
		 "password" => $password,
		 "country_code" => "+47",
		 "is_social" => "N",
		 "is_varify" => "1",
		 "is_emailverify" => "1",
		 "createdon" => $createdon
		 );
		 $card_data = array(
		 "braintree_customer_id" => $customer -> id,
		 "credit_card_no" => credit_card_number($customer -> creditCards[0] -> last4),
		 "card_type" => $customer -> creditCards[0] -> cardType,
		 "expire_date" => $customer -> creditCards[0] -> expirationMonth . '/' .
		 $customer -> creditCards[0] -> expirationYear,
		 "braintree_token" => $customer -> creditCards[0] -> token,
		 "createdon" => $createdon
		 );
		 // echo "============";
		 // print_r($user_data);
		 // print_r($card_data);
		 //exit;
		 $user_detail = array(
		 "user" => $user_data,
		 "card" => $card_data
		 );
		 //$this->Userbk_model->SaveUser($user_detail);

		 //echo $customer -> firstName;
		 //exit;
		 }
		 //$customers = Braintree_Customer::all();
		 }

		 function randomKey($length)
		 {
		 $pool = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

		 for ($i = 0; $i < $length; $i++)
		 {
		 $key .= $pool[mt_rand(0, count($pool) - 1)];
		 }
		 return $key;
		 }

		 */
		function Remove_duplicate()
		{
			error_reporting(1);
			echo "here<pre>";
			$user = $this -> Userbk_model -> duplicate_user();
			foreach ($user as $value)
			{

				//if ($value['count'] > 1)
				{

					$card_detail = $this -> Userbk_model -> Getuserids($value['email']);
					//Insert User into user_production table
					unset($value['id']);
					$user_id = $this -> Userbk_model -> SaveUser($value);
					$user_name = USER_JID_NAME . '-' . $user_id;
					$param = array(
						"username" => $user_name,
						"name" => $value['name'],
						"password" => $value['password'],
						"email" => $value['email']
					);

					$openfir_result = $this -> openfire -> openfire_action("POST", $param, "users/");
					//echo "================".$openfir_result."==============";
					$openfire_response = json_decode($openfir_result, true);
					if ($openfire_response['status'] == 1)
					{
						foreach ($card_detail as $cards)
						{
							$cards['customer_id'] = $user_id;
							unset($cards['id']);
							$card_id = $this -> Userbk_model -> SaveCards($cards);
							if (!$card_id)
							{
								echo "<br>Card Data no insert";
								//echo $this->db->last_query();
							}
							else
							{
								echo "<br>data enter";
							}
						}

					}
					else
					{
						echo "<br>Error in openfire";
						exit ;
					}
				}

			}
			//print_r($user);

		}

		function update_password()
		{
			$users = $this -> Userbk_model -> GetProductionUser();
			foreach ($users as $user)
			{
				$password = $this -> randomKey(8);
				$user_name = USER_JID_NAME . '-' . $user['id'];
				
				 $this->Userbk_model->UpdateNewPassword($user['id'],$user_name,$password);

			}

		}

		function randomKey($length)
		{
			$pool = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
			// $pool=md5($pool);
			for ($i = 0; $i < $length; $i++)
			{

				$key .= $pool[mt_rand(0, count($pool) - 1)];
			}
			$key;
			$pattern = ' ^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$ ';

			if (preg_match('/[A-Z]/', $key))
			{
				if (preg_match('/[0-9]/', $key))
				{
					return $key;
				}
				else
				{
					return $this -> randomKey(8);
				}

			}
			else
			{
				return $this -> randomKey(8);
			}

		}

	function smssend()
	{
		$userdata= $this->Userbk_model->GetProductionUser();
		//$phonedetail[]=array("country_code"=>"+91","phone"=>"7891380313");
		//$phonedetail[]=array("country_code"=>"+91","phone"=>"7737906424");
		//$phonedetail[]=array("country_code"=>"+91","phone"=>"9462099422");
		//country_code,phone
		foreach($userdata as $phone){
		echo "<br>". $phone_number=$phone['country_code'].$phone['phone'];
		$message="Private Driver App - Endring av passord
		";
		$message.="Hei. Vi har sendt deg en e-post slik at du kan endre ditt passord og fortsette og benytte vår app - nå med forbedret sikkerhet. Kontakt oss på  40 69 79 99 dersom du lurer på noe.
		";
		$message.="Hilsen Private Driver A / S";
		send_sms($phone_number, $message);
		usleep(200000);
		}
         
		echo "<br> SMS Sent To All PD Customer <br>Thanks."; 
	}

/*function setpassword()
{
	$users= $this->Userbk_model->GetUserpassword();
	foreach($users as $user)
	{
		$password =$user['password_normal'];
		$user_id=$user['id'];
		$this->Userbk_model->UpdatePassword($user_id,$password);
		
	}
	
}*/

   function sendmail()
   {
    $users=	$this->Userbk_model->GetUser();
	foreach ($users as $user) {
			$uniquecode = $user['password_recovercode'];
		echo "<br>".$to = $user['email'];
		
		//$registrationcode = $registrationcode;
		$userid = $user['id'];
		$cipher = new Cipher('PRIVATETAXI_APP');

		$encrypteduserid = $cipher -> encrypt($userid);
		$encryptedcode = $cipher -> encrypt($uniquecode);
		$md5id = $cipher -> encrypt('user');
	
echo 	$link ="http://privatedriverapp.com/app/resetpassword?userid=$encrypteduserid&code=$encryptedcode&id=$md5id";
//	exit;
	$click="<a href='http://privatedriverapp.com/app/resetpassword?userid=$encrypteduserid&code=$encryptedcode&id=$md5id'>klikke her</a>";
   	$subject ="Private Driver App - oppgradering av sikkerhetssystemer";
	
   	$body="<div style='width: 100%;margin: 10px;padding: 10px;size:15px'>

	<table align='center' cellpadding='0' cellspacing='0'>
		<tbody><tr><td align='center' width='550'>
		<table align='center' cellpadding='0' cellspacing='0'>
		<tbody><tr><td align='center' width='550'>
		<table width='550' border='0' cellspacing='0' cellpadding='0' style='border:1px solid #cccccc;border-top-left-radius:7px;border-top-right-radius:7px;border:1px solid #cccccc;margin:0 auto;padding:0px!important;width:100%!important;max-width:550px!important'>
		<tbody>
									<tr>
										<td valign='top'>
										<table  border='0' cellspacing='0' cellpadding='0' width='100%' style='padding-top:5px;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom:1px solid #cccccc;margin:0 auto'>
											<tbody>
												<tr>
												<td align='center'  valign='top'  style='padding:10px;width:50%; text-align:center'><img src='http://privatedriverapp.com/app/logo100.jpeg' border='0' alt='Private Driver'  ></td>
												</tr>	
											</tbody>
										</table>";
				
				$body.='<p style="padding: 10px">Kjære kunde.</p>';
				$body.='<p style="padding: 10px">Vi har oppgradert våre sikkerhetssystemer og beklager den ulempen dette har medført de siste dagene. Nå er oppgraderingen fullført og vi ber deg endre ditt passord.';
				$body.="Vi har laget et midlertidig passord som er <b><i>".$user['pass'];
				$body.="</i></b>.Du kan enten benytte dette til og logge deg inn på appen og endre passordet ditt der, eller du kan $click og endre passordet, for deretter og logge deg inn med det nye passordet.
				</p>";
				$body.="<p style='padding: 10px'>Vi beklager ulempen, men setter sikkerheten i høysetet. Vi er åpne 24/7 hele sommeren, både for øyeblikkelig booking og forhåndsbooking og har gleden av og presentere vår nye 16-seter som du kan forhåndsbestille på appen. Og huks du kan alltid ringe oss på 40 69 79 99 eller sende oss oss en e-post på booking@privatedriver.no dersom du trenger våre komfortable S-klasser, vår 9-seter eller den nye 16-seteren.</p>";
				$body.="<p</p><p style='padding: 10px'>Takk for hjelpen og riktig god sommer<p>";
				$body.="<p style='padding: 10px'>Hilsen</p><p style='padding: 10px'>Private Driver A/S</p>";
				$body .= "</div>";
				
				
				$config['protocol'] = 'smtp';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';

			$config['smtp_host'] = EMAIL_HOST;
			$config['smtp_user'] = EMAIL_USERNAME;
			$config['smtp_pass'] = EMAIL_PASSWORD;
			$config['smtp_port'] = EMAIL_PORT;
			$config['email_smtp_crypto'] = 'tls';
			$config['email_newline'] = '\r\n';

			
			
			$this -> email -> initialize($config);
            $this -> email -> set_newline("\r\n");
		$this -> email -> from(ADMINMAIL_FROM, 'Private Driver');
		$this -> email -> to($to);
		//echo $body;
		//$ci -> email -> cc("priyanka@foxinfosoft.com");
		$this -> email -> subject($subject);
		$this -> email -> message($body);
		$this->email->reply_to(ADMINMAIL, 'Private Driver App');
		$data = $this -> email -> send();
		if($data){
		//	echo "<br>SLEEP";
		//usleep(200000);
		echo "sent";
		}else
		{
				echo "ERROR";
				echo   $this->email->print_debugger();
			exit;
		}
	
	}
	
	echo "<br> Recover Mail Sent To All PD Customer <br>Thanks.";
   }




}
?>
