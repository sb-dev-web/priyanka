<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Restaurant extends CI_Controller
{

    function __construct()
    {
         parent::__construct();
        $this->load->helper(array(
            'form',
            'url',
            'function_helper',
            'constants_helper'));
        $this->load->model('Restaurant_model');
        $this->load->library(array(
            'session',
            'email',
            'paginationlib',
            'Pagination',
            'Twilio'));
        $this->load->library('xmpp', false);
         $user = $this->session->userdata('logged_in');
        if (!$user)
        {
            redirect('Welcome');
        }

    }
    

    public function restaurant()
    {
       $this->load->library('Ajax_pagination');
       $this->Restaurant_model->restaurant_count();
        $pagingconfig = $this->paginationlib->initPagination("/Restaurant/ajaxPaginationData/", $this->
            Restaurant_model->restaurant_count(), 10, 3,array());


        $this->ajax_pagination->initialize($pagingconfig);
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //$data['link'] = $this->pagination->create_links();
        $data['link'] = $this->ajax_pagination->create_links();
        $restaurant = $this->Restaurant_model->fetch_total_restaurant($pagingconfig['per_page'], 0);
       
        $data['restaurant'] = $restaurant;
        $data['active_page'] = 'restaurant';
        $this->load->view('site-nav/header', $data);
        $this->load->view('restaurant');
        $this->load->view('site-nav/footer');
    }
    function ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Restaurant/ajaxPaginationData/", $this->
            Restaurant_model->restaurant_count(), 10, $page['post']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['driver'] = $this->Restaurant_model->fetch_total_restaurant($pagingconfig['per_page'],
            $page);

        //load the view
        $this->load->view('restaurant_ajax', $data, false);
    }
    public function perPage()
    {
        $this->load->library('Ajax_pagination');
        return 2;
    }

    public function change_status()
    {
        $id = $_POST['id'];
        $status = $_POST['status'];

        $restaurant_status = $this->Restaurant_model->change_restaurant_status($id, $status);
        echo $restaurant_status;
    }
    public function add_restaurant()
    {
        //$restaurant=$this->Driver_model->add_driver();
        //print_r($restaurant);
        //$data['driver']=$restaurant;
        @$restaurant[0]->name = $restaurant[0]->address = $restaurant[0]->latitude = $restaurant[0]->
            longitude = $restaurant[0]->contact = $restaurant[0]->email= "";
        $restaurant = "";
        if ($this->uri->segment(3) == false)
        {

            $id = 0;

        }
        else
        {
            $id = $this->uri->segment(3);
            $restaurant = $this->Restaurant_model->get_restaurant($id);
        }
        $data['restaurant'] = $restaurant;
        $data['active_page'] = 'restaurant';
        $this->load->view('site-nav/header', $data);
        $this->load->view('add_restaurant');
        $this->load->view('site-nav/footer');
    }

    public function submit_restaurant()
    {
        $id = $_POST['id'];
        $res = $this->Restaurant_model->save_restaurant($id);
        if ($res)
        {
            if ($res != 'updated')
            {
                $user_name = 'restaurant'.'-'. $res;
                $password = $_POST['password'];
                $name = $_POST['name'] ;
                $email = $_POST['email'];

                $param = array(
                    $user_name,
                    $password,
                    $name,
                    $email);
                //$this->xmpp->initialize(false);

                $this->xmpp->api("add", $param);

                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = true;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('support@privatedriver.com', 'Private_driver ');
                $this->email->to($_POST['email']);
                $this->email->subject('Your information is');
                $link = "<p>Thanks for Joining Private Driver.Your Login information is:<br/><br/>";
                $link .= "<br>Your username is: &nbsp;&nbsp;" . $_POST['email'];
                $link .= "<br>Your Password is: &nbsp;&nbsp;" . $_POST['password'];
                $this->email->message($link);
                $this->email->send();
                echo '1';

            }
            else
            {
                $user_name = 'restaurant' .'-'. $id;
                $name = $_POST['name'];
                $update_password = $_POST['password'];
                if (empty($update_password))
                {
                    $param = array($user_name, $name);
                    //print_r($param);
                    $this->xmpp->api("update", $param);
                }
                else
                {
                    $param = array(
                        $user_name,
                        $name,
                        $update_password);
                    //print_r($param);
                    $this->xmpp->api("update", $param);
                }


            }


        }

        else
        {
            echo 0;
        }
        //echo $res;
    }
    function change_delete_status()
    {
        $id = $_POST['id'];
        echo $this->Restaurant_model->delete_restaurant($id);
    }

    function check_email()
    {
        $email = $_POST['email'];
        echo $this->Restaurant_model->check_restaurant_email($email);
    }
    
    
    
    /* restaurant booking detail table */
    public function Bookings()
    {
       $this->load->library('Ajax_pagination');
       $this->Restaurant_model->Bookings_count();
        $pagingconfig = $this->paginationlib->initPagination("/Restaurant/Booking_ajaxPaginationData/", $this->
            Restaurant_model->Bookings_count(), 10, 3,array());


        $this->ajax_pagination->initialize($pagingconfig);
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //$data['link'] = $this->pagination->create_links();
        $data['link'] = $this->ajax_pagination->create_links();
        $restaurant_booking = $this->Restaurant_model->fetch_total_bookings($pagingconfig['per_page'], 0);
       
        $data['restaurant_booking'] = $restaurant_booking;
        $data['active_page'] = 'restaurant_booking';
        $this->load->view('site-nav/header', $data);
        $this->load->view('restaurant_booking');
        $this->load->view('site-nav/footer');
    }
    function Booking_ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Restaurant/Booking_ajaxPaginationData/", $this->
            Restaurant_model->Bookings_count(), 10, $page['post']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['restaurant_booking'] = $this->Restaurant_model->fetch_total_bookings($pagingconfig['per_page'],
            $page);

        //load the view
        $this->load->view('restaurant_booking_ajax', $data, false);
    }
    
    
    
    
    /* restaurant pre booking */
    
  public function Pre_booking()
	{
		 $this->load->library('Ajax_pagination');
        $this->Restaurant_model->count_advance_booking();
        $pagingconfig = $this->paginationlib->initPagination("/Restaurant/advance_ajaxPaginationData/", $this->
            Restaurant_model->count_advance_booking(),10, 1,array());

        $this->ajax_pagination->initialize($pagingconfig);
        
        $data['link'] = $this->ajax_pagination->create_links();
        	$advance_booking=$this->Restaurant_model->fetch_advance_booking($pagingconfig['per_page'], 0);
       
        $data['advance_booking'] = $advance_booking;
		$driver=$this->Restaurant_model->fetch_driver();
		$data['advance_booking']=$advance_booking;
		$data['driver']=$driver;
		$data['active_page'] = 'restaurant_pre_booking';
        $this->load->view('site-nav/header', $data);
        $this->load->view('restro_advance_booking');
		$this->load->view('site-nav/footer');
	}
	
	
	  function advance_ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Restaurant/advance_ajaxPaginationData/", $this->
           Restaurant_model->count_advance_booking(),10, $post['page']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['advance_booking'] = $this->Restaurant_model->fetch_advance_booking($pagingconfig['per_page'],$page);
       // $driver=$this->Booking_model->fetch_driver();
		//$data['driver']=$driver;
        //load the view
        $this->load->view('restro_advance_booking_ajax', $data, false);
    }
    
    
    /* advance booking assgin */	
	function assgin_pre_booking()
	{
		$bookingid=@$_POST['booking_id'];
		$driverid=@$_POST['driverid'];
	
	  $update_advance=$this->Restaurant_model->update_booking($bookingid,$driverid);
	  if($update_advance)
	  {
	
		$fetch_driver_no=$this->Restaurant_model->get_driver_number($driverid);
	    $driver_contact=$fetch_driver_no[0]['contact'];
		$message =ADVACNE_BOOKING;
       send_sms($driver_contact,$message);
		
		
		echo 1;
	  }
	  else
	  {
		echo 0;
	  }
	}
    
    
    /* get restaurant completed booking */
    function get_restaurant_completed_booking()
{
	$booking_no=$_POST['booking_no'];
	$data['get_data']=$this->Restaurant_model->get_rcomplete_booking($booking_no);
	
	$this->load->view('restro_complete_popup',$data);
}
   
}
