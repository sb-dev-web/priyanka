<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" id="check">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Booking
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Unpaid Booking</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              
             
            </div>
            <div class="box-body record">
              <br>


            
            
     
            <div class="box-body">
              <div style="width:auto;position: relative; top:-36px;">
            <img src="<?php  echo base_url('images/printer.png') ?> " onclick="PrintElem('#printdiv')"  style="float:right" />     
     </div>
      
              <div id="printdiv">
              	
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Booking #</th>
                      
                        
                        
                       <th>Booking Time</th>
                       <!-- <th>Pickup_Address</th>
                        <th>Destination_Address</th>-->
                       
                        <th>User Paid</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                     
                      if(sizeof($completed_booking))
                      
                      {
                        for($i=0;$i<sizeof($completed_booking);$i++)
                        {
                         
                         
                      ?>
                      <tr>
                        
                       
                      
                       <td><a href="javascript:void(0);" onclick="customer_detail('<?php echo $completed_booking[$i]['id'];?>');"><?php echo $completed_booking[$i]['booking_number'];?></a></td>
                        <td><?php  $timestamp = strtotime($completed_booking[$i]['booking_time']);
                           echo date('d F Y', $timestamp);?></td>
                                               
                         <td><?php  $amount=$completed_booking[$i]['user_paid'];
                                if($amount>0)
                                {
                                  echo $completed_booking['pickup_location'];
                                ?>
                                <span class="label label-success">Yes</span>
                                 
                                <?php
                                }
                                else
                                {
                                ?>
                                 <span  class="label label-warning">No&nbsp;</span></a>
                                <?php
                                }
                                
                             
                          ?></td>
                        
              
                       
                      
                      </tr> 
                      
                      <?php
                      
                        }
                        
                      }
                      ?>


                      
                      
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
            </div>
                           <div class="row">
                                 
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
       <!-- open customer pop up -->
        <!-- open customer pop up -->
<div class="modal fade" id="open_customer" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-nm">
        <div class="modal-content background" style="max-width: none; background-color: #f39c12; padding: 0px!important; border:2px solid black;">
            <div class="modal-header" style="border-bottom: none; color:white!important; ">
              Unpaid Booking Detail
                <button style="color:black!important;" type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
            </div>
        </div>
        <div class="modal-content background" style="max-width: none; margin-top: -11px;border:2px solid black;">
            <div class="modal-body" id="modal_body">
               
                    
                </div>
                
               <div id="showreport" name="showreport"></div>
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
    </div>   
</div>
  
                 <div id="showreport" name="showreport"></div>
                    
                    <div style="margin-top: 30px;">
                        
                        </span>
                    </div>
                 
                </div>
            </div>
        </div> 
    </div>   
</div>
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b></b> 
        </div>
        <strong>Copyright &copy; 2016 <a href="http://www.privatedriver.no/" target="_blank">Private Driver</a>.</strong> All rights reserved.
      </footer>
      
     
      </aside><!-- /.control-sidebar -->

     

 <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php echo base_url();?>js/my_custom.js"></script>
<script>
  function customer_detail(booking_no)
  {
       $.ajax({
            type:'POST',
            url: '<?php echo site_url('Booking/get_completed_booking');?>',
            data:{"booking_no":booking_no},
             beforeSend: function() {
              $("#loading-image").show();
           },
            success: function(response) {
            //alert(response);
             $("#showreport").html(response);
             $('#open_customer').modal({
                 "backdrop" : "static"
         })
              $("#loading-image").hide();
             }});
             }
          
          
        
</script>

<script type="text/javascript">
$( document ).ready(function() {
    var dataTable = $('#example1').DataTable({
  "ordering": false
});
});
    function PrintElem(elem)
    {
      //alert('hiii');
    //	var elem="#printdiv";
        Popup($(elem).html());
    }

</script>
     

      