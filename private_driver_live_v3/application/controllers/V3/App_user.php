<?php

	defined('BASEPATH') or exit('No direct script access allowed');
	require APPPATH . '/libraries/REST_Controller.php';

	class App_user extends REST_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> library(array(
				'upload',
				'Braintree_lib',
				'Twilio'
			));
			$this -> load -> helper(array(
				'form',
				'url',
				'string',
				'constants_helper',
				'function_helper',
			));

			$this -> load -> model('V3/Appuser_model');

		}

		function Appuser()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				"name",
				"email",
				"phone",
			));

			$unique_number = random_number();
			$input_method['code'] = $unique_number;
			$app_user = $this -> Appuser_model -> Add_appuser($input_method);
			if ($app_user)
			{
				AppUserMail($app_user);
				
				$this -> response(array(
					'message' => APP_USER_MESSAGE,
					'status' => "1"
				), 200);
			}
			else
			{
				$this -> response(array(
					'message' => SERVER_ERROR,

					'status' => "0"
				), 200);
			}

		}

		function Appuser_code()
		{
			$input_method = $this -> webservices_inputs();
			$this -> param_validate($input_method, array(
				
				"code"
			));
			$is_approve = $this -> Appuser_model -> App_usercode($input_method);
		
			if ($is_approve)
			{
				if ($is_approve['isactive'] == 1 && $is_approve['appcode'] == $input_method['code'])
				{
					
					//now make this code inactive
					if($is_approve['id']!=10 && $is_approve['id']!=49 && $is_approve['id']!=50 ){
					$this -> Appuser_model -> Appcode_deactive($is_approve['id']);
						}
					$this -> response(array(
						'message' => SUCCESS,
						'status' => "1"
					), 200);
				}
               elseif($is_approve['isactive']==0 && $is_approve['appcode']==$input_method['code'])
				{
					$this -> response(array(
						'message' => APP_CODE_ALREADY_USED,
						'status' => "0"
					), 200);
				}elseif($is_approve['appcode']!=$input_method['code'])
				{
					$this -> response(array(
						'message' => APP_CODE_NOTEXIST,
						'status' => "0"
					), 200);
				}

			}
			else
			{
				$this -> response(array(
					'message' => APP_USER_NOT_EXIST,
					'status' => "0"
				), 200);
			}

		}

	}
?>
