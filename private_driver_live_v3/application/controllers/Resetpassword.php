<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Resetpassword extends CI_Controller
{

    function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->helper(array('form', 'url','constants_helper','function_helper'));
        $this->load->model('Resetpassword_model');
        $this->load->library('Cipher');
		$this->load->library('Openfire');
         $this->load->library('xmpp', false);
        //$this->clear_cache();
    }

    function index()
    {
        $data['userid'] = ($_GET['userid']);
        $data['code'] = $_GET['code'];
        $data['table']=$_GET['id'];
        $cipher = new Cipher('PRIVATETAXI_APP');
         $data['userid'] =$encrypteduserid = $cipher->decrypt(($_GET['userid']));
        $data['code']= $encryptedcode = $cipher->decrypt($data['code']);
		$data['table']=$cipher->decrypt($_GET['id']);
		//print_r($data);
        $user_detail=$this->Resetpassword_model->get_user_detail($data);
        if($user_detail){
        $data['email']=$user_detail[0]['email'];
        $data['id']=$user_detail[0]['id'];
        $data['name']=$user_detail[0]['name'];
        $this->load->view('setpassword', $data);
        }
        else
        {
          $data['notallow']=1;
          $this->load->view('setpassword', $data);  
        }
    }
    function submit()
    {
     
 		$set_password = $this -> Resetpassword_model -> set_user_password($_POST);
		if ($set_password)
		{
			$update_password = $_POST['form-password'];
			$user_name = USER_JID_NAME . '-' . $_POST['id'];
			$name = $_POST['name'];
			

			//$this->xmpp->api("update", $param);

			$param = array(
				"username" => $user_name,
				"name" => $name,
				"password" => $update_password,
			);

			//$this->xmpp->initialize(false);

			//$d_data= $this -> xmpp -> api("add", $param);
			$openfir_result = $this -> openfire -> openfire_action("PUT", $param, "users/$user_name");
			$openfir_result=json_decode($openfir_result,TRUE);
			if ($openfir_result['status'] == 1)
			{
				echo json_encode(array("IsValid" => 1));
			}
			else
			{
				echo json_encode(array("IsValid" => 0));
			}

		}
		else
		{
			echo json_encode(array("IsValid" => 0));
		}

	}

		function AccountVerify()
		{
		$data['userid'] = ($_GET['userid']);
        $data['code'] = $_GET['code'];
		$cipher = new Cipher('PRIVATETAXI_APP');
        $data['userid'] =$encrypteduserid = $cipher->decrypt(($_GET['userid']));
        $data['code']= $encryptedcode = $cipher->decrypt($data['code']);
        $user_detail=$this->Resetpassword_model->account_verify($data);
		//echo "<pre>";
		//print_r($user_detail);
        if($user_detail['status']==1 && $user_detail['account_type']=="NOT_VERIFY")
        {
        	$this->WelcomeMail($data);
        	//if OTP verify make this entry in user table 
        	if($user_detail['is_verify']==1)
			{
				//mobile already verify make it actuall user
				$user_id=$this->Resetpassword_model->VerifyUser($user_detail['user_id']);
				if(!$user_id)
				{
					//send mail to admin 
					ERRORUPDATETOADMIN($data);
				}
		
			}
        $data['is_emailverify']=$user_detail['email_verify'];
        $data['phone_verify']=$user_detail['is_verify'];
        $data['name']=$user_detail['username'];
        $this->load->view('account_verify', $data);
        }
        elseif($user_detail['status']==1 && $user_detail['account_type']=="VERIFY")
		{
			
			
			
			$data['is_emailverify']=$user_detail['email_verify'];
            $data['phone_verify']=$user_detail['is_verify'];
            $data['name']=$user_detail['username'];
			$data['account_type']="VERIFY";
			$this->WelcomeMail($data);
            $this->load->view('account_verify', $data);
		}
        else
        {
          $data['notallow']=1;
          $this->load->view('account_verify', $data);  
        }
	}
    


 
function WelcomeMail($data)
{
		
		extract($data);
		$ci = &get_instance();
		$ci -> load -> library('email');
     $config['protocol'] = 'smtp';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = true;
			$config['mailtype'] = 'html';

			$config['smtp_host'] = EMAIL_HOST;
			$config['smtp_user'] = EMAIL_USERNAME;
			$config['smtp_pass'] = EMAIL_PASSWORD;
			$config['smtp_port'] = EMAIL_PORT;
			$config['email_smtp_crypto'] = 'tls';
			$config['email_newline'] = '\r\n';

			
			$ci -> email -> clear();
			$ci -> email -> initialize($config);
$ci -> email -> set_newline("\r\n");

		

		$ci -> email -> set_newline("\r\n");
		$ci -> email -> from(ADMINMAIL_FROM, 'Private Driver App');
		$ci -> email -> to($email);

		

		$message = "<div style='width: 100%;margin: 0px;padding: 0px;'>

	<table align='center' cellpadding='0' cellspacing='0'>
		<tbody>
			<tr>
				<td align='center' width='550'>
				<table align='center' cellpadding='0' cellspacing='0'>
					<tbody>
						<tr>
							<td align='center' width='550'>
							<table width='550' border='0' cellspacing='0' cellpadding='0' style='border:1px solid #cccccc;border-top-left-radius:7px;border-top-right-radius:7px;border:1px solid #cccccc;margin:0 auto;padding:0px!important;width:100%!important;max-width:550px!important'>
								<tbody>
									<tr>
										<td valign='top'>
										<table  border='0' cellspacing='0' cellpadding='0' width='100%' style='padding-top:5px;border-top-left-radius:7px;border-top-right-radius:7px;border-bottom:1px solid #cccccc;margin:0 auto'>
											<tbody>
												<tr>
												<td align='center'  valign='top'  style='padding:10px;width:50%; text-align:center'><img src='http://privatedriverapp.com/app/logo100.jpeg' border='0' alt='Private Driver'  ></td>
												</tr>	
											</tbody>
										</table></td>
									</tr>

									<tr>
										<td style='border:0px solid #dbdbdb;padding:0px'>
										<table border='0' cellspacing='0' cellpadding='0' width='100%' style='border-left:0px solid #cccccc;border-right:0px solid #cccccc'>
											<tbody>
												
												<tr style='line-height:10px'>
													<td>&nbsp;</td>
												</tr>
												<tr bgcolor='#FFFFFF'>
													<td align='right' valign='top' style='font-size:21px;padding:0px;font-family:Arial,Helvetica,sans-serif;font-weight:normal;line-height:28px;color:#2d67b1'>
													<table width='100%' border='0' align='left' cellpadding='0' cellspacing='0' style='padding-left: 25px;'>
														<tbody style='padding:0px 20px;font-size:16px'>
														
															<tr>
																<td align='left'>
																<p style='text-align: justify;'>
																	<p>Hi $username,</p>
																	<p>Welcome to Private Driver, where transport becomes an experience.</p>
																	<p>Log on to your account using your login email: $email and your password.</p>
																  <p>We look forward to transport you on first class.</p>
																  </td>
															</tr>															<tr>
															<td style='text-align:left'><p>Sincerly yours, </p>
																  <p>Private Driver A/S</p>
															</td>
																
															</tr>
														</tbody>
													</table></td>
												</tr>
											</tbody>
										</table>
										<br>
										</table>
										
										
										</td>
									</tr>
								</tbody>
							</table></td>
						</tr>
					</tbody>
				</table>
			</div>
		";

		
		$ci -> email -> subject("Welcome to Private Driver");
		$ci -> email -> message($message);
		$ci -> email -> send();
		//echo "error_message" . $ci -> email -> print_debugger();
	
}
}
?>