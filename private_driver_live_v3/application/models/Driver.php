<?php

defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Driver extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library(array(
            'upload',
            'Twilio',
            'S3_lib'));

        $this->load->helper(array(
            'form',
            'url',
            'string',
            'constants_helper',
            'function_helper',
            ));
        $this->load->model('Driverapi_model');

    }


    /* driver login function */
    function user_login()
    {
        $input_method = $this->webservices_inputs();
        //$this->validate_param('login-driver', $input_method);
        $this->param_validate($input_method, array(
            "username",
            "password",
            "device_id"));
        $user_data = $this->Driverapi_model->check_login($input_method);
        // print_r($user_data);
        if ($user_data == -1)
        {
            $this->response(array('message' => ALREADYLOGIN, 'status' => 0), 200);
        }
        elseif ($user_data > 0)
        {

            foreach ($user_data as $user_detail)
            {
                /* if ($user_detail->cab_image)
                {
                $thumb_cab_image = get_thumb($user_detail->cab_image);
                }
                else
                {
                $thumb_cab_image = "";
                }*/
                $profile_image = ($user_detail->profile_pic == '') ? base_url() .
                    DRIVER_DEFAULT_IMAGE : base_url() . DRIVER_IMAGE_PATH . $user_detail->
                    profile_pic;
                //$cab_image_url = ($user_detail->cab_image == '') ? "" : base_url() .DRIVER_SHIFT_IMAGE_PATH . $thumb_cab_image;
                $driver_id = $user_detail->driver_id;
                $first_name = $user_detail->first_name;
                $last_name = $user_detail->last_name;
                $contact = $user_detail->contact;
                $gender = $user_detail->gender;
                $zipcode = $user_detail->zipcode;
                $city = $user_detail->city;
                $email_id = $user_detail->email_id;
                $j_id = DRIVER_JID_NAME . '-' . $driver_id;
                $jid = makejid($driver_id, DRIVER_JID_NAME);
                $status = $user_detail->status;
                $rating = $this->Driverapi_model->get_driver_rating($driver_id);

            }
            $this->response(array(
                'driver_id' => $driver_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email_id' => $email_id,
                'profile_image' => $profile_image,
                'j_id' => $j_id,
                'jid' => $jid,
                'rating' => "$rating",
                'status' => 1), 200);
        }

        else
        {
            $this->response(array('message' => DRIVER_LOGIN_ERROR, 'status' => 0), 200);
        }
    }
    /* uploading cab image when driver click */
    function upload_cab_image()
    {
        $input_method = $this->webservices_inputs();
        // $this->validate_param('cab-image', $input_method);
        $this->param_validate($input_method, array("driver_id", "cab_id"));
        $this->validateFile("cab_image");
        if (isset($_FILES['cab_image']))
        {

            if ($_FILES['cab_image']['error'] == 0)
            {

                $file_name = strtolower(random_string('alnum', 16));
                $config['upload_path'] = './images/shift/';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['file_name'] = $file_name;


                //$this->load->library('upload', $config);

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('cab_image'))
                {

                    //var_dump($_FILES);
                    $this->response(array('message' => $this->upload->display_errors(), 'status' =>
                            0), 200);
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                    // print_r($data);
                    $input_method['image'] = $data['upload_data']['file_name'];
                    $cab_image_id = $this->Driverapi_model->save_cab_image($input_method, $file_name);
                    $source = $config['upload_path'] . $input_method['image'];
                    $desc = $config['upload_path'] . $file_name . "_thumb" . $data['upload_data']['file_ext'];
                    $ext = $data['upload_data']['file_ext'];
                    $width = 200;
                    make_thumb($source, $desc, $width, $ext);
                    $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
                    $aws_upload = $s3->putObjectFile($source, AWS_BUCKET, $data['upload_data']['file_name'],
                        S3::ACL_PUBLIC_READ);
                    $aws_thumb_upload = $s3->putObjectFile($desc, AWS_BUCKET, $file_name . "_thumb" .
                        $data['upload_data']['file_ext'], S3::ACL_PUBLIC_READ);
                    if ($aws_thumb_upload && $aws_upload)
                    {
                        unlink($config['upload_path'] . $input_method['image']);
                        unlink($config['upload_path'] . get_thumb($input_method['image']));
                    }
                    if ($cab_image_id)
                    {

                        $this->response(array(
                            'message' => CAB_IMAGE_SAVE,
                            "cab_image_id" => $cab_image_id,
                            'status' => 1), 200);
                    }
                    else
                    {
                        $this->response(array('message' => SERVER_ERROR, 'status' => 0), 200);
                    }
                }
            }
        }
        else
        {
            $this->response(array('message' => FILENOTFOUND, 'status' => 0), 200);
        }
    }
    /* update driver password */

    function update_driver_password()
    {
        $input_method = $this->webservices_inputs();
        // $this->validate_param('change-driver-password', $input_method);
        $this->param_validate($input_method, array(
            "driver_email",
            "old_password",
            "new_password"));
        $user_data = $this->Driverapi_model->resetdriverPassword($input_method);
        if ($user_data['status'] == 1)
        {
            $this->response(array('message' => $user_data['message'], 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => $user_data['message'], 'status' => "0"), 200);
        }
    }

    /* shift start function */

    function shift_start()
    {
        $input_method = $this->webservices_inputs();
        //$this->validate_param('shift_started', $input_method);
        // echo "fvf";
        $this->param_validate($input_method, array(
            "cab_id",
            "driver_id",
            "latitude",
            "longitude",
            "cab_image_ids"));
        $user_data = $this->Driverapi_model->driver_shift($input_method);
          print_r($user_data);
        if ($user_data['status'] == 1)
        {
            $this->response(array(
                'shift_id' => $user_data['shift_id'],
                'message' => SHIFT_START,
                'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => $user_data['message'], 'status' => "0"), 200);
        }

    }

    /* fetch available cab */
    function cab_list()
    {
        $cab_data = $this->Driverapi_model->fetch_cab();
        if ($cab_data)
        {

            foreach ($cab_data as $cab)
            {
                $cab_detail[] = array(
                    "cab_id" => $cid = $cab->cab_id,
                    "cab_name" => $cab_name = $cab->cab_model,
                    "cab_type" => $cab->title);

            }
            $this->response(array('cab' => $cab_detail, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => NO_CAB_EXIST, 'status' => "0"), 200);
        }
    }

    /******************************START A BREAK***************************/
    function shift_break()
    {

        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array(
            "shift_id",
            "type",
            "driver_id")); //type= STARTED,ENDED
        if ($input_method['type'] == "STARTED")
        {

            $break_detail = $this->Driverapi_model->break_start($input_method);
        }
        else
        {

            $break_detail = $this->Driverapi_model->break_end($input_method);
        }

        if ($break_detail)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }

    }

    /*******************************SHIFT ENDED************************ */
    function shift_end()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("shift_id", "driver_id"));
        $shift_end = $this->Driverapi_model->shift_end($input_method);
        if ($shift_end['status'] == 1)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => $shift_end['message'], 'status' => "0"), 200);
        }
    }

    /* get driver infomation */
    function get_driver()
    {
        $input_method = $this->webservices_inputs();
        // $this->validate_param('driver_info', $input_method);
        $this->param_validate($input_method, array("driver_id"));
        $driver_data = $this->Driverapi_model->get_driver_info($input_method);
        if ($driver_data > 0)
        {
            foreach ($driver_data as $user_detail)
            {
                if ($user_detail->cab_image)
                {
                    $thumb_cab_image = get_thumb($user_detail->cab_image);
                }
                else
                {
                    $thumb_cab_image = "";
                }
                $profile_image = ($user_detail->profile_pic == '') ? base_url() .
                    DRIVER_DEFAULT_IMAGE : base_url() . DRIVER_IMAGE_PATH . $user_detail->
                    profile_pic;
                $cab_image_url = ($user_detail->cab_image == '') ? "" : DRIVER_SHIFT_IMAGE_PATH . $thumb_cab_image;
                $driver_id = $user_detail->driver_id;
                $first_name = $user_detail->first_name;
                $last_name = $user_detail->last_name;
                $contact = $user_detail->contact;
                $gender = $user_detail->gender;
                $zipcode = $user_detail->zipcode;
                $city = $user_detail->city;
                $email_id = $user_detail->email_id;
                $cab_type_id=$user_detail->type_id;
                $shift_id = ($user_detail->s_id == "") ? "" : $user_detail->s_id;
                $cab_image[] = $cab_image_url;
                $cab_type = ($user_detail->title == '') ? "" : $user_detail->title;
                $j_id = DRIVER_JID_NAME . '-' . $driver_id;
                $jid = makejid($driver_id, DRIVER_JID_NAME);
                $cab_plate = ($user_detail->cab_plate_no == "") ? "" : $user_detail->
                    cab_plate_no;
                $comment = $user_detail->comment;
                $cab_model = ($user_detail->cab_model == "") ? "" : $user_detail->cab_model;
                //$status = $user_detail->status;
                //                if ($status == DRIVER_ON_BREAK)
                //                {
                //                    $isbreak = "Y";
                //                }
                //                else
                //                {
                //                    $isbreak = "N";
                //                }
                $isbreak = $user_detail->isbreak;

                $rating = $this->Driverapi_model->get_driver_rating($driver_id);
            }
            $this->response(array(
                'driver_id' => $driver_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'contact' => $contact,
                "gender" => $gender,
                "zipcode" => $zipcode,
                "city" => $city,
                'email_id' => $email_id,
                'profile_image' => $profile_image,
                'cab_plate' => $cab_plate,
                'cab_image' => $cab_image,
                'cab_type' => $cab_type,
                "cab_type_id"=>$cab_type_id,
                'j_id' => $j_id,
                "jid" => $jid,
                'shift_id' => $shift_id,
                'isshift_start' => ($shift_id == '') ? "N" : "Y",
                'rating' => "$rating",
                'comment' => ($comment == '') ? "" : $comment,
                "cab_model" => $cab_model,

                "is_break" => ($isbreak == '') ? "N" : $isbreak,
                'status' => 1), 200);

        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }
    }
    /* update driver location*/
    function update_driver_location()
    {
        $input_method = $this->webservices_inputs();
        // $this->validate_param('update-location', $input_method);
        $this->param_validate($input_method, array(
            "driver_id",
            "lat",
            "lng"));
        $driver_data = $this->Driverapi_model->current_driver_location($input_method);
        if (sizeof($driver_data) > 0)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }
    }

    /* for driver location */

    function driver_location()
    {
        $input_method = $this->webservices_inputs();
        // $this->validate_param('driver-location', $input_method);
        // $re[]=array("driver_id");
        $this->param_validate($input_method, array("driver_id"));
        $driver_data = $this->Driverapi_model->get_driver_location($input_method);
        if ($driver_data)
        {
            foreach ($driver_data as $driver)
            {
                $data = array(

                    "latitude" => $latitude = $driver->latitude,
                    "longitude" => $longitude = $driver->longitude,
                    "last_update" => $last_update = $driver->last_location_update,
                    "cab_type" => $cab_type = $driver->type_id);

            }
            $this->response(array('driver_position' => $data, 'status' => 1), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => 0), 200);
        }

    }
    function get_driver_status()
    {
        $input_method = $this->webservices_inputs();
        //$this->validate_param('driver_info', $input_method);
        $this->param_validate($input_method, array("driver_id"));
        $driver_Shift_data = $this->Driverapi_model->get_driver_status($input_method);
        if ($driver_Shift_data)
        {
            $driver_Shift_data = $driver_Shift_data[0];

            $this->response(array(
                "shift_id" => $driver_Shift_data['s_id'],
                "shift_status" => $driver_Shift_data['status'],
                "isbreak" => $driver_Shift_data['isbreak'],
                "cab_plate_no" => $driver_Shift_data['cab_plate_no'],
                "cab_model" => $driver_Shift_data['cab_model'],
                'status' => 1), 200);
        }
        else
        {
            $this->response(array('message' => SHIFT_NOT_EXIT, 'status' => "1"), 200);
        }

    }
    //***************************FUNCTION GET NEXT DRIVER BOOKING**********************
    function get_next_booking()
    {
        $this->load->model('Booking_model');
        $input_method = $this->webservices_inputs();
        //$this->validate_param('driver_info', $input_method);
        $this->param_validate($input_method, array("driver_id"));
        $booking_detail = $this->Driverapi_model->get_next_booking($input_method);

        //print_r($other_item);

        if ($booking_detail)
        {
            $booking_detail = $booking_detail[0];
            $pickup_latitude = $booking_detail['pickup_latitude'];
            $pickup_longitude = $booking_detail['pickup_longitude'];
            $destination_latitude = $booking_detail['destination_latitude'];
            $destination_longitude = $booking_detail['destination_longitude'];
            $pickup_location = $booking_detail['pickup_location'];
            $destination_location = $booking_detail['destination_location'];
            $driver_latitude = $booking_detail['latitude'];
            $driver_longitude = $booking_detail['longitude'];
            $origins = $driver_latitude . ',' . $driver_longitude;
            $destination = $pickup_latitude . ',' . $pickup_longitude;
            $comment = ($booking_detail['comment'] == '') ? "" : $booking_detail['comment'];
            $time_detail = gettimedetail($origins, $destination);
            if ($time_detail)
            {
                $duration = $time_detail['duration'];
                $distance = $time_detail['distance'];
                $duration_sec = $time_detail['duration_sec'];
            }
            else
            {
                $duration = "";
                $distance = "";
            }
            $username = $booking_detail['name'];
            $email = $booking_detail['email'];
            $phone = $booking_detail['phone'];
            $customer_id = $booking_detail['customer_id'];
            $booking_id = $booking_detail['id'];
            $is_prebooking=$booking_detail['is_prebooking'];
            
            $other_item = $this->Booking_model->get_extra_order($booking_id);
            if ($other_item)
            {

                foreach ($other_item as $other_item_detail)
                {
                    $other_item_data[] = array(
                        "item" => ucwords($other_item_detail['item_name']),
                        "price" => $other_item_detail['item_price'],
                        "qty" => $other_item_detail['quantity']);

                }
            }
            else
            {
                $other_item_data = array();
            }
            if ($customer_id == "")
            {
                $booking_type = "restro";
            }
            else
            {
                $booking_type = "normal";
            }
            $this->response(array(
                'status' => "1",
                "booking_type" => $booking_type,
                "customer_id" => ($customer_id == "") ? "" : $customer_id,
                "driver_id" => $input_method['driver_id'],
                // "jid" => $jid,
                "customer_name" => $username,
                "email" => $email,
                "pickup_latitude" => $pickup_latitude,
                "pickup_longitude" => $pickup_longitude,
                "pickup_location" => $pickup_location,
                "driver_latitude" => $driver_latitude,
                "driver_longitude" => $driver_longitude,
                "customer_contact" => $phone,
                // "driver_location" => $driver_loaction,
                "duration" => $duration,
                "distance" => $distance,
                "eta" => getETA($duration_sec),
                "booking_id" => $booking_id,
                "destination_latitude" => ($destination_latitude == "") ? "" : $destination_latitude,
                "destination_longitude" => ($destination_longitude == "") ? "" : $destination_longitude,
                "comment" => ($comment == "") ? "" : $comment,
                "extra_order" => $other_item_data,
                "destination_location" => ($destination_location == "") ? "" : $destination_location,
                  "is_prebooking"=>$is_prebooking,
                ),
              
                200);
        }
        else
        {
            $this->response(array('message' => NO_ANY_BOOKING, 'status' => "0"), 200);
        }


    }
    function driver_booking_detail()
    {
        $this->load->model('Booking_model');
        $input_method = $this->webservices_inputs();
        //$this->validate_param('driver_info', $input_method);
        $this->param_validate($input_method, array("driver_id", "page_number"));
        $booking_detail_data = $this->Driverapi_model->driver_booking_detail($input_method);
        if ($booking_detail_data)
        {
            // echo "<pre>";
            //$booking_detail = $booking_detail[0];
            //print_r($booking_detail);

            foreach ($booking_detail_data as $booking_detail)
            {

                if ($booking_detail['status'] == "TRIP_ENDED")
                {
                    $display_status = "Completed";
                }
                elseif ($booking_detail['status'] == 'FINISH')
                {
                    $display_status = "Completed";
                }
                elseif ($booking_detail['status'] == 'CUSTOMER_ACCEPTED')
                {
                    $display_status = "Customer Waiting";
                }
                elseif ($booking_detail['status'] == 'TRIP_STARTED')
                {
                    $display_status = "Riding";
                }
                elseif ($booking_detail['status'] == 'ADVANCE_BOOKING')
                {
                    $display_status = "Advance Booking";
                }
                elseif ($booking_detail['status'] == 'CUSTOMER_NOT_RESPOND')
                {
                    $display_status = "Customer Not Respond";
                }
                else
                {
                    $display_status = "";
                }
                $driver_latitude = $booking_detail['latitude'];
                $driver_longitude = $booking_detail['longitude'];
                $pickup_latitude = $booking_detail['pickup_latitude'];
                $pickup_longitude = $booking_detail['pickup_longitude'];

                $origins = $driver_latitude . ',' . $driver_longitude;
                $destination = $pickup_latitude . ',' . $pickup_longitude;
                $comment = ($booking_detail['comment'] == '') ? "" : $booking_detail['comment'];
                $time_detail = gettimedetail($origins, $destination);
                if ($time_detail)
                {
                    $duration = $time_detail['duration'];
                    $distance = $time_detail['distance'];
                }
                else
                {
                    $duration = "";
                    $distance = "";
                }
                $booking_id = $booking_detail['bookingid'];
                $other_item = $this->Booking_model->get_extra_order($booking_id);
                if ($other_item)
                {

                    foreach ($other_item as $other_item_detail)
                    {
                        $other_item_data[] = array(
                            "item" => ucwords($other_item_detail['item_name']),
                            "price" => $other_item_detail['item_price'],
                            "qty" => $other_item_detail['quantity']);

                    }
                }
                else
                {
                    $other_item_data = array();
                }

                $driver_booking[] = array(
                    "booking_id" => $booking_detail['bookingid'],
                    "driver_id" => $input_method['driver_id'],
                    "CBN" => $booking_detail['booking_number'],
                    "customername" => $booking_detail['name'],
                    "customer_phone" => $booking_detail['phone'],
                    "customer_id" => $booking_detail['userid'],
                    "pickup_latitude" => $booking_detail['pickup_latitude'],
                    "pickup_longitude" => $booking_detail['pickup_longitude'],
                    "pickup_location" => $booking_detail['pickup_location'],
                    "destination_latitude" => $booking_detail['destination_latitude'],
                    "destination_longitude" => $booking_detail['destination_longitude'],
                    "destination_location" => $booking_detail['destination_location'],
                    "booking_status" => $booking_detail['status'],
                    "is_prebooking" => $booking_detail['is_prebooking'],
                    "booking_time" => $booking_detail['booking_time'],
                    "extra_order" => $other_item_data,
                    "duration" => $duration,
                    "distance" => $distance,
                    "comment" => $comment,
                    "display_status" => $display_status);
            }
            $this->response(array('booking_detail' => $driver_booking, 'status' => "1"), 200);
        }
        else
        {
            $driver_booking = array();
            $this->response(array('booking_detail' => $driver_booking, 'status' => "1"), 200);
        }
    }
    function driver_prebooking_detail()
    {
        $this->load->model('Booking_model');
        $input_method = $this->webservices_inputs();
        //$this->validate_param('driver_info', $input_method);
        $this->param_validate($input_method, array("driver_id", "page_number"));
        $booking_detail_data = $this->Driverapi_model->driver_prebooking_detail($input_method);
        if ($booking_detail_data)
        {
            // echo "<pre>";
            //$booking_detail = $booking_detail[0];
            //print_r($booking_detail);

            foreach ($booking_detail_data as $booking_detail)
            {

                if ($booking_detail['status'] == "DRIVER_ASSIGNED")
                {
                    $display_status = "Booking Alloted";
                }
                $booking_id = $booking_detail['bookingid'];
                $other_item = $this->Booking_model->get_extra_order($booking_id);
                if ($other_item)
                {

                    foreach ($other_item as $other_item_detail)
                    {
                        $other_item_data[] = array(
                            "item" => ucwords($other_item_detail['item_name']),
                            "price" => $other_item_detail['item_price'],
                            "qty" => $other_item_detail['quantity']);

                    }
                }
                else
                {
                    $other_item_data = array();
                }
                $driver_latitude = $booking_detail['latitude'];
                $driver_longitude = $booking_detail['longitude'];
                $pickup_latitude = $booking_detail['pickup_latitude'];
                $pickup_longitude = $booking_detail['pickup_longitude'];

                $origins = $driver_latitude . ',' . $driver_longitude;
                $destination = $pickup_latitude . ',' . $pickup_longitude;
                $comment = ($booking_detail['comment'] == '') ? "" : $booking_detail['comment'];
                $time_detail = gettimedetail($origins, $destination);
                if ($time_detail)
                {
                    $duration = $time_detail['duration'];
                    $distance = $time_detail['distance'];
                }
                else
                {
                    $duration = "";
                    $distance = "";
                }
                $comment = ($booking_detail['comment'] == '') ? "" : $booking_detail['comment'];
                if(!$booking_detail['userid'])
                {
                    $booking_type="restro";
                }else
                {
                    $booking_type="normal";
                }
                $driver_booking[] = array(
                    "booking_id" => $booking_detail['bookingid'],
                    "CBN" => $booking_detail['booking_number'],
                    "customername" => $booking_detail['name'],
                    "customer_phone" => $booking_detail['phone'],
                    "customer_id" => ($booking_detail['userid']=="") ? "" : $booking_detail['userid'],
                    "pickup_latitude" => $booking_detail['pickup_latitude'],
                    "pickup_longitude" => $booking_detail['pickup_longitude'],
                    "pickup_location" => $booking_detail['pickup_location'],
                    "destination_latitude" => ($booking_detail['destination_latitude']=="") ? "" : $booking_detail['destination_latitude'],
                    "destination_longitude" => ($booking_detail['destination_longitude']=="") ? "" : $booking_detail['destination_longitude'] ,
                    "destination_location" => ($booking_detail['destination_location']=="") ? "" : $booking_detail['destination_location'],
                    "booking_status" => $booking_detail['status'],
                    "is_prebooking" => $booking_detail['is_prebooking'],
                    "booking_time" => $booking_detail['booking_time'],
                    "display_status" => $display_status,
                    "pickup_time" => $booking_detail['booking_time'],
                    "extra_order" => $other_item_data,
                    "duration" => $duration,
                    "distance" => $distance,
                    "comment" => $comment,
                    "booking_type"=>$booking_type,
                    "display_status" => $display_status);
            }
            $this->response(array('booking_detail' => $driver_booking, 'status' => "1"), 200);
        }
        else
        {
            $driver_booking = array();
            $this->response(array('booking_detail' => $driver_booking, 'status' => "1"), 200);
        }
    }
    function customer_aware()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("booking_id", "driver_id"));
        $input_method['type'] = "FIVE_MIN_AWAY";
        $customer_aware = $this->Driverapi_model->customer_aware($input_method);
        //echo "<pre>";
        // print_r($customer_aware);
        // exit;
        if ($customer_aware)
        {
            //send SMS and push notification if require
            $customer_aware = $customer_aware[0];
            $customer_phone = $customer_aware['country_code'] . $customer_aware['phone'];
            $message = FIVEMINAWAY;
            send_sms($customer_phone, $message);


            $driver_jid = makejid($customer_aware['driver_id'], DRIVER_JID_NAME);
            $customer_jid = makejid($customer_aware['user_id'], USER_JID_NAME);
            $pickup_latitude = $customer_aware['pickup_latitude'];
            $pickup_longitude = $customer_aware['pickup_longitude'];
            $driver_latitude = $customer_aware['latitude'];
            $driver_longitude = $customer_aware['longitude'];
            $origins = $driver_latitude . ',' . $driver_longitude;
            $destination = $pickup_latitude . ',' . $pickup_longitude;
            $time_detail = gettimedetail($origins, $destination);
            if ($time_detail)
            {
                $duration = $time_detail['duration'];
                $distance = $time_detail['distance'];
            }
            else
            {
                $duration = "";
                $distance = "";
            }
            $this->response(array(
                'message' => CUSTOMER_AWARE . $customer_aware['booking_number'] . " booking",
                'status' => "1",
                'driver_id' => $customer_aware['driver_id'],
                'name' => $customer_aware['first_name'] . ' ' . $customer_aware['last_name'],
                'contactno' => $customer_aware['contact'],
                'email' => $customer_aware['email'],
                'profile_image' => base_url() . DRIVER_IMAGE_PATH . $customer_aware['profile_pic'],
                'distance' => $distance,
                'duration' => $duration,
                'latitude' => $customer_aware['latitude'],
                'longitude' => $customer_aware['longitude'],
                'jid' => $driver_jid,
                'cab_type' => $customer_aware['title'],
                'customer_jid' => $customer_jid,
                'cab_number' => $customer_aware['cab_plate_no'],
                'booking_id' => $customer_aware['id'],
                'CBN' => $customer_aware['booking_number'],
                ), 200);
        }
        else
        {
            $this->response(array('message' => BOOKING_NOTFOUND, 'status' => "0"), 200);
        }
    }
    function driver_arrive()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("booking_id", "driver_id"));
        $input_method['type'] = "I_AM_HERE";
        $customer_aware = $this->Driverapi_model->customer_aware($input_method);
        //print_r($customer_aware);
        if ($customer_aware)
        {
            $customer_aware = $customer_aware[0];

            //send SMS and push notification if require

            $customer_phone = $customer_aware['country_code'] . $customer_aware['phone'];
            $message = ARRIVE;
            send_sms($customer_phone, $message);
            $customer_id = $customer_aware['user_id'];
            $customer_jid = makejid($customer_id, USER_JID_NAME);
            $this->response(array(
                'message' => CABARRIVE,
                "customer_jid" => $customer_jid,
                'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => BOOKING_NOTFOUND, 'status' => "0"), 200);
        }
    }
    function driver_unavailable()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("driver_id", "type"));
        $driver_status = $this->Driverapi_model->driver_unavailable($input_method);
        if ($driver_status)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }

    }
    function driver_ride_detail()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("driver_id", "page_number"));
        //print_r($input_method);
        $ride_detail_data = $this->Driverapi_model->driver_ride_details($input_method);
        if ($ride_detail_data)
        {
            //$ride_detail=$ride_detail['0'];
           //  echo "<pre>";
            // print_r($ride_detail_data);
            // exit;
            foreach ($ride_detail_data as $ride_detail)
            {
                $booking_id = $ride_detail['booking_id'];
                $CBN = $ride_detail['booking_number'];
                $user_id = $ride_detail['user_id'];
                $username = $ride_detail['name'];
                //$email=$ride_detail['email'];
                $company_name = ($ride_detail['company_name'] == "") ? "" : $ride_detail['company_name'];
                $cab_type = $ride_detail['title'];
                $pickup_latitude = $ride_detail['pickup_latitude'];
                $pickup_longitude = $ride_detail['pickup_longitude'];
                $pickup_location = $ride_detail['pickup_location'];
                $destination_latitude = $ride_detail['destination_latitude'];
                $destination_longitude = $ride_detail['destination_longitude'];
                $destination_location = $ride_detail['destination_location'];
                $booking_time = $ride_detail['booking_time'];
                $isprebooking = $ride_detail['is_prebooking'];
                $status = $ride_detail['status'];
                $title = ($ride_detail['title'] == '') ? "" : $ride_detail['title'];
                if(@$ride_detail['booking_type']=="restro")
                {
                    $restro_name=$ride_detail['name'];
                    $username=$ride_detail['client_name'];
                     $booking_type="restro";
                     $user_id="";
                     $destination_latitude=$destination_location=$destination_longitude="";
                }else
                {
                    $booking_type="normal";
                    $restro_name="";
                }
                $ride_info[] = array(
                    "booking_id" => $booking_id,
                    "CBN" => $CBN,
                    "user_id" => $user_id,
                    "user_name" => $username,
                    "company_name" => $company_name,
                    "pickup_latitude" => $pickup_latitude,
                    "pickup_longitude" => $pickup_longitude,
                    "pickup_loaction" => $pickup_location,
                    "destination_latitude" => $destination_latitude,
                    "destination_longitude" => $destination_longitude,
                    "destination_loaction" => $destination_location,
                    "cab_type" => $title,
                    "booking_time" => $booking_time,
                    "is_pre_booking" => $isprebooking,
                    "status" => $status,
                    "booking_type"=>$booking_type,
                    "restro_name"=>$restro_name 
                    );
                // exit;
            }
            $this->response(array('ride_deatil' => $ride_info, 'status' => "1"), 200);


        }
        else
        {
            $ride_detail = array();
            $this->response(array('ride_deatil' => $ride_detail, 'status' => "1"), 200);
        }
    }
    function device_register()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("driver_id", "device_token"));
        //print_r($input_method);
        $device_register = $this->Driverapi_model->device_register($input_method);
        if ($device_register)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }
    }
    function update_login_status()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("user_jid", "login_type"));
        $login_status = $this->Driverapi_model->update_login_status($input_method);
        if ($login_status)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }
        //print_r($input_method);

    }
    function logout()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("driver_id"));
        $logout = $this->Driverapi_model->driver_logout($input_method);
        if ($logout)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }

    }
    function driver_cancel()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("driver_id", "booking_id"));
        $driver_cancel = $this->Driverapi_model->driver_cancel($input_method);
        // print_r($driver_cancel);
        if ($driver_cancel['status'] == 1)
        {
            //get custommer jid and contactdetail
            $customer_detail = $this->Driverapi_model->get_customer_detail($input_method);
            $phoneno = $customer_detail[0]['country_code'] . $customer_detail[0]['phone'];
            send_sms($phoneno, DRIVER_CANCEL_SMS);
            $customer_jid = makejid($customer_detail[0]['id'], USER_JID_NAME);
            $this->response(array(
                'message' => $driver_cancel['message'],
                "message_customer" => $driver_cancel['message_customer'],
                'status' => "1",
                "customer_jid" => $customer_jid), 200);
        }
        else
        {
            $this->response(array('message' => $driver_cancel['message'], 'status' => "0"),
                200);
        }
    }
    function check_appversion()
    {
        $input_method = $this->webservices_inputs();
        //$this->validate_param('fare-estimate', $input_method);
        $this->param_validate($input_method, array(
            "os_type",
            "app_mode",
            "app_version",
            ));
        $appversion = $this->Driverapi_model->check_appversion($input_method);

        if ($input_method['app_version'] < $appversion['version'])
        {
            $this->response(array(
                'status' => "0",
                "link" => $appversion['link'],
                "is_require" => $appversion['is_required'],
                "app_version" => $appversion['version']), 200);
        }
        else
        {
            $this->response(array(
                'status' => "1",
                "message" => SUCCESS,
                ), 200);
        }

    }
    function get_mobile_numbers()
    {
        $mobile_detail=$this->Driverapi_model->get_mobile_detail();
        if($mobile_detail)
        {
           // echo "<pre>";
           // print_r($mobile_detail);
            foreach($mobile_detail as $mobile)
            {
                $simdetail[]=array("serial_number"=>$mobile['serial_number'],
                                  "country_code"=>$mobile['country_code'],  
                                  "mobile_number"=>$mobile['contact_no'],
                );
            }
             $this->response(array(
                'status' => "1",
                "mobile_detail" => $simdetail,
                ), 200);
            
            
        }else
        {
             $this->response(array(
                'status' => "0",
                "message" => "Mobile number not exist",
                ), 200);
        }
    }
    
    function shift_image_upload()
    {
        foreach (glob("./images/shift/*.jpg") as $file)
        {
            //echo $file;
            $filedata = explode('/', $file);
            echo "<br/>filename=>" . $file = $filedata[3];
            echo "<br/>";

            //$pos = strpos($file, '08-2014', 1);
            
            {
                 $s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
                
                //echo "<br>" . $hertzawspath = 'reports/' . $file;
                if (($info = $s3->getObjectInfo(AWS_BUCKET_NAME, $file)) == false)
                {
                    echo "<br/>in function";
                    echo "<br/>path=>".'./images/shift/'. $file;
                    // $aws_upload = $s3->putObjectFile($source, AWS_BUCKET, $data['upload_data']['file_name'],
                    //    S3::ACL_PUBLIC_READ);
                    if ($s3->putObjectFile( './images/shift/'. $file, AWS_BUCKET, $file,S3::ACL_PUBLIC_READ))
                    {
                        echo "<br/>";
                        echo "yes";
                    }else
                    {
                        echo "error";
                        die;
                    }
                }
                else
                {
                    echo "<br/>Already<br/>";
                }

                // exit;
            }
            
        }
    }

}
