 <?php if($coupon[0]->coupon_type=="ALL"){
 	$all_type="checked";
	$display="display:none'";
 }else
 	{
 		$specifc_type="checked";
		$display="display:'";
 	}
	
 foreach ($coupon as  $value) {
     $coupon_user[]=($value->user_id);
   //print_r($value);
 }
  ?>
 
  <link href="<?php echo base_url() ?>js/select2.css" rel="stylesheet" type="text/css" />

<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Coupon</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
      <li><a href="#">Coupon</a></li>
      <li class="active">Add Coupon</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
          <div class="pull-left"><h3 class="box-title"></h3></div>
          <div class="pull-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-block btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
          </div>
          <div style="clear:both;"></div>
      </div>
     
      <div class="box-body"> <!--Box body start here-->
        <div class="row">
          <div class="col-md-8 col-sm-8 col-xs-8 col-sm-offset-0">
            <form id="save_coupon" action="" method="post" enctype="multipart/form-data" autocomplete="off">
              
             <input type="hidden" value="<?=(isset($coupon[0]->id))?$coupon[0]->id:"";?>"  name="id" id="id"  />
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="first name">Title</label>
                      <input type="text" class="form-control validate[required]" name="title" id="title" value="<?=(isset($coupon[0]->title))?$coupon[0]->title:"";?>" placeholder="Title">
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="last name">Discount</label>
                      <input type="text" class="form-control validate[required]" name="offer" value="<?=(isset($coupon[0]->offer))?$coupon[0]->offer:"";?>" id="offer" placeholder="Discount" onkeypress="return isNumber(event)">
                  </div>  
                </div>
              </div>
                <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">Description</label>
                    <textarea id="msg" maxlength="100" name="description" class="form-control validate[required]" rows="3" placeholder="Enter ..."><?php if(isset($coupon[0]->description)) {echo $coupon[0]->description; } else { echo "";}?></textarea>
                    <span id="remainingC" style="padding-left:50px;"></span>
                  </div>  
                </div>
              </div>
                
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email_id">code</label>
                      <input type="code" onblur="chkcode()" class="form-control validate[required]" id="code" value="<?=(isset($coupon[0]->code))?$coupon[0]->code:"";?>"" name="code" placeholder="Enter code">
                  </div>  
                </div>
              </div>
            <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="start date">Start Date</label>
                    <input type="type" class="form-control validate[required]" id="start_date" name="start_date" value="<?=(isset($coupon[0]->start_date))?$coupon[0]->start_date:"";?>" placeholder="Enter Date">
                  </div>  
                </div>
              </div>
              
              
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="license">End Date</label>
                    <input type="type" class="form-control validate[required]" id="end_date" name="end_date" value="<?=(isset($coupon[0]->expire_date))?$coupon[0]->expire_date:"";?>" placeholder="Enter Date">
                  </div>  
                </div>
              </div>
              
             <div class="row">
                <div class="col-sm-6">
               <div class="form-group">
               	   <label for="first name">Coupon Type</label>
                      <div class="radio">
               
                        <label>
                          <input type="radio" class="radiobutton" <?php echo $all_type ?> name="coupon_type" id="optionsRadios1" value="All" >
                           All User
                        </label>
                      
                    
                        <label>
                          <input type="radio" class="radiobutton" <?php echo $specifc_type ?> name="coupon_type" id="optionsRadios2" value="SPECIFIC">
                          Specific Users
                        </label>
                      
                     </div>
                    </div>
                </div></div>
                
                 <div class="row" id="userList" style="<?php echo $display ?>" >
                <div class="col-sm-6">
                <div class="form-group">
                <label>Select Multiple</label>
                <select class="form-control select2" id="UserData" name="userlist[]" multiple="multiple" data-placeholder="Select Users" style="width: 100%;">
                  <?php foreach($users as $user){?>
                  	<option value="<?php echo $user['id'] ?>" <?php if(in_array($user['id'], $coupon_user)){ echo "selected" ;} ?>><?php echo $user['name'] .'('. $user['email'] .')' ?></option>
                 <?php } ?>
                </select>
                    </div>
                   </div>
                   </div>
              
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" id="submit" name="submit">Submit</button>
              </div> 
              <div id="loader" style="display:none; margin-left:2%;">
                <img src="<?php echo base_url(); ?>images/loader19.gif" />
              </div>
              
            </form>
          </div>
        </div>
      </div> <!--Box body end here-->
    </div>
  <!-- Default box end -->
  </section>
</div>
     <!-- <script src="<?php echo base_url() ?>js/select2.full.min.js"></script> -->
          <script src="http://code.jquery.com/jquery-2.2.3.min.js"></script>

<script>
  $(function() {
                    $( "#start_date" ).datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true
                       
                        });
                   
                      });
   $(function() {
                    $( "#end_date" ).datepicker({
                      format: "yyyy-mm-dd",
                      autoclose: true
                       
                        });
                   
                      });

</script>
<script type="text/javascript">
  
//var $ = jQuery.noConflict();
$(document).ready(function(){
	//alert();
//$('#UserData').multiselect();
$("#UserData").select2();
    function submit_form(formData,status)
    {
    	 $('#submit').prop('disabled', true);
        if(status==true)
        {
            var querystr = $("#save_coupon").serialize();
           var id=$("#id").val();
          // alert(id);
           if(id)
           {
           	  var msg="Coupon Successfully Updated."
           }
           else
           {
           	var msg ="Coupon is added successfully"
           }
            
            $.ajax({
                url		:	"<?php echo site_url('Generalinfo/Submit_coupon'); ?>",
                type	:	"POST",
                data	:	querystr,
               
                //async: false,
               // cache: false,
                //contentType: false,
                //processData: false,
                
                beforeSend  :   function(){
                                    $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                	//alert(data);
                  if(data == 1)
                  {
                    $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl(msg, {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);
                    $('#save_coupon')[0].reset();
                    setTimeout(continueExecution, 1000);
                   
                  }
                  else
                  {
                    $("#loader").hide();
                   
                  }
                 }
            });
            return false;
        }
        
    }
            
      function continueExecution(){
                    window.location="<?php echo site_url('Generalinfo/campagin'); ?>";
                    } 
    
  $("#save_coupon").validationEngine('attach',{
          unbindEngine	:	false,
          validationEventTriggers	:	"keyup blur",
          promptPosition : "topRight",
          onValidationComplete	:	function(formData,status) { submit_form(formData,status) }
      });
});
</script>
<script>
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(".radiobutton").click(function(){
	
	if(this.value=='All')
	{
		
		$("#userList").css("display","none");
	}else
	{
		$("#userList").css("display","");
	}
	
})


	function chkcode()
	{
		//var code_num=document.getElementById("code").value;		
		//alert(code_num);
		var querystr = $("#save_coupon").serialize();
		//var id=$("#code").val();
		//alert(id);
		
		 $.ajax({
                url		:	"<?php echo site_url('Generalinfo/check_coupon_code'); ?>",
                type	:	"POST",
                data	:	querystr,
               
                //async: false,
               // cache: false,
                //contentType: false,
                //processData: false,
                
                beforeSend  :   function(){
                                   // $("#loader").show();
                                    //alert('show loader');
                                },   
                success	:	function(data){
                	//alert(data);                	
                  if(data ==1 )
                  {
                  	//alert('helo');
                  	document.getElementById("code").style.borderColor = "red";
                  	$("#code").val('');
                  	$("#code").attr("placeholder", "Code Already Use Please Insert New Code").placeholder();                  	
                  	
                  	//$("#code").
                   /* $("#loader").hide();
                    setTimeout(function() {
                        $.bootstrapGrowl(msg, {
                            type: 'success',
                            align: 'right',
                            width: 'auto',
                            allow_dismiss: true
                        });
                    }, 1000);  */
                    //$('#save_coupon')[0].reset();
                    //setTimeout(continueExecution, 10000);
                   
                  }
                  else
                  {
                  	document.getElementById("code").style.borderColor = "#d2d6de";
                    //$("#loader").hide();
                   
                  }
                 }
            });
	}
	
	$('#msg').keypress(function(){

    if(this.value.length > 100){
        return false;
    }
    $("#remainingC").html("Remaining characters : " +(100 - this.value.length));
});

</script>



