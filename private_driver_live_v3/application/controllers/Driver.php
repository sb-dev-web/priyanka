<?php

	defined('BASEPATH') or exit('No direct script access allowed');

	class Driver extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this -> load -> helper(array(
				'form',
				'url',
				'constants_helper'
			));
			$this -> load -> model('Driver_model');
			$this -> load -> library(array(
				'session',
				'email',
				'paginationlib',
				'Pagination'
			));
			$this -> load -> library('xmpp', false);
			$this -> load -> library('Openfire', false);
			$user = $this -> session -> userdata('logged_in');
			if (!$user)
			{
				//redirect('Welcome');
			}

		}

		public function index()
		{
			$this -> load -> library('Ajax_pagination');
			$this -> Driver_model -> driver_count();
			$pagingconfig = $this -> paginationlib -> initPagination("/Driver/ajaxPaginationData/", $this -> Driver_model -> driver_count(), 10, 0, array());

			$this -> ajax_pagination -> initialize($pagingconfig);
			//$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			//$data['link'] = $this->pagination->create_links();
			$data['link'] = $this -> ajax_pagination -> create_links();
			$driver = $this -> Driver_model -> fetch_total_driver($pagingconfig['per_page'], 0);
			$driver_rating = $this -> Driver_model -> DriverRating();
			foreach ($driver_rating as $rating)
			{
				$DriverRating[$rating['driver_id']] = ($rating['rating']) ? $rating['rating'] : "--";
			}

			$data['driver'] = $driver;
			$data['DriverRating'] = $DriverRating;
			$data['active_page'] = 'driver';
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('driver');
			$this -> load -> view('site-nav/footer');
		}

		function ajaxPaginationData()
		{
			$this -> load -> library('Ajax_pagination');
			$postdata = $this -> input -> post('data');
			$post = json_decode($postdata, true);
			$page = $post['page'];
			if (!$page)
			{
				$offset = 0;
			}
			else
			{
				$offset = $page;
			}

			$pagingconfig = $this -> paginationlib -> initPagination("/driver/ajaxPaginationData/", $this -> Driver_model -> driver_count(), 10, $page);
			$this -> ajax_pagination -> initialize($pagingconfig);
			$data['link'] = $this -> ajax_pagination -> create_links();
			//get the posts data
			$data['driver'] = $this -> Driver_model -> fetch_total_driver($pagingconfig['per_page'], $page);

			$driver_rating = $this -> Driver_model -> DriverRating();
			foreach ($driver_rating as $rating)
			{
				$DriverRating[$rating['driver_id']] = ($rating['rating']) ? $rating['rating'] : "--";
			}
			$data['DriverRating'] = $DriverRating;
			//load the view
			$this -> load -> view('driver_ajax', $data, false);
		}

		public function perPage()
		{
			$this -> load -> library('Ajax_pagination');
			return 2;
		}

		public function change_status()
		{
			$id = $_POST['id'];
			$status = $_POST['status'];

			$driver_status = $this -> Driver_model -> change_driver_status($id, $status);
			echo $driver_status;
		}

		public function add_driver()
		{
			//$driver=$this->Driver_model->add_driver();
			//print_r($driver);
			//$data['driver']=$driver;
			@$driver[0] -> first_name = $driver[0] -> last_name = $driver[0] -> gender = $driver[0] -> zipcode = $driver[0] -> city = $driver[0] -> email_id = $driver[0] -> profile_pic = $driver[0] -> license_image = $driver[0] -> address = "";
			$driver = "";
			if ($this -> uri -> segment(3) == false)
			{

				$id = 0;

			}
			else
			{
				$id = $this -> uri -> segment(3);
				$driver = $this -> Driver_model -> get_driver($id);
			}
			$data['driver'] = $driver;
			$data['active_page'] = 'driver';
			$this -> load -> view('site-nav/header', $data);
			$this -> load -> view('add_driver');
			$this -> load -> view('site-nav/footer');
		}

		public function submit_driver()
		{
			error_reporting(1);
			$id = $_POST['id'];
			$res = $this -> Driver_model -> save_drive($id);
			if ($res)
			{
				if ($res != 'updated')
				{
					$user_name = DRIVER_JID_NAME . '-' . $res;
					$password = $_POST['password'];
					$name = $_POST['first_name'] . " " . $_POST['last_name'];
					$email = $_POST['email_id'];

					$param = array(
						"username" => $user_name,
						"name" => $name,
						"password" => $password,
						"email" => $email
					);

					$openfir_result = $this -> openfire -> openfire_action("POST", $param, "users/");
	//print_r($openfir_result);		
					$config['protocol'] = 'smtp';
					$config['charset'] = 'utf-8';
					$config['wordwrap'] = true;
					$config['mailtype'] = 'html';

					$config['smtp_host'] = EMAIL_HOST;
					$config['smtp_user'] = EMAIL_USERNAME;
					$config['smtp_pass'] = EMAIL_PASSWORD;
					$config['smtp_port'] = EMAIL_PORT;
					$config['email_smtp_crypto'] = 'tls';
					$config['email_newline'] = '\r\n';

					$this -> email -> clear();
					$this -> email -> initialize($config);
					$this -> email -> set_newline("\r\n");
					$this -> email -> from('support@privatedriver.com', 'Private driver ');
					$this -> email -> to($_POST['email_id']);
					$this -> email -> subject('Your information is');
					$link = "<p>Dear $name</p>";
					$link .= "<p>Welcome to the Private Driver<br/><br/>We are pleased to inform you that your <br/><br/>";
					$link .= "<p>​Driver Account has been created for ​Private Driver App. The details of your account are given below</p><br/><br/>";
					$link .= "<br>Username: &nbsp;&nbsp;" . $_POST['email_id'];
					$link .= "<br>Password: &nbsp;&nbsp;" . $_POST['password'];
					$link .= "<br><br>For any queries or assistance, please contact us at support@privatedriver.com";
					$link .= "<br><br>Best Regards,";
					$link .= "<br><br>​Private Driver";
					$this -> email -> message($link);
					$this -> email -> send();
					echo '1';

				}
				else
				{

					$user_name = DRIVER_JID_NAME . '-' . $id;
					$name = $_POST['first_name'] . " " . $_POST['last_name'];
					$update_password = $_POST['password'];
					if (empty($update_password))
					{
						$param = array(
							$user_name,
							$name
						);
						//print_r($param);
						//$this -> xmpp -> api("update", $param);
						$param = array(
							//"username" => $user_name,
							"name" => $name,
							"password" => $password,
							"email" => $email
						);
						//$this->xmpp->initialize(false);

						//$d_data= $this -> xmpp -> api("add", $param);
						$openfir_result = $this -> openfire -> openfire_action("PUT", $param, "users/$user_name");
						$openfir_result = json_decode($openfir_result, true);
						if ($openfir_result['status'] == 1)
						{
							echo 1;
						}
						else
						{
							echo 0;
						}
					}
					else
					{
						$param = array(
							//"username" => $user_name,
							"name" => $name,
							"password" => $password,
							"email" => $email
						);
						//$this->xmpp->initialize(false);

						//$d_data= $this -> xmpp -> api("add", $param);
						$openfir_result = $this -> openfire -> openfire_action("PUT", $param, "users/$user_name");
						$openfir_result = json_decode($openfir_result, true);
						if ($openfir_result['status'] == 1)
						{
							echo 1;
						}
						else
						{
							echo 0;
						}
					}

				}

			}

			else
			{
				echo 0;
			}
			//echo $res;
		}

		function change_delete_status()
		{
			$id = $_POST['id'];
			echo $this -> Driver_model -> delete_driver($id);
		}

		function check_email()
		{
			$email = $_POST['email_id'];
			echo $this -> Driver_model -> check_driver_email($email);
		}

		function change_login_status()
		{
			$id = $_POST['id'];
			$status = $_POST['status'];

			$driver_status = $this -> Driver_model -> change_login_status($id, $status);
			echo $driver_status;
		}

		function change_shift_status()
		{
			$id = $_POST['id'];
			$status = $_POST['status'];
			$driver_status = $this -> Driver_model -> change_shif_status($id, $status);
			echo $driver_status;
		}
		
		function driveropenfire()
		{
			error_reporting(1);
			$drivers= $this->Driver_model->GetDriverLive();
			//print_r($drivers);
			echo "<pre>";
			foreach ($drivers as $driver) {
				
				$user_name= "driver-".$driver['driver_id'];
				$param = array(
						"username" => $user_name,
						"name" => $driver['first_name'],
						"password" => "123456",
						"email" => $driver['email_id']
					);
					print_r($param);

					$openfir_result = $this -> openfire -> openfire_action("POST", $param, "users/");
					//echo "================".$openfir_result."==============";
					$openfire_response = json_decode($openfir_result, true);
					if ($openfire_response['status'] == 1)
					{
						echo "<br>Success";
					}else
						{
							exit;
						}
			}
		}

	}
