<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title></title>
	<meta name="generator" content="LibreOffice 4.2.8.2 (Linux)">
	<meta name="created" content="20160216;110821960038857">
	<meta name="changed" content="20160216;111430999489928">
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
	-->
	</style>
</head>
<body lang="en-IN" dir="ltr">


<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt"><b>1.
Accepting the terms</b></font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">By
completing the registration you accept these conditions.</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt"><b>2.
Prices</b></font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">Our
prices are a result of the start price, number of minutes and
mileage. The prices are adjusted regularly, and</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">by
using the &quot;Price Estimate&quot; will be able to see what the
trip will cost. This price is based on</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">normal
traffic and builds on the experience of itinerary, time of day, day
of week and more. Price estimate is</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">not
binding when specific traffic conditions can make travel time and
routes longer than normal.</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt"><b>3.
Minimum Price</b></font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">The
minimum price per trip is £. 100, - regardless of travel time and
length.</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt"><b>4.
Cancellation</b></font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">If
you cancel your trip before it is 3 minutes to arrival incur no
charge.</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">Cancels
you when there is less than three minutes left you will be charged £.
100, -.</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt"><b>5.
No show</b></font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">If
you do not arrive car within five minutes after you have been
notified that a vehicle is</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">at
the specified address, you will be charged a £. 100, -.</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt"><b>6.
Receipt</b></font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">You
will receive specified receipt by e-mail after each trip, including
those tours where you are charged i.h.t.</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">Section.
3 and 4</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt"><b>7.
Refunds</b></font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">If
you are charged incorrectly for various reasons, please contact us by
email:</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt"><a href="mailto:post@privatedriver.no">post@privatedriver.no
</a>and we will immediately and without delay refund the excess amount.</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">This
may be instances where you'll be charged a higher amount than the
closing price indicates, double payment</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">or
if you erroneously deducted amounts i.h.t. Section. 3 and 4 of these
conditions. Other complaints on our</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">services
that you think you should be compensated for, must be justified in
writing and no refunds given</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">automatically
until we have considered your complaint.</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt"><b>8.
Your information and data</b></font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">Private
Driver A / S shall safeguard your personal information secure manner.
They can not be shared</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">third
party without your consent. The data can be used to improve our
services. by</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">use
our application you agree to receive SMS and messages via the
application with message</font></p>

<p style="margin-bottom: 0cm; line-height: 100%"><font size="2" style="font-size: 11pt">which
may include third party. This may be promotions, discounts, prizes,
news and the like.</font></p>

</body>
</html>