
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2006, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Pagination Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Pagination
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/pagination.html
 *
 * @download from  http://www.joelsays.com/downloads/jquery-pagination.zip
 */
class CI_Ajax_pagination
{

    var $base_url = ''; // The page we are linking to
    var $total_rows = ''; // Total number of items (database results)
    var $per_page = 10; // Max number of items you want shown per page
    var $num_links = 2; // Number of "digit" links to show before/after the currently viewed page
    var $cur_page = 0; // The current page being viewed
    var $first_link = '&lsaquo; First';
    var $next_link = '&gt;';
    var $prev_link = '&lt;';
    var $last_link = 'Last &rsaquo;';
    var $uri_segment = 3;
    var $full_tag_open = '<div class="pagination">';
    var $full_tag_close = '</div>';
    var $first_tag_open = '';
    var $first_tag_close = '&nbsp;';
    var $last_tag_open = '&nbsp;';
    var $last_tag_close = '';
    var $cur_tag_open = '<li class="active">';
    var $cur_tag_close = '</b>';
    var $next_tag_open = '&nbsp;';
    var $next_tag_close = '&nbsp;';
    var $prev_tag_open = '&nbsp;';
    var $prev_tag_close = '';
    var $num_tag_open = '&nbsp;';
    var $num_tag_close = '';

    // Added By Tohin
    var $js_rebind = '';
    var $div = '';
    var $postVar = '';
    var $additional_param = '';

    // Added by Sean
    var $anchor_class = '';
    var $show_count = false;

    /**
     * Constructor
     *
     * @access	public
     * @param	array	initialization parameters
     */
    function CI_Pagination($params = array())
    {
        if (count($params) > 0)
        {
            $this->initialize($params);
        }

        log_message('debug', "Pagination Class Initialized");
    }

    // --------------------------------------------------------------------

    /**
     * Initialize Preferences
     *
     * @access	public
     * @param	array	initialization parameters
     * @return	void
     */
    function initialize($params = array())
    {
        if (count($params) > 0)
        {
            foreach ($params as $key => $val)
            {
                if (isset($this->$key))
                {
                    $this->$key = $val;
                }
            }
        }

        // Apply class tag using anchor_class variable, if set.
        if ($this->anchor_class != '')
        {
            $this->anchor_class = 'class="' . $this->anchor_class . '" ';
        }
    }

    // --------------------------------------------------------------------

    /**
     * Generate the pagination links
     *
     * @access	public
     * @return	string
     */
    function create_links()
    {
        // If our item count or per-page total is zero there is no need to continue.
        if ($this->total_rows == 0 or $this->per_page == 0)
        {
            return '';
        }

        // Calculate the total number of pages
        $num_pages = round($this->total_rows / $this->per_page);
        $lpm1 = $num_pages - 1;
        $adjacents = 2;
        $output = "";
        // Is there only one page? Hm... nothing more to do here then.
        if($num_pages==0)
        {
           $info = 'Showing : ' . $this->total_rows;
            return $info;
        }
        if ($num_pages == 1)
        {
            $info = 'Showing : ' . $this->total_rows;
            return $info;
        }
        // Determine the current page number.
        $CI = &get_instance();
        if ($CI->uri->segment($this->uri_segment) != 0)
        {
            $this->cur_page = $CI->uri->segment($this->uri_segment);

            // Prep the current page - no funny business!
            $this->cur_page = (int)$this->cur_page;
        }

        $this->num_links = (int)$this->num_links;

        if ($this->num_links < 1)
        {
            show_error('Your number of links must be a positive number.');
        }

        if (!is_numeric($this->cur_page))
        {
            $this->cur_page = 0;
        }

        // Is the page number beyond the result range?
        // If so we show the last page
        if ($this->cur_page > $this->total_rows)
        {
            $this->cur_page = ($num_pages - 1) * $this->per_page;
        }

        $uri_page_number = $this->cur_page;
        $this->cur_page = floor(($this->cur_page / $this->per_page) + 1);
        // Calculate the start and end numbers. These determine
        // which number to start and end the digit links with
        $start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->
            num_links - 1) : 1;
        $end = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->
            num_links : $num_pages;

        // Add a trailing slash to the base URL if needed
        $this->base_url = rtrim($this->base_url, '/') . '/';

        // And here we go...
        $output = '';

        // SHOWING LINKS
        if ($this->show_count)
        {
            $curr_offset = $CI->uri->segment($this->uri_segment);
            $info = 'Showing ' . ($curr_offset + 1) . ' to ';

            if (($curr_offset + $this->per_page) < ($this->total_rows - 1))
                $info .= $curr_offset + $this->per_page;
            else
                $info .= $this->total_rows;

            $info .= ' of ' . $this->total_rows . ' | ';

            $output .= $info;
            $output .= "";
        }

        // Render the "First" link
        if ($this->cur_page > $this->num_links)
        {
            //	echo $extra_param=json_encode($extra_param);
            $output .= $this->first_tag_open . $this->getAJAXlink('', $this->first_link) . $this->
                first_tag_close;
        }

        // Render the "previous" link
        if ($this->cur_page != 1)
        {
            //echo $extra_param=json_encode($extra_param);
            $i = $uri_page_number - $this->per_page;
            if ($i == 0)
                $i = '';
            $output .= $this->prev_tag_open . $this->getAJAXlink($i, $this->prev_link) . $this->
                prev_tag_close;
        }

        // Write the digit links

        // for ($loop = $start - 1; $loop <= $end; $loop++)
        {
            $loop = $start - 1;
            $i = ($loop * $this->per_page) - $this->per_page;

            if ($i >= 0)
            {
                if ($this->cur_page == $loop)
                {
                    $output .= $this->cur_tag_open . $loop . $this->cur_tag_close; // Current page
                }
                else
                {
                    //	echo $extra_param=json_encode($extra_param);
                    $n = ($i == 0) ? '' : $i;
                    $output .= $this->num_tag_open . $this->getAJAXlink($n, $this->first_link) . $this->
                        num_tag_close;
                }
            }
            else
                if ($num_pages < 7 + ($adjacents * 3)) //not enough pages to bother breaking it up
                {
                    for ($counter = 1; $counter <= $num_pages; $counter++)
                    {
                      //  echo "in this";
                        if ($counter == $this->cur_page)
                            $output .= $this->cur_tag_open . $this->getAJAXlink($this->get_offset($counter),
                                $counter) . $this->first_tag_close;
                        else
                            $output .= $this->first_tag_open . $this->getAJAXlink($this->get_offset($counter),
                                $counter) . $this->first_tag_close;
                    }
                    $output .= $this->first_tag_open . $this->getAJAXlink($this->cur_page * $this->
                        per_page, $this->next_link) . $this->first_tag_close;

                }
                elseif ($this->cur_page < 1 + ($adjacents * 3))
                {

                    $n = ($i == 0) ? '' : $i;
                    for ($counter = 1; $counter < 4 + ($adjacents * 3); $counter++)
                    {

                        if ($counter == $this->cur_page)
                            $output .= $this->cur_tag_open . $this->getAJAXlink($this->cur_page, $counter) .
                                $this->first_tag_close;
                        else
                            $output .= $this->first_tag_open . $this->getAJAXlink($this->get_offset($counter),
                                $counter) . $this->first_tag_close;
                    }

                    $output .= '<li class="disabled"><a>...</a></li>';
                    $output .= $this->first_tag_open . $this->getAJAXlink($this->get_offset($lpm1 -
                        1), $lpm1 - 1) . $this->first_tag_close;
                    $output .= $this->first_tag_open . $this->getAJAXlink($this->get_offset($lpm1),
                        $lpm1) . $this->first_tag_close;
                    $output .= $this->first_tag_open . $this->getAJAXlink($this->cur_page * $this->
                        per_page, $this->next_link) . $this->first_tag_close;

                }
                elseif ($num_pages - ($adjacents * 3) > $this->cur_page && $this->cur_page > ($adjacents *
                    3))
                {
                    $i = ($this->cur_page * $this->per_page) - $this->per_page;
                    $output .= $this->first_tag_open . $this->getAJAXlink(1, 1) . $this->
                        first_tag_close;
                    $output .= $this->first_tag_open . $this->getAJAXlink(($this->cur_page * 2 - 1),
                        2) . $this->first_tag_close;

                    $output .= '<li class="disabled"><a>...</a></li>';

                    for ($counter = $this->cur_page - $adjacents; $counter <= $this->cur_page + $adjacents;
                        $counter++)
                    {
                        if ($counter == $this->cur_page)
                            $output .= $this->cur_tag_open . $this->getAJAXlink($this->get_offset($counter),
                                $counter) . $this->first_tag_close;
                        else
                            $output .= $this->first_tag_open . $this->getAJAXlink($this->get_offset($counter),
                                $counter) . $this->first_tag_close;
                    }

                    $output .= '<li class="disabled"><a>...</a></li>';
                    $i = (($num_pages * $this->per_page) - $this->per_page);
                    $output .= $this->first_tag_open . $this->getAJAXlink($this->get_offset($lpm1 -
                        1), $lpm1 - 1) . $this->first_tag_close;
                    $output .= $this->first_tag_open . $this->getAJAXlink($this->get_offset($lpm1),
                        $lpm1) . $this->first_tag_close;
                    $output .= $this->first_tag_open . $this->getAJAXlink($this->cur_page * $this->
                        per_page, $this->next_link) . $this->first_tag_close;


                }
            //close to end; only hide early pages
                else
                {
                    $i = round($this->total_rows / $this->per_page);
                    $output .= $this->first_tag_open . $this->getAJAXlink(1, 1) . $this->
                        first_tag_close;
                    $output .= $this->first_tag_open . $this->getAJAXlink(($this->cur_page * 2 - 1),
                        2) . $this->first_tag_close;
                    $output .= '<li class="disabled"><a>...</a></li>';
                    for ($counter = $i - (2 + ($adjacents * 3)); $counter < $i; $counter++)
                    {
                        if ($counter == $i)
                            $output .= $this->cur_tag_open . $this->getAJAXlink($this->get_offset($counter),
                                $counter) . $this->first_tag_close;
                        else
                            $output .= $this->first_tag_open . $this->getAJAXlink($this->get_offset($counter),
                                $counter) . $this->first_tag_close;
                    }
                    $output .= $this->first_tag_open . $this->getAJAXlink($this->cur_page * $this->
                        per_page, $this->next_link) . $this->first_tag_close;
                }

        }


        // Kill double slashes.  Note: Sometimes we can end up with a double slash
        // in the penultimate link so we'll kill all double slashes.
        $output = preg_replace("#([^:])//+#", "\\1/", $output);

        // Add the wrapper HTML if exists
        $output = $this->full_tag_open . $output . $this->full_tag_close;

        return $output;
    }

    function getAJAXlink($count, $text)
    {

        $array_key = array_keys($this->additional_param);
        $this->cur_page=$text;

        $this->additional_param['page'] = $count;
        $add = json_encode($this->additional_param);
        if ($this->div == '') // return '<a href="' . $this->anchor_class . ' ' . $this->base_url . $count . '">' .
            //     $text . '</a>';

            $url = "$this->base_url";
        $url = '"' . $this->base_url . '"';
        $pram = "'" . $add . "'";

        $jsfunction = "onclick=ajaxdata($url,$pram)";
        return $string = "<a href='javascript:void(0)' $jsfunction >$text </a>";

    }
    function get_disable($count)
    {
        return "<a href=''>";
    }
    function get_offset($page_number)
    {
       // $this->cur_page=$page_number;
       if($page_number==1)
       {
        return 0;
       }
        return $offset = ($page_number-1) * $this->per_page - 1;
    }

}
// END Pagination Class


?>
<!--<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>-->
<script type="text/javascript">

function ajaxdata(url_link,postdata)
{
    //alert('page');
   //alert(postdata+url_link);
   var base="<?php

echo site_url()

?>";
   var link=base+url_link;
  // alert(link);
   if ($("#overlay").length > 0) {
            removeOverlay();
        } else {
            displayOverlay("Loading...");
        }
        
   $.ajax({
            type:'POST',
          
           url: link,
            data:{"data":postdata},
            // data:{postdata},
             beforeSend: function() {
            //  $("#loading-image1").show();
           },
            success: function(response) {
               // alert(response);
             $(".record").html(response);
                $("#overlay").remove();
              //$("#loading-image1").hide();
             }});
}
</script>
