<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">
 <div class="box" id="record">
                  <table id="example" class="table table-bordered table-striped" style="font-size:13px;">
                    
                    <thead>
                      <tr>
                       <th>Cab Model</th> 
                       <th>Cab Class</th>
                        <th>Cab Plate No</th>
                         <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(sizeof($cab_details))
                      {
                         
                        for($i=0;$i<sizeof($cab_details);$i++)
                        {
                         
                      ?>
                      <tr>
                       
                        <td><?php echo $cab_details[$i]->cab_model;?></td>
                        <td><?php echo $cab_details[$i]->title;?></td>
                        <td><?php echo $cab_details[$i]->cab_plate_no;?></d>
                        
                     
                      
                     
                       <img src="<?php ?>/images/loaders/loader19.gif" id="image_<?php ?>" style="display: none;"/>
                    
                       <td><a href="javascript:void(0);" onclick="changeStatus('<?php echo $cab_details[$i]->cab_id;?>','loading_<?php echo $cab_details[$i]->cab_id;?>', 'span_<?php echo $cab_details[$i]->cab_id;?>')">
                                    <?php if($cab_details[$i]->isactive=='0'){?>
                                    <span id="span_<?php echo $cab_details[$i]->cab_id?>" class="label label-warning">Inactive</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $cab_details[$i]->cab_id?>" class="label label-success">Active</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $cab_details[$i]->cab_id;?>" style="display: none;"><img src="<?php echo base_url();?>/images/loader19.gif" /></div>
                       </td>
                     
                      <td>
                  <a href="<?=site_url('Cab/add_cab').'/'.$cab_details[$i]->cab_id; ?>" title="Edit"><i class="fa fa-pencil-square-o fa-2x"></a></i>                  
                                 | 
                                  
                                  <a href="javascript:void(0);" onclick="changedeleteStatus('<?php echo $cab_details[$i]->cab_id;?>','delloading_<?php echo $cab_details[$i]->cab_id;?>', 'spandel_<?php echo $cab_details[$i]->cab_id;?>')">
                                    <?php if($cab_details[$i]->isdelete=='0'){?>
                                    <span id="spandel_<?php echo $cab_details[$i]->cab_id?>" class="fa fa-trash fa-2x" style="color:red;"></span></a>
                                    <?php }?>
                                    <br /><div id="image_<?php echo $cab_details[$i]->cab_id;?>" style="display: none;"><img src="<?php echo base_url();?>/images/loader19.gif" /></div>

                      </td>
                      <?php
                      
                        }
                        
                      }
                      ?>
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  <?php
                  
                   ?>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $link ; ?>
                    </div>
        
          </div>
                </div>