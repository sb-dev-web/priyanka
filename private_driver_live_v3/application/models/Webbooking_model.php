<?php

	class Webbooking_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		function pre_booking($input_method)
		{

			//print_r($input_method);
			$lat = $input_method['pickup_latitude'];
			$lng = $input_method['pickup_longitude'];
			$pickup_location = urldecode($input_method['pickup_location']);
			$userid = $input_method['user_id'];
			$destination_lat = $input_method['destination_latitude'];
			$detination_lng = $input_method['destination_longitude'];
			$destination_location = urldecode($input_method['destination_location']);
			$cab_type = $input_method['cab_type'];
			$comment = (@$input_method['comment'] == '') ? "" : @$input_method['comment'];
			$comment = urldecode($comment);
			$date = get_gmt_time();
			$booking_time = $input_method['booking_time'];
			$is_prebooking = "Y";
			$is_fixrate = "Y";
			$airport_id = "0";
			$os_type = "web";
			$discount_amount = "0.00";
			$app_version = "";
			$coupon_code = "";
			$discount = "";

			//start tran
			$this -> db -> trans_begin();

			$data = array(
				'user_id' => $userid,
				'createdon' => $date,
				'booking_time' => $booking_time,
				"pickup_latitude" => $lat,
				"pickup_longitude" => $lng,
				"pickup_location" => $pickup_location,
				"destination_latitude" => $destination_lat,
				"destination_longitude" => $detination_lng,
				"destination_location" => $destination_location,
				"cab_type" => $cab_type,
				"comment" => $comment,
				"discount_amount" => $discount,
				"coupon" => $coupon_code,
				"is_prebooking"=>$is_prebooking,
				"is_fixrate" => $is_fixrate,
				"airport_id" => $airport_id,
				"app_version" => $app_version,
				"os_type" => $os_type,
				"status"=>"WAITING",
				"booking_type"=>"WEB_PRE_BOOKING",
				"payment_mode"=>"PRE_PAID",
				//"company_id"=>$company_id
			);
			$str = $this -> db -> insert('cab_booking', $data);
			// echo "pp". $this->db->last_query();
			 $booking_id = $this -> db -> insert_id();

			if ($booking_id)
			{
				$CBN = "PD" . sprintf("%05d", $booking_id);
				$this -> db -> query("update cab_booking set booking_number='" . $CBN . "' where id=$booking_id");
								
				//insert the amount of pre booking in payment table 
				$payment_detail=array("booking_id"=>$booking_id,
							          "billing_amount"=>$input_method['billing_amount'],
							          "fix_amount"=>$input_method['billing_amount'],
							          "km_driven"=>"",
							          "km_charge"=>"",
							          "is_paid"=>"1",
							          "status"=>"",
							          "createdon"=>get_gmt_time(),
							          "paymenton"=>get_gmt_time(),
							          "payment_type"=>"PRE_PAID",
							          "description"=>"success",
							          
				);
				$this->db->insert("payment_detail",$payment_detail);
				if ($this -> db -> trans_status() === FALSE)
				{
					$this -> db -> trans_rollback();
					return false;
				}
				else
				{
					$this -> db -> trans_commit();
					$this->db->trans_complete();
				}

				return $booking_id;

			}
			else
			{
				return false;
			}
		}
		
		function CabHireRate($type)
		{
			$query =$this->db->get_where("cab_hire_rate",array("type"=>$type));
			
			if($query->num_rows()>0)
			{
				
				
				return $query->row_array();
			}else
				{
					return FALSE;
				}
			
		}
		
		function HourlyBooking($input_method)
		{
			$lat = $input_method['pickup_latitude'];
			$lng = $input_method['pickup_longitude'];
			$pickup_location = urldecode($input_method['pickup_location']);
			$userid = $input_method['user_id'];
			$destination_lat = $input_method['destination_latitude'];
			$detination_lng = $input_method['destination_longitude'];
			$destination_location = urldecode($input_method['destination_location']);
			$cab_type = $input_method['cab_type'];
			$comment = (@$input_method['comment'] == '') ? "" : @$input_method['comment'];
			$comment = urldecode($comment);
			$date = get_gmt_time();
			$booking_time = $input_method['booking_time'];
			$is_prebooking = "Y";
			$is_fixrate = "Y";
			$airport_id = "0";
			$os_type = "web";
			$discount_amount = "0.00";
			$app_version = "";
			$coupon_code = "";
			$discount = "";
			$hour=$input_method['hour'];
			
			$this -> db -> trans_begin();

			$data = array(
				'user_id' => $userid,
				'createdon' => $date,
				'booking_time' => $booking_time,
				"pickup_latitude" => $lat,
				"pickup_longitude" => $lng,
				"pickup_location" => $pickup_location,
				"destination_latitude" => $destination_lat,
				"destination_longitude" => $detination_lng,
				"destination_location" => $destination_location,
				"cab_type" => $cab_type,
				"comment" => $comment,
				"discount_amount" => $discount,
				"coupon" => $coupon_code,
				"is_prebooking"=>$is_prebooking,
				"is_fixrate" => $is_fixrate,
				"airport_id" => $airport_id,
				"app_version" => $app_version,
				"os_type" => $os_type,
				"booking_type"=>"HOURLY_BOOKING",
				"status"=>"CONFIRMED",
				"hour"=>$hour
				//"company_id"=>$company_id
			);
			$str = $this -> db -> insert('cab_booking', $data);
			// echo "pp". $this->db->last_query();
			 $booking_id = $this -> db -> insert_id();

			if ($booking_id)
			{
				$CBN = "PD" . sprintf("%05d", $booking_id);
				$this -> db -> query("update cab_booking set booking_number='" . $CBN . "' where id=$booking_id");
				//make a web user
				$web_user=array("booking_id"=>$booking_id,
				"name"=>$input_method['name'],
				 "email"=>$input_method['email'],
				 "phone"=>$input_method['phone'],
				 "country_code"=>$input_method['country_code'],
				 "created_on"=>$date,
				);
				$this->db->insert("web_user",$web_user);
				
				//insert the amount of pre booking in payment table 
				$payment_detail=array("booking_id"=>$booking_id,
							          "billing_amount"=>$input_method['billing_amount'],
							          "fix_amount"=>$input_method['billing_amount'],
							          "km_driven"=>"",
							          "km_charge"=>"",
							          "is_paid"=>"",
							          "status"=>"",
							          
				);
				$this->db->insert("payment_detail",$payment_detail);
				if ($this -> db -> trans_status() === FALSE)
				{
					$this -> db -> trans_rollback();
					return false;
				}
				else
				{
					$this -> db -> trans_commit();
					$this->db->trans_complete();
				}

				return $booking_id;

			}
			else
			{
				return false;
			}
		}
		
		function GetUser($auth_token)
		{
			$this->db->select("*");
			$query =$this->db->get_where("user",array("auth_token"=>$auth_token,"is_active"=>"1","is_deleted"=>"0"));
			if($query->num_rows())
			{
				 $data =$query->row_array();
				return $data;
			}else
				{
					return false;
				}
		}

	}
?>
