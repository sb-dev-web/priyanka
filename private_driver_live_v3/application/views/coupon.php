<link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.css"); ?>">

        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Campagin
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Campagin</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="maincontent">
            <div class="maincontentinner">
         <div id="confirm" class="modal hide fade">
                        <div class="modal-body" id="modal_text">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Upload</button>
                            <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                        </div>
                    </div>
                    <div id="confirm_delete" class="modal hide fade">
                        <div class="modal-body" id="modal_text_delete">
                        Are you sure want to upload?
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                            <button type="button" data-dismiss="modal" class="btn">No</button>
                        </div>
                    </div>
          
          
          <!-- Default box -->
          <div class="box" id="record">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <a href="<?php echo site_url('Generalinfo/add_coupon');?>"><input type="button" class="btn btn-block btn-warning" value="Add" style="float:right; margin-right:7px;margin-top:8px; width:10%;"/></a><br/><br/>
             
            </div>
            <div class="box-body record">
              <br>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Discount</th>
                         <th>Description</th>
                        <th>Code</th>
                        <th>Start Date</th>
                        <th>Expire Date</th>
                         <th>Action</th>
                        <th>Status</th>
                       
                        
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(sizeof($campagin))
                      {
                        for($i=0;$i<sizeof($campagin);$i++)
                        {
                         
                          
                      ?>
                      <tr>
                        <td><?php echo $campagin[$i]->title;?></td>
                        <td><?php echo $campagin[$i]->offer.' '.'NOK';?></td>
                         <td><?php echo $campagin[$i]->description;?></td>
                        <td><?php echo $campagin[$i]->code;?></td>
                       <td><?php echo $campagin[$i]->start_date;?></td>
                       <td><?php echo $campagin[$i]->expire_date;?></td>
                       
                       
                       </td>  
                       
                      
                      
                       <td><a href="javascript:void(0);" onclick="changeStatus('<?php echo $campagin[$i]->id;?>','loading_<?php echo $campagin[$i]->id;?>', 'span_<?php echo $campagin[$i]->id;?>')">
                                    <?php if($campagin[$i]->isactive=='0'){?>
                                    <span id="span_<?php echo $campagin[$i]->id?>" class="label label-warning">Inactive</span></a>
                                    <?php }else{?>
                                    <span id="span_<?php echo $campagin[$i]->id?>" class="label label-success">Active</span></a>
                                    <?php }?>
                                    <br /><div id="loading_<?php echo $campagin[$i]->id;?>" style="display: none;"><img src="<?php echo base_url();?>/images/loader19.gif" /></div>

                     </td>
                     <td>
                      
                      <a href="<?=site_url('Generalinfo/add_coupon').'/'.$campagin[$i]->id; ?>" title="Edit"><i class="fa fa-pencil-square-o"></a></i>
                      |
                      
                      <a href="javascript:void(0);" onclick="changedeleteStatus('<?php echo $campagin[$i]->id;?>','delloading_<?php echo $campagin[$i]->id;?>', 'spandel_<?php echo $campagin[$i]->id;?>')">
                                    <?php if($campagin[$i]->isdelete=='0'){?>
                                    <span id="spandel_<?php echo $campagin[$i]->id?>" class="fa fa-trash" style="color:red;"></span></a>
                                    <?php }?>
                                    <br /><div id="delloading_<?php echo $campagin[$i]->id;?>" style="display: none;"><img src="<?php echo base_url();?>images/loader19.gif" /></div>

                      </td>
                    
                      
                      <?php
                      
                        }
                        
                      }
                      else
                      {
                      ?>
                       <td colspan="10"><center>NO DATA FOUND</center></td>
                      
                      <?php
                      }
                      ?>
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  <div class="row">
                    <div class="col-md-12 text-right">
                        <?php
                      
                        echo $link ; ?>
                    </div>
        
          </div>
                </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      
      
     
      
 <!--pop up -->
 <div id="dialog" style="display: none">
        <div id="dvMap" style="height: 380px; width: 580px;">
        </div>
    </div>
      
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
   


     
       <script>
    function changeStatus(id,loadingdiv,spanid)
{
  //alert();
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    if(spanValue=='Inactive')
    {  
       status = '1';
    }
    $.ajax({type:'POST',url: '<?php echo site_url('Generalinfo/Coupon_status');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       statusfeedback(response,spanid,loadingdiv);
     }});
}
function statusfeedback(retstatus,spanid,loadingdiv) 
{
    //alert(retstatus);
    if(retstatus=='0')
    {
        $('#'+spanid).removeClass('label-success').addClass('label-warning');
        $('#'+spanid).text("Inactive");
    }
    else if(retstatus=='1')
    {

        $('#'+spanid).removeClass('label-warning').addClass('label-success');
        $('#'+spanid).text("Active");
    }
     $('#'+loadingdiv).css('display','none');
    
}
</script>
       <script>
function changedeleteStatus(id,loadingdiv,spanid)
{
  //alert();
  var r=confirm("Are you sure you want to delete?");
  if (r==true) {
   
    var spanValue= $('#'+spanid).text();
    //alert(isactive);
    $('#'+loadingdiv).css('display','inline');
    var status = '0';
    $.ajax({type:'POST',url: '<?php echo site_url('Generalinfo/delete_campagin');?>',
    
    data:{"id":id,"status":status},
    success: function(response) {
       $("#delloading_"+id).closest('tr').fadeOut('slow')
     }});
}
else
{
  
}
}

   
</script>