<?php
	class Company_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}

		function fetch_total_company()
		{
			$query = $this -> db -> query("select c.*,count(u.id) as company_user from company as c left join user as u on c.id=u.company_id and u.is_active='1' where c.isdelete='0' group by c.id  order by c.id DESC  ");
			 $this->db->last_query();
			//echo $this->db->last_query();
			return $query -> result();

		}

		function company_count()
		{
			$this -> db -> select("id");

			$this -> db -> where("isdelete", "0");
			$this -> db -> from("company");
			$query = $this -> db -> get();

			return ($query -> result_id -> num_rows);

		}

		function save_company($id = "0")
		{

			if ($id > 0)
			{

				$this -> db -> where('id', $id);
				//$this->db->set('create_on', GMT_DATE, FALSE);
				$arr_feilds = array(
					'company_name' => $_POST['company_name'],
					'email' => $_POST['email'],
					'phone' => $_POST['phone'],
					'address' => $_POST['address'],
					// 'password'=>md5($_POST['password']),
					'createdon' => get_gmt_time()
				);
				if ($_POST['btcustomer_id'] > 0)
				{
					$arr_feilds['is_card_added'] = 1;
				}
				//print_r($arr_feilds);
				//exit;

				$this -> db -> update('company', $arr_feilds);

				if (!empty($_POST['btcustomer_id']))
				{

					//ad the cc detail
					$cc_number = str_replace("-", "", $_POST['cc_number']);
					$cc_detail = array(
						"customer_id" => $id,
						"credit_card_no" => credit_card_number_admin($cc_number),
						"braintree_customer_id" => $_POST['btcustomer_id'],
						"braintree_token" => $_POST['token'],
						"card_type" => $_POST['type'],
						"expire_date" => $_POST['month'] . '/' . $_POST['year'],

						"createdon" => get_gmt_time()
					);
					//print_r($cc_detail);
					//exit;
					$savecc = $this -> db -> insert("company_card_info", $cc_detail);
					if ($this -> db -> insert_id())
					{
						return 1;
					}
					else
					{
						return 0;
					}
				}

				return 1;
			}
			else
			{
				$arr_feilds = array(
					'company_name' => $_POST['company_name'],
					'email' => $_POST['email'],
					'phone' => $_POST['phone'],
					// 'address'=>$_POST['address'],
					// 'password'=>md5($_POST['password']),
					'createdon' => get_gmt_time(),
					"is_card_added" => "1",
				);
				$this -> db -> insert('company', $arr_feilds);

				$insert = $this -> db -> insert_id();
				if ($insert)
				{
					//ad the cc detail
					if ($_POST['month'] > 0)
					{
						$cc_number = str_replace("-", "", $_POST['cc_number']);
						$cc_detail = array(
							"customer_id" => $insert,
							"credit_card_no" => credit_card_number_admin($cc_number),
							"braintree_customer_id" => $_POST['btcustomer_id'],
							"braintree_token" => $_POST['token'],
							"card_type" => $_POST['type'],
							"expire_date" => $_POST['month'] . '/' . $_POST['year'],

							"createdon" => get_gmt_time()
						);
						$savecc = $this -> db -> insert("company_card_info", $cc_detail);
						if ($this -> db -> insert_id())
						{
							return 1;
						}
						else
						{
							return 0;
						}
					}
				}

				return 1;

			}
		}

		function DeleteCompanyCard($company_id)
		{
			//update comapny tabel
			$query = $this -> db -> get_where("company_card_info", array(
				"customer_id" => $company_id,
				"isactive" => "1"
			));
			//echo $this->db->last_query();
			if ($query -> num_rows() > 0)
			{
				$data = $query -> row_array();
				$btcustomer_id = $data['braintree_customer_id'];
			}
			$update = "update company set is_card_added='0' where id=$company_id";
			$this -> db -> query($update);

			//update the card_info
			$query = $this -> db -> query("update company_card_info set isactive='0' where customer_id=$company_id");
			if ($query)
			{
				return $btcustomer_id;
			}
			else
			{
				return false;
			}
		}

		function get_company($id)
		{
			if ($id > 0)
				$query = $this -> db -> query("select company.*,company_card_info.card_type,company_card_info.credit_card_no from company left join company_card_info on company_card_info.customer_id=company.id and company_card_info.isactive='1' where isdelete='0'  and company.id='$id' ");
			//echo $this->db->last_query();exit;
			return $query -> result();
		}

		function delete_company($id)
		{
			$this -> db -> where('id', $id);
			$this -> db -> set('isdelete', '1');
			$this -> db -> update("company");
			//Delete related user in user company table 
			$update_str= array("is_delete"=>"1");
			$this->db->where("company_id",$id);
			$this->db->update("user_company",$update_str);
			return 1;
		}

		function get_company_user($cid)
		{
			$get_data = $this -> db -> query("select user.company_name,user.email,user.name,concat(user.country_code,user.phone) as phone from user,company where user.company_id='$cid' and company.id='$cid' and user.is_active='1'");
			// echo $this->db->last_query();
			$count = $get_data -> num_rows();
			if ($count)
			{
				return $get_data -> result();
			}
			else
			{
				return array();
			}
		}

		function check_email($email)
		{
			$check = $this -> db -> query("select * from company where email='$email' and isdelete='0'");
			$count = $check -> num_rows();
			if ($count)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

	}
