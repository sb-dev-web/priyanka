<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mobile_info extends CI_Controller
{

    function __construct()
    {
         parent::__construct();
        $this->load->helper(array(
            'form',
            'url',
            'function_helper'));
        $this->load->model('Mobile_info_model');
        $this->load->library(array(
            'session',
            'email',
            'paginationlib',
            'Pagination'));
        
         $user = $this->session->userdata('logged_in');
        if (!$user)
        {
            redirect('Welcome');
        }

    }
    

    public function index()
    {
       $this->load->library('Ajax_pagination');
        $this->Mobile_info_model->contact_count();
        $pagingconfig = $this->paginationlib->initPagination("/Mobile_info/ajaxPaginationData/", $this->
            Mobile_info_model->contact_count(), 10, 3,array());


        $this->ajax_pagination->initialize($pagingconfig);
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //$data['link'] = $this->pagination->create_links();
        $data['link'] = $this->ajax_pagination->create_links();
        $mobile = $this->Mobile_info_model->fetch_total_contact($pagingconfig['per_page'], 0);
       
        $data['mobile'] = $mobile;
        $data['active_page'] = 'mobile';
        $this->load->view('site-nav/header', $data);
        $this->load->view('contact');
        $this->load->view('site-nav/footer');
    }
    function ajaxPaginationData()
    {
        $this->load->library('Ajax_pagination');
        $postdata = $this->input->post('data');
        $post=json_decode($postdata,true);
        $page=$post['page'];
        if (!$page)
        {
            $offset = 0;
        }
        else
        {
            $offset = $page;
        }

        $pagingconfig = $this->paginationlib->initPagination("/Mobile_info/ajaxPaginationData/", $this->
            Mobile_info_model->contact_count(), 10, $page['post']);
        $this->ajax_pagination->initialize($pagingconfig);
        $data['link'] = $this->ajax_pagination->create_links();
        //get the posts data
        $data['driver'] = $this->Mobile_info_model->fetch_total_contact($pagingconfig['per_page'],
            $page);

        //load the view
        $this->load->view('contact_ajax', $data, false);
    }
    public function perPage()
    {
        $this->load->library('Ajax_pagination');
        return 2;
    }

   
    public function add_mobile()
    {
        //$driver=$this->Driver_model->add_driver();
        //print_r($driver);
        //$data['driver']=$driver;
        @$mobile[0]->country_code = $mobile[0]->contact= "";
        $mobile = "";
        if ($this->uri->segment(3) == false)
        {

            $id = 0;
         $mobile = $this->Mobile_info_model->country_code();
       
                 }
        else
        {
            $id = $this->uri->segment(3);
            $mobile = $this->Mobile_info_model->get_contact($id);
        }
        $data['mobile'] = $mobile;
        $data['active_page'] = 'mobile';
        $this->load->view('site-nav/header', $data);
        $this->load->view('add_mobile');
        $this->load->view('site-nav/footer');
    }

    public function submit_contact()
    {
       
        $id = $_POST['id'];
      
        $res = $this->Mobile_info_model->save_contact($id);
        if ($res)
        {

          echo 1;
            }

        else
        {
            echo 0;
        }
        //echo $res;
    }
    function check_serial_no()
    {
        $res = $this->Mobile_info_model->check_serial_no();
        if ($res)
        {

          echo 1;
        }
        else
        {
            echo 0;
        }
    }
    function check_mobile_no()
    {
        $res = $this->Mobile_info_model->check_mobile_no();
        if ($res)
        {

          echo 1;
        }
        else
        {
            echo 0;
        }
    }
    function change_delete_status()
    {
        $id = $_POST['id'];
        echo $this->Mobile_info_model->delete_contact($id);
    }

    
}
