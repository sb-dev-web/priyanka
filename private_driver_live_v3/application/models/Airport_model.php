<?php 

class Airport_model extends CI_Model
	{

		public function __construct()
		{
			$this -> load -> database();
		}
		
		function GetAirport()
		{
			$airport= $this->db->get("airport");
			if($airport->num_rows()>0)
			{
				return $airport->result_array();
			}else
				{
					return FALSE;
				}
		}
		
		function GetAirportDetail($id)
		{
			$airport= $this->db->get_where("airport",array("id"=>$id));
			if($airport->num_rows()>0)
			{
				return $airport->row_array();
			}else
				{
					return FALSE;
				}
		}
		
		
		

	}
?>