<?php

defined('BASEPATH') or exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
//error_reporting(0);
class Customer extends REST_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->library(array(
            'upload',
            'Braintree_lib',
            'Twilio',
            'email'));
        $this->load->library('xmpp', false);
        $this->load->helper(array(
            'form',
            'url',
            'string',
            'constants_helper',
            'function_helper',
            'braintree_function'));

        $this->load->model(array('Customer_model','General_model'));

    }

    /* register user */
    function register_user()
    {
        /* check that user is already register or not */
        $input_method = $this->webservices_inputs();
        //$this->validate_param('register-user', $input_method);
        $this->param_validate($input_method, array(
            "name",
            "email",
            "country_code",
            "phone",
            "credit_card_no",
            "expire_date",
            "cvv_no"));
        // check user is exist or not

        $user_data = $this->Customer_model->check_exist_email($input_method);
        if (sizeof($user_data) > 0)
        {
            $this->response(array('message' => ALREADY_REGISTER, 'status' => "0"), 200);
        }
        else
        {
            $name = $input_method['name'];
            $email = $input_method['email'];

            $phone = $input_method['phone'];
            $country_code = $input_method['country_code'];
            $phone_number = "+" . $country_code . $phone;
            $credit_card_no = $input_method['credit_card_no'];
            $cvv_no = $input_method['cvv_no'];
            $expire_date = $input_method['expire_date'];

            $data = array(
                "name" => $name,
                "email" => $email,
                "phone" => $phone,
                "ccnumber" => $credit_card_no,
                "cvv_no" => $cvv_no,
                "exp_date" => $expire_date);


            $braintree_response = Create_withCreditCardAndVerification($data);

            if ($braintree_response['status'] == "1")
            {
                //Insert into user and CC info table
                $unique_number = random_number();
                $input_method['varifycode'] = $unique_number;
                $user_id = $this->Customer_model->save_user($input_method, $braintree_response);
                if ($user_id)
                {
                    $user_name = USER_JID_NAME . '-' . $user_id;


                    if (@$input_method['is_social'] == 'N')
                    {
                        $password = $input_method['password'];
                        $password = trim($password);
                        $is_social = "N";
                    }
                    else
                    {
                        $password = "123456";
                        $password = trim($password);
                        $is_social = "Y";
                    }


                    //make a braintree user


                    $param = array(
                        $user_name,
                        $password,
                        $name,
                        $email);

                    $xmpp_response = $this->xmpp->api("add", $param);

                    $message = VARIFYSMS . $unique_number;
                    // echo $message;
                    //exit;

                    send_sms($phone_number, $message);
                    $customer = array(
                        "customer_jid"=>$userid="$user_name", //19 oc add new tag for andorid
                        "customer_id" => $userid = "$user_id",
                        "name" => $name = $input_method['name'],
                        "email" => $email = $input_method['email'],
                        "dob" => $dob = $input_method['dob'],
                        "phone" => $phone,
                        'country_code' => $country_code,
                        "credit_card" => $credit_card_no,
                        "is_social" => $is_social,
                        "is_mobile_varify" => "0" // "code"=>$unique_number,
                            // "messge"=>$message,

                        );


                    $this->response(array(
                        'customer' => $customer,
                        'message' => REGISTER_SUCCESS,
                        'status' => "1"), 200);
                }
                else
                {
                    $this->response(array(

                        'message' => $braintree_response['message'],
                        'errorcode' => $braintree_response['errorcode'],
                        'status' => 0), 200);
                }
                //exit;


            }
            else
            {
                $this->response(array(

                    'message' => $braintree_response['message'],
                    'errorcode' => $braintree_response['errorcode'],
                    'status' => "0"), 200);
            }

        }
    }
    /* check social loign information */
    function social_login()
    {
        $input_method = $this->webservices_inputs();
        //$this->validate_param('social-login', $input_method);
        $this->param_validate($input_method, array(
            "name",
            "email",
            "s_id",
            ));

        $user_data = $this->Customer_model->check_exist_email($input_method);
        //print_r($user_data);
        if (sizeof($user_data))
        {
            $user_update = $this->Customer_model->update_user($input_method);
            $user_data = $this->Customer_model->get_user_detail($input_method);
            if ($user_data > 0)
            {
                foreach ($user_data as $user_detail)
                {

                    $is_registered = "Y";
                    $credit_card = '';
                    // $credit_card=$user_detail->credit_card_no;
                    //$cvv=$user_detail->cvv_no;
                    //$expire_date=$user_detail->expire_date;
                    if ($user_detail->credit_card_no == "")
                    {
                        $iscreditcard = 'N';
                    }
                    else
                    {
                        $iscreditcard = 'Y';
                    }
                    if ($user_detail->is_varify == 0)
                    {
                        $varifycode = $user_detail->varifycode;
                        $phone = $user_detail->phone;
                        $country_code = $user_detail->country_code;
                        $phone_number = "+" . $country_code . $phone;
                        $message = VARIFYSMS . $varifycode;
                        send_sms($phone_number, $message);
                    }

                    $customer = array(
                        "customer_jid"=>makejid($user_detail->user_id,USER_JID_NAME), //19 oc add new tag for andorid
                        "customer_id" => $userid = $user_detail->user_id,
                        "name" => $name = $user_detail->name,
                        "email" => $email = $user_detail->email,
                        "dob" => $dob = $user_detail->dob,
                        "sid" => $sid = $user_detail->s_id,
                        "phone" => $phone = $user_detail->phone,
                        "country_code" => $user_detail->country_code,
                        "credit_card" => $user_detail->credit_card_no, //$credit_card=$user_detail->credit_card_no,
                        "is_social" => "Y",
                        "is_mobile_varify" => "$user_detail->is_varify");
                }
                $this->response(array(
                    'customer' => $customer,
                    'is_registered' => $is_registered,
                    'status' => "1"), 200);
            }


        }
        else
        {
            $customer = array(
                "name" => $input_method['name'],
                "dob" => $input_method['dob'],
                "email" => $input_method['email'],
                "s_id" => $input_method['s_id']);


            $this->response(array(
                'customer' => $customer,
                'is_registered' => 'N',
                'status' => 1), 200);

        }
    }
    /* enter credit crad infomation */

    function user_login()
    {
        $input_method = $this->webservices_inputs();
        // $this->validate_param('user-login', $input_method);
        $this->param_validate($input_method, array(
            "email",
            "password",

            ));
        $user_data = $this->Customer_model->check_exist_email($input_method);
        if (sizeof($user_data) > 0)
        {


            $user_data = $this->Customer_model->check_login($input_method);
            if (sizeof($user_data) > 0)
            {

                // update the user device token
                foreach ($user_data as $login)
                {
                    if ($login->is_company == "Y" && $login->is_active == "N" && $login->is_varify ==
                        "1")
                    {


                        $this->response(array('message' => NOT_APPROVE, 'status' => "0"), 200);
                    }

                    if ($login->is_varify == 0)
                    {
                        $varify_code = $login->varifycode;
                        $phone = "+" . $login->phone;
                        $message = VARIFYSMS . $varify_code;
                        send_sms($phone, $message);
                    }
                    $phone = $login->phone;
                    //if($today)
                    $customer = array(
                        "customer_jid"=>$userid=USER_JID_NAME.$login->id,
                        "customer_id" => $userid = $login->id,
                        "name" => $name = $login->name,
                        "email" => $email = $login->email,
                        "dob" => $dob = $login->dob,
                        "phone" => $phone,
                        "country_code" => $login->country_code,
                        "is_social" => "N",
                        "is_mobile_varify" => "$login->is_varify",
                        "credit_card" => "$login->credit_card_no",
                        "iscompany_user" => $login->is_company);


                }
                //$this->Customer_model->update_device($input_method);


                $this->response(array(
                    'customer' => $customer,
                    'message' => SUCCESS,
                    'status' => "1"), 200);
            }
            else
            {
                $this->response(array('message' => CUSTOMER_LOGIN_ERROR, 'status' => "0"), 200);
            }


        }
        else
        {
            $this->response(array('message' => EMAIL_NOT_EXIST, 'status' => "0"), 200);
        }
    }


    function forget_password()
    {
        date_default_timezone_set('UTC');
        $input_method = $this->webservices_inputs();
        //$this->validate_param('forget-password', $input_method);
        $this->param_validate($input_method, array("email"));

        $user_data = $this->Customer_model->check_exist_email($input_method);
        // print_r($user_data);
        if (sizeof($user_data) > 0)
        {
            $login_type = $user_data[0]->is_social;
            if ($login_type == 'Y')
            {


                $this->response(array('message' => "Oops! Its seems like you have previously logged in with your facebook account",
                        'status' => 0), 200);
            }
            else
            {

                $mail_sent = SendverificationCode($user_data[0]->email, $user_data[0]->id);
                if ($mail_sent)
                {
                    $this->response(array('message' => 'We send your password on your email. Please check your mail',
                            'status' => 1), 200);
                }
                else
                {
                    $this->response(array('message' => 'Sorry something went wrong', 'status' => 0),
                        200);
                }

            }
        }
        else
        {
            $this->response(array('message' => 'sorry no such a email exist, Please check your email again ',
                    'status' => 0), 200);
        }
    }


    function getuser_detail()
    {
        $input_method = $this->webservices_inputs();
        // $this->validate_param('getuser-detail', $input_method);
        $this->param_validate($input_method, array("user_id"));
        $user_data = $this->Customer_model->get_user($input_method['user_id']);
        if ($user_data[0]->profile_pic != "")
            $user_data[0]->profile_pic = base_url() . CUSTOMER_IMAGE_PATH . $user_data[0]->
                profile_pic;
        $this->response(array('data' => $user_data[0], 'status' => "1"), 200);
    }

    /*register company */

    function company_register()
    {
        $input_method = $this->webservices_inputs();
        //$this->validate_param('company-register', $input_method);
        $this->param_validate($input_method, array(
            "name",
            "company_name",
            "email",
            "password",
            "phone"));
        $check_exisitng = $this->Customer_model->check_exist_email($input_method);

        if (sizeof($check_exisitng) <= 0)
        {

            $unique_number = random_number();
            $input_method['varifycode'] = $unique_number;
            $company_id = $this->Customer_model->save_userinfo_company($input_method);
            if ($company_id)
            {
                $user_name = USER_JID_NAME . '-' . $company_id;
                $password = $input_method['password'];


                $name = $input_method['name'];
                $email = $input_method['email'];

                $param = array(
                    $user_name,
                    $password,
                    $name,
                    $email);
                $this->xmpp->api("add", $param);

                send_mail_to_admin($company_id);
                $this->response(array('message' => COMPANY_ACCOUNT_CREATE, 'status' => "1"), 200);
            }
        }
        else
        {
            $this->response(array('message' => COMPANY_ALREADY_REGISTER, 'status' => "0"),
                200);
        }
    }

    /*****************Fare Estimate  *********************************/

    function fare_estimate()
    {
        $this->load->model('Booking_model');
        $input_method = $this->webservices_inputs();
        //$this->validate_param('fare-estimate', $input_method);
        $this->param_validate($input_method, array(
            "pickup_latitude",
            "pickup_longitude",
            "destination_latitude",
            "destination_longitude",
            "pickup_location",
            "destination_location",
            "cab_type"));
        $cab_type = $input_method['cab_type'];
        $origins = $input_method['pickup_latitude'] . ',' . $input_method['pickup_longitude'];
        $destination = $input_method['destination_latitude'] . ',' . $input_method['destination_longitude'];
        $time_detail = gettimedetail($origins, $destination);
       // $pickup_location = getaddress($input_method['pickup_latitude'], $input_method['pickup_longitude']);
       // $destination_loaction = getaddress($input_method['destination_latitude'], $input_method['destination_longitude']);
        $fare_detail = $this->Booking_model->getfare_detail($cab_type);
        
       // echo "<pre>";
//print_r($time_detail);
        if ($time_detail)
        {
            $duration = $time_detail['duration'];
            $distance = $time_detail['distance'];
            $duration_sec = $time_detail['duration_sec'];
        }
        $fare_detail = $fare_detail[0];
        /*if ($distance > $fare_detail['min_km'])
        {
        $remaining_km = $distance - $fare_detail['min_km'];
        $trip_charge = $remaining_km * $fare_detail['charges_per_km'];
        $trip_charge = $fare_detail['min_charges'] + $trip_charge;
        }
        else
        {
        $trip_charge = $fare_detail['min_charges'];
        }*/
        $min_charge = $fare_detail['min_charges'];
        $journey_charge_per_min = $fare_detail['wait_time_charges'];
        $journey_charge = ($duration_sec / 60) * ($journey_charge_per_min);
        $trip_charge = $distance * $fare_detail['charges_per_km'];
        // $waiting_time = ($duration_sec) * (1 / 10);
        
        $billing_charge = $min_charge + $journey_charge + $trip_charge;
        $billing_charge_min=($billing_charge-25<0) ? "0" : $billing_charge-25;
        $billing_charge_max=$billing_charge+25;
        
        $this->response(array(
            "estimate_bill" => round($billing_charge_min) . "-" . round($billing_charge_max) . " NOK" ,
            "duration" => $duration,
            "distance" => $distance,
            "pickup_location" => $input_method['pickup_location'],
            "destination_location" => $input_method['destination_location'],
            'status' => "1",
            "min"=>$min_charge,
            "journey_charge_per"=>$journey_charge_per_min,
            "trip_charge"=>$trip_charge,
            "journey_charge"=>$journey_charge,
            //"min"=>$
            ), 200);


    }
    //************************Last Panding Invoice Detail******************
    function invoice_detail()
    {
        $input_method = $this->webservices_inputs();
        // $this->validate_param('invoice-detail', $input_method);
        $this->param_validate($input_method, array("user_id"));
        $invoice = $this->Customer_model->invoice_detail($input_method);
        // echo "<pre>";
        // print_r($invoice);
        if ($invoice)
        {
            $invoice = $invoice[0];
            $booking_id = $invoice['booking_id'];
            $CBN = $invoice['booking_number'];
            $pickup_latitude = $invoice['pickup_latitude'];
            $pickup_longitude = $invoice['pickup_longitude'];
            $pickup_location = $invoice['pickup_location'];
            $destination_latitude = $invoice['destination_latitude'];
            $destination_longitude = $invoice['destination_longitude'];
            $destination_location = $invoice['destination_location'];
            $km_driven = $invoice['km_driven'];
            $km_driven_charge=$invoice['km_charge'];
            $journey_time = $invoice['journey_time'];
            $journey_charge = $invoice['waiting_charge'];
            $billing_charge = $invoice['billing_amount'];
            $basic_fare_charge=$invoice['basic_fare_charge'];
            $driver_id = $invoice['driver_id'];
            $driver_name = $invoice['first_name'] . ' ' . $invoice['last_name'];
            $driver_contact = $invoice['contact'];
             $car_number = $invoice['plate_number'];
            $driver_profile = $invoice['profile_pic'];
            $jid = makejid($invoice['user_id'], USER_JID_NAME);
           // $travel_charge = $billing_charge - $waiting_charge - $othercharge;
            $discount = $invoice['discount_amount'];
            
            $this->response(array(
                'booking_id' => $booking_id,
                "CBN" => $CBN,
                "pickup_latitude" => $pickup_latitude,
                "pickup_longitude" => $pickup_longitude,
                "pickup_location" => $pickup_location,
                "destination_latitude" => $destination_latitude,
                "destination_latitude" => $destination_latitude,
                "destination_longitude" => $destination_longitude,
                "destination_location" => $destination_location,
                "km_driven" => $km_driven,
                "km_driven_charge"=>(string)$km_driven_charge,
                "journey_time" => $journey_time,
                "journey_time_charge"=>$journey_charge,
                "basic_fare_charge"=>$basic_fare_charge,
                "discount" => $discount,
                "billing_charge" => (string)$billing_charge,//number_format($billing_charge, 2),
                "customet_jid" => $jid,
                "driver_image" => base_url() . DRIVER_IMAGE_PATH . $driver_profile,
                "car_number" => $car_number,
                "driver_name" => $driver_name,
                "driver_id" => $driver_id,
                "driver_contact" => $driver_contact,
                'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => NO_INVOICE, 'status' => "0"), 200);
        }

    }

    function device_register()
    {
        $input_method = $this->webservices_inputs();
        //$this->validate_param('device-register', $input_method);
        $this->param_validate($input_method, array(
            "user_id",
            "device_type",
            "device_token"));
        $device = $this->Customer_model->device_register($input_method);
        if ($device)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }
    }

    function driver_rating()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array(
            "driver_id",
            "rating",
            "booking_id",
            "user_id",
            ));
        $driver_rating = $this->Customer_model->driver_rating($input_method);
        if ($driver_rating)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }
    }
    function account_varify()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("user_id", "otp"));
        $account_varify = $this->Customer_model->account_varify($input_method);
        if ($account_varify)
        {
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => VARIFYERROR, 'status' => "0"), 200);
        }

    }
    function customer_not_responded()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("booking_id", "driver_id"));
        $booking_id = $input_method['booking_id'];
        $CBN = "PD" . sprintf("%05d", $booking_id);
        $makedriver_free = $this->Customer_model->customer_not_respond($input_method);
        if ($makedriver_free)
        {
            $makedriver_free = $makedriver_free[0];
            $customer_contact = $makedriver_free['country_code'] . $makedriver_free['phone'];
            $message = CUSTOMERTIMEOUTSMS . $CBN;
            send_sms($customer_contact, $message);
            $this->response(array('message' => SUCCESS, 'status' => "1"), 200);
        }
        else
        {
            $this->response(array('message' => SERVER_ERROR, 'status' => "0"), 200);
        }
    }

    function ride_history()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("customer_id", "page_number"));
        $ride_history = $this->Customer_model->ride_history($input_method);
        //  print_r($ride_history);
        if ($ride_history)
        {
            foreach ($ride_history as $data)
            {
                $pickup_lat = $data->pickup_latitude;
                $pickup_long = $data->pickup_longitude;
                $source_data = $data->pickup_location;
                $destination_lat = $data->destination_latitude;
                $destination_long = $data->destination_longitude;
                $destination = $data->destination_location;
                $booking_date = $data->booking_time;
                $CBN = $data->booking_number;
                $booking_id = $data->id;
                $driver_id = $data->driver_id;
                $trackable = $this->Customer_model->driver_track($booking_id);
                $status = $data->status;
                $time = time($booking_date);
                $bookdate = date('d-M-Y', strtotime($booking_date));
                $car_number = ($data->plate_number == '') ? "" : $data->plate_number;
                $booking_time = date('H:i A', strtotime($booking_date));


                $journey_time = $data->journey_time;
                $cab_type = $data->title;
                $ride_status = $data->status;
                //$wait_time = $data->waiting_time;
                $billing_amount = $data->billing_amount;
                $journey_charge = $data->waiting_charge;
                $order_extra_charge = $data->other_charge;
                $tip = $data->tip;
                $total_pay = ($data->user_paid == '') ? "" : $data->user_paid;
                $is_paid = $data->is_paid;
                $km_driven = $data->km_driven;
                $km_driven_charge=$data->km_charge;
                $discount = $data->discount_amount;
                $basic_fare_charge=$data->basic_fare_charge;


                if ($status == 'CUSTOMER_ACCEPTED' && $trackable == 'Y')
                {
                    $trackable = "Y";
                    $driver_detail = $this->Customer_model->get_driver_detail($driver_id);
                    if ($driver_detail)
                    {
                        $driver_detail = $driver_detail[0];
                        $origins = $driver_detail['latitude'] . ',' . $driver_detail['longitude'];
                        $destination = $pickup_lat . ',' . $pickup_long;
                        $time_detail = gettimedetail($origins, $destination);
                        if ($time_detail)
                        {
                            $duration = $time_detail['duration'];
                            $distance = $time_detail['distance'];
                        }
                        else
                        {
                            $duration = "";
                            $distance = "";
                        }
                        $driver_data = array(
                            "driver_id" => $driver_detail['driver_id'],
                            "name" => $driver_detail['first_name'] . ' ' . $driver_detail['last_name'],
                            "contact" => $driver_detail['contact'],
                            "profile_image" => base_url() . DRIVER_IMAGE_PATH . $driver_detail['profile_pic'],
                            "latitude" => $driver_detail['latitude'],
                            "longitude" => $driver_detail['longitude'],
                            "distance" => "$distance",
                            "duration" => "$duration",
                            "car_number" => $car_number,
                            "CBN" => $CBN,

                            );
                        //exit;
                    }
                }
                else
                    if ($status == 'CUSTOMER_ACCEPTED' && $trackable == 'N')
                    {
                        continue;
                    }

                    else
                    {
                        // continue;
                        $trackable = "N";
                        $driver_data = array();

                    }
                    $display_status = "";
                $my_ride[] = array(
                    'source_lat' => $pickup_lat,
                    'source_lng' => $pickup_long,
                    'source' => ($source_data == "") ? "" : $source_data,
                    'destination' => ($destination == "") ? "" : $destination,
                    'date' => $bookdate,
                    'time' => $booking_time,
                    'cab_type' => $cab_type,
                    'journey_time' => "$journey_time",
                    'journey_charge'=>$journey_charge,
                    'CBN' => $CBN,
                    'is_trackable' => $trackable,
                    'booking_id' => "$booking_id",
                    "status" => $status,
                    "journey_distance" => ($km_driven == '') ? "" : $km_driven,
                    
                    "km_driven"=>"$km_driven",
                    "km_charge"=>"$km_driven_charge",
                    "basic_fare_charge"=>"$basic_fare_charge",
                  //  "wait_time" => ($wait_time == '') ? "" : $wait_time,
                    "travel_charge" => ($km_driven_charge == '') ? "" : (string )($km_driven_charge),
                    "discount" => ($discount=="") ? "" : (string)($discount),
                    "tip" => ($tip == '') ? "" : $tip,
                    "billing_amount"=>$billing_amount,
                    "total_pay" => ($total_pay == '') ? "" : $total_pay,
                  //  "order_extra_charge" => ($order_extra_charge == '') ? "" : $order_extra_charge,
                    "display_status" => $display_status,
                    "car_number" => $car_number,
                    "driver_detail" => $driver_data);


            }
            $this->response(array('status' => "1", 'ride' => $my_ride), 200);
        }
        else
        {
            $this->response(array(
                'status' => "1",
                "message" => NO_RIDE_EXIST,
                'ride' => array()), 200);
        }
    }

    function upcoming_ride()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("customer_id", "page_number"));
        $upcoming_ride = $this->Customer_model->upcoming_ride($input_method);
        if ($upcoming_ride)
        {
            foreach ($upcoming_ride as $data)
            {
                $pickup_lat = $data->pickup_latitude;
                $pickup_long = $data->pickup_longitude;
                $source_data = $data->pickup_location;
                $destination_lat = $data->destination_latitude;
                $destination_long = $data->destination_longitude;
                $destination = $data->destination_location;
                $booking_date = $data->booking_time;
                $CBN = $data->booking_number;
                $booking_id = $data->id;
                $driver_id = $data->driver_id;
                $trackable = $this->Customer_model->driver_track($booking_id);
                $status = $data->status;
                $time = time($booking_date);
                $cab_type = $data->title;
                $bookdate = date('d-M-Y', strtotime($booking_date));
                $car_number = ($data->plate_number == '') ? "" : $data->plate_number;
                $booking_time = date('H:i A', strtotime($booking_date));
                $journey_time = "";
                /*$travel_charge = $ride_status = $wait_time = $waiting_change = $travel_charge =
                $order_extra_charge = $tip = $km_driven = $total_pay = $is_paid = $cab_type = "";

                */
                $display_status = "";
                if ($trackable == "Y")
                {
                    continue;
                }
                $my_ride[] = array(
                    'source_lat' => $pickup_lat,
                    'source_lng' => $pickup_long,
                    'source' => ($source_data == "") ? "" : $source_data,
                    'destination' => ($destination == "") ? "" : $destination,
                    'date' => $bookdate,
                    'time' => $booking_time,
                    'cab_type' => $cab_type,
                    'journey_time' => "$journey_time",
                    'CBN' => $CBN,
                    'booking_id' => "$booking_id",
                    "status" => $status,

                    );


            }
            $this->response(array('status' => "1", 'ride' => $my_ride), 200);
        }
        else
        {
            $this->response(array(
                'status' => "1",
                "message" => NO_RIDE_EXIST,
                'ride' => array()), 200);
        }
    }
    function campaigns_detail()
    {
        $offer_detail = $this->Customer_model->current_offer();

        if ($offer_detail)
        {
            // echo "<pre>";
            foreach ($offer_detail as $campaigns_detail)
            {
                $coupon_detail[] = array(
                    "title" => $campaigns_detail['title'],
                    "offer" => $campaigns_detail['offer'],
                    "code" => $campaigns_detail['code'],
                    "desc" => $campaigns_detail['description'],
                    "start_date" => $campaigns_detail['start_date'],
                    "expire_date" => $campaigns_detail['expire_date']);
            }
            $this->response(array(
                'status' => "1",
                "campaigns" => $coupon_detail,
                ), 200);
        }
        else
        {
            $this->response(array(
                'status' => "0",
                "message" => NO_OFFER_EXIST,
                'campaigns' => array()), 200);
        }
    }
    function update_profile()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array(
            "user_id",
            "name",
            "country_code",
            "phone_number"));
        $update_profile = $this->Customer_model->update_profile($input_method);
        if ($update_profile)
        {
            $braintree_customer_id = $this->Customer_model->get_customer_braintree_id($input_method['user_id']);
            if ($braintree_customer_id)
            {
                $braintree_info['braintree_customer_id'] = $braintree_customer_id['braintree_customer_id'];
                $braintree_info['name'] = $input_method['name'];
                $braintree_info['phone'] = '+' . $input_method['country_code'] . $input_method['phone_number'];
                $braintree_response = UpdateExistingCustomerDetail($braintree_info);
                if ($braintree_response['status'] == 1)
                {
                    $this->response(array(
                        'status' => "1",
                        "message" => PROFILE_UPDATE,
                        "name" => $input_method['name'],
                        "country_code" => $input_method['country_code'],
                        "phone_number" => $input_method['phone_number']), 200);
                }
                else
                {
                    $this->response(array(
                        'status' => "0",
                        "message" => $braintree_response['message'],
                        'campaigns' => array()), 200);
                }
            }
            else
            {
                $this->response(array(
                    'status' => "0",
                    "message" => "Braintree customer id not get",
                    'campaigns' => array()), 200);
            }


        }
        else
        {
            $this->response(array(
                'status' => "0",
                "message" => SERVER_ERROR,
                'campaigns' => array()), 200);
        }
    }

    function change_password()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array(
            "user_id",
            "old_password",
            "new_password"));
        $password_update = $this->Customer_model->change_password($input_method);
        //print_r($password_update);
        if ($password_update['status'] == 1)
        {
            //update in xmpp server
            $user_name = USER_JID_NAME . '-' . $input_method['user_id'];
            $name = $password_update['user_detail']['name'];
            $update_password = ($input_method['new_password']);
            $param = array(
                $user_name,
                $name,
                $update_password);
            // print_r($param);
            $this->xmpp->api("update", $param);

            $this->response(array(
                'status' => "1",
                "message" => PASSWORD_SUCCESS,
                ), 200);
        }
        else
        {
            $this->response(array(
                'status' => "0",
                "message" => $password_update['message'],
                ), 200);
        }
    }

    function update_cc_detail()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array(
            "user_id",
            "credit_card_no",
            "expire_date",
            "cvv_no"));

        $customer_detail = $this->Customer_model->get_customer_braintree_id($input_method['user_id']);

        if ($customer_detail)
        {
            $braintree_info['braintree_customer_id'] = $customer_detail['braintree_customer_id'];
            $braintree_info['name'] = $customer_detail['name'];
            $braintree_info['phone'] = $customer_detail['country_code'] . $customer_detail['phone'];
            $braintree_info['cvv_no'] = $input_method['cvv_no'];
            $braintree_info['expire_date'] = $input_method['expire_date'];
            $braintree_info['credit_card_no'] = $input_method['credit_card_no'];
            $braintree_info['token'] = $customer_detail['token'];

            $braintree_response = Update_withUpdatingExistingCreditCard($braintree_info);
            //  print_r($braintree_response);
            if ($braintree_response['status'] == 1)
            {

                //update cc number in DB
                $update = $this->Customer_model->update_cc_info($input_method);
                if ($update)
                {
                    $this->response(array('status' => "1", "message" => CC_SUCCESS), 200);
                }
                else
                {
                    $this->response(array(
                        'status' => "0",
                        "message" => SERVER_ERROR,
                        ), 200);
                }
            }
            else
            {
                $this->response(array(
                    'status' => "0",
                    "message" => $braintree_response['message'],
                    ), 200);
            }
        }
        else
        {
            $this->response(array(
                'status' => "0",
                "message" => "Braintree customer id not get",
                ), 200);
        }
        //Update_withUpdatingExistingCreditCard($data);
    }

    function apply_coupon()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array(
            "user_id",
            "coupon_code",
            ));

        $coupon_detail = $this->Customer_model->get_coupon_detail($input_method);
        if ($coupon_detail)
        {
            $coupon_detail = $coupon_detail[0];
            $coupon_info = array(
                "title" => $coupon_detail['title'],
                "offer" => $coupon_detail['offer'],
                "desc" => $coupon_detail['description'],
                "start_date" => $coupon_detail['start_date'],
                "end_date" => $coupon_detail['expire_date']);

            $this->response(array(
                'status' => "1",
                "message" => SUCCESS,
                "coupon_detail" => $coupon_info), 200);

        }
        else
        {
            $this->response(array(
                'status' => "0",
                "message" => COUPON_NOT_VAILD,
                ), 200);
        }
    }
    function resend_otp()
    {
        $input_method = $this->webservices_inputs();
        $this->param_validate($input_method, array("user_id"));
        $otp_send = $this->Customer_model->get_otp($input_method);
        if ($otp_send)
        {
            $user_detail = $otp_send[0];
            if ($user_detail['is_varify'] == "1")
            {
                $this->response(array(
                    'status' => "0",
                    "message" => ACCOUNT_VERIFY,
                    ), 200);
            }
            else
            {
                /*chnage made 19 oct resend otp is not generate */
                $unique_number = random_number();
                $input_method['verifycode'] = $unique_number;
                $country_code = $user_detail['country_code'];
                $phone = $user_detail['phone'];
                /*this for enter resned otp into table 19 oct*/
                 $otp_resend = $this->Customer_model->resend_otp($input_method);
                $message = VARIFYSMS . $unique_number;
                $to = $country_code . $phone;
                send_sms($to, $message);

                $this->response(array(
                    'status' => "1",
                    "message" => OTP_RESET_CODE,
                    ), 200);
            }
        }
        else
        {
            $this->response(array(
                'status' => "0",
                "message" => $otp_send['message'],
                ), 200);
        }
    }
    function testdata()
    {
        echo "fv";
        send_mail_to_admin(268);
    }
    
    
    
 /* customer birthday */
 function get_customer_birthday()
{
   // echo 'hello';
	$date=date('Y-m-d');
	$get_customer=$this->General_model->Fetch_customer_birthday($date);
     
	if($get_customer)
	{

	 $html='<table id="example2" style="font-family:Trebuchet MS, Arial, Helvetica, sans-serif;width:30%;border-collapse: collapse">
                                        <thead>
                                            <tr>
                                                <th style="font-size: 1.1em;text-align:left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942; color: #ffffff;">
																				<center>Name</center></th>
                                                
                                                <th style="font-size: 1.1em;text-align:left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942; color: #ffffff;"><center>Email</center></th>
                                                <th style="font-size: 1.1em;text-align:left;padding-top: 5px;padding-bottom: 4px;background-color: #A7C942; color: #ffffff;"><center>contact</center></th>
                                                 </tr>	';
												 
	 foreach($get_customer as $data)
	 {
	
	
		$html.='<tr style=" color:"#000000;background-color: #EAF2D3;">
		      <td style=" font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;">
	'.$data['name'].'
			  </td>
			  <td style=" font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;">
	'.$data['email'].'
			  </td>
			   <td style=" font-size: 1em;border: 1px solid #98bf21;padding: 3px 7px 2px 7px;">
'.$data['phone'].'
			  </td>
			  </tr>';
                   
				                               
	 }
	
	    $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = true;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
         $this->email->from('support@privatedriver.no', 'pd');
        $this->email->to('post@privatedriver.no');
		//$this->email->cc('priyanka@foxinfosoft.com');
        $this->email->subject('Customer Birthday Report');
        $this->email->message($html);
      
        //$counter ++;
        if ($this->email->send())
        {
            echo '1';
        }
        else
        {
        
            //echo $filename;
            show_error($this->email->print_debugger());

        }
	
	
	
	}

}


}
