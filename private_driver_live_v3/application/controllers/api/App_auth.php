<?php

class Auth_module
{
    private $CI;

    function Auth_module()
    {
        $this->CI = &get_instance();
    }

    function index()
    {
        if ($this->CI->session->userdata('user_id') == "")
            // If no session found redirect to login page.
        {
            echo $_GET;
            redirect(site_url("login"));
        }
    }
}
?>